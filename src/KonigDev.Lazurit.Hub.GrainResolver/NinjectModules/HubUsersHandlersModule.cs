﻿using System.Collections.Generic;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Grains.Users.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Users;
using KonigDev.Lazurit.Hub.Model.DTO.Users.Queries;
using Ninject.Modules;
using KonigDev.Lazurit.Hub.Model.DTO.Users.Commands;
using KonigDev.Lazurit.Hub.Grains.Users.CommandHandlers;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    public class HubUsersHandlersModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IHubQueryHandler<GetUsersByShowRoomIdQuery, List<DtoUserProfile>>>().To<GetUsersByShowRoomIdQueryHandler>();
            Bind<IHubQueryHandler<GetUserProfileByIdQuery, DtoUserProfile>>().To<GetUserProfileByIdQueryHandler>();
            Bind<IHubQueryHandler<GetUserProfileByLinkIdQuery, DtoUserProfile>>().To<GetUserProfileByLinkIdQueryHandler>();

            Bind<IHubCommandHandler<AddUserCommand>>().To<AddUserProfileCommandHandler>();
            Bind<IHubCommandHandler<UpdateUserProfileCommand>>().To<UpdateUserProfileCommandHandler>();
            Bind<IHubCommandHandler<UpdateCityUserProfile>>().To<UpdateCityForUserProfileCommandHandler>();

        }
    }
}