﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Grains.Layaways.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Layaways.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Layaways;
using KonigDev.Lazurit.Hub.Model.DTO.Layaways.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Layaways.Query;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
   public class HubLayawayHandlerModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IHubCommandHandler<AddLayawayItemCommand>>().To<AddLayawayItemCommandHandler>();
            Bind<IHubCommandHandler<MergeLayawasCommand>>().To<MergeLayawasCommandHandler>();
            Bind<IHubCommandHandler<RemoveLayawayItemCommand>>().To<RemoveLayawayItemCommandHandler>();
            Bind<IHubCommandHandler<DeleteLayawayItemCommand>>().To<DeleteLayawayItemCommandHandler>();

            Bind<IHubQueryHandler<GetLayawaysByUserQuery, List<DtoLayaway>>>().To<GetLayawaysByUserQueryHandler>();
            Bind<IHubQueryHandler<GetLayawaysByLayawaysIdsQuery, List<DtoLayaway>>>().To<GetLayawaysByLayawaysIdsQueryHandler>();
        }
    }
}
