﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Grains.MarketingActions.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.MarketingActions.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.MarketingActions;
using KonigDev.Lazurit.Hub.Model.DTO.MarketingActions.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.MarketingActions.Queries;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    public class HubMarketingActionsModule : NinjectModule
    {
        public override void Load()
        {
            // Database
            // NOTE: Фэйковая фабрика юзабельна в случае, когда несколько человек работают с миграцией БД.
            //Bind<IDBContextFactory>()
            //    .To<DbContextWithFakeFactory>()                
            //    .WhenInjectedExactlyInto
            //    (
            //        typeof(CreateMarketingActionCommandHandler),
            //        typeof(DeleteMarketingActionCommandHandler),
            //        typeof(UpdateMarketingActionCommandHandler),
            //        typeof(SingleMarketingActionQueryHandler),
            //        typeof(AllMarketingActionsQueryHandler)
            //    )
            //    .InSingletonScope();
                

            // Commands
            Bind<IHubCommandHandler<CreateMarketingActionCommand>>().To<CreateMarketingActionCommandHandler>();
            Bind<IHubCommandHandler<DeleteMarketingActionCommand>>().To<DeleteMarketingActionCommandHandler>();
            Bind<IHubCommandHandler<UpdateMarketingActionCommand>>().To<UpdateMarketingActionCommandHandler>();

            // Queries
            Bind<IHubQueryHandler<SingleMarketingActionQuery, DtoMarketingAction>>().To<SingleMarketingActionQueryHandler>();
            Bind<IHubQueryHandler<AllMarketingActionsQuery, DtoMarketingActions>>().To<AllMarketingActionsQueryHandler>();
        }
    }
}
