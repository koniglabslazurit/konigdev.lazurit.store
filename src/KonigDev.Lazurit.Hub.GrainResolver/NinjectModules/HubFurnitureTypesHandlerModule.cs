﻿using KonigDev.Lazurit.Hub.Grains.FurnitureTypes.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.FurnitureTypes.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes;
using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using Ninject.Modules;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    public class HubFurnitureTypesHandlerModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IHubQueryHandler<GetFurnitureTypesByCollectionQuery, List<DtoFurnitureTypeResult>>>().To<GetFurnitureTypesByCollectionQueryHandler>();

            Bind<IHubCommandHandler<CreateFurnitureTypesCommand>>().To<CreateFurnitureTypesCommandHandler>();

            Bind<IHubQueryHandler<GetFurnitureTypesQuery, List<DtoFurnitureTypeItem>>>().To<GetFurnitureTypesQueryHandler>();

            Bind<IHubQueryHandler<GetFurnitureTypesWithDefaultProductQuery, List<DtoFurnitureTypeWithDefaultProductResult>>>().To<GetFurnitureTypesWithDefaultProductQueryHandler>();

            Bind<IHubQueryHandler<GetFurnitureTypesByProductRequestQuery, List<DtoFurnitureTypeItem>>>().To<GetFurnitureTypesByProductRequestQueryHandler>();

        }
    }
}
