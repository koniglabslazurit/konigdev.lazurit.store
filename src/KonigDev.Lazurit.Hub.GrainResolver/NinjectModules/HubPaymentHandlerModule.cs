﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Grains.Payments.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Payment.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Payment.Query;
using Ninject.Modules;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    public class HubPaymentHandlerModule: NinjectModule
    {
        public override void Load()
        {
            Bind<IHubQueryHandler<GetPaymentSettingsQuery, DtoPaymentSettings>>().To<GetPaymentSettingsQueryHandler>();
        }
    }
}
