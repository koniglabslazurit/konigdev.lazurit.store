﻿using KonigDev.Lazurit.Hub.Grains.IntegrationGrains.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers.CollectionsGrains;
using KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers.ProductsQueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto.Products;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query.Collection;
using Ninject.Modules;
using System.Collections.Generic;
using KonigDev.Lazurit.Hub.Grains.IntegrationGrains.CommandHandlers.IntegrationProductsContentCommandHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto.Collections;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Query;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Dto;
using KonigDev.Lazurit.Hub.Grains.CollectionsVariants.QueryHandlers;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    public class HubIntegrationHandlerModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IHubQueryHandler<GetIntegrationRoomsQuery, List<DtoIntegrationRoomItem>>>().To<GetIntegratedRoomsQueryHandler>();
            Bind<IHubQueryHandler<GetIntegrationCollectionsQuery, List<DtoIntegrationCollectionItem>>>().To<GetIntegrationCollectionsQueryHandler>();
            Bind<IHubQueryHandler<GetIntegrationCollectionsVariantsQuery, List<DtoIntegrationCollectionVariantItem>>>().To<GetIntegrationCollectionsVariantsQueryHandler>();
            Bind<IHubQueryHandler<GetIntegrationProductHashByCodeQuery, DtoIntegrationProduct>>().To<GetIntegrationProductHashByCodeQueryHandler>();
            Bind<IHubQueryHandler<GetIntegrationSeriesQuery, List<DtoIntegrationSeriesItem>>>().To<GetIntegrationSeriesQueryHandler>();
            Bind<IHubQueryHandler<GetIntegrationProductsQuery, List<DtoIntegrationProduct>>>().To<GetIntegrationProductsQueryHandler>();
            Bind<IHubQueryHandler<GetIntegrationVendorCodeQuery, List<DtoIntegrationVendorCodeItem>>>().To<GetIntegrationVendorCodeQueryHandler>();
            Bind<IHubQueryHandler<GetIntegrationFurnitureTypeQuery, List<DtoIntegrationFurnitureTypeItem>>>().To<GetIntegrationFurnitureTypeQueryHandler>();
            Bind<IHubQueryHandler<GetIntegrationProductByRequestQuery, DtoIntegrationProductResponse>>().To<GetIntegrationProductByRequestQueryHandler>();
            Bind<IHubQueryHandler<GetIntegrationCollectionsProductsQuery, DtoIntegrationCollectionTableResponse>>().To<GetIntegrationCollectionsProductsQueryHandler>();
            Bind<IHubQueryHandler<GetSyncCode1CsQuery, List<DtoSyncCode1CItem>>>().To<GetSyncCode1CsQueryHandler>();
            Bind<IHubQueryHandler<GetPublishedIntegrationProductsByVendorCodeQuery, DtoPublishedProductResponse>>().To<GetPublishedIntegrationProductsByVendorCodeQueryHandler>();
            Bind<IHubQueryHandler<GetIntegrationProductValidationQuery, DtoIntegrationProductValidation>>().To<GetIntegrationProductValidationQueryHandler>();
            Bind<IHubQueryHandler<GetIntegrationVariantsWithContentQuery, DtoIntegrationCollectionWithContent>>().To<GetIntegrationVariantsWithContentQueryHandler>();
            Bind<IHubQueryHandler<GetCollectionVariantValidatorQuery, DtoCollectionVariantValidation>>().To<GetCollectionVariantValidatorQueryHandler>();

            Bind<IHubCommandHandler<CreateIntegrationProductsCommand>>().To<CreateIntegrationProductsCommandHandler>();
            Bind<IHubCommandHandler<CreateIntegrationCollectionsCommand>>().To<CreateIntegrationCollectionsCommandHandler>();
            Bind<IHubCommandHandler<CreateIntegrationCollectionVariantsCommand>>().To<CreateIntegrationCollectionVariantsCommandHandler>();
            Bind<IHubCommandHandler<CreateIntegrationRoomsCommand>>().To<CreateIntegrationRoomsCommandHandler>();
            Bind<IHubCommandHandler<CreateIntegrationSeriesCommand>>().To<CreateIntegrationSeriesCommandHandler>();
            Bind<IHubCommandHandler<CreateVariantByIntegrationVariantCommand>>().To<CreateVariantByIntegrationVariantCommandHandler>();
            Bind<IHubCommandHandler<UpdateIntegrationProductsCommand>>().To<UpdateIntegrationProductsCommandHandler>();
            Bind<IHubCommandHandler<CreateIntegrationProductContentCommand>>().To<CreateIntegrationProductContentCommandHandler>();
            Bind<IHubCommandHandler<UpdateIntegrationProductContentCommand>>().To<UpdateIntegrationProductContentCommandHandler>();
            Bind<IHubCommandHandler<UpdateIntegrationVariantCommand>>().To<UpdateIntegrationVariantCommandHandler>();
            Bind<IHubCommandHandler<CreateIntegrationLoadingCommand>>().To<CreateIntegrationLoadingCommandHandler>();
            Bind<IHubCommandHandler<UpdateIntegrationProductsAfterImportCommand>>().To<UpdateIntegrationProductsAfterImportCommandHandler>();
        }
    }
}
