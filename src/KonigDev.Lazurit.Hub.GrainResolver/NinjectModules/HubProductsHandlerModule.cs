﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Grains.Products.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Products.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.RoomTypes.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Styles.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Styles.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.TargetAudiencesGrains.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.TargetAudiencesGrains.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO;
using KonigDev.Lazurit.Hub.Model.DTO.Products;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Query;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Styles.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Styles.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Styles.Query;
using KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Command;
using KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Query;
using Ninject.Modules;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    public class HubProductsHandlerModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IHubQueryHandler<GetProductsContextByCollectionVariantQuery, List<DtoProductContentResult>>>().To<GetProductsContentByCollectionVariantQueryHandler>();
            Bind<IHubQueryHandler<GetProductByUniqueFieldsQuery, DtoProductByUniqueFieldsResult>>().To<GetProductByUnicFieldsQueryHandler>();
            Bind<IHubQueryHandler<GetProductPartMarkersQuery, List<DtoProductPartMarker>>>().To<GetProductPartMarkersQueryHandler>();
            Bind<IHubQueryHandler<GetProductsByRequestQuery, List<DtoProduct>>>().To<GetProductsByRequestQueryHandler>();
            Bind<IHubQueryHandler<GetProductUnPublishValidationQuery, DtoProductUnPublishValidation>>().To<GetProductUnPublishValidationQueryHandler>();

            Bind<IHubCommandHandler<CreateProductContentCommand>>().To<CreateProductContentCommandHandler>();
            Bind<IHubCommandHandler<UpdateProductContentCommand>>().To<UpdateProductContentCommandHandler>();
            Bind<IHubCommandHandler<AddProductPartMarkerCommand>>().To<AddProductPartMarkerCommandHandler>();
            Bind<IHubCommandHandler<UpdateProductPricesCommand>>().To<UpdateProductPricesCommandHandler>();
            Bind<IHubCommandHandler<UnPublishProductCommand>>().To<UnPublishProductCommandHandler>();
            /* сопутствующие грейны */
            Bind<IHubQueryHandler<GetStylesQuery, List<DtoStyleItem>>>().To<GetStylesQueryHandler>();
            Bind<IHubQueryHandler<GetTargetAudiencesQuery, List<DtoTargetAudienceItem>>>().To<GetTargetAudiencesQueryHandler>();
            Bind<IHubQueryHandler<GetProductsMarketerQuery, DtoProductMarketerTableResponse>>().To<GetProductsMarketerQueryHandler>();
            Bind<IHubCommandHandler<CreateTargetsAudiencesCommand>>().To<CreateTargetsAudiencesCommandHandler>();
            Bind<IHubCommandHandler<CreateStylesCommand>>().To<CreateStylesCommandHandler>();
            Bind<IHubCommandHandler<UpdateOrCreateProductsPackCommand>>().To<UpdateOrCreateProductsPackCommandHandler>();
        }
    }
}
