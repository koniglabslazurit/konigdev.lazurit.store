﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Grains.SeriesGrains.CommandHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Seriess.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Seriess.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Seriess.Queries;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    [Serializable]
    public class HubSeriesHandlerModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IHubQueryHandler<GetSeriesByRequestQuery, List<DtoSeriesItem>>>().To<GetSeriesByRequestQueryHandler>();

            Bind<IHubCommandHandler<UpdateSeriesCommand>>().To<UpdateSeriesCommandHandler>();
        }
    }
}
