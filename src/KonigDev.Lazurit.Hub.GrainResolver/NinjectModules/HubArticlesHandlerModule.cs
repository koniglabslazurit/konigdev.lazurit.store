﻿using System.Collections.Generic;
using KonigDev.Lazurit.Hub.Grains.Articles.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Article;
using KonigDev.Lazurit.Hub.Model.DTO.Article.Query;
using Ninject.Modules;
using KonigDev.Lazurit.Hub.Model.DTO.Filters;
using KonigDev.Lazurit.Hub.Model.DTO.Property.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Property.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Article.Commands;
using KonigDev.Lazurit.Hub.Grains.Articles.CommandHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Article.Dto;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    public class HubArticlesHandlerModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IHubQueryHandler<GetArticleByUniqueFieldsQuery, DtoArticleByUniqueFieldsQueryResult>>().To<GetArticleByUniqueFieldsQueryHandler>();
            Bind<IHubQueryHandler<GetAvailableFiltersByArticleQuery, List<DtoFilterFrameColor>>>().To<GetAvailableFiltersByArticleQueryHandler>();
            Bind<IHubQueryHandler<GetAvailablePropertiesByArticleQuery, DtoProperties>>().To<GetAvailablePropertiesByArticleQueryHandler>();
            Bind<IHubQueryHandler<GetVendorCodesForInstallmentQuery, List<DtoVendorCodeItem>>>().To<GetVendorCodesForInstallmentQueryHandler>();

            Bind<IHubCommandHandler<CreateVendorCodeCommand>>().To<CreateVendorCodeCommandHandler>();
            Bind<IHubCommandHandler<UpdateVendorCodeCommand>>().To<UpdateVendorCodeCommandHandler>();
        }
    }
}