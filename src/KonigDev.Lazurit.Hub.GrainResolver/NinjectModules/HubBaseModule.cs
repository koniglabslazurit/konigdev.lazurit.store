﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using Ninject.Modules;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    public class HubBaseModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IDBContextFactory>().To<DBContextFactory>().WithConstructorArgument("connectionStringName","DefaultConnection");
        }
    }
}
