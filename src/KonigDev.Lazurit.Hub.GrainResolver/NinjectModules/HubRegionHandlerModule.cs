﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Grains.Regions.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Regions.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Queries;
using Ninject.Modules;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    public class HubRegionHandlerModule: NinjectModule
    {
        public override void Load()
        {
            Bind<IHubCommandHandler<CreateRegionCommand>>().To<CreateRegionCommandHandler>();
            Bind<IHubCommandHandler<RemoveRegionCommand>>().To<RemoveRegionCommandHandler>();
            Bind<IHubCommandHandler<UpdateRegionCommand>>().To<UpdateRegionCommandHandler>();
            Bind<IHubCommandHandler<UpdateRegionsCommand>>().To<UpdateRegionsCommandHandler>();

            Bind<IHubQueryHandler<GetRegionQuery, DtoRegion>>().To<GetRegionQueryHandler>();            
            Bind<IHubQueryHandler<GetRegionsQuery, List<DtoRegionItem>>>().To<GetRegionsQueryHandler>();
        }
    }
}
