﻿using KonigDev.Lazurit.Hub.Grains.CustomerRequests.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.CustomerRequests.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.CustomerRequests.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.CustomerRequests.Dtos;
using KonigDev.Lazurit.Hub.Model.DTO.CustomerRequests.Queries;
using Ninject.Modules;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    public class HubCustomerRequestHandlerModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IHubQueryHandler<GetAllCustomerRequestsQuery, DtoCustomerRequests>>().To<GetAllCustomerRequestsQueryHandler>();
            Bind<IHubCommandHandler<CreateCustomerRequestCommand>>().To<CreateCustomerRequestCommandHandler>();
            Bind<IHubCommandHandler<RemoveCustomerRequestCommand>>().To<RemoveCustomerRequestCommandHandler>();
            Bind<IHubCommandHandler<UpdateCustomerRequestStatusCommand>>().To<UpdateCustomerRequestStatusCommandHandler>();
        }
    }
}
