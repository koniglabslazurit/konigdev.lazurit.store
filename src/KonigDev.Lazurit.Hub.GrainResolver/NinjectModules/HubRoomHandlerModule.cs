﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Grains.RoomTypes.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.RoomTypes.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Query;
using Ninject.Modules;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    public class HubRoomHandlerModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IHubQueryHandler<GetRoomsQuery, List<DtoRoomItem>>>().To<GetRoomsQueryHandler>();
            Bind<IHubQueryHandler<GetRoomTypesQuery, List<DtoRoomTypesResult>>>().To<GetRoomTypesQueryHandler>();
            Bind<IHubQueryHandler<GetRoomTypesByProductRequestQuery, List<DtoRoomByProductItem>>>().To<GetRoomTypesByProductRequestQueryHandler>();

            Bind<IHubCommandHandler<UpdateRoomTypesCommand>>().To<UpdateRoomTypeCommandHandler>();
        }
    }
}