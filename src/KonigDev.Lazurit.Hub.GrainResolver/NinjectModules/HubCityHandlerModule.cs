﻿using KonigDev.Lazurit.Hub.Grains.Cities.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Cities.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Grains.Users.Region.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Queries;
using KonigDev.Lazurit.Hub.Model.DTO.Users;
using KonigDev.Lazurit.Hub.Model.DTO.Users.Region.Queries;
using Ninject.Modules;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    public class HubCityHandlerModule: NinjectModule
    {
        public override void Load()
        {            
            Bind<IHubQueryHandler<GetCitiesQuery, List<DtoCityItem>>>().To<GetCitiesQueryHandler>();
            Bind<IHubQueryHandler<GetCityByCoordinates, DtoCityCoordinates>>().To<GetNearestCityByCoordinatesQueryHandler>();
            Bind<IHubQueryHandler<GetAllCitiesQuery, List<DtoCity>>>().To<GetAllCitiesQueryHandler>();
            Bind<IHubQueryHandler<GetCityQuery, DtoCity>>().To<GetCityQueryHandler>();
            Bind<IHubQueryHandler<GetCitiesByRegionIdQuery, List<DTOCityResult>>>().To<GetCitiesByRegionIdQueryHandler>();

            Bind<IHubCommandHandler<CreateCityCommand>>().To<CreateCityCommandHandler>();
            Bind<IHubCommandHandler<RemoveCityCommand>>().To<RemoveCityCommandHandler>();
            Bind<IHubCommandHandler<UpdateCityCommand>>().To<UpdateCityCommandHandler>();
        }
    }
}
