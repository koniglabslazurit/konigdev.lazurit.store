﻿using KonigDev.Lazurit.Hub.Grains.Collections.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Collections;
using Ninject.Modules;
using System.Collections.Generic;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Query;
using KonigDev.Lazurit.Hub.Grains.CollectionsVariants.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Commands;
using KonigDev.Lazurit.Hub.Grains.Collections.CommandHandlers;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    public class HubCollectionsHandlersModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IHubQueryHandler<GetCollectionsQuery, List<DtoCollectionResultForCatalog>>>().To<GetCollectionsByRequestQueryHandler>();
            Bind<IHubQueryHandler<GetCollectionByAliasQuery, DtoCollectionByAliasResult>>().To<GetCollectionByAliasQueryHandler>();
            Bind<IHubQueryHandler<GetCollectionWithVariantsAndContentQuery, DtoCollectionWithContent>>().To<GetCollectionWithVariantsAndContentQueryHandler>();
            Bind<IHubQueryHandler<GetCollections, List<DtoCollectionItem>>>().To<GetCollectionsQueryHandler>();
            Bind<IHubQueryHandler<GetCollectionsVariantsQuery, List<DtoCollectionVariantItem>>>().To<GetCollectionsVariantsQueryHandler>();
            Bind<IHubQueryHandler<GetCollectionsForBreadcrumbsQuery, List<DtoCollectionItemBreadcrumbs>>>().To<GetCollectionsForBreadcrumbsQueryHandler>();
            Bind<IHubCommandHandler<UpdateCollectionCommand>>().To<UpdateCollectionCommandHandler>();
        }
    }
}
