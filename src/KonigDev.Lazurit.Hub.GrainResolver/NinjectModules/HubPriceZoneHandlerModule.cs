﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Grains.PriceZones.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.PriceZones.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Queries;
using Ninject.Modules;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    public class HubPriceZoneHandlerModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IHubCommandHandler<CreatePriceZoneCommand>>().To<CreatePriceZoneCommandHandler>();
            Bind<IHubCommandHandler<RemovePriceZoneCommand>>().To<RemovePriceZoneCommandHandler>();
            Bind<IHubCommandHandler<UpdatePriceZoneCommand>>().To<UpdatePriceZoneCommandHandler>();
            Bind<IHubCommandHandler<CreatePriceZonesCommand>>().To<CreatePriceZonesCommandHandler>();


            Bind<IHubQueryHandler<GetPrizeZonesQuery, List<DtoPriceZoneItem>>>().To<GetPrizeZonesQueryHandler>();
            Bind<IHubQueryHandler<GetPriceZoneQuery, DtoPriceZone>>().To<GetPriceZoneQueryHandler>();
        }
    }
}
