﻿using System.Collections.Generic;
using KonigDev.Lazurit.Hub.Grains.Carts.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Carts.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.CollectionsVariants.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Grains.Products.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Cart;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.FullCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.MiniCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Query;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Products;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Query;
using Ninject.Modules;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    public class HubCartHandlerModule: NinjectModule
    {
        public override void Load()
        {
            Bind<IHubCommandHandler<AddCartItemCommand>>().To<AddCartItemCommandHandler>();
            Bind<IHubCommandHandler<CreateCartCommand>>().To<CreateCartCommandHandler>();
            Bind<IHubCommandHandler<CreateOrUpdateCommand>>().To<CreateOrUpdateCartCommandHandler>();
            Bind<IHubCommandHandler<DeleteCartCommand>>().To<DeleteCartCommandHandler>();
            Bind<IHubCommandHandler<RemoveCartItemCommand>>().To<RemoveCartItemCommandHandler>();
            Bind<IHubCommandHandler<UpdateCartItemCommand>>().To<UpdateCartItemCommandHandler>();
            Bind<IHubQueryHandler<GetFullCartByIdQuery, DtoFullCart>>().To<GetFullCartByIdQueryHandler>();
            Bind<IHubQueryHandler<GetMiniCartByIdQuery, DtoMiniCart>>().To<GetMiniCartByIdQueryHandler>();
            Bind<IHubQueryHandler<GetMiniCartByUserIdQuery, DtoMiniCart>>().To<GetMiniCartByUserIdQueryHandler>();
            Bind<IHubQueryHandler<GetProductsPricesQuery, List<DtoProductPricesResult>>>().To<GetProductsPricesQueryHandler>();
            Bind<IHubQueryHandler<GetComplectsPricesQuery, List<DtoComplectPricesResult>>>().To<GetComplectsPricesQueryHandler>();
            Bind<IHubQueryHandler<GetCartIsApprovalRequiredQuery, DtoCartIsNeedApproval>>().To<GetCartIsApprovalRequiredQueryHandler>();
        }
    }
}
