﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Grains.PropertiesGrains.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.PropertiesGrains.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Property.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Property.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Property.Query;
using Ninject.Modules;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    public class HubPropertiesHandlersModule : NinjectModule
    {
        public override void Load()
        {            
            Bind<IHubQueryHandler<GetPropertiesQuery, List<DtoPropertyItem>>>().To<GetPropertiesQueryHandler>();
            Bind<IHubCommandHandler<CreatePropertiesCommand>>().To<CreatePropertiesCommandHandler>();
        }
    }
}
