﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Grains.ShowRooms.Queries;
using KonigDev.Lazurit.Hub.Model.DTO.ShowRooms;
using KonigDev.Lazurit.Hub.Model.DTO.ShowRooms.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.ShowRooms.Query;
using Ninject.Modules;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    [Serializable]
    public class HubShowRoomHandlersModule : NinjectModule
    {
        public override void Load()
        {            
            Bind<IHubQueryHandler<GetShowRoomsQuery, List<DtoShowRoomItem>>>().To<GetShowRoomsQueryHandler>();
            Bind<IHubQueryHandler<GetShowRoomsByCityIdQuery, List<DtoShowRoomResult>>>().To<GetShowRoomsByCityIdQueryHandler>();
        }
    }
}
