﻿using KonigDev.Lazurit.Hub.Grains.FacingColors.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.FacingColors.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Facings.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Facings.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.FrameColors.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.FrameColors.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Facings;
using KonigDev.Lazurit.Hub.Model.DTO.Facings.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Facings.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Facings.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Filters.Query;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Query;
using Ninject.Modules;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    public class HubFiltersHandlerModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IHubQueryHandler<GetFacingsByCollectionQuery, List<DtoFacingsResult>>>().To<GetFacingsByCollectionQueryHandler>();
            Bind<IHubQueryHandler<GetFacingsByRoomTypeIdQuery, List<DtoFacingsResult>>>().To<GetFacingsByRoomTypeIdQueryHandler>();
            Bind<IHubQueryHandler<GetFacingsQuery, List<DtoFacingItem>>>().To<GetFacingsQueryHandler>();
            Bind<IHubCommandHandler<CreateFacingsCommand>>().To<CreateFacingsCommandHandler>();
            Bind<IHubQueryHandler<GetFiltersByProductRequestQuery, List<DtoFacingItem>>>().To<GetFacingsByProductRequestQueryHandler>();

            Bind<IHubQueryHandler<GetFacingColorsByCollectionQuery, List<DtoFacingColorResult>>>().To<GetFacingColorsByCollectionQueryHandler>();
            Bind<IHubQueryHandler<GetFacingColorsByRoomTypeIdQuery, List<DtoFacingColorResult>>>().To<GetFacingColorsByRoomTypeIdQueryHandler>();
            Bind<IHubQueryHandler<GetFacingColorQuery, List<DtoFacingColorItem>>>().To<GetFacingColorsQueryHandler>();
            Bind<IHubCommandHandler<CreateFacingColorsCommand>>().To<CreateFacingColorsCommandHandler>();
            Bind<IHubQueryHandler<GetFiltersByProductRequestQuery, List<DtoFacingColorItem>>>().To<GetFacingColorsByProductRequestQueryHandler>();

            Bind<IHubQueryHandler<GetFrameColorsByCollectionQuery, List<DtoFrameColorResult>>>().To<GetFrameColorsByCollectionQueryHandler>();
            Bind<IHubQueryHandler<GetFrameColorsByRoomTypeIdQuery, List<DtoFrameColorResult>>>().To<GetFrameColorsByRoomTypeIdQueryHandler>();
            Bind<IHubQueryHandler<GetFrameColorQuery, List<DtoFrameColorItem>>>().To<GetFrameColorsQueryHandler>();
            Bind<IHubCommandHandler<CreateFrameColorsCommand>>().To<CreateFrameColorsCommandHandler>();
            Bind<IHubQueryHandler<GetFiltersByProductRequestQuery, List<DtoFrameColorItem>>>().To<GetFrameColorsByProductRequestQueryHandler>();
        }
    }
}
