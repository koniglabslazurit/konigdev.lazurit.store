﻿using System.Collections.Generic;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Dto;
using Ninject.Modules;
using KonigDev.Lazurit.Hub.Grains.FilesContent.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.FilesContent.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Command;
using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Queries;
using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Dto.Command;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    public class HubFileContentModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IHubCommandHandler<CreateFileContentCommand>>().To<CreateFileContentCommandHandler>();
            Bind<IHubCommandHandler<CreateUpdateFileContentCommand>>().To<CreateUpdateFileContentCommandHandler>();

            Bind<IHubQueryHandler<GetFilesContentByRequestQuery, List<DtoFilesContent>>>().To<GetFilesContentByRequestQueryHandler>();
            Bind<IHubQueryHandler<GetFilesContentObjectsQuery, DtoFilesContentObjectsResponse>>().To<GetFilesContentObjectsQueryHandler>();
        }
    }
}
