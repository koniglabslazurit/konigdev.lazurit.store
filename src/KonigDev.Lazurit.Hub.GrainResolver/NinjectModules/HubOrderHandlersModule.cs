﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Grains.Orders.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Orders.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Query;
using Ninject.Modules;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    public class HubOrderHandlersModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IHubCommandHandler<CreateOrderCommand>>().To<CreateOrderCommandHandler>();
            Bind<IHubCommandHandler<UpdateOrderApproveCommand>>().To<UpdateOrderApproveCommandHandler>();
            Bind<IHubCommandHandler<UpdateOrderPaidCommand>>().To<UpdateOrderPaidCommandHandler>();
            Bind<IHubCommandHandler<UpdateOrder1CFieldsCommand>>().To<UpdateOrder1CFieldsCommandHandler>();
            Bind<IHubCommandHandler<UpdateOrderKkmFieldsCommand>>().To<UpdateOrderKkmFieldsCommandHandler>();

            Bind<IHubQueryHandler<GetOrderItemQuery, DtoOrderItem>>().To<GetOrderItemQueryHandler>();
            Bind<IHubQueryHandler<GetOrderQuery, DtoOrder>>().To<GetOrderQueryHandler>();
            Bind<IHubQueryHandler<GetOrdersQuery, DtoOrderTableResponse>>().To<GetOrdersQueryHandler>();
            Bind<IHubQueryHandler<GetOrderItemByNumberQuery, DtoOrderItem>>().To<GetOrderItemByNumberQueryHandler>();
        }
    }
}
