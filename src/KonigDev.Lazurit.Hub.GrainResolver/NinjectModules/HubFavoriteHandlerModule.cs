﻿using KonigDev.Lazurit.Hub.Grains.Favorites.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Favorites.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Favorite;
using KonigDev.Lazurit.Hub.Model.DTO.Favorite.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Favorite.Query;
using Ninject.Modules;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    public class HubFavoriteHandlerModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IHubCommandHandler<AddFavoriteItemCommand>>().To<AddFavoriteItemCommandHandler>();
            Bind<IHubCommandHandler<CreateFavoriteCommand>>().To<CreateFavoriteCommandHandler>();
            Bind<IHubCommandHandler<RemoveFavoriteCommand>>().To<RemoveFavoriteCommandHandler>();
            Bind<IHubCommandHandler<RemoveFavoriteItemCommand>>().To<RemoveFavoriteItemCommandHandler>();
            Bind<IHubQueryHandler<GetFavoriteByUserQuery, DtoFavorite>>().To<GetFavoriteByUserQueryHandler>();
            Bind<IHubQueryHandler<GetShareIdByUserIdQuery, DtoShareId>>().To<GetShareIdByUserIdQueryHandler>();
        }
    }
}
