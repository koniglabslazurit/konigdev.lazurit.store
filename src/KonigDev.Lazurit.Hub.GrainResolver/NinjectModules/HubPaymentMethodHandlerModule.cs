﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Grains.PaymentMethods.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.PaymentMethods;
using KonigDev.Lazurit.Hub.Model.DTO.PaymentMethods.Query;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
   public class HubPaymentMethodHandlerModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IHubQueryHandler<GetPaymentMethodsQuery, List<DtoPaymentMethod>>>().To<GetPaymentMethodsQueryHandler>();          
        }
    }
}
