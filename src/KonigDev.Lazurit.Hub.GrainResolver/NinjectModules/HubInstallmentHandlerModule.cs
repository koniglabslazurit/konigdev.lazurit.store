﻿using KonigDev.Lazurit.Hub.Grains.InstallmentGrains.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.InstallmentGrains.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Installment.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Installment.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Installment.Query;
using Ninject.Modules;
using System;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    [Serializable]
    public class HubInstallmentHandlerModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IHubQueryHandler<GetInstallmentQuery, DtoInstallmentTableResponse>>().To<GetInstallmentQueryHandler>();
            
            Bind<IHubCommandHandler<CreateInstallmentCommand>>().To<CreateInstallmentCommandHandler>();

            Bind<IHubCommandHandler<DeleteInstallmentCommand>>().To<DeleteInstallmentCommandHandler>();
        }
    }
}
