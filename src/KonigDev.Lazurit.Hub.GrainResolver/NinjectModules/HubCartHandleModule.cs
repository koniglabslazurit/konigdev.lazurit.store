﻿using KonigDev.Lazurit.Hub.Grains.Carts.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Carts.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cart;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Query;
using Ninject.Modules;

namespace KonigDev.Lazurit.Hub.GrainResolver.NinjectModules
{
    public class HubCartHandleModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IHubQueryHandler<GetCartByUserQuery, DtoCart>>().To<GetCartByUserQueryHandler>();
            Bind<IHubCommandHandler<AddCartItemCommand>>().To<AddCartItemCommandHandler>();
            Bind<IHubCommandHandler<UpdateCartItemCommand>>().To<UpdateCartItemCommandHandler>();
            Bind<IHubCommandHandler<RemoveCartItemCommand>>().To<RemoveCartItemCommandHandler>();
        }
    }
}
