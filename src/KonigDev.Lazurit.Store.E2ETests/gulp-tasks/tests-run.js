var gulp = require("gulp");
var angularProtractor = require('gulp-angular-protractor');
var testsBuild = require("./tests-build");
var constants = require("./constants")

gulp.task("tests-run", ["tests-build"], function () {
    gulp.src(['dst/*.js'])
        .pipe(angularProtractor({ configFile: "protractor.conf.js" }))
        .on('error', function (e) { throw e; });
});