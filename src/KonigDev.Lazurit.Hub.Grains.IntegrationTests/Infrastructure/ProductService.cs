﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using System;
using KonigDev.Lazurit.Model.Entities;
using System.Linq;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.Infrastructure
{
    public static class ProductService
    {
        private const string connectionName = "LazuritBaseContext";

        public static void AddProduct(Guid id, Guid? collectionId = null)
        {
            IDBContextFactory _dbContextFactory = new DBContextFactory(connectionName);
 

            var furnitureType = new FurnitureType { Id = Guid.NewGuid(), SyncCode1C = Guid.NewGuid().ToString().Substring(0,25), Alias = Guid.NewGuid().ToString().Substring(0,25) };
            var article = new Article { Id = Guid.NewGuid(), FurnitureTypeId = furnitureType.Id, SyncCode1C = Guid.NewGuid().ToString().Substring(0,25) };
            var facingColor = new FacingColor { Id = Guid.NewGuid(), SyncCode1C = Guid.NewGuid().ToString().Substring(0,25) };
            var frameColor = new FrameColor { Id = Guid.NewGuid(), SyncCode1C = Guid.NewGuid().ToString().Substring(0,25) };
            var facing = new Facing { Id = Guid.NewGuid(), SyncCode1C = Guid.NewGuid().ToString().Substring(0,25) };

            var series = new Series { Id = Guid.NewGuid(), SyncCode1C = Guid.NewGuid().ToString().Substring(0,25), Alias = Guid.NewGuid().ToString().Substring(0,25) };
            var room = new RoomType { Id = Guid.NewGuid(), Alias = Guid.NewGuid().ToString().Substring(0,25), SyncCode1C = Guid.NewGuid().ToString().Substring(0,25) };

            var collection = new Collection { Id = collectionId == null ? Guid.NewGuid() : collectionId.Value, RoomTypeId = room.Id, SeriesId = series.Id };

            var collectionVariant = new CollectionVariant { Id = Guid.NewGuid(), CollectionId = collection.Id, FacingColorId = facingColor.Id, FacingId = facing.Id, FrameColorId = frameColor.Id };

            var product = new Product
            {
                Id = id,
                ArticleId = article.Id,
                FacingColorId = facingColor.Id,
                FrameColorId = frameColor.Id,
                FacingId = facing.Id,
                CollectionId = collection.Id,
                SyncCode1C = Guid.NewGuid().ToString().Substring(0,25)
            };

            product.CollectionsVariants.Add(collectionVariant);

            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var productMarkerId = context.Set<ProductPartMarker>().Select(p => p.Id).FirstOrDefault();
                product.ProductPartMarkerId = productMarkerId;
                context.Set<FurnitureType>().Add(furnitureType);
                context.Set<Series>().Add(series);
                context.Set<RoomType>().Add(room);
                context.Set<Collection>().Add(collection);
                context.Set<Facing>().Add(facing);
                context.Set<Article>().Add(article);
                context.Set<FacingColor>().Add(facingColor);
                context.Set<FrameColor>().Add(frameColor);
                context.Set<CollectionVariant>().Add(collectionVariant);               
                context.Set<Product>().Add(product);
                context.SaveChanges();
            }
        }

        public static void DeleteProduct(Guid id)
        {
            IDBContextFactory _dbContextFactory = new DBContextFactory(connectionName);
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var product = context.Set<Product>().FirstOrDefault(p => p.Id == id);
                var facing = context.Set<Facing>().FirstOrDefault(p => p.Id == product.FacingId);
                var facingColor = context.Set<FacingColor>().FirstOrDefault(p => p.Id == product.FacingColorId);
                var frameColor = context.Set<FrameColor>().FirstOrDefault(p => p.Id == product.FrameColorId);

                var collectionVariantId = product.CollectionsVariants.FirstOrDefault().Id;
                var collectionVariant = context.Set<CollectionVariant>().FirstOrDefault(p => p.Id == collectionVariantId);
                var series = context.Set<Series>().FirstOrDefault(p => p.Id == collectionVariant.Collection.SeriesId);
                var collection = context.Set<Collection>().FirstOrDefault(p => p.Id == collectionVariant.CollectionId);

                context.Set<Product>().Remove(product);
                context.Set<Facing>().Remove(facing);
                context.Set<FacingColor>().Remove(facingColor);
                context.Set<FrameColor>().Remove(frameColor);
                context.Set<Series>().Remove(series);
                context.Set<CollectionVariant>().Remove(collectionVariant);
                context.Set<Collection>().Remove(collection);
                                
                context.SaveChanges();
            }
        }

        public static void AddCollectionVariant(Guid id)
        {
            IDBContextFactory _dbContextFactory = new DBContextFactory(connectionName);
            Guid collectionTypeId;
            //using (var context = _dbContextFactory.CreateLazuritContext())
            //{
            //    collectionTypeId = context.Set<CollectionType>().Where(p => p.TechName == "Collection").Select(p => p.Id).FirstOrDefault();
            //}
            var furnitureType = new FurnitureType { Id = Guid.NewGuid(), SyncCode1C = Guid.NewGuid().ToString().Substring(0,25), Alias = Guid.NewGuid().ToString().Substring(0,25) };
            var article = new Article { Id = Guid.NewGuid(), FurnitureTypeId = furnitureType.Id, SyncCode1C = Guid.NewGuid().ToString().Substring(0,25) };
            var facingColor = new FacingColor { Id = Guid.NewGuid(), SyncCode1C = Guid.NewGuid().ToString().Substring(0,25) };
            var frameColor = new FrameColor { Id = Guid.NewGuid(), SyncCode1C = Guid.NewGuid().ToString().Substring(0,25) };
            var facing = new Facing { Id = Guid.NewGuid(), SyncCode1C = Guid.NewGuid().ToString().Substring(0,25) };

            var series = new Series { Id = Guid.NewGuid(), SyncCode1C = Guid.NewGuid().ToString().Substring(0,25), Alias = Guid.NewGuid().ToString().Substring(0,25) };
            var room = new RoomType { Id = Guid.NewGuid(), Alias = Guid.NewGuid().ToString().Substring(0,25), SyncCode1C = Guid.NewGuid().ToString().Substring(0,25) };
            var collection = new Collection { Id = Guid.NewGuid(), RoomTypeId = room.Id, SeriesId = series.Id };
            var collectionVariant = new CollectionVariant { Id = id, CollectionId = collection.Id, FacingColorId = facingColor.Id, FacingId = facing.Id, FrameColorId = frameColor.Id };

            var product = new Product
            {
                Id = Guid.NewGuid(),
                ArticleId = article.Id,
                FacingColorId = facingColor.Id,
                FrameColorId = frameColor.Id,
                FacingId = facing.Id,
                SyncCode1C = Guid.NewGuid().ToString().Substring(0,25),
                CollectionId = collection.Id
            };

            product.CollectionsVariants.Add(collectionVariant);

            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var productMarkerId = context.Set<ProductPartMarker>().Select(p => p.Id).FirstOrDefault();
                product.ProductPartMarkerId = productMarkerId;
                context.Set<FurnitureType>().Add(furnitureType);
                context.Set<Series>().Add(series);
                context.Set<RoomType>().Add(room);
                context.Set<Collection>().Add(collection);
                context.Set<Facing>().Add(facing);
                context.Set<Article>().Add(article);
                context.Set<FacingColor>().Add(facingColor);
                context.Set<FrameColor>().Add(frameColor);
                context.Set<CollectionVariant>().Add(collectionVariant);
                context.Set<Product>().Add(product);

                //foreach (var i in context.GetValidationErrors())
                //{
                //    var t = i.ValidationErrors;
                //}
                context.SaveChanges();
            }
        }

        public static void DeleteCollectionVariant(Guid id)
        {
            IDBContextFactory _dbContextFactory = new DBContextFactory(connectionName);
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var collectionVariant = context.Set<CollectionVariant>().FirstOrDefault(p => p.Id == id);

                var product = context.Set<Product>().FirstOrDefault(p => p.CollectionsVariants.Any(c => c.Id == id));
                var facing = context.Set<Facing>().FirstOrDefault(p => p.Id == product.FacingId);
                var facingColor = context.Set<FacingColor>().FirstOrDefault(p => p.Id == product.FacingColorId);
                var frameColor = context.Set<FrameColor>().FirstOrDefault(p => p.Id == product.FrameColorId);
                var series = context.Set<Series>().FirstOrDefault(p => p.Id == collectionVariant.Collection.SeriesId);
                var collection = context.Set<Collection>().FirstOrDefault(p => p.Id == collectionVariant.CollectionId);


                context.Set<CollectionVariant>().Remove(collectionVariant);
                context.Set<Product>().Remove(product);
                context.Set<Facing>().Remove(facing);
                context.Set<FacingColor>().Remove(facingColor);
                context.Set<FrameColor>().Remove(frameColor);
                context.Set<Series>().Remove(series);
                context.Set<Collection>().Remove(collection);
                context.SaveChanges();
            }
        }
    }
}
