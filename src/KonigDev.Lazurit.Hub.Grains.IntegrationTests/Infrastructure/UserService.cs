﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using System;
using KonigDev.Lazurit.Model.Entities;
using System.Linq;


namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.Infrastructure
{
    public static class UserService
    {
        private const string connectionName = "LazuritBaseContext";      

        public static void AddTestUser(Guid id)
        {
            IDBContextFactory _dbContextFactory = new DBContextFactory(connectionName);
            var userProfile = new UserProfile
            {
                Id = id
            };
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                context.Set<UserProfile>().Add(userProfile);
                context.SaveChanges();
            }
        }
        public static void DeleteTestUser(Guid id)
        {
            IDBContextFactory _dbContextFactory = new DBContextFactory(connectionName);
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var userProfile = context.Set<UserProfile>().FirstOrDefault(p => p.Id == id);
                context.Set<UserProfile>().Remove(userProfile);
                context.SaveChanges();
            }
        }

    }
}
