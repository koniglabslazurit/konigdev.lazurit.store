﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.NUnit3;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.Infrastructure.Attributes.CommonAutoFixture
{
    public class NotCircularAutoData : AutoDataAttribute
    {
        public NotCircularAutoData() : base(new Fixture().Customize(new CircularCustomization()))
        {
        }

        private class CircularCustomization : ICustomization
        {
            public void Customize(IFixture fixture)
            {
                fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            }
        }

    }
}
