﻿using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Regions.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Regions.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Queries;
using NUnit.Framework;
using System;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.Region
{
    [TestFixture]
    public class UpdateRegionCommandHandlerTests
    {
        private IDBContextFactory _contextFactory;
        private Guid _regionId;
        [SetUp]
        public void SetUp()
        {
            _contextFactory = new DBContextFactory("LazuritBaseContext");
            _regionId = Guid.NewGuid();
            
        }
        [Test]
        public void UpdateRegion_ShouldUpdateRegion_WhenFoundIt()
        {
            //arrange
            var regionUpdate = new UpdateRegionCommand
            {
                Id = _regionId,
                Title = "LeningradskayaRO",
                LazuritPasswordHash = "0000000000",
                SyncCode1C = "0002"
            };
            var updateCommand = new UpdateRegionCommandHandler(_contextFactory);
            var getQuery = new GetRegionQueryHandler(_contextFactory);
            var query = new GetRegionQuery { Id = _regionId };
            var removeCommand = new RemoveRegionCommandHandler(_contextFactory);
            var regionCreate = new CreateRegionCommand
            {
                Id = _regionId,
                Title = "KaliningradskayaRO",
                LazuritPasswordHash = "123456789",
                SyncCode1C = "0001"
            };
            var createHandler = new CreateRegionCommandHandler(_contextFactory);
            createHandler.Execute(regionCreate).Wait();
            //act
            updateCommand.Execute(regionUpdate).Wait();
            //assert
            Assert.AreEqual(_regionId, getQuery.Execute(query).Result.Id);
            Assert.AreEqual("LeningradskayaRO", getQuery.Execute(query).Result.Title);
            Assert.AreEqual("0000000000", getQuery.Execute(query).Result.LazuritPasswordHash);
            Assert.AreEqual("0002", getQuery.Execute(query).Result.SyncCode1C);
            removeCommand.Execute(new RemoveRegionCommand { Id = _regionId}).Wait();
        }

        [Test]
        public void UpdateRegion_ShouldReturnException_WhenNotFoundIt()
        {
            //arrange
            var regionUpdate = new UpdateRegionCommand
            {
                Id = Guid.NewGuid(),
                Title = "LeningradskayaRO",
                LazuritPasswordHash = "0000000000",
                SyncCode1C = "0002"
            };
            var updateCommand = new UpdateRegionCommandHandler(_contextFactory);
            //act
            //assert
            Assert.ThrowsAsync<NotValidCommandException>(async() =>await updateCommand.Execute(regionUpdate));
        }

        [Test]
        public void UpdateRegion_ShouldReturnException_WhenModelGuidIsEmpty()
        {
            //arrange
            var regionUpdate = new UpdateRegionCommand
            {
                Id = Guid.Empty,
                Title = "LeningradskayaRO",
                LazuritPasswordHash = "0000000000",
                SyncCode1C = "0002"
            };
            var updateCommand = new UpdateRegionCommandHandler(_contextFactory);
            //act
            //assert
            Assert.ThrowsAsync<NotValidCommandException>(async () => await updateCommand.Execute(regionUpdate));
        }
    }
}
