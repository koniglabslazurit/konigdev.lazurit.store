﻿using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Regions.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Regions.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Queries;
using NUnit.Framework;
using System;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.Region
{
    [TestFixture]
    public class CreateRegionCommandHandlerTests
    {
        private IDBContextFactory _contextFactory;
        [SetUp]
        public void SetUp()
        {
            _contextFactory = new DBContextFactory("LazuritBaseContext");
        }
        [Test]
        public void CreateRegion_ShouldAddRegion_WhenValidModel()
        {
            //arrange
            var region = new CreateRegionCommand
            {
                Id = Guid.NewGuid(),
                Title = "KaliningradskayaRO",
                LazuritPasswordHash = "123456789",
                SyncCode1C = "789745"
            };
            var query = new GetRegionQuery
            {
                Id = region.Id
            };
            var createHandler = new CreateRegionCommandHandler(_contextFactory);
            var getQuery = new GetRegionQueryHandler(_contextFactory);
            var removeHandler = new RemoveRegionCommandHandler(_contextFactory);
            //act
            createHandler.Execute(region).Wait();
            //assert
            Assert.AreEqual(region.Id, getQuery.Execute(query).Result.Id);
            Assert.AreEqual("KaliningradskayaRO", getQuery.Execute(query).Result.Title);
            Assert.AreEqual("123456789", getQuery.Execute(query).Result.LazuritPasswordHash);
            Assert.AreEqual("789745", getQuery.Execute(query).Result.SyncCode1C);
            removeHandler.Execute(new RemoveRegionCommand { Id = region.Id }).Wait();
        }

        [Test]
        public void CreateRegion_ShouldNotAddRegion_WhenModelGuidIsEmpty()
        {
            //arrange
            var region = new CreateRegionCommand
            {
                Id = Guid.Empty,
                Title = "KaliningradskayaRO",
                LazuritPasswordHash = "123456789",
                SyncCode1C = "0001"
            };
            var query = new GetRegionQuery
            {
                Id = region.Id
            };
            var createHandler = new CreateRegionCommandHandler(_contextFactory);
            //act
            //assert
            Assert.ThrowsAsync<NotValidCommandException>(async () =>  await createHandler.Execute(region) );
        }
        [Test]
        public void CreateRegion_ShouldNotAddRegion_WhenTitleIsEmpty()
        {
            //arrange
            var region = new CreateRegionCommand
            {
                Id = Guid.NewGuid(),
                Title = "",
                LazuritPasswordHash = "123456789",
                SyncCode1C = "0001"
            };
            var query = new GetRegionQuery
            {
                Id = region.Id
            };
            var createHandler = new CreateRegionCommandHandler(_contextFactory);
            //act
            //assert
            Assert.ThrowsAsync<NotValidCommandException>(async () => await createHandler.Execute(region));
        }
    }
}
