﻿using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Regions.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Regions.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Queries;
using NUnit.Framework;
using System;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.Region
{
    [TestFixture]
    public class RemoveRegionCommandHandlerTests
    {
        private IDBContextFactory _contextFactory;
        private Guid _regionId;
        [SetUp]
        public void SetUp()
        {
            _contextFactory = new DBContextFactory("LazuritBaseContext");
            _regionId = Guid.NewGuid();
        }
        [Test]
        public void RemoveRegion_ShouldRemoveRegion_WhenFoundIt()
        {
            //arrange
            var regionRemove = new RemoveRegionCommand
            {
                Id = _regionId
            };
            var getQuery = new GetRegionQueryHandler(_contextFactory);
            var query = new GetRegionQuery { Id = _regionId };
            var removeCommand = new RemoveRegionCommandHandler(_contextFactory);
            var regionCreate = new CreateRegionCommand
            {
                Id = _regionId,
                Title = "KaliningradskayaRO",
                LazuritPasswordHash = "123456789",
                SyncCode1C = "2125454"
            };
            var createHandler = new CreateRegionCommandHandler(_contextFactory);
            createHandler.Execute(regionCreate).Wait();
            //act
            removeCommand.Execute(regionRemove).Wait();
            //assert
            Assert.IsNull(getQuery.Execute(query).Result);
        }

        [Test]
        public void RemoveRegion_ShouldReturnException_WhenNotFoundIt()
        {
            //arrange
            var regionRemove = new RemoveRegionCommand
            {
                Id = _regionId
            };
            var removeCommand = new RemoveRegionCommandHandler(_contextFactory);
            //act
            //assert
            Assert.ThrowsAsync<NotValidCommandException>(async () => await removeCommand.Execute(regionRemove));
        }

        [Test]
        public void RemoveRegion_ShouldReturnException_WhenModelIdEmpty()
        {
            //arrange
            var regionRemove = new RemoveRegionCommand
            {
                Id = Guid.Empty
            };
            var removeCommand = new RemoveRegionCommandHandler(_contextFactory);
            //act
            //assert
            Assert.ThrowsAsync<NotValidCommandException>(async () => await removeCommand.Execute(regionRemove));
        }
    }
}

