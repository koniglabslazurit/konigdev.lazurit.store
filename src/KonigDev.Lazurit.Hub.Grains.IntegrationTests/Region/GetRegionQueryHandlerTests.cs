﻿using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Regions.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Regions.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Queries;
using NUnit.Framework;
using System;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.Region
{
    [TestFixture]
    public class GetRegionQueryHandlerTests
    {
        private IDBContextFactory _contextFactory;
        private Guid _region1Id;
        private Guid _region2Id;
        private Guid _region3Id;
        [OneTimeSetUp]
        public void SetUp()
        {
            _region1Id = Guid.NewGuid();
            _region2Id = Guid.NewGuid();
            _region3Id = Guid.NewGuid();
            _contextFactory = new DBContextFactory("LazuritBaseContext");
            var region1Create = new CreateRegionCommand
            {
                Id = _region1Id,
                Title = "LeningradskayaRO",
                LazuritPasswordHash = "99999999",
                SyncCode1C = "0002"
            };
            var region2Create = new CreateRegionCommand
            {
                Id = _region2Id,
                Title = "KaliningradskayaRO",
                LazuritPasswordHash = "123456789",
                SyncCode1C = "0001"
            };
            var region3Create = new CreateRegionCommand
            {
                Id = _region3Id,
                Title = "NovgorodskayaRO",
                LazuritPasswordHash = "111111111",
                SyncCode1C = "0003"
            };
            var createHandler = new CreateRegionCommandHandler(_contextFactory);
            createHandler.Execute(region1Create).Wait();
            createHandler.Execute(region2Create).Wait();
            createHandler.Execute(region3Create).Wait();
        }
        [Test]
        public void GetRegion_ShouldReturnRegion1_WhenFoundIt()
        {
            //arrange
            var getRegion = new GetRegionQuery
            {
                Id = _region1Id
            };
            var getQuery = new GetRegionQueryHandler(_contextFactory);
            var removeCommand = new RemoveRegionCommandHandler(_contextFactory);
            //act
            var actual = getQuery.Execute(getRegion);
            //assert
            Assert.AreEqual(_region1Id, actual.Result.Id);
            Assert.AreEqual("LeningradskayaRO", actual.Result.Title);
            Assert.AreEqual("99999999", actual.Result.LazuritPasswordHash);
            Assert.AreEqual("0002", actual.Result.SyncCode1C);
            removeCommand.Execute(new RemoveRegionCommand { Id = _region1Id }).Wait();
            removeCommand.Execute(new RemoveRegionCommand { Id = _region2Id }).Wait();
            removeCommand.Execute(new RemoveRegionCommand { Id = _region3Id }).Wait();
        }

        [Test]
        public void GetRegion_ShouldReturnNull_WhenNotFoundIt()
        {
            //arrange
            var getRegion = new GetRegionQuery
            {
                Id = Guid.NewGuid()
            };
            var getQuery = new GetRegionQueryHandler(_contextFactory);
            //act
            //assert
            Assert.IsNull(getQuery.Execute(getRegion).Result);
        }

        [Test]
        public void GetRegion_ShouldReturnException_WhenModelIdEmpty()
        {
            //arrange
            var getRegion = new GetRegionQuery
            {
                Id = Guid.Empty
            };
            var getQuery = new GetRegionQueryHandler(_contextFactory);
            //act
            //assert
            Assert.Throws<NotValidCommandException>(() => getQuery.Execute(getRegion));
        }
    }
}

