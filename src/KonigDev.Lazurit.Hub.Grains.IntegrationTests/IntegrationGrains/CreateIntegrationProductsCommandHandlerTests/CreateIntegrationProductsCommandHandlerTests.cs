﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Core.Enums.Enums;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.IntegrationGrains.CommandHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.Mappings.Integrations;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using NUnit.Framework;
using Ploeh.AutoFixture;

//namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.IntegrationGrains.CreateIntegrationProductsCommandHandlerTests
//{
//    [TestFixture]
//    public class CreateIntegrationProductsCommandHandlerTests
//    {
//        private IDBContextFactory _dbContextFactory;
//        private readonly List<string> _addedProductSyncCodes = new List<string>();
//        private readonly List<string> _addedProductSyncCodesInUpdate = new List<string>();
//        private readonly List<string> _changedProductSyncCodes = new List<string>();
//        private readonly List<Guid> _addedCollectionVariantsId = new List<Guid>();
//        private readonly List<Guid> _deletedIntegrationCollections = new List<Guid>();
//        private List<DtoIntegrationProductCreating> _productsTestCases;
//        private List<IntegrationCollectionVariant> _collectionVariants;

//        [SetUp]
//        public void SetUp()
//        {
//            _dbContextFactory = new DBContextFactory("LazuritIntegrationContext");
//            var fixture = new Fixture();
//            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
//            _productsTestCases = fixture.Create<List<DtoIntegrationProductCreating>>();
//            _collectionVariants = fixture.Create<List<IntegrationCollectionVariant>>();
//        }

//        [Test, Ignore("TODO!!! edited grain.")]
//        public async Task Execute_ProductsWithMissingStatus_AddProductsOnlyWithMissingStatus()
//        {
//            //assert
//            _productsTestCases.ForEach(x => x.IntegrationStatus = EnumIntegrationProductStatus.NotChanged);
//            var firstProduct = _productsTestCases.First();
//            firstProduct.IntegrationStatus = EnumIntegrationProductStatus.Missing;
//            _addedProductSyncCodes.Add(firstProduct.SyncCode1C);
//            if (_productsTestCases.Count > 1)
//            {
//                var lastProduct = _productsTestCases.Last();
//                lastProduct.IntegrationStatus = EnumIntegrationProductStatus.Missing;
//                _addedProductSyncCodes.Add(lastProduct.SyncCode1C);
//            }
//            var cmd = new CreateIntegrationProductsCommandHandler(_dbContextFactory);

//            //act
//            int productStartedCount = 0;
//            using (var ctx = _dbContextFactory.CreateLazuritIntegrationContext())
//            {
//                productStartedCount = ctx.Set<IntegrationProduct>().Count();
//            }
//            await cmd.Execute(new CreateIntegrationProductsCommand() { Products = _productsTestCases });

//            //arrange
//            using (var ctx = _dbContextFactory.CreateLazuritIntegrationContext())
//            {
//                //проверяем на то что добавились продукты со статусом Missing
//                foreach (var productSyncCode in _addedProductSyncCodes)
//                {
//                    var product = ctx.Set<IntegrationProduct>().FirstOrDefault(x => x.SyncCode1C == productSyncCode);
//                    Assert.IsNotNull(product);
//                }

//                //проверяем на то что кроме продуктов со статусом Missing больше в таблицу IntegrationProduct ничего не добавилось
//                int productEndedCount = ctx.Set<IntegrationProduct>().Count() - _addedProductSyncCodes.Count;
//                Assert.IsTrue(productEndedCount == productStartedCount);
//            }
//        }

//        [Test]
//        public async Task Execute_ProductsWithChangedStatus_UpdateProductsOnlyWithChangedStatus()
//        {
//            //assert
//            _productsTestCases.ForEach(x => x.IntegrationStatus = EnumIntegrationProductStatus.NotChanged);
//            var firstProduct = _productsTestCases.First();
//            firstProduct.IntegrationStatus = EnumIntegrationProductStatus.Changed;
//            _changedProductSyncCodes.Add(firstProduct.SyncCode1C);

//            using (var ctx = _dbContextFactory.CreateLazuritIntegrationContext())
//            {
//                firstProduct.FacingColor = Guid.NewGuid().ToString();
//                firstProduct.SleepingAreaWidth = Guid.NewGuid().ToString();
//                var entityProduct = firstProduct.MapToEntity();
//                entityProduct.ProductId = Guid.NewGuid();
//                entityProduct.IntegrationStatusId = ctx.Set<IntegrationStatus>().Where(p => p.TechName == EnumIntegrationStatus.Validation.ToString()).Select(p => p.Id).FirstOrDefault();
//                ctx.Set<IntegrationProduct>().Add(entityProduct);
//                ctx.SaveChanges();
//            }

//            var cmd = new CreateIntegrationProductsCommandHandler(_dbContextFactory);

//            //act
//            await cmd.Execute(new CreateIntegrationProductsCommand() { Products = _productsTestCases });

//            //arrange
//            using (var ctx = _dbContextFactory.CreateLazuritIntegrationContext())
//            {
//                var firstProductEntity = ctx.Set<IntegrationProduct>().Single(x => x.SyncCode1C == firstProduct.SyncCode1C);
//                Assert.IsTrue(firstProductEntity.FacingColor == firstProduct.FacingColor);
//                Assert.IsTrue(firstProductEntity.SleepingAreaWidth == firstProduct.SleepingAreaWidth);
//            }
//        }

//        [Test, Ignore("Проверка на вариант коллекции!!!")]
//        public async Task Execute_ProductsCollectionVariants_ProductHaveCollectionVariants()
//        {
//            //assert
//            _collectionVariants.ForEach(x =>
//            {
//                x.IntegrationProducts = new List<IntegrationProduct>();
//                _deletedIntegrationCollections.Add(x.IntegrationCollection.Id);
//            });
//            using (var ctx = _dbContextFactory.CreateLazuritIntegrationContext())
//            {
//                ctx.Set<IntegrationCollectionVariant>().AddRange(_collectionVariants);
//                ctx.SaveChanges();
//                _addedCollectionVariantsId.AddRange(_collectionVariants.Select(x => x.IntegrationCollectionId));
//            }

//            var relatedCollectionVariant = _collectionVariants.First();
//            _productsTestCases.ForEach(x =>
//            {
//                x.IntegrationStatus = EnumIntegrationProductStatus.Missing;
//                x.FacingColor = relatedCollectionVariant.FacingColor;
//                x.Facing = relatedCollectionVariant.Facing;
//                x.FrameColor = relatedCollectionVariant.FrameColor;
//                _addedProductSyncCodesInUpdate.Add(x.SyncCode1C);
//            });


//            var cmd = new CreateIntegrationProductsCommandHandler(_dbContextFactory);

//            //act
//            await cmd.Execute(new CreateIntegrationProductsCommand() { Products = _productsTestCases });

//            //arrange
//            using (var ctx = _dbContextFactory.CreateLazuritIntegrationContext())
//            {
//                var productEntities = ctx.Set<IntegrationProduct>().Include(x => x.IntegrationCollectionsVariants).Where(x => _addedProductSyncCodesInUpdate.Contains(x.SyncCode1C));

//                foreach (var productEntity in productEntities)
//                {
//                    var assertCollectionVariant = productEntity.IntegrationCollectionsVariants.SingleOrDefault(x => x.IntegrationCollectionId == relatedCollectionVariant.IntegrationCollectionId);
//                    Assert.IsNotNull(assertCollectionVariant);
//                }
//            }
//        }


//        [TearDown]
//        public void Dispose()
//        {
//            DeleteProducts(_addedProductSyncCodes);
//            DeleteProducts(_addedProductSyncCodesInUpdate);
//            DeleteProducts(_changedProductSyncCodes);
//            DeleteCollectionVariants(_addedCollectionVariantsId);
//            DeleteCollection(_deletedIntegrationCollections);
//        }

//        private void DeleteCollection(List<Guid> deletedIntegrationCollections)
//        {
//            using (var ctx = _dbContextFactory.CreateLazuritIntegrationContext())
//            {
//                foreach (var collectionId in deletedIntegrationCollections)
//                {
//                    var colelction = ctx.Set<IntegrationCollection>().FirstOrDefault(x => x.Id == collectionId);
//                    if (colelction != null)
//                        ctx.Set<IntegrationCollection>().Remove(colelction);
//                }
//                ctx.SaveChanges();
//            }
//        }

//        private void DeleteCollectionVariants(List<Guid> addedCollectionVariantsId)
//        {
//            using (var ctx = _dbContextFactory.CreateLazuritIntegrationContext())
//            {
//                foreach (var collectionId in addedCollectionVariantsId)
//                {
//                    var colelction = ctx.Set<IntegrationCollectionVariant>().FirstOrDefault(x => x.IntegrationCollectionId == collectionId);
//                    if (colelction != null)
//                        ctx.Set<IntegrationCollectionVariant>().Remove(colelction);
//                }
//                ctx.SaveChanges();
//            }
//        }

//        private void DeleteProducts(List<string> productsForDelete)
//        {
//            using (var ctx = _dbContextFactory.CreateLazuritIntegrationContext())
//            {
//                foreach (var productSyncCode in productsForDelete)
//                {
//                    var product = ctx.Set<IntegrationProduct>().FirstOrDefault(x => x.SyncCode1C == productSyncCode);
//                    if (product != null)
//                        ctx.Set<IntegrationProduct>().Remove(product);
//                }
//                ctx.SaveChanges();
//            }
//        }
//    }
//}
