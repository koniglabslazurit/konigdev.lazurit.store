﻿using KonigDev.Lazurit.Core.Enums.Exeptions.Cart;
using System.Linq;
using KonigDev.Lazurit.Hub.Grains.Carts.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Carts.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Query;
using KonigDev.Lazurit.Model.Entities;
using NUnit.Framework;
using System;
using KonigDev.Lazurit.Hub.Grains.IntegrationTests.Infrastructure;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;
using System.Collections.Generic;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.MiniCartDto;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.Cart
{
    [TestFixture]
    public class AddCartItemCommandHandlerTest
    {
        private IDBContextFactory _dbContextFactory;

        [SetUp]
        public void SetUp()
        {
            _dbContextFactory = new DBContextFactory("LazuritBaseContext");
        }

        [Test]
        public void Add_Product_Item_To_Empty_Cart()
        {
            //arrange
            var userId = Guid.NewGuid();
            UserService.AddTestUser(userId);
            var productId = Guid.NewGuid(); var collectionId = Guid.NewGuid();
            ProductService.AddProduct(productId, collectionId);

            var context = _dbContextFactory.CreateLazuritContext();
            var addItemToCartCommad = new AddCartItemCommand
            {
                AddElementId = productId,
                CartItemType = Core.Enums.EnumCartItemType.Product,
                CartId = Guid.NewGuid()
            };
            var getCartQuery = new GetFullCartByIdQueryHandler(_dbContextFactory);
            var addCartItemCommandHandler = new AddCartItemCommandHandler(_dbContextFactory);
            //act
            addCartItemCommandHandler.Execute(addItemToCartCommad).Wait();
            //assert
            var cart = getCartQuery.Execute(new GetFullCartByIdQuery
            {
                CartId = addItemToCartCommad.CartId
            }).Result;
            Assert.IsTrue(cart.Products.Count != 0);
            new DeleteCartCommandHandler(_dbContextFactory).Execute(new DeleteCartCommand { CartId = cart.Id }).Wait();
            UserService.DeleteTestUser(userId);
            ProductService.DeleteProduct(productId);
        }

        [Test]
        public void Add_Product_Item_To_Cart()
        {
            //arrange
            var userId = Guid.NewGuid();
            UserService.AddTestUser(userId);
            var productId = Guid.NewGuid();
            var collectionId = Guid.NewGuid();
            ProductService.AddProduct(productId, collectionId);

            var addItemToCartCommad = new AddCartItemCommand
            {
                AddElementId = productId,
                CartItemType = Core.Enums.EnumCartItemType.Product,
                CartId = Guid.NewGuid()
            };
            //create cart
            new CreateOrUpdateCartCommandHandler(_dbContextFactory).Execute(new CreateOrUpdateCommand
            {
                UserId = userId,
                NewCart = new DtoMiniCart
                {
                    Id = addItemToCartCommad.CartId,
                    Collections = new List<DtoCartMiniItemCollection>(),
                    Complects = new List<DtoCartMiniItemComplect>(),
                    Products = new List<DtoCartMiniItemProduct>()
                }
            }).Wait();

            var getCartQuery = new GetFullCartByIdQueryHandler(_dbContextFactory);
            var addCartItemCommandHandler = new AddCartItemCommandHandler(_dbContextFactory);
            //act
            addCartItemCommandHandler.Execute(addItemToCartCommad).Wait();
            //assert
            var cart = getCartQuery.Execute(new GetFullCartByIdQuery
            {
                CartId = addItemToCartCommad.CartId
            }).Result;
            Assert.IsTrue(cart.Products.Count != 0);
            new DeleteCartCommandHandler(_dbContextFactory).Execute(new DeleteCartCommand { CartId = cart.Id }).Wait();
            UserService.DeleteTestUser(userId);
            ProductService.DeleteProduct(productId);
        }

        [Test]
        public void Add_Collection_To_Same_Cart()
        {
            //up
            var userId = Guid.NewGuid();
            UserService.AddTestUser(userId);
            var collectionVariantId = Guid.NewGuid();
            ProductService.AddCollectionVariant(collectionVariantId);
            //arrange
            var collection = _dbContextFactory.CreateLazuritContext().Set<CollectionVariant>().Include("Products").First(p => p.Id == collectionVariantId);

            var addItemToCartCommand = new AddCartItemCommand
            {
                AddElementId = collection.Id,
                CartItemType = Core.Enums.EnumCartItemType.Collection,
                CartId = Guid.NewGuid()
            };
            //create new one
            new CreateOrUpdateCartCommandHandler(_dbContextFactory).Execute(new CreateOrUpdateCommand
            {
                UserId = userId,
                NewCart = new DtoMiniCart
                {
                    Id = addItemToCartCommand.CartId,
                    Collections = new List<DtoCartMiniItemCollection>(),
                    Complects = new List<DtoCartMiniItemComplect>(),
                    Products = new List<DtoCartMiniItemProduct>()
                }
            }).Wait();
            var addCartItemCommandHandler = new AddCartItemCommandHandler(_dbContextFactory);
            //act
            addCartItemCommandHandler.Execute(addItemToCartCommand).Wait();
            //assert
            Assert.ThrowsAsync<NotValidExeption>(async () => await addCartItemCommandHandler.Execute(addItemToCartCommand));
            var getCartQuery = new GetFullCartByIdQueryHandler(_dbContextFactory);
            var cart = getCartQuery.Execute(new GetFullCartByIdQuery
            {
                CartId = addItemToCartCommand.CartId
            }).Result;
            //down
            new DeleteCartCommandHandler(_dbContextFactory).Execute(new DeleteCartCommand { CartId = cart.Id }).Wait();
            UserService.DeleteTestUser(userId);
            ProductService.DeleteCollectionVariant(collectionVariantId);
        }

        [Test]
        public void Add_Collection_To_Empty_Cart()
        {
            //arrange
            var userId = Guid.NewGuid();
            UserService.AddTestUser(userId);
            var collectionVariantId = Guid.NewGuid();
            ProductService.AddCollectionVariant(collectionVariantId);

            var collection = _dbContextFactory.CreateLazuritContext().Set<CollectionVariant>().Include("Products").First(p => p.Id == collectionVariantId);
            var addItemToCartCommand = new AddCartItemCommand
            {
                AddElementId = collectionVariantId,
                CartItemType = Core.Enums.EnumCartItemType.Collection,
                CartId = Guid.NewGuid()
            };
            //delete old cart
            var getCartQuery = new GetFullCartByIdQueryHandler(_dbContextFactory);
            var cartOld = getCartQuery.Execute(new GetFullCartByIdQuery
            {
                CartId = addItemToCartCommand.CartId
            }).Result;
            if (cartOld != null)
            {
                new DeleteCartCommandHandler(_dbContextFactory).Execute(new DeleteCartCommand { CartId = cartOld.Id }).Wait();
            }
            //create new one
            new CreateOrUpdateCartCommandHandler(_dbContextFactory).Execute(new CreateOrUpdateCommand
            {
                UserId = userId,
                NewCart = new DtoMiniCart
                {
                    Id = addItemToCartCommand.CartId,
                    Collections = new List<DtoCartMiniItemCollection>(),
                    Complects = new List<DtoCartMiniItemComplect>(),
                    Products = new List<DtoCartMiniItemProduct>()
                }
            }).Wait();
            var addCartItemCommandHandler = new AddCartItemCommandHandler(_dbContextFactory);
            //act
            addCartItemCommandHandler.Execute(addItemToCartCommand).Wait();
            //assert
            var cartNew = getCartQuery.Execute(new GetFullCartByIdQuery
            {
                CartId = addItemToCartCommand.CartId
            }).Result;
            Assert.IsTrue(cartNew.Collections.First().Products.Count == collection.Products.Count);

            //down
            new DeleteCartCommandHandler(_dbContextFactory).Execute(new DeleteCartCommand { CartId = cartNew.Id }).Wait();
            UserService.DeleteTestUser(userId);
            ProductService.DeleteCollectionVariant(collectionVariantId);
        }
    }
}
