﻿using KonigDev.Lazurit.Hub.Grains.Carts.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Carts.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.IntegrationTests.Infrastructure;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Command;
using NUnit.Framework;
using System;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.Cart
{
    [TestFixture]
    public class DeleteCartItemFromCartHandlerTest
    {
        private IDBContextFactory _dbContextFactory;

        private GetFullCartByIdQueryHandler _getCartHandler;
        [SetUp]
        public void SetUp()
        {
            _dbContextFactory = new DBContextFactory("LazuritBaseContext");
            _getCartHandler = new GetFullCartByIdQueryHandler(_dbContextFactory);
        }

        [Test, Ignore("Remove cart item by id!")]
        public void Delete_Cart_Item_From_Cart()
        {

            //arrange
            var productId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var collectionId = Guid.NewGuid();
            ProductService.AddProduct(productId, collectionId);
            UserService.AddTestUser(userId);

            var addItemToCartCommad = new AddCartItemCommand
            {
                AddElementId = productId,
                CartItemType = Core.Enums.EnumCartItemType.Product,
                CartId=Guid.NewGuid()
            };
            new AddCartItemCommandHandler(_dbContextFactory).Execute(addItemToCartCommad).Wait();
            //act
            new RemoveCartItemCommandHandler(_dbContextFactory).Execute(new RemoveCartItemCommand
            {
                CartItemType = Core.Enums.EnumCartItemType.Product,
                RemoveProductId = addItemToCartCommad.AddElementId,
                CartId = addItemToCartCommad.CartId
            }).Wait();
            //assert
            var result = _getCartHandler.Execute(new Model.DTO.Cart.Query.GetFullCartByIdQuery
            {
                CartId = addItemToCartCommad.CartId
            }).Result;
            Assert.IsTrue(result.Products.Count == 0);
            new DeleteCartCommandHandler(_dbContextFactory).Execute(new DeleteCartCommand { CartId = result.Id }).Wait();
            ProductService.DeleteProduct(productId);
            UserService.DeleteTestUser(userId);
        }
    }
}
