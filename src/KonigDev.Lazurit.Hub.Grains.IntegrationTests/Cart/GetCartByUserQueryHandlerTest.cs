﻿using System;
using KonigDev.Lazurit.Hub.Grains.Carts.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Carts.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Command;
using NUnit.Framework;
using KonigDev.Lazurit.Hub.Grains.IntegrationTests.Infrastructure;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.Cart
{
    [TestFixture]
    public class GetCartByUserQueryHandlerTest
    {
        private IDBContextFactory _dbContextFactory;
        [SetUp]
        public void SetUp()
        {
            _dbContextFactory = new DBContextFactory("LazuritBaseContext");
        }

        [Test]
        public void Get_Items_From_Cart()
        {
            //arrange
            var productId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var collectionId = Guid.NewGuid();
            ProductService.AddProduct(productId, collectionId);
            UserService.AddTestUser(userId);

            var addItemToCartCommad = new AddCartItemCommand
            {
                AddElementId = productId,
                CartItemType = Core.Enums.EnumCartItemType.Product,
                CartId=Guid.NewGuid()
            };
            new AddCartItemCommandHandler(_dbContextFactory).Execute(addItemToCartCommad).Wait();
            //act
            var cart = new GetFullCartByIdQueryHandler(_dbContextFactory).Execute(new Model.DTO.Cart.Query.GetFullCartByIdQuery
            {
                CartId = addItemToCartCommad.CartId
            }).Result;
            //assert
            Assert.IsTrue(cart.Products.Count != 0);
            //down
            new DeleteCartCommandHandler(_dbContextFactory).Execute(new DeleteCartCommand { CartId = cart.Id }).Wait();
            ProductService.DeleteProduct(productId);
            UserService.DeleteTestUser(userId);
        }

        [Test]
        public void Get_Nonexistent_Items_From_Cart()
        {
            //arrange
            var productId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var collectionId = Guid.NewGuid();
            ProductService.AddProduct(productId, collectionId);
            UserService.AddTestUser(userId);

            var addItemToCartCommad = new AddCartItemCommand
            {
                AddElementId = productId,
                CartItemType = Core.Enums.EnumCartItemType.Product,
                CartId=  Guid.NewGuid()
            };
            new AddCartItemCommandHandler(_dbContextFactory).Execute(addItemToCartCommad).Wait();
            //act
            var cart = new GetFullCartByIdQueryHandler(_dbContextFactory).Execute(new Model.DTO.Cart.Query.GetFullCartByIdQuery
            {
                CartId =  Guid.NewGuid()
                
            }).Result;
            //assert
            Assert.IsNull(cart);
            var cartForRollback = new GetFullCartByIdQueryHandler(_dbContextFactory).Execute(new Model.DTO.Cart.Query.GetFullCartByIdQuery
            {
                CartId = addItemToCartCommad.CartId
            }).Result;
            new DeleteCartCommandHandler(_dbContextFactory).Execute(new DeleteCartCommand { CartId = cartForRollback.Id }).Wait();
            ProductService.DeleteProduct(productId);
            UserService.DeleteTestUser(userId);
        }
    }
}
