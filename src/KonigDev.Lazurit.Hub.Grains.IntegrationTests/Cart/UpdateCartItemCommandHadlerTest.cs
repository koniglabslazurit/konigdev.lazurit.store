﻿using KonigDev.Lazurit.Hub.Grains.Carts.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Carts.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.IntegrationTests.Infrastructure;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Command;
using NUnit.Framework;
using System;
using System.Linq;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.Cart
{
    [TestFixture]
    public class UpdateCartItemCommandHadlerTest
    {
        private IDBContextFactory _dbContextFactory;
        private GetFullCartByIdQueryHandler _getCartHandler;

        [SetUp]
        public void SetUp()
        {
            _dbContextFactory = new DBContextFactory("LazuritBaseContext");
            _getCartHandler = new GetFullCartByIdQueryHandler(_dbContextFactory);
        }

        [Test]
        public void Update_Quntity_In_Cart()
        {
            //arrange
            var productId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var collectionId = Guid.NewGuid();
            ProductService.AddProduct(productId, collectionId);
            UserService.AddTestUser(userId);

            var addItemToCartCommad = new AddCartItemCommand
            {
                AddElementId = productId,
                CartItemType = Core.Enums.EnumCartItemType.Product,
                CartId= Guid.NewGuid()
            };
            new AddCartItemCommandHandler(_dbContextFactory).Execute(addItemToCartCommad).Wait();
            //act
            new UpdateCartItemCommandHandler(_dbContextFactory).Execute(new UpdateCartItemCommand
            {
                CartId= addItemToCartCommad.CartId,
                Quantity = 2,
                CartItemType = Core.Enums.EnumCartItemType.Product,
                UpdateProductId = addItemToCartCommad.AddElementId
            }).Wait();
            //assert
            var result = _getCartHandler.Execute(new Model.DTO.Cart.Query.GetFullCartByIdQuery
            {
                CartId = addItemToCartCommad.CartId
            }).Result;
            Assert.IsTrue(result.Products.First().Quantity == 2);
            new DeleteCartCommandHandler(_dbContextFactory).Execute(new DeleteCartCommand { CartId = result.Id }).Wait();

            ProductService.DeleteProduct(productId);
            UserService.DeleteTestUser(userId);
        }
    }
}
