﻿using KonigDev.Lazurit.Core.Enums;
using KonigDev.Lazurit.Core.Enums.Enums;
using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.IntegrationGrains.CommandHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Model.Entities;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using NUnit.Framework;
using Ploeh.AutoFixture;
using System;
using System.Linq;
using System.Threading.Tasks;

/* todo неймспейс не переименовывать, на нем завязан сетап */
namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.IntegrationProductsModuleTests.UpdateIntegrationProductsCommandHandlerTest
{
    /// <summary>
    /// Класс для сетапа и тирдауна для неймспейса тестов на хэндлер UpdateIntegrationProductsCommandHandler
    /// </summary>
   // [SetUpFixture]
    public class SetUp
    {
        // [OneTimeSetUp]
        public static void AssemblyInitalize()
        {

        }

        //[OneTimeTearDown]
        public static void AssemblyTearDown()
        {

        }
    }

    [TestFixture]
    public class UpdateIntegrationProductsCommandHandlerTests
    {
        private IDBContextFactory _dbContextFactory;
        private ITransactionFactory _transactionFactory;


        [SetUp]
        public void SetUp()
        {
            _dbContextFactory = new DBContextFactory("LazuritBaseContext");
            _transactionFactory = new TransactionFactory();
        }

        [Test]
        public async Task UpdateIntegrationProductsCommandHandler_ProductExistFieldsCorrectUpdating()
        {
            //up
            var fixture = new Fixture();

            var integrationTestData = fixture.Build<IntegrationTestData>().Without(p => p.SyncCodeExist).Create();
            SetDataToIntegrationTables.Set(integrationTestData);
            //arrange

            var command = fixture.Build<UpdateIntegrationProductsCommand>().Without(p => p.Styles).Without(p => p.TargetTypes).Create();
            command.SyncCode1C = integrationTestData.SyncCode;
            command.IntegrationStatus = EnumIntegrationStatus.Validation.ToString();
            //act
            UpdateIntegrationProductsCommandHandler handler = new UpdateIntegrationProductsCommandHandler(_dbContextFactory, _transactionFactory);
            await handler.Execute(command);
            IntegrationProduct integrationProduct = null;
            using (var context = _dbContextFactory.CreateLazuritIntegrationContext())
            {
                integrationProduct = context.Set<IntegrationProduct>().FirstOrDefault(p => p.SyncCode1C == integrationTestData.SyncCode);
            }

            //assert
            Assert.That(integrationProduct != null);
            Assert.AreEqual(integrationProduct.SyncCode1C, command.SyncCode1C);
            /* цвета, вендор код, не обновляются, и не должны */
            Assert.AreNotEqual(integrationProduct.Facing, command.Facing);
            Assert.AreNotEqual(integrationProduct.FacingColor, command.FacingColor);
            Assert.AreNotEqual(integrationProduct.FrameColor, command.FrameColor);
            Assert.AreNotEqual(integrationProduct.VendorCode, command.VendorCode);
            /* Различные свойстви итп */
            Assert.AreEqual(integrationProduct.FurnitureForm, command.FurnitureForm);
            Assert.AreEqual(integrationProduct.Backlight, command.Backlight);
            Assert.AreEqual(integrationProduct.LeftRight, command.LeftRight);
            Assert.AreEqual(integrationProduct.Material, command.Material);
            Assert.AreEqual(integrationProduct.Mechanism, command.Mechanism);

            //down
            SetDataToIntegrationTables.Down(integrationTestData);
        }

        [Test]
        public async Task UpdateIntegrationProductsCommandHandler_ProductMissingCreateOriginalProduct()
        {
            //up
            var fixture = new Fixture();

            var integrationTestData = fixture.Build<IntegrationTestData>().Without(p=>p.SyncCodeExist).Create();
            SetDataToIntegrationTables.Set(integrationTestData);
            SetDataToIntegrationTables.SetOriginalTablesData(integrationTestData);

            //arrange
            UpdateIntegrationProductsCommand command = new UpdateIntegrationProductsCommand { SyncCode1C = integrationTestData.SyncCode, ProductPartMarker= EnumProductPartMarker.Missing.ToString(), IntegrationStatus = EnumIntegrationStatus.Published.ToString() };

            //act
            UpdateIntegrationProductsCommandHandler handler = new UpdateIntegrationProductsCommandHandler(_dbContextFactory, _transactionFactory);
            await handler.Execute(command);
            IntegrationProduct integrationProduct = null;
            using (var context = _dbContextFactory.CreateLazuritIntegrationContext())
            {
                integrationProduct = context.Set<IntegrationProduct>().FirstOrDefault(p => p.SyncCode1C == integrationTestData.SyncCode);
            }
            Product product = null;
            bool variantsExists = false;
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                product = context.Set<Product>().FirstOrDefault(p => p.SyncCode1C == integrationTestData.SyncCode);
                variantsExists = product.CollectionsVariants.Any();
            }

            //assert
            Assert.IsTrue(product != null);
            Assert.AreEqual(product.Descr, integrationProduct.Descr);
            Assert.AreEqual(product.Height, integrationProduct.Height);
            Assert.AreEqual(product.IsDisassemblyState, integrationProduct.IsDisassemblyState);
            Assert.IsFalse(variantsExists); /* при публикации не создается вариант по новой логике */
            Assert.AreEqual(product.Id, integrationProduct.ProductId);
            Assert.AreEqual(product.Id, integrationProduct.OriginalId);

            //down
            SetDataToIntegrationTables.Down(integrationTestData);
        }

        [Test]
        public async Task UpdateIntegrationProductsCommandHandler_ProductExistUpdateOriginalProduct()
        {
            //up
            var fixture = new Fixture();

            var integrationTestData = fixture.Create<IntegrationTestData>();
            integrationTestData.SyncCodeExist = integrationTestData.SyncCode;
            SetDataToIntegrationTables.Set(integrationTestData);
            SetDataToIntegrationTables.SetOriginalTablesData(integrationTestData);            

            //arrange
            UpdateIntegrationProductsCommand command = new UpdateIntegrationProductsCommand { SyncCode1C = integrationTestData.SyncCode, IntegrationStatus = EnumIntegrationStatus.Published.ToString() };

            //act
            UpdateIntegrationProductsCommandHandler handler = new UpdateIntegrationProductsCommandHandler(_dbContextFactory, _transactionFactory);
            await handler.Execute(command);
            IntegrationProduct integrationProduct = null;
            using (var context = _dbContextFactory.CreateLazuritIntegrationContext())
            {
                integrationProduct = context.Set<IntegrationProduct>().FirstOrDefault(p => p.SyncCode1C == integrationTestData.SyncCode);
            }
            Product product = null;
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                product = context.Set<Product>().FirstOrDefault(p => p.SyncCode1C == integrationTestData.SyncCode);
            }

            //assert
            Assert.IsTrue(product != null);
            Assert.AreEqual(product.Descr, integrationProduct.Descr);
            Assert.AreEqual(product.Height, integrationProduct.Height);
            Assert.AreEqual(product.IsDisassemblyState, integrationProduct.IsDisassemblyState);
            Assert.AreEqual(product.SyncCode1C, integrationProduct.SyncCode1C);
            Assert.AreEqual(product.Id, integrationProduct.ProductId);
            Assert.AreEqual(product.Id, integrationProduct.OriginalId);

            //down
            SetDataToIntegrationTables.Down(integrationTestData);
        }
    }

    internal class IntegrationTestData
    {
        public string SyncCode { get; set; }
        public string SyncCodeExist { get; set; }
        public string Room { get; set; }
        public string Series { get; set; }
        public Guid CollectionId { get; set; }
        public Guid VariantCollectionId { get; set; }
        public string FurnitureType { get; set; }
        public string VendorCode { get; set; }
        public string RoomType { get; set; }
        public string Style { get; set; }
        public string Collection { get; set; }
        public string FrameColor { get; set; }
        public string Facing { get; set; }
        public string FacingColor { get; set; }
        public Guid ProductId { get; set; }
    }

    internal static class SetDataToIntegrationTables
    {
        private const string connectionName = "LazuritBaseContext";

        public static void Set(IntegrationTestData data)
        {
            IDBContextFactory _dbContextFactory = new DBContextFactory(connectionName);

            IntegrationRoom intRoom = new IntegrationRoom { Id = Guid.NewGuid(), SyncCode1C = data.Room };
            IntegrationSeries intSeries = new IntegrationSeries { Id = Guid.NewGuid(), SyncCode1C = data.Series };
            IntegrationCollection intCollection = new IntegrationCollection { Id = data.CollectionId, IntegrationSeriesId = intSeries.Id, IntegrationRoomId = intRoom.Id };
            IntegrationCollectionVariant intVariant = new IntegrationCollectionVariant { Id = data.VariantCollectionId, IntegrationCollectionId = intCollection.Id, Facing = data.Facing, FrameColor = data.FrameColor, FacingColor = data.FacingColor };
            

            IntegrationProduct integrationProduct = new IntegrationProduct
            {
                SyncCode1C = data.SyncCode,
                Series = data.Series,
                Collection = data.Collection,
                FurnitureType = data.FurnitureType,
                VendorCode = data.VendorCode,
                RoomType = data.RoomType,
                FrameColor = data.FrameColor,
                Facing = data.Facing,
                FacingColor = data.FacingColor,
                ProductPartMarker = "нетнет",
                Style = data.Style,
                ProductId = data.ProductId,
                MarketerRange = "",
                MarketerVendorCode = data.VendorCode,
                CollectionId = intCollection.Id
            };

            IntegrationProductToStyle style = new IntegrationProductToStyle { Id = Guid.NewGuid(), IntegrationProductId = integrationProduct.SyncCode1C, Style = data.Style };
            intVariant.IntegrationProducts.Add(integrationProduct);

            if (!string.IsNullOrEmpty(data.SyncCodeExist))
            {
                integrationProduct.OriginalId = integrationProduct.ProductId;
            }
                    
            using (var context = _dbContextFactory.CreateLazuritIntegrationContext())
            {
                integrationProduct.IntegrationStatusId = context.Set<IntegrationStatus>().FirstOrDefault(p => p.TechName == EnumIntegrationStatus.Validation.ToString()).Id;
                context.Set<IntegrationProduct>().Add(integrationProduct);
                context.Set<IntegrationRoom>().Add(intRoom);
                context.Set<IntegrationSeries>().Add(intSeries);
                context.Set<IntegrationCollection>().Add(intCollection);
                context.Set<IntegrationCollectionVariant>().Add(intVariant);
                context.Set<IntegrationProductToStyle>().Add(style);

                // для проверки валидации моделей
                foreach (var i in context.GetValidationErrors())
                {
                    var t = i.ValidationErrors;
                }
                context.SaveChanges();
            }
        }

        public static void Down(IntegrationTestData data)
        {
            IDBContextFactory _dbContextFactory = new DBContextFactory(connectionName);

            using (var context = _dbContextFactory.CreateLazuritIntegrationContext())
            {
                IntegrationProduct integrationProduct = context.Set<IntegrationProduct>().FirstOrDefault(p => p.SyncCode1C == data.SyncCode);
                IntegrationRoom integrationRoom = context.Set<IntegrationRoom>().FirstOrDefault(p => p.SyncCode1C == data.Room);
                IntegrationSeries integrationSeries = context.Set<IntegrationSeries>().FirstOrDefault(p => p.SyncCode1C == data.Series);
                IntegrationCollection integrationCollection = context.Set<IntegrationCollection>().FirstOrDefault(p => p.Id == data.CollectionId);
                IntegrationCollectionVariant integrationCollectionVariant = context.Set<IntegrationCollectionVariant>().FirstOrDefault(p => p.Id == data.VariantCollectionId);

                context.Set<IntegrationProduct>().Remove(integrationProduct);
                context.Set<IntegrationRoom>().Remove(integrationRoom);
                context.Set<IntegrationSeries>().Remove(integrationSeries);
                context.Set<IntegrationCollection>().Remove(integrationCollection);
                context.Set<IntegrationCollectionVariant>().Remove(integrationCollectionVariant);
                context.SaveChanges();
            }
        }

        public static void SetOriginalTablesData(IntegrationTestData data)
        {
            IDBContextFactory _dbContextFactory = new DBContextFactory(connectionName);

            FacingColor facingColor = new FacingColor { Id = Guid.NewGuid(), Name = data.FacingColor, SyncCode1C = data.FacingColor };
            FrameColor frameColor = new FrameColor { Id = Guid.NewGuid(), Name = data.FrameColor, SyncCode1C = data.FrameColor };
            Facing faсing = new Facing { Id = Guid.NewGuid(), Name = data.Facing, SyncCode1C = data.Facing };
            FurnitureType furnitureType = new FurnitureType { Id = Guid.NewGuid(), Alias = Guid.NewGuid().ToString(), SyncCode1C = data.FurnitureType };
            Article article = new Article { Id = Guid.NewGuid(), SyncCode1C = data.VendorCode, FurnitureTypeId = furnitureType.Id };

            Series series = new Series { Id = Guid.NewGuid(), Alias = Guid.NewGuid().ToString(), Name = Guid.NewGuid().ToString(), SyncCode1C = Guid.NewGuid().ToString() };
            RoomType room = new RoomType { Id = Guid.NewGuid(), Alias = Guid.NewGuid().ToString(), Name = Guid.NewGuid().ToString(), SyncCode1C = Guid.NewGuid().ToString() };
            Collection collection = new Collection { RoomTypeId = room.Id, SeriesId = series.Id, Id = Guid.NewGuid() };

            Product product = new Product { Id = data.ProductId, ArticleId = article.Id, FacingColorId = facingColor.Id, FrameColorId = frameColor.Id, FacingId = faсing.Id, SyncCode1C = data.SyncCodeExist, CollectionId = collection.Id };

            using (var context = _dbContextFactory.CreateLazuritContext())
            {                
                context.Set<FacingColor>().Add(facingColor);
                context.Set<FrameColor>().Add(frameColor);
                context.Set<Facing>().Add(faсing);
                context.Set<FurnitureType>().Add(furnitureType);
                context.Set<Article>().Add(article);
                context.Set<Collection>().Add(collection);
                context.Set<Series>().Add(series);
                context.Set<RoomType>().Add(room);
                if (!string.IsNullOrEmpty(data.SyncCodeExist))
                {
                    product.ProductPartMarkerId = context.Set<ProductPartMarker>().Select(p => p.Id).FirstOrDefault();
                    context.Set<Product>().Add(product);
                }
                context.SaveChanges();
            }
        }
    }
}