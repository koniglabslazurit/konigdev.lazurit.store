﻿using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.PriceZones.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.PriceZones.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Queries;
using NUnit.Framework;
using System;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.Region
{
    [TestFixture]
    public class GetPriceZoneQueryHandlerTests
    {
        private IDBContextFactory _contextFactory;
        private Guid _priceZone1Id;
        private Guid _priceZone2Id;
        private Guid _priceZone3Id; 
        [OneTimeSetUp]
        public void SetUp()
        {
            _priceZone1Id = Guid.NewGuid();
            _priceZone2Id = Guid.NewGuid();
            _priceZone3Id = Guid.NewGuid();
            _contextFactory = new DBContextFactory("LazuritBaseContext");
            var priceZone1Create = new CreatePriceZoneCommand
            {
                Id = _priceZone1Id,
                Name = "Zone1",
                Number= 1,
                SyncCode1C = "0001"
            };
            var priceZone2Create = new CreatePriceZoneCommand
            {
                Id = _priceZone2Id,
                Name = "Zone2",
                Number = 2,
                SyncCode1C = "0002"
            };
            var priceZone3Create = new CreatePriceZoneCommand
            {
                Id = _priceZone3Id,
                Name = "Zone3",
                Number = 3,
                SyncCode1C = "0003"
            };
            var createHandler = new CreatePriceZoneCommandHandler(_contextFactory);
            createHandler.Execute(priceZone1Create).Wait();
            createHandler.Execute(priceZone2Create).Wait();
            createHandler.Execute(priceZone3Create).Wait();
        }
        [Test]
        public void GetPriceZone_ShouldReturnPriceZone1_WhenFoundIt()
        {
            //arrange
            var getZone = new GetPriceZoneQuery
            {
                Id = _priceZone1Id
            };
            var getQuery = new GetPriceZoneQueryHandler(_contextFactory);
            var removeCommand = new RemovePriceZoneCommandHandler(_contextFactory);
            //act
            var actual = getQuery.Execute(getZone);
            //assert
            Assert.AreEqual(_priceZone1Id, actual.Result.Id);
            Assert.AreEqual("Zone1", actual.Result.Name);
            Assert.AreEqual(1, actual.Result.Number);
            Assert.AreEqual("0001", actual.Result.SyncCode1C);
            removeCommand.Execute(new RemovePriceZoneCommand { Id = _priceZone1Id}).Wait();
            removeCommand.Execute(new RemovePriceZoneCommand { Id = _priceZone2Id }).Wait();
            removeCommand.Execute(new RemovePriceZoneCommand { Id = _priceZone3Id }).Wait();
        }

        [Test]
        public void GetPriceZone_ShouldReturnNull_WhenNotFoundIt()
        {
            //arrange
            var getPriceZone = new GetPriceZoneQuery
            {
                Id = Guid.NewGuid()
            };
            var getQuery = new GetPriceZoneQueryHandler(_contextFactory);
            //act
            //assert
            Assert.IsNull(getQuery.Execute(getPriceZone).Result);
        }

        [Test]
        public void GetPriceZone_ShouldReturnException_WhenModelIdEmpty()
        {
            //arrange
            var getPriceZone = new GetPriceZoneQuery
            {
                Id = Guid.Empty
            };
            var getQuery = new GetPriceZoneQueryHandler(_contextFactory);
            //act
            //assert
            Assert.Throws<NotValidCommandException>(() => getQuery.Execute(getPriceZone));
        }
    }
}

