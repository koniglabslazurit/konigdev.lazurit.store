﻿using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.PriceZones.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.PriceZones.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Queries;
using NUnit.Framework;
using System;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.PriceZone
{
    [TestFixture]
    public class CreatePriceZoneCommandHandlerTests
    {
        private IDBContextFactory _contextFactory;
        [SetUp]
        public void SetUp()
        {
            _contextFactory = new DBContextFactory("LazuritBaseContext");
        }
        [Test]
        public void CreatePriceZone_ShouldAddPriceZone_WhenValidModel()
        {
            //arrange
            var zone = new CreatePriceZoneCommand
            {
                Id = Guid.NewGuid(),
                Name = "Zone1",
                Number = 1,
                SyncCode1C = "98987"
            };
            var query = new GetPriceZoneQuery
            {
                Id = zone.Id
            };
            var createHandler = new CreatePriceZoneCommandHandler(_contextFactory);
            var getQuery = new GetPriceZoneQueryHandler(_contextFactory);
            var removeHandler = new RemovePriceZoneCommandHandler(_contextFactory);
            //act
            createHandler.Execute(zone).Wait();
            //assert
            Assert.AreEqual(zone.Id, getQuery.Execute(query).Result.Id);
            Assert.AreEqual("Zone1", getQuery.Execute(query).Result.Name);
            Assert.AreEqual(1, getQuery.Execute(query).Result.Number);
            Assert.AreEqual("98987", getQuery.Execute(query).Result.SyncCode1C);
            removeHandler.Execute(new RemovePriceZoneCommand { Id = zone.Id }).Wait();
        }

        [Test]
        public void CreatePriceZone_ShouldNotAddPriceZone_WhenModelGuidIsEmpty()
        {
            //arrange
            var zone = new CreatePriceZoneCommand
            {
                Id = Guid.Empty,
                Name = "Zone1",
                Number = 1,
                SyncCode1C = "0001"
            };
            var createHandler = new CreatePriceZoneCommandHandler(_contextFactory);
            //act
            //assert
            Assert.ThrowsAsync<NotValidCommandException>(async () => await createHandler.Execute(zone));
        }
        [Test]
        public void CreatePriceZone_ShouldNotAddPriceZone_WhenTitleIsEmpty()
        {
            //arrange
            var zone= new CreatePriceZoneCommand
            {
                Id = Guid.NewGuid(),
                Name= "",
                Number= 1,
                SyncCode1C = "0001"
            };
            var createHandler = new CreatePriceZoneCommandHandler(_contextFactory);
            //act
            //assert
            Assert.ThrowsAsync<NotValidCommandException>(async () => await createHandler.Execute(zone));
        }
    }
}
