﻿using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.PriceZones.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.PriceZones.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Queries;
using NUnit.Framework;
using System;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.PriceZone
{
    [TestFixture]
    public class RemovePriceZoneCommandHandlerTests
    {
        private IDBContextFactory _contextFactory;
        [SetUp]
        public void SetUp()
        {
            _contextFactory = new DBContextFactory("LazuritBaseContext");
        }
        [Test]
        public void RemovePriceZone_ShouldRemovePriceZone_WhenValidModel()
        {
            //arrange
            var zoneId = Guid.NewGuid();
            var zone = new CreatePriceZoneCommand
            {
                Id = zoneId,
                Name = "Zone1",
                Number = 1,
                SyncCode1C = "55555"
            };
            var zoneRemove = new RemovePriceZoneCommand
            {
                Id = zoneId
            };
            var query = new GetPriceZoneQuery
            {
                Id = zoneId
            };
            var createHandler = new CreatePriceZoneCommandHandler(_contextFactory);
            var getQuery = new GetPriceZoneQueryHandler(_contextFactory);
            var removeHandler = new RemovePriceZoneCommandHandler(_contextFactory);
            createHandler.Execute(zone).Wait();
            //act
            removeHandler.Execute(zoneRemove).Wait();
            //assert
            Assert.IsNull(getQuery.Execute(query).Result);
        }

        [Test]
        public void RemovePriceZone_ShouldNotRemovePriceZone_WhenModelGuidIsEmpty()
        {
            //arrange
            var zone = new RemovePriceZoneCommand
            {
                Id = Guid.Empty
            };
            var removeHandler = new RemovePriceZoneCommandHandler(_contextFactory);
            //act
            //assert
            Assert.ThrowsAsync<NotValidCommandException>(async () => await removeHandler.Execute(zone));
        }
        [Test]
        public void RemovePriceZone_ShouldNotRemovePriceZone_WhenNotFound()
        {
            //arrange
            var zone = new RemovePriceZoneCommand
            {
                Id = Guid.NewGuid()
            };
            var removeHandler = new RemovePriceZoneCommandHandler(_contextFactory);
            //act
            //assert
            Assert.ThrowsAsync<NotValidCommandException>(async () => await removeHandler.Execute(zone));
        }
    }
}
