﻿using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.PriceZones.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.PriceZones.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Queries;
using NUnit.Framework;
using System;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.PriceZone
{
    [TestFixture]
    public class UpdatePriceZoneCommandHandlerTests
    {
        private IDBContextFactory _contextFactory;
        [SetUp]
        public void SetUp()
        {
            _contextFactory = new DBContextFactory("LazuritBaseContext");
        }
        [Test]
        public void UpdatePriceZone_ShouldUpdatePriceZone_WhenValidModel()
        {
            //arrange
            var zoneId = Guid.NewGuid();
            var zone = new CreatePriceZoneCommand
            {
                Id = zoneId,
                Name = "Zone1",
                Number = 1,
                SyncCode1C = "0001"
            };
            var zoneUpdate = new UpdatePriceZoneCommand
            {
                Id = zoneId,
                Name = "Zone2",
                Number = 2,
                SyncCode1C = "0002"
            };
            var query = new GetPriceZoneQuery
            {
                Id = zoneId
            };
            var createHandler = new CreatePriceZoneCommandHandler(_contextFactory);
            var updateHandler = new UpdatePriceZoneCommandHandler(_contextFactory);
            var getQuery = new GetPriceZoneQueryHandler(_contextFactory);
            var removeHandler = new RemovePriceZoneCommandHandler(_contextFactory);
            createHandler.Execute(zone).Wait();
            //act
            updateHandler.Execute(zoneUpdate).Wait();
            //assert
            Assert.AreEqual(zone.Id, getQuery.Execute(query).Result.Id);
            Assert.AreEqual("Zone2", getQuery.Execute(query).Result.Name);
            Assert.AreEqual(2, getQuery.Execute(query).Result.Number);
            Assert.AreEqual("0002", getQuery.Execute(query).Result.SyncCode1C);
            removeHandler.Execute(new RemovePriceZoneCommand { Id = zoneId }).Wait();
        }

        [Test]
        public void UpdatePriceZone_ShouldNotUpdatePriceZone_WhenModelGuidIsEmpty()
        {
            //arrange
            var zone = new UpdatePriceZoneCommand
            {
                Id = Guid.Empty,
                Name = "Zone1",
                Number = 1,
                SyncCode1C = "0001"
            };
            var updateHandler = new UpdatePriceZoneCommandHandler(_contextFactory);
            //act
            //assert
            Assert.ThrowsAsync<NotValidCommandException>(async () => await updateHandler.Execute(zone));
        }
        [Test]
        public void UpdatePriceZone_ShouldNotUpdatePriceZone_WhenNotFound()
        {
            //arrange
            var zone = new UpdatePriceZoneCommand
            {
                Id = Guid.NewGuid(),
                Name = "",
                Number = 1,
                SyncCode1C = "0001"
            };
            var updateHandler = new UpdatePriceZoneCommandHandler(_contextFactory);
            //act
            //assert
            Assert.ThrowsAsync<NotValidCommandException>(async () => await updateHandler.Execute(zone));
        }
    }
}
