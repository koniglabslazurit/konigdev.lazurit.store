﻿using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.IntegrationTests.Infrastructure;
using KonigDev.Lazurit.Hub.Grains.Orders.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Orders.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Query;
using KonigDev.Lazurit.Model.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.OrderTests
{
    [TestFixture]
    public class OrderGrainTests
    {
        private IDBContextFactory _dbContextFactory;
        private ITransactionFactory _transactionFactory;

        [SetUp]
        public void SetUp()
        {
            _dbContextFactory = new DBContextFactory("LazuritBaseContext");
            _transactionFactory = new TransactionFactory();
        }

        [Test]
        public void CreateOrder_CorrectWriteToDb_CorrectRespone()
        {
            //arrange
            /* заполнение данных для пользовтеля и продукта */
            var userId = Guid.NewGuid();
            UserService.AddTestUser(userId);
            var productId = Guid.NewGuid();
            ProductService.AddProduct(productId);
            var collectionVariantId = Guid.NewGuid();
            ProductService.AddCollectionVariant(collectionVariantId);

            var context = _dbContextFactory.CreateLazuritContext();
            var productInCollectionVariantId = context.Set<Product>()
                .Where(p => p.CollectionsVariants.Any(x => x.Id == collectionVariantId)).Select(p => p.Id).FirstOrDefault();
            var paymentMhetodId = context.Set<PaymentMethod>().Select(p=>p.Id).FirstOrDefault();

            /* комманада на создание ордера */
            var orderId = Guid.NewGuid();
            var command = new CreateOrderCommand
            {
                Id = orderId,
                IsApprovedBySeller = true,
                Sum = 10M,
                CreationTime = DateTime.UtcNow,
                IsDeliveryRequired = true,
                UserProfileId = userId,
                PaymentMethodId = paymentMhetodId,
                Collections = new List<DtoOrderCollectionCreating>
                {
                    new DtoOrderCollectionCreating
                    {
                        CollectionVariantId = collectionVariantId, Id = Guid.NewGuid(),
                        Products = new List<DtoOrderProductCreating> {
                            new DtoOrderProductCreating
                            {
                                Id = Guid.NewGuid(), Discount = 10, IsAssemblyRequired = true, Price = 100M, ProductId = productInCollectionVariantId, Quantity= 1
                            }
                        }
                    }
                },
                Products = new List<DtoOrderProductCreating>
                {
                    new DtoOrderProductCreating
                    {
                        ProductId = productId, Quantity = 2, Discount = 20, Id = Guid.NewGuid(), IsAssemblyRequired = true, Price = 20M
                    }
                }
            };

            //act
            var createOrderCommandHandler = new CreateOrderCommandHandler(_dbContextFactory, _transactionFactory).Execute(command);
            createOrderCommandHandler.Wait();
            var result = new GetOrdersQueryHandler(_dbContextFactory).Execute(new GetOrdersQuery { UserProfileId = userId }).Result;
            var firstItem = result.Items.FirstOrDefault();
            var product = firstItem.Products.FirstOrDefault();
            //assert
            Assert.IsNotEmpty(result.Items);
            Assert.IsTrue(result.Items.Count() == 1);
            Assert.IsTrue(result.TotalCount == 1);
            Assert.IsNotEmpty(firstItem.Collections);
            Assert.IsNotEmpty(firstItem.Products);
            Assert.IsTrue(product.IsAssemblyRequired);
            Assert.AreEqual(product.Quantity, 2);
            Assert.AreEqual(product.Price, 20M);

            //down
            UserService.DeleteTestUser(userId);
            ProductService.DeleteCollectionVariant(collectionVariantId);
            ProductService.DeleteProduct(productId);
            context.Set<Order>().FirstOrDefault(p => p.Id == orderId);
        }
    }
}
