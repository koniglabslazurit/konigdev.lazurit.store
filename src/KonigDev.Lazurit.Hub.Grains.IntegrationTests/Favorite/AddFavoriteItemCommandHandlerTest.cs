﻿using KonigDev.Lazurit.Hub.Grains.Favorites.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Favorites.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Model.DTO.Favorite.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Favorite.Query;
using KonigDev.Lazurit.Model.Entities;
using NUnit.Framework;
using System.Linq;
using KonigDev.Lazurit.Core.Enums.Exeptions.Favorite;
using KonigDev.Lazurit.Core.Transactions;
using System;
using KonigDev.Lazurit.Hub.Grains.IntegrationTests.Infrastructure;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.Favorite
{
    [TestFixture]
    public class AddFavoriteItemCommandHandlerTest
    {
        private IDBContextFactory _dbContextFactory;
        private ITransactionFactory _transactionFactory;
        [SetUp]
        public void SetUp()
        {
            _dbContextFactory = new DBContextFactory("LazuritBaseContext");
            _transactionFactory = new TransactionFactory();
        }

        [Test]
        public void Add_Product_Item_To_Empty_Favorite()
        {
            //arrange
            var userId = Guid.NewGuid();
            UserService.AddTestUser(userId);
            var productId = Guid.NewGuid();
            ProductService.AddProduct(productId);

            var context = _dbContextFactory.CreateLazuritContext();
            var addItemToFavoriteCommad = new AddFavoriteItemCommand
            {
                AddElementId = productId,
                FavoriteItemType = Core.Enums.EnumFavoriteItemType.Product,
                UserId = userId
            };
            var getFavoriteQuery = new GetFavoriteByUserQueryHandler(_dbContextFactory);
            var addFavoriteItemCommandHandler = new AddFavoriteItemCommandHandler(_dbContextFactory, _transactionFactory);
            //act
            addFavoriteItemCommandHandler.Execute(addItemToFavoriteCommad).Wait();
            //assert
            var favorite = getFavoriteQuery.Execute(new GetFavoriteByUserQuery
            {
                UserId = addItemToFavoriteCommad.UserId
            }).Result;
            Assert.IsTrue(favorite.Products.Count() != 0);
            new RemoveFavoriteCommandHandler(_dbContextFactory).Execute(new RemoveFavoriteCommand { FavoriteId = favorite.Id }).Wait();
            UserService.DeleteTestUser(userId);
            ProductService.DeleteProduct(productId);
        }

        [Test]
        public void Add_Product_Item_To_Favorite()
        {
            //arrange
            var userId = Guid.NewGuid();
            UserService.AddTestUser(userId);
            var productId = Guid.NewGuid();
            ProductService.AddProduct(productId);

            var addItemToFavoriteCommad = new AddFavoriteItemCommand
            {
                AddElementId = productId,
                FavoriteItemType = Core.Enums.EnumFavoriteItemType.Product,
                UserId = userId
            };
            //create favorite
            new CreateFavoriteCommandHandler(_dbContextFactory).Execute(new CreateFavoriteCommand { UserId = addItemToFavoriteCommad.UserId }).Wait();
            var getFavoriteQuery = new GetFavoriteByUserQueryHandler(_dbContextFactory);
            var addFavoriteItemCommandHandler = new AddFavoriteItemCommandHandler(_dbContextFactory, _transactionFactory);
            //act
            addFavoriteItemCommandHandler.Execute(addItemToFavoriteCommad).Wait();
            //assert
            var favorite = getFavoriteQuery.Execute(new GetFavoriteByUserQuery
            {
                UserId = addItemToFavoriteCommad.UserId
            }).Result;
            Assert.IsTrue(favorite.Products.Count() != 0);
            new RemoveFavoriteCommandHandler(_dbContextFactory).Execute(new RemoveFavoriteCommand { FavoriteId = favorite.Id }).Wait();
            UserService.DeleteTestUser(userId);
            ProductService.DeleteProduct(productId);
        }

        [Test]
        public void Add_Collection_To_Same_Favorite()
        {
            //arrange
            var collectionVariantId = Guid.NewGuid();            
            ProductService.AddCollectionVariant(collectionVariantId);
            var userId = Guid.NewGuid();
            UserService.AddTestUser(userId);

            var addItemToFavoriteCommand = new AddFavoriteItemCommand
            {
                AddElementId = collectionVariantId,
                FavoriteItemType = Core.Enums.EnumFavoriteItemType.Collection,
                UserId = userId
            };
            //create new one
            new CreateFavoriteCommandHandler(_dbContextFactory).Execute(new CreateFavoriteCommand { UserId = addItemToFavoriteCommand.UserId }).Wait();
            var addFavoriteItemCommandHandler = new AddFavoriteItemCommandHandler(_dbContextFactory, _transactionFactory);
            //act
            addFavoriteItemCommandHandler.Execute(addItemToFavoriteCommand).Wait();
            //assert
            Assert.ThrowsAsync<NotValidExeption>(async () => await addFavoriteItemCommandHandler.Execute(addItemToFavoriteCommand));
            var getFavoriteQuery = new GetFavoriteByUserQueryHandler(_dbContextFactory);
            var favorite = getFavoriteQuery.Execute(new GetFavoriteByUserQuery
            {
                UserId = addItemToFavoriteCommand.UserId
            }).Result;
            new RemoveFavoriteCommandHandler(_dbContextFactory).Execute(new RemoveFavoriteCommand { FavoriteId = favorite.Id }).Wait();
            ProductService.DeleteCollectionVariant(collectionVariantId);
            UserService.DeleteTestUser(userId);
        }

        [Test]
        public void Add_Collection_To_Empty_Favorite()
        {
            //arrange
            var collectionVariantId = Guid.NewGuid();            
            ProductService.AddCollectionVariant(collectionVariantId);
            var userId = Guid.NewGuid();
            UserService.AddTestUser(userId);

            var collection = _dbContextFactory.CreateLazuritContext().Set<CollectionVariant>().Include("Products").FirstOrDefault(p => p.Id == collectionVariantId);

            var addItemToFavoriteCommand = new AddFavoriteItemCommand
            {
                AddElementId = collectionVariantId,
                FavoriteItemType = Core.Enums.EnumFavoriteItemType.Collection,
                UserId = userId
            };
            //delete old favorite
            var getFavoriteQuery = new GetFavoriteByUserQueryHandler(_dbContextFactory);
            var favoriteOld = getFavoriteQuery.Execute(new GetFavoriteByUserQuery
            {
                UserId = addItemToFavoriteCommand.UserId
            }).Result;
            if (favoriteOld != null)
            {
                new RemoveFavoriteCommandHandler(_dbContextFactory).Execute(new RemoveFavoriteCommand { FavoriteId = favoriteOld.Id }).Wait();
            }
            //create new one
            new CreateFavoriteCommandHandler(_dbContextFactory).Execute(new CreateFavoriteCommand { UserId = addItemToFavoriteCommand.UserId }).Wait();
            var addFavoriteItemCommandHandler = new AddFavoriteItemCommandHandler(_dbContextFactory, _transactionFactory);
            //act
            addFavoriteItemCommandHandler.Execute(addItemToFavoriteCommand).Wait();
            //assert
            var favoriteNew = getFavoriteQuery.Execute(new GetFavoriteByUserQuery
            {
                UserId = addItemToFavoriteCommand.UserId
            }).Result;
            Assert.IsTrue(favoriteNew.Collections.First().Products.Count() == collection.Products.Count);
            new RemoveFavoriteCommandHandler(_dbContextFactory).Execute(new RemoveFavoriteCommand { FavoriteId = favoriteNew.Id }).Wait();
            ProductService.DeleteCollectionVariant(collectionVariantId);
            UserService.DeleteTestUser(userId);
        }       

    }
}
