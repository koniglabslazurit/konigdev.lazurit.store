﻿using KonigDev.Lazurit.Hub.Grains.Favorites.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Favorites.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Model.DTO.Favorite.Command;
using NUnit.Framework;
using System;
using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.IntegrationTests.Infrastructure;
using System.Linq;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.Favorite
{
    [TestFixture]
    public class GetFavoriteByUserQueryHandlerTest
    {
        private IDBContextFactory _dbContextFactory;
        private ITransactionFactory _transactionFactory;
        [SetUp]
        public void SetUp()
        {
            _dbContextFactory = new DBContextFactory("LazuritBaseContext");
            _transactionFactory = new TransactionFactory();
        }

        [Test]
        public void Get_Items_From_Favorite()
        {
            //arrange
            var productId = Guid.NewGuid();
            var userId = Guid.NewGuid();           
            ProductService.AddProduct(productId);            
            UserService.AddTestUser(userId);

            var addItemToFavoriteCommad = new AddFavoriteItemCommand
            {
                AddElementId = productId,
                FavoriteItemType = Core.Enums.EnumFavoriteItemType.Product,
                UserId = userId
            };
            new AddFavoriteItemCommandHandler(_dbContextFactory, _transactionFactory).Execute(addItemToFavoriteCommad).Wait();
            //act
            var favorite = new GetFavoriteByUserQueryHandler(_dbContextFactory).Execute(new Model.DTO.Favorite.Query.GetFavoriteByUserQuery
            {
                UserId = addItemToFavoriteCommad.UserId
            }).Result;
            //assert
            Assert.IsTrue(favorite.Products.Count() != 0);
            new RemoveFavoriteCommandHandler(_dbContextFactory).Execute(new RemoveFavoriteCommand { FavoriteId = favorite.Id }).Wait();
            ProductService.DeleteProduct(productId);
            UserService.DeleteTestUser(userId);
        }

        [Test]
        public void Get_Nonexistent_Items_From_Favorite()
        {
            //arrange
            var productId = Guid.NewGuid();
            var userId = Guid.NewGuid();            
            ProductService.AddProduct(productId);            
            UserService.AddTestUser(userId);

            var addItemToFavoriteCommad = new AddFavoriteItemCommand
            {
                AddElementId = productId,
                FavoriteItemType = Core.Enums.EnumFavoriteItemType.Product,
                UserId = userId
            };
            new AddFavoriteItemCommandHandler(_dbContextFactory, _transactionFactory).Execute(addItemToFavoriteCommad).Wait();
            //act
            var favorite = new GetFavoriteByUserQueryHandler(_dbContextFactory).Execute(new Model.DTO.Favorite.Query.GetFavoriteByUserQuery
            {
                UserId = Guid.NewGuid()
            }).Result;
            //assert
            Assert.IsNull(favorite);
            var favoriteForRollback = new GetFavoriteByUserQueryHandler(_dbContextFactory).Execute(new Model.DTO.Favorite.Query.GetFavoriteByUserQuery
            {
                UserId = addItemToFavoriteCommad.UserId
            }).Result;
            new RemoveFavoriteCommandHandler(_dbContextFactory).Execute(new RemoveFavoriteCommand { FavoriteId = favoriteForRollback.Id }).Wait();
            ProductService.DeleteProduct(productId);
            UserService.DeleteTestUser(userId);
        }
    }
}
