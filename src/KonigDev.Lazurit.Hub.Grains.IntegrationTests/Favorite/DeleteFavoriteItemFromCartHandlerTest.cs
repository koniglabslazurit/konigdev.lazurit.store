﻿using KonigDev.Lazurit.Hub.Grains.Favorites.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Favorites.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Model.DTO.Favorite.Command;
using NUnit.Framework;
using System;
using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.IntegrationTests.Infrastructure;
using System.Linq;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.Favorite
{
    [TestFixture]
    public class DeleteFavoriteItemFromFavoriteHandlerTest
    {
        private IDBContextFactory _dbContextFactory;
        private ITransactionFactory _transactionFactory;
        private GetFavoriteByUserQueryHandler _getFavoriteHandler;

        [SetUp]
        public void SetUp()
        {
            _dbContextFactory = new DBContextFactory("LazuritBaseContext");
            _getFavoriteHandler = new GetFavoriteByUserQueryHandler(_dbContextFactory);
            _transactionFactory = new TransactionFactory();
        }

        [Test]
        public void Delete_Favorite_Item_From_Favorite()
        {
            //arrange
            var userId = Guid.NewGuid();
            UserService.AddTestUser(userId);
            var productId = Guid.NewGuid();
            ProductService.AddProduct(productId);

            var addItemToFavoriteCommad = new AddFavoriteItemCommand
            {
                AddElementId = productId,
                FavoriteItemType = Core.Enums.EnumFavoriteItemType.Product,
                UserId = userId
            };
            new AddFavoriteItemCommandHandler(_dbContextFactory, _transactionFactory).Execute(addItemToFavoriteCommad).Wait();
            //act
            new RemoveFavoriteItemCommandHandler(_dbContextFactory).Execute(new RemoveFavoriteItemCommand
            {
                FavoriteItemType = Core.Enums.EnumFavoriteItemType.Product,
                RemoveProductId = addItemToFavoriteCommad.AddElementId,
                UserId = addItemToFavoriteCommad.UserId
            }).Wait();
            //assert
            var result = _getFavoriteHandler.Execute(new Model.DTO.Favorite.Query.GetFavoriteByUserQuery
            {
                UserId = addItemToFavoriteCommad.UserId
            }).Result;
            Assert.IsTrue(result.Products.Count() == 0);
            new RemoveFavoriteCommandHandler(_dbContextFactory).Execute(new RemoveFavoriteCommand { FavoriteId = result.Id }).Wait();
            UserService.DeleteTestUser(userId);
            ProductService.DeleteProduct(productId);
        }
    }
}
