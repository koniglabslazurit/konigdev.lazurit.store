﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using NUnit.Framework;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.Intergations
{
    [TestFixture]
    public class GetIntegrationCollectionsProductQueryHandlerTests
    {
        private IDBContextFactory _contextFactory;
        private GetIntegrationCollectionsProductsQueryHandler _classForTest; 

        [SetUp]
        public void SetUp()
        {
            _contextFactory = new DBContextFactory("LazuritIntegrationContext");
            _classForTest = new GetIntegrationCollectionsProductsQueryHandler(_contextFactory);
        }

        //[Test]
        //public void GetIntegrationCollectionsProductQueryHandler_ShouldReturnCollectionsWithProducts_WhenTheyExist()
        //{
        //    //arrange
        //    //act
        //    var actual = _classForTest.Execute(new GetIntegrationCollectionsProductsQuery());
        //    //assert
        //    Assert.IsInstanceOf<Task<List<DtoIntegrationCollectionsProductsItem>>>(actual);
        //    Assert.AreEqual(1, actual.Result.Count);
        //    Assert.AreEqual(2, actual.Result.First().IntegrationProducts.Count);
        //}

        //[Test]
        //public void GetIntegrationCollectionsProductQueryHandler1_ShouldReturnCollectionsWithProducts_WhenTheyExist()
        //{
        //    //arrange

        //    //act
        //    var actual = _classForTest.Execute(new GetIntegrationCollectionsProductsQuery());
        //    //assert
        //    Assert.IsInstanceOf<Task<List<DtoIntegrationCollectionsProductsItem>>>(actual);
        //    Assert.AreEqual(1, actual.Result.Count);
        //    Assert.AreEqual(0, actual.Result.FirstOrDefault()?.IntegrationProducts.Count);
        //}
        //[Test]
        //public void GetIntegrationCollectionsProductQueryHandler1_ShouldReturn1CollectionWithProducts_WhenQuery2()
        //{
        //    //arrange

        //    //act
        //    var actual = _classForTest.Execute(new GetIntegrationCollectionsProductsQuery {Page =2, ItemsOnPage = 1});
        //    //assert
        //    Assert.IsInstanceOf<Task<List<DtoIntegrationCollectionsProductsItem>>>(actual);
        //    Assert.AreEqual(1, actual.Result.Count);
        //}
        
    }
}
