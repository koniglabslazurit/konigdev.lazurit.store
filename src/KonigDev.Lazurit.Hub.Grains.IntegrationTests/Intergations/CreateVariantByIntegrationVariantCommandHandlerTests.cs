﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.IntegrationGrains.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using NUnit.Framework;
using KonigDev.Lazurit.Hub.Grains.IntegrationGrains.CommandHandlers.IntegrationProductsContentCommandHandlers;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.Intergations
{
    [TestFixture]
    public class CreateVariantByIntegrationVariantCommandHandlerTests
    {
        private IDBContextFactory _contextFactory;
        private ITransactionFactory _transactionFactory;
        private CreateVariantByIntegrationVariantCommandHandler _classForTest;

        [SetUp]
        public void SetUp()
        {
            _contextFactory = new DBContextFactory("LazuritIntegrationContext");
            _transactionFactory = new TransactionFactory();
            _classForTest = new CreateVariantByIntegrationVariantCommandHandler(_contextFactory, _transactionFactory);
        }

        //[Test]
        //public void CreateVariantByIntegrationVariantCommandHandler_ShouldCreateAll_WhenValdi()
        //{
        //    //arrange
        //    //act
        //    var actual = _classForTest.Execute(new CreateVariantByIntegrationVariantCommand
        //    {
        //        Id = Guid.Parse("7b5d8b80-2036-4dd6-9b60-0b43f1b739cd")
        //    });
        //    //assert
        //    Assert.IsTrue(true);
        // }

        //[Test]
        //public void Update_ShouldCreateAll_WhenValdi()
        //{
        //    //arrange
        //    var update = new UpdateIntegrationProductsCommandHandler(_contextFactory, _transactionFactory);
        //    //act
        //    var actual = update.Execute(new UpdateIntegrationProductsCommand
        //    {
        //        SyncCode1C = "5302.кп.95у",
        //        IntegrationStatus = "Published",
        //        FurnitureType= "Кровать",
        //        SyncCode1CConformity = null,
        //        IsDisassemblyState = false,
        //        Backlight= "Подсветка кровати",
        //        Descr="",
        //        FurnitureForm= "Кровать без решетки",
        //        Height = 1000,
        //        LeftRight = "Унив.",
        //        Length = 1000,
        //        Material = "ЛДСП",
        //        Mechanism = "",
        //        PermissibleLoad = "0",
        //        SleepingAreaLength = "1600",
        //        SleepingAreaWidth = "2000",
        //        Warranty = "36",
        //        Width = 1000,
        //        RoomSize = "20",
        //        SleepingAreaAmount= 1,
        //        ProductPartMarker ="Нет нет"
        //    });
        //    //assert
        //    Assert.IsTrue(true);
        //}

        //[Test]
        //public void UpdateContext_ShouldCreateAll_WhenValdi()
        //{
        //    //arrange
        //    var update = new UpdateIntegrationProductContentCommandHandler(_contextFactory, _transactionFactory);
        //    //act
        //    var actual = update.Execute(new UpdateIntegrationProductContentCommand
        //    {
        //        Id = Guid.Parse("B781112E-9DCB-4E37-B147-135F7E7CC13B"),
        //        Width = 20,
        //        Height = 20,
        //        Left = 20,
        //        Top = 20
        //    });
        //    //assert
        //    Assert.IsTrue(true);
        //}
    }
}
