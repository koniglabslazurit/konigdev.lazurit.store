﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Cities.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Cities.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Queries;
using NUnit.Framework;
using System;
using System.Linq;
using KonigDev.Lazurit.Hub.Grains.Regions.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.PriceZones.CommandHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Commands;
using KonigDev.Lazurit.Core.Enums.Exeptions.Common;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.City
{
    [TestFixture]
    public class RemoveCityCommandHandlerTests
    {
        private IDBContextFactory _contextFactory;
        [SetUp]
        public void SetUp()
        {
            _contextFactory = new DBContextFactory("LazuritBaseContext");
        }
        [Test]
        public void RemoveCity_ShouldRemoveCity_WhenValidModel()
        {
            //arrange
            var cityId = Guid.NewGuid();
            var context = _contextFactory.CreateLazuritContext();
            var createRegionHandler = new CreateRegionCommandHandler(_contextFactory);
            var createPriceZoneHandler = new CreatePriceZoneCommandHandler(_contextFactory);
            var removeRegionHandler = new RemoveRegionCommandHandler(_contextFactory);
            var removePriceZoneHandler = new RemovePriceZoneCommandHandler(_contextFactory);
            var region = new CreateRegionCommand
            {
                Id = Guid.NewGuid(),
                Title = "KaliningradskayaRO",
                LazuritPasswordHash = "123456789",
                SyncCode1C = "789745"
            };
            var zone = new CreatePriceZoneCommand
            {
                Id = Guid.NewGuid(),
                Name = "Zone1",
                Number = 1,
                SyncCode1C = "98987"
            };
            createPriceZoneHandler.Execute(zone).Wait();
            createRegionHandler.Execute(region).Wait();

            var city = new CreateCityCommand
            {
                Id = cityId,
                Title = "Kaliningrad",
                CodeKladr = "123456789",
                SyncCode1C = "789745",
                Latitude = 123,
                Longitude = 123,
                PriceZoneId = context.Set<Lazurit.Model.Entities.PriceZone>().First().Id,
                RegionId = context.Set<Lazurit.Model.Entities.Region>().First().Id,
            };
            var cityRemove = new RemoveCityCommand
            {
                Id = cityId
            };
            var query = new GetCityQuery
            {
                Id = cityId
            };
            var createHandler = new CreateCityCommandHandler(_contextFactory);
            var getQuery = new GetCityQueryHandler(_contextFactory);
            var removeHandler = new RemoveCityCommandHandler(_contextFactory);
            createHandler.Execute(city).Wait();
            //act
            removeHandler.Execute(cityRemove).Wait();
            //assert
            Assert.IsNull(getQuery.Execute(query).Result);
            removePriceZoneHandler.Execute(new RemovePriceZoneCommand { Id = zone.Id }).Wait();
            removeRegionHandler.Execute(new RemoveRegionCommand { Id = region.Id }).Wait();
        }

        [Test]
        public void RemoveCity_ShouldNotCityPriceZone_WhenModelGuidIsEmpty()
        {
            //arrange
            var city = new RemoveCityCommand
            {
                Id = Guid.Empty
            };
            var removeHandler = new RemoveCityCommandHandler(_contextFactory);
            //act
            //assert
            Assert.ThrowsAsync<NotValidCommandException>(async () => await removeHandler.Execute(city));
        }
        [Test]
        public void RemoveCity_ShouldNotRemoveCity_WhenNotFound()
        {
            //arrange
            var city = new RemoveCityCommand
            {
                Id = Guid.NewGuid()
            };
            var removeHandler = new RemoveCityCommandHandler(_contextFactory);
            //act
            //assert
            Assert.ThrowsAsync<NotValidCommandException>(async () => await removeHandler.Execute(city));
        }
    }
}