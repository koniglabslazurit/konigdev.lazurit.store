﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Cities.CommandHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Queries;
using KonigDev.Lazurit.Model.Entities;
using NUnit.Framework;
using System;
using System.Linq;
using KonigDev.Lazurit.Hub.Grains.Cities.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Regions.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.PriceZones.CommandHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Commands;
using KonigDev.Lazurit.Core.Enums.Exeptions.Common;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.Region
{
    [TestFixture]
    public class CreateCityCommandHandlerTests
    {
        private IDBContextFactory _contextFactory;
        [SetUp]
        public void SetUp()
        {
            _contextFactory = new DBContextFactory("LazuritBaseContext");
            
        }
        [Test]
        public void CreateCity_ShouldAddCity_WhenValidModel()
        {
            //arrange
            var context = _contextFactory.CreateLazuritContext();
            var createRegionHandler = new CreateRegionCommandHandler(_contextFactory);
            var createPriceZoneHandler = new CreatePriceZoneCommandHandler(_contextFactory);
            var removeRegionHandler = new RemoveRegionCommandHandler(_contextFactory);
            var removePriceZoneHandler = new RemovePriceZoneCommandHandler(_contextFactory);
            var region = new CreateRegionCommand
            {
                Id = Guid.NewGuid(),
                Title = "KaliningradskayaRO",
                LazuritPasswordHash = "123456789",
                SyncCode1C = "789745"
            };
            var zone = new CreatePriceZoneCommand
            {
                Id = Guid.NewGuid(),
                Name = "Zone1",
                Number = 1,
                SyncCode1C = "98987"
            };
            createPriceZoneHandler.Execute(zone).Wait();
            createRegionHandler.Execute(region).Wait();

            var city = new CreateCityCommand
            {
                Id = Guid.NewGuid(),
                Title = "Kaliningrad",
                CodeKladr = "123456789",
                SyncCode1C = "789745",
                Latitude = 123,
                Longitude = 123,
                PriceZoneId = context.Set<Lazurit.Model.Entities.PriceZone>().First().Id,
                RegionId = context.Set<Lazurit.Model.Entities.Region>().First().Id,
            };
            var query = new GetCityQuery
            {
                Id = city.Id
            };
            var createHandler = new CreateCityCommandHandler(_contextFactory);            
            var getQuery = new GetCityQueryHandler(_contextFactory);
            var removeHandler = new RemoveCityCommandHandler(_contextFactory);
            //act
            createHandler.Execute(city).Wait();
            //assert
            Assert.AreEqual(city.Id, getQuery.Execute(query).Result.Id);
            Assert.AreEqual("Kaliningrad", getQuery.Execute(query).Result.Title);
            Assert.AreEqual("123456789", getQuery.Execute(query).Result.CodeKladr);
            Assert.AreEqual("789745", getQuery.Execute(query).Result.SyncCode1C);
            Assert.AreEqual(123, getQuery.Execute(query).Result.Latitude);
            Assert.AreEqual(123, getQuery.Execute(query).Result.Longitude);
            removeHandler.Execute(new RemoveCityCommand { Id = city.Id }).Wait();
            removePriceZoneHandler.Execute(new RemovePriceZoneCommand { Id = zone.Id}).Wait();
            removeRegionHandler.Execute(new RemoveRegionCommand { Id = region.Id }).Wait();
        }

        [Test]
        public void CreateCity_ShouldNotAddCity_WhenModelGuidIsEmpty()
        {
            //arrange
            var city = new CreateCityCommand
            {
                Id = Guid.Empty
            };
            var createHandler = new CreateCityCommandHandler(_contextFactory);
            //act
            //assert
            Assert.ThrowsAsync<NotValidCommandException>(async () => await createHandler.Execute(city));
        }

        [Test]
        public void CreateCity_ShouldNotAddCity_WhenTitleIsEmpty()
        {
            //arrange
            var city = new CreateCityCommand
            {
                Id = Guid.NewGuid(),
                Title = "",
            };
            var createHandler = new CreateCityCommandHandler(_contextFactory);
            //act
            //assert
            Assert.ThrowsAsync<NotValidCommandException>(async () => await createHandler.Execute(city));
        }
    }
}
