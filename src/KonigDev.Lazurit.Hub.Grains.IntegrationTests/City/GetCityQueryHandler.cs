﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Cities.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Cities.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Queries;
using NUnit.Framework;
using System;
using System.Linq;
using KonigDev.Lazurit.Hub.Grains.Regions.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.PriceZones.CommandHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Commands;
using KonigDev.Lazurit.Core.Enums.Exeptions.Common;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.City
{
    [TestFixture]
    public class GetCityQueryHandlerTests
    {
        private IDBContextFactory _contextFactory;
        private Guid _city1Id;
        private Guid _regino1Id;
        private Guid _priceZone1Id;
        [OneTimeSetUp]
        public void SetUp()
        {
            _city1Id = Guid.NewGuid();
            _regino1Id = Guid.NewGuid();
            _priceZone1Id = Guid.NewGuid();
            _contextFactory = new DBContextFactory("LazuritBaseContext");            
        }

        [Test]
        public void GetCity_ShouldReturnCity1_WhenFoundIt()
        {
            //arrange
            var context = _contextFactory.CreateLazuritContext();

            var region = new CreateRegionCommand
            {
                Id = _regino1Id,
                Title = "KaliningradskayaRO",
                LazuritPasswordHash = "123456789",
                SyncCode1C = "0001"
            };
            var zone = new CreatePriceZoneCommand
            {
                Id = _priceZone1Id,
                Name = "Zone1",
                Number = 1,
                SyncCode1C = "0001"
            };

            var createRegionHandler = new CreateRegionCommandHandler(_contextFactory);
            var createPriceZoneHandler = new CreatePriceZoneCommandHandler(_contextFactory);
            createPriceZoneHandler.Execute(zone).Wait();
            createRegionHandler.Execute(region).Wait();

            var city1Create = new CreateCityCommand
            {
                Id = _city1Id,
                Title = "Kaliningrad",
                CodeKladr = "123456789",
                SyncCode1C = "0001",
                Latitude = 54,
                Longitude = 20,
                PriceZoneId = context.Set<Lazurit.Model.Entities.PriceZone>().Where(x => x.SyncCode1C == "0001").Select(x => x.Id).FirstOrDefault(),
                RegionId = context.Set<Lazurit.Model.Entities.Region>().Where(x => x.SyncCode1C == "0001").Select(x => x.Id).FirstOrDefault(),
                DeliveryPrice = 1000
            };
            var city2Create = new CreateCityCommand
            {
                Id = Guid.NewGuid(),
                Title = "Москва",
                CodeKladr = "123456789",
                SyncCode1C = "0001",
                Latitude = 55,
                Longitude = 37,
                PriceZoneId = context.Set<Lazurit.Model.Entities.PriceZone>().Where(x => x.SyncCode1C == "0001").Select(x => x.Id).FirstOrDefault(),
                RegionId = context.Set<Lazurit.Model.Entities.Region>().Where(x => x.SyncCode1C == "0001").Select(x => x.Id).FirstOrDefault(),
                DeliveryPrice = 3000
            };
            var createHandler = new CreateCityCommandHandler(_contextFactory);
            createHandler.Execute(city1Create).Wait();
            createHandler.Execute(city2Create).Wait();

            var getCity = new GetCityQuery
            {
                Id = _city1Id
            };
            var getQuery = new GetCityQueryHandler(_contextFactory);
            //act
            var actual = getQuery.Execute(getCity);
            //assert
            Assert.AreEqual(_city1Id, actual.Result.Id);
            Assert.AreEqual("Kaliningrad", actual.Result.Title);
            Assert.AreEqual("123456789", actual.Result.CodeKladr);
            Assert.AreEqual("0001", actual.Result.SyncCode1C);
            Assert.AreEqual(54, actual.Result.Latitude);
            Assert.AreEqual(20, actual.Result.Longitude);
            
            var removeCityHandler = new RemoveCityCommandHandler(_contextFactory);
            removeCityHandler.Execute(new RemoveCityCommand { Id = _city1Id }).Wait();
            removeCityHandler.Execute(new RemoveCityCommand { Id = city2Create.Id }).Wait();
            var removeRegionHandler = new RemoveRegionCommandHandler(_contextFactory);
            removeRegionHandler.Execute(new RemoveRegionCommand { Id = _regino1Id}).Wait();
            var removeZoneHandler = new RemovePriceZoneCommandHandler(_contextFactory);
            removeZoneHandler.Execute(new RemovePriceZoneCommand { Id = _priceZone1Id}).Wait();
        }

        [Test]
        public void GetCity_ShouldReturnNull_WhenNotFoundIt()
        {
            //arrange
            var getCity = new GetCityQuery
            {
                Id = Guid.NewGuid()
            };
            var getQuery = new GetCityQueryHandler(_contextFactory);
            //act
            //assert
            Assert.IsNull(getQuery.Execute(getCity).Result);
        }

        [Test]
        public void GetCity_ShouldReturnException_WhenModelIdEmpty()
        {
            //arrange
            var getCity = new GetCityQuery
            {
                Id = Guid.Empty
            };
            var getQuery = new GetCityQueryHandler(_contextFactory);
            //act
            //assert
            Assert.Throws<NotValidCommandException>(() => getQuery.Execute(getCity));
        }
    }
}

