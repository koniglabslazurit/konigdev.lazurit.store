﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Cities.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Cities.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Queries;
using NUnit.Framework;
using System;
using System.Linq;
using KonigDev.Lazurit.Hub.Grains.Regions.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.PriceZones.CommandHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Commands;
using KonigDev.Lazurit.Core.Enums.Exeptions.Common;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.City
{
    [TestFixture]
    public class UpdateCityCommandHandlerTests
    {
        private IDBContextFactory _contextFactory;
        [SetUp]
        public void SetUp()
        {
            _contextFactory = new DBContextFactory("LazuritBaseContext");
        }
        [Test]
        public void UpdateCity_ShouldUpdateCity_WhenValidModel()
        {
            //arrange
            var cityId = Guid.NewGuid();
            var context = _contextFactory.CreateLazuritContext();
            var createRegionHandler = new CreateRegionCommandHandler(_contextFactory);
            var createPriceZoneHandler = new CreatePriceZoneCommandHandler(_contextFactory);
            var removeRegionHandler = new RemoveRegionCommandHandler(_contextFactory);
            var removePriceZoneHandler = new RemovePriceZoneCommandHandler(_contextFactory);
            var region1 = new CreateRegionCommand
            {
                Id = Guid.NewGuid(),
                Title = "KaliningradskayaRO",
                LazuritPasswordHash = "123456789",
                SyncCode1C = "0001"
            };
            var region2 = new CreateRegionCommand
            {
                Id = Guid.NewGuid(),
                Title = "LeningradskayaRO",
                LazuritPasswordHash = "123456789",
                SyncCode1C = "0002"
            };
            var zone1 = new CreatePriceZoneCommand
            {
                Id = Guid.NewGuid(),
                Name = "Zone1",
                Number = 1,
                SyncCode1C = "0001"
            };
            var zone2 = new CreatePriceZoneCommand
            {
                Id = Guid.NewGuid(),
                Name = "Zone2",
                Number = 2,
                SyncCode1C = "0002"
            };
            createPriceZoneHandler.Execute(zone1).Wait();
            createPriceZoneHandler.Execute(zone2).Wait();
            createRegionHandler.Execute(region1).Wait();
            createRegionHandler.Execute(region2).Wait();

            var city = new CreateCityCommand
            {
                Id = cityId,
                Title = "Kaliningrad",
                CodeKladr = "123456789",
                SyncCode1C = "789745",
                Latitude = 123,
                Longitude = 123,
                PriceZoneId = context.Set<Lazurit.Model.Entities.PriceZone>().Where(x => x.SyncCode1C == "0001").Select(x=>x.Id).FirstOrDefault(),
                RegionId = context.Set<Lazurit.Model.Entities.Region>().Where(x => x.SyncCode1C == "0001").Select(x => x.Id).FirstOrDefault(),
            };
            var cityUpdate = new UpdateCityCommand
            {
                Id = cityId,
                Title = "Leniningrad",
                CodeKladr = "0000000",
                SyncCode1C = "0000007",
                Latitude = 555,
                Longitude = 555,
                PriceZoneId = context.Set<Lazurit.Model.Entities.PriceZone>().Where(x => x.SyncCode1C == "0002").Select(x => x.Id).FirstOrDefault(),
                RegionId = context.Set<Lazurit.Model.Entities.Region>().Where(x => x.SyncCode1C == "0002").Select(x => x.Id).FirstOrDefault(),
            };
            var query = new GetCityQuery
            {
                Id = cityId
            };
            var createHandler = new CreateCityCommandHandler(_contextFactory);
            var updateHandler = new UpdateCityCommandHandler(_contextFactory);
            var getQuery = new GetCityQueryHandler(_contextFactory);
            var removeHandler = new RemoveCityCommandHandler(_contextFactory);
            createHandler.Execute(city).Wait();
            //act
            updateHandler.Execute(cityUpdate).Wait();
            //assert
            Assert.AreEqual(city.Id, getQuery.Execute(query).Result.Id);
            Assert.AreEqual("Leniningrad", getQuery.Execute(query).Result.Title);
            Assert.AreEqual("0000000", getQuery.Execute(query).Result.CodeKladr);
            Assert.AreEqual("0000007", getQuery.Execute(query).Result.SyncCode1C);
            Assert.AreEqual(555, getQuery.Execute(query).Result.Latitude);
            Assert.AreEqual(555, getQuery.Execute(query).Result.Longitude);
            Assert.AreEqual(context.Set<Lazurit.Model.Entities.PriceZone>().Where(x => x.SyncCode1C == "0002").Select(x => x.Id).FirstOrDefault(), getQuery.Execute(query).Result.PriceZoneId);
            Assert.AreEqual(context.Set<Lazurit.Model.Entities.Region>().Where(x => x.SyncCode1C == "0002").Select(x => x.Id).FirstOrDefault(), getQuery.Execute(query).Result.RegionId);
            removeHandler.Execute(new RemoveCityCommand { Id = cityId }).Wait();
            removePriceZoneHandler.Execute(new RemovePriceZoneCommand { Id = zone1.Id }).Wait();
            removePriceZoneHandler.Execute(new RemovePriceZoneCommand { Id = zone2.Id }).Wait();
            removeRegionHandler.Execute(new RemoveRegionCommand { Id = region1.Id }).Wait();
            removeRegionHandler.Execute(new RemoveRegionCommand { Id = region2.Id }).Wait();
        }

        [Test]
        public void UpdateCity_ShouldNotUpdateCity_WhenModelGuidIsEmpty()
        {
            //arrange
            var city = new UpdateCityCommand
            {
                Id = Guid.Empty
            };
            var updateHandler = new UpdateCityCommandHandler(_contextFactory);
            //act
            //assert
            Assert.ThrowsAsync<NotValidCommandException>(async () => await updateHandler.Execute(city));
        }
        [Test]
        public void UpdateCity_ShouldNotUpdateCity_WhenNotFound()
        {
            //arrange
            var city = new UpdateCityCommand
            {
                Id = Guid.NewGuid(),
                Title = ""
            };
            var updateHandler = new UpdateCityCommandHandler(_contextFactory);
            //act
            //assert
            Assert.ThrowsAsync<NotValidCommandException>(async () => await updateHandler.Execute(city));
        }
    }
}
