﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Models.Constants.Account
{
    public static class Exception_Validations
    {
        public const string RequiredField = "Поле не должно быть пустым";
        public const string PasswordDoesnotCompare = "Пароли не совпадают";
    }
}