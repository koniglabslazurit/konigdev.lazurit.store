﻿namespace KonigDev.Lazurit.Store.Models.Constants.Account
{
    public static class Exception_User
    {
        public const string UserNotFound = "Текущий пользователь не определён.";
    }
}