﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Models.Constants.Account
{
    public static class AuthData
    {
        public const string authorizationCookieName = "authToken";
    }
}