﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Models.Constants
{
    public static class Common
    {
        /// <summary>
        /// Уникальный идентификатор корзины, который хранится на клиенте.
        /// </summary>
        public const string CartCookieId = "cartId";
        /// <summary>
        /// Идентификатор отложенных, который хранится на клиенте.
        /// </summary>
        public const string LayawayCookieId = "layawayId";
        public const string RubSymbol = "руб";
    }
}