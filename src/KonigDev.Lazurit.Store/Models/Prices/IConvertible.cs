﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Models.Prices
{
    public interface IConvertible<T>
    {
        T ConvertTo(Currency currency);
    }
}