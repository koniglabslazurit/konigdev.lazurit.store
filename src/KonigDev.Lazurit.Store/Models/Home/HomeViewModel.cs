﻿using KonigDev.Lazurit.Hub.Model.DTO.MarketingActions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Models.Home
{
    public class HomeViewModel
    {
        public DtoMarketingActions Slides { get; set; }
    }
}