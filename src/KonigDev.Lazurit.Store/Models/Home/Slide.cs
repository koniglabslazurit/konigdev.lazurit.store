﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Models.Home
{
    public class SlideViewModel
    {
        public Guid Id { get; set; }
        public string SlideUrl { get; set; }
        public string Discount { get; set; }
        public string DiscountDescription { get; set; }
    }
}