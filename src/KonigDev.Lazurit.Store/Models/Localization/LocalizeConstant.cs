﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Models.Localization
{
    public static class LocalizeConstant
    {
        public const string DefaultCulture = "ru-RU";
        public const string DefaultCurrencyCode = "RUB";
    }
}