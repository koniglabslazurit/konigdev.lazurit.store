﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Models.AuthModels
{
    public class AuthErrorResponseModel
    {
        [JsonProperty("error")]
        public string ErrorCode { get; set; }
        [JsonProperty("error_description")]
        public string ErrorDescriptions { get; set; }
    }
}