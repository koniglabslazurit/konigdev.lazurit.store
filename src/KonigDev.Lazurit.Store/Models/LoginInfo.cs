﻿using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.Models
{
    public class LoginInfo
    {
        public LoginInfo(string userName, string userId, IEnumerable<string> roles)
        {
            UserName = userName;
            UserId = userId;
            UserRoles = roles;
        }

        public string UserName { get; set; }
        public string UserId { get; set; }
        public IEnumerable<string> UserRoles { get; set; }
    }
}