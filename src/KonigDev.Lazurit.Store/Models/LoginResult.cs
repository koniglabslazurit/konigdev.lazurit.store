﻿using System;
using Microsoft.AspNet.Identity.Owin;

namespace KonigDev.Lazurit.Store.Models
{
    public class LoginResult
    {
        public SignInStatus LoginStatus { get; private set; }
        public LoginInfo LoginInfo { get; private set; }
        public string Error { get; private set; }

        public LoginResult(SignInStatus loginStatus)
        {
            LoginStatus = loginStatus;
        }

        public LoginResult(SignInStatus loginStatus, LoginInfo loginInfo)
        {
            if (loginStatus == SignInStatus.Success && loginInfo == null) throw new NotSupportedException("При удачной авторизации информация о пользователе должна присутствовать обязательно.");
            LoginStatus = loginStatus;
            LoginInfo = loginInfo;
        }

        public LoginResult(SignInStatus loginStatus, string error)
        {
            LoginStatus = loginStatus;
            Error = error;
        }
    }
}