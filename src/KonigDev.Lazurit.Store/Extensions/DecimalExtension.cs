﻿using KonigDev.Lazurit.Store.Models.Localization;
using KonigDev.Lazurit.Store.Models.Prices;
using KonigDev.Lazurit.Store.Models.Prices.KonigDev.Lazurit.Store.Models.Prices;
using System;

namespace KonigDev.Lazurit.Store.Extensions
{
    public static class DecimalExtension
    {
        public static Money ToMoney(this decimal price, Currency currency)
        {
            return new Money(Math.Round(price), currency);
        }
    }
}