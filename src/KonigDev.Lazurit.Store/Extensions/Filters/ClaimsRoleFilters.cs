﻿using System.Security.Claims;
using AuthDTOConstants = KonigDev.Lazurit.Auth.Model.DTO.Constants;

namespace KonigDev.Lazurit.Store.Extensions.Filters
{
    public static class ClaimsRoleFilters
    {
        public static bool IsAdmin(this Claim role)
        {
            return role.Value.ToLower() == AuthDTOConstants.AdminRoleName.ToLower();
        }
        public static bool IsBuyer(this Claim role)
        {
            return role.Value.ToLower() == AuthDTOConstants.BuyerRoleName.ToLower();
        }
        public static bool IsContentManager(this Claim role)
        {
            return role.Value.ToLower() == AuthDTOConstants.ContentManagerRoleName.ToLower();
        }
        public static bool IsMarketer(this Claim role)
        {
            return role.Value.ToLower() == AuthDTOConstants.MarketerRoleName.ToLower();
        }
        public static bool IsRegion(this Claim role)
        {
            return role.Value.ToLower() == AuthDTOConstants.RegionRoleName.ToLower();
        }
        public static bool IsSalonSaller(this Claim role)
        {
            return role.Value.ToLower() == AuthDTOConstants.SalonSallerRoleName.ToLower();
        }
        public static bool IsShopSaller(this Claim role)
        {
            return role.Value.ToLower() == AuthDTOConstants.ShopSallerRoleName.ToLower();
        }
        public static bool IsSaller(this Claim role)
        {
            return role.Value.ToLower() == AuthDTOConstants.SallerRoleName.ToLower();
        }
    }
}