﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Extensions.Filters
{
    public static class GuidExtensions
    {
        /// <summary>
        /// Использую анализатор по дефолту у эластика поиск по умолчанию будет производиться зависимый от регистра и знаки "-" не будут искаться. 
        /// Поэтому ложим в эластик тот вид, по которому будет происходить поиск в стандартном анализаторе.
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static string ToElasticGuidStore(this Guid guid)
        {
            if (Guid.Empty == guid)
                return string.Empty;
            return guid.ToString().Replace("-", "_").ToLowerInvariant();
        }

        public static Guid FromElasticGuidStore(this string guid)
        {
            return Guid.Parse(guid.Replace("_", "-"));
        }
    }
}