﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KonigDev.Lazurit.Auth.Model.Context;
using KonigDev.Lazurit.Store.Models;
using AuthDTOConstants = KonigDev.Lazurit.Auth.Model.DTO.Constants;

namespace KonigDev.Lazurit.Store.Extensions.Filters
{
    public static class StringRoleFilters
    {
        public static bool IsRegion(this string role)
        {
            return role.ToLower() == AuthDTOConstants.RegionRoleName.ToLower();
        }
    }
}