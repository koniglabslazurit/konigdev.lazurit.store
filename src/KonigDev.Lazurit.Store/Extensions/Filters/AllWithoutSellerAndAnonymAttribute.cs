﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using KonigDev.Lazurit.Auth.Model.DTO;

namespace KonigDev.Lazurit.Store.Extensions.Filters
{
    public class AllWithoutSellerAndAnonymAttribute : AuthorizeAttribute
    {
        public AllWithoutSellerAndAnonymAttribute()
        {
            Roles = Constants.AllRoles.Where(x => x != Constants.RegionRoleName || x != Constants.AdminRoleName || x != Constants.SalonSallerRoleName || x != Constants.ShopSallerRoleName || x != Constants.SallerRoleName || x != Constants.AnonymRoleName)
                .Aggregate(Constants.AdminRoleName, (current, role) => current + ", " + role);
        }
    }
}