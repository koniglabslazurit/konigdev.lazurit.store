﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;
using NLog;

namespace KonigDev.Lazurit.Store.Extensions.Filters
{
    /// <summary>
    /// Обработка исключений, не обработанных в коде
    /// </summary>
    public class NotImplExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private readonly Logger _logger;

        public NotImplExceptionFilterAttribute()
        {
            _logger = LogManager.GetCurrentClassLogger();
        }

        public override void OnException(HttpActionExecutedContext context)
        {
            _logger.Error(context.Exception);
        }
    }
}