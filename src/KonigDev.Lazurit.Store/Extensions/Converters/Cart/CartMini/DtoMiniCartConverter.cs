﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.MiniCartDto;
using KonigDev.Lazurit.Store.Dto.Cart.CartMini;

namespace KonigDev.Lazurit.Store.Extensions.Converters.Cart.CartMini
{
    public static class DtoMiniCartConverter
    {
        public static DtoCartMiniPriceResult ToPriceResult(this DtoMiniCart dtoMiniCart)
        {
            return new DtoCartMiniPriceResult
            {
                Id = dtoMiniCart.Id,
                IsDeliveryRequired = dtoMiniCart.IsDeliveryRequired,
                Products = new List<DtoCartMiniItemPriceProduct>(dtoMiniCart.Products?.Select(x=>new DtoCartMiniItemPriceProduct(x)) ?? new DtoCartMiniItemPriceProduct[0]),
                Complects = new List<DtoCartMiniItemPriceComplect>(dtoMiniCart.Complects?.Select(x => new DtoCartMiniItemPriceComplect(x)) ?? new DtoCartMiniItemPriceComplect[0]),
                Collections = new List<DtoCartMiniItemPriceCollection>(dtoMiniCart.Collections?.Select(x => new DtoCartMiniItemPriceCollection(x)) ?? new DtoCartMiniItemPriceCollection[0])
            };
        }
    }
}