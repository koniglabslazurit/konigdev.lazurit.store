﻿using KonigDev.Lazurit.Hub.Model.DTO.Cart.FullCartDto;
using KonigDev.Lazurit.Store.Dto.Cart;
using KonigDev.Lazurit.Store.Dto.Cart.CartFull;
using KonigDev.Lazurit.Store.Services;
using KonigDev.Lazurit.Store.Services.PriceSrvice;

namespace KonigDev.Lazurit.Store.Extensions.Converters.Cart.CartFull
{
    public static class DtoCartItemProductConverter
    {
        /// <summary>
        /// Конвертирует dto из DataLayer в Dto для View и добавляет цену. Поля с ценами пока пустые. Их заполняет сервис <see cref="PriceService"/>
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static DtoCartFullItemPriceProduct ToPriceResult(this DtoCartItemProduct item)
        {
            return new DtoCartFullItemPriceProduct
            {
                CartId = item.CartId,
                Id = item.Id,
                Product = item.Product,
                ProductId = item.ItemId,
                Quantity = item.Quantity,
                IsAssemblyRequired = item.IsAssemblyRequired
            };
        }

        /// <summary>
        /// Конвертирует dto из DataLayer в Dto для View и добавляет цену. Поля с ценами пока пустые. Их заполняет сервис <see cref="PriceService"/>
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static DtoCartFullItemPriceProductInCollection ToPriceResult(this DtoCartItemProductInCollection item)
        {
            return new DtoCartFullItemPriceProductInCollection
            {
                Id = item.Id,
                Product = item.Product,
                ProductId = item.ItemId,
                Quantity = item.Quantity,
                IsAssemblyRequired = item.IsAssemblyRequired,
                CartItemCollectionId = item.CartItemCollectionId
            };
        }
    }
}