﻿using System;
using System.Collections.Generic;
using System.Linq;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.FullCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Dto;
using KonigDev.Lazurit.Store.Dto.Cart;
using KonigDev.Lazurit.Store.Dto.Cart.CartFull;
using KonigDev.Lazurit.Store.Services;
using KonigDev.Lazurit.Store.Services.PriceSrvice;

namespace KonigDev.Lazurit.Store.Extensions.Converters.Cart.CartFull
{
    public static class DtoCartConverter
    {
        /// <summary>
        /// Конвертирует dto из DataLayer в Dto для View и добавляет цену. Поля с ценами пока пустые. Их заполняет сервис <see cref="PriceService"/>
        /// </summary>
        /// <param name="dtoCart"></param>
        /// <returns></returns>
        public static DtoCartFullPriceResult ToPriceResult(this DtoFullCart dtoCart)
        {
            return new DtoCartFullPriceResult
            {
                Id = dtoCart.Id,
                IsDeliveryRequired = dtoCart.IsDeliveryRequired,
                Products = dtoCart.Products?.Select(x => DtoCartItemProductConverter.ToPriceResult((DtoCartItemProduct) x)).ToList() ?? new List<DtoCartFullItemPriceProduct>(),
                Collections = dtoCart.Collections?.Select(x => x.ToPriceResult()).ToList() ?? new List<DtoCartFullItemPriceCollection>(),
                Complects = dtoCart.Complects?.Select(x => x.ToPriceResult()).ToList() ?? new List<DtoCartFullItemPriceComplect>()
            };
        }

        public static CreateOrderCommand ToOrder(this DtoCartFullPriceResult cart)
        {
            if (cart == null) return null;

            var result = new CreateOrderCommand();
            result.Id = Guid.NewGuid();
            result.Sum = cart.TotalPrice.Amount;
            result.IsDeliveryRequired = cart.IsDeliveryRequired;

            result.Collections = cart.Collections
                .Select(p => new DtoOrderCollectionCreating
                {
                    CollectionVariantId = p.CollectionVariantId,
                    Id = Guid.NewGuid(),
                    Quantity = p.Quantity,
                    Products = p.Products.Select(x => new DtoOrderProductCreating
                    {
                        Id = Guid.NewGuid(),
                        Discount = (byte)x.Discount,
                        IsAssemblyRequired = x.IsAssemblyRequired,
                        Price = x.SalePrice.Amount,
                        ProductId = x.ItemId,
                        Quantity = x.Quantity
                    }).ToList()
                }).ToList();

            result.Products = cart.Products.Select(x => new DtoOrderProductCreating
            {
                Id = Guid.NewGuid(),
                Discount = (byte)x.Discount,
                IsAssemblyRequired = x.IsAssemblyRequired,
                Price = x.SalePrice.Amount,
                ProductId = x.ItemId,
                Quantity = x.Quantity
            }).ToList();

            result.Complects = cart.Complects.Select(p => new DtoOrderComplectCreating
            {
                Quantity = p.Quantity,
                CollectionVariantId = p.CollectionVariant.Id,
                Discount = (byte)p.Discount,
                Id = Guid.NewGuid(),
                IsAssemblyRequired = p.IsAssemblyRequired,
                Price = p.SalePrice.Amount
            }).ToList();

            return result;
        }
    }
}