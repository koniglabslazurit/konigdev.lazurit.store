﻿using System.Linq;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.FullCartDto;
using KonigDev.Lazurit.Store.Dto.Cart;
using KonigDev.Lazurit.Store.Dto.Cart.CartFull;
using KonigDev.Lazurit.Store.Services;
using KonigDev.Lazurit.Store.Services.PriceSrvice;

namespace KonigDev.Lazurit.Store.Extensions.Converters.Cart.CartFull
{
    public static class DtoCartItemCollectionConverter
    {
        /// <summary>
        /// Конвертирует dto из DataLayer в Dto для View и добавляет цену. Поля с ценами пока пустые. Их заполняет сервис <see cref="PriceService"/>
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static DtoCartFullItemPriceCollection ToPriceResult(this DtoCartItemCollection item)
        {
            return new DtoCartFullItemPriceCollection
            {
                CartId = item.CartId,
                CollectionVariantId = item.CollectionVariantId,
                Id = item.Id,
                Quantity = item.Quantity,
                Products = item.Products?.Cast<DtoCartItemProductInCollection>().Select(x => x.ToPriceResult()).ToList()
            };
        }
    }
}