﻿using KonigDev.Lazurit.Hub.Model.DTO.Cart.FullCartDto;
using KonigDev.Lazurit.Store.Dto.Cart.CartFull;

namespace KonigDev.Lazurit.Store.Extensions.Converters.Cart.CartFull
{
    public static class DtoCartItemComplectConverter
    {
        /// <summary>
        /// Конвертирует dto из DataLayer в Dto для View и добавляет цену. Поля с ценами пока пустые. Их заполняет сервис <see cref="PriceService"/>
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static DtoCartFullItemPriceComplect ToPriceResult(this DtoCartItemComplect item)
        {
            return new DtoCartFullItemPriceComplect
            {
                CartId = item.CartId,
                CollectionVariant = item.CollectionVariant,
                Id = item.Id,
                Quantity = item.Quantity,
                IsAssemblyRequired = item.IsAssemblyRequired,
                ItemId = item.CollectionVariant.Id
            };
        }
    }
}