﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands;
using KonigDev.Lazurit.Store.Dto.Account;

namespace KonigDev.Lazurit.Store.Extensions.Converters.Account
{
    public static class DtoRestorePasswordConverter
    {
        public static RestorePasswordCmd ToRestoreCmdConverter(this DtoRestorePasswordRequest request, Guid userId)
        {
            return new RestorePasswordCmd
            {
                OldPassword=request.OldPassword,
                NewPassword = request.NewPassword,
                UserId = userId
            };
        }
    }
}