﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace KonigDev.Lazurit.Store.Extensions
{
    public class ProductAliasParser
    {
        /// <summary>
        /// Parsing alias of Product page on separate fields of return type
        /// </summary>
        /// <param name="alias">Alias for Product page</param>
        /// <returns>Custom DtoResult</returns>
        public static ProductAliasParserResult ParseAlias(string alias)
        {
            var regexItem = new Regex("^[a-zA-Z0-9-]*$");

            if (regexItem.IsMatch(alias))
            {
                alias = alias.TrimEnd('-');
                var aliasWithoutArticle = alias.Remove(alias.LastIndexOf('-') + 1);
                aliasWithoutArticle = aliasWithoutArticle.TrimEnd('-');
                var substrings = alias.Split('-');

                return new ProductAliasParserResult
                {
                    CollectionAlias = aliasWithoutArticle,
                    ArticleCodeFull = substrings.Last(),
                    ArticleCodeOnlyDigits = new string(substrings.Last().TakeWhile(char.IsDigit).ToArray()),
                    HasErrors = false
                };
            }
            return new ProductAliasParserResult();
        }
    }

    public class ProductAliasParserResult
    {
        [Obsolete("Must be rename to series alias")]
        public string CollectionAlias { get; set; }
        public string ArticleCodeFull { get; set; }
        public string ArticleCodeOnlyDigits { get; set; }
        public bool HasErrors { get; set; } = true;
    }
}