﻿using KonigDev.Lazurit.Store.SignalrService.SignalRExtensions;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Extensions.AppBuilderExtensions
{
    public static class JsonSerializerResolver
    {
        public static void UseCammelCaseSignalRJsonResolver(this IAppBuilder app)
        {
            var settings = new JsonSerializerSettings();
            settings.ContractResolver = new SignalRContractResolver();
            var serializer = JsonSerializer.Create(settings);
            GlobalHost.DependencyResolver.Register(typeof(JsonSerializer), () => serializer);
        }
    }
}