﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using Owin;

namespace KonigDev.Lazurit.Store.Extensions.AppBuilderExtensions
{
    public static class OAuthTokenOwinAppExtension
    {
        public static void UseOAuthTokenForConsumer(this IAppBuilder app)
        {
            var issuer = $"{ConfigurationManager.AppSettings["auth:Protokol"]}://{ConfigurationManager.AppSettings["auth:Domain"]}";

            string audienceId = ConfigurationManager.AppSettings["auth:AudienceId"];
            var audienceSecret = TextEncodings.Base64Url.Decode(ConfigurationManager.AppSettings["auth:AudienceSecret"]);

            app.UseJwtBearerAuthentication(
            new JwtBearerAuthenticationOptions
            {
                AuthenticationMode = AuthenticationMode.Active,
                AllowedAudiences = new[] { audienceId },
                IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                {
                        new SymmetricKeyIssuerSecurityTokenProvider(issuer, audienceSecret)
                }
            });

        }
    }
}