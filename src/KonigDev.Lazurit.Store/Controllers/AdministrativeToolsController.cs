﻿using KonigDev.Lazurit.Store.Extensions.Filters;
using System.Web.Mvc;
using AuthDTOConstants = KonigDev.Lazurit.Auth.Model.DTO.Constants;

namespace KonigDev.Lazurit.Store.Controllers
{
    [Authorize]
    [NotImplExceptionFilter]
    public class AdministrativeToolsController : Controller
    {
        /// <summary>
        /// Страница просмотра заказов
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = AuthDTOConstants.AdminRoleName + "," + AuthDTOConstants.ShopSallerRoleName)]
        public ActionResult OrdersTool()
        {
            return View();
        }

        /// <summary>
        /// Страница верификации продавцов
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = AuthDTOConstants.AdminRoleName)]
        public ActionResult VerifySellers()
        {
            return View();
        }

        /// <summary>
        /// Страница управления продавцами
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = AuthDTOConstants.AdminRoleName)]
        public ActionResult Sellers()
        {
            return View();
        }
        /// <summary>
        /// Страница управления административными ролями
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = AuthDTOConstants.AdminRoleName)]
        public ActionResult AdministrativeRoles()
        {
            return View();
        }

        /// <summary>
        /// Управление пользователями-клиентами
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = AuthDTOConstants.AdminRoleName)]
        public ActionResult AllocateRole()
        {
            return View();
        }

        /// <summary>
        /// Управление маркетинговыми акциями
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = AuthDTOConstants.AdminRoleName)]
        public ActionResult MarketingActions()
        {
            return View();
        }

        /// <summary>
        /// List of customer requests
        /// </summary>
        /// <remarks>
        /// List of call request, designer request, etc.
        /// </remarks>
        [Authorize(Roles = AuthDTOConstants.AdminRoleName + "," + AuthDTOConstants.MarketerRoleName)]
        public ActionResult CustomerRequests()
        {
            return View();
        }
    }
}