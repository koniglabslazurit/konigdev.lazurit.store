﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using KonigDev.Lazurit.Core.Enums.Enums;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto.Products;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Dto.Import;
using KonigDev.Lazurit.Store.Extensions.Filters;
using AuthDTOConstants = KonigDev.Lazurit.Auth.Model.DTO.Constants;
using KonigDev.Lazurit.Store.ViewModels.CommonViewModels;
using KonigDev.Lazurit.Core.Enums;
using System;
using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query.Collection;
using KonigDev.Lazurit.Store.ViewModels.CollectionViewModels;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto.Collections;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Command;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Query;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Dto;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [Authorize]
    [NotImplExceptionFilter]
    [RoutePrefix("api/import")]
    public class ImportController : ApiController
    {
        private readonly IHubHandlersFactory _hubFactory;

        public ImportController(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        /// <summary>
        /// получение списка корнизов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("product-markers")]
        [Authorize(Roles = AuthDTOConstants.AdminRoleName + "," + AuthDTOConstants.ContentManagerRoleName)]
        public IHttpActionResult GetProductMarkers()
        {
            var result = new List<VmSelectItem>
            {
                new VmSelectItem {Id = "Да нет", Name = "Да нет" },
                new VmSelectItem {Id = "Нет нет", Name = "Нет нет" },
                new VmSelectItem {Id = "Да да", Name = "Да да" }
            };
            return Ok(result);
        }

        /// <summary>
        /// Получение списка интеграционных продуктов по запросу
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("product")]
        [Authorize(Roles = AuthDTOConstants.AdminRoleName + "," + AuthDTOConstants.ContentManagerRoleName)]
        public async Task<IHttpActionResult> GetProducts([FromUri]GetIntegrationProductByRequestQuery request)
        {
            if (request.IntegrationCollectionId == Guid.Empty)
                return BadRequest("IntegrationCollectionId is missing");

            if (string.IsNullOrEmpty(request.ProductStatus))
                return BadRequest("ProductStatus is missing");

            var response = await _hubFactory.CreateQueryHandler<GetIntegrationProductByRequestQuery, DtoIntegrationProductResponse>().Execute(request);
            return Ok(response);
        }

        /// <summary>
        /// Обновление интеграционных/опубликованных продуктов
        /// </summary>
        /// <param name="command">Список продуктов на обновление</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("products")] /* todo сменить ActionName на Route */
        public async Task<IHttpActionResult> UpdateProducts([FromBody] List<UpdateIntegrationProductsCommand> command)
        {
            foreach (var item in command)
            {
                var commandHandler = _hubFactory.CreateCommandHandler<UpdateIntegrationProductsCommand>();
                await commandHandler.Execute(item);
            }
            return Ok();
        }

        /// <summary>
        /// Получение списка коллекций из интеграционных данных.
        /// </summary>
        /// <param name="query" <seealso cref="KonigDev.Lazurit.Hub.Model.DTO.DtoCommonTableRequest"/> <see cref="KonigDev.Lazurit.Store.Dto.Import.DtoImportProducts"/>></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("collections")]/* todo сменить ActionName на Route */
        public async Task<DtoIntegrationCollectionTableResponse> GetCollectionProducts([FromUri]DtoGetImportProductsQuery query)
        {
            var queryHandler = _hubFactory.CreateQueryHandler<GetIntegrationCollectionsProductsQuery, DtoIntegrationCollectionTableResponse>();
            var result = await queryHandler.Execute(new GetIntegrationCollectionsProductsQuery
            {
                Page = query.Page,
                ItemsOnPage = query.ItemsOnPage
            });
            return result;
        }

        /// <summary>
        /// Получение данных по коллекции по ее ид
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("collection")]/* todo сменить ActionName на Route */
        public async Task<IHttpActionResult> GetCollection([FromUri] VmCollectionId collection)
        {
            var queryHandler = _hubFactory.CreateQueryHandler<GetIntegrationVariantsWithContentQuery, DtoIntegrationCollectionWithContent>();
            var result = await queryHandler.Execute(new GetIntegrationVariantsWithContentQuery { CollectionId = collection.CollectionId });
            return Ok(result);
        }

        [HttpPost]
        [Route("collection-variant")]
        public async Task<IHttpActionResult> PublishCollectionVariant([FromBody] CreateVariantByIntegrationVariantCommand command)
        {
            var commandHandler = _hubFactory.CreateCommandHandler<CreateVariantByIntegrationVariantCommand>();
            await commandHandler.Execute(command);
            return Ok();
        }

        /// <summary>
        /// Валидация возможности публикации варианта исполнения
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("collection-variant/valid")]
        public async Task<IHttpActionResult> ValidateCollectionVariant([FromUri] GetCollectionVariantValidatorQuery query)
        {
            var result = await _hubFactory.CreateQueryHandler<GetCollectionVariantValidatorQuery, DtoCollectionVariantValidation>().Execute(query);
            return Ok(result);            
        }


        [HttpGet]
        [ActionName("sync-code")] /* todo сменить ActionName на Route */
        public async Task<List<DtoSyncCode1CItem>> GetSyncCode1C([FromUri]GetSyncCode1CsQuery query)
        {
            var getSyncCode1CQuery = _hubFactory.CreateQueryHandler<GetSyncCode1CsQuery, List<DtoSyncCode1CItem>>();
            return await getSyncCode1CQuery.Execute(query);
        }

        [HttpGet]
        [ActionName("furniture-types")] /* todo сменить ActionName на Route */
        public async Task<List<DtoFurnitureTypeItem>> GetFurnitureTypes([FromUri]GetFurnitureTypesQuery query)
        {
            var getFurnitureTypesQuery = _hubFactory.CreateQueryHandler<GetFurnitureTypesQuery, List<DtoFurnitureTypeItem>>();
            return await getFurnitureTypesQuery.Execute(query);
        }

        /// <summary>
        /// Получение списка опубликованных продуктов
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("published-products")] /* todo сменить ActionName на Route */
        public async Task<DtoPublishedProductResponse> GetPublishedProducts([FromUri]GetPublishedIntegrationProductsByVendorCodeQuery query)
        {
            var getPublishedProductsQuery = _hubFactory.CreateQueryHandler<GetPublishedIntegrationProductsByVendorCodeQuery, DtoPublishedProductResponse>();
            return await getPublishedProductsQuery.Execute(query);
        }

        /// <summary>
        /// Поставить вариант основным
        /// </summary>
        [HttpPost]
        [ActionName("make-default-variant")] /* todo сменить ActionName на Route */
        public async Task<IHttpActionResult> MakeDefault([FromBody]DtoMakeDefault model)
        {
            var commandHandler = _hubFactory.CreateCommandHandler<UpdateIntegrationVariantCommand>();
            await commandHandler.Execute(new UpdateIntegrationVariantCommand { CollectionVariantId = model.VariantId });
            return Ok();
        }

        /// <summary>
        /// Валидация возможности публикации товара
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("valid")] /* todo сменить ActionName на Route */
        public async Task<IHttpActionResult> Valid([FromUri] GetIntegrationProductValidationQuery query)
        {
            var result = await _hubFactory.CreateQueryHandler<GetIntegrationProductValidationQuery, DtoIntegrationProductValidation>().Execute(query);
            return Ok(result);
        }

        [HttpGet]
        [Route("validunpublish")]
        public async Task<IHttpActionResult> ValidUnPublish([FromUri] GetProductUnPublishValidationQuery command)
        {
            var result = await _hubFactory.CreateQueryHandler<GetProductUnPublishValidationQuery, DtoProductUnPublishValidation>().Execute(command);
            return Ok(result);
        }


        [HttpPost]
        [Route("unpublish")]
        public async Task<IHttpActionResult> UnPublish([FromBody] UnPublishProductCommand command)
        {
            var hub = _hubFactory.CreateCommandHandler<UnPublishProductCommand>();
            await hub.Execute(command);
            return Ok();
        }
    }
}
