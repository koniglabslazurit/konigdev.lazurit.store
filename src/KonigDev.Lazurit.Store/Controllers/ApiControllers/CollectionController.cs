﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Collections;
using KonigDev.Lazurit.Store.Dto.Collection;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Http;
using MoreLinq;
using KonigDev.Lazurit.Store.Extensions.Filters;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Query;
using AutoMapper;
using KonigDev.Lazurit.Store.Dto.Prices;
using KonigDev.Lazurit.Store.Services.LazuritContext;
using KonigDev.Lazurit.Store.Services.PriceSrvice;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [RoutePrefix("api/collection")]
    [NotImplExceptionFilter]
    public class CollectionController : ApiController
    {
        private readonly IHubHandlersFactory _hubFactory;
        private readonly ILazuritCtxSessionService _lazuritCtxService;
        private readonly IPriceService _priceService;

        public CollectionController(IHubHandlersFactory hubFactory, ILazuritCtxSessionService lazuritCtxService, IPriceService priceService)
        {
            _hubFactory = hubFactory;
            _lazuritCtxService = lazuritCtxService;
            _priceService = priceService;
        }

        /// <summary>
        /// Получение модели для типа комнаты.
        /// </summary>
        /// <param name="alias"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public async Task<DtoRoomFiltersResult> List(string alias)
        {
            var getCollectionsQueryHandler = _hubFactory.CreateQueryHandler<GetCollectionsQuery, List<DtoCollectionResultForCatalog>>();
            var collections = await getCollectionsQueryHandler.Execute(new GetCollectionsQuery { RoomAlias = alias });

            /* todo откидываем те записи, у которыех нет вариантов коллекции, посмотреть перенос логики в грейн?
             *     * необходимо чтобы не отображались комнаты, с коллекциями, у кторых нет опубликованных вариантов */
            collections = collections.Where(c => c.Variants.Any()).ToList();

            return new DtoRoomFiltersResult
            {
                Room = collections.FirstOrDefault()?.Room,
                Collections = collections,
                Facings = collections.SelectMany(c => c.Variants).SelectMany(x => x.Facings).DistinctBy(x => x.Id),
                FacingColors = collections.SelectMany(c => c.Variants).SelectMany(x => x.FacingColors).DistinctBy(x => x.Id),
                FrameColors = collections.SelectMany(c => c.Variants).SelectMany(x => x.FrameColors).DistinctBy(x => x.Id),
                Styles = collections.SelectMany(c => c.Variants).SelectMany(x => x.Styles).DistinctBy(x => x.Id),
                TargetAudiences = collections.SelectMany(c => c.Variants).SelectMany(x => x.TargetAudiences).DistinctBy(x => x.Id),
            };
        }

        [HttpGet]
        [Route("variant-content")]
        public async Task<IHttpActionResult> VariantContent(string roomTypeAlias, string seriesAlias)
        {
            /* получение вариантов коллекции с контентом и продуктами */
            var handler = _hubFactory.CreateQueryHandler<GetCollectionWithVariantsAndContentQuery, DtoCollectionWithContent>();
            var collectionWithVariantsContent = await handler.Execute(new GetCollectionWithVariantsAndContentQuery
            {
                RoomTypeAlias = roomTypeAlias,
                SeriesAlias = seriesAlias,
                UserId = User.Identity.IsAuthenticated ? Guid.Parse(User.Identity.GetUserId()) : (Guid?)null
            });

            var model = Mapper.Map<DtoCollectionWithContentPrice>(collectionWithVariantsContent);
            var priceInfo = _lazuritCtxService.GetPriceInfoFromContext();
            await _priceService.GetProductsPrices(priceInfo.PriceZoneId, priceInfo.Currency, model.Variants.SelectMany(p => p.Products).ToList());
            return Ok(model);
        }

        [HttpGet]
        [Route("product")]
        public async Task<IHttpActionResult> CollectionProducts()
        {

            return null;
        }
    }
}
