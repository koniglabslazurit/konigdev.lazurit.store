﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.CustomerRequests.Dtos;
using KonigDev.Lazurit.Hub.Model.DTO.CustomerRequests.Queries;
using KonigDev.Lazurit.Store.Extensions.Filters;
using System.Threading.Tasks;
using System.Web.Http;
using KonigDev.Lazurit.Hub.Model.DTO.CustomerRequests.Commands;
using KonigDev.Lazurit.Core.Enums.Enums.CustomerRequests;
using System;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [AllowAnonymous]
    [NotImplExceptionFilter]
    [RoutePrefix("api/requests")]
    public class CustomerRequestController : ApiController
    {
        private readonly IHubHandlersFactory _hubFactory;

        public CustomerRequestController(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        [HttpGet]
        [Route("")]
        public async Task<DtoCustomerRequests> GetAsync()
        {
            var queryHandler = _hubFactory.CreateQueryHandler<GetAllCustomerRequestsQuery, DtoCustomerRequests>();
            return await queryHandler.Execute(new GetAllCustomerRequestsQuery());
        }

        [HttpPost]
        [Route("call")]
        public async Task CreateCallRequestAsync(CreateCustomerRequestCommand command)
        {
            var commandHandler = _hubFactory.CreateCommandHandler<CreateCustomerRequestCommand>();
            command.Type = EnumCustomerRequestType.Call.ToString();
            await commandHandler.Execute(command);
        }

        [HttpPost]
        [Route("designer-online")]
        public async Task CreateDesignerOnlineRequestAsync(CreateCustomerRequestCommand command)
        {
            var commandHandler = _hubFactory.CreateCommandHandler<CreateCustomerRequestCommand>();
            command.Type = EnumCustomerRequestType.DesignerOnline.ToString();
            await commandHandler.Execute(command);
        }

        [HttpPost]
        [Route("designer-to-home")]
        public async Task CreateDesignerToHomeRequestAsync(CreateCustomerRequestCommand command)
        {
            var commandHandler = _hubFactory.CreateCommandHandler<CreateCustomerRequestCommand>();
            command.Type = EnumCustomerRequestType.DesignerHome.ToString();
            await commandHandler.Execute(command);
        }

        [HttpPut]
        [Route("{id}")]
        public async Task UpdateRequestStatusAsync(Guid id, UpdateCustomerRequestStatusCommand command)
        {
            var commandHandler = _hubFactory.CreateCommandHandler<UpdateCustomerRequestStatusCommand>();
            command.Id = id;
            await commandHandler.Execute(command);
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task RemoveRequestAsync(Guid id)
        {
            var commandHandler = _hubFactory.CreateCommandHandler<RemoveCustomerRequestCommand>();
            var command = new RemoveCustomerRequestCommand
            {
                Id = id
            };
            await commandHandler.Execute(command);
        }
    }
}
