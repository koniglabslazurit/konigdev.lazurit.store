﻿using KonigDev.Lazurit.Core.Enums;
using KonigDev.Lazurit.Core.Enums.Constants;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Query;
using KonigDev.Lazurit.Store.Extensions.Filters;
using KonigDev.Lazurit.Store.Services.OrderService;
using KonigDev.Lazurit.Store.Services.PaymentService;
using KonigDev.Lazurit.Store.ViewModels.OrderViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AuthDTOConstants = KonigDev.Lazurit.Auth.Model.DTO.Constants;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [NotImplExceptionFilter]
    [RoutePrefix("api/order")]
    public class OrderController : ApiController
    {
        private readonly IOrderService _orderService;
        private readonly IPaymentService _paymentService;
        public OrderController(IOrderService orderService, IPaymentService paymentService)
        {
            _orderService = orderService;
            _paymentService = paymentService;
        }

        /// <summary>
        /// Получение заказа по его ид
        /// </summary>
        /// <param name="id">Ид заказа</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:Guid}")]
        [Authorize]
        public async Task<HttpResponseMessage> Get(Guid id)
        {
            var res = await _orderService.GetOrder(id);
            return Request.CreateResponse(res);
        }

        /// <summary>
        /// Получение списка заказов авторизованного пользователя
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("my")]
        [Authorize]
        public async Task<HttpResponseMessage> GetMyOrders()
        {
            var userId = new Guid(User.Identity?.GetUserId());
            var res = await _orderService.GetOrderList(new GetOrdersQuery { UserProfileId = userId });
            return Request.CreateResponse(res);
        }

        /// <summary>
        /// Получение списка заказов по запросу
        /// </summary>
        /// <param name="request">Параметризированный запрос</param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        [Authorize]
        public async Task<HttpResponseMessage> GetOrders([FromUri]GetOrdersQuery request)
        {
            var res = await _orderService.GetOrderList(request);
            return Request.CreateResponse(res);
        }
        /// <summary>
        /// Пометка заказа апрувленным контент менеджером
        /// </summary>
        /// <param name="request">Модель на запрос</param>
        /// <returns></returns>
        [Authorize(Roles = AuthDTOConstants.AdminRoleName + "," + AuthDTOConstants.ContentManagerRoleName)]
        [HttpPost]
        [Route("approve")]
        public async Task<HttpResponseMessage> UpdateApprove([FromBody] VmOrderApprove request)
        {
            var userId = new Guid(User.Identity?.GetUserId());
            await _orderService.UpdateApprove(request.Id, request.IsApproved);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        [Authorize(Roles = AuthDTOConstants.AdminRoleName + "," + AuthDTOConstants.ContentManagerRoleName)]
        [Route("1c-fields")]
        public async Task<HttpResponseMessage> UpdateOrder1CFields([FromBody] VmOrder1CFields request)
        {
            await _orderService.UpdateOrder1CFields(request.Id, request.OrderNumber1C, request.CreationTime1C);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// Создание заказа
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [Route("")]
        public async Task<HttpResponseMessage> Create(VmOrderCreating model)
        {
            var userId = new Guid(User.Identity.GetUserId());
            var result = await _orderService.CreateOrder(userId, model);

            var response = new VmOrderResponse { Id = result.Id, PaymentMethod = result.PaymentMethod, IsPaid = result.IsPaid, Number = result.Number, Sum = Convert.ToString(result.Sum).Replace(",", ".") };
            /* если оплата через робокассу */
            if (result.PaymentMethod == EnumPaymentMethod.QiwiWallet.ToString())
            {
                var settings = _paymentService.GetPaymentSettings();
                response.RobokassaUrl = settings.RobokassaUrl;
                response.MrchLogin = settings.MerchantLogin;
                response.Desc = string.Format(ConstantMessage.Order.OrderDescr + "{0}", response.Number);
                response.SignatureValue = _paymentService.GetPaymentSignature(settings, response.Number, response.Sum).SignatureValue;
            }
            return Request.CreateResponse(response);
        }

        [HttpGet]
        [Route("details")]
        [Authorize]
        public async Task<HttpResponseMessage> GetOrderDetails(Guid id)
        {
            var order = await _orderService.GetOrderDetails(id);
            var response = new VmOrderResponse { Id = order.Id, PaymentMethod = order.PaymentMethod, IsPaid = order.IsPaid, Number = order.Number, Sum = Convert.ToString(order.Sum).Replace(",", ".") };
            /* если оплата через робокассу */
            if (order.PaymentMethod == EnumPaymentMethod.QiwiWallet.ToString()
                || order.PaymentMethod == EnumPaymentMethod.YandexMoney.ToString()
                || order.PaymentMethod == EnumPaymentMethod.PayPal.ToString())
            {
                var settings = _paymentService.GetPaymentSettings();
                response.RobokassaUrl = settings.RobokassaUrl;
                response.MrchLogin = settings.MerchantLogin;
                response.Desc = string.Format(ConstantMessage.Order.OrderDescr + "{0}", response.Number);
                response.SignatureValue = _paymentService.GetPaymentSignature(settings, response.Number, response.Sum).SignatureValue;
            }
            return Request.CreateResponse(response);
        }
    }
}
