﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.ShowRooms;
using KonigDev.Lazurit.Hub.Model.DTO.ShowRooms.Query;
using KonigDev.Lazurit.Store.Extensions.Filters;
using AuthDTOConstants = KonigDev.Lazurit.Auth.Model.DTO.Constants;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [RoutePrefix("api/showrooms")]
    [NotImplExceptionFilter]
    [Authorize]
    public class ShowRoomController : ApiController
    {
        private readonly IHubHandlersFactory _hubFactory;

        public ShowRoomController(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        [HttpGet]
        [Authorize(Roles = AuthDTOConstants.RegionRoleName)]
        [Route("")]
        public async Task<IEnumerable<DtoShowRoomResult>> GetShowrooms(Guid cityId)
        {
            var getShowRoomsByIdQueryHandler = _hubFactory.CreateQueryHandler<GetShowRoomsByCityIdQuery, List<DtoShowRoomResult>>();
            return await getShowRoomsByIdQueryHandler.Execute(new GetShowRoomsByCityIdQuery() { CityId = cityId });
        }
    }
}
