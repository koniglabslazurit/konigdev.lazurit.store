﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Styles.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Styles.Query;
using KonigDev.Lazurit.Store.Extensions.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [NotImplExceptionFilter]
    public class StyleController : ApiController
    {
        private readonly IHubHandlersFactory _hubFactory;

        public StyleController(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        [HttpGet]
        public async Task<List<DtoStyleItem>> GetStyles()
        {
            var getStyleHandler = _hubFactory.CreateQueryHandler<GetStylesQuery, List<DtoStyleItem>>();
            var styles = await getStyleHandler.Execute(new GetStylesQuery());
            return styles;
        }
    }
}
