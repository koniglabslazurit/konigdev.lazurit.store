﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.MarketingActions;
using KonigDev.Lazurit.Hub.Model.DTO.MarketingActions.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.MarketingActions.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [RoutePrefix("api/marketing/actions")]
    public class MarketingActionsController : ApiController
    {
        private readonly IHubHandlersFactory _hubFactory;
        public MarketingActionsController(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> GetAll([FromUri] AllMarketingActionsQuery query)
        {
            var queryHandler = _hubFactory.CreateQueryHandler<AllMarketingActionsQuery, DtoMarketingActions>();
            var result = await queryHandler.Execute(query);

            return Ok(result);
        }

        [HttpPost]
        [Route("")]
        public async Task<IHttpActionResult> PostCreate([FromBody] CreateMarketingActionCommand command)
        {
            command.Action.Id = Guid.NewGuid();
            var commandHandler = _hubFactory.CreateCommandHandler<CreateMarketingActionCommand>();
            await commandHandler.Execute(command);

            return Ok(command.Action.Id);
        }

        [HttpPut]
        [Route("")]
        public async Task<IHttpActionResult> PutUpdate([FromBody] UpdateMarketingActionCommand command)
        {
            var commandHandler = _hubFactory.CreateCommandHandler<UpdateMarketingActionCommand>();
            await commandHandler.Execute(command);

            return Ok();
        }

        [HttpDelete]
        [Route("{id:guid}")]
        public async Task<IHttpActionResult> Delete(Guid id)
        {
            var commandHandler = _hubFactory.CreateCommandHandler<DeleteMarketingActionCommand>();
            await commandHandler.Execute(new DeleteMarketingActionCommand { ActionId = id});

            return Ok();
        }
    }
}
