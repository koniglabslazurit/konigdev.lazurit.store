﻿using System;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.InternalAPI.AccountApi.Interfaces;
using KonigDev.Lazurit.Auth.InternalAPI.DTO;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Email;
using KonigDev.Lazurit.Core.Services;
using KonigDev.Lazurit.Store.Dto;
using KonigDev.Lazurit.Store.Dto.Account;
using KonigDev.Lazurit.Store.Extensions;
using KonigDev.Lazurit.Store.Extensions.Converters.Account;
using KonigDev.Lazurit.Store.Extensions.Filters;
using KonigDev.Lazurit.Store.Services;
using KonigDev.Lazurit.Store.Models;
using KonigDev.Lazurit.Store.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using AuthDTOConstants = KonigDev.Lazurit.Auth.Model.DTO.Constants;
using KonigDev.Lazurit.Store.ViewModels.UserViewMOdels;
using KonigDev.Lazurit.Store.ViewModels.ProfileViewModels;
using KonigDev.Lazurit.Store.KonigDev.Lazurit.Store.Services.Auth;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [RoutePrefix("api/account")]
    [NotImplExceptionFilter]
    public class AccountController : ApiController
    {
        private readonly IAuthServer _authServer;
        private readonly IAccountInternalAPI _accountInternalApi;
        private readonly IAuthHandlersFactory _authFactory;
        private readonly ILoggingService _logger;
        private readonly IUserService _userService;

        public AccountController(IAuthServer authServer, IAccountInternalAPI accountInternalApi,
            IAuthHandlersFactory authFactory, ILoggingService loggingService, IUserService userService)
        {
            _authServer = authServer;
            _accountInternalApi = accountInternalApi;
            _authFactory = authFactory;
            _logger = loggingService;
            _userService = userService;
        }

        [HttpGet]
        [Authorize]
        public bool IsRegionUser()
        {
            var userRoles = _authServer.GetRolesForIdentity();
            return userRoles.Any(x => x.IsRegion());
        }

        /// <summary>
        /// Метод авторизации пользователей - прилетает логин, пароль стучится в сервер авторизации и возвращяет результат
        /// </summary>
        ///<param name="model">модель представления для авторизации</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<DtoUserAuthResult> LoginByUserName(DtoUserAuthByUserName model)
        {
            var result = await _authServer.Login(model.Login, model.Password);
            return ParseLoginResult(result, model.ReturnUrl);
        }

        /// <summary>
        /// Метод авторизации пользователей - прилетает логин, пароль стучится в сервер авторизации и возвращяет результат
        /// </summary>
        ///<param name="model">модель представления для авторизации</param>
        /// <returns></returns>
        [AllowAnonymous]
        public async Task<DtoUserAuthResult> LoginByUserId(DtoUserAuthByUserId model)
        {
            var result = await _authServer.Login(model.UserID, model.Password);
            return ParseLoginResult(result, model.ReturnUrl);
        }

        [AllowAnonymous]
        public async Task<DtoUserAuthResult> LoginByUserPhone(DtoUserAuthByUserPhone model)
        {
            var result = await _authServer.LoginByUserPhone(model.UserPhone, model.Password);
            return ParseLoginResult(result, model.ReturnUrl);
        }

        [AllowAnonymous]
        public async Task<DtoRecoveryPasswordResult> RecoveryPassword(DtoUserRestorePassword model)
        {
            if (!ModelState.IsValid)
                return new DtoRecoveryPasswordResult { IsAccepted = false, Message = GetModelStateErrors(ModelState) };
            var recoveryResult = await _accountInternalApi.RecoveryPasswordAsync(model.Email);

            return recoveryResult.IsSuccess
                ? new DtoRecoveryPasswordResult { IsAccepted = true, Message = "На вашу почту отправлено сообщение для восстановления пароля" }
                : new DtoRecoveryPasswordResult { IsAccepted = false, Message = recoveryResult.ErrorMessage };
        }

        [Authorize]
        public void Logout()
        {
            _authServer.LogOut();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<DtoRegisterResult> Register(DtoUserRegiser model)
        {
            if (!ModelState.IsValid)
                return new DtoRegisterResult { IsAccepted = false, Message = GetModelStateErrors(ModelState) };
            var confirmationTokenResult = await _accountInternalApi.CreateUser("123456", new CreateUserDto
            {
                Email = model.Email,
                UserName = model.Name,
                RoleName = AuthDTOConstants.BuyerRoleName,
                PhoneNumber = model.Phone
            });
            if (confirmationTokenResult.IsSuccess)
            {
                var confirmString = ConfigurationManager.AppSettings["auth:ConfirmUrl"] + "?userId=" + confirmationTokenResult.UserId + "&token=" + confirmationTokenResult.Token;
                //прячем плюс для то что бы не кодировка не сбилась
                confirmString = confirmString.Replace("+", "PLUSCHAR");
                var sendEmailCmdHandler = _authFactory.CreateCommandHandler<SendEmailToUserCmd>();
                try
                {
                    await sendEmailCmdHandler.Execute(new SendEmailToUserCmd
                    {
                        UserId = confirmationTokenResult.UserId,
                        Subject = "Подтверждение регистрации",
                        Body = this.RenderRazorViewToString("~/Views/Account/ConfirmEmail.cshtml", new VMConfirmEmail { ConfirmUrl = new Uri(confirmString) })
                    });
                }
                catch (Exception ex)
                {
                    _logger.ErrorWithIncomeParameter(ex, model);
                    throw;
                }
                return new DtoRegisterResult { IsAccepted = true, Message = "Регистрация прошла успешно. Мы выслали Вам письмо, проверьте Вашу почту" };
            }
            var message = "";
            if (confirmationTokenResult.ErrorMessage == "Email '"+model.Email+"' is already taken.\r\n")
            {
                message = "Пользователь "+model.Email+" уже зарегистрирован";
            }
            return new DtoRegisterResult { IsAccepted = false, Message = message };
        }

        [HttpPost]
        [Authorize]
        public async Task<DtoChangeEmailResult> ChangeEmail(DtoChangeEmail model)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return new DtoChangeEmailResult { IsAccepted = false, Message = "Пользователь не найден. Обратитесь к администратору ресурса" };
            try
            {
                await _accountInternalApi.ChangeEmail(new DtoChangeEmailRequest { NewEmail = model.Email, UserId = Guid.Parse(userId) });
            }
            catch (Exception ex)
            {
                _logger.ErrorWithIncomeParameter(ex, model.Email);
                return new DtoChangeEmailResult { IsAccepted = false, Message = ex.Message };
            }
            DtoConfirmationTokenChangeEmailResult token = null;
            try
            {
                token = await _accountInternalApi.GetTokenForChangeEmail(new ChangeEmailDto { Email = model.Email });
            }
            catch (Exception ex)
            {
                _logger.ErrorWithIncomeParameter(ex, model.Email);
                return new DtoChangeEmailResult { IsAccepted = false, Message = ex.Message };
            }
            if (token.IsSuccess)
            {
                _authServer.LogOut();
                var confirmString = ConfigurationManager.AppSettings["auth:ConfirmChangeEmail"] + "?userId=" + userId + "&token=" + token.Token;
                //прячем плюс для то что бы не кодировка не сбилась
                confirmString = confirmString.Replace("+", "PLUSCHAR");
                var sendEmailCmdHandler = _authFactory.CreateCommandHandler<SendEmailToUserCmd>();
                try
                {
                    await sendEmailCmdHandler.Execute(new SendEmailToUserCmd
                    {
                        UserId = Guid.Parse(userId),
                        Subject = "Подтверждение изменения email",
                        Body = this.RenderRazorViewToString("~/Views/Account/ConfirmChangeEmail.cshtml", new VmChangeEmail { ConfirmUrl = new Uri(confirmString) })
                    });
                }
                catch (Exception ex)
                {
                    _logger.ErrorWithIncomeParameter(ex, model.Email);
                    throw;
                }
                return new DtoChangeEmailResult { IsAccepted = true, Message = "E-mail изменен. Мы выслали Вам письмо, проверьте Вашу почту" };
            }
            else
            {
                return new DtoChangeEmailResult { IsAccepted = true, Message = token.ErrorMessage };
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IHttpActionResult> GetConfirmRegistrData(string userId, string token)
        {
            if (string.IsNullOrEmpty(userId))
            {
                string error = "Не возможно подтвердить регистрацию, так как UserId пустой";
                _logger.Error(error);
                return BadRequest(error);
            }
            if (string.IsNullOrEmpty(token))
            {
                string error = "Не возможно подтвердить регистрацию, так как token пустой";
                _logger.Error(error);
                return BadRequest(error);
            }

            var user = await _userService.GetUserById(Guid.Parse(userId));
            return Ok(new DtoUserConfirmRegistration
            {
                Token = HttpUtility.HtmlDecode(token).Replace("PLUSCHAR", "+"),
                UserId = userId,
                Login = user.Email,
                PhoneNumber = user.PhoneNumber
            });
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<DtoRegisterResult> ConfirmRegistration(DtoUserConfirmRegistrationRequest model)
        {
            if (string.IsNullOrEmpty(model.Login)) /* если не указали логин, оставляем его емейлом */
            {
                var user = await _userService.GetUserById(model.UserId);
                model.Login = user.Email;
            }
            var res = await _accountInternalApi.ConfirmEmail(model);

            return res.IsSuccess
                ? new DtoRegisterResult { IsAccepted = true }
                : new DtoRegisterResult { IsAccepted = false, Message = res.ErrorMessage };
        }

        [HttpPost]
        [Authorize(Roles = AuthDTOConstants.SallerRoleName)]
        public async Task<DtoSellerVerificationResult> SellerVerification(DtoSellerVerification model)
        {
            if (!ModelState.IsValid)
                return new DtoSellerVerificationResult { IsAccepted = false, Message = GetModelStateErrors(ModelState) };

            var userId = User.Identity.GetUserId();
            if (userId == null)
                return new DtoSellerVerificationResult { IsVerified = false, Message = "Пользователь не найден. Обратитесь к администратору ресурса" };
            try
            {
                var handler = _authFactory.CreateCommandHandler<SetUserEmailVerifiedCmd>();
                await handler.Execute(new SetUserEmailVerifiedCmd
                {
                    UserId = Guid.Parse(userId),
                    Email = model.Email
                });
                return new DtoSellerVerificationResult { IsVerified = true, Message = "Вы успешно верифицировали свой E-mail" };
            }
            catch (Exception ex)
            {
                return new DtoSellerVerificationResult { IsVerified = false, Message = ex.Message };
            }
        }

        [HttpPost]
        [AllWithoutSellerAndAnonym]
        public async Task<IHttpActionResult> RestorePassword(DtoRestorePasswordRequest restorePass)
        {
            if (!ModelState.IsValid)
                return BadRequest(GetModelStateErrors(ModelState));

            Guid userId = new Guid(User.Identity.GetUserId());
            try
            {
                await _accountInternalApi.RestorePassword(restorePass.ToRestoreCmdConverter(userId));
            }
            catch (UserDoesNotExistException ex)
            {
                _logger.Error(ex);
                return BadRequest(ex.Message);
            }
            catch (UserDoesNotCanUpdatedException ex)
            {
                _logger.Error(ex);
                return BadRequest(ex.Message);
            }
            catch(Exception ex)
            {
                _logger.Error(ex);
                return BadRequest("Произошла ошибка при смене пароля");
            }
            Logout();
            var result = await LoginByUserId(new DtoUserAuthByUserId
            {
                Password = restorePass.NewPassword,
                ReturnUrl = string.Empty,
                UserID = userId
            });

            if (!result.IsAuthentificate)
                return BadRequest(result.Message);
            return Ok();
        }

        private DtoUserAuthResult ParseLoginResult(LoginResult result, string returnUrl)
        {
            switch (result.LoginStatus)
            {
                case SignInStatus.Success:
                    if (result.LoginInfo.UserRoles.Any(x => x.IsRegion()))
                        return new DtoUserAuthResult { IsAuthentificate = true, ReturnUrl = returnUrl, IsRegion = true };
                    return new DtoUserAuthResult { IsAuthentificate = true, ReturnUrl = returnUrl };
                case SignInStatus.Failure:
                    return
                        new DtoUserAuthResult
                        {
                            IsAuthentificate = false,
                            Message = result.Error,
                            ReturnUrl = returnUrl
                        };

                //TODO не реализованно на бекенде AUTH
                case SignInStatus.LockedOut:
                    return new DtoUserAuthResult { IsAuthentificate = false, Message = "Пользователь заблокирован." };
                case SignInStatus.RequiresVerification:
                    return new DtoUserAuthResult { IsAuthentificate = true, ReturnUrl = returnUrl, RequiresVerification = true };
            }
            return new DtoUserAuthResult { IsAuthentificate = false, Message = "Не удаётся зайти." };
        }

        /// <summary>
        /// Получение информации по авторизованному пользователю.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public IHttpActionResult CurrentUser()
        {
            var userRoles = _authServer.GetCurrentUserRoles();
            VmUserInformation uinfo = new VmUserInformation
            {
                Roles = userRoles
            };
            return Ok(uinfo);
        }

        [HttpGet]
        [Route("userexist")]
        public async Task<IHttpActionResult> GetUserExist([FromUri]VmUserExistRequest request)
        {
            var result = new VmUserExist { Exist = false };
            var user = await _userService.GetUserByName(request.UserName);
            if (user != null)
            {
                result.Exist = true;
                result.Message = "Пользователь с таким логином уже существует";
            }
            user = await _userService.GetUserByPhone(request.Phone);
            if (user != null)
            {
                result.Exist = true;
                result.Message = !string.IsNullOrEmpty(result.Message) ? result.Message + Environment.NewLine + "Пользователь с таким телефоном уже существует" : "Пользователь с таким телефоном уже существует";
            }
            return Ok(result);
        }

        private static string GetModelStateErrors(ModelStateDictionary modelState)
        {
            var message = modelState.ToDictionary(p => p.Key, p => p.Value.Errors.Select(e => e.ErrorMessage))
                  .Where(p => !string.IsNullOrWhiteSpace(p.Key))
                  .Select(error => error.Value.ToList())
                  .Aggregate("", (current1, e) => e.Aggregate(current1, (current, str) => $"{current}{str};"));
            return message;
        }

    }
}
