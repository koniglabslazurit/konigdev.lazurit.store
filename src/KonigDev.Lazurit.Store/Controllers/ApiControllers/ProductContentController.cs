﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Products;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Query;
using KonigDev.Lazurit.Store.Dto.Common;
using KonigDev.Lazurit.Store.Dto.Product;
using KonigDev.Lazurit.Store.Extensions.Filters;
using KonigDev.Lazurit.Store.Services;
using KonigDev.Lazurit.Store.ViewModels.ProductViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Store.Dto.Collection;
using KonigDev.Lazurit.Store.Models.Localization;
using KonigDev.Lazurit.Store.Models.Prices;
using KonigDev.Lazurit.Store.Services.CookieService;
using KonigDev.Lazurit.Store.Services.LazuritContext;
using KonigDev.Lazurit.Store.Services.PriceSrvice;
using Microsoft.AspNet.Identity;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [NotImplExceptionFilter]
    public class ProductContentController : ApiController
    {
        private readonly IHubHandlersFactory _hubFactory;
        private readonly IPriceService _priceService;
        private readonly ILazuritCtxSessionService _lazuritCtxService;

        public ProductContentController(IHubHandlersFactory hubFactory, IPriceService priceService, ILazuritCtxSessionService lazuritCtxService)
        {
            _hubFactory = hubFactory;
            _priceService = priceService;
            _lazuritCtxService = lazuritCtxService;
        }

        [HttpGet]
        public async Task<List<VmProductContentWithPrices>> List([FromUri]DtoCollectionVariantModel model)
        {
            var getProductContextByCollectionVariantQueryHandler = _hubFactory.CreateQueryHandler<GetProductsContextByCollectionVariantQuery, List<DtoProductContentResult>>();
            var productContext = await getProductContextByCollectionVariantQueryHandler.Execute(new GetProductsContextByCollectionVariantQuery { CollectionVariantId = model.CollectionVariantId, UserId = User.Identity.IsAuthenticated ? Guid.Parse(User.Identity.GetUserId()) : (Guid?)null });

            var result = productContext.Select(p => new VmProductContentWithPrices
            {
                Article = p.Article,
                Height = p.Height,
                IsEnabled = p.IsEnabled,
                Left = p.Left,
                ObjectId = p.ObjectId,
                Id = p.Id,
                Top = p.Top,
                Width = p.Width
            }).ToList();

            var priceInfo = _lazuritCtxService.GetPriceInfoFromContext();
            result = await _priceService.GetProductsPrices(priceInfo.PriceZoneId, priceInfo.Currency, result);

            return result;
        }

        [HttpPost]

        public DtoResponse Add([FromBody] DtoProductContentAdding request)
        {
            var Id = Guid.NewGuid();
            var createProductContextCommandHandler = _hubFactory.CreateCommandHandler<CreateProductContentCommand>();
            createProductContextCommandHandler.Execute(new CreateProductContentCommand
            {
                Id = Id,
                ProductId = request.ProductId,
                CollectionVariantId = request.CollectionVariantId
            });
            return new DtoResponse { IsValid = true, Message = Id.ToString() };
        }

        [HttpPut]

        public DtoResponse Update([FromBody] DtoProductContentUpdating request)
        {
            var updateProductContextCommandHandler = _hubFactory.CreateCommandHandler<UpdateProductContentCommand>();
            updateProductContextCommandHandler.Execute(new UpdateProductContentCommand
            {
                Id = request.Id,
                Left = request.Left,
                Top = request.Top,
                Width = request.Width,
                Height = request.Height,
            });
            return new DtoResponse { IsValid = true };
        }

        [HttpPost]

        public DtoResponse AddIntegration([FromBody] DtoProductContentAdding request)
        {
            var Id = Guid.NewGuid();
            var createIntegrationProductContextCommandHandler = _hubFactory.CreateCommandHandler<CreateIntegrationProductContentCommand>();
            createIntegrationProductContextCommandHandler.Execute(new CreateIntegrationProductContentCommand
            {
                Id = Id,
                ProductId = request.ProductId,
                IntegrationCollectionVariantId = request.CollectionVariantId
            });
            return new DtoResponse { IsValid = true, Message = Id.ToString() };
        }

        [HttpPut]

        public DtoResponse UpdateIntegration([FromBody] DtoProductContentUpdating request)
        {
            var updateIntegrationProductContextCommandHandler = _hubFactory.CreateCommandHandler<UpdateIntegrationProductContentCommand>();
            updateIntegrationProductContextCommandHandler.Execute(new UpdateIntegrationProductContentCommand
            {
                Id = request.Id,
                Left = request.Left,
                Top = request.Top,
                Width = request.Width,
                Height = request.Height,
            });
            return new DtoResponse { IsValid = true };
        }

    }
}
