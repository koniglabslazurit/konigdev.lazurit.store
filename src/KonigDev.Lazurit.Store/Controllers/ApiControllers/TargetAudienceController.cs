﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Query;
using KonigDev.Lazurit.Store.Extensions.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [NotImplExceptionFilter]
    public class TargetAudienceController : ApiController
    {
        private readonly IHubHandlersFactory _hubFactory;

        public TargetAudienceController(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        [HttpGet]
        public async Task<List<DtoTargetAudienceItem>> GetStyles()
        {
            var getTargetAudienceHandler = _hubFactory.CreateQueryHandler<GetTargetAudiencesQuery, List<DtoTargetAudienceItem>>();
            var targetAudiences = await getTargetAudienceHandler.Execute(new GetTargetAudiencesQuery());
            return targetAudiences;
        }
    }
}
