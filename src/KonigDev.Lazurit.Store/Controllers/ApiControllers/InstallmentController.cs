﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Article.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Article.Query;
using KonigDev.Lazurit.Store.Extensions.Filters;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using KonigDev.Lazurit.Hub.Model.DTO.Installment.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Installment.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Installment.Dto;
using System;
using KonigDev.Lazurit.Store.Providers;
using KonigDev.Lazurit.Store.Services.LazuritContext;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [NotImplExceptionFilter]
    [RoutePrefix("api/installment")]
    public class InstallmentController : ApiController
    {
        private readonly IHubHandlersFactory _hubFactory;
        private readonly ICartProvider _cartProvider;
        private readonly ILazuritCtxSessionService _lazuritCtxService;

        public InstallmentController(IHubHandlersFactory hubFactory, ICartProvider cartProvider, ILazuritCtxSessionService lazuritCtxService)
        {
            _hubFactory = hubFactory;
            _cartProvider = cartProvider;
            _lazuritCtxService = lazuritCtxService;
        }

        [HttpGet]
        [Route("")]
        public async Task<DtoInstallmentTableResponse> Get([FromUri] GetInstallmentQuery query)
        {
            return await _hubFactory.CreateQueryHandler<GetInstallmentQuery, DtoInstallmentTableResponse>().Execute(query);
        }

        [HttpDelete]
        [Route("")]
        public async Task<IHttpActionResult> Delete(Guid id)
        {
            DeleteInstallmentCommand command = new DeleteInstallmentCommand {Id = id };
            await _hubFactory.CreateCommandHandler<DeleteInstallmentCommand>().Execute(command);
            /*обновить кэш в малой корзине*/
            var cartId = _lazuritCtxService.GetSingleCartId();
            var priceInfo = _lazuritCtxService.GetPriceInfoFromContext();
            await _cartProvider.ForOnlyMiniCart().UpdateCart(cartId, priceInfo.Currency, priceInfo.PriceZoneId, priceInfo.DeliveryPriceForCity);
            return Ok();
        }

        [HttpPost]
        [Route("vendorcode")]
        public async Task<List<DtoVendorCodeItem>> GetVendorCodes([FromBody] GetVendorCodesForInstallmentQuery query)
        {
            return await _hubFactory.CreateQueryHandler<GetVendorCodesForInstallmentQuery, List<DtoVendorCodeItem>>().Execute(query);
        }

        [HttpPost]
        [Route("")]
        public async Task<IHttpActionResult> Create([FromBody] CreateInstallmentCommand command)
        {
            await _hubFactory.CreateCommandHandler<CreateInstallmentCommand>().Execute(command);
            /*обновить кэш в малой корзине*/
            var cartId = _lazuritCtxService.GetSingleCartId();
            var priceInfo = _lazuritCtxService.GetPriceInfoFromContext();
            await _cartProvider.ForOnlyMiniCart().UpdateCart(cartId, priceInfo.Currency, priceInfo.PriceZoneId, priceInfo.DeliveryPriceForCity);
            return Ok();
        }
    }
}