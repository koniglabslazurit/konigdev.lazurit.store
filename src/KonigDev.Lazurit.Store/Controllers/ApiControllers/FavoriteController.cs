﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using KonigDev.Lazurit.Store.Extensions.Filters;
using Microsoft.AspNet.Identity;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Favorite;
using KonigDev.Lazurit.Hub.Model.DTO.Favorite.Command;
using KonigDev.Lazurit.Core.Enums;
using KonigDev.Lazurit.Hub.Model.DTO.Favorite.Query;
using KonigDev.Lazurit.Store.Dto.Favorite;
using KonigDev.Lazurit.Hub.Model.DTO.Users;
using KonigDev.Lazurit.Hub.Model.DTO.Users.Queries;
using KonigDev.Lazurit.Store.Services.PriceSrvice;
using AutoMapper;
using KonigDev.Lazurit.Store.Dto.Favorite.FavoriteItemsWithPrices;
using System.Collections.Generic;
using KonigDev.Lazurit.Store.Services.LazuritContext;
using System.Linq;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [AllowAnonymous]
    [NotImplExceptionFilter]
    [RoutePrefix("api/favorite")]
    public class FavoriteController : ApiController
    {
        private readonly IHubHandlersFactory _hubFactory;
        private readonly IPriceService _priceService;
        private readonly ILazuritCtxSessionService _lazuritCtxService;

        public FavoriteController(IHubHandlersFactory hubFactory, IPriceService priceService, ILazuritCtxSessionService lazuritCtxService)
        {
            _hubFactory = hubFactory;
            _priceService = priceService;
            _lazuritCtxService = lazuritCtxService;
        }

        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> GetCurrentFavorite()
        {
            Guid userId;
            if (!Guid.TryParse(User?.Identity?.GetUserId(), out userId))
                return null;
            else
            {
                var favoriteWithPrices = await GetCurrentFavorite(userId);
                

                return Ok(favoriteWithPrices);
            }
        }

        [HttpGet]
        [Route("shareLinkId")]
        public async Task<IHttpActionResult> GetShareLinkId()
        {
            Guid userId;
            if (!Guid.TryParse(User?.Identity?.GetUserId(), out userId))
                return BadRequest(Core.Enums.Constants.Exceptions.User.RequiresRegistration);
            var getShareLinkIdHandler = _hubFactory.CreateQueryHandler<GetShareIdByUserIdQuery, DtoShareId>();
            var shareLink = await getShareLinkIdHandler.Execute(new GetShareIdByUserIdQuery
            {
                UserId = userId
            });
            return Ok(shareLink);
        }

        [HttpGet]
        [Route("getFavoriteByLinkId")]
        public async Task<IHttpActionResult> GetFavoriteByLinkId([FromUri]DtoGetFavoriteRequest request)
        {
            Guid userId;
            if (request.IsUserOwner)
            {
                if (!Guid.TryParse(User?.Identity?.GetUserId(), out userId))
                    return null;
                else
                {
                    var favoriteWithPrices = await GetCurrentFavorite(userId);
                    return Ok(favoriteWithPrices);
                }
            }
            else
            {
                var anotherUserProfile = await _hubFactory.CreateQueryHandler<GetUserProfileByLinkIdQuery, DtoUserProfile>().Execute(new GetUserProfileByLinkIdQuery
                {
                    LinkId = request.LinkId
                });

                if (anotherUserProfile == null)
                    return null;
                else
                {
                    var favoriteWithPrices = await GetCurrentFavorite(anotherUserProfile.Id);
                    return Ok(favoriteWithPrices);
                }
            }
        }

        #region Product
        [HttpPost]
        [Route("product")]
        public async Task<IHttpActionResult> AddProductToFavorite([FromBody]DtoAddProductToFavoriteRequest product)
        {
            Guid userId;
            if (!Guid.TryParse(User?.Identity?.GetUserId(), out userId))
                return BadRequest(Core.Enums.Constants.Exceptions.User.RequiresRegistration);
            try
            {
                await _hubFactory.CreateCommandHandler<AddFavoriteItemCommand>().Execute(new AddFavoriteItemCommand
                {
                    AddElementId = product.ProductId,
                    FavoriteItemType = EnumFavoriteItemType.Product,
                    UserId = userId
                });
            }
            catch (Core.Enums.Exeptions.Favorite.NotValidExeption ex)
            {
                return BadRequest(ex.Message);
            }

            var favorite = await GetCurrentFavorite(userId);
            return Ok(favorite);
        }
        [HttpDelete]
        [Route("product/{productId}")]
        public async Task<IHttpActionResult> DeleteProductFromFavorite(Guid productId)
        {
            Guid userId;
            if (!Guid.TryParse(User?.Identity?.GetUserId(), out userId))
                return null;
            try
            {
                await _hubFactory.CreateCommandHandler<RemoveFavoriteItemCommand>().Execute(new RemoveFavoriteItemCommand
                {
                    FavoriteItemType = EnumFavoriteItemType.Product,
                    RemoveProductId = productId,
                    UserId = userId
                });
            }
            catch (Core.Enums.Exeptions.Favorite.NotValidExeption ex)
            {
                return BadRequest(ex.Message);
            }
            var favorite = await GetCurrentFavorite(userId);
            return Ok(favorite);
        }
        #endregion

        #region Collection
        [HttpPost]
        [Route("collection")]
        public async Task<IHttpActionResult> AddCollectionToFavorite([FromBody]DtoAddCollectionToFavoriteRequest collection)
        {
            Guid userId;
            if (!Guid.TryParse(User?.Identity?.GetUserId(), out userId))
                return BadRequest(Core.Enums.Constants.Exceptions.User.RequiresRegistration);
            try
            {
                await _hubFactory.CreateCommandHandler<AddFavoriteItemCommand>().Execute(new AddFavoriteItemCommand
                {
                    AddElementId = collection.CollectionId,
                    FavoriteItemType = EnumFavoriteItemType.Collection,
                    UserId = userId
                });
            }
            catch (Core.Enums.Exeptions.Favorite.NotValidExeption ex)
            {
                return BadRequest(ex.Message);
            }
            var favorite = await GetCurrentFavorite(userId);
            return Ok(favorite);
        }
        [HttpDelete]
        [Route("collection/{productId}/{collectionId}")]
        public async Task<IHttpActionResult> DeleteItemCollectionFromFavorite(Guid productId, Guid collectionId)
        {
            Guid userId;
            if (!Guid.TryParse(User?.Identity?.GetUserId(), out userId))
                return null;
            try
            {
                await _hubFactory.CreateCommandHandler<RemoveFavoriteItemCommand>().Execute(new RemoveFavoriteItemCommand
                {
                    FavoriteItemType = EnumFavoriteItemType.ProductInCollection,
                    RemoveProductId = productId,
                    RemoveCollectionId = collectionId,
                    UserId = userId
                });
            }
            catch (Core.Enums.Exeptions.Favorite.NotValidExeption ex)
            {
                return BadRequest(ex.Message);
            }
            var favorite = await GetCurrentFavorite(userId);
            return Ok(favorite);
        }
        [HttpDelete]
        [Route("collection/full/{collectionId}")]
        public async Task<IHttpActionResult> DeleteFullCollectionFromFavorite(Guid collectionId)
        {
            Guid userId;
            if (!Guid.TryParse(User?.Identity?.GetUserId(), out userId))
                return null;
            try
            {
                await _hubFactory.CreateCommandHandler<RemoveFavoriteItemCommand>().Execute(new RemoveFavoriteItemCommand
                {
                    FavoriteItemType = EnumFavoriteItemType.Collection,
                    RemoveCollectionId = collectionId,
                    UserId = userId
                });
            }
            catch (Core.Enums.Exeptions.Favorite.NotValidExeption ex)
            {
                return BadRequest(ex.Message);
            }
            var favorite = await GetCurrentFavorite(userId);
            return Ok(favorite);
        }
        #endregion

        #region Complect
        [HttpPost]
        [Route("complect")]
        public async Task<IHttpActionResult> AddComplectToFavorite([FromBody]DtoAddComplectToFavoriteRequest complect)
        {
            Guid userId;
            if (!Guid.TryParse(User?.Identity?.GetUserId(), out userId))
                return BadRequest(Core.Enums.Constants.Exceptions.User.RequiresRegistration);
            try
            {
                await _hubFactory.CreateCommandHandler<AddFavoriteItemCommand>().Execute(new AddFavoriteItemCommand
                {
                    AddElementId = complect.CollectionId,
                    FavoriteItemType = EnumFavoriteItemType.Complect,
                    UserId = userId
                });
            }
            catch (Core.Enums.Exeptions.Favorite.NotValidExeption ex)
            {
                return BadRequest(ex.Message);
            }
            var favorite = await GetCurrentFavorite(userId);
            return Ok(favorite);
        }
        [HttpDelete]
        [Route("complect/{complectId}")]
        public async Task<IHttpActionResult> DeleteItemComplectFromFavorite(Guid complectId)
        {
            Guid userId;
            if (!Guid.TryParse(User?.Identity?.GetUserId(), out userId))
                return null;
            try
            {
                await _hubFactory.CreateCommandHandler<RemoveFavoriteItemCommand>().Execute(new RemoveFavoriteItemCommand
                {
                    FavoriteItemType = EnumFavoriteItemType.Complect,
                    RemoveCollectionId = complectId,
                    UserId = userId
                });
            }
            catch (Core.Enums.Exeptions.Favorite.NotValidExeption ex)
            {
                return BadRequest(ex.Message);
            }
            var favorite = await GetCurrentFavorite(userId);
            return Ok(favorite);
        }
        #endregion

        private async Task<DtoFavoriteWithPrices> GetCurrentFavorite(Guid userId)
        {
            var favorite = await _hubFactory.CreateQueryHandler<GetFavoriteByUserQuery, DtoFavorite>().Execute(new GetFavoriteByUserQuery
            {
                UserId = userId
            });

            var favoriteWithPrices = Mapper.Map<DtoFavoriteWithPrices>(favorite);

            var priceInfo = _lazuritCtxService.GetPriceInfoFromContext();

            await _priceService.GetProductsPrices(priceInfo.PriceZoneId, priceInfo.Currency, favoriteWithPrices.Products.Select(p => p.Product).ToList());
            await _priceService.GetProductsPrices(priceInfo.PriceZoneId, priceInfo.Currency, favoriteWithPrices.Collections.SelectMany(c => c.Products).Select(p => p.Product).ToList());
            await _priceService.GetComplectPrices(priceInfo.PriceZoneId, priceInfo.Currency, favoriteWithPrices.Complects.Select(p => p.CollectionVariant).ToList());

            return favoriteWithPrices;
        }
    }
}