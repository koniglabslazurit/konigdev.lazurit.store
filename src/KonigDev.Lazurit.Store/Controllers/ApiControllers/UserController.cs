﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Users;
using KonigDev.Lazurit.Hub.Model.DTO.Users.Queries;
using KonigDev.Lazurit.Store.Extensions.Filters;
using AuthDTOConstants = KonigDev.Lazurit.Auth.Model.DTO.Constants;
using KonigDev.Lazurit.Store.ViewModels.CommonViewModels;
using KonigDev.Lazurit.Core.Enums.Constants.Message;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.DTO.Common;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Roles;
using KonigDev.Lazurit.Store.Dto.UserRole;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [RoutePrefix("api/users")]
    [NotImplExceptionFilter]
    [Authorize]
    public class UserController : ApiController
    {
        private readonly IHubHandlersFactory _hubFactory;
        private readonly IAuthHandlersFactory _authFactory;

        public UserController(IHubHandlersFactory hubFactory, IAuthHandlersFactory authFactory)
        {
            _hubFactory = hubFactory;
            _authFactory = authFactory;
        }

        [HttpGet]
        [Authorize(Roles = AuthDTOConstants.AdminRoleName)]
        [Route("")]
        public async Task<IEnumerable<DtoUserProfile>> GetUsers(Guid showroomId)
        {
            var getUserProfilesQueryHandler = _hubFactory.CreateQueryHandler<GetUsersByShowRoomIdQuery, List<DtoUserProfile>>();
            var result = await getUserProfilesQueryHandler.Execute(new GetUsersByShowRoomIdQuery { ShowRoomId = showroomId });

            return result;
        }

        [HttpDelete]
        [Authorize(Roles = AuthDTOConstants.AdminRoleName)]
        [Route("")]
        public async Task<IHttpActionResult> DeleteUsers(Guid userId)
        {
            var deleteUserCommandHandler = _authFactory.CreateCommandHandler<DeleteUserCmd>();
            await deleteUserCommandHandler.Execute(new DeleteUserCmd { UserId = userId});

            return Ok();
        }

        /// <summary>
        /// Получение списка ролей продавцов: ИМ и салона
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("role/seller")]
        [Authorize(Roles = AuthDTOConstants.AdminRoleName)]
        [AllowAnonymous]
        public IHttpActionResult SellerRoles()
        {
            var result = new List<VmSelectItem>
            {
                new VmSelectItem {Id = AuthDTOConstants.SalonSallerRoleName, Name = Role.SalonSallerRoleName },
                new VmSelectItem {Id = AuthDTOConstants.ShopSallerRoleName, Name = Role.ShopSallerRoleName }
            };
            return Ok(result);
        }

        /// <summary>
        /// Получение списка ролей админ, контент менеджер, маркетолог
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("role/administrative")]
        [Authorize(Roles = AuthDTOConstants.AdminRoleName)]
        [AllowAnonymous]
        public IHttpActionResult OfficeRoles()
        {
            var result = new List<VmSelectItem>
            {
                new VmSelectItem {Id = AuthDTOConstants.AdminRoleName, Name = Role.AdminRoleName },
                new VmSelectItem {Id = AuthDTOConstants.ContentManagerRoleName, Name = Role.ContentManagerRoleName },
                new VmSelectItem {Id = AuthDTOConstants.MarketerRoleName, Name = Role.MarketerRoleName }
            };
            return Ok(result);
        }
        
        [HttpGet]
        [Authorize(Roles = AuthDTOConstants.AdminRoleName)]
        [Route("by-roles")]
        public async Task<IHttpActionResult> GetUserByRoles([FromUri]GetUsersByRequestQuery query)
        {
            var getUsersQueryHandler = _authFactory.CreateQueryHandler<GetUsersByRequestQuery, DtoCommonTableResponse<DtoUserItem>>();
            var users = await getUsersQueryHandler.Execute(query);
            return Ok(users);
        }

        [HttpPut]
        [Authorize(Roles = AuthDTOConstants.AdminRoleName)]
        [Route("role")]
        public async Task<IHttpActionResult> ChangeRole([FromBody]List<UpdateUserRoleQuery> query)
        {
            var changeRoleCommand = _authFactory.CreateCommandHandler<UpdateUserRoleCmd>();            
            foreach (var item in query)
            {
                await changeRoleCommand.Execute(new UpdateUserRoleCmd { UserId = item.UserId, Role = item.Role });
            }
            return Ok();
        }
    }
}
