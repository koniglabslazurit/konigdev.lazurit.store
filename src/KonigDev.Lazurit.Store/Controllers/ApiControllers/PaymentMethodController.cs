﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.PaymentMethods;
using KonigDev.Lazurit.Hub.Model.DTO.PaymentMethods.Query;
using KonigDev.Lazurit.Store.Extensions.Filters;
using KonigDev.Lazurit.Store.Services.CustomRequest;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using KonigDev.Lazurit.Store.Dto.Payment;
using System.IO;
using Newtonsoft.Json;
using System.Text;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [NotImplExceptionFilter]
    [RoutePrefix("api/payment-methods")]
    [AllowAnonymous]
    public class PaymentMethodController : ApiController
    {
        private readonly IHubHandlersFactory _hubFactory;
        private readonly ICustomRequestService _requestService;
        private const string getRoboPaymentMethodsUrl = "https://auth.robokassa.ru/Merchant/WebService/Service.asmx/GetCurrencies?MerchantLogin=qademostorefront&Language=ru";

        public PaymentMethodController(IHubHandlersFactory hubFactory, ICustomRequestService requestService)
        {
            _hubFactory = hubFactory;
            _requestService = requestService;
        }

        [HttpGet]
        [Route("")]
        public async Task<IList<DtoPaymentMethod>> GetPaymentMethods()
        {
            var getPaymentMethodsQueryHandler = _hubFactory.CreateQueryHandler<GetPaymentMethodsQuery, List<DtoPaymentMethod>>();
            return await getPaymentMethodsQueryHandler.Execute(new GetPaymentMethodsQuery());
        }

        //[HttpGet]
        //[Route("robo")]
        //public IEnumerable<DtoRoboPaymentMethod> GetRoboPaymentMethods()
        //{
        //    //возвращает все доступные способы оплаты через робокассу
        //    //var xmlPaymentTypes = _requestService.CreateRequest(getRoboPaymentMethodsUrl);
        //    return new List<DtoRoboPaymentMethod> {
        //        new DtoRoboPaymentMethod
        //        {
        //            TechName ="BankCard",
        //            IncCurrLabel = "BankSberBank"
        //        },
        //        new DtoRoboPaymentMethod {
        //            TechName ="QiwiWallet",
        //            IncCurrLabel = "QiwiWallet"
        //        }
        //    };
        //}
    }
}