﻿using System.Collections.Generic;
using System.Web.Http;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Layaways;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Layaways.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Layaways.Command;
using System;
using KonigDev.Lazurit.Store.Extensions.Filters;
using KonigDev.Lazurit.Store.Providers;
using Microsoft.AspNet.Identity;
using KonigDev.Lazurit.Store.Services.LazuritContext;
using KonigDev.Lazurit.Store.Services.PriceSrvice;
using AutoMapper;
using System.Linq;
using KonigDev.Lazurit.Store.Dto.Layaway;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [AllowAnonymous]
    [NotImplExceptionFilter]
    public class LayawayController : ApiController
    {
        private readonly IHubHandlersFactory _hubFactory;
        private readonly ICartProvider _cartProvider;
        private readonly ILazuritCtxSessionService _lazuritCtxService;
        private readonly IPriceService _priceService;

        public LayawayController(IHubHandlersFactory hubFactory, ICartProvider cartProvider, ILazuritCtxSessionService lazuritCtxService, IPriceService priceService)
        {
            _hubFactory = hubFactory;
            _cartProvider = cartProvider;
            _lazuritCtxService = lazuritCtxService;
            _priceService = priceService;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<List<DtoLayawayWithPrices>> GetLayaway()
        {
            List<DtoLayaway> result;
            if (User.Identity.IsAuthenticated)
            {
                var userId = new Guid(User.Identity.GetUserId());

                var getLayawayHandler = _hubFactory.CreateQueryHandler<GetLayawaysByUserQuery, List<DtoLayaway>>();
                result = await getLayawayHandler.Execute(new GetLayawaysByUserQuery { UserId = userId });
            }
            else
            {
                var layawayIds = _lazuritCtxService.GetLayawaytIds();

                if (layawayIds != null && layawayIds.Count != 0)
                {
                    var getLayawayHandler = _hubFactory.CreateQueryHandler<GetLayawaysByLayawaysIdsQuery, List<DtoLayaway>>();
                    result = await getLayawayHandler.Execute(new GetLayawaysByLayawaysIdsQuery { LayawayIds = layawayIds });
                }
                else
                {
                    return new List<DtoLayawayWithPrices>();
                }

            }
            if (result != null)
            {
                var model = Mapper.Map<List<DtoLayawayWithPrices>>(result);

                var priceInfo = _lazuritCtxService.GetPriceInfoFromContext();

                await _priceService.GetProductsPrices(priceInfo.PriceZoneId, priceInfo.Currency, model.SelectMany(p => p.Products).Select(p => p.Product).ToList());
                await _priceService.GetProductsPrices(priceInfo.PriceZoneId, priceInfo.Currency, model.SelectMany(p => p.Collections).SelectMany(c => c.ProductsInCollection).Select(p => p.Product).ToList());
                await _priceService.GetComplectPrices(priceInfo.PriceZoneId, priceInfo.Currency, model.SelectMany(p => p.Complects).ToList());

                return model;
            }
            return new List<DtoLayawayWithPrices>();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task MoveToLayaway([FromBody] AddLayawayItemCommand addLayawayCommand)
        {
            if (addLayawayCommand == null)
                return;
            var layawayIds = _lazuritCtxService.GetLayawaytIds();
            addLayawayCommand.LayawayIds = layawayIds;

            var cartId = _lazuritCtxService.GetSingleCartId();
            addLayawayCommand.CartId = cartId;

            Guid newLayawayId = Guid.NewGuid();
            addLayawayCommand.NewLayawayId = newLayawayId;

            if (User.Identity.IsAuthenticated)
            {
                var userId = new Guid(User.Identity.GetUserId());
                addLayawayCommand.UserId = userId;
            }

            var addLayawayHandler = _hubFactory.CreateCommandHandler<AddLayawayItemCommand>();
            await addLayawayHandler.Execute(addLayawayCommand);
            if (!User.Identity.IsAuthenticated)
            {
                layawayIds.Add(newLayawayId);
                //todo лишний запрос в базу, чтобы перезаписать куки по существующим отложенным. надо подумать, как избежать лишнее действие.
                var layawaysList = await _hubFactory.CreateQueryHandler<GetLayawaysByLayawaysIdsQuery, List<DtoLayaway>>().Execute(new GetLayawaysByLayawaysIdsQuery { LayawayIds = layawayIds });
                _lazuritCtxService.AddLayawaysCookie(layawaysList.Select(l => l.Id).ToList());
            }

            var priceInfo = _lazuritCtxService.GetPriceInfoFromContext();
            await _cartProvider.ForOnlyMiniCart().UpdateCart(_lazuritCtxService.GetSingleCartId(), priceInfo.Currency, priceInfo.PriceZoneId, priceInfo.DeliveryPriceForCity);
        }

        [AllowAnonymous]
        [HttpPut]
        public async Task MoveFromLayaway([FromBody] RemoveLayawayItemCommand removeLayawayCommand)
        {
            if (removeLayawayCommand == null)
                return;

            var cartId = _lazuritCtxService.GetSingleCartId();
            removeLayawayCommand.CartId = cartId;
            var removeLayawayHandler = _hubFactory.CreateCommandHandler<RemoveLayawayItemCommand>();
            await removeLayawayHandler.Execute(removeLayawayCommand);

            var layawayIds = _lazuritCtxService.GetLayawaytIds();
            var layawaysList = await _hubFactory.CreateQueryHandler<GetLayawaysByLayawaysIdsQuery, List<DtoLayaway>>().Execute(new GetLayawaysByLayawaysIdsQuery { LayawayIds = layawayIds });
            _lazuritCtxService.AddLayawaysCookie(layawaysList.Select(l => l.Id).ToList());

            var priceInfo = _lazuritCtxService.GetPriceInfoFromContext();
            await _cartProvider.ForOnlyMiniCart().UpdateCart(_lazuritCtxService.GetSingleCartId(), priceInfo.Currency, priceInfo.PriceZoneId, priceInfo.DeliveryPriceForCity);
        }

        [AllowAnonymous]
        [HttpDelete]
        public async Task DeleteFromLayaway([FromUri] DeleteLayawayItemCommand deleteLayawayCommand)
        {
            if (deleteLayawayCommand == null)
                return;

            var deleteLayawayHandler = _hubFactory.CreateCommandHandler<DeleteLayawayItemCommand>();
            await deleteLayawayHandler.Execute(deleteLayawayCommand);

            var layawayIds = _lazuritCtxService.GetLayawaytIds();
            var layawaysList = await _hubFactory.CreateQueryHandler<GetLayawaysByLayawaysIdsQuery, List<DtoLayaway>>().Execute(new GetLayawaysByLayawaysIdsQuery { LayawayIds = layawayIds });
            _lazuritCtxService.AddLayawaysCookie(layawaysList.Select(l => l.Id).ToList());
        }
    }
}
