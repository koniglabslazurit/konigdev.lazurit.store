﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Facings;
using KonigDev.Lazurit.Hub.Model.DTO.Facings.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Facings.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Filters;
using KonigDev.Lazurit.Hub.Model.DTO.Filters.Query;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Query;
using KonigDev.Lazurit.Store.Dto.Filters;
using KonigDev.Lazurit.Store.Extensions.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [NotImplExceptionFilter]
    public class FilterController : ApiController
    {
        private readonly IHubHandlersFactory _hubFactory;

        public FilterController(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        [HttpGet]
        [ActionName("filters")]
        public DtoFiltersResult GetFilters([FromUri]DtoGetFiltersRequest request)
        {
            var getFacingsByRequest = _hubFactory.CreateQueryHandler<GetFacingsByCollectionQuery, List<DtoFacingsResult>>();
            var getFacingColorsByRequest = _hubFactory.CreateQueryHandler<GetFacingColorsByCollectionQuery, List<DtoFacingColorResult>>();
            var getFrameColorsByRequest = _hubFactory.CreateQueryHandler<GetFrameColorsByCollectionQuery, List<DtoFrameColorResult>>();
            var facings = new List<DtoFacingsResult>();
            var facingcolors = new List<DtoFacingColorResult>();
            var framecolors = new List<DtoFrameColorResult>();
            if (request != null && request.CollectionId != null && request.CollectionId != Guid.Empty)
            {
                facings = getFacingsByRequest.Execute(new GetFacingsByCollectionQuery { CollectionId = request.CollectionId }).Result;
                facingcolors = getFacingColorsByRequest.Execute(new GetFacingColorsByCollectionQuery { CollectionId = request.CollectionId }).Result;
                framecolors = getFrameColorsByRequest.Execute(new GetFrameColorsByCollectionQuery { CollectionId = request.CollectionId }).Result;
            }

            return new DtoFiltersResult { FacingColors = facingcolors, Facings = facings, FrameColors = framecolors };
        }

        [HttpGet]
        [ActionName("single-product")]
        public async Task<DtoFilterItem> GetFilterByProducts([FromUri]GetFiltersByProductRequestQuery query)
        {
            var getFacingColorsQuery = _hubFactory.CreateQueryHandler<GetFiltersByProductRequestQuery, List<DtoFacingColorItem>>();
            var getFacingsQuery = _hubFactory.CreateQueryHandler<GetFiltersByProductRequestQuery, List<DtoFacingItem>>();
            var getFrameColorsQuery = _hubFactory.CreateQueryHandler<GetFiltersByProductRequestQuery, List<DtoFrameColorItem>>();
            
            var facingColors = await getFacingColorsQuery.Execute(query);
            var facings = await getFacingsQuery.Execute(query);
            var frameColors = await getFrameColorsQuery.Execute(query);

            return new DtoFilterItem { FacingColors = facingColors, Facings = facings, FrameColors = frameColors };
        }

        [HttpGet]
        [ActionName("facings")]
        public List<DtoFacingItem> GetFacings()
        {
            var getFacingsQuery = _hubFactory.CreateQueryHandler<GetFacingsQuery, List<DtoFacingItem>>();
            var facings = getFacingsQuery.Execute(new GetFacingsQuery()).Result;
            return facings.OrderBy(p => p.Name).ToList();
        }

        [HttpGet]
        [ActionName("facingColors")]
        public List<DtoFacingColorItem> GetFacingColors()
        {
            var getFacingColorsQuery = _hubFactory.CreateQueryHandler<GetFacingColorQuery, List<DtoFacingColorItem>>();
            var facingColors = getFacingColorsQuery.Execute(new GetFacingColorQuery()).Result;
            return facingColors.OrderBy(p=>p.Name).ToList();
        }

        [HttpGet]
        [ActionName("frameColors")]
        public List<DtoFrameColorItem> GetFrameColors()
        {
            var getFrameColorsQuery = _hubFactory.CreateQueryHandler<GetFrameColorQuery, List<DtoFrameColorItem>>();
            var frameColors = getFrameColorsQuery.Execute(new GetFrameColorQuery()).Result;
            return frameColors.OrderBy(p => p.Name).ToList();
        }
    }
}
