﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Query;
using KonigDev.Lazurit.Store.Dto.Cart;
using KonigDev.Lazurit.Store.Dto.Cart.CartFull;
using KonigDev.Lazurit.Store.Extensions.Filters;
using KonigDev.Lazurit.Store.Services;
using KonigDev.Lazurit.Store.Providers;
using KonigDev.Lazurit.Store.Services.LazuritContext;
using Microsoft.AspNet.Identity;
using System.Diagnostics;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers.Cart
{
    [AllowAnonymous]
    [NotImplExceptionFilter]
    [RoutePrefix("api/cartfull")]
    public class CartFullController : ApiController
    {
        private readonly ICartProvider _cartProvider;
        private readonly ILazuritCtxSessionService _lazuritCtxService;
        private readonly IHubHandlersFactory _hubFactory;

        public CartFullController(ICartProvider cartProvider, ILazuritCtxSessionService lazuritCtxService, IHubHandlersFactory hubFactory)
        {
            _cartProvider = cartProvider;
            _hubFactory = hubFactory;
            _lazuritCtxService = lazuritCtxService;
        }

        #region Cart
        [HttpGet]
        [Route("")]
        public async Task<DtoCartFullPriceResult> GetCurrentCart()
        {
            var cartId = _lazuritCtxService.GetSingleCartId();
            var priceInfo = _lazuritCtxService.GetPriceInfoFromContext();
            var cart = await _cartProvider.ForOnlyFullCart().GetCart(cartId, priceInfo.Currency, priceInfo.PriceZoneId, priceInfo.DeliveryPriceForCity);
            return cart;
        }

        [HttpDelete]
        [Route("")]
        public async Task<IHttpActionResult> DeleteCart()
        {
            var cartId = _lazuritCtxService.GetSingleCartId();
            var priceInfo = _lazuritCtxService.GetPriceInfoFromContext();
            await _cartProvider.ForOnlyMiniCart().RemoveCart(cartId, priceInfo.Currency, priceInfo.PriceZoneId, priceInfo.DeliveryPriceForCity);
            return Ok();
        }

        [HttpGet]
        [Route("approval")]
        public DtoCartIsNeedApproval CheckIsApprovalRequired()
        {
            var cartId = _lazuritCtxService.GetSingleCartId();
            var getIsApprovalRequiredQueryhandler = _hubFactory.CreateQueryHandler<GetCartIsApprovalRequiredQuery, DtoCartIsNeedApproval>();
            var dtoCartIsNeedApproval = getIsApprovalRequiredQueryhandler.Execute(new GetCartIsApprovalRequiredQuery { CartId = cartId });

            return dtoCartIsNeedApproval.Result;
        }

        #endregion

        #region Product
        [HttpPost]
        [Route("product")]
        public async Task<IHttpActionResult> AddProductToCart([FromBody] DtoAddProductToCartRequest request)
        {
            var cartId = _lazuritCtxService.GetSingleCartId();
            try
            {
                await _cartProvider.ForOnlyFullCart().AddProductToCart(request, cartId);
            }
            catch (Core.Enums.Exeptions.Cart.NotValidExeption ex)
            {
                return BadRequest(ex.Message);
            }
            await UpdateMiniCart(cartId);

            return Ok();
        }

        [HttpDelete]
        [Route("product/{id}")]
        public async Task<IHttpActionResult> DeleteProductFromCart(Guid id)
        {
            var cartId = _lazuritCtxService.GetSingleCartId();
            Debug.Assert(cartId != null);

            await _cartProvider.ForOnlyFullCart().DeleteProductFromCart(id, cartId);
            await UpdateMiniCart(cartId);

            return Ok();
        }

        [HttpPut]
        [Route("product")]
        public async Task<IHttpActionResult> UpdateProductInCart([FromBody]DtoUpdateProductInCartRequest request)
        {
            var cartId = _lazuritCtxService.GetSingleCartId();
            await _cartProvider.ForOnlyFullCart().UpdateProductInCart(request, cartId);
            await UpdateMiniCart(cartId);
            return Ok();
        }
        #endregion

        #region Collection
        [HttpPost]
        [Route("collection")]
        public async Task<IHttpActionResult> AddCollectionToCart([FromBody]DtoAddCollectionToCartRequest request)
        {
            var cartId = _lazuritCtxService.GetSingleCartId();

            try
            {
                await _cartProvider.ForOnlyFullCart().AddCollectionToCart(request, cartId);
            }
            catch (Core.Enums.Exeptions.Cart.NotValidExeption ex)
            {
                return BadRequest(ex.Message);
            }

            await UpdateMiniCart(cartId);
            return Ok();
        }
        [HttpDelete]
        [Route("collection/{product}/{collection}")]
        public async Task<IHttpActionResult> DeleteItemCollectionFromCart(Guid product, Guid collection)
        {
            var cartId = _lazuritCtxService.GetSingleCartId();
            await _cartProvider.ForOnlyFullCart().DeleteItemCollectionFromCart(product, collection, cartId);

            await UpdateMiniCart(cartId);
            return Ok();

        }

        [HttpDelete]
        [Route("collection/full/{collection}")]
        public async Task<IHttpActionResult> DeleteFullCollectionFromCart(Guid collection)
        {
            var cartId = _lazuritCtxService.GetSingleCartId();
            await _cartProvider.ForOnlyFullCart().DeleteFullCollectionFromCart(collection, cartId);

            await UpdateMiniCart(cartId);
            return Ok();
        }

        [HttpPut]
        [Route("collection")]
        public async Task<IHttpActionResult> UpdateProductCollectionInCart([FromBody]DtoUpdateCollectionProductInCartRequest request)
        {
            var cartId = _lazuritCtxService.GetSingleCartId();
            await _cartProvider.ForOnlyFullCart().UpdateProductCollectionInCart(request, cartId);

            await UpdateMiniCart(cartId);
            return Ok();
        }
        #endregion

        #region Complect
        [HttpPost]
        [Route("complect")]
        public async Task<IHttpActionResult> AddComplectToCart([FromBody]DtoAddComplectToCartRequest request)
        {
            var cartId = _lazuritCtxService.GetSingleCartId();
            try
            {
                await _cartProvider.ForOnlyFullCart().AddComplectToCart(request, cartId);
            }
            catch (Core.Enums.Exeptions.Cart.NotValidExeption ex)
            {
                return BadRequest(ex.Message);
            }

            await UpdateMiniCart(cartId);
            return Ok();
        }
        [HttpDelete]
        [Route("complect/{id}")]
        public async Task<IHttpActionResult> DeleteItemComplectFromCart(Guid id)
        {
            var cartId = _lazuritCtxService.GetSingleCartId();
            await _cartProvider.ForOnlyFullCart().DeleteItemComplectFromCart(id, cartId);

            await UpdateMiniCart(cartId);
            return Ok();
        }
        [HttpPut]
        [Route("complect")]
        public async Task<IHttpActionResult> UpdateComplectInCart([FromBody]DtoUpdateComplectInCartRequest request)
        {
            var cartId = _lazuritCtxService.GetSingleCartId();
            await _cartProvider.ForOnlyFullCart().UpdateComplectInCart(request, cartId);

            await UpdateMiniCart(cartId);
            return Ok();
        }
        #endregion

        private async Task UpdateMiniCart(Guid cartId)
        {
            var priceInfo = _lazuritCtxService.GetPriceInfoFromContext();
            await _cartProvider.ForOnlyMiniCart().UpdateCart(cartId, priceInfo.Currency, priceInfo.PriceZoneId, priceInfo.DeliveryPriceForCity);
        }
    }
}