﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Query;
using KonigDev.Lazurit.Store.Dto.Cart;
using KonigDev.Lazurit.Store.Dto.Cart.CartMini;
using KonigDev.Lazurit.Store.Extensions.Filters;
using KonigDev.Lazurit.Store.Providers;
using KonigDev.Lazurit.Store.Services.LazuritContext;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers.Cart
{
    [AllowAnonymous]
    [NotImplExceptionFilter]
    [RoutePrefix("api/cartmini")]
    public class CartMiniController : ApiController
    {
        private readonly ICartProvider _cartProvider;
        private readonly IHubHandlersFactory _hubFactory;
        private readonly ILazuritCtxSessionService _lazuritCtxSesion;

        public CartMiniController(ICartProvider cartProvider, ILazuritCtxSessionService lazuritCtxService,
            IHubHandlersFactory hubFactory)
        {
            _cartProvider = cartProvider;
            _hubFactory = hubFactory;
            _lazuritCtxSesion = lazuritCtxService;
        }

        #region Cart
        [HttpGet]
        [Route("")]
        [AllowAnonymous]
        public async Task<DtoCartMiniPriceResult> GetCurrentCart()
        {
            var cartId = _lazuritCtxSesion.GetSingleCartId();
            var priceInfo = _lazuritCtxSesion.GetPriceInfoFromContext();
            var cart = await _cartProvider.ForOnlyMiniCart().GetCart(cartId, priceInfo.Currency, priceInfo.PriceZoneId, priceInfo.DeliveryPriceForCity);
            return cart;
        }

        [HttpPost]
        [Route("cookie")]
        [AllowAnonymous]
        public IHttpActionResult SetCartCookies()
        {
            _lazuritCtxSesion.GetSingleCartId();
            return Ok();
        }

        [HttpDelete]
        [Route("")]
        [AllowAnonymous]
        public void DeleteCart()
        {
            var cartId = _lazuritCtxSesion.GetSingleCartId();
            var priceInfo = _lazuritCtxSesion.GetPriceInfoFromContext();
            _cartProvider.ForOnlyMiniCart().RemoveCart(cartId, priceInfo.Currency, priceInfo.PriceZoneId, priceInfo.DeliveryPriceForCity);
        }
        
        [HttpGet]
        [Route("approval")]
        [AllowAnonymous]
        public DtoCartIsNeedApproval CheckIsApprovalRequired()
        {
            var cartId = _lazuritCtxSesion.GetSingleCartId();
            var getIsApprovalRequiredQueryhandler = _hubFactory.CreateQueryHandler<GetCartIsApprovalRequiredQuery, DtoCartIsNeedApproval>();
            var dtoCartIsNeedApproval = getIsApprovalRequiredQueryhandler.Execute(new GetCartIsApprovalRequiredQuery { CartId = cartId });

            return dtoCartIsNeedApproval.Result;
        }
        #endregion

        #region Product
        [HttpPost]
        [Route("product")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> AddProductToCart([FromBody] DtoAddProductToCartRequest request)
        {
            var cartId = _lazuritCtxSesion.GetSingleCartId();
            try
            {
                await _cartProvider.ForOnlyMiniCart().AddProductToCart(request, cartId);
            }
            catch (Core.Enums.Exeptions.Cart.NotValidExeption ex)
            {
                return BadRequest(ex.Message);
            }


            await UpdateMiniCart(cartId);
            return Ok();
        }

        [HttpDelete]
        [Route("product/{id}")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> DeleteProductFromCart(Guid id)
        {
            var cartId = _lazuritCtxSesion.GetSingleCartId();
            await _cartProvider.ForOnlyMiniCart().DeleteProductFromCart(id, cartId);

            await UpdateMiniCart(cartId);
            return Ok();
        }

        [HttpPut]
        [Route("product")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> UpdateProductInCart([FromBody]DtoUpdateProductInCartRequest request)
        {
            var cartId = _lazuritCtxSesion.GetSingleCartId();
            await _cartProvider.ForOnlyMiniCart().UpdateProductInCart(request, cartId);

            await UpdateMiniCart(cartId);
            return Ok();
        }
        #endregion

        #region Collection
        [HttpPost]
        [Route("collection")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> AddCollectionToCart([FromBody]DtoAddCollectionToCartRequest request)
        {
            var cartId = _lazuritCtxSesion.GetSingleCartId();

            try
            {
                await _cartProvider.ForOnlyMiniCart().AddCollectionToCart(request, cartId);
            }
            catch (Core.Enums.Exeptions.Cart.NotValidExeption ex)
            {
                return BadRequest(ex.Message);
            }

            await UpdateMiniCart(cartId);
            return Ok();
        }
        [HttpDelete]
        [Route("collection/{product}/{collection}")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> DeleteItemCollectionFromCart(Guid product, Guid collection)
        {
            var cartId = _lazuritCtxSesion.GetSingleCartId();
            await _cartProvider.ForOnlyMiniCart().DeleteItemCollectionFromCart(product, collection, cartId);

            await UpdateMiniCart(cartId);
            return Ok();

        }

        [HttpDelete]
        [Route("collection/full/{collection}")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> DeleteFullCollectionFromCart(Guid collection)
        {
            var cartId = _lazuritCtxSesion.GetSingleCartId();
            await _cartProvider.ForOnlyMiniCart().DeleteFullCollectionFromCart(collection, cartId);

            await UpdateMiniCart(cartId);
            return Ok();
        }

        [HttpPut]
        [Route("collection")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> UpdateProductCollectionInCart([FromBody]DtoUpdateCollectionProductInCartRequest request)
        {
            var cartId = _lazuritCtxSesion.GetSingleCartId();
            await _cartProvider.ForOnlyMiniCart().UpdateProductCollectionInCart(request, cartId);

            await UpdateMiniCart(cartId);
            return Ok();
        }
        #endregion

        #region Complect
        [HttpPost]
        [Route("complect")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> AddComplectToCart([FromBody]DtoAddComplectToCartRequest request)
        {
            var cartId = _lazuritCtxSesion.GetSingleCartId();
            try
            {
                await _cartProvider.ForOnlyMiniCart().AddComplectToCart(request, cartId);
            }
            catch (Core.Enums.Exeptions.Cart.NotValidExeption ex)
            {
                return BadRequest(ex.Message);
            }

            await UpdateMiniCart(cartId);
            return Ok();
        }
        [HttpDelete]
        [Route("complect/{id}")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> DeleteItemComplectFromCart(Guid id)
        {
            var cartId = _lazuritCtxSesion.GetSingleCartId();
            await _cartProvider.ForOnlyMiniCart().DeleteItemComplectFromCart(id, cartId);

            await UpdateMiniCart(cartId);
            return Ok();
        }
        [HttpPut]
        [Route("complect")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> UpdateComplectInCart([FromBody]DtoUpdateComplectInCartRequest request)
        {
            var cartId = _lazuritCtxSesion.GetSingleCartId();
            await _cartProvider.ForOnlyMiniCart().UpdateComplectInCart(request, cartId);

            await UpdateMiniCart(cartId);
            return Ok();
        }
        #endregion

        private async Task UpdateMiniCart(Guid cartId)
        {
            var priceInfo = _lazuritCtxSesion.GetPriceInfoFromContext();
            await _cartProvider.ForOnlyMiniCart().UpdateCart(cartId, priceInfo.Currency, priceInfo.PriceZoneId, priceInfo.DeliveryPriceForCity);
        }
    }
}