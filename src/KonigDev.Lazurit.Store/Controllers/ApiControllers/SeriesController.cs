﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Seriess.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Seriess.Queries;
using KonigDev.Lazurit.Store.Extensions.Filters;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [NotImplExceptionFilter]
    public class SeriesController : ApiController
    {
        private readonly IHubHandlersFactory _hubFactory;

        public SeriesController(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        [HttpGet]
        public async Task<List<DtoSeriesItem>> GetSeries()
        { 
            return await _hubFactory.CreateQueryHandler<GetSeriesByRequestQuery, List<DtoSeriesItem>>().Execute(new GetSeriesByRequestQuery());
        }
    }
}
