﻿using KonigDev.Lazurit.Auth.InternalAPI.AccountApi.Interfaces;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Users;
using KonigDev.Lazurit.Hub.Model.DTO.Users.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Users.Queries;
using KonigDev.Lazurit.Store.Services;
using KonigDev.Lazurit.Store.SignalrService;
using System;
using System.Threading.Tasks;
using System.Web.Http;
using KonigDev.Lazurit.Auth.InternalAPI.DTO;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands;
using KonigDev.Lazurit.Store.Dto.Account;
using KonigDev.Lazurit.Store.Extensions.Converters.Account;
using KonigDev.Lazurit.Store.Extensions.Filters;
using KonigDev.Lazurit.Store.Services;
using KonigDev.Lazurit.Store.Services.Cart;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using AuthDTOConstants = KonigDev.Lazurit.Auth.Model.DTO.Constants;
using KonigDev.Lazurit.Store.ViewModels.ProfileViewModels;
using System.Web.Http.ModelBinding;
using System.Net.Http;
using System.Net;
using System.Linq;
using KonigDev.Lazurit.Store.Infrastructure;
using KonigDev.Lazurit.Store.Services.Profile;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [AllWithoutSellerAndAnonym]
    [NotImplExceptionFilter]
    public class ProfileController : ApiController
    {
        private readonly IProfileService _profileService;

        private readonly IHubHandlersFactory _hubFactory;
      
        public ProfileController(IHubHandlersFactory hubFactory, IProfileService profileService)
        {
            _hubFactory = hubFactory;
            _profileService = profileService;
        }

        [HttpPut]
        public async Task<HttpResponseMessage> UpdateProfile([FromBody] VmProfileForUpdate userCommand)
        {
            if (!ModelState.IsValid)
            {
                var errorMessage = HttpErrorsResponseBuilder.GetModelStateErrors(ModelState);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errorMessage);
            }
            var updateProfileHandler = _hubFactory.CreateCommandHandler<UpdateUserProfileCommand>();
            await updateProfileHandler.Execute(new UpdateUserProfileCommand
            {
                //todo вынести в маппер????
                MiddleName = userCommand.MiddleName,
                FirstName = userCommand.FirstName,
                UserAddresses = userCommand.UserAddresses,
                Id = userCommand.Id,
                LastName = userCommand.LastName,
                Title = userCommand.Title,
                UserPhones = userCommand.UserPhones
            });
            return Request.CreateResponse(HttpStatusCode.OK);
        }      

        [HttpGet]
        public async Task<DtoUserProfile> GetProfile([FromUri] GetUserProfileByIdQuery getUserQuery)
        {
            if (getUserQuery == null || getUserQuery.UserId == null || getUserQuery.UserId == Guid.Empty)
            {
                getUserQuery.UserId = new Guid(User.Identity?.GetUserId());
            }
            var result = await _profileService.GetUserProfile(getUserQuery);
            return result;
        }
    }
}
