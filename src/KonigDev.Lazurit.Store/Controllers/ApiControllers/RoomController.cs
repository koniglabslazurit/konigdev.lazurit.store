﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Query;
using KonigDev.Lazurit.Store.Extensions.Filters;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [NotImplExceptionFilter]
    [RoutePrefix("api/room")]
    public class RoomController : ApiController
    {
        private readonly IHubHandlersFactory _hubFactory;

        public RoomController(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        [HttpGet]
        [Route("")]
        public async Task<List<DtoRoomItem>> GetRooms()
        {
            return await _hubFactory.CreateQueryHandler<GetRoomsQuery, List<DtoRoomItem>>().Execute(new GetRoomsQuery());
        }

        [HttpGet]
        [Route("single-product")]
        public async Task<List<DtoRoomByProductItem>> GetRoomTypesByProduct([FromUri]GetRoomTypesByProductRequestQuery request)
        {
            var getRoomTypesByProductRequestQueryHandler = _hubFactory.CreateQueryHandler<GetRoomTypesByProductRequestQuery, List<DtoRoomByProductItem>>();
            var roomTypes = await getRoomTypesByProductRequestQueryHandler.Execute(request);

            return roomTypes;
        }
    }
}
