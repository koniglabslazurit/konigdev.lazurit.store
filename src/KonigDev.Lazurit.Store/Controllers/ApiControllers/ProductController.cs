﻿using System;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Products;
using KonigDev.Lazurit.Hub.Model.DTO.Filters;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Query;
using KonigDev.Lazurit.Store.Extensions.Filters;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using KonigDev.Lazurit.Hub.Model.DTO.Article.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Property.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Property.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Article;
using KonigDev.Lazurit.Store.Dto.Prices;
using KonigDev.Lazurit.Store.Extensions;
using KonigDev.Lazurit.Store.Services.PriceSrvice;
using Microsoft.AspNet.Identity;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;
using KonigDev.Lazurit.Store.Services.LazuritContext;
using KonigDev.Lazurit.Store.Services.ProductService;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [RoutePrefix("api/Product")]
    [NotImplExceptionFilter]
    public class ProductController : ApiController
    {
        private readonly IHubHandlersFactory _hubFactory;
        private readonly ILazuritCtxSessionService _lazuritCtxService;
        private readonly IPriceService _priceService;
        private readonly IProductService _productService;

        public ProductController(IHubHandlersFactory hubFactory, ILazuritCtxSessionService lazuritCtxService, IPriceService priceService, IProductService productService)
        {
            _hubFactory = hubFactory;
            _lazuritCtxService = lazuritCtxService;
            _priceService = priceService;
            _productService = productService;
        }

        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> Get([FromUri]GetProductsMarketerQuery query)
        {
            if (query == null)
                return Ok(new List<DtoProductPrice>());

            query.UserId = User.Identity.IsAuthenticated ? Guid.Parse(User.Identity.GetUserId()) : (Guid?)null;
            //todo проверить нинжект!
            var marketerProducts = await _hubFactory.CreateQueryHandler<GetProductsMarketerQuery, DtoProductMarketerTableResponse>().Execute(query);
            var marketerModelMapped = _productService.MapProductsToViewModel(marketerProducts);

            var priceInfo = _lazuritCtxService.GetPriceInfoFromContext();
            await _priceService.GetProductsPrices(priceInfo.PriceZoneId, priceInfo.Currency, marketerModelMapped.Items.SelectMany(p => p.Products).ToList());
            return Ok(marketerModelMapped);
        }

        [HttpGet]
        [Route("filters")]
        public async Task<List<DtoFilterFrameColor>> GetFilters([FromUri] GetAvailableFiltersByArticleQuery query)
        {
            var handler = _hubFactory.CreateQueryHandler<GetAvailableFiltersByArticleQuery, List<DtoFilterFrameColor>>();
            return await handler.Execute(query);
        }

        [HttpGet]
        [Route("property")]
        public async Task<DtoProperties> GetProperties([FromUri] GetAvailablePropertiesByArticleQuery query)
        {
            var handler = _hubFactory.CreateQueryHandler<GetAvailablePropertiesByArticleQuery, DtoProperties>();
            return await handler.Execute(query);
        }

        [HttpGet]
        [Route("partMarkers")]
        public async Task<List<DtoProductPartMarker>> GetProductpartMarkers([FromUri] GetProductPartMarkersQuery query)
        {
            var handler = _hubFactory.CreateQueryHandler<GetProductPartMarkersQuery, List<DtoProductPartMarker>>();
            return await handler.Execute(query);
        }

        [HttpGet]
        [Route("vendorCode")]
        public async Task<DtoArticleWithPrices> GetProductsInVendorCode([FromUri] GetArticleByUniqueFieldsQuery query)
        {
            var article = new DtoArticleWithPrices();
            var handler = _hubFactory.CreateQueryHandler<GetArticleByUniqueFieldsQuery, DtoArticleByUniqueFieldsQueryResult>();
            if (query != null)
            {
                query.UserId = User.Identity.IsAuthenticated ? Guid.Parse(User.Identity.GetUserId()) : (Guid?)null;
                var articleDto = await handler.Execute(query);
                //todo либо получать продукт сразу с ценой, либо вынести это в мэппер
                article.ArticleId = articleDto.ArticleId;
                article.ArticleSyncCode1C = articleDto.ArticleSyncCode1C;
                article.Facings = articleDto.Facings;
                article.FacingsColors = articleDto.FacingsColors;
                article.FramesColors = articleDto.FramesColors;
                article.FurnitureTypeName = articleDto.FurnitureTypeName;
                article.Heights = articleDto.Heights;
                article.Lengths = articleDto.Lengths;
                article.Widths = articleDto.Widths;
                article.Products = articleDto.Products.Select(x => new DtoProductInCollectionVariantPrice
                {
                    Article = x.Article,
                    ArticleId = x.ArticleId,
                    CollectionAlias = x.CollectionAlias,
                    Descr = x.Descr,
                    SeriesName = x.SeriesName,
                    FacingColorId = x.FacingColorId,
                    FacingColorName = x.FacingColorName,
                    FacingId = x.FacingId,
                    FacingName = x.FacingName,
                    FrameColorId = x.FrameColorId,
                    FrameColorIdName = x.FrameColorIdName,
                    FurnitureType = x.FurnitureType,
                    Height = x.Height,
                    Id = x.Id,
                    IsAssemblyRequired = x.IsAssemblyRequired,
                    Length = x.Length,
                    Propirties = x.Propirties,
                    RoomTypeName = x.RoomTypeName,
                    SyncCode1C = x.SyncCode1C,
                    Width = x.Width,
                    IsFavorite = x.IsFavorite,
                    PackagesAmount = x.PackagesAmount,
                    PackageWeight = x.PackageWeight,
                    ProductPackage = x.ProductPackage,
                    Warranty = x.Warranty
                }).ToList();

                var priceInfo = _lazuritCtxService.GetPriceInfoFromContext();
                await _priceService.GetProductsPrices(priceInfo.PriceZoneId, priceInfo.Currency, article.Products.ToList());

                //var getPriceProduct = _hubFactory.CreateQueryHandler<GetProductsPricesQuery, List<DtoProductPricesResult>>();
                //var productPrices = await getPriceProduct.Execute(new GetProductsPricesQuery { ProductIds = article.Products.Select(x => x.Id).ToList(), CurrencyCode = priceInfo.Currency.Code, PriceZoneId = priceInfo.PriceZoneId });

                //foreach (var price in productPrices)
                //{
                //    foreach (var product in article.Products)
                //    {
                //        if (price.ProductId == product.Id)
                //        {
                //            product.Price = price.Price.ToMoney(priceInfo.Currency);
                //            product.PriceForAssembly = price.AssemblyPrice.ToMoney(priceInfo.Currency);
                //            price.Discount = price.Discount ?? 0;
                //            product.Discount = (int)price.Discount.Value;
                //            product.SalePrice = (price.Price - (price.Price * product.Discount / 100)).ToMoney(priceInfo.Currency);
                //            product.SalePriceWithAssembly = (price.Price - (price.Price * product.Discount / 100) + price.AssemblyPrice).ToMoney(priceInfo.Currency);
                //        }
                //    }
                //}
            }
            return article;
        }
    }
}
