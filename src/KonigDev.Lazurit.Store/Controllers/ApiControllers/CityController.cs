﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Users;
using KonigDev.Lazurit.Hub.Model.DTO.Users.Region.Queries;
using KonigDev.Lazurit.Store.Extensions.Filters;
using Microsoft.AspNet.Identity;
using AuthDTOConstants = KonigDev.Lazurit.Auth.Model.DTO.Constants;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [RoutePrefix("api/cities")]
    [NotImplExceptionFilter]
    [Authorize]
    public class CityController : ApiController
    {
        private readonly IHubHandlersFactory _hubFactory;

        public CityController(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        [HttpGet]
        [Authorize(Roles = AuthDTOConstants.RegionRoleName)]
        [Route("")]
        public async Task<IEnumerable<DTOCityResult>> GetAsync()
        {
            var getCityQuryHandler = _hubFactory.CreateQueryHandler<GetCitiesByRegionIdQuery, List<DTOCityResult>>();
            return await getCityQuryHandler.Execute(new GetCitiesByRegionIdQuery { RegionId = new Guid(User.Identity.GetUserId()) });
        }

    }
}
