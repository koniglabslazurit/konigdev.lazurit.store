﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes;
using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Extensions.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [NotImplExceptionFilter]
    public class FurnitureTypeController : ApiController
    {
        private readonly IHubHandlersFactory _hubFactory;

        public FurnitureTypeController(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        [HttpGet]
        [ActionName("furniture-types")]
        public async Task<List<DtoFurnitureTypeResult>> GetFurnitureTypes([FromUri]GetFurnitureTypesByCollectionQuery query)
        {
            var getFurnitureTypesByRequestQuery = _hubFactory.CreateQueryHandler<GetFurnitureTypesByCollectionQuery, List<DtoFurnitureTypeResult>>();
            return await getFurnitureTypesByRequestQuery.Execute(query);
        }       

        [HttpGet]
        [ActionName("installment")]
        public async Task<List<DtoIntegrationFurnitureTypeItem>> Get()
        {
            var handler = _hubFactory.CreateQueryHandler<GetIntegrationFurnitureTypeQuery, List<DtoIntegrationFurnitureTypeItem>>();
            return await handler.Execute(new GetIntegrationFurnitureTypeQuery());
        }

        [HttpGet]
        public async Task<List<DtoFurnitureTypeWithDefaultProductResult>> GetAllFurnitureWithDefaultProductTypes([FromUri]GetFurnitureTypesWithDefaultProductQuery query)
        {
            var getFurnitureTypesWithDefaultProductQuery = _hubFactory.CreateQueryHandler<GetFurnitureTypesWithDefaultProductQuery, List<DtoFurnitureTypeWithDefaultProductResult>>();
            return await getFurnitureTypesWithDefaultProductQuery.Execute(query);
        }
        
        [HttpGet]
        [ActionName("single-product")]
        public async Task<List<DtoFurnitureTypeItem>> GetFurnitureTypesByProductRequest([FromUri]GetFurnitureTypesByProductRequestQuery query)
        {
            var getFurnitureTypesByProductRequestQuery = _hubFactory.CreateQueryHandler<GetFurnitureTypesByProductRequestQuery, List<DtoFurnitureTypeItem>>();
            return await getFurnitureTypesByProductRequestQuery.Execute(query);
        }     
    }
}
