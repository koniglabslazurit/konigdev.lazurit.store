﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Property.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Property.Query;
using KonigDev.Lazurit.Store.Extensions.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [NotImplExceptionFilter]
    public class PropertyController : ApiController
    {
        private readonly IHubHandlersFactory _hubFactory;

        public PropertyController(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        [HttpGet]
        public async Task<List<DtoPropertyItem>> GetStyles([FromUri]GetPropertiesQuery query)
        {
            var getPropertyHandler = _hubFactory.CreateQueryHandler<GetPropertiesQuery, List<DtoPropertyItem>>();
            var properties = await getPropertyHandler.Execute(new GetPropertiesQuery { PropertyType = query.PropertyType });
            return properties;
        }
    }
}
