﻿using KonigDev.Lazurit.Core.Enums;
using KonigDev.Lazurit.Core.Services.StorageServices;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Queries;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Command;
using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Dto.Command;
using KonigDev.Lazurit.Store.Extensions.Filters;
using KonigDev.Lazurit.Store.Dto.Image;
using KonigDev.Lazurit.Store.ViewModels.CommonViewModels;

namespace KonigDev.Lazurit.Store.Controllers.ApiControllers
{
    [NotImplExceptionFilter]
    [RoutePrefix("api/images")]
    public class ImageController : ApiController
    {
        private readonly IHubHandlersFactory _hubFactory;
        private readonly IStorageService _storageService;

        public ImageController(IHubHandlersFactory hubFactory, IStorageService storageService)
        {
            _storageService = storageService;
            _hubFactory = hubFactory;
        }

        /// <summary>
        /// Получение изображения для коллекции
        /// </summary>
        /// <param name="id">Ид коллекции</param>
        /// <param name="type">Тип изображения ><seealso cref="EnumFileContentType"/></param>
        /// <seealso cref="EnumFileContentType"/>
        /// <returns></returns>
        [Route("collection/{id:guid}/{type:maxlength(20)}")]
        public async Task<HttpResponseMessage> GetCollection(Guid id, string type)
        {
            /* для коллекции получаем фото из варианта коллекции, берем дефолтный, если нет, то берем первый с изображением */
            return await GetImageByObjectId(id, type);
        }

        /// <summary>
        /// Получение изображения для вариантоа исполнения коллекции
        /// </summary>
        /// <param name="id">Ид вариатна коллекции</param>
        /// <param name="type">Тип изображения</param>
        /// <seealso cref="EnumFileContentType"/>
        /// <returns></returns>
        [Route("collectionvariant/{id:Guid}/{type:maxlength(20)}")]
        public async Task<HttpResponseMessage> GetCollectionVariansImage(Guid id, string type)
        {
            return await GetImageByObjectId(id, type);
        }

        /// <summary>
        /// Получение изображения для вариантов-фильтров (цвета, отделка) возвращает всегда одного типа изображение EnumFileContentType.TileMin
        /// </summary>
        /// <param name="id">Id варианта фильтра</param>        
        /// <returns></returns>
        [Route("variant/{id:Guid}")]
        public async Task<HttpResponseMessage> GetVariantImage(Guid id)
        {
            return await GetImageByObjectId(id, EnumFileContentType.TileMin.ToString());
        }

        /// <summary>
        /// Получение изображения для продуктов(цветное) на фоне изображения коллекции(черно-белая)
        /// </summary>
        /// <param name="id">Id продукта в варианте коллекции</param> 
        /// <param name="type">Тип изображения</param>
        /// <seealso cref="EnumFileContentType"/>
        /// <returns></returns>
        [Route("productinvariant/{id:Guid}/{type:maxlength(20)}")]
        public async Task<HttpResponseMessage> GetProductInCollectionVariantImage(Guid id, string type)
        {
            return await GetImageByObjectId(id, type);
        }

        /// <summary>
        /// Получение изображения для продуктов 
        /// </summary>
        /// <param name="id">Id продукта</param>        
        /// <param name="type">Тип изображения</param>
        /// <returns></returns>
        [Route("product/{id:Guid}/{type:maxlength(20)}")]
        public async Task<HttpResponseMessage> GetProductImage(Guid id, string type)
        {
            return await GetImageByObjectId(id, type);
        }

        /// <summary>
        /// Получение изображения по его ид
        /// </summary>
        /// <param name="id">Id изображения</param>        
        /// <returns></returns>
        [Route("image/{id:Guid}")]
        public async Task<HttpResponseMessage> GetImageByImageId(Guid id)
        {
            var image = await Task.FromResult(_storageService.GetFile(id.ToString()));
            if (image == null)
                return new HttpResponseMessage(HttpStatusCode.NotFound);

            var response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new ByteArrayContent(image);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
            return response;
        }

        /// <summary>
        /// Получение списка доступных изображений для объекта - отдает list dto
        /// </summary>
        /// <param name="id">Ид объекта</param>
        /// <returns></returns>
        [Route("filecontent/{id:Guid}")]
        public async Task<List<DtoFilesContent>> GetAllFileContentsForObject(Guid id)
        {
            var handler = _hubFactory.CreateQueryHandler<GetFilesContentByRequestQuery, List<DtoFilesContent>>();
            return await handler.Execute(new GetFilesContentByRequestQuery
            {
                ObjectId = id,
                Type = EnumFileContentType.None
            });
        }

        /// <summary>
        /// Получение изображения по ид объекта и по типу - выдается одно первое изображение
        /// </summary>
        /// <param name="id">Ид </param>
        /// <param name="type"></param>
        /// <returns></returns>
        private async Task<HttpResponseMessage> GetImageByObjectId(Guid id, string type)
        {
            var enumType = EnumFileContentType.None;
            if (!Enum.TryParse(type, out enumType))
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            var handler = _hubFactory.CreateQueryHandler<GetFilesContentByRequestQuery, List<DtoFilesContent>>();
            var result = await handler.Execute(new GetFilesContentByRequestQuery
            {
                ObjectId = id,
                Type = enumType
            });

            if (result.Count == 0)
                return new HttpResponseMessage(HttpStatusCode.NotFound);

            var image = await Task.FromResult(_storageService.GetFile(result[0].Id.ToString()));
            if (image == null)
                return new HttpResponseMessage(HttpStatusCode.NotFound);

            var response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new ByteArrayContent(image);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/" + result[0].Extensions);
            return response;
        }

        [HttpPost]
        [Route("upload")]
        public async Task<HttpResponseMessage> Upload([FromUri]UploadFile model)
        {
            var files = System.Web.HttpContext.Current.Request.Files;
            if (files == null || files.Count == 0)
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            if (files[0] == null)
                return new HttpResponseMessage(HttpStatusCode.NotFound);

            var enumType = EnumFileContentType.None;
            if (!Enum.TryParse(model.Type, out enumType))
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            var file = files[0];
            var handler = _hubFactory.CreateCommandHandler<CreateUpdateFileContentCommand>();
            var fileId = Guid.NewGuid();


            await handler.Execute(new CreateUpdateFileContentCommand
            {
                FileName = file.FileName,
                FileExtension = Path.GetExtension(file.FileName),
                ObjectId = model.ObjectId,
                Type = enumType,
                Id = fileId,
                Size = file.ContentLength
            });

            byte[] bytes;
            using (var ms = new MemoryStream())
            {
                file.InputStream.CopyTo(ms);
                bytes = ms.ToArray();
            }
            _storageService.SaveFile(fileId.ToString(), bytes);
            var response = Request.CreateResponse(HttpStatusCode.OK);
            return response;
        }

        [HttpPost]
        public async Task<HttpResponseMessage> CreateContent(UploadFile model)
        {
            var files = System.Web.HttpContext.Current.Request.Files;
            if (files == null)
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            if (files[0] == null)
                return new HttpResponseMessage(HttpStatusCode.NotFound);

            var enumType = EnumFileContentType.None;
            if (!Enum.TryParse(model.Type, out enumType))
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            var file = files[0];
            var handler = _hubFactory.CreateCommandHandler<CreateFileContentCommand>();
            var fileId = Guid.NewGuid();

            await handler.Execute(new CreateFileContentCommand
            {
                FileName = file.FileName,
                FileExtension = Path.GetExtension(file.FileName),
                ObjectId = model.ObjectId,
                Type = enumType,
                Id = fileId,
                Size = file.ContentLength
            });

            byte[] bytes;
            using (var ms = new MemoryStream())
            {
                file.InputStream.CopyTo(ms);
                bytes = ms.ToArray();
            }
            _storageService.SaveFile(fileId.ToString(), bytes);
            var response = Request.CreateResponse(HttpStatusCode.OK);
            return response;
        }
    }
}
