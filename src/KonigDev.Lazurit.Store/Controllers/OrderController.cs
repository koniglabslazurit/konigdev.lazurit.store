﻿using KonigDev.Lazurit.Store.Extensions.Filters;
using System;
using System.Web.Mvc;

namespace KonigDev.Lazurit.Store.Controllers
{
    [Authorize]
    [NotImplExceptionFilter]
    public class OrderController : Controller
    {
        public ViewResult Thanks(Guid id) {
            return View(id);
        }
    }
}