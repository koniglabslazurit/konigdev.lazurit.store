﻿using KonigDev.Lazurit.Store.Extensions.Filters;
using KonigDev.Lazurit.Store.ViewModels.CollectionViewModels;
using System;
using System.Web.Mvc;
using AuthDTOConstants = KonigDev.Lazurit.Auth.Model.DTO.Constants;

namespace KonigDev.Lazurit.Store.Controllers
{
    /* TODO определить набор ролей и использовать их а не то как сейчас роль+","+роль */

    [Authorize]
    [NotImplExceptionFilter]
    public class ImportController : Controller
    {
        /// <summary>
        /// Загрузка изображений для вариантов коллекций
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = AuthDTOConstants.AdminRoleName + "," + AuthDTOConstants.ContentManagerRoleName)]
        public ActionResult Collection(Guid collectionId)
        {
            return View(collectionId);
        }

        /// <summary>
        /// Загрузка коллекций из 1С
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = AuthDTOConstants.AdminRoleName + "," + AuthDTOConstants.ContentManagerRoleName)]
        public ActionResult Collections()
        {
            return View();
        }

        /// <summary>
        /// Загрузка продуктов для валидации из 1С
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = AuthDTOConstants.AdminRoleName + "," + AuthDTOConstants.ContentManagerRoleName)]
        public ActionResult ProductValidation(Guid collectionId)
        {
            return View(collectionId);
        }

        /// <summary>
        /// Редактирование полей продуктов с статусом template и возможность их публикации
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = AuthDTOConstants.AdminRoleName + "," + AuthDTOConstants.ContentManagerRoleName)]        
        public ActionResult ProductsTemplates(Guid collectionId)
        {
            return View(collectionId);
        }

        /// <summary>
        /// Редактирование полей продуктов с статусом published
        /// </summary>
        /// <returns></returns>        
        [HttpGet]
        [Authorize(Roles = AuthDTOConstants.AdminRoleName + "," + AuthDTOConstants.ContentManagerRoleName)]
        public ActionResult ProductsPublished()
        {
            return View();
        }

        [Authorize(Roles = AuthDTOConstants.AdminRoleName + "," + AuthDTOConstants.ContentManagerRoleName)]
        public ActionResult ProductInstallment()
        {
            return View();
        }


        [Authorize(Roles = AuthDTOConstants.AdminRoleName + "," + AuthDTOConstants.ContentManagerRoleName)]
        public ActionResult Filters()
        {
            return View();
        }
    }
}