﻿using KonigDev.Lazurit.Auth.InternalAPI.AccountApi.Interfaces;
using KonigDev.Lazurit.Auth.InternalAPI.DTO;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Users;
using KonigDev.Lazurit.Hub.Model.DTO.Users.Queries;
using KonigDev.Lazurit.Store.Dto.Favorite;
using KonigDev.Lazurit.Store.Extensions.Filters;
using Microsoft.AspNet.Identity;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace KonigDev.Lazurit.Store.Controllers
{
    [NotImplExceptionFilter]
    [Authorize]
    public class PersonalController : Controller
    {
        private readonly IHubHandlersFactory _hubFactory;
        private readonly IAccountInternalAPI _accountInternalApi;

        public PersonalController(IHubHandlersFactory hubFactory, IAccountInternalAPI accountInternalApi)
        {
            _hubFactory = hubFactory;
            _accountInternalApi = accountInternalApi;
        }
        // GET: Profile
        public ActionResult Profile()
        {
            return View();
        }

        [AllowAnonymous]
        public async Task<ActionResult> Favorite(Guid? id)
        {
            string patch = Url.Action("Favorite", "Personal", new { id = "" }, Request.Url.Scheme);
            Guid? linkId = id;
            if (linkId == null)
                return View(new DtoModelOfFavoriteRequest
                {
                    IsUserOwner = true,
                    Patch = patch
                });
            else
            {
                Guid userId;
                if (!Guid.TryParse(User?.Identity?.GetUserId(), out userId))
                    return View(new DtoModelOfFavoriteRequest
                    {
                        IsUserOwner = false,
                        LinkId = (Guid)linkId,
                        Patch = patch
                    });
                else
                {
                    var getUserProfileQueryHandler = _hubFactory.CreateQueryHandler<GetUserProfileByIdQuery, DtoUserProfile>();
                    var data = await getUserProfileQueryHandler.Execute(new GetUserProfileByIdQuery
                    {
                        UserId = userId
                    });
                    if (data.LinkId == linkId)
                        return View(new DtoModelOfFavoriteRequest
                        {
                            IsUserOwner = true,
                            Patch = patch
                        });
                    else
                        return View(new DtoModelOfFavoriteRequest
                        {
                            IsUserOwner = false,
                            LinkId = (Guid)linkId,
                            Patch = patch
                        });
                }
            }
        }
        [AllowAnonymous]
        public ViewResult Cart()
        {
            return View();
        }
        public ViewResult Wishlist()
        {
            return View();
        }

        [AllowAnonymous]
        public async Task<ActionResult> ConfirmChangeEmail(string userId, string token)
        {
            token = token.Replace("PLUSCHAR", "+");
            if (string.IsNullOrEmpty(userId))
            {
                string error = "Не возможно подтвердить регистрацию, так как UserId пустой";
                return View(error);
            }
            if (string.IsNullOrEmpty(token))
            {
                string error = "Не возможно подтвердить регистрацию, так как token пустой";
                return View(error);
            }
            else {
                await _accountInternalApi.ConfirmChangeEmail(new DtoConfirmChangeEmailRequest { UserId = Guid.Parse(userId), Token = token});
                return RedirectToAction("Profile");
            }
        }
    }
}