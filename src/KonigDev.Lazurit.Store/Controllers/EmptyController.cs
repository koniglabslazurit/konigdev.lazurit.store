﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KonigDev.Lazurit.Store.Extensions;

namespace KonigDev.Lazurit.Store.Controllers
{
    /// <summary>
    /// Служит для получения контекста контроллера <see cref="ControllerExtension.RenderRazorViewToString(System.Web.Http.ApiController, string, object)"/>
    /// </summary>
    public class EmptyController : Controller
    {
    }
}