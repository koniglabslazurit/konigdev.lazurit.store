﻿using KonigDev.Lazurit.Core.Enums;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Queries;
using KonigDev.Lazurit.Store.ViewModels.CommonViewModels;
using KonigDev.Lazurit.Store.ViewModels.FilesContentManagment;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;
using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Dto.Command;
using KonigDev.Lazurit.Core.Services.StorageServices;
using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Command;
using KonigDev.Lazurit.Store.Dto.Image;
using KonigDev.Lazurit.Store.Extensions.Filters;

namespace KonigDev.Lazurit.Store.Controllers
{

    /// <summary>
    /// Контроллер для демо, для возможности загрузки изображений!!! Нужен тольк для того чтобы подгрузит изображения для демо показа!!! 
    /// Не использовать его в продакшене - выпилить. Когда у будет контент-менеджер.
    /// </summary>
    [NotImplExceptionFilter]
    public class ImageManagerController : Controller
    {
        private readonly IHubHandlersFactory _hubFactory;
        private readonly IStorageService _storageService;

        public ImageManagerController(IHubHandlersFactory hubFactory, IStorageService storageService)
        {
            _storageService = storageService;
            _hubFactory = hubFactory;
        }
        // GET: ImageManager
        public async Task<ActionResult> Index()
        {
            var handler = _hubFactory.CreateQueryHandler<GetFilesContentObjectsQuery, DtoFilesContentObjectsResponse>();
            var result = await handler.Execute(new GetFilesContentObjectsQuery());

            var types = new List<SelectListItem>();
            types.AddRange(new List<SelectListItem>
            {
                new SelectListItem { Text = "В интерьере", Value = EnumFileContentType.InInterior.ToString() },
                new SelectListItem { Text = "Слайдер среднего (главная, тип комнаты)", Value = EnumFileContentType.SliderMiddle.ToString() },
                new SelectListItem { Text = "Слайдер малый (в карточке)", Value = EnumFileContentType.SliderMin.ToString() },
                new SelectListItem { Text = "Тайл большой (в товарах)", Value = EnumFileContentType.TileBig.ToString() },
                new SelectListItem { Text = "Тайл малый (для цветов)", Value = EnumFileContentType.TileMin.ToString() }
            });
            var model = new FilesContentTable { ContentObjects = result, Types = types };
            return View(model);
        }

        [HttpPost]
        public async Task<JsonResult> Upload(UploadFile model)
        {
            if (Request.Files == null || Request.Files.Count == 0)
                return Json(new VmJsonResult { Success = false, Message = "Отсутсвуют файлы" });
            if (Request.Files[0] == null)
                return Json(new VmJsonResult { Success = false, Message = "Отсутсвуют файлы" });

            var enumType = EnumFileContentType.None;
            if (!Enum.TryParse(model.Type, out enumType))
                return Json(new VmJsonResult { Success = false, Message = "Неверыный тип фала контента" });

            var file = Request.Files[0];
            var handler = _hubFactory.CreateCommandHandler<CreateUpdateFileContentCommand>();
            var fileId = Guid.NewGuid();


            await handler.Execute(new CreateUpdateFileContentCommand
            {
                FileName = file.FileName,
                FileExtension = Path.GetExtension(file.FileName),
                ObjectId = model.ObjectId,
                Type = enumType,
                Id = fileId,
                Size = file.ContentLength
            });

            byte[] bytes;
            using (var ms = new MemoryStream())
            {
                file.InputStream.CopyTo(ms);
                bytes = ms.ToArray();
            }
            _storageService.SaveFile(fileId.ToString(), bytes);
            return Json(new VmJsonResult { Success = true });
        }

        [HttpPost]
        public async Task<JsonResult> CreateContent(UploadFile model)
        {
            if (Request.Files == null)
                return Json(new VmJsonResult { Success = false, Message = "Отсутсвуют файлы" });
            if (Request.Files[0] == null)
                return Json(new VmJsonResult { Success = false, Message = "Отсутсвуют файлы" });

            var enumType = EnumFileContentType.None;
            if (!Enum.TryParse(model.Type, out enumType))
                return Json(new VmJsonResult { Success = false, Message = "Неверыный тип фала контента" });

            var file = Request.Files[0];
            var handler = _hubFactory.CreateCommandHandler<CreateFileContentCommand>();
            var fileId = Guid.NewGuid();

            await handler.Execute(new CreateFileContentCommand
            {
                FileName = file.FileName,
                FileExtension = Path.GetExtension(file.FileName),
                ObjectId = model.ObjectId,
                Type = enumType,
                Id = fileId,
                Size = file.ContentLength
            });

            byte[] bytes;
            using (var ms = new MemoryStream())
            {
                file.InputStream.CopyTo(ms);
                bytes = ms.ToArray();
            }
            _storageService.SaveFile(fileId.ToString(), bytes);
            return Json(new VmJsonResult { Success = true });
        }
        
        /// <summary>
        /// Страница для теста загрузки картинок для конкретного товара  
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult Product(ProductContent model)
        {
            return View(model);
        }

        public ViewResult Areas(Guid id)
        {
            return View(id);
        }

        public class ProductContent
        {
            public Guid Id { get; set; }
            public string SyncCode1C { get; set; }
        }
    }
}