﻿using AutoMapper;
using KonigDev.Lazurit.Core.Enums;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Article;
using KonigDev.Lazurit.Hub.Model.DTO.Article.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Collections;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Query;
using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Queries;
using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Query;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Query;
using KonigDev.Lazurit.Store.Dto.Article;
using KonigDev.Lazurit.Store.Dto.Catalog;
using KonigDev.Lazurit.Store.Dto.Collection;
using KonigDev.Lazurit.Store.Dto.Prices;
using KonigDev.Lazurit.Store.Extensions;
using KonigDev.Lazurit.Store.Extensions.Filters;
using KonigDev.Lazurit.Store.Services.LazuritContext;
using KonigDev.Lazurit.Store.Services.PriceSrvice;
using Microsoft.AspNet.Identity;
using MoreLinq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using KonigDev.Lazurit.Store.Dto.Product;

namespace KonigDev.Lazurit.Store.Controllers
{
    [NotImplExceptionFilter]
    public class CatalogController : Controller
    {
        private readonly IHubHandlersFactory _hubFactory;
        private readonly IPriceService _priceService;
        private readonly ILazuritCtxSessionService _lazuritCtxService;

        public CatalogController(IHubHandlersFactory hubFactory, ILazuritCtxSessionService lazuritCtxService, IPriceService priceService)
        {
            _hubFactory = hubFactory;
            _priceService = priceService;
            _lazuritCtxService = lazuritCtxService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var getCatalogQueryHandler = _hubFactory.CreateQueryHandler<GetRoomTypesQuery, List<DtoRoomTypesResult>>();
            var rooms = getCatalogQueryHandler.Execute(new GetRoomTypesQuery()).Result;

            /* todo откидываем те записи, у которыех нет вариантов коллекции, посмотреть перенос логики в грейн?
             * необходимо чтобы не отображались комнаты, с коллекциями, у кторых нет опубликованных вариантов */
            foreach (var item in rooms)
                item.Collections = item.Collections.Where(p => p.DefaultVariantId != Guid.Empty).ToList();
            rooms = rooms.Where(p => p.Collections.Any()).ToList();

            //distincting all filters for specific room type 
            foreach (var room in rooms)
            {
                room.Styles = room.Collections.SelectMany(c => c.Styles).DistinctBy(s => s.Id);
                room.TargetAudiences = room.Collections.SelectMany(c => c.TargetAudiences).DistinctBy(ta => ta.Id);
            }
            //distincting all filters for all room types
            var roomsExtended = new DtoRoomsExtendedResult
            {
                Rooms = rooms,
                Styles = rooms.SelectMany(r => r.Styles).DistinctBy(s => s.Id),
                TargetAudiences = rooms.SelectMany(r => r.TargetAudiences).DistinctBy(ta => ta.Id)
            };
            return View(roomsExtended);
        }

        [HttpGet]
        public ActionResult Room(string alias)
        {
            return View(new DTORomByAliasResult() { Alias = alias });
        }

        [HttpGet]
        public ActionResult Collection(string roomAlias, string collectionAlias)
        {
            return View(new DTOCollectionFoomResult
            {
                RommAlias = roomAlias,
                CollectionAlias = collectionAlias
            });
        }

        //GET : PRODUCT
        //[SiteMapTitle("alias")]
        public async Task<ActionResult> Product(string furnitureAlias, string productAlias)
        {
            /* todo вынести все это в отдельный сервис/класс - тут слишком много всего, нечитаемо */
            var parserResult = ProductAliasParser.ParseAlias(productAlias);
            if (parserResult.HasErrors)
                return HttpNotFound();

            var getArticleQueryHandler = _hubFactory.CreateQueryHandler<GetArticleByUniqueFieldsQuery, DtoArticleByUniqueFieldsQueryResult>();
            var article = await getArticleQueryHandler.Execute(new GetArticleByUniqueFieldsQuery
            {
                ArticleSyncCode1C = parserResult.ArticleCodeOnlyDigits
            });
            if (article == null)
                return HttpNotFound();
            // не используется. удалить закоменченное после того, как убедимся, что не нужно
            //var getCollectionByAlias = _hubFactory.CreateQueryHandler<GetCollectionByAliasQuery, DtoCollectionByAliasResult>();
            //var collection = await getCollectionByAlias.Execute(new GetCollectionByAliasQuery
            //{
            //    Alias = parserResult.CollectionAlias
            //});

            var result = new DtoArticleWithFilesContent
            {
                Article = article,
                ProductsfilesContent = new List<DtoProductFilesContent>()
                // Collection = collection
            };

            var handler = _hubFactory.CreateQueryHandler<GetFilesContentByRequestQuery, List<DtoFilesContent>>();
            foreach (var product in result.Article.Products)
            {
                var fileContentResult = await handler.Execute(new GetFilesContentByRequestQuery
                {
                    ObjectId = product.Id,
                    Type = EnumFileContentType.None
                });
                result.ProductsfilesContent.Add(new DtoProductFilesContent
                {
                    ProductId = product.Id,
                    ProductFilesContent = fileContentResult
                });
            }
            return View(result);
        }

        public async Task<ActionResult> GetProductsFromSameCollection(GetProductsByRequestQuery query)
        {
            if (query == null)
                return PartialView(new List<DtoProduct>());

            var handler = _hubFactory.CreateQueryHandler<GetProductsByRequestQuery, List<DtoProduct>>();
            query.UserId = User.Identity.IsAuthenticated ? Guid.Parse(User.Identity.GetUserId()) : (Guid?)null;
            var dto = await handler.Execute(query);

            //TODO использовать через ninject (IMapperEngine или что-то в этом духе у него уже есть.)
            var model = Mapper.Map<List<DtoProductPrice>>(dto);

            var priceInfo = _lazuritCtxService.GetPriceInfoFromContext();
            await _priceService.GetProductsPrices(priceInfo.PriceZoneId, priceInfo.Currency, model);
            return PartialView(model);
        }

        public async Task<ActionResult> Furniture()
        {
            var getFurnitureTypesWithDefaultProductQuery = _hubFactory.CreateQueryHandler<GetFurnitureTypesWithDefaultProductQuery, List<DtoFurnitureTypeWithDefaultProductResult>>();
            var data = await getFurnitureTypesWithDefaultProductQuery.Execute(new GetFurnitureTypesWithDefaultProductQuery());

            return View(data);
        }

        public ActionResult ProductsInFurniture(string furnitureAlias)
        {
            return View(new DtoProductsByFurnitureAlias { FurnitureAlias = furnitureAlias });
        }

        public ActionResult ProductsInSubFurniture(string parentFurnitureAlias, string furnitureAlias)
        {
            if (parentFurnitureAlias == furnitureAlias)
                return RedirectToAction("ProductsInFurniture", new { furnitureAlias });
            return View(new DtoProductsByFurnitureAlias { ParentFurnitureAlias = parentFurnitureAlias, FurnitureAlias = furnitureAlias });
        }
    }
}