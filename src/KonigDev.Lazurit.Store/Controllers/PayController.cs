﻿using KonigDev.Lazurit.Core.Services;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Dto;
using KonigDev.Lazurit.Store.Dto.Kkm;
using KonigDev.Lazurit.Store.Dto.Payment;
using KonigDev.Lazurit.Store.Extensions.Filters;
using KonigDev.Lazurit.Store.Services.OrderService;
using KonigDev.Lazurit.Store.Services.PaymentService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace KonigDev.Lazurit.Store.Controllers
{
    [NotImplExceptionFilter]
    public class PayController : Controller
    {
        private readonly IPaymentService _paymentService;
        private readonly ILoggingService _loggingService;
        private readonly IOrderService _orderService;
        public PayController(IPaymentService paymentService, ILoggingService loggingService, IOrderService orderService)
        {
            _paymentService = paymentService;
            _loggingService = loggingService;
            _orderService = orderService;
        }

        [HttpGet]
        public ActionResult RkResult(DtoRobokassaResult result)
        {
            _loggingService.InfoMessage(string.Format("Robokassa result: InvId:{0}, OutSum:{1}", result.InvId, result.OutSum));
            if (!_paymentService.IsValidResultResponse(result))
            {
                _loggingService.InfoMessage(string.Format("Robokassa invalid: InvId:{0}, OutSum:{1}", result.InvId, result.OutSum));
                return View("PaymentError", result);
            }
            _paymentService.UpdateOrderPaid(result, true);
            return Content(string.Format("OK{0}", result.InvId));
        }
        [HttpGet]
        public async Task<ViewResult> RkSuccess(DtoRobokassaResult result)
        {
            _loggingService.InfoMessage(string.Format("Robokassa success: InvId:{0}, OutSum:{1}", result.InvId, result.OutSum));
            if (!_paymentService.IsValidResultResponse(result))
            {
                _loggingService.InfoMessage(string.Format("Robokassa invalid: InvId:{0}, OutSum:{1}", result.InvId, result.OutSum));
                return View("PaymentError", result);
            }
            DtoKkmResponse kkmOrderInfo = await _paymentService.GetRestKkmResult(new DtoKkmRequest
            {
                InvId = result.InvId,
                OutSum = result.OutSum,
                SignatureValue = result.SignatureValue
            });
            await _orderService.UpdateOrderKkmFields(result.InvId, kkmOrderInfo.KkmOrderRecordUrl);
            return View("PaymentSuccess", result);
        }

        [HttpGet]
        public ViewResult RkFail(DtoRobokassaResult result)
        {
            _loggingService.InfoMessage(string.Format("Robokassa fail: InvId:{0}, OutSum:{1}", result.InvId, result.OutSum));
            return View("PaymentError", result);
        }

        [HttpGet]
        public ViewResult Sberbank()
        {
            return View();
        }
        
    }
}