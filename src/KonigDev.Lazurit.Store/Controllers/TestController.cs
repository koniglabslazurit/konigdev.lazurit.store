﻿using KonigDev.Lazurit.Store.Dto.Kkm;
using KonigDev.Lazurit.Store.Extensions.Filters;
using System.Web.Mvc;

namespace KonigDev.Lazurit.Store.Controllers
{
    [NotImplExceptionFilter]
    public class TestController : Controller
    {
        public ActionResult Index(string returnUrl)
        {
            return Redirect("/#/auth/login-popup?" + returnUrl);
            //return Redirect("/#/auth/login-popup");
        }
        // GET: Test
        public ActionResult ScriptCatalog()
        {
            return View();
        }

        /// <summary>
        /// Это временный мок внешнего апи сервиса который возвращает нам УРЛ по которому будет доступен просмотра выписки из ККМ
        /// </summary>
        /// <param name="kkmRequest">параметры нашего заказа</param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public DtoKkmResponse GetKkmOrder(DtoKkmRequest kkmRequest)
        {
            return new DtoKkmResponse { KkmOrderRecordUrl = Url.Action("KkmOrder", new { kkmNumber = 123 }) };
        }
        /// <summary>
        /// Мок страницы с информацией по выписки ККМ
        /// </summary>
        /// <param name="kkmNumber"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        public ActionResult KkmOrder(int kkmNumber)
        {
            return View(kkmNumber);
        }
    }
}