﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Queries;
using KonigDev.Lazurit.Store.Extensions.Filters;
using KonigDev.Lazurit.Store.Services.Geolocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using KonigDev.Lazurit.Core.Services;
using KonigDev.Lazurit.Store.Dto.Geolocation;
using Orleans.Concurrency;

namespace KonigDev.Lazurit.Store.Controllers
{
    [System.Web.Mvc.AllowAnonymous]
    [NotImplExceptionFilter]

    public class GeolocationController : Controller
    {
        private readonly IGeolocationService _geolocationService;
        private readonly IHubHandlersFactory _hubFactory;
        private readonly ILoggingService _loggingService;
        public GeolocationController(IGeolocationService geolocationService, IHubHandlersFactory hubFactory, ILoggingService loggingSrevice)
        {
            _geolocationService = geolocationService;
            _hubFactory = hubFactory;
            _loggingService = loggingSrevice;
        }
        //TODO Использовать CookieService!!!!!!!!!!!!
        [System.Web.Mvc.HttpPost]
        public ActionResult City([FromBody]DtoCoordinates coords)
        {
            _loggingService.InfoMessage(string.Format("Your coordinates are: lat:{0}, lon:{1}", coords.Latitude, coords.Longitude));            
            var cityCookieId = Request.Cookies["cityId"];
            var cityId = cityCookieId?.Value;
            var firstCookie = Request.Cookies["firstTime"];

            var cities = _hubFactory.CreateQueryHandler<GetAllCitiesQuery, List<DtoCity>>().Execute(new GetAllCitiesQuery()).Result;
            DtoCity currentCity = null;
            if (!string.IsNullOrEmpty(cityId))
            {
                currentCity = cities.FirstOrDefault(x => x.Id == Guid.Parse(cityId));
            }
            /*проверка, если ид города из куков нет в текущей БД, стираем куки и вычисляем город по-новому*/
            if (string.IsNullOrEmpty(cityId) || currentCity == null)
            {
                Response.Cookies.Remove("cityId");
                Response.Cookies.Remove("cityName");
                Response.Cookies.Remove("zoneId");
                Response.Cookies.Remove("delivery");
                Response.Cookies.Remove("firstTime");
                cityCookieId = null;
                cityId = string.Empty;
                firstCookie = null;
            }
            if (cityCookieId == null || string.IsNullOrEmpty(cityId) || firstCookie==null)
            {
                var coordsDouble = _geolocationService.ParseCoordinates(coords);
                var nearestCity = _hubFactory.CreateQueryHandler<GetCityByCoordinates, DtoCityCoordinates>().Execute(coordsDouble).Result;
                _loggingService.InfoMessage(string.Format("CurrentLat: {0}, CurrentLong: {1}", coordsDouble.Latitude, coordsDouble.Longitude));
                _loggingService.InfoMessage(string.Format("Min distanse: {0}, nearestLat: {1}, nearestLong: {2}", nearestCity.Distance, nearestCity.Latitude, nearestCity.Longitude));
                cityId = nearestCity.Id.ToString();
                //todo to sepatare service add cookie

                Response.Cookies.Add(new HttpCookie("cityId") { Value= nearestCity.Id.ToString(), Path="/" });
                Response.Cookies.Add(new HttpCookie("cityName") { Value =HttpUtility.UrlEncode(nearestCity.Title), Path = "/" });
                Response.Cookies.Add(new HttpCookie("zoneId") { Value = nearestCity.PriceZoneId.ToString(), Path = "/" });
                Response.Cookies.Add(new HttpCookie("delivery") { Value = nearestCity.DeliveryPrice.ToString(), Path = "/" });
                Response.Cookies.Add(new HttpCookie("firstTime") { Value = true.ToString(), Path = "/" });
            }
            else
            {
                Response.Cookies.Add(new HttpCookie("firstTime") { Value = false.ToString(), Path = "/" });
            }
            var city = cities.SingleOrDefault(x => x.Id == Guid.Parse(cityId));
            if (city != null)
            {
                city.IsNearest = true;
            }
            return Json(cities, JsonRequestBehavior.AllowGet);
        }
    }
}