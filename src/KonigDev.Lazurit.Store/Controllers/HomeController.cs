﻿using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.MarketingActions;
using KonigDev.Lazurit.Hub.Model.DTO.MarketingActions.Queries;
using KonigDev.Lazurit.Store.Models.Home;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace KonigDev.Lazurit.Store.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHubHandlersFactory _hubFactory;
        public HomeController(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        public async Task<ActionResult> Index()
        {
            var handler = _hubFactory.CreateQueryHandler<AllMarketingActionsQuery, DtoMarketingActions>();
            var model = new HomeViewModel
            {
                Slides = await handler.Execute(new AllMarketingActionsQuery { PageNumber = 1, Take = int.MaxValue, IsActive = true})
            };
            return View(model);
        }

        public async Task<ActionResult> Single(int id)
        {
            var handler = _hubFactory.CreateQueryHandler<SingleMarketingActionQuery, DtoMarketingAction>();
            var action = await handler.Execute(new SingleMarketingActionQuery { ActionSeoId = id });
            return View(action);
        }
    }
}
