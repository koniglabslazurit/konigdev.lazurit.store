﻿(function ($) {
    $.fn.extend({
        lazSlider: function (options) {

            var defautOptions = {
                maxSpeed: 5,                
                sliderSelector: 'laz-slider-row',
                wrapperSelector: 'laz-slider-wrapper',
                sliderItemSelector: 'laz-slider-item',
                /* расстояние на которой начиается скролл при наведении - от верха или от низа */
                scrollboxH: 50,
                /* шаг прокрутки сроллинга */
                scrollStep: 5,
                classScrolling: 'is-scrolling',
                animatedDuration: 1000 /* время отработки анимации в милисикундах */,
                animatedClass: 'animate-it',
                delay: 20,
                scrollTime: 250
            };

            if (!options)
                options = defautOptions;

            for (option in defautOptions) {
                if (!(option in options) || options[option] === undefined) options[option] = defautOptions[option];
            }

            return this.each(function () {

                var _slider = $(this);
                var _sliderRows = $(this).find('.' + options.sliderSelector); /* все строки слайдеров */
                var _topTimep, _bottomTimer, _leftTimer, _rightTimer;

                var bondtall = 0;
                var scrollspeed = 0;
                var righttime;
                var rowSize = 0;
                var scrollstate = '';
                
                _sliderRows.each(function () {
                    $(this).lazSliderH(options);
                });

                $(window).scroll(function () {                    
                    addClassToParent();
                    clearTimeout($.data(this, "scrollCheck"));
                    $.data(this, "scrollCheck", setTimeout(function () {
                        removeClassToParent();
                    }, options.scrollTime));
                });

                _slider.bind('mousemove', function (e) { scrollSliderHorizontal(e); });
                _slider.bind('mouseleave', function (e) { stopScroll(e); });
                animationOnLoad();

                /* Скролл слайдера по горизонтали - определение направления */
                function scrollSliderHorizontal(e) {
                    var pointY = (window.event ? event.clientY : e.clientY ? e.clientY : '') - _slider.offset().top;
                    if (pointY <= options.scrollboxH) {
                        if (scrollstate != 'scroll-top') {
                            scrollSliderTop(e);
                        }
                    } else if (pointY + options.scrollboxH >= _slider.height()) {
                        if (scrollstate != 'scroll-bottom')
                            scrollSliderBottom(e);
                    } else stopScroll();
                };

                /* Скролл слайдера по горизонтали вверх */
                function scrollSliderTop(e) {
                    scrollstate = 'scroll-top';
                    var pointY = $(document).scrollTop() - options.scrollStep;
                    scroll(0, pointY);
                    _topTimep = setTimeout(scrollSliderTop, 10);
                };

                /* Скролл слайдера по горизонтали вниз */
                function scrollSliderBottom(e) {                    
                    scrollstate = 'scroll-bottom';
                    var pointY = $(document).scrollTop() + options.scrollStep;
                    scroll(0, pointY);
                    _bottomTimer = setTimeout(scrollSliderBottom, 10);
                };

                /* Остановка скрола */
                function stopScroll() {
                    removeClassToParent();
                    clearTimeout(_topTimep);
                    clearTimeout(_bottomTimer);
                    scrollstate = '';
                };

                /* добавление класса на родителя */
                function addClassToParent() {
                    if (!_slider.hasClass(options.classScrolling))
                        _slider.addClass(options.classScrolling);
                };

                /* удалене класса с родителя */
                function removeClassToParent() {
                    if (_slider.hasClass(options.classScrolling))
                        _slider.removeClass(options.classScrolling);
                };

                function animationOnLoad() {
                    _slider.addClass(options.animatedClass);
                    setTimeout(function () {
                        _slider.removeClass(options.animatedClass)
                    }, options.animatedDuration);
                };
            });
        },
        lazSliderH: function (options) {

            return this.each(function () {
                var _this = $(this);
                var _leftTimer, _rightTimer;
                var _wrapper = _this.find('.' + options.wrapperSelector);
                var _sliderSize = 0;
                var _movespeed = 0;
                var scrollstate = '';

                _this.bind('mousemove', function (e) { moveHorizontal(e); });
                _this.bind('mouseleave', function (e) { stopMove(e); });

                /* Перемещение слайдера по горизонтали */
                function moveHorizontal(e) {
                    var left = _this.offset().left;
                    var scroll = $(document).scrollLeft();
                    var pointX = (window.event ? event.clientX : e.clientX ? e.clientX : '') - left - scroll;
                    var middle = _this.width() / 2;

                    if (pointX > middle) {
                        _movespeed = (pointX - middle) / middle * options.maxSpeed;
                        clearTimeout(_rightTimer);
                        if (scrollstate != 'move-left') {
                            moveleft();
                        }
                    }
                    else if (pointX < _this.width() / 2) {
                        _movespeed = (middle - pointX) / middle * options.maxSpeed;
                        clearTimeout(_leftTimer);
                        if (scrollstate != 'move-right') {
                            moveright();
                        }
                    }
                };

                /* Перемещение слайдара влево */
                function moveleft() {
                    scrollstate = 'move-left';
                    var left = parseFloat(_wrapper.css('left'));
                    if (left >= (_this.width() - calcSliderSize())) {
                        _wrapper.delay(options.delay).css('left', parseFloat(left - _movespeed) + 'px');
                        _leftTimer = setTimeout(moveleft, 10);
                    } else {
                        stopMove();                        
                    }                   
                };
                
                /* Перемещение слайдера вправо */
                function moveright() {
                    scrollstate = 'move-right';                    
                    var left = parseFloat(_wrapper.css('left'));                    
                    if (left < 0) {
                        _wrapper.delay(options.delay).css('left', parseFloat(left + _movespeed) + 'px');
                        _rightTimer = setTimeout(moveright, 10);
                    } else if (left > 0) {
                        _wrapper.css('left', '0px');
                        stopMove();
                    }                    
                };

                /* Остановка прокрутки */
                function stopMove() {
                    clearTimeout(_leftTimer);
                    clearTimeout(_rightTimer);
                    scrollstate = "";
                };

                function calcSliderSize() {
                    var res = 0;
                    _this.find('.' + options.sliderItemSelector)
                        .each(function () {
                            res += $(this)[0].getBoundingClientRect().width;
                        });
                    return res;
                };
            });
        }
    });
})(jQuery);