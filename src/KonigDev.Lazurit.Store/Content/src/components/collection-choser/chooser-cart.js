jQuery(document).ready(function($){
	var cartWrapper = $('.cd-cart-container');
	//product id - you don't need a counter in your real project but you can use your real product id
	var productId = 0;
	var data_price = 0;
	var name_prod;
	var img_prod;
	var img_prod_alt;
	var discount = 0;
	var discount_price = 0;
	var sum = 0;

	if( cartWrapper.length > 0 ) {
		//store jQuery objects
		var cartBody = cartWrapper.find('.body')
		var cartList = cartBody.find('ul').eq(0);
		var cartTotal = cartWrapper.find('.checkout').find('span');
		var cartTrigger = cartWrapper.children('.cd-cart-trigger');
		var cartCount = cartTrigger.children('.count')
		var addToCartBtn = $('.cd-add-to-cart');
		var undo = cartWrapper.find('.undo');
		var undoTimeoutId;

		addToCartBtn.on('click', function (event) {
			name_prod = $(this).text();
			data_price=$(this).data('price');
			img_prod = $(this).find("img").attr('src');
			img_prod_alt = $(this).find("img").attr('alt');
			discount = $(this).data('discount');
			if( typeof discount === 'undefined'){
				discount_price = data_price.toFixed(2);
				sum = +(Number(sum + Number(discount_price))).toFixed(2);
				return data_price,name_prod,img_prod,img_prod_alt,discount_price;
			}
			discount_price = (data_price-(data_price/100*discount)).toFixed(2);
			sum = +(Number(sum + Number(discount_price))).toFixed(2);
			return data_price,name_prod,img_prod,img_prod_alt,discount_price,discount; 
		});
		//add product to cart
		addToCartBtn.on('click', function(event){
			event.preventDefault();
			addToCart($(this));
		});

		//open/close cart
		cartTrigger.on('click', function(event){
			event.preventDefault();
			toggleCart();
		});

		//close cart when clicking on the .cd-cart-container::before (bg layer)
		cartWrapper.on('click', function(event){
			if( $(event.target).is($(this)) ) toggleCart(true);
		});

		//delete an item from the cart
		cartList.on('click', '.delete-item', function(event){
			event.preventDefault();
			removeProduct($(event.target).parents('.product'));
		});

		//update item quantity
		cartList.on('change', 'select', function(event){
			quickUpdateCart();
		});

		//reinsert item deleted from the cart
		undo.on('click', 'a', function(event){
			clearInterval(undoTimeoutId);
			event.preventDefault();
			cartList.find('.deleted').addClass('undo-deleted').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(){
				$(this).off('webkitAnimationEnd oanimationend msAnimationEnd animationend').removeClass('deleted undo-deleted').removeAttr('style');
				quickUpdateCart();
			});
			undo.removeClass('visible');
		});
	}

	function toggleCart(bool) {
		var cartIsOpen = ( typeof bool === 'undefined' ) ? cartWrapper.hasClass('cart-open') : bool;
		
		if( cartIsOpen ) {
			cartWrapper.removeClass('cart-open');
			//reset undo
			clearInterval(undoTimeoutId);
			undo.removeClass('visible');
			cartList.find('.deleted').remove();

			setTimeout(function(){
				cartBody.scrollTop(0);
				//check if cart empty to hide it
				if( Number(cartCount.find('li').eq(0).text()) == 0) cartWrapper.addClass('empty');
			}, 500);
		} else {
			cartWrapper.addClass('cart-open');
		}
	}

	function addToCart(trigger) {
		var cartIsEmpty = cartWrapper.hasClass('empty');
		//update cart product list
		addProduct();
		//update number of items 
		updateCartCount(cartIsEmpty);
		//update total price
		if(typeof discount === 'undefined'){
			updateCartTotal(trigger.data('price'), true);
		}
		else{
			updateCartTotal(trigger.data('discount'), true);
		}
		//show cart
		cartWrapper.removeClass('empty');
	}

	function addProduct() {
	    //this is just a product placeholder
	    //you should insert an item with the selected product info
	    //replace productId, productName, price and url with your real product info
	    if(typeof discount === 'undefined'){
	    	productId = productId + 1;
	    	var productAdded = $('<li class="product"><div class="product-image"><a href="#0"><img src="'+img_prod+'"alt="placeholder"></a></div><div class="product-details"><h3><a href="#0">'+img_prod_alt+' '+name_prod+'</a></h3><span class="price"><span class="price">'+discount_price+' руб</span><div class="actions"><a href="#0" class="delete-item">Delete</a><div class="quantity"><label for="cd-product-' + productId + '"></label><span class="select"><select id="cd-product-' + productId + '" name="quantity">');
	    	cartList.prepend(productAdded);
	    }
	    else{
		    productId = productId + 1;
		    var productAdded = $('<li class="product"><div class="product-image"><a href="#0"><img src="'+img_prod+'"alt="placeholder"></a></div><div class="product-details"><h3><a href="#0">'+img_prod_alt+' '+name_prod+'</a></h3><span class="price">'+data_price+' руб<br/>-'+discount+'%<br/><span class="price">'+discount_price+' руб</span><div class="actions"><a href="#0" class="delete-item">Delete</a><div class="quantity"><label for="cd-product-' + productId + '"></label><span class="select"><select id="cd-product-' + productId + '" name="quantity">');
	    	cartList.prepend(productAdded);
	    }
	}

	function removeProduct(product) {
		clearInterval(undoTimeoutId);
		cartList.find('.deleted').remove();
		
		var topPosition = product.offset().top - cartBody.children('ul').offset().top ,
			productQuantity = Number(product.find('.quantity').find('select').val());
		
		product.css('top', topPosition+'px').addClass('deleted');

		//update items count + total price
		var del = +(Number(product.find('.price').find('.price').text().replace('$', ''))).toFixed(2);
		console.log(del);
		sum = +(Number(sum) - Number(del)).toFixed(2);
		updateCartTotal(sum);
		updateCartCount(true, -productQuantity);
		undo.addClass('visible');	

		//wait 8sec before completely remove the item
		undoTimeoutId = setTimeout(function(){
			undo.removeClass('visible');
			cartList.find('.deleted').remove();
		}, 8000);
	}

	function updateCartCount(emptyCart, quantity) {
		if( typeof quantity === 'undefined' ) {
			var actual = Number(cartCount.find('li').eq(0).text()) + 1;
			var next = actual + 1;
			
			if( emptyCart ) {
				cartCount.find('li').eq(0).text(actual);
				cartCount.find('li').eq(1).text(next);
			} else {
				cartCount.addClass('update-count');

				setTimeout(function() {
					cartCount.find('li').eq(0).text(actual);
				}, 150);

				setTimeout(function() {
					cartCount.removeClass('update-count');
				}, 200);

				setTimeout(function() {
					cartCount.find('li').eq(1).text(next);
				}, 230);
			}
		} else {
			var actual = Number(cartCount.find('li').eq(0).text()) + quantity;
			var next = actual + 1;
			
			cartCount.find('li').eq(0).text(actual);
			cartCount.find('li').eq(1).text(next);
		}
	}

	function updateCartTotal(bool) {
		console.log(sum);
		cartTotal.text(sum);
	}
});