﻿$(".slider-wrap").backgroundCycle({
    imageUrls: [
        '/Content/src/img/bg-slider-1.jpg',
        '/Content/src/img/bg-slider-2.jpg',
        '/Content/src/img/bg-slider-3.jpg'
    ],
    fadeSpeed: 1000,
    duration: 5000,
    backgroundSize: SCALING_MODE_COVER
});

$("html").niceScroll({});

/* запрет скрола в меню каталога */
$(".catalog-wrapper").each(function () {
    $(this).on("mousewheel", function (e) {
        e.stopPropagation();
    })
});

setTimeout(function () {
    $('.laz-slider-body').lazSlider({
        animatedDuration: 5000
    });
}, 2000);

$("#catalog-options").navgoco({
    accordion: true,
    onClickAfter: function (e) {

        setTimeout(function () {
            $('#catalog-options .nano').nanoScroller({
                preventPageScrolling: true
            });
        }, 500);

        if ($('#catalog-options .open').length === 0) {
            $("#catalog-options #rooms-types-menu").click();
        }
    }
});

// Scroll
var _topTimep, _bottomTimer, rowClass;
/* скролл вниз */
function scrollSliderBottom() {
    if (!rowClass) {
        stopScroll();
        return;
    }
    var boundingRow = document.getElementsByClassName(rowClass)[0].getBoundingClientRect();
    var containerBounding = document.getElementsByClassName("laz-slider-body")[0].getBoundingClientRect();

    /* расчитываем где находится элемент, с учетом шапки в 50 px */
    if (boundingRow.bottom > containerBounding.height + 50) {
        /* скролим вниз, пока строка слайдера полностью не покажется, с заданным шагом скрола */
        var pointY = $(document).scrollTop() + 5; /* шаг скрола */
        scroll(0, pointY);
        _bottomTimer = setTimeout(scrollSliderBottom, 10); /* таймаут в мс */
    } else {
        stopScroll();
    }
};
/* скролл вверх */
function scrollSliderTop() {
    if (!rowClass) {
        stopScroll();
        return;
    }
    var boundingRow = document.getElementsByClassName(rowClass)[0].getBoundingClientRect();
    if (boundingRow.top - 50 < 0) {
        /* скролим вверх, пока строка слайдера полностью не покажется, с заданным шагом скрола */
        var pointY = $(document).scrollTop() - 5; /* шаг скрола */
        scroll(0, pointY);
        _topTimep = setTimeout(scrollSliderTop, 15); /* таймаут в мс */
    } else {
        stopScroll();
    }
};

/* остановка скрола  */
function stopScroll() {
    rowClass = null;
    bottomPoint = null;
    clearTimeout(_topTimep);
    clearTimeout(_bottomTimer);
};

/* проверка позиции строки слайдера */
function checkPosition(className) {
    var boundingRow = document.getElementsByClassName(className)[0].getBoundingClientRect();
    var containerBounding = document.getElementsByClassName("laz-slider-body")[0].getBoundingClientRect();

    if (boundingRow.top - 50 < 0) {
        rowClass = className;
        scrollSliderTop();
    } else if (boundingRow.bottom > containerBounding.height) {
        rowClass = className;
        scrollSliderBottom();
    }
};

$('.catalog-options').hover(function () {
    $('.catalog-slider').toggleClass('active');

});

//....................................................................................................................................................................................................//

//функция установки размера ячеек с коллекциями
function setSizeOfCollectionsTiles(visibleCollectionsCount) {
    if (visibleCollectionsCount >= 6) {
        $('.bond_body').removeClass('lines-1 lines-2 lines-3 lines-4 lines-5 lines-6');
        $('.bond_body').addClass('lines-6');
    }
    else if (visibleCollectionsCount == 5) {
        $('.bond_body').removeClass('lines-1 lines-2 lines-3 lines-4 lines-5 lines-6');
        $('.bond_body').addClass('lines-5');
    }
    else if (visibleCollectionsCount == 4) {
        $('.bond_body').removeClass('lines-1 lines-2 lines-3 lines-4 lines-5 lines-6');
        $('.bond_body').addClass('lines-4');
    }
    else if (visibleCollectionsCount == 3) {
        $('.bond_body').removeClass('lines-1 lines-2 lines-3 lines-4 lines-5 lines-6');
        $('.bond_body').addClass('lines-3');
    }
    else if (visibleCollectionsCount == 2) {
        $('.bond_body').removeClass('lines-1 lines-2 lines-3 lines-4 lines-5 lines-6');
        $('.bond_body').addClass('lines-2');
    }
    else if (visibleCollectionsCount == 1) {
        $('.bond_body').removeClass('lines-1 lines-2 lines-3 lines-4 lines-5 lines-6');
        //because lines-2 is a mininal size of a row
        $('.bond_body').addClass('lines-2');
    }
}

//функции ховера строк типов комнат в фильтрах при ховере строк с коллекциями и наоборот
$('.room-type-item').on('mouseenter mouseleave', function (e) {
    var id = $(this).attr('id');
    var x = $('#room-' + id).toggleClass('active');
});
$('.catalog-item').on('mouseenter mouseleave', function (e) {
    var id = $(this).attr('pseudo-id');
    var x = $('#' + id).toggleClass('active');
});

$('.room-type-item').hover(function () {
    var id = $(this).attr('id');
    checkPosition('room-' + id);
});

//функции подсчета и отображения количества типов комнат в виде цифр слева над фильтрами
function getCountOfVisibleRooms() {
    var visibleCollectionsCount = $('.catalog-item:visible').size();
    var allCollectionsCount = $('.catalog-item').size();
    $('.room-types-counter').html('<b>' + visibleCollectionsCount + '</b>' + '/' + allCollectionsCount);
    //и сразу меняем размер ячеек в зависимости от их кол-ва
    setSizeOfCollectionsTiles(visibleCollectionsCount);
}

//удаляем сразу со страницы все типы комнат в которых нет коллекций в бд (если такое вообще возможно)
function disableEmptyRooms() {
    $('.catalog-item').not(':has(.collection-selector)').hide();
}

//нахождение скрытых строк типов комнат и блокирование соответствующих строк типов комнат в фильтрах
function disableHiddenRooms() {
    $('.catalog-item:visible').each(function () {
        var id = $(this).attr('pseudo-id');
        var x = $('#' + id).removeClass('non-active-filter');
    });
    $('.catalog-item:hidden').each(function () {
        var id = $(this).attr('pseudo-id');
        var x = $('#' + id).addClass('non-active-filter');
    });
}

//массивы с активными фильтрами
var selectedStyles = [];
var selectedTa = [];

//функция для блокирования выбора фильтров стилей и ца которых нет в видимых коллекциях
function disableUnnecessaryFilters() {
    var ta = [];
    var styles = [];
    //для каждого видимого элемента-коллекции
    $('.collection-selector:visible').each(function () {
        //находим те атрибуты что хранят id та или стилей
        $.each(this.attributes, function () {
            if (this.specified) {
                //и все их значения с дублями кидаем в массивы
                if (this.name.startsWith('ta-')) {
                    ta.push(this.value);
                }
                if (this.name.startsWith('style-')) {
                    styles.push(this.value);
                }
            }
        });
    });
    //оставляем лишь уникальные значения допустимых фильтров
    var uniqueTa = ta.filter(function (item, pos, self) {
        return self.indexOf(item) == pos;
    });
    var uniqueStyles = styles.filter(function (item, pos, self) {
        return self.indexOf(item) == pos;
    });
    //блокируем все фильтры стилей и та на странице
    $('.room-ta-item').prop("disabled", true);
    $('.room-ta-item').removeClass("necessary")
    $('.room-style-item').prop("disabled", true);
    $('.room-style-item').removeClass("necessary")
    //разблокируем лишь допустимые фильтры
    ta.forEach(function (taId) {
        $('#' + taId).prop("disabled", false);
        $('#' + taId).addClass("necessary");
    });
    styles.forEach(function (styleId) {
        $('#' + styleId).prop("disabled", false);
        $('#' + styleId).addClass("necessary");
    });
}

//обновление контента в соответствиии с выбранными фильтрами
function updateFilterAffectedItems() {
    $('[filter-affected]').hide();
    //отображение комнат удовлетворяющих фильтрам ЦА
    if (selectedTa !== undefined && selectedTa.length > 0) {
        selectedTa.forEach(function (taId) { $('[ta-' + taId + ']').show() });
    }
    //отображение комнат удовлетворяющих фильтрам стилей
    if (selectedStyles !== undefined && selectedStyles.length > 0) {
        selectedStyles.forEach(function (styleId) { $('[style-' + styleId + ']').show(); });
    }
    //отображение всех комнат если фильтров нет
    if ((selectedTa == undefined || selectedTa.length == 0) && (selectedStyles == undefined || selectedStyles.length == 0)) {
        $('[filter-affected]').show();
    }
    //скрытие пустых комнат (в которых нет коллекций в самой бд)
    disableEmptyRooms();
    //пересчет количества комнат
    getCountOfVisibleRooms();
    //скрытие неактивных комнат из спимска слева в соответствие с плитками справа
    disableHiddenRooms();
    //делаем неактивными все фильтры которые не содержатся в видимых комнатах (баги-баги-баги)
    //disableUnnecessaryFilters();
}

//обработчик нажатия конкретного фильтра ЦА
$('.room-ta-item').click(function () {
    var id = $(this).attr('id');
    if ($(this).prop('checked') == true) {
        selectedTa.push(id);
        //делаем видимым выбранный фильтр при сворчаивании списка
        $('[filter-ta-id=' + id + ']').addClass("active");
        updateFilterAffectedItems();
    }
    else {
        var index = selectedTa.indexOf(id);
        selectedTa.splice(index, 1);
        //делаем невидимым выбранный фильтр при сворачивании списка
        $('[filter-ta-id=' + id + ']').removeClass("active");
        updateFilterAffectedItems();
    }
});

//обработчик нажатия конкретного фильтра ЦА, уже выбранного и отображаемого лишь при сворачивании вкладки ЦА
$('.room-ta-choosen-item').click(function () {
    var id = $(this).attr('filter-ta-id');
    //тут нет проверки на checked так как пользователь имеет возможность нажать на элемент только после его выбора
    var index = selectedTa.indexOf(id);
    selectedTa.splice(index, 1);
    //убираем данный элемент из области видимости
    $(this).removeClass("active");
    //разблокируем доступ к ней если это нажатие было вызвано каскадно от группы фильтров
    $(this).removeClass('non-active-filter');
    //снимаем галочку с соотв эл-та в списке
    $('#' + id + '.necessary').prop("checked", false);
    //и делаем его активным если это нажатие было вызвано каскадно от группы фильтров
    $('#' + id + '.necessary').prop("disabled", false);
    updateFilterAffectedItems();
});

//нажатие на конкретный фильтр ЦА вызванное нажатием на родительскую группу фильтров
function falseClick(element) {
    var id = element.attr('id');
    if (element.prop('checked') == true) {
        element.prop('checked', false);
        var index = selectedTa.indexOf(id);
        selectedTa.splice(index, 1);
        updateFilterAffectedItems();
    }
    else {
        element.prop('checked', true);
        selectedTa.push(id);
        updateFilterAffectedItems();
    }
}

//обработчик нажатия малых груп фильтров ЦА
$('.ta-submenu').click(function () {
    var id = $(this).attr('id');
    //находим все дочерние чекбоксы
    var allDependentCheckboxes = $('.necessary[ta-submenu-id="' + id + '"]');
    //если группа еще не активная
    if ($(this).attr('is-checked') == 'false') {
        //проверяем есть ли вообще доступные для выбора активные фильтры в этой группе
        if ($('.necessary[ta-submenu-id="' + id + '"]:enabled').length <= 0) {
            return;
        }
        //помечаем активной
        $(this).attr('is-checked', 'true');
        //и активируем все дочерние чекбоксы
        allDependentCheckboxes.each(function () {
            if ($(this).prop('checked') == false) {
                falseClick($(this));
            }
            $(this).prop('disabled', true);
        });
        //делаем активным соответствующий элемент списка активных фильтров (при сворачивании)
        $('[sub-id="' + id + '"]').addClass("active");
        //блокируем доступ к ее дочерним выбранным фильтрам
        $('[ta-choosen-submenu-id="' + id + '"]').addClass('non-active-filter');
    }
        //если группа уже активна
    else {
        //проверяем есть ли вообще доступные для отмены выбора активные фильтры в этой группе
        if ($('.necessary[ta-submenu-id="' + id + '"]:disabled').length <= 0) {
            return;
        }
        //помечаем ее как неактивную
        $(this).attr('is-checked', 'false');
        //все ее чекбоксы активны и просто снимаем блокировку и протыкиваем их
        allDependentCheckboxes.each(function () {
            $(this).prop('disabled', false);
            falseClick($(this));
        });
        //делаем не активным соответствующий элемент списка активных фильтров (при сворачивании)
        $('[sub-id="' + id + '"]').removeClass("active");
        //разблокируем доступ к ее дочерним выбранным фильтрам
        $('[ta-choosen-submenu-id="' + id + '"]').removeClass('non-active-filter');
    }
});

//обработчик нажатия малой группы фильтров ЦА, уже выбранной и отображаемой лишь при сворачивании вкладки ЦА
$('.ta-choosen-submenu').click(function () {
    var id = $(this).attr('sub-id');
    //тут нет проверки на checked так как пользователь имеет возможность нажать на элемент только после его выбора
    var allDependentSubmenus = $('[ta-choosen-submenu-id="' + id + '"]');
    //убираем данный элемент из области видимости
    $(this).removeClass('active');
    //и разблокируем доступ к данному элементу
    $(this).removeClass('non-active-filter');
    //делаем активным соотв эл-т в списке
    $('#' + id).removeClass('non-active-filter');
    //и не забываем указать что она логически стала не активной
    $('#' + id).attr('is-checked', 'false');
    //запускаем событие нажатия на сабменю в этом меню
    allDependentSubmenus.each(function () {
        $(this).removeClass('active');
        $(this).trigger("click");
    });
});


//нажатие на малую группу фильтров ЦА вызванное нажатием на родительскую большую группу фильтров
function groupFalseClick(element) {
    var id = element.attr('id');
    //находим все дочерние чекбоксы
    var allDependentCheckboxes = $('.necessary[ta-submenu-id="' + id + '"]');
    //если группа еще не активная
    if (element.attr('is-checked') == 'false') {
        //проверяем есть ли вообще доступные для выбора активные фильтры в этой группе
        if ($('.necessary[ta-submenu-id="' + id + '"]:enabled').length <= 0) {
            return;
        }
        //помечаем активной
        element.attr('is-checked', 'true');
        //и активируем все дочерние чекбоксы
        allDependentCheckboxes.each(function () {
            if ($(this).prop('checked') == false) {
                falseClick($(this));
            }
            $(this).prop('disabled', true);
        });
        //блокируем доступ к ее дочерним выбранным фильтрам
        $('[ta-choosen-submenu-id="' + id + '"]').addClass('non-active-filter');
    }
        //если группа уже активна
    else {
        //проверяем есть ли вообще доступные для отмены выбора активные фильтры в этой группе
        if ($('.necessary[ta-submenu-id="' + id + '"]:disabled').length <= 0) {
            return;
        }
        //помечаем ее как неактивную
        element.attr('is-checked', 'false');
        //все ее чекбоксы активны и просто снимаем блокировку и протыкиваем их
        allDependentCheckboxes.each(function () {
            $(this).prop('disabled', false);
            falseClick($(this));
        });
        //разблокируем доступ к ее дочерним выбранным фильтрам
        $('[ta-choosen-submenu-id="' + id + '"]').removeClass('non-active-filter');
    }
}

//обработчик нажатия больших груп фильтров ЦА
$('.ta-menu').click(function () {
    var id = $(this).attr('id');
    //находим все дочерние сабменю
    var allDependentSubmenus = $('[ta-menu-id="' + id + '"]');
    //если эта меню верхнего уровня еще не активна
    if ($(this).attr('is-checked') == 'false') {
        //проверяем есть ли вообще доступные для выбора активные фильтры в подгруппах этой большой группы
        if ($('.necessary[ta-menu-id-pseudo="' + id + '"]:enabled').length <= 0) {
            return;
        }
        //помечаем активной
        $(this).attr('is-checked', 'true');
        //и активируем все дочерние сабменю
        allDependentSubmenus.each(function () {
            if ($(this).attr('is-checked') == 'false') {
                groupFalseClick($(this));
            }
            $(this).addClass('non-active-filter');
        });
        //делаем активным соответствующий элемент списка активных фильтров (при сворачивании)
        $('[sub-id="' + id + '"]').addClass("active");
        //блокируем доступ к ее дочерним группам фильтров
        $('[ta-choosen-menu-id="' + id + '"]').addClass('non-active-filter');
    }
        //если группа уже активна
    else {
        //проверяем есть ли вообще доступные для выбора активные фильтры в подгруппах этой большой группы
        if ($('.necessary[ta-menu-id-pseudo="' + id + '"]:disabled').length <= 0) {
            return;
        }
        //помечаем ее как неактивную
        $(this).attr('is-checked', 'false');
        //все ее подменю активны и просто снимаем блокировку и протыкиваем их
        allDependentSubmenus.each(function () {
            $(this).removeClass('non-active-filter');
            groupFalseClick($(this));
        });
        //делаем не активным соответствующий элемент списка активных фильтров (при сворачивании)
        $('[sub-id="' + id + '"]').removeClass("active");
        //разблокируем доступ к ее дочерним группам фильтров
        $('[ta-choosen-menu-id="' + id + '"]').removeClass('non-active-filter');
    }
});

//обработчик нажатия большой группы фильтров ЦА, уже выбранной и отображаемой лишь при сворачивании вкладки ЦА
$('.ta-choosen-menu').click(function () {
    var id = $(this).attr('sub-id');
    //тут нет проверки на checked так как пользователь имеет возможность нажать на элемент только после его выбора
    var allDependentSubmenus = $('[ta-choosen-menu-id="' + id + '"]');
    //убираем данный элемент из области видимости
    $(this).removeClass('active');
    //делаем активным соотв эл-т в списке
    $('#' + id).removeClass('non-active-filter');
    //и не забываем указать что она логически стала не активной
    $('#' + id).attr('is-checked', 'false');
    //запускаем событие нажатия на сабменю в этом меню
    allDependentSubmenus.each(function () {
        $(this).removeClass('active');
        $(this).trigger("click");
    });
});

//обработчик нажатия конкретного фильтра стиля
$('.room-style-item').click(function () {
    var id = $(this).attr('id');
    if ($(this).prop('checked') == true) {
        selectedStyles.push(id);
        //добавляем видимость фильтра при сворачивании списка фильтров стилей
        $('[filter-style-id=' + id + ']').addClass("active");
        updateFilterAffectedItems();
    }
    else {
        var index = selectedStyles.indexOf(id);
        selectedStyles.splice(index, 1);
        //убираем видимость фильтра при сворачивании списка фильтров стилей
        $('[filter-style-id=' + id + ']').removeClass("active");
        updateFilterAffectedItems();
    }
});

//обработчик нажатия конкретного фильтра стиля, уже выбранного и отображаемого лишь при сворачивании вкладки стилей
$('.room-style-choosen-item').click(function () {
    var id = $(this).attr('filter-style-id');
    //тут нет проверки на checked так как пользователь имеет возможность нажать на элемент только после его выбора
    var index = selectedStyles.indexOf(id);
    selectedStyles.splice(index, 1);
    //убираем данный элемент из области видимости
    $(this).removeClass("active");
    //снимаем галочку с соотв эл-та в списке
    $('#' + id).prop("checked", false);
    updateFilterAffectedItems();
});

$(disableUnnecessaryFilters());
$(updateFilterAffectedItems());
