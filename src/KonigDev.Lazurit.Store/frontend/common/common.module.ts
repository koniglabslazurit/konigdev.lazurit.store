﻿import * as angular from "angular";

import eventService from "./services/event.service";
import messageService from "./services/message.service";
import cookieService from "./services/cookie.service";

export const name = "kd.common";
export const eventServiceName = "eventService";
export const messageServiceName = "commonMessageService";
export const cookieServiceName = "commonCookieService";

angular
    .module(name, [ ])
    .service(eventServiceName, ["$window", eventService])
    .service(messageServiceName, ["toastr", messageService])
    .service(cookieServiceName, ["$document", cookieService]);
