import { $element, $timeout } from "../../../globals/angular.names";

export class FileSelectController {
    private readonly _timeout: ng.ITimeoutService;
    private readonly _element: ng.IAugmentedJQuery;

    private _fileChanged: (element: HTMLInputElement) => void;

    // bindings
    onFileChanged: () => (file: File) => void;

    // view model
    file: File;

    constructor(
        element: ng.IAugmentedJQuery,
        timeout: ng.ITimeoutService
    ) {
        this._timeout = timeout;
        this._element = element;

        this._fileChanged = (input: HTMLInputElement) => {
            if (!input.files || input.files.length === 0) {
                return;
            }

            this.file = input.files[0];
            let handler = this.onFileChanged();
            if (typeof (handler) === "function") {
                handler(this.file);
            }
        };

        this._bindFileInputEvents();
    }

    private _bindFileInputEvents() {
        this._element
            .find("[type='file']")
            .bind("change", (event) => {
                this._timeout(() => {
                    this._fileChanged(<HTMLInputElement>event.target);
                })

            });
    }
}
FileSelectController.$inject = [$element, $timeout];