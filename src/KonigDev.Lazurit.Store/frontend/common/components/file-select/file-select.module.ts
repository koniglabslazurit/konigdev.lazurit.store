// dependencies
import * as angular from "angular";

import { FileSelectController } from "./file-select.controller";

// constants
export const name = "kd.component.file-select"

// module
angular
    .module(name, [])
    .component("kdFileSelectBootstrap",
    {
        template: require("./file-select.bootstrap.html"),
        controller: FileSelectController,
        bindings: {
            onFileChanged: "&"
        }
    })
    .component("kdFileSelectCustom",
    {
        template: require("./file-select.custom.html"),
        controller: FileSelectController,
        bindings: {
            onFileChanged: "&"
        }
    });