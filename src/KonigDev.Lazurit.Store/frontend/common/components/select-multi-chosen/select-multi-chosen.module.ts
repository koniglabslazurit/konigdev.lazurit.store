﻿import * as angular from "angular"

import controller from "./select-multi-chosen.controller";

export const name = "kd.component.select-multi-chosen";
export const controllerName = "selectMultiChosenController";
const componentName = "kdSelectMultiChosen";

angular
    .module(name, [ ])
    .controller(controllerName,
    [
        controller
    ])
    .component(componentName, {
        bindings: {
            settings: '<',
            object: '<',
            listItems: '<',
            kdChange: '&',
            kdModel: '='
        },
        controller: controller,
        template: require("./select-multi-chosen.view.html")
    });
