﻿import * as angular from "angular"

import controller from "./select-single.controller";

export const name = "kd.component.select-single";
export const controllerName = "selectSingleController";
const componentName = "kdSelectSingle";

angular
    .module(name, [ ])
    .controller(controllerName,
    [
        controller
    ])
    .component(componentName, {
        bindings: {
            listItems: '<',
            settings: '<',
            kdModel: '<',
            kdChange: '=',
            kdBage: '<'
        },
        controller: controller,
        template: require("./select-single.view.html")
    });
