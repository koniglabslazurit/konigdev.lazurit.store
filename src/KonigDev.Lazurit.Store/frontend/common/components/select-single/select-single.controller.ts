﻿export default class SelectSingleController {
    private isShow: boolean = false;
    private items: Array<Dto.Select.Item.ISelectItem> = [];
    private currentItem: Dto.Select.Item.ISelectItem;

    private settings: any;
    private kdChange: (item: Dto.Select.Item.ISelectItem) => void;
    private kdModel: Dto.Select.Item.ISelectItem;
    private kdBage: any;

    public constructor() {
    }

    itemSelect(item: Dto.Select.Item.ISelectItem): void {
        if (item) {
            this.isShow = false;
            this.currentItem = item;
            if (typeof (this.kdChange) === 'function') {
                this.kdChange(item);
            }
        }
    };

    $onChanges(changesObj: any): void {
        if (changesObj.listItems) {
            if (changesObj.listItems.currentValue) {
                this.items = JSON.parse(JSON.stringify(changesObj.listItems.currentValue));
            }
        }

        if (changesObj.kdModel) {
            if (changesObj.kdModel.currentValue) {
                this.currentItem = changesObj.kdModel.currentValue;                
            }
        }
    };
};