﻿import * as angular from "angular"

import controller from "./select-single-chosen.controller";

export const name = "kd.component.select.single.chosen";
export const controllerName = "selectMultiSingleController";
const componentName = "kdSelectSingleChosen";

angular
    .module(name, [ ])
    .controller(controllerName,
    [
        controller
    ])
    .component(componentName, {
        bindings: {
            settings: '<',
            object: '<',
            listItems: '<',
            kdChange: '&',
            kdModel: '='
        },
        controller: controller,
        template: require("./select-single-chosen.view.html")
    });
