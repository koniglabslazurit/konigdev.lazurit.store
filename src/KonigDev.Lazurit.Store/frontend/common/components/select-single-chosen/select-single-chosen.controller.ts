﻿export default class SelectSingleChosenController {
    public settings: any;
    public kdChange: any;
    public items: Array<Dto.Select.Item.ISelectItem>;
    public object: any;
    public listItems: Array<Dto.Select.Item.ISelectItem>;
    public kdModel: string;

    public onItemChanges() {
        if (typeof (this.kdChange()) === 'function') {
            this.kdChange()(this.object, this.kdModel);
        }
    }

    $onChanges(changesObj: any): void {
        if (changesObj.listItems) {
            if (changesObj.listItems.currentValue) {
                this.items = JSON.parse(JSON.stringify(changesObj.listItems.currentValue));
            }
        }
    }
}
