﻿import * as angular from "angular"

import controller from "./dropdown-single.controller";

export const name = "kd.component.dropdown-single";
const controllerName = "dropdownSingleController";
const componentName = "kdDropdownSingle";

angular
    .module(name, [ ])
    .controller(controllerName,
    [
        controller
    ])
    .component(componentName, {
        bindings: {
            listItems: '<',
            settings: '<',
            kdModel: '=',
            kdChange: '&',
            object: '<'
        },
        controller: controller,
        template: require("./dropdown-single.view.html")
    });
