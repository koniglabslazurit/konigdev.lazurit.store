﻿export default class DropDownSingleController {
    public isShow: boolean = false;
    public items: Array<Dto.Select.Item.ISelectItem> = [];
    public object: any;
    public settings: any;
    public kdChange: any;
    public kdModel: string;

    public constructor() {
    }

    public itemSelect(): void {
        this.isShow = false;
        if (typeof (this.kdChange()) === 'function') {
            this.kdChange()(this.kdModel, this.object);
        }
        console.log(this.kdModel);

    };

    public $onChanges(changesObj: any): void {
        if (changesObj.listItems) {
            if (changesObj.listItems.currentValue) {
                this.items = JSON.parse(JSON.stringify(changesObj.listItems.currentValue));
            }
        }
    };
};