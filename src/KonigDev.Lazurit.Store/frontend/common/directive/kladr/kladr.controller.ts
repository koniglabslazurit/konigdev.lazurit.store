﻿export default class KladrController {

    public deliveryList: Array<Dto.Delivery.IDelivery>;
    public selectedDelivery: Dto.Delivery.IDelivery;

    constructor() {

        this.init();
    }


    private init() {
        this.getDeliveryList();
        this.selectedDelivery = this.deliveryList.filter((d) => { return d.id === "adressDelivery" })[0];
    }

    private getDeliveryList() {
        this.deliveryList = [
            { id: "adressDelivery", name: "Адресная доставка" },
            { id: "selfDelivery", name: "Самовывоз" },
        ];
    }

    public selectDelivery = (item: Dto.Delivery.IDelivery) => {
        this.selectedDelivery = item;
    }
}