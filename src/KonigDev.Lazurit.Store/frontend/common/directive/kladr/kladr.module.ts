﻿import * as angular from "angular";

import kladrController from "./kladr.controller";

export const name = "kd.common.directive.kladr";
export const kladrDirectiveName = "kdKladr";
export const kladrControllerName = "KladrController";

angular
    .module(name, [])
    .directive(kladrDirectiveName, ["$timeout", ($timeout) => {
        let directive: angular.IDirective = {
            restrict: "E",
            template: require("./kladr.view.html"),
            scope: {
                model: '=',
                kladrCode: '=kladrCode'
            },
            link: function (scope: any, element: any, attrs: any) {
                $timeout(() => {
                    (<any>$).kladr.setDefault({
                        parentInput: function (model) {
                            if (model) {
                                return '#' + scope.model.id;
                            } else {
                                return ' ';
                            };
                        },
                        verify: true,
                        select: function (obj) {
                            if (obj) {
                                if ((<any>$(this))[0].name === 'city') {
                                    scope.model.cityId = obj.id;
                                    scope.model.city = obj.name;
                                    scope.model.KladrCode = obj.id;
                                } else if ((<any>$(this))[0].name === 'street') {
                                    scope.model.streetId = obj.id;
                                    scope.model.street = obj.name;
                                    scope.model.KladrCode = obj.id;
                                }
                                //} else if ((<any>$(this))[0].name === 'house') {
                                //    scope.model.house = obj.name;
                                //    scope.model.KladrCode = obj.id;
                                //}
                            }
                            //scope.model.address = (<any>$).kladr.getAddress('#' + scope.model.id);
                        }
                    });

                    element.find('[name="city"]').kladr('type', (<any>$).kladr.type.city);
                    element.find('[name="street"]').kladr('type', (<any>$).kladr.type.street);
                    //element.find('[name="house"]').kladr('type', (<any>$).kladr.type.building);
                }, 0);
            }
        };
        return directive;
    }])
    .controller(kladrControllerName,
    [
        kladrController
    ]);
