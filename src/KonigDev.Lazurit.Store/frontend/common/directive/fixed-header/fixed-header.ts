﻿import * as angular from "angular";

export const name = "kd.common.directive.fixed-header";
export const fixedHeaderDirectiveName = "kdFixedHeader";

angular
    .module(name, [])
    .directive(fixedHeaderDirectiveName, ["$timeout", ($timeout) => {
        let directive: angular.IDirective = {
            restrict: "A",
            link: function (scope: angular.IScope, element: any, attrs: angular.IAttributes) {
                $timeout(() => {
                    element.stickyTableHeaders();
                }, 0);
            }
        };
        return directive;
    }]);
