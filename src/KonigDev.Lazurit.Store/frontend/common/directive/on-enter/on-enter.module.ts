﻿/* TODO директива не дописана. На отработку на энтер */
import * as angular from "angular";

const name = "kd.common.directive.onenter";
const onEnterDirectiveName = "kdOnenter";

angular
    .module(name, [ ])
    .directive(onEnterDirectiveName, ["$timeout", ($timeout) => {
        let directive: angular.IDirective = {
            restrict: "A",
            link: function (scope: angular.IScope, element: any, attrs: angular.IAttributes) {
                element.bind("keydown keypress", function (event) {
                    if (event.which === 13) {
                        scope.$apply(function () {

                        });
                        event.preventDefault();
                    }
                });
            }
        };
        return directive;
    }]);
