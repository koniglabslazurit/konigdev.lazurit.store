﻿import * as angular from "angular";

export const name = "kd.common.directive.bond";
export const bondProductDirectiveName = "kdBond";
export const bondCollectionDirectiveName = "kdBondCollection";

angular
    .module(name, [])
    .directive(bondProductDirectiveName, ["$timeout", "$window", "$document", ($timeout: ng.ITimeoutService, $window: ng.IWindowService, $document: ng.IDocumentService) => {
        let directive: angular.IDirective = {
            restrict: "A",
            link: function (scope: angular.IScope, element: any, attrs: angular.IAttributes) {
                function onResize() {
                    if ($window.innerWidth > 1800) {
                        element.bond({
                            orientation: 'vert',
                            desktopMode: 'move',
                            maxSpeed: 3
                        });
                        let wrappers = $document[0].getElementsByClassName("bond-wrapper");
                        Array.prototype.forEach
                            .call(wrappers, (item: HTMLElement) => item.style.left = '0');
                    }
                    else {
                        element.bond({
                            orientation: 'horz',
                            desktopMode: 'move',
                            maxSpeed: 3
                        });
                    }
                }
                $timeout(onResize, 500);
                $window.addEventListener('resize', onResize);
            }
        };
        return directive;
    }])
    .directive(bondCollectionDirectiveName, ["$timeout", ($timeout) => {
        let directive: angular.IDirective = {
            restrict: "A",
            link: function (scope: angular.IScope, element: any, attrs: angular.IAttributes) {
                function onResize() {
                        element.bond({
                            orientation: 'horz',
                            desktopMode: 'move',
                            maxSpeed: 3
                        });
                }
                $timeout(onResize, 500);
            }
        };
        return directive;
    }]);
