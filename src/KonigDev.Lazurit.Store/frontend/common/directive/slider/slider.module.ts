﻿import * as angular from "angular";

export const name = "kd.common.directive.slider";
export const sliderDirectiveName = "kdSlider";

angular
    .module(name, [ ])
    .directive(sliderDirectiveName, ["$timeout", ($timeout) => {
        let directive: angular.IDirective = {
            restrict: "A",
            link: function (scope: angular.IScope, element: any, attrs: angular.IAttributes) {
                $timeout(() => {
                    element.lazSlider({
                        animatedDuration: 5000
                    });
                }, 2000);
            }
        };
        return directive;
    }]);
