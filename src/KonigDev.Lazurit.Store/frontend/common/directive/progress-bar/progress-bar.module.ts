﻿import * as angular from "angular";

import controller from "./progress-bar.controller";

export const name = "kd.common.directive.progress-bar";
export const progressBarComponentName = "kdProgressBar";
export const controllerName = "progressBarController";

angular.module(name, [])
    .controller(controllerName,
    [
        controller
    ])
    .component(progressBarComponentName, {
        bindings: {
            message: '<',
            isInprogress: '<'
        },
        controller: controller,
        template: require("./progress-bar.view.html")
    });