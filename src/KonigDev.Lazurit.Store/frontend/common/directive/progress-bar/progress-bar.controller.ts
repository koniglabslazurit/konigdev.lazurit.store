﻿export default class ProgressBarController {
    public message: string;
    public isInprogress: boolean;

    constructor() {
        this.isInprogress = false;
    }    
}