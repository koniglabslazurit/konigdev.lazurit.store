﻿import * as angular from "angular";
import "angular-i18n/ru-ru";

let uiBootstrap = require("angular-ui-bootstrap");

import controller from "./admin-date-picker-range.controller";

/*constants*/
export const name = "kd.component.admin-date-picker-range";
const controllerName = "adminDatePickerRangeController";
const componentName = "kdAdminDatePickerRange";

angular
    .module(name, [
        uiBootstrap
    ])
    .controller(controllerName,
    [
        controller
    ])
    .component(componentName, {
        bindings: {
            options: '<',
            settings: '<',
            kdChange: '&',
            kdModel: '='
        },
        controller: controller,
        template: require("./admin-date-picker-range.view.html")
    });
