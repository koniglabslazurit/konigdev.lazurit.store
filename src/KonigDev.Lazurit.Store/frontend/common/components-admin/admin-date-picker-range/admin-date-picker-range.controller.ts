﻿export default class AdminDatePickerRangeController {
    public options: any;
    public settings: any;
    public kdChange: any;
    public kdModel: any;
        
    public isOpen: boolean = false;

    public constructor() {
    }

    public open() {
        this.isOpen = true;
    };

};