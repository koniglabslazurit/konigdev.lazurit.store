﻿namespace Common.Message {
    export interface IMessage {
        error(userMessage: string, systemMessage: string): void;
        uiError(message: string): void;
        info(message: string): void;
        success(message: string): void;
        warning(message: string): void;
        functionalityInDev(): void;
    }
}