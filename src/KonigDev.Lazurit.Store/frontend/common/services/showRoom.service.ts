﻿export default class ShowRoomService {
    getShowRoomUrl: '/api/showrooms?cityId=';

    constructor(private $http: angular.IHttpService) {
    }

    getShowRoom(cityId) {
        return this.$http.get(this.getShowRoomUrl + cityId,
            (response) => {
                return response.Data;
            });
    }
}