﻿
export default class CommonService implements Common.Services.IEventService {
    constructor(private $window: angular.IWindowService) { }

    public suscribeOnEvent(eventName, callback): void {
        $(window).on(eventName, callback);
        //this.$window.addEventListener(eventName, callback);
    };

    public triggerEvent(eventName): void {
        //jq.event().trigger(eventName);
        //let event = new Event(eventName);
        //this.$window.dispatchEvent(event);
        (<any>$).event.trigger(eventName);
    }
};