﻿export default class UserService {
    getUsersUrl: '/api/users?showroomId=';

    constructor(private $http: angular.IHttpService) {
    }

    getUsers(showroomId) {

        return this.$http.get(this.getUsersUrl + showroomId,
            (response) => {
                return response.Data;
            });
    }
}