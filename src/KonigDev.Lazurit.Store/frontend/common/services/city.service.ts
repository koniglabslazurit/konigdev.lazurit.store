﻿export default class CityService {
    getAllUrl = '/api/cities';

    constructor(private $http: angular.IHttpService) {
    }

    public getAll() {
        return this.$http.get(this.getAllUrl,
            (response) => {
                return response.Data;
            });
    }
}