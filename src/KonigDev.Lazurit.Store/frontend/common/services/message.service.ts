﻿export default class MessageService implements Common.Message.IMessage {
    constructor(private toastr: Toastr) {

    }
    public error(userMessage: string, systemMessage: string): void {
        this.toastr.error(userMessage);
        console.log(systemMessage);
    };

    public uiError(message: string): void {
        this.toastr.error(message);
    };

    public info(message: string): void {
        this.toastr.info(message);
    };

    public success(message: string): void {
        this.toastr.success(message);
    };

    public warning(message: string): void {
        this.toastr.warning(message);
    };

    public functionalityInDev(): void {
        this.toastr.warning("Функционал находится в разработке");
    };
}