﻿export default class CookieService {

    constructor(private $document: angular.IDocumentService) {
    }

    public getCookie(name: any): any {
        var matches = this.$document[0].cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    };
}