﻿namespace Common.Services {
    export interface IEventService {
        suscribeOnEvent(eventName, callback): void;
        triggerEvent(eventName): void;
    }
}