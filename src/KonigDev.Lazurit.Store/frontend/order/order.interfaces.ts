﻿namespace Order.ApiService {
    export interface IApiService {
        getOrders(params: any): angular.IPromise<any>;
        create(model: Dto.Order.VmOrderCreating): angular.IPromise<Dto.Order.VmOrderResponse>;
        updateApprove(orderId: string, isApproved: boolean): angular.IPromise<any>;
        updateOrder1CFields(orderId: string, orderNumber1C: string, creationTime1C: string): angular.IPromise<any>;
        getOrderDetails(orderId: string): angular.IPromise<Dto.Order.VmOrderResponse>;
    }
}