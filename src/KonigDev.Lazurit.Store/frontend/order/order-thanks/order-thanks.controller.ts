﻿export default class OrderThanksController {

    private orderId: string;
    private rk: any;
    public roboPaymentMethods: Array<any>;
    private sberbankPaymentUrl: string = "/pay/sberbank/";
    private order: any;
    public isOnlinePay: boolean;
    private roboPaymentList: Array<string>;

    constructor(public orderApiServiceName: Order.ApiService.IApiService
        , private $window: angular.IWindowService
        , public constants: Constants.IConstants
        , public cartFullService: CartFull.Services.ICartFullService
        , private apiCartService: Cart.Services.ICartService
        , public messageService: Common.Message.IMessage) {
        this.init();
    }

    private init() {
        this.roboPaymentList = [
            this.constants.order.paymentMethods.yandexMoney,
            this.constants.order.paymentMethods.payPal,
            this.constants.order.paymentMethods.qiwiWallet
        ];
        this.isOnlinePay = false;
            this.orderApiServiceName.getOrderDetails(this.orderId)
                .then((response) => {
                    this.order = response;
                    if (this.roboPaymentList.indexOf(this.order.paymentMethod) > -1 || this.order.paymentMethod === this.constants.order.paymentMethods.bankCard) {
                        this.isOnlinePay = true;
                    }
                })
                .catch((err) => {
                    this.messageService.error(err, err);
            });        
    };

    public makePayment() {
        if (this.roboPaymentList.indexOf(this.order.paymentMethod) > -1) {
            this.rk = JSON.parse(JSON.stringify(this.order));
            var form = this.cartFullService.findElementByName("robokassaForm")
            form.action = this.rk.robokassaUrl;
            form[0].value = this.rk.mrchLogin;
            form[1].value = this.rk.sum;
            form[2].value = this.rk.number;
            form[3].value = this.rk.desc;
            form[4].value = this.rk.signatureValue;
            //this.rk.incCurrLabel = this.roboPaymentMethods.filter((t) => { return t.techName === "QiwiWallet" })[0].incCurrLabel;
            //form[5].value = this.rk.incCurrLabel;
            //todo remove for real shop
            this.rk.isTest = 1;
            form[5].value = this.rk.isTest;
            form.submit();
        }
        else if (this.order.paymentMethod === this.constants.order.paymentMethods.bankCard) {
            this.$window.location.assign(this.sberbankPaymentUrl);
        }        
    };
                      
}