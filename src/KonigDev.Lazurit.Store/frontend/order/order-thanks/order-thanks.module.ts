﻿import * as angular from "angular";

import * as orderApiModule from "./../order.module";
import * as constantsModule from "../../constants/constants.module";
import * as cartFullModule from "../../cart/cart-full/cart-full.module";
import * as cartServiceModule from "../../cart/cart-service/cart-full-service.module";
import * as commonModule from "../../common/common.module";

import controller from "./order-thanks.controller";

/* constants */
export const name = "kd.order.thanks";
export const controllerName = "orderThanksController";
const componentName = "kdOrderThanks";


angular
    .module(name,
    [
        orderApiModule.name,
        constantsModule.name,
        cartFullModule.name,
        cartServiceModule.name,
        commonModule.name
    ])
    .controller(controllerName, [
        orderApiModule.oderApiServiceName
        , '$window'
        , constantsModule.serviceName
        , cartFullModule.cartFullServiceName
        , cartServiceModule.cartServiceName
        , commonModule.messageServiceName
        , controller])
    .component(componentName,
    {
        bindings: {
            orderId: '@'
        },
        controller: controllerName,
        template: require("./order-thanks.view.html")
    });