﻿import * as angular from "angular";

import oderApiService from "./order.service.api";

/* constants */
export const name = "kd.cart.order";
export const oderApiServiceName = "orderApiService";

angular
    .module(name, [ ])
    .service(oderApiServiceName, ["$http", oderApiService]);