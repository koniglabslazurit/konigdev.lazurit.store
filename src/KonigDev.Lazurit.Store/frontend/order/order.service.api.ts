﻿export default class OrderApiService implements Order.ApiService.IApiService {

    private rest: string = '/api/order';
    private updateApproveUrl: string = '/api/order/approve';
    private updateOrder1CFieldseUrl: string = '/api/order/1c-fields'
    private getOrderDetailsUrl: string = '/api/order/details';

    constructor(public $http: angular.IHttpService) {
    }

    public getOrders(params: any): any {
        return this.$http
            .get(this.rest, { params: params })
            .then((response: any) => {
                return response.data;
            });
    };

    public create(model: Dto.Order.VmOrderCreating): angular.IPromise<Dto.Order.VmOrderResponse> {
        return this.$http
            .post(this.rest, model)
            .then((request: any) => {
                return request.data;
            });
    }

    public updateApprove(orderId: string, isApproved: boolean): any {
        let model = {
            Id: orderId,
            IsApproved: isApproved
        }
        return this.$http
            .post(this.updateApproveUrl, model)
            .then((request: any) => {
                return request;
            });
    }

    public updateOrder1CFields(orderId: string, orderNumber1C: string, creationTime1C: string): any {
        let model = {
            Id: orderId,
            OrderNumber1C: orderNumber1C,
            CreationTime1C: creationTime1C
        }
        return this.$http
            .post(this.updateOrder1CFieldseUrl, model)
            .then((request: any) => {
                return request;
            });
    }

    public getOrderDetails(orderId: string): angular.IPromise<Dto.Order.VmOrderResponse> {
        return this.$http
            .get(this.getOrderDetailsUrl, { params: { id: orderId } })
            .then((response: any) => {
                return response.data;
            });
    }
}