﻿import * as angular from "angular";

import service from "./furniture-type.api.service";
import controller from "./furniture-type.controller"

export const name = "kd.furniture";
export const serviceName = "FurnitureTypeService"
const furnitureTypeComponentName = "kdFurnitureTypes";
const controllerName = "furnitureTypeController";


angular
    .module(name, [ ])
    .service(serviceName,
    [
        "$http", service
    ])
    .controller(controllerName,
    [
        controller
    ])
    .component(furnitureTypeComponentName, {
        bindings: {
            listItems: '<',
            kdOnClick: '&'
        },
        controller: controller,
        template: require("./furniture-type.view.html")
    });
