﻿export default class FurnitureTypeService implements FurnitureType.Services.IFurnitureTypeApiService {
    private url = '/api/furnituretype/furniture-types';
    private urlIntergration = '/api/furnituretype/integrations';
    private urlByProduct = '/api/furnituretype/single-product';

    constructor(private $http: angular.IHttpService) { }   

    public getFurnitureTypes(request: any): angular.IPromise<Array<any>> {
        return this.$http
            .get(this.url, { params: request })
            .then((response: angular.IHttpPromiseCallbackArg<Array<any>>) => {
                return response.data;
            });
    };

    public getFurnitureTypesByProduct(request: Dto.Product.ProductRequest): angular.IPromise<Array<Dto.FurnitureType.DtoFurnitureTypeItem>> {
        return this.$http
            .get(this.urlByProduct, { params: request })
            .then((response: angular.IHttpPromiseCallbackArg<Array<Dto.FurnitureType.DtoFurnitureTypeItem>>) => {
                return response.data;
            });
    };

    public getFurnitureTypesForIntegration(request: Dto.FurnitureType.GetFurnitureTypesQuery): angular.IPromise<Array<Dto.FurnitureType.DtoFurnitureTypeItem>> {
        return this.$http
            .get(this.urlIntergration, { params: request })
            .then((response: angular.IHttpPromiseCallbackArg<Array<Dto.FurnitureType.DtoFurnitureTypeItem>>) => {
                return response.data;
            });
    };
}