﻿namespace FurnitureType.Services {
    export interface IFurnitureTypeApiService {
        getFurnitureTypes(request): angular.IPromise<Array<any>>;
        getFurnitureTypesForIntegration(request: Dto.FurnitureType.GetFurnitureTypesQuery): angular.IPromise<Array<Dto.FurnitureType.DtoFurnitureTypeItem>>;
        getFurnitureTypesByProduct(request: Dto.Product.ProductRequest): angular.IPromise<Array<Dto.FurnitureType.DtoFurnitureTypeItem>>;
    }
}