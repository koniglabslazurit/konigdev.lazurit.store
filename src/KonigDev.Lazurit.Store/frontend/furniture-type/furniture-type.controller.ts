﻿export default class FurnitureTypesCtrl {
    private listItems: Array<Dto.Product.IFurnitureType>;
    private totalCount: number;
    private kdOnClick: any;

    $onChanges(changesObj: any): void {
        if (changesObj.listItems) {
            if (changesObj.listItems.currentValue) {
                var count = 0;
                changesObj.listItems.currentValue.forEach((i:Dto.Product.IFurnitureType) => {
                    count = count + i.amount;
                });
                this.totalCount = count;
            }
        }
    }

    public onClick(request: any): void {
        if (typeof (this.kdOnClick) === 'function') {
            //первым вызовом находим callback, вторым вызываем его с параметром
            this.kdOnClick()(request);
        }
    }

    isAnyTypeIsSelected(): boolean {
        if (this.listItems && this.listItems.length > 0) {
            return this.listItems.some((type: Dto.Product.IFurnitureType) => type.isActive === true);
        }
        else return false;
    }
};