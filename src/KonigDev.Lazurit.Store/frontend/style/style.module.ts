﻿import * as angular from "angular";

import service from "./style.api.service";

export const name = "kd.style";
export const serviceName = "styleService";

angular
    .module(name, [ ])
    .service(serviceName,
    [
        "$http",
        service
    ]);
