﻿export default class StyleService implements Style.Services.IStyleApiService {

    private url: string = "/api/style";

    constructor(public $http: angular.IHttpService) {
    }

    public getStyles(): angular.IPromise<Array<Dto.Style.DtoStyleItem>> {
        return this.$http
            .get(this.url)
            .then((response: angular.IHttpPromiseCallbackArg<Array<Dto.Style.DtoStyleItem>>) => {
                return response.data;
            });
    }
}