﻿namespace Style.Services {
    export interface IStyleApiService {
        getStyles(): angular.IPromise<Array<Dto.Style.DtoStyleItem>>;
    }
}