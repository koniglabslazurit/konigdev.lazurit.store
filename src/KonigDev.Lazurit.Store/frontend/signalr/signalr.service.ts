﻿export default class SignalrService {
    private _jquery: any;
    private _connection: any;
    private _onConnection: () => void;
    private _stopConnection: () => void;
    private _refreshCart: () => void;
    private _updateMiniCartByData: (data:any) => void;

    constructor(
        private $timeout: angular.ITimeoutService,
        jquery: any,
        public cartIconHubService: Cart.Hub.ICartIconHubService,
        public cartMiniHubService: Cart.Hub.ICartMiniHubService
        ) {
        this._jquery = jquery;
        this._onConnection = () => {
            console.log("On connection");
        };
        this._stopConnection = () => {
            console.log("Stop connection");
        };
        this._refreshCart = () => {
            console.log("Refresh cart");
            cartIconHubService.updateCart();
            cartMiniHubService.updateCart();
        };
        this._updateMiniCartByData = (data: any) => {
            $timeout(() => this._onUpdateMiniCartDate(data));
        }
    }

    private _onUpdateMiniCartDate(data: any) {
        console.log("_updateMiniCartByData cart");
        console.log(data);
        this.cartIconHubService.mappingNewCart(data);
        this.cartMiniHubService.mappingNewCart(data);
    }

    public start(): void {
        var proxy = this._jquery.connection.commonHub;
        this.cartMiniHubService.setCartCookies().then((data: any) => {
            this._jquery.connection.hub.start()
                .done(() => {
                    proxy.server.connect();
                });
        });

        proxy.client.onConnected = this._onConnection;
        proxy.client.refreshCart = this._refreshCart;
        proxy.client.updateMiniCartByData = this._updateMiniCartByData;
    };

    public stop(): void {
        this._stopConnection();
    };
}