﻿//dependencies
import * as angular from "angular";
import * as cartHubModule from "../cart/cart-hub/cart-hub.module";

import signalrService from "./signalr.service";

//constants
export const name = "kd.signalr";
export const serviceName = "kd.signalr.service";

//module
angular
    .module(name,
    [
        cartHubModule.name
    ])
    .factory("jquery", [
        "$window",
        ($window: any) => { return $window.$ || {}; }
    ])
    .service(serviceName, [
        "$timeout",
        "jquery",
        cartHubModule.cartIconHubServiceName,
        cartHubModule.cartMiniHubServiceName,
        signalrService
    ]);
