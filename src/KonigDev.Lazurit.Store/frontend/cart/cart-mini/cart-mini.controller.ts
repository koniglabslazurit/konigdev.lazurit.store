﻿export default class CartMiniController {

    private isMiniCartEnabled: boolean;
    private cartMapped: Dto.CartMini.ICartMiniContent;
    //private getCart: () => void;
    private fullCartUrl: string = "/personal/cart/";
    private sberbankPaymentUrl: string = "/pay/sberbank/";
    private profile: any;
    private isRequiredSeller: boolean;
    /*рассрочка*/
    private installments: Array<Dto.Filter.IInstallmentFilter>;
    private selectedInstallment: Dto.Filter.IInstallmentFilter;
    /*общая сумма*/
    private totalPriceAcount: number;
    private totalSavingAcount: number;

    constructor(private $window: angular.IWindowService,
        private cartService: Cart.Services.ICartService,
        private commonService: Common.Services.IEventService,
        public constants: Constants.IConstants,
        public messageService: Common.Message.IMessage,
        public profileService: any,
        public orderApiService: Order.ApiService.IApiService,
        public cartMiniHubService: Cart.Hub.ICartMiniHubService,
        private installmentServiceModule: ProductsInstallment.Service) {
        //event that means that user has clicked on cart-icon button in cart-icon component
        commonService.suscribeOnEvent('showCartEvent', this.showCart.bind(this));
        this.init();        
    }

    private init() {
        //initial cart update
        this.cartMiniHubService.updateCart();
        this.getProfile();
        //reference from service that provides info about cart items whenever cart is changed
        this.cartMapped = this.cartMiniHubService.cartMapped;
        /*рассрочка*/
        this.installments = this.installmentServiceModule.getInstallmentList();
        this.selectedInstallment = this.installments.filter((i) => { return i.id === "24" })[0];
    };

    //delete items methods
    private deleteProductFromCart(productId: string): void {
        this.cartService.deleteProductFromCart(productId)
            .then((data) => {
                this.cartMiniHubService.updateCart();
            })
            .catch((error: any) => {
                this.messageService.error(error.data.message, error);
            })
    }

    private deleteItemFullCollectionFromCart(collectionId: string): void {
        this.cartService.deleteItemFullCollectionFromCart(collectionId)
            .then((data) => {
                this.cartMiniHubService.updateCart();
            })
            .catch((error: any) => {
                this.messageService.error(error.data.message, error);
            })
    }

    private deleteComplectFromCart(complectId: string): void {
        this.cartService.deleteItemComplectFromCart(complectId)
            .then((data) => {
                this.cartMiniHubService.updateCart();
            })
            .catch((error: any) => {
                this.messageService.error(error.data.message, error);
            })
    }

    //methods for price calculation
    private calculatePriceForProduct(productItem: Dto.CartMini.ICartMiniProduct): string {        
        return productItem.salePriceInstallment24MonthWithAssembly.formattedAmountWithoutPoint;
    }

    private calculatePriceForCollection(collectionItem: Dto.CartMini.ICartMiniCollection): string {
        var price = 0;
        for (var product of collectionItem.products) {
            price = price + Math.round(product.salePriceInstallment24MonthWithAssembly.amount);            
        }
        return price + " руб";        
    }

    private calculatePriceForComplect(complectItem: Dto.CartMini.ICartMiniComplect): string {
        /*считаем стоимость по рассрочке 24 месяца*/
        return complectItem.salePriceInstallment24MonthWithAssembly.formattedAmountWithoutPoint;
    }

    private calculateTotalPrice(): string {
        this.totalPriceAcount = 0;            
        if (this.cartMapped.products) {
            for (var product of this.cartMapped.products) {
                this.totalPriceAcount = this.totalPriceAcount + Math.round(product.salePriceInstallment24MonthWithAssembly.amount);
            }
            for (var complect of this.cartMapped.complects) {
                this.totalPriceAcount = this.totalPriceAcount + Math.round(complect.salePriceInstallment24MonthWithAssembly.amount);
            }
            for (var collection of this.cartMapped.collections) {
                for (var product of collection.products) {
                    this.totalPriceAcount = this.totalPriceAcount + Math.round(product.salePriceInstallment24MonthWithAssembly.amount);
                }
            }
        }
        return this.totalPriceAcount + " руб";
    }

    private calculateTotalSaving(): string {
        this.totalSavingAcount = 0;
        if (this.cartMapped.products) {
            for (var product of this.cartMapped.products) {
                var saving = Math.round(product.price.amount) - (Math.round(product.salePriceInstallment24MonthWithAssembly.amount) - Math.round(product.priceForAssembly.amount));
                this.totalSavingAcount = this.totalSavingAcount + saving;
            }
            for (var complect of this.cartMapped.complects) {
                var saving = Math.round(complect.price.amount) - (Math.round(complect.salePriceInstallment24MonthWithAssembly.amount) - Math.round(complect.priceForAssembly.amount));
                this.totalSavingAcount = this.totalSavingAcount + saving;
            }
            for (var collection of this.cartMapped.collections) {
                for (var product of collection.products) {
                    var saving = Math.round(product.price.amount) - (Math.round(product.salePriceInstallment24MonthWithAssembly.amount) - Math.round(product.priceForAssembly.amount));
                    this.totalSavingAcount = this.totalSavingAcount + saving;
                }
            }
        }
        return this.totalSavingAcount + " руб";
    }

    private calculateInstallmentPayment(): string {
        return Math.round(this.totalPriceAcount / 24) + " руб";
    }

    //methods for viewing
    private showCart(): void {
        //if current url contains /test/ word then we are in full cart already => do nothing
        if (!/cart/.test(this.$window.location.href)) {
            //if car mini window is showed already => close
            if (this.isMiniCartEnabled) {
                this.isMiniCartEnabled = false
            }
            else {
                //if cart is not empty then show it
                if (this.cartMiniHubService.cartItemsCount > 0) {
                    this.isMiniCartEnabled = true;
                }
                //if cart is empty go to full cart
                else {
                    if (!/cart/.test(this.$window.location.href)) {
                        this.goToCart();
                    }
                }
            }
        }
    };

    private getProfile(): void {
        this.profileService.getProfile()
            .then((data) => {
                this.profile = data;
            });
    }

    private goToCart(): void {
        this.$window.location.assign(this.fullCartUrl);
    };


    public makePayment(): void {
        if (!this.profileService.checkProfileValidityForCartMini(this.profile)) {
            this.$window.location.assign(this.fullCartUrl);
            return;
        }
        this.cartService.checkIsApprovalRequired()
            .then((response: Dto.Order.ApprovalSign.IApprovalSign) => {
                this.isRequiredSeller = response.isApprovalRequired;
                return response;
            })
            .then((response: any) => {
                return this.cartService.getPaymentMethods();
            })
            .then((response: Array<Dto.Order.PaymentMethod.IPaymentMethod>) => {
                let model: Dto.Order.VmOrderCreating = {
                    paymentMethodId: response.filter((i: Dto.Order.PaymentMethod.IPaymentMethod) => { return i.techName === "BankCard" })[0].id,
                    deliveryKladrCode: this.profile.userAddresses.filter((i: any) => { return i.isDefault === true })[0].kladrCode,
                    deliveryAddress: this.profile.userAddresses.filter((i: any) => { return i.isDefault === true })[0].address,
                    isRequiredSeller: this.isRequiredSeller
                }
                return this.orderApiService.create(model);
            })
            .then((response: Dto.Order.VmOrderResponse) => {
                if (this.isRequiredSeller) {
                    this.messageService.success(this.constants.cart.consultantWillContact);
                    return
                }
                this.$window.location.assign(this.sberbankPaymentUrl);
            });
    }
}