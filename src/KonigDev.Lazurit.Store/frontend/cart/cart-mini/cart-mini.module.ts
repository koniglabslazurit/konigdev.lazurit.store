﻿import * as angular from "angular";

import cartMiniController from "./cart-mini.controller";
import * as cartServiceModule from "../cart-service/cart-mini-service.module";
import * as commonServiceModule from "../../common/common.module";
import * as constantsServiceModule from "../../constants/constants.module";
import * as profileServiceModule from "../../profile/profile-services/profile-services.module";
import * as orderModule from "../../order/order.module";
import * as cartHubModule from "../cart-hub/cart-hub.module";
import * as installmentServiceModule from "../../product/product-installment/products-installment-service/products-installment-service.module";

/* constants */
export const name = "kd.cart.mini";
const cartMiniControllerName = "cartMiniController";
const cartMiniComponentName = "cartMiniComponent";

angular
    .module(name,
    [
        cartServiceModule.name,
        commonServiceModule.name,
        constantsServiceModule.name,
        profileServiceModule.name,
        orderModule.name,
        cartHubModule.name,
        installmentServiceModule.name
    ])
    .controller(cartMiniControllerName,
    [
        '$window',
        cartServiceModule.cartServiceName,
        commonServiceModule.eventServiceName,
        constantsServiceModule.serviceName,
        commonServiceModule.messageServiceName,
        profileServiceModule.profileServiceName,
        orderModule.oderApiServiceName,
        cartHubModule.cartMiniHubServiceName,
        installmentServiceModule.serviceInstallmentName,
        cartMiniController
    ])
    .component(cartMiniComponentName, {
        bindings: {
        },
        controller: cartMiniControllerName,
        template: require("./cart-mini.view.html")
    });
