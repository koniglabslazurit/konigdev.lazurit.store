﻿import * as angular from "angular";

import cartIconHubService from "./cart-icon-hub.service";
import cartMiniHubService from "./cart-mini-hub.service";
import * as cartMiniApiModule from "../cart-service/cart-mini-service.module";
import * as geolocationModule from "../../geolocation/geolocation.module";

export const name = "kd.cart.hub";
export const cartIconHubServiceName = "cartIconHubService";
export const cartMiniHubServiceName = "cartMiniHubService";

angular
    .module(name,
    [
        cartMiniApiModule.name
    ])
    .service(cartIconHubServiceName,
    [
        cartMiniApiModule.cartServiceName,
        "$timeout",
        geolocationModule.geolocationServiceName,
        cartIconHubService
    ])
    .service(cartMiniHubServiceName,
    [
        cartMiniApiModule.cartServiceName,
        "$timeout",
        geolocationModule.geolocationServiceName,
        cartMiniHubService
    ]);
