﻿export default class CartIconHubService implements Cart.Hub.ICartIconHubService {
    public cartIcon: Dto.CartMini.ICartIcon = { id: "", count: 0 };
    private mappingCart: (data: any) => void;

    constructor(public cartMiniApiService: Cart.Services.ICartService,
        public $timeout: angular.ITimeoutService,
        private geolocationService: any) {
        this.mappingCart = (data: any): void => {
            //if cart has been created already
            if (data) {
                this.cartIcon.id = data.id;
                this.cartIcon.count = data.products.length + data.collections.length + data.complects.length;
            }
            //else cart hasn't been created or has been deleted already
            else {
                this.cartIcon.id = "";
                this.cartIcon.count = 0;
            }
        };
    }

    public updateCart(): void {
        this.geolocationService.getNearbyCities()
            .then((result: any) => {
                return this.cartMiniApiService.getCart()
            })
            .then((data: any) => {
                this.$timeout(() => {
                    this.mappingCart(data);
                });
            });
    };

    public mappingNewCart(data) {
        this.mappingCart(data);
    }
}