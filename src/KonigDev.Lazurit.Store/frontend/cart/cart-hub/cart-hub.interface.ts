﻿namespace Cart.Hub {
    export interface ICartIconHubService {
        cartIcon: Dto.CartMini.ICartIcon;
        updateCart(): void;
        mappingNewCart(data: any): void;
    }
}

namespace Cart.Hub {
    export interface ICartMiniHubService {
        cartMapped: Dto.CartMini.ICartMiniContent;
        cartItemsCount: number;
        updateCart(): void;
        mappingNewCart(data: any): void;
        setCartCookies(): any;
    }
}