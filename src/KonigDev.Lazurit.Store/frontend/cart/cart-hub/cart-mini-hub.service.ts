﻿export default class CartMiniHubService implements Cart.Hub.ICartMiniHubService {
    //variable for cart-mini component
    public cartMapped: Dto.CartMini.ICartMiniContent = <Dto.CartMini.ICartMiniContent>{};
    public cartItemsCount: number;
    private mappingCart: (data: any) => void;

    constructor(public cartMiniApiService: Cart.Services.ICartService,
        public $timeout: angular.ITimeoutService,
        private geolocationService: any    ) {
        this.mappingCart = (data: any): void => {
            //if cart has been created already
            if (data) {
                //mapping for cartMapped
                this.cartMapped.products = <Array<Dto.CartMini.ICartMiniProduct>>data.products;
                this.cartMapped.complects = <Array<Dto.CartMini.ICartMiniComplect>>data.complects;
                this.cartMapped.collections = <Array<Dto.CartMini.ICartMiniCollection>>data.collections
                //cart items count
                var productInCollectionCount = 0;
                for (var collection of this.cartMapped.collections) {
                    productInCollectionCount = productInCollectionCount + collection.products.length;
                }
                this.cartItemsCount = data.products.length + data.complects.length + productInCollectionCount;
                this.cartMapped.totalPrice = data.totalPrice;
            }
            //else cart hasn't been created or has been deleted already
            else {
                this.cartMapped.products.length = 0;
                this.cartMapped.complects.length = 0;
                this.cartMapped.collections.length = 0;
                this.cartItemsCount = 0;
            }
        };
    }

    public updateCart(): void {
        this.geolocationService.getNearbyCities()
            .then((result: any) => {
                return this.cartMiniApiService.getCart()
            })                                            
            .then((data: any) => {
                this.$timeout(() => {
                    this.mappingCart(data);
                });
        });
    };

    public mappingNewCart(data: any): void {
        this.mappingCart(data);
    }

    public setCartCookies(): any {
        return this.cartMiniApiService.setCartCookies();
    }
}