﻿import * as angular from "angular";

import addressPartController from "./address-part.controller";

/* constants */
export const name = "kd.cart.address.part";
const addressPartControllerName = "addressPartController";
const addressPartComponentName = "kdAddressPart";

angular
    .module(name,[ ])
    .controller(addressPartControllerName,
    [
        addressPartController
    ])
    .component(addressPartComponentName, {
        bindings: {            
            profile: "<"
        },
        controller: addressPartControllerName,
        template: require("./address-part.view.html")
    });
