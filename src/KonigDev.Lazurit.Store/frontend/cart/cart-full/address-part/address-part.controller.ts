﻿export default class AddressPartController {

    public profile: any;
    public defaultAddress: any;
    
    controller() {
    }


    map(): void {
        if (this.profile) {
        //костыль: должен выбираться дефолтный, но пока его нет берем первый
            //this.defaultAddress = this.profile.userAddresses.filter((i: any) => { return i.isDefault === true })[0];
            this.defaultAddress = this.profile.userAddresses[0];
            if (!this.defaultAddress) {
                this.defaultAddress = { address: "", isDefault: true, KladrCode: "" };
                this.profile.userAddresses.push(this.defaultAddress);
            }
        }
    };

    $onChanges(changesObj: any): void {
        if (changesObj.profile) {
            if (changesObj.profile.currentValue) {
                this.map();
            }
        }
    }

}