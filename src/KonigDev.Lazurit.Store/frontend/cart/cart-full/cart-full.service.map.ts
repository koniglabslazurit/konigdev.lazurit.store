export default class CartFullMapService implements CartFull.Services.ICartFullMapService {
    public constructor(private cartFullService: CartFull.Services.ICartFullService) {

    }

    public mappingCart(data: any): Dto.CartFull.ICart {        
        var cart: Dto.CartFull.ICart = {
            collections: [],
            complects: [],
            id: '',
            isDeliveryRequired: true,
            orderCount: null,
            priceForDelivery: null,
            products: [],
            totalPrice: null,
            totalPriceWithoutDelivery: null,
            saving: null,
            totalAssembly: null,
            installmentSum: null
        }
        
        if (!data) return cart;        
        cart.products = data.products.map((p: Dto.CartFull.IProductItem) => {
            p.summ = this.cartFullService.getSummProduct(p, undefined).sum;                    
            return p;
        });

        cart.complects = data.complects.map((c: Dto.CartFull.IComplectItem) => {
            c.summ = this.cartFullService.getSummComplect(c, undefined).sum;                   
            return c;
        });

        cart.collections = data.collections;
        var productInCollectionCount = 0;
        for (var collection of cart.collections) {
            productInCollectionCount = productInCollectionCount + collection.products.length;
            collection.products = collection.products.map((p: Dto.CartFull.IProductItem) => {
                p.summ = this.cartFullService.getSummProduct(p, undefined).sum;                
                return p;
            });
        }

        cart.orderCount = cart.products.length + cart.complects.length + productInCollectionCount;
        cart.id = data.id;
        cart.priceForDelivery = data.priceForDelivery.amount;
        cart.isDeliveryRequired = data.isDeliveryRequired;
        if (cart.products.length + cart.complects.length + cart.collections.length === 0) {
            cart.totalPrice = 0;
        } else {
            cart.totalPrice = data.totalPrice.amount;
        }
        return cart;
    }
}