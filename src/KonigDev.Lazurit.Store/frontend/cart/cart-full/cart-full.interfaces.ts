namespace CartFull.Services {
    export interface ICartFullMapService {
        /**
        * Мапинг данных в представление для корзины
        */
        mappingCart(data: any): Dto.CartFull.ICart;
    }

    export interface ICartFullService {
        getSummProduct(item: Dto.CartFull.IProductItem, installment: Dto.Filter.IInstallmentFilter): Dto.Sum.ISum;
        getSummComplect(complect: Dto.CartFull.IComplectItem, installment: Dto.Filter.IInstallmentFilter): Dto.Sum.ISum;
        getTotalSumm(cart: Dto.CartFull.ICart, installment: Dto.Filter.IInstallmentFilter): Dto.Sum.ITotalSum;
        findElementByName(name: string): any;
    }
}