﻿namespace Dto.CartFull {
    export interface ICart {
        id: string;
        products: Array<Dto.CartFull.IProductItem>;
        complects: Array<Dto.CartFull.IComplectItem>;
        collections: Array<Dto.CartFull.ICollectionItem>;
        orderCount: number;
        priceForDelivery: number;
        totalPrice: number;
        isDeliveryRequired: boolean;
        totalPriceWithoutDelivery: number;
        saving: number; 
        totalAssembly: number;
        installmentSum: number;
    }
    export interface IProductItem {
        id: string;
        summ: number;                                                   
        quantity: number;
        price: Dto.Money.IMoney;
        salePrice: Dto.Money.IMoney;
        salePriceInstallment6Month: Dto.Money.IMoney;
        salePriceInstallment10Month: Dto.Money.IMoney;
        salePriceInstallment12Month: Dto.Money.IMoney;
        salePriceInstallment18Month: Dto.Money.IMoney;
        salePriceInstallment24Month: Dto.Money.IMoney;
        salePriceInstallment36Month: Dto.Money.IMoney;
        salePriceInstallment48Month: Dto.Money.IMoney;
        salePriceWithAssembly: Dto.Money.IMoney;
        salePriceInstallment6MonthWithAssembly: Dto.Money.IMoney;
        salePriceInstallment10MonthWithAssembly: Dto.Money.IMoney;
        salePriceInstallment12MonthWithAssembly: Dto.Money.IMoney;
        salePriceInstallment18MonthWithAssembly: Dto.Money.IMoney;
        salePriceInstallment24MonthWithAssembly: Dto.Money.IMoney;
        salePriceInstallment36MonthWithAssembly: Dto.Money.IMoney;
        salePriceInstallment48MonthWithAssembly: Dto.Money.IMoney;
        priceInTotal: Dto.Money.IMoney;              
        discount: number;
        isAssemblyRequired: boolean;
        priceForAssembly: Dto.Money.IMoney;
    }
    export interface ICollectionItem {
        id: string;
        products: Array<Dto.CartFull.IProductItem>;
    }
    export interface IComplectItem {
        id: string;
        summ: number;           
        quantity: number;
        price: Dto.Money.IMoney;
        salePrice: Dto.Money.IMoney;
        salePriceInstallment6Month: Dto.Money.IMoney;
        salePriceInstallment10Month: Dto.Money.IMoney;
        salePriceInstallment12Month: Dto.Money.IMoney;
        salePriceInstallment18Month: Dto.Money.IMoney;
        salePriceInstallment24Month: Dto.Money.IMoney;
        salePriceInstallment36Month: Dto.Money.IMoney;
        salePriceInstallment48Month: Dto.Money.IMoney;
        salePriceWithAssembly: Dto.Money.IMoney;
        salePriceInstallment6MonthWithAssembly: Dto.Money.IMoney;
        salePriceInstallment10MonthWithAssembly: Dto.Money.IMoney;
        salePriceInstallment12MonthWithAssembly: Dto.Money.IMoney;
        salePriceInstallment18MonthWithAssembly: Dto.Money.IMoney;
        salePriceInstallment24MonthWithAssembly: Dto.Money.IMoney;
        salePriceInstallment36MonthWithAssembly: Dto.Money.IMoney;
        salePriceInstallment48MonthWithAssembly: Dto.Money.IMoney;
        priceInTotal: Dto.Money.IMoney;
        discount: number;
        isAssemblyRequired: boolean;
        priceForAssembly: Dto.Money.IMoney;
    }
}

namespace Dto.CartItem {
    export interface ICartFullComplect {
        id: string;
        quantity: number;
        salePrice: Dto.Money.IMoney;
        price: Dto.Money.IMoney;
        discount: number;
        isAssemblyRequired: boolean;
        priceForAssembly: Dto.Money.IMoney;
    }
    export interface ICartItem {
        id: string;
        sum: any;
    }
    export interface ICartFullItem {
        id: string;
        productId: string;
        product: Dto.Product.IProduct;
        quantity: number;
        price: Dto.Money.IMoney;
        salePrice: Dto.Money.IMoney;
        priceForAssembly: Dto.Money.IMoney;
        discount: number;
        isAssemblyRequired: boolean;
    }
}

namespace Dto.Sum {
    export interface ISum {
        sum: number;  
        assembly: number;
        saving: number;     
    }


    export interface ITotalSum {
        totalSum: number;
        totalSumOnly: number;
        totalAssembly: number;
        totalSaving: number;
        installmentSum: number;
    }
}
