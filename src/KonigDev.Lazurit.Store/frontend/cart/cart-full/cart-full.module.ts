﻿import * as angular from "angular";

/* import internal modules of cart */
import serviceMap from "./cart-full.service.map";
import cartFullController from "./cart-full.controller";
import service from "./cart-full.service";
import * as installmentServiceModule from "../../product/product-installment/products-installment-service/products-installment-service.module";

/* import full modules */
import * as cartServiceModule from "../cart-service/cart-full-service.module";
import * as constantsModule from "../../constants/constants.module";
import * as commonModule from "../../common/common.module";
import * as layawayModule from "../../layaway/layaway-service/layaway-service.module";
import * as addressPartModule from "./address-part/address-part.module";
import * as profilePartModule from "./profile-part/profile-part.module";
import * as profileServiceModule from "../../profile/profile-services/profile-services.module";
import * as orderModule from "../../order/order.module";

/* constants */
export const name = "kd.cart.full";
export const cartFullServiceName = "cartFullService";
const cartFullServiceMapName = "cartFullMapService";
const cartFullControllerName = "cartFullController";
const cartFullComponentName = "kdCartFull";

angular
    .module(name,
    [
        cartServiceModule.name,
        constantsModule.name,
        commonModule.name,
        layawayModule.name,
        addressPartModule.name,
        profilePartModule.name,
        profileServiceModule.name,
        orderModule.name
    ])
    .service(cartFullServiceName, [service])
    .service(cartFullServiceMapName, [cartFullServiceName, serviceMap])
    .controller(cartFullControllerName,
    [
        cartServiceModule.cartServiceName,
        cartFullServiceName,
        constantsModule.serviceName,
        commonModule.messageServiceName,
        layawayModule.layawayServiceName,
        cartFullServiceMapName,
        profileServiceModule.profileServiceName,
        orderModule.oderApiServiceName,
        installmentServiceModule.serviceInstallmentName,
        '$window',
        cartFullController
    ])
    .component(cartFullComponentName, {
        bindings: {
        },
        controller: cartFullControllerName,
        template: require("./cart-full.view.html")
    });
