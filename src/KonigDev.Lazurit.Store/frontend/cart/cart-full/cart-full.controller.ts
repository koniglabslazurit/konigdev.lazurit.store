﻿export default class CartFullController {

    public cart: Dto.CartFull.ICart;
    public profile: any;
    public layaways: any[];
    public layawayItemsCount: number;
    public paymentMethods: any[];
    public selectedPaymentMethod: any;
    public isShowOrder: boolean = true;
    public cartItemsCount: number;
    private isRequiredSeller: boolean;
    private orderThanksUrl: string = "/order/thanks/:id";
    /*рассрочка*/
    public installments: Array<Dto.Filter.IInstallmentFilter>;
    public selectedInstallment: Dto.Filter.IInstallmentFilter;

    constructor(private apiCartService: Cart.Services.ICartService,
        public cartFullService: CartFull.Services.ICartFullService,
        public constants: Constants.IConstants,
        public messageService: Common.Message.IMessage,
        public layawayApiService: any,
        public cartFullMapService: CartFull.Services.ICartFullMapService,
        public profileService: any,
        public orderApiService: Order.ApiService.IApiService,
        private installmentServiceModule: ProductsInstallment.Service,
        private $window: angular.IWindowService) {
        this.getInstallments();
        this.init();
        this.layawayItemsCount = 0;
    }

    private init() {
        this.getCart();
        this.getLayaways();
        this.getProfile();
        this.getPaymentMethods();
    };

    private mappingCart(data: any): void {
        this.cart = this.cartFullMapService.mappingCart(data);
        this.refreshCartItemsCount();
        this.recountTotalCart();
    }

    private refreshCartItemsCount() {
        this.cartItemsCount = this.cart.collections.length + this.cart.complects.length + this.cart.products.length;
    }

    private getCart(): void {
        this.apiCartService.getCart()
            .then((data) => {
                this.mappingCart(data);
            });
    }

    private deleteCart(): void {
        this.apiCartService.deleteCart()
            .then((data) => {
                this.mappingCart(null);
                this.messageService.success("Корзина была успешно очищена");
            })
            .catch((error) => {
                console.log(error);
            });
    }

    public refreshCart = () => {
        this.getCart();
        this.getLayaways();
    }

    private getLayaways(): void {
        this.layawayApiService.getLayaway()
            .then((response: any) => {
                this.layaways = response;
                this.layawayItemsCount = 0;
                this.layaways.forEach((l: any) => {
                    this.layawayItemsCount += l.products.length + l.collections.length + l.complects.length;
                });
            });
    };

    private getInstallments(): void {
        /*рассрочка*/
        this.installments = this.installmentServiceModule.getInstallmentList();
    };

    public selectInstallment = (item: Dto.Filter.IInstallmentFilter): void => {
        this.selectedInstallment = item;
        this.recountTotalCart();
    };

    public onChangeProductAssembly(item: any): void {
        item.summ = this.getSummProduct(item).sum;
        this.setAssemblyForAll(item.isAssemblyRequired);
        this.recountTotalCart();
    };

    public onChangeComplectAssembly(item: any): void {
        item.summ = this.getSummComplect(item).sum;
        this.setAssemblyForAll(item.isAssemblyRequired);
        this.recountTotalCart();
    };

    public setAssemblyForAll(isAssembly) {
        for (var product of this.cart.products) {
            product.isAssemblyRequired = isAssembly;
            product.summ = this.getSummProduct(product).sum;
        }
        for (var collection of this.cart.collections) {
            for (var product of collection.products) {
                product.isAssemblyRequired = isAssembly;
                product.summ = this.getSummProduct(product).sum;
            }
        }
        for (var complect of this.cart.complects) {
            complect.isAssemblyRequired = isAssembly;
            complect.summ = this.getSummComplect(complect).sum;
        }
    };

    public changeDeliveryRequired(): void {
        this.recountTotalCart();
    };

    private recountTotalCart() {
        var dto = this.getTotalSumm();
        this.cart.totalPrice = dto.totalSum;
        this.cart.totalPriceWithoutDelivery = dto.totalSumOnly;
        this.cart.saving = dto.totalSaving;
        this.cart.totalAssembly = dto.totalAssembly;
        this.cart.installmentSum = dto.installmentSum;
    };

    /* calc summ product */
    public getSummProduct(item: Dto.CartFull.IProductItem): Dto.Sum.ISum {        
        return this.cartFullService.getSummProduct(item, this.selectedInstallment);        
    };

    /* calc summ complect */
    public getSummComplect(complect: Dto.CartFull.IComplectItem): Dto.Sum.ISum {
        return this.cartFullService.getSummComplect(complect, this.selectedInstallment);        
    };

    /* calc total summ */
    public getTotalSumm(): Dto.Sum.ITotalSum {
        return this.cartFullService.getTotalSumm(this.cart, this.selectedInstallment);
    };

    public deleteProduct(productId: string): void {
        this.apiCartService.deleteProductFromCart(productId)
            .then((response: any) => {
                let deletedProduct = this.cart.products.filter((i: any) => { return i.id === productId })[0];
                let index = this.cart.products.indexOf(deletedProduct);
                this.cart.products.splice(index, 1);
                this.cart.orderCount--;
                this.refreshCartItemsCount();
            });
    };

    public deleteComplect(complectId: string): void {
        this.apiCartService.deleteItemComplectFromCart(complectId)
            .then((response: any) => {
                let deletedComplect = this.cart.complects.filter((i: any) => { return i.id === complectId })[0];
                let index = this.cart.complects.indexOf(deletedComplect);
                this.cart.complects.splice(index, 1);
                this.cart.orderCount--;
                this.refreshCartItemsCount();
            })
    };

    public deleteProductFromCollection(productId: string, collectionId: string): void {
        this.apiCartService.deleteItemCollectionFromCart(productId, collectionId)
            .then((response: any) => {
                let deletedCollection = this.cart.collections.filter((i: any) => { return i.id === collectionId })[0];
                let indexOfCollection = this.cart.collections.indexOf(deletedCollection);
                let deletedProduct = deletedCollection.products.filter((i: any) => { return i.productId === productId })[0];
                let indexOfProduct = deletedCollection.products.indexOf(deletedProduct);
                deletedCollection.products.splice(indexOfProduct, 1);
                if (deletedCollection.products.length <= 0) {
                    this.cart.collections.splice(indexOfCollection, 1);
                }
                this.cart.orderCount--;
                this.refreshCartItemsCount();
            })
    };

    public validateQuantity(quantity: number) {
        if (quantity < 1) {
            quantity = 1;
        }
        else if (quantity > 100) {
            quantity = 100;
        }
        return Number(quantity);
    }

    public changeProductQuantity(productId: string): void {
        var item = this.cart.products.filter((f: any) => {
            return f.productId === productId
        })[0];
        item.quantity = this.validateQuantity(item.quantity);
        item.summ = this.getSummProduct(item).sum;                        
        this.recountTotalCart();
    };

    public incrQuantity(item: Dto.CartFull.IProductItem): void {
        if (item.quantity < 100) {
            item.quantity++;
            item.summ = this.getSummProduct(item).sum;                    
            this.recountTotalCart();
        }
    };

    public decrQuantity(item: Dto.CartFull.IProductItem): void {
        if (item.quantity > 1) {
            item.quantity--;
            item.summ = this.getSummProduct(item).sum;                    
            this.recountTotalCart();
        }
    };

    public changeProducFromCollectionQuantity(collectionId: string, productId: string): void {
        var item = this.cart.collections.filter((c: any) => {
            return c.id === collectionId
        })[0].products.filter((f: any) => {
            return f.productId === productId
        })[0];
        item.quantity = this.validateQuantity(item.quantity);
        item.summ = this.getSummProduct(item).sum;                        
        this.recountTotalCart();
    };

    public changeComplectQuantity(complectId: string): void {
        var item = this.cart.complects.filter((f: any) => {
            return f.id === complectId
        })[0];
        item.quantity = this.validateQuantity(item.quantity);
        item.summ = this.getSummComplect(item).sum;                       
        this.recountTotalCart();
    };

    public incrQueantityKomplekt(komplekt: Dto.CartFull.IComplectItem): void {
        if (komplekt.quantity < 100) {
            komplekt.quantity++;
            komplekt.summ = this.getSummComplect(komplekt).sum;
            this.recountTotalCart();
        }
    };

    public decrQueantityKomplekt(komplekt: Dto.CartFull.IComplectItem): void {
        if (komplekt.quantity > 1) {
            komplekt.quantity--;
            komplekt.summ = this.getSummComplect(komplekt).sum;           
            this.recountTotalCart();
        }
    };

    public moveProductToLayaway(productId: any) {
        var item = this.cart.products.filter((f: any) => {
            return f.productId === productId
        })[0];
        this.layawayApiService.moveToLayaway({ Products: [item] })
            .then(() => {
                var index = this.cart.products.indexOf(item);
                this.cart.products.splice(index, 1);
                this.cart.orderCount--;
                this.cartItemsCount--;
                this.recountTotalCart();
                this.layawayItemsCount++;
            });
    }

    public moveCollectionToLayaway(id: string) {
        var currentCollection = this.cart.collections.filter((c: Dto.CartFull.ICollectionItem) => {
            return c.id === id;
        })[0];
        this.layawayApiService.moveToLayaway({ Collections: [{ CollectionCartItemId: id, ProductsInCollection: currentCollection.products }] })
            .then(() => {
                this.cart.orderCount -= currentCollection.products.length;
                this.cartItemsCount--;
                var index = this.cart.collections.indexOf(currentCollection);
                this.cart.collections.splice(index, 1);
                this.recountTotalCart();
                this.layawayItemsCount++;
            });
    }

    public moveProductInCollectionToLayaway(productId: any, collectionId: string) {
        var currentCollection = this.cart.collections.filter((c: Dto.CartFull.ICollectionItem) => {
            return c.id === collectionId;
        })[0];
        var productInCollection = currentCollection.products.filter((p: any) => {
            return p.productId === productId
        })[0];

        this.layawayApiService.moveToLayaway({ Collections: [{ CollectionCartItemId: collectionId, ProductsInCollection: [productInCollection] }] })
            .then(() => {
                currentCollection.products.splice(currentCollection.products.indexOf(productInCollection), 1);
                if (currentCollection.products.length <= 0) {
                    this.cart.collections.splice(this.cart.collections.indexOf(currentCollection), 1);
                    this.cart.orderCount--;
                    this.cartItemsCount--;
                }
                this.recountTotalCart();
                this.layawayItemsCount++;
            }).catch((error) => {
                this.messageService.error(error.data.message, error);
            });
    };

    public moveComplectToLayaway(complectId: any) {
        var item = this.cart.complects.filter((f: any) => {
            return f.id === complectId
        })[0];
        this.layawayApiService.moveToLayaway({ Complects: [item] })
            .then(() => {
                this.cart.complects.splice(this.cart.complects.indexOf(item), 1);
                this.cart.orderCount--;
                this.cartItemsCount--;
                this.recountTotalCart();
                this.layawayItemsCount++;
            }).catch((error) => {
                this.messageService.error(error.data.message, error);
            });
    };

    public moveWholeCartToLayaway() {
        this.cart.collections.map((c: any) => {
            c.CollectionCartItemId = c.id;
            c.ProductsInCollection = [];
            for (var i = 0; i < c.products.length; i++) {
                c.ProductsInCollection[i] = c.products[i];
                delete c.products[i];
            }
        });
        this.layawayApiService.moveToLayaway({
            IsWholeCart: true,
            Products: this.cart.products,
            Collections: this.cart.collections,
            Complects: this.cart.complects
        }).then(() => {
            this.cart.products = [];
            this.cart.complects = [];
            this.cart.collections = [];
            this.cart.orderCount = 0;
            this.cartItemsCount = 0;
            this.recountTotalCart();
            this.layawayItemsCount++;
        });
    }

    //profile
    private getProfile(): void {
        this.profileService.getProfile()
            .then((data) => {
                this.profile = data;
            });
    }

    public validateProfile(): boolean {
        return this.profileService.checkProfileValidityForCartFull(this.profile);
    }

    //payments initialization
    private getPaymentMethods(): void {
        this.apiCartService.getPaymentMethods()
            .then((data: Array<Dto.Order.PaymentMethod.IPaymentMethod>) => {
                this.paymentMethods = data.map((m) => {
                    m.class = m.techName;
                    return m;
                });
            });
    }

    public selectPayment = (data: any) => {
        this.selectedPaymentMethod = data;
        if (this.selectedPaymentMethod.name === 'Рассрочка') {
            this.selectedInstallment = this.installments.filter((i) => { return i.id === "24" })[0];
            this.recountTotalCart();
        }
    }

    public makePayment(): void {
        if (!this.selectedPaymentMethod) {
            this.messageService.success(this.constants.cart.choosePaymentMethod);
            return;
        }
        if (this.validateProfile()) {
            this.profileService.updateProfile(this.profile) /* profile update */
                .then((response: any) => {
                    return this.apiCartService.checkIsApprovalRequired();
                })
                .then((response: Dto.Order.ApprovalSign.IApprovalSign) => {
                    /* create order */
                    this.isRequiredSeller = response.isApprovalRequired;
                    let model: Dto.Order.VmOrderCreating = {
                        paymentMethodId: this.selectedPaymentMethod.id,
                        deliveryKladrCode: this.profile.userAddresses.filter((i: any) => { return i.isDefault === true })[0].kladrCode,
                        deliveryAddress: this.profile.userAddresses.filter((i: any) => { return i.isDefault === true })[0].address,
                        isRequiredSeller: this.isRequiredSeller
                    }
                    return this.orderApiService.create(model);
                })
                .then((response: Dto.Order.VmOrderResponse) => {
                    /*redirect to order thanks page*/
                    this.$window.location.assign(this.orderThanksUrl.replace(":id", response.id));
                });
        }
    }
}