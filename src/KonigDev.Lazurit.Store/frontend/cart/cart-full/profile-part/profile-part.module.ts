﻿import * as angular from "angular";

import profilePartController from "./profile-part.controller";

/* constants */
export const name = "kd.cart.profile.part";
const profilePartControllerName = "profilePartController";
const profilePartComponentName = "kdProfilePart";

angular
    .module(name,[ ])
    .controller(profilePartControllerName,
    [
        profilePartController
    ])
    .component(profilePartComponentName, {
        bindings: {
            profile:"<"
        },
        controller: profilePartControllerName,
        template: require("./profile-part.view.html")
    });
