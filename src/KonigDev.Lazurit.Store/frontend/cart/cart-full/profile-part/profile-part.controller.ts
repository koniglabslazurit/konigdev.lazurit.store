﻿export default class ProfilePartController {

    public profile: any;
    public defaultPhone: any;  
    
    map(): void {
        if (this.profile) {
        //костыль: должен выбираться дефолтный, но пока его нет берем первый
            //this.defaultPhone = this.profile.userPhones.filter((i: any) => { return i.isDefault === true })[0];
            this.defaultPhone = this.profile.userPhones[0];
            if (!this.defaultPhone) {
                this.defaultPhone = { phone: "", isDefault: true };
                this.profile.userPhones.push(this.defaultPhone);
            }
        }
    };

    $onChanges(changesObj: any): void {
        if (changesObj.profile) {
            if (changesObj.profile.currentValue) {
                this.map();
            }
        }
    }

}