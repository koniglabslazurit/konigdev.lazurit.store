export default class CartFullService implements CartFull.Services.ICartFullService {

    public getSummProduct(item: Dto.CartFull.IProductItem, selectedInstallment: Dto.Filter.IInstallmentFilter): Dto.Sum.ISum {
        var sum = 0, saving=0, assembly=0;

        if (!selectedInstallment || selectedInstallment.id === "0") {
            item.priceInTotal = item.salePrice;
        }
        else {
            if (selectedInstallment.id === "6") {
                item.priceInTotal = item.salePriceInstallment6Month;
            }
            if (selectedInstallment.id === "10") {
                item.priceInTotal = item.salePriceInstallment10Month;
            }
            if (selectedInstallment.id === "12") {
                item.priceInTotal = item.salePriceInstallment12Month;
            }
            if (selectedInstallment.id === "18") {
                item.priceInTotal = item.salePriceInstallment18Month;
            }
            if (selectedInstallment.id === "24") {
                item.priceInTotal = item.salePriceInstallment24Month;
            }
            if (selectedInstallment.id === "36") {
                item.priceInTotal = item.salePriceInstallment36Month;
            }
            if (selectedInstallment.id === "48") {
                item.priceInTotal = item.salePriceInstallment48Month;
            }
        }
        if (item.quantity && item.priceInTotal.amount) {
            sum = item.quantity * Math.round(item.priceInTotal.amount);
            saving = (Math.round(item.price.amount) - Math.round(item.priceInTotal.amount)) * item.quantity;
        } else throw new EvalError("item.quantity && item.price.amount && item.discount.amount can not equal undefined");
        
        if (item.isAssemblyRequired) {
            if (!selectedInstallment || selectedInstallment.id === "0") {
                sum = Math.round(item.salePriceWithAssembly.amount) * item.quantity;
            }
            else {
                if (selectedInstallment.id === "6") {
                    sum = Math.round(item.salePriceInstallment6MonthWithAssembly.amount) * item.quantity;
                }
                if (selectedInstallment.id === "10") {
                    sum = Math.round(item.salePriceInstallment10MonthWithAssembly.amount) * item.quantity;
                }
                if (selectedInstallment.id === "12") {
                    sum = Math.round(item.salePriceInstallment12MonthWithAssembly.amount) * item.quantity;
                }
                if (selectedInstallment.id === "18") {
                    sum = Math.round(item.salePriceInstallment18MonthWithAssembly.amount) * item.quantity;
                }
                if (selectedInstallment.id === "24") {
                    sum = Math.round(item.salePriceInstallment24MonthWithAssembly.amount) * item.quantity;
                }
                if (selectedInstallment.id === "36") {
                    sum = Math.round(item.salePriceInstallment36MonthWithAssembly.amount) * item.quantity;
                }
                if (selectedInstallment.id === "48") {
                    sum = Math.round(item.salePriceInstallment48MonthWithAssembly.amount) * item.quantity;
                }
            }
        }        
        assembly = sum - Math.round(item.priceInTotal.amount) * item.quantity;
        var resultSum: Dto.Sum.ISum = {
            sum: sum,                
            saving: saving,
            assembly: assembly
        };
        item.summ = resultSum.sum;
        return resultSum;
    };

    public getSummComplect(item: Dto.CartFull.IComplectItem, selectedInstallment: Dto.Filter.IInstallmentFilter): Dto.Sum.ISum {
        var sum = 0, saving = 0, assembly = 0;
        if (!selectedInstallment || selectedInstallment.id === "0") {
            item.priceInTotal = item.salePrice;
        }
        else {
            if (selectedInstallment.id === "6") {
                item.priceInTotal = item.salePriceInstallment6Month;
            }
            if (selectedInstallment.id === "10") {
                item.priceInTotal = item.salePriceInstallment10Month;
            }
            if (selectedInstallment.id === "12") {
                item.priceInTotal = item.salePriceInstallment12Month;
            }
            if (selectedInstallment.id === "18") {
                item.priceInTotal = item.salePriceInstallment18Month;
            }
            if (selectedInstallment.id === "24") {
                item.priceInTotal = item.salePriceInstallment24Month;
            }
            if (selectedInstallment.id === "36") {
                item.priceInTotal = item.salePriceInstallment36Month;
            }
            if (selectedInstallment.id === "48") {
                item.priceInTotal = item.salePriceInstallment48Month;
            }
        }
        if (item.price.amount) {
            sum = item.quantity * Math.round(item.priceInTotal.amount);
            saving = (Math.round(item.price.amount) - Math.round(item.priceInTotal.amount)) * item.quantity;
        } else throw new EvalError("komplekt.price.amount can not equal undefined");

        if (item.isAssemblyRequired) {
            if (!selectedInstallment || selectedInstallment.id === "0") {
                sum = Math.round(item.salePriceWithAssembly.amount) * item.quantity;
            }
            else {
                if (selectedInstallment.id === "6") {
                    sum = Math.round(item.salePriceInstallment6MonthWithAssembly.amount) * item.quantity;
                }
                if (selectedInstallment.id === "10") {
                    sum = Math.round(item.salePriceInstallment10MonthWithAssembly.amount) * item.quantity;
                }
                if (selectedInstallment.id === "12") {
                    sum = Math.round(item.salePriceInstallment12MonthWithAssembly.amount) * item.quantity;
                }
                if (selectedInstallment.id === "18") {
                    sum = Math.round(item.salePriceInstallment18MonthWithAssembly.amount) * item.quantity;
                }
                if (selectedInstallment.id === "24") {
                    sum = Math.round(item.salePriceInstallment24MonthWithAssembly.amount) * item.quantity;
                }
                if (selectedInstallment.id === "36") {
                    sum = Math.round(item.salePriceInstallment36MonthWithAssembly.amount) * item.quantity;
                }
                if (selectedInstallment.id === "48") {
                    sum = Math.round(item.salePriceInstallment48MonthWithAssembly.amount) * item.quantity;
                }
            }
        }        
        assembly = sum - Math.round(item.priceInTotal.amount) * item.quantity;
        var resultSum: Dto.Sum.ISum = {
            sum: sum,              
            saving: saving,
            assembly: assembly
        };
        item.summ = resultSum.sum;
        return resultSum;
    };

    public getTotalSumm(cart: Dto.CartFull.ICart, selectedInstallment: Dto.Filter.IInstallmentFilter): Dto.Sum.ITotalSum {
        var productsSumm = 0, productsSaving = 0, productAssembly=0;
        for (var product of cart.products) {
            var dto = this.getSummProduct(product, selectedInstallment);
            productsSumm = productsSumm + dto.sum;
            productsSaving = productsSaving + dto.saving;
            productAssembly = productAssembly + dto.assembly;
        }        
        var collectionsSumm = 0, collectionsSaving = 0, collectionsAssembly = 0;
        for (var collection of cart.collections) {
            for (var product of collection.products) {
                var dto = this.getSummProduct(product, selectedInstallment);
                collectionsSumm = collectionsSumm + dto.sum;
                collectionsSaving = collectionsSaving + dto.saving;
                collectionsAssembly = collectionsAssembly + dto.assembly;
            }
        }
        
        var complectsSumm = 0, complectsSaving = 0, complectsAssembly=0;
        for (var complect of cart.complects) {
            var dto = this.getSummComplect(complect, selectedInstallment); 
            complectsSumm = complectsSumm + dto.sum;
            complectsSaving = complectsSaving + dto.saving;
            complectsAssembly = complectsAssembly + dto.assembly;
        }
        
        var deliverableSumm = cart.priceForDelivery * +cart.isDeliveryRequired;
        if (cart.products.length + cart.complects.length + cart.collections.length === 0) {
            deliverableSumm = 0;
        }                                                                                   
        var totalSum = productsSumm + collectionsSumm + complectsSumm + deliverableSumm;
        var totalSaving = productsSaving + collectionsSaving + complectsSaving;
        var totalAssembly = productAssembly + collectionsAssembly + complectsAssembly
        var totalSumOnly = productsSumm + collectionsSumm + complectsSumm - totalAssembly;
        var installmentSum = totalSum;
        if (selectedInstallment && Number(selectedInstallment.id) !== 0) {
            installmentSum = Math.round(totalSum / Number(selectedInstallment.id));
        }
        var result: Dto.Sum.ITotalSum = {
            totalSum: totalSum,
            totalSumOnly: totalSumOnly,
            totalSaving: totalSaving,
            totalAssembly: totalAssembly,
            installmentSum: installmentSum
        };
        return result;
    };       

    public findElementByName(name: string): any {
        var element = document.getElementsByName(name)[0];
        return element;
    };
};