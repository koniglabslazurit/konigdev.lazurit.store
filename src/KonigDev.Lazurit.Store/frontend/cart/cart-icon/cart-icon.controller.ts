﻿export default class CartIconController {             
    public _cart: Dto.CartMini.ICartIcon;    

    constructor(public cartIconHubService: Cart.Hub.ICartIconHubService, $timeout: angular.ITimeoutService, public commonService: Common.Services.IEventService) {
        this._cart = cartIconHubService.cartIcon;
    }          

    public showCart(): void {
        this.commonService.triggerEvent('showCartEvent');
    };

}