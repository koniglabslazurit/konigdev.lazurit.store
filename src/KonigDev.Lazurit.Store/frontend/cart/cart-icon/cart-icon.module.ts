﻿import * as angular from "angular";

import * as cartHubModule from "../cart-hub/cart-hub.module";
import * as commonModule from "../../common/common.module";

import controller from "./cart-icon.controller";

export const name = "kd.cart.icon";
const controllerName = "CartIconController";
const componentName = "kdCartIcon";

angular
    .module(name,
    [
        cartHubModule.name,
        commonModule.name
    ])
    .controller(controllerName,
    [
        cartHubModule.cartIconHubServiceName,
        "$timeout",
        commonModule.eventServiceName,
        controller
    ])
    .component(componentName,
    {
        bindings: {},
        controller: controllerName,
        template: require("./cart-icon.view.html")
    });   
