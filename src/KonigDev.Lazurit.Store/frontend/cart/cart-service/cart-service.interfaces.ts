﻿namespace Cart.Services {
    export interface ICartService {
        getCart(): any;      
        deleteCart(): any; 
        setCartCookies(): any;                   
        checkIsApprovalRequired(): any;
        getPaymentMethods(): any;
        addProductToCart(model): any;
        addCollectionToCart(model): any;
        addComplectToCart(model): any;
        deleteProductFromCart(productId): any;
        deleteItemCollectionFromCart(productId, collectionId): any;
        deleteItemFullCollectionFromCart(collectionId): any;
        deleteItemComplectFromCart(complectId): any;
    }
}
