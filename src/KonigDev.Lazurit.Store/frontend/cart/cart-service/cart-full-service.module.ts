﻿import * as angular from "angular";

import cartService from "./cart-full-service.service";

/* constants */
export const name = "kd.cartfull.service";
export const cartServiceName = "CartFullService";

angular
    .module(name, [ ])
    .service(cartServiceName,
    [
        "$http",
        cartService
    ]);
