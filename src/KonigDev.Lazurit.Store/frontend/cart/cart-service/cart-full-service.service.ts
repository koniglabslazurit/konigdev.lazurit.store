﻿export default class CartFullService implements Cart.Services.ICartService {

    private getCartUrl: string = '/api/cartfull/';
    private addProductToCartUrl: string = '/api/cartfull/product/';
    private addCollectionToCartUrl: string = '/api/cartfull/collection/';
    private addComplectToCartUrl: string = '/api/cartfull/complect/';
    private deleteProductFromCartUrl: string = '/api/cartfull/product/';
    private deleteItemCollectionFromCartUrl: string = '/api/cartfull/collection/';
    private deleteItemComplectFromCartUrl: string = '/api/cartfull/complect/';
    private checkIsApprovalRequiredUrl: string = '/api/cartfull/approval/'; 
    private getPaymentMethodsUrl: string = '/api/payment-methods/';
    private setCartCookiesUrl: string = '/api/cartmini/cookie/';

    constructor(public $http: angular.IHttpService) {
    }

    public getCart(): any {
        return this.$http.get(this.getCartUrl)
            .then((response: any) => {
                return response.data;
            });
    };

    public deleteCart(): any {
        return this.$http.delete(this.getCartUrl)
            .then((response: any) => {
                return response.data;
            });
    };

    public setCartCookies(): any {
        return this.$http.post(this.setCartCookiesUrl,"")
            .then((response: any) => {
                return response.data;
            });
    };
    
    public checkIsApprovalRequired(): any {
        return this.$http.get(this.checkIsApprovalRequiredUrl)
            .then((response: any) => {
                return response.data;
            });
    };

    public addProductToCart(model: any): any {
        return this.$http.post(this.addProductToCartUrl, model)
            .then((response: any) => {
                return response.data;
            });
    };

    public addCollectionToCart(model: string): any {
        return this.$http({
            method: 'POST',
            url: this.addCollectionToCartUrl,
            data: model
        }).then((response: any) => {
            return response.data;
        });
    };

    public addComplectToCart(model: string): any {
        return this.$http({
            method: 'POST',
            url: this.addComplectToCartUrl,
            data: model
        }).then((response: any) => {
            return response.data;
        });
    }

    public deleteProductFromCart(productId: string): any {
        return this.$http({
            method: 'DELETE',
            url: this.deleteProductFromCartUrl + productId
        }).then((response: any) => {
            return response.data;
        });
    }

    public deleteItemCollectionFromCart(productId: string, collectionId: string): any {
        return this.$http({
            method: 'DELETE',
            url: this.deleteItemCollectionFromCartUrl + productId + "/" + collectionId
        }).then((response: any) => {
            return response.data;
        });
    }

    public deleteItemFullCollectionFromCart(collectionId: string): any {
        return this.$http({
            method: 'DELETE',
            url: this.deleteItemCollectionFromCartUrl + "full/" + collectionId
        }).then((response: any) => {
            return response.data;
        });
    }

    public deleteItemComplectFromCart(complectId: string): any {
        var me = this;
        return me.$http({
            method: 'DELETE',
            url: me.deleteItemComplectFromCartUrl + complectId
        }).then((response) => {
            return response.data;
        });
    };

    public getPaymentMethods(): any {
        return this.$http.get(this.getPaymentMethodsUrl)
            .then((response: any) => {
                return response.data;
            });
    };

}