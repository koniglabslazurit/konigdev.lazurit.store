﻿import * as angular from "angular";

import cartService from "./cart-mini-service.service";

/* constants */
export const name = "kd.cartmini.service";
export const cartServiceName = "CartMiniService";

angular
    .module(name,
    [

    ])
    .service(cartServiceName,
    [
        "$http",
        cartService
    ]);
