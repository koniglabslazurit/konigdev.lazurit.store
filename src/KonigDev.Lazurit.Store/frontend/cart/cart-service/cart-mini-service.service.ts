﻿export default class CartMiniService implements Cart.Services.ICartService {

    private getCartUrl: string = '/api/cartmini/';
    private addProductToCartUrl: string = '/api/cartmini/product/';
    private addCollectionToCartUrl: string = '/api/cartmini/collection/';
    private addComplectToCartUrl: string = '/api/cartmini/complect/';
    private deleteProductFromCartUrl: string = '/api/cartmini/product/';
    private deleteItemCollectionFromCartUrl: string = '/api/cartmini/collection/';
    private deleteItemComplectFromCartUrl: string = '/api/cartmini/complect/';
    private checkIsApprovalRequiredUrl: string = '/api/cartmini/approval/';
    private getPaymentMethodsUrl: string = '/api/payment-methods/';
    private setCartCookiesUrl: string = '/api/cartmini/cookie/';

    constructor(public $http: angular.IHttpService) {
    }

    public getCart(): any {
        return this.$http.get(this.getCartUrl)
            .then((response: any) => {
                return response.data;
            });
    };

    public deleteCart(): any {
        return this.$http.delete(this.getCartUrl)
            .then((response: any) => {
                return response.data;
            });
    };

    public setCartCookies(): any {
        return this.$http.post(this.setCartCookiesUrl,"")
            .then((response: any) => {
                return response.data;
            });
    };


    public checkIsApprovalRequired(): any {
        return this.$http.get(this.checkIsApprovalRequiredUrl)
            .then((response: any) => {
                return response.data;
            });
    };

    public addProductToCart(model: any): any {
        return this.$http.post(this.addProductToCartUrl, model)
            .then((response: any) => {
                return response;
            });
    };

    public addCollectionToCart(model: string): any {
        return this.$http({
            method: 'POST',
            url: this.addCollectionToCartUrl,
            data: model
        }).then((response: any) => {
            return response;
        });
    };

    public addComplectToCart(model: string): any {
        return this.$http({
            method: 'POST',
            url: this.addComplectToCartUrl,
            data: model
        }).then((response: any) => {
            return response;
        });
    }

    public deleteProductFromCart(productId: string): any {
        return this.$http({
            method: 'DELETE',
            url: this.deleteProductFromCartUrl + productId
        }).then((response: any) => {
            return response;
        });
    }

    public deleteItemCollectionFromCart(productId: string, collectionId: string): any {
        return this.$http({
            method: 'DELETE',
            url: this.deleteItemCollectionFromCartUrl + productId + "/" + collectionId
        }).then((response: any) => {
            return response;
        });
    }

    public deleteItemFullCollectionFromCart(collectionId: string): any {
        return this.$http({
            method: 'DELETE',
            url: this.deleteItemCollectionFromCartUrl + "full/" + collectionId
        }).then((response: any) => {
            return response;
        });
    }

    public deleteItemComplectFromCart(complectId: string): any {
        var me = this;
        return me.$http({
            method: 'DELETE',
            url: me.deleteItemComplectFromCartUrl + complectId
        }).then((response) => {
            return response;
        });
    };

    public getPaymentMethods(): any {
        return this.$http.get(this.getPaymentMethodsUrl)
            .then((response: any) => {
                return response.data;
            });
    };
    //метод пока здесь не нужен
    public getRoboPaymentMethods(): any {
        return null;
    };
}