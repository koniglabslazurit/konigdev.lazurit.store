﻿import * as angular from "angular";

import service from "./product.api.service";

export const name = "kd.product";
export const serviceName = "productService";

angular
    .module(name, [])
    .service(serviceName,
    [
        "$http",
        service
    ]);
