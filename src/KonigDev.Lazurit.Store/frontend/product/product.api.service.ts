﻿export default class ProductService implements ProductPage.Services.IApiProductService {

    private url: string = "/api/product";
    private filtersUrl: string = "/api/product/filters";
    private propertyUrl: string = "/api/product/property";
    private vendorUrl: string = "/api/product/vendorCode";

    constructor(
        public $http: angular.IHttpService
    ) { }

    public getProducts(request: any): any {
        return this.$http
            .get(this.url, { params: request })
            .then(response => response.data);
    }

    public getVendor(request: any): any {
        return this.$http
            .get(this.vendorUrl, { params: request })
            .then(response => response.data);
    }

    public getFilters(request: any): any {
        return this.$http
            .get(this.filtersUrl, { params: request })
            .then(response => response.data);
    }

    public getProperties(request: any): any {
        return this.$http
            .get(this.propertyUrl, { params: request })
            .then(response => response.data);
    }
}
