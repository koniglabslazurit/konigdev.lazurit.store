﻿export default class ProductPageSliderController {

    public hideModal: boolean = true;
    public current: Dto.Product.ICurrentProduct;

    public constructor(
        private $rootScope: angular.IRootScopeService
    ) {
        var mainImage = <HTMLImageElement>document.getElementById("main_image");
        this.current = { id: mainImage.name, number: 1, type: "SliderInInterior", openType: "SliderInInterior" };
    }

    public selectImage(id: string, imgNmb: number, imgType: string): void {
        this.$rootScope.$broadcast("ppSelectImage", id);
        this.setImage(id, imgNmb, imgType);
    };


    private setImage(id: string, imgNmb: number, imgType: string) {
        var openType = "SliderOpen";
        if (imgType === "SliderInInterior") {
            openType = "SliderInInterior";
        }
        this.current = { id: id, number: imgNmb, type: imgType, openType: openType };
    }

    public openModal(imgNmbr: number, imgType: string): void {
        //this.hideModal = false;
        document.getElementById("modal-window").style.display = "block";
        var element = <HTMLAnchorElement> document.getElementById(imgNmbr + "_" + imgType);
        element.style.display = "block";
        this.current = { id: element.name, type: imgType, number: imgNmbr, openType: "SliderOpen" };
    }

    public closeModal(): void {
        this.hideModal = true;
        document.getElementById("modal-window").style.display = "none";
        document.getElementById(this.current.number + "_" + this.current.type).style.display = "none";
    }

    public prev(): void {
        document.getElementById(this.current.number + "_" + this.current.type).style.display = "none";
        if (this.current.type === "SliderOpen") {
            this.current.type = "SliderMiddle";
        }
        else if (this.current.type = "SliderMiddle") {
            if (this.current.number === 1) {
                this.current.type = "SliderInInterior";
            }
            else if (this.current.number > 1) {
                this.current.number = this.current.number - 1;
                this.current.type = "SliderOpen";
                var prevElement = <HTMLAnchorElement>document.getElementById(this.current.number + "_" + this.current.type);
                this.current.id = prevElement.name;
            }
        }       
        document.getElementById(this.current.number + "_" + this.current.type).style.display = "block";        
    }

    public next(): void {
        document.getElementById(this.current.number + "_" + this.current.type).style.display = "none";
        if (this.current.type === "SliderMiddle") {
            this.current.type = "SliderOpen";
        }           
        else if (this.current.type === "SliderInInterior") {
            this.current.type = "SliderMiddle";
        }
        else if (this.current.type === "SliderOpen") {
            var nextNumber = this.current.number + 1;
            var nextElement = <HTMLAnchorElement>document.getElementById(nextNumber + "_SliderMiddle");
            if (nextElement) {
                this.current.number = nextNumber;
                this.current.type = "SliderMiddle";
                this.current.id = nextElement.name;                
            }                                                  
        }
        document.getElementById(this.current.number + "_" + this.current.type).style.display = "block";        
    }

    public selectProduct(): void {                
        this.closeModal();
        this.selectImage(this.current.id, this.current.number, this.current.type);
    }
}
