﻿export default class ProductPageController {

    private frameColors: Array<Dto.Filter.IProductFilterFrameColor>;
    private facings: Array<Dto.Filter.IProductFilterFacing>;
    private facingColors: Array<Dto.Filter.IProductFilterFacingColor>;
    private dimentions: Array<Dto.Filter.IDimentionFilter> = [];
    private allDimentions: Array<Dto.Filter.IDimentionFilter> = [];
    private installments: Array<Dto.Filter.IInstallmentFilter>;

    private selectedFrameColor: Dto.Filter.IProductFilterFrameColor;
    private selectedFacing: Dto.Filter.IProductFilterFacing;
    private selectedFacingColor: Dto.Filter.IProductFilterFacingColor;
    private selectedDimention: Dto.Filter.IDimentionFilter;
    private selectedInstallment: Dto.Filter.IInstallmentFilter;

    private product: any;
    private products: Array<any> = [];
    private propertiesMulti: Array<Dto.Property.IPropertyMulti> = [];
    private propertiesSingle: Array<Dto.Property.IPropertyItem> = [];
    private selectedProperties: Array<any> = [];
    private selectedPropertiesMap: Array<Dto.Property.IPropertyItem> = [];
    private selectedPropertiesSingle: Array<Dto.Property.IPropertyItem> = [];

    private isAssemblyRequired: boolean = true;
    private isDeliverableRequired: boolean = true;

    private vendorCode: Dto.VendorCode.IVendorCodeItem;
    private vendorCodeId: string;
    private urlHash: string;

    public constructor(
        private productApiService: ProductPage.Services.IApiProductService,
        private productPageService: ProductPage.Services.IProductPageService,
        private productMapService: ProductPage.Services.IProductMapPageService,
        private productPageJQService: ProductPage.Services.IJqueryProductService,
        private installmentServiceModule: ProductsInstallment.Service,
        private geolocationService: any
    ) {
        this.constructorInit();
    }

    private selectFrameColor = (item: Dto.Filter.IProductFilterFrameColor): void => {
        this.selectedFrameColor = item;
        this.facings = this.productPageService.getFacings(this.frameColors, item.id);
        if (this.facings.length === 1) {
            this.selectedFacing = this.facings[0];
            this.selectFacing(this.selectedFacing);

        } else {
            if (this.facings.indexOf(this.selectedFacing) === -1) {
                this.selectedFacing = this.facings[0];
            }
        }
        this.getDimentions();
        this.getProduct();
    }

    private selectFacing = (item: Dto.Filter.IProductFilterFacing): void => {
        this.selectedFacing = item;
        this.facingColors = this.productPageService.getFacingColors(this.frameColors, item.id);
        if (this.facingColors.length === 1) {
            this.selectedFacingColor = this.facingColors[0];
            this.selectFacingColor(this.selectedFacingColor);
        } else {
            if (this.facingColors.indexOf(this.selectedFacingColor) === -1) {
                this.selectedFacingColor = this.facingColors[0];
            }
        }
        this.getDimentions();
        this.getProduct();
    }

    private selectFacingColor = (item: Dto.Filter.IProductFilterFacingColor): void => {
        this.selectedFacingColor = item;
        this.getDimentions();
        this.getProduct();
    }

    private selectDimention = (item: Dto.Filter.IDimentionFilter): void => {
        this.selectedDimention = item;
        this.getProduct();
    }

    private getDimentions() {
        if (!this.selectedFacing || !this.selectedFrameColor)
            return;

        var selectedProducts = JSON.parse(JSON.stringify(this.products.filter((p: any) => {
            if (this.selectedFacingColor)
                return (p.facingColorId === this.selectedFacingColor.id
                    && p.facingId === this.selectedFacing.id
                    && p.frameColorId === this.selectedFrameColor.id)
            else return (p.facingId === this.selectedFacing.id
                && p.frameColorId === this.selectedFrameColor.id)
        })));
        var dimentions: Array<Dto.Filter.IDimentionFilter> = [];
        for (var product of selectedProducts) {
            for (var dimention of this.allDimentions) {
                if (dimention.id === product.id) {
                    dimentions.push(dimention);
                }
            }
        }
        this.dimentions = dimentions;
        if (this.dimentions.length > 0) {
            this.selectedDimention = this.dimentions[0];
        } else {
            this.selectedDimention = null;
        }
    }

    private selectProperty = (item: any): void => {
        this.selectedProperties[item.groupeId] = item;
        this.selectedPropertiesMap = this.productMapService.mapPropertiesSelected(this.propertiesMulti, this.selectedProperties);
        this.getProduct();
        if (!this.product) {
            this.productPageService.messageProductNotFound();
        }
    }

    private getProduct(): void {
        var product;
        if (this.selectedFacing && this.selectFacingColor && this.selectFrameColor) {
            var products = this.products.filter((item: any) => {
                return item.facingId === this.selectedFacing.id
                    && item.facingColorId === this.selectedFacingColor.id
                    && item.frameColorId === this.selectedFrameColor.id
                    && item.id === this.selectedDimention.id;
            });

            this.selectedPropertiesMap.forEach((item: Dto.Property.IPropertyItem) => {
                products = products.filter((f: any) => {
                    return f.propirties.indexOf(item.id) > -1;
                });
            });
            product = products[0];
        }
        this.product = product;

        if (this.product) {
            this.productPageService.setHashUrl(product.syncCode1C);
            this.productPageJQService.changeCurrentImage(this.product.id, "SliderMiddle");
        } else {
            this.productPageService.setHashUrl("");
            //this.productPageJQService.setNotFound();
        }
    }

    private setFilters(): void {
        if (!this.product)
            return;
        /* get frame color */
        var selectedFrameColor = this.frameColors.filter((f: any) => { return f.id === this.product.frameColorId })[0];
        if (selectedFrameColor) {
            this.selectedFrameColor = selectedFrameColor;
            /* get facings*/
            this.facings = this.productPageService.getFacings(this.frameColors, this.selectedFrameColor.id);
            var facing = this.facings.filter((f: any) => { return f.id === this.product.facingId })[0];
            if (facing) {
                this.selectedFacing = facing;
                /* get facings color */
                this.facingColors = this.productPageService.getFacingColors(this.frameColors, this.selectedFacing.id);
                var facingColor = this.facingColors.filter((f: any) => { return f.id === this.product.facingColorId })[0];
                if (facingColor) {
                    this.selectedFacingColor = facingColor;
                }
            }
        }
        /*габариты*/
        this.dimentions = [];
        this.allDimentions = [];
        for (var product of this.products) {
            this.allDimentions.push({ id: product.id, name: product.height + "x" + product.length + "x" + product.width });
        };
        this.getDimentions();
        this.selectedDimention = this.dimentions.filter((d) => { return d.id === this.product.id })[0];

        /*рассрочка*/
        this.installments = this.installmentServiceModule.getInstallmentList();
        this.selectedInstallment = this.installments.filter((i) => { return i.id === "24" })[0];
        this.getPrices();
    }

    private selectInstallment = (item: Dto.Filter.IInstallmentFilter): void => {
        this.selectedInstallment = item;
        this.getPrices();
    }

    private getPrices() {
        if (!this.selectedInstallment || this.selectedInstallment.id === "0") {
            this.product.priceInTotal = (this.isAssemblyRequired === false) ? this.product.salePrice : this.product.salePriceWithAssembly;
            this.product.discountInTotal = this.product.discount;
        }
        else {
            if (this.selectedInstallment.id === "6") {
                this.product.priceInTotal = (this.isAssemblyRequired === false) ? this.product.salePriceInstallment6Month : this.product.salePriceInstallment6MonthWithAssembly;
                this.product.discountInTotal = this.product.discountInstallment6Month;
            }
            if (this.selectedInstallment.id === "10") {
                this.product.priceInTotal = (this.isAssemblyRequired === false) ? this.product.salePriceInstallment10Month : this.product.salePriceInstallment10MonthWithAssembly;
                this.product.discountInTotal = this.product.discountInstallment10Month;
            }
            if (this.selectedInstallment.id === "12") {
                this.product.priceInTotal = (this.isAssemblyRequired === false) ? this.product.salePriceInstallment12Month : this.product.salePriceInstallment12MonthWithAssembly;
                this.product.discountInTotal = this.product.discountInstallment12Month;
            }
            if (this.selectedInstallment.id === "18") {
                this.product.priceInTotal = (this.isAssemblyRequired === false) ? this.product.salePriceInstallment18Month : this.product.salePriceInstallment18MonthWithAssembly;
                this.product.discountInTotal = this.product.discountInstallment18Month;
            }
            if (this.selectedInstallment.id === "24") {
                this.product.priceInTotal = (this.isAssemblyRequired === false) ? this.product.salePriceInstallment24Month : this.product.salePriceInstallment24MonthWithAssembly;
                this.product.discountInTotal = this.product.discountInstallment24Month;
            }
            if (this.selectedInstallment.id === "36") {
                this.product.priceInTotal = (this.isAssemblyRequired === false) ? this.product.salePriceInstallment36Month : this.product.salePriceInstallment36MonthWithAssembly;
                this.product.discountInTotal = this.product.discountInstallment36Month;
            }
            if (this.selectedInstallment.id === "48") {
                this.product.priceInTotal = (this.isAssemblyRequired === false) ? this.product.salePriceInstallment48Month : this.product.salePriceInstallment48MonthWithAssembly;
                this.product.discountInTotal = this.product.discountInstallment48Month;
            }
        };
        if (this.isAssemblyRequired === true) {
            this.product.saving = (this.product.priceInTotal) ? this.product.price.amount - this.product.priceInTotal.amount - this.product.priceForAssembly.amount : 0;
        }
        else {
            this.product.saving = (this.product.priceInTotal) ? this.product.price.amount - this.product.priceInTotal.amount : 0;
        }
        this.product.savingString = this.product.saving + " руб";
    }

    private setPropirties(): void {
        if (!this.product)
            return;
        this.selectedPropertiesSingle = [];
        this.product.propirties.forEach((id: string) => {
            var prop;
            this.propertiesMulti.forEach((item: Dto.Property.IPropertyMulti) => {
                var res = item.items.filter((f: Dto.Property.IPropertyItem) => { return f.id === id })[0];
                if (res) {
                    prop = res;
                }
            });
            if (prop) {
                this.selectedProperties[prop.groupeId] = prop;
            }
            var singleProp = this.propertiesSingle.filter((item: Dto.Property.IPropertyItem) => { return item.id === id; })[0];
            if (singleProp) {
                this.selectedPropertiesSingle.push(singleProp);
            }
        });
        this.selectedPropertiesMap = this.productMapService.mapPropertiesSelected(this.propertiesMulti, this.selectedProperties);
    }

    private constructorInit(): void {
        this.vendorCode = { furnitureType: "", height: 0, id: "", length: 0, syncCode1C: "", width: 0 };

        this.urlHash = this.productPageService.getHashUrl();
        this.productPageJQService.setImage(this.selectImage);
    }

    private selectImage = (id: string): void => {
        var product = this.products.filter((item: any) => {
            return item.id === id;
        })[0];
        if (product) {
            console.log(product);
            this.product = product;
            //this.productPageJQService.setFound();
            this.productPageService.setHashUrl(product.syncCode1C);
            //this.productPageJQService.changeImage(this.product.id);                
            this.setFilters();
            this.setPropirties();
        }
    }

    private init(): void {
        this.geolocationService.getNearbyCities()
            .then((result: any) => {
                return this.productApiService.getFilters({ ArticleId: this.vendorCode.id });
            })
            .then((data: any) => {
                this.frameColors = this.productMapService.mapFilters(data);
                return data;
            })
            .then((data: any) => {
                return this.productApiService.getVendor({ ArticleId: this.vendorCodeId });
            })
            .then((data: any) => {
                this.vendorCode = this.productMapService.mapVendorData(data);
                this.products = data.products;

                if (this.urlHash) {
                    var p = this.products.filter((f: any) => { return f.syncCode1C === this.urlHash })[0];
                    if (p) {
                        this.product = p;
                    }
                }
                if (!this.product) {
                    this.product = this.products[0];
                }
                return this.productApiService.getProperties({ ArticleId: this.vendorCode.id });
            })
            .then((data: any) => {
                var mapResult = this.productMapService.mapProperties(data);
                this.propertiesMulti = mapResult.propertiesMulti;
                this.propertiesSingle = mapResult.propertiesSingle;
                return data;
            })
            .then((data: any) => {
                this.setFilters();
                this.setPropirties();
            })
            .catch((error: any) => {
                console.log(error);
            });
    }

    $onChanges(changesObj: any): void {
        if (changesObj.vendorCodeId) {
            if (changesObj.vendorCodeId.currentValue) {
                this.vendorCode.id = changesObj.vendorCodeId.currentValue;
                this.init();
            }
        };
    }

    private addToCart(): void {
        if (this.product) {
            this.product.isAddingToCart = true;
            this.productPageService.addToCart(this.product, this.isAssemblyRequired).then(() => {
                this.product.isAddingToCart = false;
            }).catch((error) => {
                this.product.isAddingToCart = false;
                });
        }
    }

    private addToFavorite(): void {
        if (this.product)
            this.productPageService.addToFavorite(this.product);
    }
}
