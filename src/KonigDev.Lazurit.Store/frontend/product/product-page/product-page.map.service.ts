﻿export default class ProductPageMapService implements ProductPage.Services.IProductMapPageService {

    public mapFilters(list: Array<any>): Array<Dto.Filter.IProductFilterFrameColor> {
        var result = list.map((item: Dto.Filter.IProductFilterFrameColor) => {
            return {
                id: item.id,
                name: item.name,
                imageUrl: '/api/images/variant/' + item.id,
                aliavableFacings: item.aliavableFacings
                    .map((itemFacing: Dto.Filter.IProductFilterFacing) => {
                        return {
                            id: itemFacing.id,
                            name: itemFacing.name,
                            imageUrl: '/api/images/variant/' + itemFacing.id,
                            aliavableFacingColors: itemFacing.aliavableFacingColors
                                .map((itemFacingColor: Dto.Filter.IProductFilterFacingColor) => {
                                    return {
                                        id: itemFacingColor.id,
                                        name: itemFacingColor.name,
                                        imageUrl: '/api/images/variant/' + itemFacingColor.id,
                                    }
                                })
                        }
                    })
            }
        });
        return result;
    }

    public mapProperties(list: any): Dto.Property.IPropertyModel {
        var result: Dto.Property.IPropertyModel;
        result = {
            propertiesMulti: [],
            propertiesSingle: []
        };

        list.properties.forEach((item: any) => {
            result.propertiesMulti.push({
                title: item.listPropirties[0].title,
                groupeId: item.listPropirties[0].groupeId,
                items: item.listPropirties.map((m: any) => {
                    return {
                        name: m.value,
                        id: m.id,
                        groupeId: m.groupeId,
                        title: m.title
                    }
                })
            })
        });
        result.propertiesSingle = list.propertiesSingle.map((m: any) => {
            return {
                name: m.value,
                id: m.id,
                title: m.title
            }
        });
        return result;
    }

    public mapVendorData(data: any): Dto.VendorCode.IVendorCodeItem {
        return {
            id: data.articleId,
            syncCode1C: data.articleSyncCode1C,
            width: data.widths[0],
            length: data.lengths[0],
            height: data.heights[0],
            furnitureType: data.furnitureTypeName
        }
    }

    public mapPropertiesSelected(properties: Array<Dto.Property.IPropertyMulti>, listItems: Array<Dto.Property.IPropertyMulti>): Array<Dto.Property.IPropertyItem> {
        var result = [];
        properties.forEach((item: Dto.Property.IPropertyMulti) => {
            var prop = listItems[item.groupeId];
            if (prop)
                result.push(listItems[item.groupeId]);
        });
        return result;
    }
}