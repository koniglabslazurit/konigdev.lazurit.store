﻿export default class ProductPageService implements ProductPage.Services.IProductPageService {

    public constructor(
        private cartService: any,
        private messageService: Common.Message.IMessage,
        private constants: Constants.IConstants,
        private $window: angular.IWindowService) {
    }

    public getFacings(list: Array<Dto.Filter.IProductFilterFrameColor>, frameColorId: string): Array<Dto.Filter.IProductFilterFacing> {
        var frameColor = list
            .filter((f: Dto.Filter.IProductFilterFrameColor) => f.id === frameColorId)[0];
        var result = frameColor.aliavableFacings;
        return result;
    }

    public getFacingColors(list: Array<Dto.Filter.IProductFilterFrameColor>, facingId: string): Array<Dto.Filter.IProductFilterFacingColor> {
        var result = [];
        list.forEach((e: Dto.Filter.IProductFilterFrameColor) => {
            e.aliavableFacings.forEach((f: Dto.Filter.IProductFilterFacing) => {
                if (f.id === facingId) {
                    result = f.aliavableFacingColors;
                }
            });
        });
        return result;
    }

    public initVendorCode(): Dto.VendorCode.IVendorCodeItem {
        return {
            furnitureType: "", height: null, id: null, length: null, syncCode1C: null, width: null
        }
    }

    public addToCart(product: any, isAssemblyRequired: boolean): angular.IPromise<any> {
        var model = { ProductId: product.id, isAssemblyRequired: isAssemblyRequired };
        return this.cartService.addProductToCart(model)
            .then((response: any) => {
                if (response.status === 200) {
                }
                else {
                    this.messageService.uiError(this.constants.cart.goodsNotAdded);
                }
            }).catch((error: any) => {
                this.messageService.error(error.data.message, error);
            });
    };

    public addToFavorite(product: any): void {
        alert("Функционал не реализован");
    };

    public messageProductNotFound(): void {
        this.messageService.warning(this.constants.product.productNotFound);
    };

    public getHashUrl(): string {
        var result;
        if (this.$window.location.hash) {
            if (this.$window.location.hash.substring(1).indexOf("/") === 0) {
                return this.$window.location.hash.substring(2);
            } else {
                return this.$window.location.hash.substring(1);
            }
        }
        return result;
    };

    public setHashUrl(hash: string): void {
        this.$window.location.hash = hash;
    };
};