﻿export default class ProductPageJQService implements ProductPage.Services.IJqueryProductService {

    public constructor(private $rootScope: angular.IRootScopeService) {

    }
    /* this is kostil to work with jq slider wow. try write directive */
    //public changeImage(id: string): void {
    //    var sliderElement = $('#' + id);
    //    if (sliderElement) {
    //        var itemNumber = sliderElement.data("item");
    //        var slider = (<any>$('#wowslider-container1'))[0];
    //        slider.style.opacity = "1";
    //        slider.wsStart(itemNumber);
    //    }
    //};

    public changeCurrentImage(id: string, imgType: string): void {
        var mainImage = <HTMLImageElement>document.getElementById("main_image");
        mainImage.setAttribute("src", "/api/images/product/" + id + "/" + imgType);
        var mainImageHover = <HTMLImageElement>document.getElementById("main_image_hover");
        var openType = "SliderOpen";
        if (imgType === "SliderInInterior") {
            openType = "SliderInInterior";
        }
        mainImageHover.setAttribute("style", "background-image: url(/api/images/product/" + id + "/" + openType + ");");
    };

    public setImage(callback: any): void {
        this.$rootScope.$on("ppSelectImage", (event, data) => {
            callback(data);            
        });
    };

    //public setNotFound(): void {
    //    var slider = (<any>$('#wowslider-container1'))[0];
    //    slider.style.opacity = "0.6";
    //}

    //public setFound(): void {
    //    var slider = (<any>$('#wowslider-container1'))[0];
    //    slider.style.opacity = "1";
    //}
};