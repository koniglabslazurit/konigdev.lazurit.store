namespace ProductPage.Services {
    export interface IProductPageService {
        /**
        * Получение списка доступных отделок по ид цвета
        */
        getFacings(list: Array<Dto.Filter.IProductFilterFrameColor>, frameColorId: string): Array<Dto.Filter.IProductFilterFacing>;
        /**
        * Получение списка доступных цыетов отделок по ид отделки
        */
        getFacingColors(list: Array<Dto.Filter.IProductFilterFrameColor>, facingId: string): Array<Dto.Filter.IProductFilterFacingColor>;        
        /**
        * Инициализация артикула
        */
        initVendorCode(): Dto.VendorCode.IVendorCodeItem;
        /**
        * Добавление товара в корзину
        */
        addToCart(product: any, isAssemblyRequired: boolean): angular.IPromise<any>;
        /**
        * Добавление товара в изюранное
        */
        addToFavorite(product: any): void;
        /**
        * Отображение сообщения о ненаденном продукте по фильтрам
        */
        messageProductNotFound(): void;
        /**
        * Получение хэш значения из урла - код товара
        */
        getHashUrl(): string;
        /**
        * Установка в урл значения хэш - код товара
        */
        setHashUrl(hash: string): void;
    }

    export interface IProductMapPageService {
        /**
        * Мапинг фильтров
        */
        mapFilters(list: Array<any>): Array<Dto.Filter.IProductFilterFrameColor>;
        /**
        * Мапинг свойств
        */
        mapProperties(list: any): Dto.Property.IPropertyModel;
        /**
        * Мапинг данных по артикулу
        */
        mapVendorData(data: any): Dto.VendorCode.IVendorCodeItem;
        /**
        * Мапинг выбранных свойств для удобного отображения
        */
        mapPropertiesSelected(properties: Array<Dto.Property.IPropertyMulti>, listItems: Array<Dto.Property.IPropertyMulti>): Array<Dto.Property.IPropertyItem>;
    }

    export interface IApiProductService {
        getProducts(request: Dto.Product.ProductRequest): angular.IPromise<any>;
        getVendor(request: any): any;
        getFilters(request: any): angular.IPromise<Dto.Filter.IProductFilterFrameColor>;
        getProperties(request: any): any;
    }

    export interface IJqueryProductService {
        /**
        * Установка выбранного изображения продукта в слайдере
        */
        //changeImage(id: string): void;
        changeCurrentImage(id: string, imgType: string): void;
        /**
        * Выбор изображения в слайдере.
        */
        setImage(callback: any): void;
        /**
        * Установка признака в слайдере ненайденного продукта
        */
        //setNotFound(): void;
        //setFound(): void;
    }
}