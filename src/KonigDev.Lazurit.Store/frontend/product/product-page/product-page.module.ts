﻿import * as angular from "angular";

/* */
import * as productModule from "../product.module";
import * as selectsingleModule from "../../common/components/select-single/select-single.module";
import * as cartModule from "../../cart/cart-service/cart-mini-service.module";
import * as constantsModule from "../../constants/constants.module";
import * as commonModule from "../../common/common.module";
import * as favoriteTriggerModule from "../../favorite/favorite-trigger/favorite-trigger.module";
import * as installmentServiceModule from "../product-installment/products-installment-service/products-installment-service.module";
import * as geolocationModule from "../../geolocation/geolocation.module";
import * as bondModule from "../../common/directive/bond/bond.module";

import controller from "./product-page.controller";
import controllerForSlider from "./product-page.slider.controller";
import service from "./product-page.service";
import serviceMap from "./product-page.map.service";
import serviceJq from "./product-page.service.jq";

/* */
export const name = "kd.product.page";
const controllerName = "productPageController";
const controllerPropName = "productPagePropController";
const serviceName = "productPageService";
const serviceMapName = "productPageServiceMap";
const serviceJqName = "productPageJQService";
const controllerForSliderName = "controllerProductPageSlider";

/* components */
const componentName = "kdProductPage";
/*  */
angular
    .module(name,
    [
        productModule.name,
        selectsingleModule.name,
        cartModule.name,
        favoriteTriggerModule.name,
        installmentServiceModule.name,
        bondModule.name
    ])
    .service(serviceMapName, [serviceMap])
    .service(serviceJqName, ['$rootScope', serviceJq])
    .service(serviceName, [cartModule.cartServiceName, commonModule.messageServiceName, constantsModule.serviceName, '$window', service])
    .controller(controllerForSliderName, ['$rootScope', controllerForSlider])
    .controller(controllerName,
    [
        productModule.serviceName, serviceName, serviceMapName, serviceJqName, installmentServiceModule.serviceInstallmentName, geolocationModule.geolocationServiceName, 
        controller
    ])
    .component(componentName, {
        transclude: {
            'slider': 'slider'
            //'fromcollection': 'fromCollection'
            //'promotional': 'promotional',
            //'buywith': 'buyWith'
        },
        bindings: {
            vendorCodeId: '<'
        },
        controller: controllerName,
        template: require("./product-page.view.html")
    })
    .config(['$stateProvider', ($stateProvider: any) => {
        $stateProvider
            .state("product", {
                url: "/{productcode}"
            });
    }]);