﻿namespace ProductsInstallment {
    export interface ApiService {
        getVendorCodes(request: ProductsInstallment.Query): angular.IPromise<any>;
        getSeries(): angular.IPromise<any>;
        getRooms(): angular.IPromise<any>;
        getFuritureTypes(): angular.IPromise<any>;
        updateProducts(request: any): angular.IPromise<any>;
        getInstallment(request: any): angular.IPromise<any>;
        deleteInstallment(request: any): angular.IPromise<any>;
    }

    export interface Service {
        mapFromFilter(item: Dto.Product.InstallmentFilter): Dto.Product.Installment;
        getInstallmentList(): Array<Dto.Product.IInstallmentItem>;
    }

    export interface Query {
        furnitureTypesIds: Array<string>;
        seriesIds: Array<string>;
        rooms: Array<string>;
    }

    export interface Command {

    }
}

/* todo вынести в дто папку */
namespace Dto.Product {
    /**
     * Отображение записи инсталлмента
     */
    export interface Installment {
        id: string;
        name: string;
        discount: number;
        installment6Month: number;
        installment10Month: number;
        installment12Month: number;
        installment18Month: number;
        installment24Month: number;
        installment36Month: number;
        installment48Month: number;
        dateStart: Date;
        dateEnd: Date;
        series: Array<string>;
        rooms: Array<string>;
        furnitureTypes: Array<string>;
        vendorCodes: Array<string>;
        isNew: boolean;
        counter: number;
    };

    export interface InstallmentFilter {
        series: Array<any>;
        rooms: Array<any>;
        furnitureTypes: Array<any>;
        vendorCodes: Array<any>;
    };

    export interface InstallmentEnum {
        syncCode1C: string;
        id: string;
        isSelected: boolean;
        name: string;
    }

    export interface IInstallmentItem {
        id: string;
        name: string;
    }
} 