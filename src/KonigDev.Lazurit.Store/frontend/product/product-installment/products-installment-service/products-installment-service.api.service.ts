﻿export default class ProductsInstallmentApiService implements ProductsInstallment.ApiService {

    private getVendorCodesUrl: string = "/api/installment/vendorcode";
    private getSeriesUrl: string = "/api/series";
    private getRoomsUrl: string = "/api/room";
    private getFuritureTypesUrl: string = "/api/furnitureType/installment";
    private installmentUrl: string = "/api/installment";

    constructor(
        public $http: angular.IHttpService
    ) { }

    public getVendorCodes(request: ProductsInstallment.Query): angular.IPromise<any> {
        return this.$http.post(this.getVendorCodesUrl, request)
            .then((response: any) => {
                return response;
            });
    }

    public getSeries(): angular.IPromise<any> {
        return this.$http.get(this.getSeriesUrl)
            .then((response: any) => {
                return response;
            });
    }

    public getRooms(): angular.IPromise<any> {
        return this.$http.get(this.getRoomsUrl)
            .then((response: any) => {
                return response;
            });
    }

    public getFuritureTypes(): angular.IPromise<any> {
        return this.$http.get(this.getFuritureTypesUrl)
            .then((response: any) => {
                return response;
            });
    }

    public updateProducts(request): angular.IPromise<any> {
        return this.$http
            .post(this.installmentUrl, request)
            .then((response: any) => {
                return response;
            });
    }

    public getInstallment(request): angular.IPromise<any> {
        return this.$http
            .get(this.installmentUrl, { params: request })
            .then((response: any) => {
                return response;
            });
    }

    public deleteInstallment(installmentId: string): angular.IPromise<any> {
        return this.$http
            .delete(this.installmentUrl, { params: { id: installmentId } })
            .then((response: any) => {
                return response;
            });
    }
}