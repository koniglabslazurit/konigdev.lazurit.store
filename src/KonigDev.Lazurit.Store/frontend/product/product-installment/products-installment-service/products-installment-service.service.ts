﻿export default class ProductsInstallmentService implements ProductsInstallment.Service {

    constructor() {

    }

    mapFromFilter(item: Dto.Product.InstallmentFilter): Dto.Product.Installment {
        var name = "";

        let selectedSeries: Array<Dto.Product.InstallmentEnum> = item.series.filter((i: Dto.Product.InstallmentEnum) => { return i.isSelected === true });
        let selectedRooms: Array<Dto.Product.InstallmentEnum> = item.rooms.filter((i: Dto.Product.InstallmentEnum) => { return i.isSelected === true });
        let selectedFurnitureTypes: Array<Dto.Product.InstallmentEnum> = item.furnitureTypes.filter((i: Dto.Product.InstallmentEnum) => { return i.isSelected === true });
        let selectedVendorCodes: Array<Dto.Product.InstallmentEnum> = item.vendorCodes.filter((i: Dto.Product.InstallmentEnum) => { return i.isSelected === true });

        if (selectedSeries.length <= 0 && selectedRooms.length <= 0 && selectedFurnitureTypes.length <= 0 && selectedVendorCodes.length <= 0) {
            return null;
        }

        if (selectedSeries.length > 0) {
            selectedSeries.forEach((i: Dto.Product.InstallmentEnum, index: number, array: Array<Dto.Product.InstallmentEnum>) => {
                if (index === array.length - 1) { /* last item */
                    name = name + i.syncCode1C;
                } else {/* not last item */
                    name = name + i.syncCode1C + ", ";
                }
            });
        }

        if (selectedRooms.length > 0) {
            name = name !== "" ? name + "; " : "";
            selectedRooms.forEach((i: Dto.Product.InstallmentEnum, index: number, array: Array<Dto.Product.InstallmentEnum>) => {
                if (index === array.length - 1) { /* last item */
                    name = name + i.syncCode1C;
                } else { /* not last item */
                    name = name + i.syncCode1C + ", ";
                }
            });
        }

        if (selectedFurnitureTypes.length > 0) {
            name = name !== "" ? name + "; " : "";
            selectedFurnitureTypes.forEach((i: Dto.Product.InstallmentEnum, index: number, array: Array<Dto.Product.InstallmentEnum>) => {
                if (index === array.length - 1) { /* last item */
                    name = name + i.syncCode1C;
                } else { /* not last item */
                    name = name + i.syncCode1C + ", ";
                }
            });
        };
        if (selectedVendorCodes.length > 0) {
            name = name !== "" ? name + "; " : "";
            selectedVendorCodes.forEach((i: Dto.Product.InstallmentEnum, index: number, array: Array<Dto.Product.InstallmentEnum>) => {
                if (index === array.length - 1) { /* last item */
                    name = name + i.syncCode1C;
                } else { /* not last item */
                    name = name + i.syncCode1C + ", ";
                }
            });
        };

        var result: Dto.Product.Installment = new ItemViewClass(name, true,
            selectedSeries.map((i: Dto.Product.InstallmentEnum) => { return i.id }),
            selectedRooms.map((i: Dto.Product.InstallmentEnum) => { return i.id }),
            selectedFurnitureTypes.map((i: Dto.Product.InstallmentEnum) => { return i.id }),
            selectedVendorCodes.map((i: Dto.Product.InstallmentEnum) => { return i.id }));
        return result;
    }

    public getInstallmentList(): Array<Dto.Product.IInstallmentItem> {
        var installments: Array<Dto.Product.IInstallmentItem>= [
        { id: "0", name: "Без рассрочки" },
        { id: "6", name: "6 месяцев" },
        { id: "10", name: "10 месяцев" },
        { id: "12", name: "12 месяца" },
        { id: "18", name: "18 месяцев" },
        { id: "24", name: "24 месяца" },
        { id: "36", name: "36 месяцев" },
        { id: "48", name: "48 месяцев" },
        ];
        return installments;
    };
}


export class ItemViewClass implements Dto.Product.Installment {

    public id: string;
    public name: string;
    public discount: number;
    public installment6Month: number;
    public installment10Month: number;
    public installment12Month: number;
    public installment18Month: number;
    public installment24Month: number;
    public installment36Month: number;
    public installment48Month: number;
    public dateStart: Date = null;
    public dateEnd: Date = null;
    public series: Array<string> = [];
    public rooms: Array<string> = [];
    public furnitureTypes: Array<string> = [];
    public vendorCodes: Array<string> = [];
    public isNew: boolean;
    public period: any = {};
    public counter: number;

    public constructor(private _name: string, private _isNew: boolean, private _series: Array<string>, private _rooms: Array<string>, _furnitureTypes: Array<string>, _vendorCodes: Array<string>) {
        this.dateStart = new Date();
        this.isNew = _isNew;
        this.series = _series;
        this.rooms = _rooms;
        this.furnitureTypes = _furnitureTypes;
        this.vendorCodes = _vendorCodes;
        this.name = _name;
        this.period = {
            dateStart: this.dateStart,
            dateEnd: this.dateEnd
        }
        this.id = null;
        this.dateEnd = null;
        this.discount = 0;
        this.installment6Month = 0;
        this.installment10Month = 0;
        this.installment12Month = 0;
        this.installment18Month = 0;
        this.installment24Month = 0;
        this.installment36Month = 0;
        this.installment48Month = 0;
    }
}