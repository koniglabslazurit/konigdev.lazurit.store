﻿import * as angular from "angular";

import serviceApi from "./products-installment-service.api.service";
import serviceInstallment from "./products-installment-service.service"

/*constants*/
export const name = "kd.products.installment.service";
export const serviceNameApi = "productsInstallmentApiService";
export const serviceInstallmentName = "productsInstallmentService";


angular
    .module(name, [ ])
    .service(serviceInstallmentName, [serviceInstallment])
    .service(serviceNameApi, ['$http', serviceApi]);