﻿import * as angular from "angular";

import * as serviceModule from "../products-installment-service/products-installment-service.module"
import * as installmentFilterModule from "../products-installment-filter/products-installment-filter.module"
import * as adminDatePickerPangeModule from "../../../common/components-admin/admin-date-picker-range/admin-date-picker-range.module";
import * as commonModule from "../../../common/common.module";

import controller from "./products-installment-page.controller";

/*constants*/
export const name = "kd.products.installment.page";
const controllerName = "productsInstallmentController";
const componentName = "kdProductsInstallment";

angular
    .module(name,
    [        
        serviceModule.name,
        installmentFilterModule.name,
        adminDatePickerPangeModule.name
    ])
    .controller(controllerName,
    [
        serviceModule.serviceInstallmentName, serviceModule.serviceNameApi, commonModule.messageServiceName,
        controller
    ])
    .component(componentName,
    {
        bindings: {},
        controller: controllerName,
        template: require("./products-installment-page.view.html")
    });