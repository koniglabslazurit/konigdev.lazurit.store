﻿export default class ProductsInstallmentController {

    public showFilter: boolean;
    public sets: Array<Dto.Product.Installment>;
    public isEndOpen: boolean;
    public counter: number;

    public constructor(
        private service: ProductsInstallment.Service,
        private apiService: ProductsInstallment.ApiService,
        private messageService: Common.Message.IMessage
    ) {
        this.showFilter = false;
        this.sets = [];
        this.isEndOpen = false;
        this.counter = 0;
        this.getInstallemnt();
    }



    public checkValue(installment: Dto.Product.Installment): void {
        if (!installment) {
            return
        }
        if (installment.discount < 0 || installment.discount === undefined) {
            installment.discount = 0;
        } else if (installment.discount > 100) {
            installment.discount = 100;
        } else if (installment.installment6Month < 0 || installment.installment6Month === undefined) {
            installment.installment6Month = 0;
        } else if (installment.installment6Month > 100) {
            installment.installment6Month = 100;
        } else if (installment.installment10Month < 0 || installment.installment10Month === undefined) {
            installment.installment10Month = 0;
        } else if (installment.installment10Month > 100) {
            installment.installment10Month = 100;
        } else if (installment.installment12Month < 0 || installment.installment12Month === undefined) {
            installment.installment12Month = 0;
        } else if (installment.installment12Month > 100) {
            installment.installment12Month = 100;
        } else if (installment.installment18Month < 0 || installment.installment18Month === undefined) {
            installment.installment18Month = 0;
        } else if (installment.installment18Month > 100) {
            installment.installment18Month = 100;
        } else if (installment.installment24Month < 0 || installment.installment24Month === undefined) {
            installment.installment24Month = 0;
        } else if (installment.installment24Month > 100) {
            installment.installment24Month = 100;
        } else if (installment.installment36Month < 0 || installment.installment36Month === undefined) {
            installment.installment36Month = 0;
        } else if (installment.installment36Month > 100) {
            installment.installment36Month = 100;
        } else if (installment.installment48Month < 0 || installment.installment48Month === undefined) {
            installment.installment48Month = 0;
        } else if (installment.installment48Month > 100) {
            installment.installment48Month = 100;
        }
    }

    public getInstallemnt() {
        this.apiService.getInstallment({ IsFuture: true })
            .then((response: any) => {
                for (var item of response.data.items) {
                    item.dateEnd = new Date(item.dateEnd);
                    item.dateStart = new Date(item.dateStart);
                    item.isNew = false;
                    item.counter = this.counter;
                    this.counter++;
                }
                this.sets = response.data.items;
            });
    }

    public addInstallment() {
        this.showFilter = true;
    }

    public selectFilter = (response: Dto.Product.Installment) => {
        this.showFilter = false;
        response.counter = this.counter;
        this.counter++;
        this.sets.push(response);
    }

    public open() {
        this.isEndOpen = true;
    }


    public onClose = () => {
        this.showFilter = false;
    }

    public delete(item: Dto.Product.Installment) {
        if (!item.isNew) {
            this.apiService.deleteInstallment(item.id).then(() => {
                var indexOfDeletingItem = this.sets.indexOf(item);
                this.sets.splice(indexOfDeletingItem, 1);
            }).catch((error) => {
                console.log(error);
            });
        } else {
            var indexOfDeletingItem = this.sets.indexOf(item);
            this.sets.splice(indexOfDeletingItem, 1);
        }
    }

    public copy(item: Dto.Product.Installment) {
        var set: Dto.Product.Installment = {
            id: null, name: item.name, dateStart: item.dateStart, dateEnd: item.dateEnd, discount: 0, furnitureTypes: item.furnitureTypes, installment10Month: 0, installment12Month: 0, installment18Month: 0, installment24Month: 0, installment36Month: 0, installment48Month: 0, installment6Month: 0, isNew: true, rooms: item.rooms, series: item.series, vendorCodes: item.vendorCodes, counter: this.counter
        };
        this.counter++;
        this.sets.push(set);
    }

    public save() {
        var newItems = this.sets.filter((i: Dto.Product.Installment) => { return i.isNew === true });
        if (newItems.length <= 0) {
            this.messageService.warning("Нет данных на сохранение");
            return;
        }
        if (this.isSuchInstallmentExist()) {
            this.messageService.warning("Скидка на эти даты с такими же фильтрами существует");
            return;
        }
        for (var item of newItems) {
            if (!item.dateEnd)
                item.dateEnd = item.dateStart;
        }

        for (var item of newItems) {
            this.apiService.updateProducts(item)
                .then((response: any) => {
                    item.isNew = false;
                    this.messageService.info("Успешно сохранено для " + item.name);
                    this.getInstallemnt();
                }).catch((error: any) => {
                    this.messageService.uiError("Ошибка сохранения для " + item.name);
                    console.log(error);
                });
        }
    }
    public isSuchInstallmentExist(): boolean {
        var newItems = this.sets.filter((i: Dto.Product.Installment) => { return i.isNew === true });
        var counterExistingInstallments = 0;
        newItems.forEach((i) => {
            if (this.sets.some((set) => {
                const checkDate = i.dateStart <= set.dateEnd && i.dateStart >= set.dateStart
                    || i.dateEnd >= set.dateStart && i.dateEnd <= set.dateEnd
                    || i.dateStart <= set.dateStart && i.dateEnd >= set.dateEnd;
                const checkFurnitureTypes = i.furnitureTypes.length === 0
                    || set.furnitureTypes.length === 0
                    || i.furnitureTypes.some((iF) => set.furnitureTypes.some((sF) => iF === sF));
                const checkRooms = i.rooms.length === 0
                    || set.rooms.length === 0
                    || i.rooms.some((iR) => set.rooms.some((sR) => iR === sR));
                const checkSeries = i.series.length === 0
                    || set.series.length === 0
                    || i.series.some((iS) => set.series.some((sS) => iS === sS));
                const checkVendorCodes = i.vendorCodes.length === 0
                    || set.vendorCodes.length === 0
                    || i.vendorCodes.some((iV) => set.vendorCodes.some((sV) => iV === sV));
                return i.counter !== set.counter
                    && (checkDate)
                    && (checkFurnitureTypes)
                    && (checkRooms)
                    && (checkSeries)
                    && (checkVendorCodes)
            })) {
                counterExistingInstallments++;
            }
        });
        if (counterExistingInstallments > 0) {
            return true;
        } else {
            return false;
        }
    }
}