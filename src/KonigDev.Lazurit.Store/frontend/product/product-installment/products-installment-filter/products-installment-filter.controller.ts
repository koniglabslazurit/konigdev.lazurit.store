﻿export default class ProductsInstallmentFilterController {

    public show: boolean;
    public onSelect: any;
    public onClose: any;

    public series: Array<any>;
    public rooms: Array<any>;
    public furnitureTypes: Array<any>;
    public vendorCodes: Array<any>;

    public isAllSeriesSelected: boolean;
    public isAllRoomsSelected: boolean;
    public isAllFurnitureTypesSelected: boolean;
    public isAllVendorCodesSelected: boolean;

    constructor(
        private apiService: ProductsInstallment.ApiService,
        private service: ProductsInstallment.Service
    ) {
        this.show = false;
        this.series = [];
        this.rooms = [];
        this.furnitureTypes = [];
        this.vendorCodes = [];
        this.isAllSeriesSelected = false;
        this.isAllRoomsSelected = false;
        this.isAllFurnitureTypesSelected = false;
        this.isAllVendorCodesSelected = false;
        this.init();
    }

    public init() {
        this.getSeries();
        this.getRooms();
        this.getFuritureTypes();
    }

    public ok() {
        if (typeof (this.onSelect() === "function")) {
            var response: Dto.Product.InstallmentFilter = {
                series: this.series.filter((item: Dto.Product.InstallmentEnum) => { return item.isSelected === true }),
                rooms: this.rooms.filter((item: Dto.Product.InstallmentEnum) => { return item.isSelected === true }),
                furnitureTypes: this.furnitureTypes.filter((item: Dto.Product.InstallmentEnum) => { return item.isSelected === true }),
                vendorCodes: this.vendorCodes.filter((item: Dto.Product.InstallmentEnum) => { return item.isSelected === true })
            };
            var model = this.service.mapFromFilter(response);
            if (model) {
                this.onSelect()(model);
                this.initialProperties();
            }
        }
    }

    initialProperties() {
        this.series = this.series.map((s) => {
            s.isSelected = false;
            return s;
        })
        this.rooms = this.rooms.map((r) => {
            r.isSelected = false;
            return r;
        })
        this.furnitureTypes = this.furnitureTypes.map((f) => {
            f.isSelected = false;
            return f;
        })
        this.vendorCodes = this.vendorCodes.map((v) => {
            v.isSelected = false;
            return v;
        }) 
        this.isAllSeriesSelected = false;
        this.isAllRoomsSelected=false;
        this.isAllFurnitureTypesSelected=false;
        this.isAllVendorCodesSelected=false;
    }

    public closeModal() {
        this.show = false;
        if (typeof (this.onClose() === 'function')) {
            this.onClose()();
        }
    }

    public getSeries() {
        this.apiService.getSeries()
            .then((data: any) => {
                this.series = data.data;
            })
    }

    public getRooms() {
        this.apiService.getRooms()
            .then((data: any) => {
                this.rooms = data.data;
            })
    }

    public getFuritureTypes() {
        this.apiService.getFuritureTypes()
            .then((data: any) => {
                this.furnitureTypes = data.data;
            })
    }

    public getVendorCodes() {
        var request: ProductsInstallment.Query = {
            furnitureTypesIds: this.furnitureTypes
                .filter((item: any) => { return item.isSelected === true })
                .map((item: any) => { return item.id }),
            seriesIds: this.series
                .filter((item: any) => { return item.isSelected === true })
                .map((item: any) => { return item.id }),
            rooms: this.rooms
                .filter((item: any) => { return item.isSelected === true })
                .map((item: any) => { return item.id })
        };
        if (request.furnitureTypesIds.length > 0 && request.rooms.length > 0 && request.seriesIds.length > 0) {
            this.apiService.getVendorCodes(request)
                .then((data: any) => {
                    this.vendorCodes = data.data;
                })
        } else {
            this.vendorCodes = [];
        }
    }

    public selectAllSeries() {
        for (let item of this.series) {
            item.isSelected = this.isAllSeriesSelected;
        }
        this.getVendorCodes();
    }


    public selectAllRooms() {
        for (let item of this.rooms) {
            item.isSelected = this.isAllRoomsSelected;
        }
        this.getVendorCodes();
    }

    public selectAllFurnitures() {
        for (let item of this.furnitureTypes) {
            item.isSelected = this.isAllFurnitureTypesSelected;
        }
    }

    public selectAllVendorCodes() {
        for (let item of this.vendorCodes) {
            item.isSelected = this.isAllVendorCodesSelected;
        }
    }
}