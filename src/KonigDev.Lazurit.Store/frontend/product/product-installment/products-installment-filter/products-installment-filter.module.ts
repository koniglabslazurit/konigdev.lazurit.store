﻿import * as angular from "angular";

import * as serviceModule from "../products-installment-service/products-installment-service.module"

import controller from "./products-installment-filter.controller";

/*constants*/
export const name = "kd.products.installment.filter";
const controllerName = "productsInstallmentFilterController";
const componentName = "kdProductsInstallmentFilter";

angular
    .module(name,
    [
        serviceModule.name
    ])
    .controller(controllerName,
    [
        serviceModule.serviceNameApi,
        serviceModule.serviceInstallmentName,
        controller
    ])
    .component(componentName,
    {
        bindings: {
            show: '<',
            onSelect: '&',
            onClose: '&'
        },
        controller: controllerName,
        template: require("./products-installment-filter.view.html")
    });