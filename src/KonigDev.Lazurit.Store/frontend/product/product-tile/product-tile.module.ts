﻿import * as angular from "angular";

import * as commonModule from "../../common/common.module";
import * as constantsModule from "../../constants/constants.module";
import * as cartServiceModule from "../../cart/cart-service/cart-mini-service.module";
import * as favoriteTriggerModule from "../../favorite/favorite-trigger/favorite-trigger.module";

import controller from "./product-tile.controller";

export const name = "kd.product.tile";

const productTileComponentName = "kdProducts";
const controllerName = "productTileController";

angular
    .module(name,
    [
        commonModule.name,
        constantsModule.name,
        cartServiceModule.name,
        favoriteTriggerModule.name
    ])
    .controller(controllerName,
    [
        commonModule.messageServiceName, constantsModule.serviceName, cartServiceModule.cartServiceName,
        controller
    ])
    .component(productTileComponentName, {
        bindings: {
            product: '<',
            settings: '<',
            imagesContent: '<',
            marketerProduct: '<'
        },
        controller: controllerName,
        template: require("./product-tile.view.html")
    });