﻿export default class ProductTile {
    public product: any;
    public settings: any;
    public marketerProduct: any;
    public currentSize: any;

    public constructor(
        private messageService: Common.Message.IMessage,
        private constants: Constants.IConstants,
        private CartService: Cart.Services.ICartService
    ) { }    

    $onChanges(changesObj): void {
        if (changesObj.marketerProduct && changesObj.marketerProduct.currentValue) {

            this.marketerProduct.sizes.forEach((i: any) => {
                i.id = i.productId;
                i.name = i.height + "x" + i.length + "x" + i.width;
            });

            this.currentSize = this.marketerProduct.sizes.filter((s: any) => {
                return s.id === this.marketerProduct.maxRangeProductId;
            })[0];

            for (var item of this.marketerProduct.products) {
                if (item.id === this.marketerProduct.maxRangeProductId) {
                    item.isCurrent = true;
                } else {
                    item.isCurrent = false;
                }
            }
        }
    };

    public onSelectItem = (selectedValue: any) => {
        console.log(selectedValue);

        if (selectedValue) {
            for (var item of this.marketerProduct.products) {
                if (item.id === selectedValue.id) {
                    item.isCurrent = true;
                } else {
                    item.isCurrent = false;
                }
            }
        }

        this.currentSize = this.marketerProduct.sizes.filter((s: any) => {
            return s.id === selectedValue.id;
        })[0];
    };

    public addToCart(product: any): any {
        // todo здесь передавалась также collectionId, которая на бэкенде не используется. если что, проверьте
        var model = { ProductId: product.id };
        product.isAddingToCart = true;
        this.CartService.addProductToCart(model)
            .then((response: any) => {
                if (response.status === 200) {
                }
                else {
                    this.messageService.uiError(this.constants.cart.goodsNotAdded);
                }
                product.isAddingToCart = false;
            }).catch((error: any) => {
                this.messageService.error(error.data.message, error);
                product.isAddingToCart = false;
            });
    };
};