﻿import * as angular from "angular";

import * as commonModule from "../../common/common.module";
import * as constantsModule from "../../constants/constants.module";
import * as productModule from "../product.module";
import * as filterModule from "../../filter/filter.module";
import * as furnitureTypeModule from "../../furniture-type/furniture-type.module";
import * as roomModule from "../../room/room.module";
import * as geolocationModule from "../../geolocation/geolocation.module";

import controller from "./product-list.controller";

export const name = "kd.product.list";

const productTileComponentName = "kdProductsList";
const controllerName = "productListController";

angular
    .module(name,
    [
        commonModule.name,
        constantsModule.name,
        productModule.name,
        filterModule.name,
        furnitureTypeModule.name,
        roomModule.name
    ])
    .controller(controllerName,
    [
        commonModule.messageServiceName, constantsModule.serviceName, productModule.serviceName,
        filterModule.serviceName, furnitureTypeModule.serviceName, roomModule.serviceName,
        geolocationModule.geolocationServiceName,
        controller
    ])
    .component(productTileComponentName, {
        bindings: {
            furnitureAlias: '<',
            parentFurnitureAlias:'<'
        },
        controller: controllerName,       
        template: require("./product-list.view.html")
    });