﻿export default class ProductList {

    public furnitureAlias: string;
    public parentFurnitureAlias: string;

    public products: Array<Dto.Product.IProductPrice>;
    public furnitureTypes: Array<Dto.FurnitureType.DtoFurnitureTypeItem>;
    public roomTypes: Array<Dto.RooomType.IRoomByProductItem>;
    /* все доступные фильтры */
    public facingColors: Array<Dto.Filter.IDtoFacingColorItem>;
    public frameColors: Array<Dto.Filter.IDtoFrameColorItem>;
    public facings: Array<Dto.Filter.IDtoFacingItem>;
    /* выбранные фильтры */
    public selectedFrameColors: Array<any> = [];
    public selectedFacingColors: Array<any> = [];
    public selectedFacings: Array<any> = [];

    private request: Dto.Product.ProductRequest = { facingIds: [], facingColorIds: [], frameColorIds: [], furnitureTypeId: '', furnitureAlias: '', collectionId: '', collectionVariantId: '', userId: '' };

    public constructor(
        private messageService: Common.Message.IMessage,
        private constants: Constants.IConstants,
        private ProductService: ProductPage.Services.IApiProductService,
        private FilterService: Filter.Services.IFilterApiService,
        private FurnituretypeService: FurnitureType.Services.IFurnitureTypeApiService,
        private RoomService: Room.Services.IRoomApiService,
        private geolocationService: any
    ) {
        this.init();
    }

    init(): void {
        if (this.furnitureAlias) {
            this.request.furnitureAlias = this.furnitureAlias;
        }
        if (!this.parentFurnitureAlias) {
            this.parentFurnitureAlias = this.furnitureAlias;
        }
        this.getFilters();
        this.getFurnitureTypes();
        this.getProducts();
        this.getRoomTypes();
    }

    refresh(): void {
        this.getFurnitureTypes();
        this.getProducts();
        this.getRoomTypes();
    }

    getProducts(): void {
        this.geolocationService.getNearbyCities()
            .then((result: any) => {
                return this.ProductService.getProducts(this.request)
            })
            .then((response) => {
                this.products = response.items;
            }).catch((error) => {
                console.log(error);
            });
    }

    getFilters(): void {
        this.FilterService.getFiltersByProduct(this.request).then((response) => {
            this.facingColors = response.facingColors;
            this.facings = response.facings;
            this.frameColors = response.frameColors;
        }).catch((error) => {
            console.log(error);
        });
    }

    getFurnitureTypes(): void {
        this.FurnituretypeService.getFurnitureTypesByProduct(this.request).then((response) => {
            this.furnitureTypes = response;
        }).catch((error) => {
            console.log(error);
        });
    }

    getRoomTypes(): void {
        this.RoomService.getRoomTypesByProduct(this.request).then((response) => {
            this.roomTypes = response;
        }).catch((error) => {
            console.log(error);
        });
    }

    /* выбор цвета корпуса */
    changeFrameColor = (newVal: Array<any>) => {
        this.selectedFrameColors = newVal;
        this.request.frameColorIds = this.selectedFrameColors;
        this.refresh();
    }

    /* выбор отделки */
    changeFacing = (newVal) => {
        this.selectedFacings = newVal;
        this.request.facingIds = this.selectedFacings;
        this.refresh();
    }

    /* выбор цвета отделки */
    changeFacingColor = (newVal) => {
        this.selectedFacingColors = newVal;
        this.request.facingColorIds = this.selectedFacingColors;
        this.refresh();
    }
}
