﻿import FilterMultiSelectController from "../../filter/filter-multi-select/filter-multi-select.controller";

/* import mock data from mock module */
import * as mockData from "./filter-multi-select.mock";

describe("HeaderToolsController", () => {
    let classforTest: FilterMultiSelectController = new FilterMultiSelectController();

    it("should map and mark first item as selected", () => {
        //Arrange
        classforTest.items = JSON.parse(JSON.stringify(mockData.items));
        classforTest.kdModel = JSON.parse(JSON.stringify(mockData.kdModel));
        classforTest.listItems = JSON.parse(JSON.stringify(mockData.listItems));
        //Act

        //Assert        
        expect(classforTest.isSelected(classforTest.items[0])).toBe(true);
        expect(classforTest.isSelected(classforTest.items[1])).toBe(false);
        expect(classforTest.isSelected(classforTest.items[2])).toBe(true);
    });

    it("should delete item from kdModel", () => {
        //Arrange
        classforTest.items = JSON.parse(JSON.stringify(mockData.items));
        classforTest.kdModel = JSON.parse(JSON.stringify(mockData.kdModel));
        classforTest.listItems = JSON.parse(JSON.stringify(mockData.listItems));
        classforTest.kdChange = JSON.parse(JSON.stringify(mockData.kdChange));
        let item = JSON.parse(JSON.stringify(mockData.item1));
        //Act
        classforTest.itemRemove(item);
        //Assert        
        expect(classforTest.kdModel.length).toBe(1);
        expect(classforTest.kdModel[0]).toBe("111");
        expect(classforTest.isSelected(item)).toBe(false);
    });

    it("should add item to kdModel", () => {
        //Arrange
        classforTest.items = JSON.parse(JSON.stringify(mockData.items));
        classforTest.kdModel = JSON.parse(JSON.stringify(mockData.kdModel));
        classforTest.listItems = JSON.parse(JSON.stringify(mockData.listItems));
        classforTest.kdChange = JSON.parse(JSON.stringify(mockData.kdChange));
        let item = JSON.parse(JSON.stringify(mockData.item2));
        //Act
        classforTest.itemSelect(item);
        //Assert        
        expect(classforTest.kdModel.length).toBe(3);
        expect(classforTest.kdModel[2]).toBe("222");
        expect(classforTest.isSelected(item)).toBe(true);
    });
});

