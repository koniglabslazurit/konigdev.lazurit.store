﻿import FavoriteProfileController from "../../../favorite/favorite-profile/favorite-profile.controller";
import FavoriteService from "../../../favorite/favorite-service/favorite-service.service";
import Constants from "../../../constants/constants";
import MessageService from "../../../common/services/message.service";
import CartService from "../../../cart/cart-service/cart-full-service.service";

/* import mock data from mock module */
import * as mockData from "./favorite-profile.mock";

describe("FavoriteProfileController", () => {

    let classforTest: FavoriteProfileController;
    let favoriteService: any = {
        getFavoriteByLinkId: () => {
            return {
                then: (callback) => {
                    return callback(
                        {
                            id: "111",
                            products: [
                                JSON.parse(JSON.stringify(mockData.favoriteItem1)),
                                JSON.parse(JSON.stringify(mockData.favoriteItem2))
                            ],
                            complects: [],
                            collections: [],
                            priceForDelivery: JSON.parse(JSON.stringify(mockData.price)),
                            totalPrice: JSON.parse(JSON.stringify(mockData.price))
                        }
                    );
                }
            }
        },
        getShareLinkId: () => {
            return {
                then: (callback) => {
                    return callback({ id: "123" });
                }
            }
        },
    };
    let constants: Constants;
    let messageService: MessageService;
    let cartService: CartService;
    let $document: any;

    classforTest = new FavoriteProfileController(favoriteService, constants, messageService, cartService, $document);


    it("should have products in favorite after initializing", () => {
        //Arrange
        //Act
        //Assert
        expect(classforTest.favorite.id).toBe("111");
        expect(classforTest.favorite.products.length).toBe(2);
    });

    //it("should create correct link lazurit/123", () => {
    //    //Arrange
    //    classforTest.isUserOwnerBool = true;
    //    classforTest.patch = "lazurit"
    //    //Act
    //    classforTest.getShareLink();
    //    //Assert
    //    expect(classforTest.shareLink).toBe("lazurit/123");
    //});

    it("should create correct link lazurit/000", () => {
        //Arrange
        classforTest.isUserOwnerBool = false;
        classforTest.linkId = "000";
        classforTest.patch = "lazurit"
        //Act
        classforTest.getShareLink();
        //Assert
        expect(classforTest.shareLink).toBe("lazurit/000");
    });
});

