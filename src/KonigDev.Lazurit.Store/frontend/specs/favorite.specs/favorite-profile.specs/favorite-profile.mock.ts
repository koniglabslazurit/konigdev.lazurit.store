﻿export const favoriteItem1: Dto.Favorite.IFavoriteProduct = {
    id: "222",
    product: {
        id: "333",
        article: "1",
        furnitureType: "1",
        furnitureTypeAlias: "",
        collectionAlias: "",
        collectionName: "",
        discount: 10,
        facingColorName: "",
        facingName: "",
        frameColorIdName: "",
        priceForAssembly: {
            internalAmount: 1,
            amount: 1,
            truncatedAmount: 1,
            formattedAmount: "1",
            formattedAmountWithoutPoint: "1",
            formattedAmountWithoutPointAndCurrency: "1",
            decimalDigits: 1
        },
        syncCode1C: "1",
        width: 1,
        length: 1,
        height: 1,
        price: {
            internalAmount: 1,
            amount: 1,
            truncatedAmount: 1,
            formattedAmount: "1",
            formattedAmountWithoutPoint: "1",
            formattedAmountWithoutPointAndCurrency: "1",
            decimalDigits: 1
        },
        salePrice: {
            internalAmount: 1,
            amount: 1,
            truncatedAmount: 1,
            formattedAmount: "1",
            formattedAmountWithoutPoint: "1",
            formattedAmountWithoutPointAndCurrency: "1",
            decimalDigits: 1
        }
    }
}

export const favoriteItem2: Dto.Favorite.IFavoriteProduct = {
    id: "444",
    product: {
        id: "555",
        article: "1",
        furnitureType: "1",
        furnitureTypeAlias: "",
        collectionAlias: "",
        collectionName: "",
        discount: 10,
        facingColorName: "",
        facingName: "",
        frameColorIdName: "",
        priceForAssembly: {
            internalAmount: 1,
            amount: 1,
            truncatedAmount: 1,
            formattedAmount: "1",
            formattedAmountWithoutPoint: "1",
            formattedAmountWithoutPointAndCurrency: "1",
            decimalDigits: 1
        },
        syncCode1C: "1",
        width: 1,
        length: 1,
        height: 1,
        price: {
            internalAmount: 1,
            amount: 1,
            truncatedAmount: 1,
            formattedAmount: "1",
            formattedAmountWithoutPoint: "1",
            formattedAmountWithoutPointAndCurrency: "1",
            decimalDigits: 1
        },
        salePrice: {
            internalAmount: 1,
            amount: 1,
            truncatedAmount: 1,
            formattedAmount: "1",
            formattedAmountWithoutPoint: "1",
            formattedAmountWithoutPointAndCurrency: "1",
            decimalDigits: 1
        }
    }
}

export const price: Dto.Money.IMoney = {
    internalAmount: 1,
    amount: 1,
    truncatedAmount: 1,
    formattedAmount: "1",
    formattedAmountWithoutPoint: "1",
    formattedAmountWithoutPointAndCurrency: "1",
    decimalDigits: 1
}
