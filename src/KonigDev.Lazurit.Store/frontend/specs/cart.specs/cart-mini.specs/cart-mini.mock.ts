﻿export const cartResultMock = {
    products: [],
    complects: [],
    collections: [],
    id: ""
};

export const productMock: Dto.Product.IProduct = {
    id: "12312312312312",
    article: "q123",
    articleId: "123123",
    collection: "1231",
    seriesName:"Orneta",
    furnitureType: "type",
    syncCode1C: "123wer",
    width: 50,
    length: 100,
    height: 200
};

//export const productResultMock: Dto.CartMiniItem.ICartMiniProduct = {
//    quantity: 1,
//    id: '123123',
//    productId: "12312312312312",
//    product: productMock,
//    price: 1000,
//    salePrice : 1500
//};