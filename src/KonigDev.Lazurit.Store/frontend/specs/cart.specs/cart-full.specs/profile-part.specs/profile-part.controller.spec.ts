﻿import ProfilePartController from "../../../../cart/cart-full/profile-part/profile-part.controller";

/* import mock data from mock module */
import * as mockData from "./profile-part.mock";

describe("ProfilePartController", () => {

    let classforTest: ProfilePartController = new ProfilePartController();

    it("should copy default phone from profile to field", () => {
        //Arrange
        classforTest.profile = mockData.profile;
        //Act
        classforTest.map();
        //Assert
        expect(classforTest.defaultPhone.id).toBe("111");
    });

    it("should push new empty default phone to profile", () => {
        //Arrange
        classforTest.profile = mockData.profile;
        classforTest.profile.userPhones = [];
        //Act
        classforTest.map();
        //Assert
        expect(classforTest.defaultPhone.phone).toBe("");
        expect(classforTest.defaultPhone.isDefault).toBe(true);
        expect(classforTest.profile.userPhones.length).toBe(1);
        expect(classforTest.profile.userPhones[0].phone).toBe("");
    });
});

