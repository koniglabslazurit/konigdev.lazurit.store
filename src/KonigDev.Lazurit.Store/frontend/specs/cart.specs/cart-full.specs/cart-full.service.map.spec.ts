import CartFullController from "../../../cart/cart-full/cart-full.controller";
import messageService from "../../../common/services/message.service";
import constantsService from "../../../constants/constants";
import layawayService from "../../../layaway/layaway-service/layaway-service.service";
import cartFullMapService from "../../../cart/cart-full/cart-full.service.map";
import cartService from "../../../cart/cart-full/cart-full.service";
import profileService from "../../../profile/profile-services/profile.service";
import CartFullMapService from "../../../cart/cart-full/cart-full.service.map";
import CartFullService from "../../../cart/cart-full/cart-full.service";

/* import mock data from mock module */
import * as mockData from "./cart-full.mock";

describe("CartFullMapService", () => {
    //todo надо глянуть фреймворк какой нибудь для генерации данных, чтобы не писать простыни кода.     
    let classforTest: CartFullMapService;
    let cartFullService = new CartFullService();
    classforTest = new CartFullMapService(cartFullService);

    it("should get correct map cart with empty arrays", () => {
        //Arrange        
        var item = { id: "123", products: [], complects: [], collections: [], priceForDelivery: {} }
        //Act
        var res = classforTest.mappingCart(item);
        //Assert        
        expect(res.id).toBe("123");
        expect(res.products.length).toBe(0);
        expect(res.complects.length).toBe(0);
        expect(res.collections.length).toBe(0);
    });
                                                                                                                  
    it("should get correct cart order count with 2 products and 1 collections and complects", () => {
        //Arrange
        var id = "1";
        var twoId = "2";
        var priceDto = { amount: 10 }; 
        var item = {
            id: "123",
            products: [{
                id: id, quantity: 1, price: { amount: 10 }, salePrice: { amount: 10 }, discount: 0,
                salePriceInstallment6Month:priceDto,
                salePriceInstallment10Month: priceDto,
                salePriceInstallment12Month: priceDto,
                salePriceInstallment18Month: priceDto,
                salePriceInstallment24Month: priceDto,
                salePriceInstallment36Month: priceDto,
                salePriceInstallment48Month: priceDto,
                salePriceWithAssembly: priceDto,
                salePriceInstallment6MonthWithAssembly: priceDto,
                salePriceInstallment10MonthWithAssembly: priceDto,
                salePriceInstallment12MonthWithAssembly: priceDto,
                salePriceInstallment18MonthWithAssembly: priceDto,
                salePriceInstallment24MonthWithAssembly: priceDto,
                salePriceInstallment36MonthWithAssembly: priceDto,
                salePriceInstallment48MonthWithAssembly: priceDto,
                priceInTotal: priceDto,   
                isAssemblyRequired: false,
                priceForAssembly: priceDto    
            }, {
                    id: twoId, quantity: 2, price: { amount: 100 }, salePrice: { amount: 10 }, discount: 10, salePriceInstallment6Month: priceDto,
                    salePriceInstallment10Month: priceDto,
                    salePriceInstallment12Month: priceDto,
                    salePriceInstallment18Month: priceDto,
                    salePriceInstallment24Month: priceDto,
                    salePriceInstallment36Month: priceDto,
                    salePriceInstallment48Month: priceDto,
                    salePriceWithAssembly: priceDto,
                    salePriceInstallment6MonthWithAssembly: priceDto,
                    salePriceInstallment10MonthWithAssembly: priceDto,
                    salePriceInstallment12MonthWithAssembly: priceDto,
                    salePriceInstallment18MonthWithAssembly: priceDto,
                    salePriceInstallment24MonthWithAssembly: priceDto,
                    salePriceInstallment36MonthWithAssembly: priceDto,
                    salePriceInstallment48MonthWithAssembly: priceDto,
                    priceInTotal: priceDto,
                    isAssemblyRequired: false,
                    priceForAssembly: priceDto    }],
            complects: [],
            collections: [{
                id: id, products: [{
                    id: id, quantity: 1, price: { amount: 10 }, salePrice: { amount: 10 }, discount: 10, isAssemblyRequired: true, priceForAssembly: { amount: 2 },
                    salePriceInstallment10Month: priceDto,
                    salePriceInstallment12Month: priceDto,
                    salePriceInstallment18Month: priceDto,
                    salePriceInstallment24Month: priceDto,
                    salePriceInstallment36Month: priceDto,
                    salePriceInstallment48Month: priceDto,
                    salePriceWithAssembly: priceDto,
                    salePriceInstallment6MonthWithAssembly: priceDto,
                    salePriceInstallment10MonthWithAssembly: priceDto,
                    salePriceInstallment12MonthWithAssembly: priceDto,
                    salePriceInstallment18MonthWithAssembly: priceDto,
                    salePriceInstallment24MonthWithAssembly: priceDto,
                    salePriceInstallment36MonthWithAssembly: priceDto,
                    salePriceInstallment48MonthWithAssembly: priceDto,
                    priceInTotal: priceDto                 }]
            }],
            priceForDelivery: {}, totalPrice: { amount: 1 }
        }
        //Act
        var res = classforTest.mappingCart(item);
        //Assert
        expect(res.orderCount).toBe(3);
    });
                                                                                
    it("should get correct map cart priceForDelivery", () => {
        //Arrange        
        var data = { id: "123", priceForDelivery: { amount: 10, }, isDeliveryRequired: false, orderCount: 2, totalPrice: { amount: 100 }, products: [], complects: [], collections: [] }
        //Act
        var res = classforTest.mappingCart(data);
        //Assert        
        expect(res.priceForDelivery).toBe(10);
    });

    it("should get correct map cart isDeliveryRequired false", () => {
        //Arrange        
        var data = { id: "123", priceForDelivery: { amount: 10, }, isDeliveryRequired: false, orderCount: 2, totalPrice: { amount: 100 }, products: [], complects: [], collections: [] }
        //Act
        var res = classforTest.mappingCart(data);
        //Assert        
        expect(res.isDeliveryRequired).toBe(false);
    });                                     

    it("should get correct map cart isDeliveryRequired false", () => {
        //Arrange        
        var data = { id: "123", priceForDelivery: { amount: 10, }, isDeliveryRequired: true, orderCount: 2, totalPrice: { amount: 100 }, products: [], complects: [], collections: [] }
        //Act
        var res = classforTest.mappingCart(data);
        //Assert        
        expect(res.isDeliveryRequired).toBe(true);
    });

    it("should get correct map cart totalPrice when cart is empty", () => {
        //Arrange        
        var data = { id: "123", priceForDelivery: { amount: 10, }, isDeliveryRequired: true, orderCount: 2, totalPrice: { amount: 100 }, products: [], complects: [], collections: [] }
        //Act
        var res = classforTest.mappingCart(data);
        //Assert        
        expect(res.totalPrice).toBe(0);
    });
                                                                  
});