import CartFullService from "../../../cart/cart-full/cart-full.service";

import * as mockData from "./cart-full.mock";

class MoneyImpl implements Dto.Money.IMoney {
    public internalAmount = 0;
    public amount = 0;
    public truncatedAmount = 0;
    public formattedAmount = "";
    public formattedAmountWithoutPoint = "";
    public formattedAmountWithoutPointAndCurrency = "";
    public decimalDigits = 0;
}

describe("CartFullService", () => {
    let classforTest = new CartFullService();

    it("should correct return summ product with quantity 1, price 100, discount 0, assembly - false", () => {
        //Arrange        
        var price = new MoneyImpl();
        price.amount = 100;
        var priceForAssembly = new MoneyImpl();
        var item: Dto.CartFull.IProductItem = {
            id: "123", discount: 0, isAssemblyRequired: false, quantity: 1, summ: 0,
            price: price, salePrice: price, 
            priceForAssembly: priceForAssembly,
            salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price
        };
        //Act
        var res = classforTest.getSummProduct(item, undefined).sum;
        //Assert
        expect(res).toBe(100);
    });

    it("should correct return summ product with quantity 2, price 100, discount 0, assembly - false", () => {
        //Arrange        
        var price = new MoneyImpl();
        price.amount = 100;
        var priceForAssembly = new MoneyImpl();
        var item: Dto.CartFull.IProductItem = {
            id: "123", discount: 0, isAssemblyRequired: false, quantity: 2, summ: 0,
            price: price, salePrice: price, 
            priceForAssembly: priceForAssembly,
            salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price
        };
        //Act
        var res = classforTest.getSummProduct(item, undefined).sum;
        //Assert
        expect(res).toBe(200);
    });

    it("should correct return summ product with quantity 1, price 100, discount 0 assembly - true", () => {
        //Arrange        
        var price = new MoneyImpl();
        price.amount = 100;
        var priceForAssembly = new MoneyImpl();
        priceForAssembly.amount = 10;
        var item: Dto.CartFull.IProductItem = {
            id: "123", discount: 0, isAssemblyRequired: true, quantity: 1, summ: 0,
            price: price, salePrice: price, 
            priceForAssembly: priceForAssembly,
            salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price
        };
        //Act
        var res = classforTest.getSummProduct(item, undefined).sum;
        //Assert
        expect(res).toBe(100);
    });

    it("should correct return summ product with quantity 2, price 100, discount 0 assembly - true", () => {
        //Arrange        
        var price = new MoneyImpl();
        price.amount = 100;
        var priceForAssembly = new MoneyImpl();
        priceForAssembly.amount = 10;
        var item: Dto.CartFull.IProductItem = {
            id: "123", discount: 0, isAssemblyRequired: true, quantity: 2, summ: 0,
            price: price, salePrice: price, 
            priceForAssembly: priceForAssembly,
            salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price
        };
        //Act
        var res = classforTest.getSummProduct(item, undefined).sum;
        //Assert
        expect(res).toBe(200);
    });

    it("should correct return summ product with quantity 1, price 100, discount 10 assembly - false", () => {
        //Arrange        
        var price = new MoneyImpl();
        price.amount = 100;
        var priceForAssembly = new MoneyImpl();
        priceForAssembly.amount = 10;
        var item: Dto.CartFull.IProductItem = {
            id: "123", discount: 10, isAssemblyRequired: false, quantity: 1, summ: 0,
            price: price, salePrice: price, 
            priceForAssembly: priceForAssembly,
            salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price
        };
        //Act
        var res = classforTest.getSummProduct(item, undefined).sum;
        //Assert
        expect(res).toBe(100);
    });

    it("should correct return summ product with quantity 1, price 100, discount 10 assembly - true", () => {
        //Arrange        
        var price = new MoneyImpl();
        price.amount = 100;
        var priceForAssembly = new MoneyImpl();
        priceForAssembly.amount = 10;
        var item: Dto.CartFull.IProductItem = {
            id: "123", discount: 10, isAssemblyRequired: true, quantity: 1, summ: 0,
            price: price, salePrice: price, 
            priceForAssembly: priceForAssembly,
            salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price
        };
        //Act
        var res = classforTest.getSummProduct(item, undefined).sum;
        //Assert
        expect(res).toBe(100);
    });

    it("should correct return summ product with quantity 2, price 100, discount 10 assembly - true", () => {
        //Arrange        
        var price = new MoneyImpl();
        price.amount = 100;
        var priceForAssembly = new MoneyImpl();
        priceForAssembly.amount = 10;
        var item: Dto.CartFull.IProductItem = {
            id: "123", discount: 10, isAssemblyRequired: true, quantity: 2, summ: 0,
            price: price, salePrice: price, 
            priceForAssembly: priceForAssembly,
            salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price
        };
        //Act
        var res = classforTest.getSummProduct(item, undefined).sum;
        //Assert
        expect(res).toBe(200);
    });

    /* ---------  Get complect summ ---------------- */
    /* ---------  Get complect summ ---------------- */

    it("should correct return summ complect with quantity 1, price 100, discount 0, assembly - false", () => {
        //Arrange        
        var price = new MoneyImpl();
        price.amount = 100;
        var priceForAssembly = new MoneyImpl();
        var item: Dto.CartFull.IComplectItem = {
            id: "123", discount: 0, isAssemblyRequired: false, quantity: 1, summ: 0,
            price: price, salePrice: price, 
            priceForAssembly: priceForAssembly,
            salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price
        };
        //Act
        var res = classforTest.getSummComplect(item, undefined).sum;
        //Assert
        expect(res).toBe(100);
    });

    it("should correct return summ complect with quantity 2, price 100, discount 0, assembly - false", () => {
        //Arrange        
        var price = new MoneyImpl();
        price.amount = 100;
        var priceForAssembly = new MoneyImpl();
        var item: Dto.CartFull.IComplectItem = {
            id: "123", discount: 0, isAssemblyRequired: false, quantity: 2, summ: 0,
            price: price, salePrice: price, 
            priceForAssembly: priceForAssembly,
            salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price
        };
        //Act
        var res = classforTest.getSummComplect(item, undefined).sum;
        //Assert
        expect(res).toBe(200);
    });

    it("should correct return summ complect with quantity 1, price 100, discount 0 assembly - true", () => {
        //Arrange        
        var price = new MoneyImpl();
        price.amount = 100;
        var priceForAssembly = new MoneyImpl();
        priceForAssembly.amount = 10;
        var item: Dto.CartFull.IComplectItem = {
            id: "123", discount: 0, isAssemblyRequired: true, quantity: 1, summ: 0,
            price: price, salePrice: price, 
            priceForAssembly: priceForAssembly,
            salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price
        };
        //Act
        var res = classforTest.getSummComplect(item, undefined).sum;
        //Assert
        expect(res).toBe(100);
    });

    it("should correct return summ complect with quantity 2, price 100, discount 0 assembly - true", () => {
        //Arrange        
        var price = new MoneyImpl();
        price.amount = 100;
        var priceForAssembly = new MoneyImpl();
        priceForAssembly.amount = 10;
        var item: Dto.CartFull.IComplectItem = {
            id: "123", discount: 0, isAssemblyRequired: true, quantity: 2, summ: 0,
            price: price, salePrice: price, 
            priceForAssembly: priceForAssembly,
            salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price
        };
        //Act
        var res = classforTest.getSummComplect(item, undefined).sum;
        //Assert
        expect(res).toBe(200);
    });

    it("should correct return summ complect with quantity 1, price 100, discount 10 assembly - false", () => {
        //Arrange        
        var price = new MoneyImpl();
        price.amount = 100;
        var priceForAssembly = new MoneyImpl();
        priceForAssembly.amount = 10;
        var item: Dto.CartFull.IComplectItem = {
            id: "123", discount: 10, isAssemblyRequired: false, quantity: 1, summ: 0,
            price: price, salePrice: price, 
            priceForAssembly: priceForAssembly,
            salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price
        };
        //Act
        var res = classforTest.getSummComplect(item, undefined).sum;
        //Assert
        expect(res).toBe(100);
    });

    it("should correct return summ complect with quantity 1, price 100, discount 10 assembly - true", () => {
        //Arrange        
        var price = new MoneyImpl();
        price.amount = 100;
        var priceForAssembly = new MoneyImpl();
        priceForAssembly.amount = 10;
        var item: Dto.CartFull.IComplectItem = {
            id: "123", discount: 10, isAssemblyRequired: true, quantity: 1, summ: 0,
            price: price, salePrice: price, 
            priceForAssembly: priceForAssembly,
            salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price
        };
        //Act
        var res = classforTest.getSummComplect(item, undefined).sum;
        //Assert
        expect(res).toBe(100);
    });

    it("should correct return summ complect with quantity 2, price 100, discount 10 assembly - true", () => {
        //Arrange
        var price = new MoneyImpl();
        price.amount = 100;
        var priceForAssembly = new MoneyImpl();
        priceForAssembly.amount = 10;
        var item: Dto.CartFull.IComplectItem = {
            id: "123", discount: 10, isAssemblyRequired: true, quantity: 2, summ: 0,
            price: price, salePrice: price, 
            priceForAssembly: priceForAssembly,
            salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price
        };
        //Act
        var res = classforTest.getSummComplect(item, undefined).sum;
        //Assert
        expect(res).toBe(200);
    });


    /* ---------  Get total summ ---------------- */
    /* ---------  Get total summ ---------------- */
    it("should correct return total summ when has products, not has complects, not has collection ", () => {
        //Arrange
        var price = new MoneyImpl();
        price.amount = 100;
        var priceForAssembly = new MoneyImpl();
        priceForAssembly.amount = 10;

        var cart: Dto.CartFull.ICart = JSON.parse(JSON.stringify(mockData.cart));
        cart.products.push({
            id: "id", quantity: 1, price: price, salePrice: price, discount: 0, summ: 110, isAssemblyRequired: true, priceForAssembly: priceForAssembly, salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price        });
        //Act
        var res = classforTest.getTotalSumm(cart, undefined).totalSum;
        //Assert
        expect(res).toBe(100);
    });

    it("should correct return total summ when has products, has complects, not has collection ", () => {
        //Arrange
        var price = new MoneyImpl();
        price.amount = 100;
        var priceForAssembly = new MoneyImpl();
        priceForAssembly.amount = 10;

        var cart: Dto.CartFull.ICart = JSON.parse(JSON.stringify(mockData.cart));
        cart.products.push({
            id: "id", quantity: 1, price: price, salePrice: price, discount: 0, summ: 110, isAssemblyRequired: true, priceForAssembly: priceForAssembly, salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price        });
        cart.complects.push({
            id: "id", quantity: 1, price: price, salePrice: price, discount: 0, isAssemblyRequired: false, priceForAssembly: priceForAssembly, summ: 100, salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price        });
        //Act
        var res = classforTest.getTotalSumm(cart, undefined).totalSum;
        //Assert
        expect(res).toBe(200);
    });

    it("should correct return total summ when has products, has complects, has collection ", () => {
        //Arrange
        var price = new MoneyImpl();
        price.amount = 100;
        var priceForAssembly = new MoneyImpl();
        priceForAssembly.amount = 10;

        var cart: Dto.CartFull.ICart = JSON.parse(JSON.stringify(mockData.cart));
        cart.products.push({
            id: "id", quantity: 1, price: price, salePrice: price, discount: 0, summ: 110, isAssemblyRequired: true, priceForAssembly: priceForAssembly, salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price        });
        cart.complects.push({
            id: "id", quantity: 1, price: price, salePrice: price,  discount: 0, isAssemblyRequired: false, priceForAssembly: priceForAssembly, summ: 100, salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price        });
        cart.collections.push({
            id: "id",
            products: [{
                id: "id", quantity: 1, price: price, salePrice: price, discount: 10, isAssemblyRequired: true, priceForAssembly: priceForAssembly, summ: 100, salePriceInstallment6Month: price,
                salePriceInstallment10Month: price,
                salePriceInstallment12Month: price,
                salePriceInstallment18Month: price,
                salePriceInstallment24Month: price,
                salePriceInstallment36Month: price,
                salePriceInstallment48Month: price,
                priceInTotal: price,
                salePriceWithAssembly: price,
                salePriceInstallment6MonthWithAssembly: price,
                salePriceInstallment10MonthWithAssembly: price,
                salePriceInstallment12MonthWithAssembly: price,
                salePriceInstallment18MonthWithAssembly: price,
                salePriceInstallment24MonthWithAssembly: price,
                salePriceInstallment36MonthWithAssembly: price,
                salePriceInstallment48MonthWithAssembly: price            }]
        })
        //Act
        var res = classforTest.getTotalSumm(cart, undefined).totalSum;
        //Assert
        expect(res).toBe(300);
    });

    it("should correct return total summ when not has products, not has complects, not has collection ", () => {
        //Arrange
        var cart: Dto.CartFull.ICart = JSON.parse(JSON.stringify(mockData.cart));
        //Act
        var res = classforTest.getTotalSumm(cart, undefined).totalSum;
        //Assert
        expect(res).toBe(0);
    });

    it("should correct return total summ when has products, not has complects, not has collection and isDeliveryRequired", () => {
        //Arrange
        var price = new MoneyImpl();
        price.amount = 100;
        var priceForAssembly = new MoneyImpl();
        priceForAssembly.amount = 10;

        var cart: Dto.CartFull.ICart = JSON.parse(JSON.stringify(mockData.cart));
        cart.products.push({
            id: "id", quantity: 1, price: price, salePrice: price, discount: 0, summ: 110, isAssemblyRequired: true, priceForAssembly: priceForAssembly, salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price        });
        cart.isDeliveryRequired = true,
            cart.priceForDelivery = 20;
        //Act
        var res = classforTest.getTotalSumm(cart, undefined).totalSum;
        //Assert
        expect(res).toBe(120);
    });

    it("should correct return total summ when has products, has complects, has collection and isDeliveryRequired", () => {
        //Arrange
        var price = new MoneyImpl();
        price.amount = 100;
        var priceForAssembly = new MoneyImpl();
        priceForAssembly.amount = 10;

        var cart: Dto.CartFull.ICart = JSON.parse(JSON.stringify(mockData.cart));
        cart.products.push({
            id: "id", quantity: 1, price: price, salePrice: price, discount: 0, summ: 110, isAssemblyRequired: true, priceForAssembly: priceForAssembly, salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price        });
        cart.complects.push({
            id: "id", quantity: 1, price: price, salePrice: price, discount: 0, isAssemblyRequired: false, priceForAssembly: priceForAssembly, summ: 100, salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price        });
        cart.collections.push({
            id: "id",
            products: [{
                id: "id", quantity: 1, price: price, salePrice: price, discount: 10, isAssemblyRequired: true, priceForAssembly: priceForAssembly, summ: 100, salePriceInstallment6Month: price,
                salePriceInstallment10Month: price,
                salePriceInstallment12Month: price,
                salePriceInstallment18Month: price,
                salePriceInstallment24Month: price,
                salePriceInstallment36Month: price,
                salePriceInstallment48Month: price,
                priceInTotal: price,
                salePriceWithAssembly: price,
                salePriceInstallment6MonthWithAssembly: price,
                salePriceInstallment10MonthWithAssembly: price,
                salePriceInstallment12MonthWithAssembly: price,
                salePriceInstallment18MonthWithAssembly: price,
                salePriceInstallment24MonthWithAssembly: price,
                salePriceInstallment36MonthWithAssembly: price,
                salePriceInstallment48MonthWithAssembly: price            }]
        })
        cart.isDeliveryRequired = true,
            cart.priceForDelivery = 20;
        //Act
        var res = classforTest.getTotalSumm(cart, undefined).totalSum;
        //Assert
        expect(res).toBe(320);
    });
});