export const productItem = {
    quantity: 1,
    isAssemblyRequired: true,
    price: {
        amount: 10
    },
    priceForAssembly: {
        amount: 1
    },
    priceForDelivery: {
        amount: 15
    },
    discount: 10
}

export const resultDataCart = {
    products: [],
    complects: [],
    collections: [],
    id: "",
    priceForDelivery: {
        amount: 100
    },
    isDeliveryRequired: true,
    totalPrice: {
        amount: 200
    }
}

export const cart: Dto.CartFull.ICart = {
    id: "id",
    products: [],
    complects: [],
    collections: [],
    orderCount: 0,
    priceForDelivery: 0,
    totalPrice: 0,
    isDeliveryRequired: false,
    totalPriceWithoutDelivery: 0,
    saving: 0,
    totalAssembly: 0,
    installmentSum: 0
}