﻿import AddressPartController from "../../../../cart/cart-full/address-part/address-part.controller";

/* import mock data from mock module */
import * as mockData from "./address-part.mock";

describe("AddressPartController", () => {

    let classforTest: AddressPartController = new AddressPartController();

    it("should copy default address from profile to field", () => {
        //Arrange
        classforTest.profile = JSON.parse(JSON.stringify(mockData.profile));
        //Act
        classforTest.map();
        //Assert
        expect(classforTest.defaultAddress.id).toBe("111");
    });

    it("should push new empty default address to profile", () => {
        //Arrange
        classforTest.profile = JSON.parse(JSON.stringify(mockData.profile));
        classforTest.profile.userAddresses = [];
        //Act
        classforTest.map();
        //Assert
        expect(classforTest.defaultAddress.address).toBe("");
        expect(classforTest.defaultAddress.isDefault).toBe(true);
        expect(classforTest.profile.userAddresses.length).toBe(1);
        expect(classforTest.profile.userAddresses[0].address).toBe("");
    });
});

