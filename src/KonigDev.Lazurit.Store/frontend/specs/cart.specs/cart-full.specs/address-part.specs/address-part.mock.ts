﻿export const profile: any = {
    id: "123",
    userPhones: [
        {
            id: "111",
            phone: "00001",
            isDefault: true
        },
        {
            id: "222",
            phone: "00001",
            isDefault: false
        }
    ],
    userAddresses: [
        {
            id: "111",
            address: "adress1",
            isDefault: true,
            kladrCode: "111"
        },
        {
            id: "222",
            address: "adress2",
            isDefault: false,
            kladrCode: "222"
        }
    ]
}