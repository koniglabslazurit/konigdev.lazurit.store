﻿import CartFullController from "../../../cart/cart-full/cart-full.controller";
import MessageService from "../../../common/services/message.service";
import Constants from "../../../constants/constants";
import LayawayService from "../../../layaway/layaway-service/layaway-service.service";
import CartFullMapService from "../../../cart/cart-full/cart-full.service.map";
import CartFullService from "../../../cart/cart-full/cart-full.service";

import toastrMock from "../../services.mock/toastr.mock";

class MoneyImpl implements Dto.Money.IMoney {
    public internalAmount = 0;
    public amount = 0;
    public truncatedAmount = 0;
    public formattedAmount = "";
    public formattedAmountWithoutPoint = "";
    public formattedAmountWithoutPointAndCurrency = "";
    public decimalDigits = 0;
}

/* import mock data from mock module */
import * as mockData from "./cart-full.mock";

describe("CartFullController", () => {
    //var _q: ng.IQService;
    let windowServiceMock: any;
    let toastr: toastrMock;
    let classforTest: CartFullController;
    let cartFullServiceMock = new CartFullService();
    let cartFullMapService = new CartFullMapService(new CartFullService());
    let constants = new Constants();
    let messageService = new MessageService(toastr);
    let orderApiServiceMock: any = {
        create: (model: any) => {
            return {
                then: (callback) => {
                    return callback({})
                }
            }
        }
    };

    //todo это можно мокать используя spyOn, но не получилось с промисом разобраться.    
    let cartServiceMock: any = {
        getCart: () => {
            return {
                then: (callback) => {
                    return callback(mockData.resultDataCart);
                }
            }
        },
        getPaymentMethods: () => {
        return {
            then: (callback) => {
                return callback([]);
            }
        }
        },
        getRoboPaymentMethods: () => {
            return {
                then: (callback) => {
                    return callback([]);
                }
            }
        },
    };

    let profileService: any = {
        getProfile: () => {
            return {
                then: (callback) => {
                    return callback({});
                }
            }
        }
    };

    let layawayService: any = {
        getLayaway: () => {
            return {
                then: (callback: any) => {
                    return callback([]);
                }
            }
        }, moveToLayaway: (request: any) => {
            return {
                then: (callback: any) => {
                    return callback();
                }
            }
        }
    };

    let installmentService: ProductsInstallment.Service = {
        getInstallmentList: () => {
            return [{id: "24", name:"name"}];
        },
        mapFromFilter: (item: Dto.Product.InstallmentFilter): Dto.Product.Installment => {
            return <Dto.Product.Installment>{};
        }
    };
    classforTest = new CartFullController(cartServiceMock, cartFullServiceMock, constants, messageService, layawayService, cartFullMapService, profileService, orderApiServiceMock, installmentService, windowServiceMock);

    // beforeEach(() => {
    //     spyOn(cartServiceMock, "getCart").and.callFake($q.when({}))
    // });

    it("should move to Layaway product with correct update data", () => {
        //Arrange
        var price = new MoneyImpl();
        price.amount = 100;
        var priceForAssembly = new MoneyImpl();
        classforTest.cart = JSON.parse(JSON.stringify(mockData.cart));
        classforTest.cart.orderCount = 2;
        classforTest.cart.products.push({
            id: "1", quantity: 1, price: price, salePrice: price, discount: 0, summ: 110, isAssemblyRequired: true, priceForAssembly: priceForAssembly, salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price
        });
        classforTest.cart.products.push({
            id: "2", quantity: 1, price: price, salePrice: price, discount: 0, summ: 110, isAssemblyRequired: true, priceForAssembly: priceForAssembly, salePriceInstallment6Month: price,
            salePriceInstallment10Month: price,
            salePriceInstallment12Month: price,
            salePriceInstallment18Month: price,
            salePriceInstallment24Month: price,
            salePriceInstallment36Month: price,
            salePriceInstallment48Month: price,
            priceInTotal: price,
            salePriceWithAssembly: price,
            salePriceInstallment6MonthWithAssembly: price,
            salePriceInstallment10MonthWithAssembly: price,
            salePriceInstallment12MonthWithAssembly: price,
            salePriceInstallment18MonthWithAssembly: price,
            salePriceInstallment24MonthWithAssembly: price,
            salePriceInstallment36MonthWithAssembly: price,
            salePriceInstallment48MonthWithAssembly: price
        });
        //Act
        classforTest.moveProductToLayaway("1");
        var deletedItem = classforTest.cart.products.filter((item: Dto.CartFull.IProductItem) => { return item.id === "1"; })
        var item = classforTest.cart.products.filter((item: Dto.CartFull.IProductItem) => { return item.id === "2"; })
        //Assert
        expect(classforTest.cart.products.length).toBe(1);
        expect(item).not.toBeNull;
        expect(deletedItem).toBeNull;
        expect(classforTest.layawayItemsCount).toBe(1);
        expect(classforTest.cart.orderCount).toBe(1);
    });

    //it("should get correct summ product", () => {
    //    //Arrange        
    //    var item = JSON.parse(JSON.stringify(mockData.productItem));
    //    //Act
    //    //Assert
    //    expect(classforTest.getSummProduct(item)).toBe(10);
    //});

    //it("should get correct summ product with quantity 2", () => {
    //    //Arrange
    //    var item = JSON.parse(JSON.stringify(mockData.productItem));
    //    item.quantity = 2;
    //    //Act
    //    //Assert
    //    expect(classforTest.getSummProduct(item)).toBe(20);
    //});

    //it("should get correct summ product when assembly is not required", () => {
    //    //Arrange
    //    var item = JSON.parse(JSON.stringify(mockData.productItem));
    //    item.isAssemblyRequired = false;
    //    //Act
    //    //Assert
    //    expect(classforTest.getSummProduct(item)).toBe(9);
    //});

    /* Пример игнора теста */
    // xit("should true is true", () => {
    //     expect(true).toBe(true);
    // });
});