﻿export const productMock: Dto.Product.IProduct = {
    id: "12312312312312",
    article: "q123",
    articleId: "123123",
    collection: "1231",
    seriesName: "Orneta",
    furnitureType: "type",
    syncCode1C: "123wer",
    width: 50,
    length: 100,
    height: 200
};

export const cartMock = {
    products: [productMock, productMock],
    complects: [productMock, productMock],
    collections: [productMock, productMock],
    id: "123"
};