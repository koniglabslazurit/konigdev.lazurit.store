﻿export const layawayItem = {

}

export const resultDataLayaway = {
    layaways: [{
            products: [{
                productId: "product",
                quantity: 1
            }],
            complects: [{
                id: "complect",
                quantity: 1
            }],
            collections: [{
                id: "collection",
                quantity: 1,
                productsInCollection: [{
                    productId: "productInCollection",
                    quantity: 1
                }]
            }],
            id: "layaway",
            userId: "",
            isWholeCart: false
        }]
}