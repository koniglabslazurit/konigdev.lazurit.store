﻿import LayawayController from "../../layaway/layaway.controller";

/* import mock data from mock module */
import * as mockData from "./layaway.mock";

describe("LayawayController", () => {

    let classforTest: LayawayController;

    let layawayServiceMock: any = {
        getLayaway: () => {
            return {
                then: (callback) => {
                    return callback(mockData.resultDataLayaway.layaways);
                }
            }
        }
    };
    // classforTest.layaways = JSON.parse(JSON.stringify(mockData.resultDataLayaway.layaways));
    classforTest = new LayawayController(layawayServiceMock);

    it("should return correct layaway by layawayId", () => {
        //Arrange
        var layawayid = "layaway";
        //Act
        var res = classforTest.getCurrentLayaway(layawayid);
        //Assert
        expect(res.id).toBe(layawayid);
    });

    it("should return correct collection by collectionId", () => {
        //Arrange
        var layawayid = "layaway";
        var id = "collection";
        //Act
        var res = classforTest.getCurrentCollection(id, layawayid);
        //Assert
        expect(res.id).toBe(id);
    });

    it("should return correct complect by complectId", () => {
        //Arrange
        var layawayid = "layaway";
        var id = "complect";
        //Act
        var res = classforTest.getCurrentComplect(id, layawayid);
        //Assert
        expect(res.id).toBe(id);
    });

    it("should return correct product by pruductId", () => {
        //Arrange
        var layawayid = "layaway";
        var productId = "product";
        //Act
        var res = classforTest.getCurrentProduct(productId, layawayid);
        //Assert
        expect(res.productId).toBe(productId);
    });

    it("should return correct productInCollection by pruductId", () => {
        //Arrange
        var layawayid = "layaway";
        var id = "collection";
        var productId = "productInCollection";
        //Act
        var res = classforTest.getCurrentProductInCollection(productId, id, layawayid);
        //Assert
        expect(res.productId).toBe(productId);
    });
});