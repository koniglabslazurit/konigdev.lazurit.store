﻿import ProfileEditController from "../../../profile/profile-edit/profile-edit.controller";
import ProfileEditJQSrvice from "../../../profile/profile-edit/profile-edit.service.jq";
import MessageService from "../../../common/services/message.service";
import Constants from "../../../constants/constants";
import * as angular from "angular";

import toastrMock from "../../services.mock/toastr.mock";


/* import mock data from mock module */
import * as mockData from "./profile-edit.mock";

describe("ProfileEditController", () => {
    let toastr: toastrMock;
    let classforTest: ProfileEditController;
    let messageService = new MessageService(toastr);
    let constants = new Constants();
    let windowsService: angular.IWindowService;
    let accountApiServiceMock: any = {
        userExist: () => {
            return {
                then: (callback) => {
                    return callback();
                }
            }
        }
    }

    let profileServiceMock: any = {
        updateProfile: () => {
            return {
                then: (callback) => {
                    return callback();
                }
            }
        }
    };

    classforTest = new ProfileEditController(profileServiceMock, accountApiServiceMock, ProfileEditJQSrvice, windowsService, messageService, constants);

    it("should be added one address if their amount less then 10", () => {
        //Arrange
        classforTest.kdModel = {
            id : "",
            firstName: "",
            lastName: "",
            email: "",
            unchangedMail: "",
            userAddresses: [
                { address: 'a' }, { address: 'b' }, { address: 'c' }, { address: 'd' }
            ],
            userPhones: [
                { phone: '77' }, { phone: '77' }
            ]
        };
        //Act
        classforTest.addAddress();
        //Assert
        expect(classforTest.kdModel.userAddresses.length).toBe(5);
    });

    it("should not be added one address if their amount less then 10, but last address is empty", () => {
        //Arrange
        classforTest.kdModel = {
            id: "",
            firstName: "",
            lastName: "",
            email: "",
            unchangedMail: "",
            userAddresses: [
                { address: '' }, { address: '' }, { address: '' }, { address: '' }
            ],
            userPhones: [
                { phone: '77' }, { phone: '77' }
            ]
        };
        //Act
        classforTest.addAddress();
        //Assert
        expect(classforTest.kdModel.userAddresses.length).toBe(4);
    });

    it("should not be added one address if their amount more then 10", () => {
        //Arrange
        classforTest.kdModel = {
            id: "",
            firstName: "",
            lastName: "",
            email: "",
            unchangedMail: "",
            userAddresses: [
                { address: 'a' }, { address: 'b' }, { address: 'c' }, { address: 'd' }, { address: 'e' },
                { address: 'f' }, { address: 'g' }, { address: 'h' }, { address: 'i' }, { address: 'j' }
            ],
            userPhones: [
                { phone: '77' }, { phone: '77' }
            ]
        };
        //Act
        classforTest.addAddress();
        //Assert
        expect(classforTest.kdModel.userAddresses.length).toBe(10);
    });

    it("should be added one phone if their amount less then 5", () => {
        //Arrange
        classforTest.kdModel = {
            id: "",
            firstName: "",
            lastName: "",
            email: "",
            unchangedMail: "",
            userAddresses: [
                { address: 'a' }, { address: 'b' }, { address: 'c' }, { address: 'd' }
            ],
            userPhones: [
                { phone: '77' }, { phone: '77' }
            ]
        };
        //Act
        classforTest.addPhone();
        //Assert
        expect(classforTest.kdModel.userPhones.length).toBe(3);
    });

    it("should not be added one phone if their amount less then 5, but last phone is empty", () => {
        //Arrange
        classforTest.kdModel = {
            id: "",
            firstName: "",
            lastName: "",
            email: "",
            unchangedMail: "",
            userAddresses: [
                { address: 'a' }, { address: 'b' }, { address: 'c' }, { address: 'd' }
            ],
            userPhones: [
                { phone: '77' }, { phone: '77' }, { phone: '' }
            ]
        };
        //Act
        classforTest.addPhone();
        //Assert
        expect(classforTest.kdModel.userPhones.length).toBe(3);
    });

    it("should not be added one phone if their amount more then 5", () => {
        //Arrange
        classforTest.kdModel = {
            id: "",
            firstName: "",
            lastName: "",
            email: "",
            unchangedMail: "",
            userAddresses: [
                { address: 'a' }, { address: 'b' }, { address: 'c' }, { address: 'd' }
            ],
            userPhones: [
                { phone: '77' }, { phone: '77' }, { phone: '77' }, { phone: '77' }, { phone: '77' }
            ]
        };
        //Act
        classforTest.addPhone();
        //Assert
        expect(classforTest.kdModel.userPhones.length).toBe(5);
    });
});