export default class ToastrMock implements Toastr {
    public clear() { };
    public remove() { };
    public error: ToastrDisplayMethod;
    public info: ToastrDisplayMethod;
    public options: ToastrOptions;
    public success: ToastrDisplayMethod;
    public warning: ToastrDisplayMethod;
    public version: string;
    public getContainer: {
        (options?: ToastrOptions): JQuery,
        (options: ToastrOptions, create: boolean): JQuery,
    };                                       
    public subscribe: (callback: (response: any) => any) => void;

}