﻿//import carticonSingeltonService from "../../cart/cart-hub/cart-icon-hub.service";
//import eventSevice from "../../common/services/event.service";
//import cookieSevice from "../../common/services/cookie.service";
//import signalrSevice from "../../signalr/signalr.service";
//import HeaderToolsController from "../../header-tools/header-tools.controller";

///* import mock data from mock module */
//import * as mockData from "./header-tools.mock";

//describe("HeaderToolsController", () => {
//    let cartSingeltonSeviceMock: any = {
//        singeltonGetCart: () => {
//            return null;
//        }
//    }
//    let eventSeviceMock: any = {
//        triggerEvent: (event: any) => {
//            return {
//                then: (callback: any) => {
//                    return callback();
//                }
//            }
//        },
//        suscribeOnEvent: (request: any, callback: any) => {
//            return {
//                then: (callback: any) => {
//                    return callback();
//                }
//            }
//        }
//    }
//    let cookieServiceMock: any = {
//        getCookie: () => {
//            return {
//                then: (callback: any) => {
//                    return callback();
//                }
//            }
//        }
//    }               

//    let signalrServiceMock: any = {
//        start: () => { return null; }
//    }

//    let classforTest: HeaderToolsController = new HeaderToolsController(cartSingeltonSeviceMock, eventSeviceMock, cookieServiceMock, signalrServiceMock);

//    it("shouldn't have products in cart after initializing", () => {
//        //Arrange
//        //Act
//        //Assert
//        expect(classforTest.cart.id).toBe("123");
//        expect(classforTest.cart.products.length).toBe(2);
//        expect(classforTest.cart.collections.length).toBe(2);
//        expect(classforTest.cart.complects.length).toBe(2);
//        expect(classforTest.cart.itemsCount).toBe(6);
//    });
//});

