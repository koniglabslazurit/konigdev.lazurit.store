﻿//import * as angular from "angular";
import ProductsInstallmentController from "../../../../product/product-installment/products-installment-page/products-installment-page.controller";
import MessageService from "../../../../common/services/message.service";

import toastrMock from "../../../services.mock/toastr.mock";

/* import mock data from mock module */
import * as mockData from "./product-installment-page.mock";

describe("ProductsInstallmentApiService", () => {
    let classforTest: ProductsInstallmentController;
    let toastr: toastrMock;
    let productInstallmentServiceMock: ProductsInstallment.Service;
    let productInstallmentApiServiceMock: any = {
        getInstallment: () => {
            return {
                then: (callback) => {
                    return callback({ data: { items: [], totalCount: 0 } });
                }
            }
        }
    };
    let messageService = new MessageService(toastr);

    classforTest = new ProductsInstallmentController(productInstallmentServiceMock, productInstallmentApiServiceMock, messageService);

    it("should be found installment with such period", () => {
        //Arrange
        classforTest.sets = [mockData.InstallmentWithOneFurnitureType, mockData.NewInstallmentWithOneFurnitureTypeStartDuringThePeriod];

        //Act
        console.log(classforTest.sets);
        var res = classforTest.isSuchInstallmentExist();
        console.log(res);
        //Assert
        expect(res).toBe(true);
    });

    it("should be found installment with such period", () => {
        //Arrange
        classforTest.sets = [mockData.InstallmentWithOneFurnitureType, mockData.NewInstallmentWithOneFurnitureTypeEndDuringThePeriod];

        //Act
        console.log(classforTest.sets);
        var res = classforTest.isSuchInstallmentExist();
        console.log(res);
        //Assert
        expect(res).toBe(true);
    });

    it("should be found installment with such period", () => {
        //Arrange
        classforTest.sets = [mockData.InstallmentWithOneVendorCodeAndOneFurnitureType, mockData.NewInstallmentWithOneFurnitureTypeEndDuringThePeriod];

        //Act
        console.log(classforTest.sets);
        var res = classforTest.isSuchInstallmentExist();
        console.log(res);
        //Assert
        expect(res).toBe(true);
    });

    it("should not be found installment with such period", () => {
        //Arrange
        classforTest.sets = [mockData.InstallmentWithOneVendorCode, mockData.NewInstallmentWithOneFurnitureTypeEndDuringThePeriod];

        //Act
        console.log(classforTest.sets);
        var res = classforTest.isSuchInstallmentExist();
        console.log(res);
        //Assert
        expect(res).toBe(true);
    });

    it("should not be found installment with such period", () => {
        //Arrange
        classforTest.sets = [mockData.InstallmentWithOneVendorCodeAndOneFurnitureType, mockData.NewInstallmentWithOneFurnitureAndOtherVendorCodeTypeEndDuringThePeriod];

        //Act
        console.log(classforTest.sets);
        var res = classforTest.isSuchInstallmentExist();
        console.log(res);
        //Assert
        expect(res).toBe(false);
    });
})