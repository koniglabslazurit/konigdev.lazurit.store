import { IHelloService } from "./hello-service.interface";
import { HelloComponent } from "./hello.component";
import * as angular from "angular";

class MockHelloService implements IHelloService {

    public sayHello(): string {
        return "Hello world!";
    }
}

describe("HelloComponent", () => {

    it("should say 'Hello world!'", () => {

        let mockHelloService = new MockHelloService();
        let helloComponent = new HelloComponent(mockHelloService);

        expect(helloComponent.sayHello()).toEqual("Hello world!");
    });
});
