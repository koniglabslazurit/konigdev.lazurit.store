﻿export default class GeolocationController {

    private isShowCityPopup: boolean; 
    private isShowCitiesPopup: boolean; 
    public cities: any;
    public curCity: any;
    private coords: any;

    constructor(public CommonService: any, private GeolocationService: any) {
        this.init();
        this.CommonService.suscribeOnEvent("showAllCities", function () {
            this.isShowCitiesPopup = true;
        }.bind(this));
    }

    private init(): void {
        this.isShowCitiesPopup = false;                               
        this.getCities();        
    };
        
    public getCities(): void {
        var me = this;
        this.GeolocationService.getNearbyCities()
            .then((response) => {
                me.cities = response;
                me.curCity = me.cities.filter((c) => { return c.IsNearest === true })[0];
                var firstTime = me.GeolocationService.getCookie("firstTime");
                if (firstTime === "True") {
                    me.isShowCityPopup = true;       
                    this.CommonService.triggerEvent("DontShowPopup"); 
                }
                else {
                    me.isShowCityPopup = false;
                }
            });
    };   
                                                                                        
    public asseptCity(cityName:string): void {
        this.isShowCityPopup = false;
        this.CommonService.triggerEvent("writeCityInHeader"); 
    };
    public rejectCity(): void {
        this.isShowCityPopup = false;
        this.isShowCitiesPopup = true;
    };
    public selectCity(cityId: string, cityTitle:string, zoneId:string, delivery:string): void {
        this.isShowCitiesPopup = false;        
        document.cookie = "cityId=" + cityId + "; path=/;";
        document.cookie = "cityName=" + cityTitle + "; path=/;";
        document.cookie = "zoneId=" + zoneId + "; path=/;";
        document.cookie = "delivery=" + delivery + "; path=/;";
        this.CommonService.triggerEvent("writeCityInHeader");
        window.location.reload();
    };
    public close(): void {
        this.CommonService.triggerEvent("writeCityInHeader");
        this.isShowCitiesPopup = false;                                        
    }                               
};