﻿export default class GeolocationService {

    private pushCoordsUrl: string;
    public isLoading: boolean;
    private cities: any[];
    private cityIdCookieName: string;

    constructor(private $http: angular.IHttpService, private $q: angular.IQService) {
        this.pushCoordsUrl = "/geolocation/city";
        this.isLoading = false;
        this.cityIdCookieName = "cityId";

    }

    public getCookie(name: any): any {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    };

    public pushCoords(coords: any): angular.IPromise<any> {
        return this.$http
            .post(this.pushCoordsUrl, coords)
            .then((response: any) => {
                this.isLoading = false;
                return response.data;
            });
    }

    public getNearbyCities(): angular.IPromise<any> {
        let citiesDefered = this.$q.defer();              
        this.getBrowserLocation()
            .then((result: any) => {
                console.log("Coordinates: lantitude " + result.Latitude + ", longitude " + result.Longitude);
                return this.pushCoords(result);
            })
            .then((result: any) => {
                citiesDefered.resolve(result);
            })
            .catch((err: any) => {
                console.error(err);
            });
        return citiesDefered.promise;
    }

    private getBrowserLocation() {
        let coordsDefered = this.$q.defer();
        let options = {
            enableHighAccuracy: true,
            timeout: 30000,
            maximumAge: 75000
        };
        let defaultCoords = { Latitude: 0, Longitude: 0 };
        if (!navigator.geolocation) {
            coordsDefered.reject("geolocation is not support");
            return coordsDefered.promise;        
        }
        navigator.geolocation.getCurrentPosition(
            (position: any) => {
                let coords = { Latitude: position.coords.latitude, Longitude: position.coords.longitude };
                coordsDefered.resolve(coords);
            },
            (err: any) => {
                //todo apply correct error handler
                console.error(err);
                coordsDefered.resolve(defaultCoords);
            },
            options);
        return coordsDefered.promise;
    }
                                              

}