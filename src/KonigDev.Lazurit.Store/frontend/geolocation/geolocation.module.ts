﻿import * as angular from "angular";

import * as commonModule from "../common/common.module";

import geolocationController from "./geolocation.controller";
import geolocationService from "./geolocation.service";

/* constants */
export const name = "kd.geolocation";
const geolocationControllerName = "geolocationController";
export const geolocationServiceName = "geolocationService";
const geolocationComponentName = "kdGeolocation";



angular
    .module(name,
    [
        commonModule.name
    ])
    .controller(geolocationControllerName,
    [
        commonModule.eventServiceName,
        geolocationServiceName,
        geolocationController
    ])
    .service(geolocationServiceName,
    [
        "$http",
        "$q",
        geolocationService
    ])
    .component(geolocationComponentName, {
        bindings: {},
        controller: geolocationControllerName,
        template: require("./geolocation.view.html")
    });