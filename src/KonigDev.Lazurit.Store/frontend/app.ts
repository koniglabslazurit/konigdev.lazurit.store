﻿import * as angular from "angular";

import * as favoriteProfileModule from "./favorite/favorite-profile/favorite-profile.module";
import * as favoriteTriggerModule from "./favorite/favorite-trigger/favorite-trigger.module";
import * as cartFullModule from "./cart/cart-full/cart-full.module";
import * as cartMiniModule from "./cart/cart-mini/cart-mini.module";
import * as furnitureModule from "./furniture-type/furniture-type.module";
import * as collectionModule from "./collection/collection.module";
import * as productModule from "./product/product.module";
import * as productTileModule from "./product/product-tile/product-tile.module";
import * as headerModule from "./header-tools/header-tools.module";
import * as roomModule from "./room/room.module";
import * as imageModule from "./image/image.module";
import * as imageManagerAreasModule from "./image/image-manager-areas/image-manager-areas.module";
import * as slider from "./common/directive/slider/slider.module";
import * as kladr from "./common/directive/kladr/kladr.module";
import * as profileModule from "./profile/profile.module";
import * as layawayModule from "./layaway/layaway.module";
import * as authModule from "./auth/auth.module";
import * as filterModule from "./filter/filter-multi-select/filter-multi-select.module";
import * as productPageModule from "./product/product-page/product-page.module";
import * as ordersToolModule from "./administrative-tools/orders-tool/orders-tool.module";
import * as roleToolModule from "./administrative-tools/roles-tool/role-tool.module";
import * as verifySellersModule from "./administrative-tools/roles-tool/verify-sellers/verify-sellers.module";
import * as roleControlModule from "./administrative-tools/roles-tool/role-control/role-control.module";
import * as administrativeRolesModule from "./administrative-tools/roles-tool/role-control/administrative-roles/administrative-roles.module";
import * as allocateeRolesModule from "./administrative-tools/roles-tool/role-control/allocate-role/allocate-role.module";
import * as sellerRolesModule from "./administrative-tools/roles-tool/role-control/seller-role/seller-role.module";
import * as importCollectionModule from "./import/collections-variants/collections-variants.module";
import * as collectionsImportModule from "./import/collections-import/collections-import.module";
import * as productsValidationsImportModule from "./import/products-validations-import/products-validations-import.module";
import * as productsTemplatesImportModule from "./import/products-templates-import/products-templates-import.module";
import * as productsPublishedImportModule from "./import/products-published-import/products-published-import.module";
import * as selectSingleModule from "./common/components/select-single/select-single.module";
import * as geolocationModule from "./geolocation/geolocation.module";
import * as signalrModule from "./signalr/signalr.module";
import * as cartHubModule from "./cart/cart-hub/cart-hub.module";
import * as cartIconModule from "./cart/cart-icon/cart-icon.module";
import * as customerRequestCreateModule from "./customer-request/create/customer-request.create.module";
import * as customerRequestListModule from "./customer-request/list/customer-requests.module";
import * as styleModule from "./style/style.module";
import * as targetAudienceModule from "./target-audience/target-audience.module";
import * as propertyModule from "./property/property.module";
import * as productsImagesImportModule from "./import/products-images-import/products-images-import.module";
import * as productsListModule from "./product/product-list/product-list.module";
import * as marketingActionsListModule from "./administrative-tools/marketing-actions/components/actions-list/actions-list.module";
import * as orderThanksModule from "./order/order-thanks/order-thanks.module";
import * as filterColorImportModule from "./import/filters-colors/filters-colors.module";

/* !!! Прежде чем подключать модуль тут, убедись, что его необходимо подключать глобально
*  что его нельзя подключить как зависимость в необходимом модуле. Не надо тут создавать нечитабельную портянку модулей */
import * as productsInstallmentModule from "./product/product-installment/products-installment-page/products-installment-page.module";

angular
    .module('KonigDevApp', [
        'mrImage',   
        'toastr',
        'ui.bootstrap',
        'ui.router',    
        'localytics.directives',
        'infinite-scroll',
        'ct.ui.router.extras',
        'angular-click-outside',
        /* app modules */
        slider.name,
        kladr.name,
        furnitureModule.name,
        favoriteProfileModule.name,
        favoriteTriggerModule.name,
        cartFullModule.name,
        cartMiniModule.name,
        collectionModule.name,
        productModule.name,
        productTileModule.name,
        headerModule.name,
        roomModule.name,
        imageModule.name,
        imageManagerAreasModule.name,
        authModule.name,
        profileModule.name,
        layawayModule.name,
        filterModule.name,
        selectSingleModule.name,
        productPageModule.name,    
        geolocationModule.name,
        ordersToolModule.name,
        verifySellersModule.name,
        roleToolModule.name,
        roleControlModule.name,
        administrativeRolesModule.name,
        allocateeRolesModule.name,
        sellerRolesModule.name,
        importCollectionModule.name,
        collectionsImportModule.name,
        signalrModule.name,
        cartHubModule.name,
        cartIconModule.name,
        customerRequestCreateModule.name,
        customerRequestListModule.name,
        productsValidationsImportModule.name,
        productsTemplatesImportModule.name,
        productsPublishedImportModule.name,
        styleModule.name,
        targetAudienceModule.name,
        propertyModule.name,
        productsImagesImportModule.name,
        productsInstallmentModule.name,
        productsListModule.name,
        marketingActionsListModule.name,
        orderThanksModule.name,
        filterColorImportModule.name
    ]);
