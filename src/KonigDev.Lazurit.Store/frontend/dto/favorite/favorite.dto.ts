﻿namespace Dto.Favorite {
    export interface IFavoriteContent {
        id: string;
        products: Array<Dto.Favorite.IFavoriteProduct>;
        complects: Array<Dto.Favorite.IFavoriteComplect>;
        collections: Array<Dto.Favorite.IFavoriteCollection>;
    }

}

namespace Dto.Favorite {
    export interface IFavoriteProduct {
        id: string; 
        product: Dto.Favorite.IProduct;
    }
}

namespace Dto.Favorite {
    export interface IFavoriteComplect {
        id: string;
        collectionVariant: Dto.Favorite.ICollectionVariant;
    }
}

namespace Dto.Favorite {
    export interface ICollectionVariant {
        id: string;
        collectionName: string;
        collectionAlias: string;
        roomTypeName: string;
        roomTypeAlias: string;
        products: Array<Dto.Favorite.IProduct>;
    }
}

namespace Dto.Favorite {
    export interface IFavoriteCollection {
        id: string;
        products: Array<Dto.Favorite.IFavoriteProduct>;
        //but without products
        collectionVariant: Dto.Favorite.ICollectionVariant;
        //additional for frontend
        salePrice: number;
        price: number;
    }
}

namespace Dto.Favorite {
    export interface IProduct {
        id: string;
        article: string;
        width: number;
        length: number;
        height: number;
        facingColorName: string;
        facingName: string;
        frameColorIdName: string;
        furnitureType: string;
        furnitureTypeAlias: string;
        syncCode1C: string;
        collectionAlias: string;
        collectionName: string;  
        //prices
        salePrice: Dto.Money.IMoney;
        price: Dto.Money.IMoney;
        discount: number;
        priceForAssembly: Dto.Money.IMoney;
    }
}