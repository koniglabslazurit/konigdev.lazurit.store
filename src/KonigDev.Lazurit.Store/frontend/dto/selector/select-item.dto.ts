﻿namespace Dto.Select.Item {
    export interface ISelectItem {
        id: string;
        name: string;
        imageUrl: string;
    }
}