namespace Dto.VendorCode {
    export interface IVendorCodeItem {
        id: string
        syncCode1C: string;
        width: number;
        length: number;
        height: number;
        furnitureType: string;
    }
}