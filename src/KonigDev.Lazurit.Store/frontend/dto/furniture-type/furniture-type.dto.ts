﻿namespace Dto.FurnitureType {
    export interface DtoFurnitureTypeItem {
        id: string;
        syncCode1C?: string;
        alias?: string;
        name: string;
    }
    export interface GetFurnitureTypesQuery {
        integrationCollectionId: string
    }
}
