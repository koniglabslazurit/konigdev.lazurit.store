﻿namespace Dto.Style {
    export interface DtoStyleItem {
        id: string;
        name: string;
        syncCode1C?: string;
    }
}