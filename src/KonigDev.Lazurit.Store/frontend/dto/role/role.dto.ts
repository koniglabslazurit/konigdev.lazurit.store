﻿namespace Dto.Role {
    export interface IUserRole {
        checked: boolean;
        name: string;
        email: string;
        role: string;
        userId: string;
    };

    export interface IRole {
        name: string;
        id: string;
    };
    export interface IUpdateRole {
        userId: string, 
        role: string,
    };
};