﻿namespace Dto.Complect {
    export interface IComplect {
        id: string;
        name: string;
        priceForAssembly: Dto.Money.IMoney;
        price: Dto.Money.IMoney;
    }
}