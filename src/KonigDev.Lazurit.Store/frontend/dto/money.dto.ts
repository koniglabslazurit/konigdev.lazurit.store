﻿namespace Dto.Money {
    export interface IMoney {
        internalAmount: number;
        amount: number;
        truncatedAmount: number;
        formattedAmount: string;
        formattedAmountWithoutPoint: string;
        formattedAmountWithoutPointAndCurrency: string;
        decimalDigits: number;
    }
}