﻿namespace Dto.TargetAudience {
    export interface DtoTargetAudienceItem {
        id: string;
        name: string;
        syncCode1C?: string;
    }
}