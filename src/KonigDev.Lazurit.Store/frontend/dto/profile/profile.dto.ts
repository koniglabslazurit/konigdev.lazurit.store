﻿namespace Dto.Profile {
    export interface IProfileItem {
        id: string,
        userAddresses: Array<IUserAddressItem>,
        userPhones: Array<IUserPhoneItem>,
        email: string,
        unchangedMail: string,
        firstName: string,
        middleName?: string,
        lastName: string
    }
    export interface IUserAddressItem {
        id?: string,
        kladrCode?: string,
        address: string,
        isDefault?: boolean,
        city?: string,
        street?: string,
        block?: string,
        building?: string,
        house?: string,
        entranceHall?: string,
        floor?: string,
        apartment?: string,
        isMarkedDeleted?: boolean
    }
    export interface IUserPhoneItem {
        id?: string,
        phone: string,
        isDefault?: boolean,
        isMarkedDeleted?: boolean
    }
}