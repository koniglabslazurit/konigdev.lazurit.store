﻿namespace Dto.Order.PaymentMethod {
    export interface IPaymentMethod {
        id: string,
        name: string,
        techName: string,
        sortNumber: number,
        class: string;
    }
}