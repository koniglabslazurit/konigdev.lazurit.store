﻿namespace Dto.Order.ApprovalSign {
    export interface IApprovalSign {
        isApprovalRequired: boolean
    }
}