﻿namespace Dto.Order {
    export interface VmOrderCreating {
        paymentMethodId: any;
        deliveryKladrCode: string;
        deliveryAddress: string;
        isRequiredSeller: boolean;
    }
}