﻿namespace Dto.Order {
    export interface VmOrderResponse {
        id: any;
        sum: number;
        number: number;
        isPaid: boolean;
        paymentMethod: string;
        roboKassaUrl: string;
    }
}