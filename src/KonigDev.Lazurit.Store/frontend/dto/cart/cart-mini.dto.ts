﻿namespace Dto.CartMini {
    export interface ICartMiniContent {
        id: string;
        products: Array<Dto.CartMini.ICartMiniProduct>;
        complects: Array<Dto.CartMini.ICartMiniComplect>;
        collections: Array<Dto.CartMini.ICartMiniCollection>;
        totalPrice: Dto.Money.IMoney;
        priceForDelivery: Dto.Money.IMoney;
    }

}


namespace Dto.CartMini {
    export interface ICartMiniProduct {
        id: string;
        itemId: string;
        article: string;
        collectionAlias: string;
        discount: number;
        furnitureType: string;
        isAssemblyRequired: boolean;
        quantity: number;
        roomTypeName: string;
        seriesName: string;
        syncCode1C: string;
        price: Dto.Money.IMoney;
        priceForAssembly: Dto.Money.IMoney;
        salePrice: Dto.Money.IMoney;
        salePriceWithAssembly: Dto.Money.IMoney;
        salePriceWithOutAssembly: Dto.Money.IMoney;
        salePriceInstallment24MonthWithAssembly: Dto.Money.IMoney;
        furnitureTypeAlias: string;
        articleSyncCode1C: string;

    }
}

namespace Dto.CartMini {
    export interface ICartMiniComplect {
        id: string;
        complect: Dto.Complect.IComplect;
        quantity: number;
        salePrice: Dto.Money.IMoney;
        salePriceInstallment24MonthWithAssembly: Dto.Money.IMoney;
        price: Dto.Money.IMoney;
        priceForAssembly: Dto.Money.IMoney;
    }
}

namespace Dto.CartMini {
    export interface ICartMiniCollection {
        id: string;
        quantity: number;
        collectionVariantId: string;
        collectionSalePrice: Dto.Money.IMoney;
        products: Array<Dto.CartMini.ICartMiniProduct>;
    }
}

namespace Dto.CartMini {
    export interface ICartIcon {
        id: string;
        count: number;
    }
}