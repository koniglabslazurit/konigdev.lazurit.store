﻿namespace Dto.Property {
    export interface IPropertyItem {
        id: string;
        name: string;
        title: string;
        groupeId: string;
    }

    export interface IPropertyMulti {
        title: string;
        groupeId: string;
        items: Array<Dto.Property.IPropertyItem>
    }   

    export interface IPropertyModel {
        propertiesMulti: Array<Dto.Property.IPropertyMulti>;
        propertiesSingle: Array<Dto.Property.IPropertyItem>;
    }
}