﻿namespace Dto.SyncCode {
    export interface DtoSyncCode1CItem {
        name: string;
        id: string;
    }
    export interface GetSyncCode1CsQuery {
        integrationCollectionId: string
    }
}