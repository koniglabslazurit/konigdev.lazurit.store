﻿namespace Dto.Filter {
    export interface IDtoFiltersResult {
        facings: Array<IColorFilter>;
        facingColors: Array<IColorFilter>;
        frameColors: Array<IColorFilter>;
    }
    export interface IProductFilterFrameColor {
        id: string;
        name: string;
        imageUrl: string;
        aliavableFacings: Array<IProductFilterFacing>;
    }

    export interface IProductFilterFacing {
        id: string;
        name: string;
        imageUrl: string;
        aliavableFacingColors: Array<IProductFilterFacingColor>
    }

    export interface IProductFilterFacingColor {
        id: string;
        name: string;
        imageUrl: string;
    }

    export interface IDtoFilterItem {
        facingColors: Array<IDtoFacingColorItem>,
        facings: Array<IDtoFacingItem>,
        frameColors: Array<IDtoFrameColorItem>
    }

    export interface IDtoFacingItem {
        id: string;
        name: string;
        syncCode1C?: string;
    }

    export interface IDtoFacingColorItem {
        id: string;
        name: string;
        syncCode1C?: string;
    }

    export interface IDtoFrameColorItem {
        id: string;
        name: string;
        syncCode1C?: string;
    }

    export interface DtoFilterColorContentItem {
        id: string;
        name: string;
        syncCode1C: string;
        imageUrl: string;
    }

    /**
    * Интерфейс для всех фильтров цветов/фасадов/отделок. Цвет фасада/корпуса, отделка, цвет отделки
    */
    export interface IColorFilter {
        id: string;
        name: string;
    }

    /*интерфейс для габариты*/  
    export interface IDimentionFilter {
        id: string;
        name: string;     
    }

    /*интерфейс для рассрочки*/
    export interface IInstallmentFilter {
        id: string;
        name: string;
    }
}