﻿namespace Dto.RooomType {
    export interface IRoomByProductItem {
        id: string,
        name: string,
        alias: string
    }
}