﻿namespace Dto.Delivery {
    export interface IDelivery {
        id: string;
        name: string;
    }
}