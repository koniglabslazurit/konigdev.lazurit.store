﻿namespace Dto.Product {
    export interface IProduct {
        id: string;
        article: string;
        articleId: string;
        collection: string;
        seriesName: string;
        furnitureType: string;
        syncCode1C: string;
        width: number;
        length: number;
        height: number;
    }
    export interface IProductPrice {
        id: string;
        SyncCode1C: string;
        SeriesName: string;
        SeriesAlias: string;
        ArticleName: string;
        ArticleSyncCode1c: string;
        FurnitureTypeAlias: string;
        FurnitureTypeName: string;
        FurnitureTypeId: string;
        IsFavorite: boolean;
        Height: number;
        Width: number;
        Length: number;
        Discount: number;
        IsAssemblyRequired: boolean;
        ItemId: string;
        Price: number;
        PriceForAssembly: number;
        SalePrice: number;
        SalePriceWithAssembly: number;
        SalePriceWithOutAssembly: number;
    }

    export interface ICurrentProduct {
        id: string;
        type: string;
        number: number;
        openType: string;
    }
}
