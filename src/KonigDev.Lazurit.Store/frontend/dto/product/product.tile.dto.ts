﻿namespace Dto.Product {
    export interface IProductTileItem {
        id: any;
        syncCode1C: string;
        articleName: string;
        articleSyncCode1c: string;
        furnitureTypeAlias: string;
        furnitureTypeName: string;
        furnitureTypeId: string;
        seriesName: string;
        seriesAlias: string;
        isFavorite: boolean;
        discount: number;
        isAssemblyRequired: boolean;
        width: number;
        length: number;
        height: number;
        price: Dto.Money.IMoney;
        priceForAssembly: Dto.Money.IMoney;
        salePrice: Dto.Money.IMoney;
        salePriceWithAssembly: Dto.Money.IMoney;
        salePriceWithOutAssembly: Dto.Money.IMoney;
    }
}