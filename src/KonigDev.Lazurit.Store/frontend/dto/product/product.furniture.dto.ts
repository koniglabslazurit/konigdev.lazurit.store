﻿namespace Dto.Product {
    export interface IFurnitureType {
        id: any;
        name: string;
        nameInPlural: string;
        amount: number;
        isActive: boolean
    }
}