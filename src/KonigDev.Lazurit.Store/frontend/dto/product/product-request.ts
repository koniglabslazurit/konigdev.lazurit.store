﻿namespace Dto.Product {
    export interface ProductRequest {
        facingIds: Array<string>,
        facingColorIds: Array<string>,
        frameColorIds: Array<string>,
        furnitureTypeId: string,
        furnitureAlias: string,
        collectionId: string,
        collectionVariantId: string,
        userId: string
    }
}
