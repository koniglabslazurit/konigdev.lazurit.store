﻿namespace Dto.Image.ProductContentAdding {
    export interface IProductContentAdding {
        productId: string;
        collectionVariantId: string;
    }
}

namespace Dto.Image {
    export interface IUploadFileModel {
        objectId: string;
        type: string;
    }

    export interface IUploadFileModelExtended {
        abstractProduct: any;
        imageType: string;
    }
}