﻿namespace Dto.Validate {
    export interface IResponse {
        success: boolean;
        message: string;
    };
}