﻿namespace Dto.Paging {
    export interface IPaging {
        itemsOnPage: number;
        page: number;
        totalCount: number;
        pageCount: number;
    }
}