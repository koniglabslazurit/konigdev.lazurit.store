﻿
namespace Dto.Import {
    export interface ICollections {
        id: string;
        originalId: string;
        room: string;
        series: string;
        validationProducts: number;
        templateProducts: number;
        publishedProducts: number;
    }
}

namespace Dto.Import {
    export interface ICollectionsQuery {
        page: number;
        itemsOnPage: number;
    }
}