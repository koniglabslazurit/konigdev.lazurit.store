﻿namespace Dto.Import {
    export interface IProductImportResponse {
        items: Array<Dto.Import.IProductImport>;
        totalCount: number;
    };

    /**

    */
    export interface IValidationResponse {
        isValid: boolean;
        errors: Array<string>;
    };

    export interface IValidationRequest {
        syncCode1C: string;
    }
};

namespace Dto.Import {
    export interface IProductValidationImportResponse {
        items: Array<Dto.Import.IProductValidationImport>;
        totalCount: number;
    }
}

namespace Dto.Import {
    export interface IProductImport {
        id: string;
        isSelected: boolean;
        integrationStatus: string;
        //specs
        furnitureType: string;
        syncCode1C: string;
        frameColor: string;
        facing: string;
        facingColor: string;
        syncCode1CConformity: string;
        targetTypes: Array<string>;
        styles: Array<string>;
        descr: string;
        isDisassemblyState: boolean;
        isDefault: boolean;
        roomSize: string;
        sleepingAreaAmount: number;
        //1c properties
        backlight: string;
        material: string;
        leftRight: string;
        furnitureForm: string;
        productPartMarker: string;
        height: number;
        width: number;
        length: number;
        permissibleLoad: number;
        sleepingAreaWidth: number;
        sleepingAreaLength: number;
        warranty: number;
        productDeliveryAndAssembly: Dto.Import.Products.IProductImportDeliveryAndAssembly;
        productId: string;
        marketerRange: string;
        marketerVendorCode: string;
        isImageWithoutInteriorExist: boolean;
        isImageInInteriorExist: boolean;
        isImageSliderInInteriorExist: boolean;
        isImageSliderMiddleExist: boolean;
        isImageSliderOpenExist: boolean;
    }
}

namespace Dto.Import {
    export interface IProductValidationImport {
        id: string;
        isSelected: boolean;
        isPublished: boolean;
        //specs
        furnitureType: string;
        syncCode1C: string;
        frameColor: string;
        facing: string;
        facingColor: string;
        accordance: string;
        targetTypes: Array<any>;
        targetTypesDto: Array<Dto.Style.DtoStyleItem>;
        styles: Array<any>;
        stylesDto: Array<Dto.Style.DtoStyleItem>;
        descr: string;
        disassemblyState: boolean;
        roomSize: string;
        sleepingAreaAmount: number;
        //1c properties
        backlight: string;
        backlightDto: Dto.Select.Item.ISelectItem;
        material: string;
        materialDto: Dto.Select.Item.ISelectItem;
        leftRight: string;
        leftRightDto: Dto.Select.Item.ISelectItem;
        furnitureForm: string;
        furnitureFormDto: Dto.Select.Item.ISelectItem;
        productPartMarker: string;
        productPartMarkerDto: Dto.Select.Item.ISelectItem;
        mechanism: string;
        mechanismDto: Dto.Select.Item.ISelectItem;
        height: number;
        width: number;
        length: number;
        permissibleLoad: number;
        sleepingAreaWidth: number;
        sleepingAreaLength: number;
        warranty: number;
        productDeliveryAndAssembly: Dto.Import.Products.IProductImportDeliveryAndAssembly;
        //download image
        productId: string;
        marketerRange: string;
        marketerVendorCode: string;
        isImageWithoutInteriorExist: boolean;
        isImageInInteriorExist: boolean;
        isImageSliderInInteriorExist: boolean;
        isImageSliderMiddleExist: boolean;
        isImageSliderOpenExist: boolean;
    }
}

namespace Dto.Import.Products {
    export interface IProductImportDeliveryAndAssembly {
    }
}

namespace Dto.Import {
    export interface IProductImportQuery {
        page: number;
        itemsOnPage: number;
        productStatus: string;
        vendorCode: string;
        integrationCollectionId: string;
        furnitureType: string;
        syncCode1C: string;
        frameColor: string;
        facing: string;
        facingColor: string;
    }
}

namespace Dto.Import {
    export interface IPublishedProductsResponse {
        items: Array<Dto.Import.IPublishedProduct>;
        totalCount: number;
    }
}

namespace Dto.Import {
    export interface IPublishedProduct {
        vendorCode: string;
        furnitureType: string;
        products: Array<IProductImport>;
    }
}

namespace Dto.Import {
    export interface IPublishedProductsQuery {
        page: number;
        itemsOnPage: number;
        vendorCode: string;
    }
}
