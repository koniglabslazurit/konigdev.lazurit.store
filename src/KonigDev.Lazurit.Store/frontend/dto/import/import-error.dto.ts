﻿namespace Dto.Import {
    export interface IImportError {
        errors: Array<string>;
        isValid: boolean;
    }
}