﻿namespace Dto.Property {
    export interface DtoPropertyItem {
        id: string;
        title: string;
        syncCode1C: string;
        titleInPlural: string;
        alias: string;
        aliasInPlural: string;
        value: string;
        isRequired: boolean;
        isMultiValue: boolean;
        sortNumber: number;
        groupeId: string;
        name: string;
        imageUrl: string;
    }
    export interface GetPropertiesQuery {
        propertyType: string
    }
}