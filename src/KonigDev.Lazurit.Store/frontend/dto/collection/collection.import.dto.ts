﻿namespace Dto.ImportCollection {

    export interface IIntegrationCollection {
        id: string,
        roomType: string;
        seriesName: string;
        integrationVariants: Array<Dto.ImportCollection.IIntegrationCollectionsVariant>;
    }

    export interface IIntegrationCollectionsVariant {
        id: string;
        number: number;
        collectionId: string;
        facingName: string;
        facingColorName: string;
        frameColorName: string;
        collectionTypeName: string;
        isComplect: boolean;
        isDefault: boolean;
        isPublished: boolean;
        originalId: string;
        isProductsVisible: boolean;
        largeImage: Dto.ImportCollection.IImage;
        middleImage: Dto.ImportCollection.IImage;
        smallImage: Dto.ImportCollection.IImage;
        integrationProducts: Array<Dto.ImportCollection.IIntegrationProduct>;
    }

    export interface IImage {
        id: string;
        objectId: string;
        typeId: string;
        typeName: string;
        name: string;     
    }

    export interface IIntegrationProduct {
        itemId: string;                                               
        productId: string;
        objectId: string;
        syncCode1C: string;
        vendorCodeName: string;
        furnutureType: string;
        src: string;   
        contentHeight: number;
        contentWidth: number;
        contentLeft: number;
        contentTop: number;
            
    }

    export interface ICoordinatesModel {
        id: string;
        left: string;
        top: string;
        width: string;
        height: string;
    }
        
}