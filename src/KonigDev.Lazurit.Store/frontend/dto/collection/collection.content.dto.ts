﻿namespace Dto.CollectionPrice {

    export interface ICollectionWithContentPrice {
        id: string;
        roomType: string;
        seriesName: string;
        facings: Array<Dto.Filter.IColorFilter>;
        facingColors: Array<Dto.Filter.IColorFilter>;
        frameColors: Array<Dto.Filter.IColorFilter>;
        variants: Dto.CollectionPrice.ICollectionVariantContentPrice[];
    }

    export interface ICollectionVariantContentPrice {
        id: string;
        collectionId: string;
        facings: Array<Dto.Filter.IColorFilter>;
        facingColors: Array<Dto.Filter.IColorFilter>;
        frameColors: Array<Dto.Filter.IColorFilter>;
        collectionTypeName: string;
        isActive: boolean;
        isSelected: boolean;
        isComplect: boolean;
        isFavorite: boolean;
        products: Dto.CollectionPrice.IProductContentPrice[];
    }
    export interface IProductContentPrice {
        id: any;
        articleName: string;
        articleSyncCode1c: string;
        furnitureTypeAlias: string;
        furnitureTypeName: string;
        objectId: string;
        isEnabled: boolean;
        contentLeft: number;
        contentTop: number;
        contentWidth: number;
        contentHeight: number;
        isFavorite: boolean;
        productId: string;
        discount: number;
        isAssemblyRequired: boolean;
        itemId: string;
        price: Dto.Money.IMoney;
        priceForAssembly: Dto.Money.IMoney;
        salePrice: Dto.Money.IMoney;
        salePriceWithAssembly: Dto.Money.IMoney;
        salePriceWithOutAssembly: Dto.Money.IMoney;
    }
}