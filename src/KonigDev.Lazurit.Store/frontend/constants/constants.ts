﻿export default class Constants implements Constants.IConstants {
    public cart: Constants.Cart.ICartConstants;
    public favorite: Constants.Favorite.IFavoriteConstants;
    public auth: Constants.Auth.IAuthConstants;
    public product: Constants.Product.IProductConstants;
    public order: Constants.Orders.IOrderConstants;
    public popupUrls: Constants.Urls.IPopupUrls;
    public customerRequests: Constants.CustomerRequests.ICustomerRequestConstants;

    public constructor() {
        console.log("init constants");

        this.cart = {
            collectionAdded: "Коллекция была добавлена в корзину",
            collectionNotAdded: "Коллекция не была добавлена в корзину",
            collectionDeleted: "Коллекция была удалена из корзины",
            collectionNotDeleted: "Коллекция не была удалена из корзины",

            complectAdded: "Комплект был добавлен в корзину",
            complectNotAdded: "Комплект не был добавлен в корзину",
            complectDeleted: "Комплект был удален из корзины",
            complectNotDeleted: "Комплект не был удален из корзины",

            productAdded: "Продукт был добавлен в корзину",
            productNotAdded: "Продукт не был добавлен в корзину",
            productDeleted: "Продукт был удален из корзины",
            productNotDeleted: "Продукт не был удален из корзины",

            goodsAdded: "Товар добавлен в корзину",
            goodsNotAdded: "Товар не добавлен в корзину. Пользователь не найден",

            consultantWillContact: "В ближайшее время с вами свяжется продавец-консультант",
            choosePaymentMethod: "Пожалуйста выберите способ оплаты"
        };

        this.favorite = {
            messages: {
                collectionAdded: "Коллекция была добавлена в избранное",
                collectionNotAdded: "Коллекция не была добавлена в избранное",
                collectionDeleted: "Коллекция была удалена из избранного",
                collectionNotDeleted: "Коллекция не была добавлена в избранное",

                complectAdded: "Комплект был добавлен в избранное",
                complectNotAdded: "Комплект не был добавлен в избранное",
                complectDeleted: "Комплект был удален из избранного",
                complectNotDeleted: "Комплект не был удален в избранное",

                productAdded: "Продукт был добавлен в избранное",
                productNotAdded: "Продукт не был добавлен в избранное",
                productDeleted: "Продукт был удален из избранного",
                productNotDeleted: "Продукт не был удален из избранного",
            },
            labels: {
                isInFavorite: "В избранном",
                IsNotInFavorite: "В избранное"
            },
            buttons: {},
            placeholders: {},
            links: {}
        };

        this.auth = {
            enterEmail: "введите e-mail",
            enterName: "введите имя",
            enterLastName: "введите фамилию",
            enterMiddleName: "введите отчество",
            enterPhoneNumber: "введите номер телефона",
            enterAddress: "введите ваш адрес",
            registrateOrLogin: "зарегистрируйтесь или войдите под своим профилем"
        };

        this.product = {
            productNotFound : "Не найден товар с выбранными параметрами"
        }

        this.order = {
            paymentMethods: {
                cash: "Cash",
                bankCard: "BankCard",
                instalment: "Instalment",
                qiwiWallet: "QiwiWallet",
                yandexMoney: "YandexMoney",
                payPal: "PayPal"
            }
        }

        this.popupUrls = {
            loginPopup: "/login-popup"
        }

        this.customerRequests = {
            messages: {
                requestCreated :"Заявка отправлена",
                notSupportedType: "Not supported type of CustomerRequest",
                statusUpdateSuccess: "Статус заявки обновлён",
                statusUpdateError: "Ошибка изменеия статуса",
                removeSuccess: "Заявка удалена",
                removeError: "Ошибка удаления заявки",
            },
            types: {
                call: "Call",
                designerOnline: "DesignerOnline",
                designerHome: "DesignerHome"
            },
            modalHeaders: {
                call: "Заказ звонка",
                designerOnline: "Заказ дизайнера (онлайн)",
                designerHome: "Заказ дизайнера (на дом)"
            }
        }
    }
}