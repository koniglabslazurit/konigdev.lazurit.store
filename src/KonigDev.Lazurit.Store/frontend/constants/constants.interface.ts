﻿namespace Constants {
    export interface IConstants {
        cart: Constants.Cart.ICartConstants;
        favorite: Constants.Favorite.IFavoriteConstants;
        auth: Constants.Auth.IAuthConstants;
        product: Constants.Product.IProductConstants;
        order: Constants.Orders.IOrderConstants;
        popupUrls: Constants.Urls.IPopupUrls;
        customerRequests: CustomerRequests.ICustomerRequestConstants;
    }
}

namespace Constants.Cart {
    export interface ICartConstants {

        productAdded: string;
        productNotAdded: string;
        productDeleted: string;
        productNotDeleted: string,

        complectAdded: string;
        complectNotAdded: string;
        complectDeleted: string;
        complectNotDeleted: string;

        collectionAdded: string;
        collectionNotAdded: string;
        collectionDeleted: string;
        collectionNotDeleted: string;

        goodsAdded: string;
        goodsNotAdded: string;

        consultantWillContact: string;
        choosePaymentMethod: string;
    }
}

namespace Constants.Favorite {
    export interface IFavoriteConstants {
        messages: Constants.Favorite.Messages.IFavoriteMessagesConstants;
        labels: Constants.Favorite.Labels.IFavoriteLabelsConstants;
        buttons: Constants.Favorite.Buttons.IFavoriteButtonsConstants;
        placeholders: Constants.Favorite.Placeholders.IFavoritePlaceholdersConstants;
        links: Constants.Favorite.Links.IFavoriteLinksConstants;
    }
}

namespace Constants.Favorite.Messages {
    export interface IFavoriteMessagesConstants {
        productAdded: string;
        productNotAdded: string;
        productDeleted: string;
        productNotDeleted: string,

        complectAdded: string;
        complectNotAdded: string;
        complectDeleted: string;
        complectNotDeleted: string;

        collectionAdded: string;
        collectionNotAdded: string;
        collectionDeleted: string;
        collectionNotDeleted: string;
    }
}

namespace Constants.Favorite.Labels {
    export interface IFavoriteLabelsConstants {
        isInFavorite: string;
        IsNotInFavorite: string;
    }
}

namespace Constants.Favorite.Buttons {
    export interface IFavoriteButtonsConstants {

    }
}

namespace Constants.Favorite.Placeholders {
    export interface IFavoritePlaceholdersConstants {

    }
}

namespace Constants.Favorite.Links {
    export interface IFavoriteLinksConstants {

    }
}

namespace Constants.Auth {
    export interface IAuthConstants {
        enterEmail: string;
        enterName: string;
        enterLastName: string;
        enterMiddleName: string;
        enterPhoneNumber: string;
        enterAddress: string;
        registrateOrLogin: string;
    }
}

namespace Constants.Product {
    export interface IProductConstants {
        productNotFound: string;
    }
}

namespace Constants.Orders {
    export interface IOrderConstants {
        paymentMethods: Constants.Orders.PaymentMethods.IPaymentMethodsConstants;
    }
}

namespace Constants.Orders.PaymentMethods {
    export interface IPaymentMethodsConstants {
        cash: string;
        instalment: string;
        bankCard: string;
        qiwiWallet: string;
        yandexMoney: string;
        payPal: string;
    }
}

namespace Constants.Urls {
    export interface IPopupUrls {
        loginPopup: string;
    }
}

namespace Constants.CustomerRequests {
    export interface ICustomerRequestConstants {
        messages: Messages.ICustomerRequestMessagesConstants;
        types: Types.ICustomerRequestTypesConstants;
        modalHeaders: Types.ICustomerRequestTypesConstants;
    }
}

namespace Constants.CustomerRequests.Messages {
    export interface ICustomerRequestMessagesConstants {
        requestCreated: string;
        notSupportedType: string;
        statusUpdateSuccess: string;
        statusUpdateError: string;
        removeSuccess: string;
        removeError: string;
    }
}

namespace Constants.CustomerRequests.Types {
    export interface ICustomerRequestTypesConstants {
        call: string;
        designerOnline: string;
        designerHome: string;
    }
}
