﻿import * as angular from "angular";

import service from "./constants";

export const name = "kd.constants";
export const serviceName = "constantsService";

angular
    .module(name, [ ])
    .service(serviceName, [service]);
