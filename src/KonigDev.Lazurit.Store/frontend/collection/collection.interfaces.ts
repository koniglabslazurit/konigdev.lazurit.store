﻿namespace Collection.Services {
    export interface ICollectionApiService {
        getCollectionVariantsWithContent(request: any): angular.IPromise<Dto.CollectionPrice.ICollectionWithContentPrice>;
    };

    export interface ICollectionService {
        getFilters(collectionId: string): angular.IPromise<Dto.Filter.IDtoFiltersResult>;
        getFurnitureTypes(request: any): angular.IPromise<Array<any>>;
        getProducts(request: any): any;
        addProductToCart(model: any): any;
        addCollectionToCart(model: any): any;
        addComplectToCart(model: any): any;
        /* todo перенести работу с куками в отдельный сервис */
        getCookie(value: string): string;
        removeCookie(value: string): void;
        getElementById(name: string): HTMLElement;
        getAttribute(formElement: HTMLElement, attribute: string): string;
    };
}