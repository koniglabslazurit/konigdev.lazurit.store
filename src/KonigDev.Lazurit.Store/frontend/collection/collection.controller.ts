﻿export default class CollectionController {

    /* варианты исполнения коллекции */
    public currentVariant: Dto.CollectionPrice.ICollectionVariantContentPrice;
    /* активные (подходящие выбранным фильтрам) варианты исполнения коллекции */
    public activeVariants: Array<Dto.CollectionPrice.ICollectionVariantContentPrice>;
    /* алиасы */
    public seriesAlias: string;
    public roomTypeAlias: string;
    /* коллекция */
    public collection: Dto.CollectionPrice.ICollectionWithContentPrice = null;
    /* все фильтры коллекции */
    public frameColorsList: Array<Dto.Filter.IColorFilter> = [];
    public facingColorsList: Array<Dto.Filter.IColorFilter> = [];
    public facingsList: Array<Dto.Filter.IColorFilter> = [];
    /* выбранные фильтры на странице */
    public selectedFrameColors: Array<string> = [];
    public selectedFacingColors: Array<string> = [];
    public selectedFacings: Array<string> = [];
    /** список продуктов в тайлах */
    public productsList: Array<Dto.Product.IProductTileItem> = [];
    public productsListLoaded: Array<Dto.Product.IProductTileItem> = [];

    /* все типы фурнитуры в коллекции */
    public furnitureTypesList: Array<Dto.Product.IFurnitureType>;
    public furnitureTypeId: string;
    private productsOnPageCount: any = 5;

    /** 1. поучаем все варианты коллекции с товарами для анимированной картинки, с ценами для этих товаров, входящих в анимированную картинку
     * 2. получаем список товаров для нижней части, отбираем по фильтрам выбранным в фильтрах или если выбран вариант коллекции- по фильтрам варианта, получаем с ценами
     * 3. при изменении фильтров или варианта - запрашиваем снова товары для этих фильтров (по выбранным фильтрам или по фильрам из варианта)
     * 4. Если выбираем тип фурнитуры - учитваем ее в запросе, пересчитываем для типов фурнитуры количества
     */

    /* todo вынести в сервисы, прокинуть туда зависимости */
    constructor(public collectionApiService: Collection.Services.ICollectionApiService,
        private collecionService: Collection.Services.ICollectionService,
        private messageService: Common.Message.IMessage,
        private constants: Constants.IConstants,
        private geolocationService: any) {
        this.init();
    }

    init(): void {
        /** получение значений из атрибутов */
        var formElement = this.collecionService.getElementById("collectionCardForm");
        this.seriesAlias = this.collecionService.getAttribute(formElement, "collection-alias"); //formElement.getAttribute("collection-alias").valueOf();
        this.roomTypeAlias = this.collecionService.getAttribute(formElement, "room-alias"); //formElement.getAttribute("room-alias").valueOf();

        /* получаем значения из кук если пришил с конкретного варианта */
        let collectionVariantId = this.collecionService.getCookie("variantId");
        /* получаем список всех вариантов в коллекции */
        var params = {
            roomTypeAlias: this.roomTypeAlias,
            seriesAlias: this.seriesAlias
        };
        this.geolocationService.getNearbyCities()
            .then((result: any) => {
                return this.collectionApiService
                    .getCollectionVariantsWithContent(params)
            })
            .then((response: any) => {
                this.collection = response;
                /* если нет конкретного варианта, берем первый прилетевший, иначе отбираем по переданному ид в куках */
                if (!collectionVariantId || this.collection.variants.filter((v: Dto.CollectionPrice.ICollectionVariantContentPrice) => { return v.id === collectionVariantId }).length === 0) {
                    this.currentVariant = this.collection.variants[0];
                } else {
                    this.currentVariant = this.collection.variants.filter((v: Dto.CollectionPrice.ICollectionVariantContentPrice) => { return v.id === collectionVariantId })[0];
                }
                this.collecionService.removeCookie("variantId");
                return response;
            })
            .then((response: any) => {
                /* маппим все фильтры/цвета у коллекции */
                this.getFilters();
            })
            .catch((error: any) => {
                console.log(error);
            });
    };

    /* получение всех доступных фильтров в коллекции */
    getFilters(): void {
        this.facingColorsList = this.collection.facingColors;
        this.frameColorsList = this.collection.frameColors;
        this.facingsList = this.collection.facings;
        /* устанавливаем выбранные фильры по дефолтному варианту коллекции*/
        this.setFilters();
        /* Запрос списка продуктов в коллекции (во всех вариантах из коллекции) подходящих под выбранные фильтры */
        this.getProductsByFilters();
    };

    /*  установка фильтров по выбранному варианту коллекции */
    setFilters(): void {
        if (this.currentVariant) {
            //так как наш мултифильтр принимает объекты но возвращает лишь их Id пришлось смапить их в строки
            this.selectedFacingColors = this.currentVariant.facingColors.map((filter: Dto.Filter.IColorFilter) => filter.id);
            this.selectedFacings = this.currentVariant.facings.map((filter: Dto.Filter.IColorFilter) => filter.id);
            this.selectedFrameColors = this.currentVariant.frameColors.map((filter: Dto.Filter.IColorFilter) => filter.id);
            this.filterVariants();
        }
    };

    /* получение вариантов, подходящих под фильтры и установка их как доступных, а так же выбор выбранного варианта */
    filterVariants(): void {
        this.activeVariants = this.collection.variants;
        if (this.selectedFacings && this.selectedFacings.length > 0) {
            this.activeVariants = this.activeVariants
                .filter((v: Dto.CollectionPrice.ICollectionVariantContentPrice) => {
                    return v.facingColors.some((fc: Dto.Filter.IColorFilter) => {
                        return this.selectedFacingColors.some((filterId: string) => {
                            return fc.id === filterId;
                        });
                    })
                });
        }
        if (this.selectedFrameColors && this.selectedFrameColors.length > 0) {
            this.activeVariants = this.activeVariants
                .filter((v: Dto.CollectionPrice.ICollectionVariantContentPrice) => {
                    return v.frameColors.some((frm: Dto.Filter.IColorFilter) => {
                        return this.selectedFrameColors.some((filterId: string) => {
                            return frm.id === filterId;
                        });
                    })
                });
        }
        if (this.selectedFrameColors && this.selectedFrameColors.length > 0) {
            this.activeVariants = this.activeVariants
                .filter((v: Dto.CollectionPrice.ICollectionVariantContentPrice) => {
                    return v.facingColors.some((fc: Dto.Filter.IColorFilter) => {
                        return this.selectedFacingColors.some((filterId: string) => {
                            return fc.id === filterId;
                        });
                    })
                });
        }
        if (this.activeVariants) {
            this.collection.variants.forEach((c) => {
                var isActive = this.activeVariants.some((v) => { return v.id === c.id });
                if (isActive) {
                    c.isActive = true;
                }
                else c.isActive = false;
            });
        }
        this.collection.variants.forEach((v) => {
            if (v.id === this.currentVariant.id)
                v.isSelected = true;
            else
                v.isSelected = false;
        });
    };

    /* получение продуктов коллекции по параметрам фильтров при выборе варианта коллекции получаем все продукты, у которых фильтры такие же как у варианта коллекции */
    getProductsByFilters(): void {
        var request = {
            CollectionId: this.collection.id,
            FurnitureTypeId: this.furnitureTypeId,
            FacingIds: this.selectedFacings,
            FacingColorIds: this.selectedFacingColors,
            FrameColorIds: this.selectedFrameColors
        };
        this.collecionService.getProducts(request)
            .then((response: any) => {

                console.log(response);

                this.productsList = response.items;
                this.productsListLoaded = [];
                this.loadMoreProducts();
            })
            .catch((error: any) => {
                console.log(error);
            });
        this.getFurnitureTypes();
    };

    /* получение списка типов фурнитуры */
    getFurnitureTypes(): void {
        var request = {
            CollectionId: this.collection.id,
            FacingIds: this.selectedFacings,
            FacingColorIds: this.selectedFacingColors,
            FrameColorIds: this.selectedFrameColors
        };
        this.collecionService.getFurnitureTypes(request)
            .then((response: Array<any>) => {
                this.furnitureTypesList = response;
                //помечаем выбранный тип фурнитуры как активный
                this.furnitureTypesList.forEach((furnitureType: Dto.Product.IFurnitureType) => {
                    if (furnitureType.id === this.furnitureTypeId) {
                        //если продуктов с выбранным типом фурнитуры бльше 0 то все ок
                        if (furnitureType.amount !== 0) {
                            furnitureType.isActive = true;
                        }
                        //но если мы выбрали тип фурнитуры, и фильтры несопоставимые с ним, то т.к. по верстке мы скрываем типы мебели с нулевым количеством итемов
                        //то юзер увидит пустой список товаров и в меню типов мебели будет иллюзия что выбранны все типы мебели так как выбранный тип просто скрыт
                        //поэтому мы сразу перезапрашиваем в таком  случае мебель по тем же фильтрам но по всем типам
                        else {
                            this.furnitureTypeId = '';
                            this.getProductsByFilters();
                        }
                    }
                    else {
                        furnitureType.isActive = false;
                    }
                });
            })
            .catch((error: any) => {
                console.log(error);
            });
    };

    /* смена выбранного варианта */
    changeVariant(variantId: string): void {
        this.currentVariant = this.collection.variants.filter((v: Dto.CollectionPrice.ICollectionVariantContentPrice) => {
            return v.id === variantId;
        })[0];
        this.setFilters();
        this.getProductsByFilters();
    };

    /** todo вынести в директиву? */
    hover(productId: string, isHover): void {
        var photo = document.getElementById("photo_" + productId);
        if (isHover === true) {
            photo.className = "active item-photo";
        }
        else {
            photo.className = "item-photo";
        }
    };

    /* выбор цвета корпуса */
    changeFrameColor = (newVal: Array<any>) => {
        this.selectedFrameColors = newVal;
        this.filterVariants();
        this.getProductsByFilters();
    };

    /* выбор отделки */
    changeFacing = (newVal) => {
        this.selectedFacings = newVal;
        this.filterVariants();
        this.getProductsByFilters();
    };

    /* выбор цвета отделки */
    changeFacingColor = (newVal) => {
        this.selectedFacingColors = newVal;
        this.filterVariants();
        this.getProductsByFilters();
    };

    changeFurnitur = (id) => {
        this.furnitureTypeId = id;
        this.getProductsByFilters();
    };

    /* подгрузка продуктов */
    loadMoreProducts(): void {
        if (this.productsList.length > this.productsListLoaded.length) {
            var count = this.productsListLoaded.length + this.productsOnPageCount;
            for (var i = this.productsListLoaded.length; i < count; i++) {
                if (this.productsList[i]) {
                    this.productsListLoaded.push(this.productsList[i]);
                }
            }
        }
    };

    /* продукт в корзину */
    addProductToCart(product: any): void {
        product.isAddingToCart = true;
        this.collecionService.addProductToCart({ ProductId: product.productId })
            .then((response: any) => {
                product.isAddingToCart = false;
            }).catch((error: any) => {
                product.isAddingToCart = false;
                this.messageService.error(error.data.message, error);
            });
    };
    /** коллекция в корзину */
    addCollectionToCart(collection: any): void {
        collection.isAddingToCart = true;
        this.collecionService.addCollectionToCart({ CollectionId: collection.id })
            .then((response) => {
                collection.isAddingToCart = false;
            }).catch((error: any) => {
                collection.isAddingToCart = false;
                this.messageService.error(error.data.message, error);
            });
    };
    /** комплект в корзину */
    addComplectToCart(collection: any): void {
        collection.isAddingToCart = true;
        this.collecionService.addComplectToCart({ CollectionId: collection.id })
            .then((response) => {
                collection.isAddingToCart = false;
            }).catch((error: any) => {
                collection.isAddingToCart = false;
                this.messageService.error(error.data.message, error);
            });
    };
};