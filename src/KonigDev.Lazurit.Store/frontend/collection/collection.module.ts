﻿import * as angular from "angular";

/* import others modules */
import * as flterModule from "../filter/filter.module";
import * as furnitureTypeModule from "../furniture-type/furniture-type.module";
import * as productModule from "../product/product.module";
import * as cartModule from "../cart/cart-service/cart-mini-service.module";
import * as commonModule from "../common/common.module";
import * as constantsModule from "../constants/constants.module";
import * as favoriteTriggerModule from "../favorite/favorite-trigger/favorite-trigger.module";
import * as geolocationModule from "../geolocation/geolocation.module";
import * as bondModule from "../common/directive/bond/bond.module";

/* import some  */
import controller from "./collection.controller";
import collectionApiService from "./collection.service.api";
import collectionService from "./collection.service";

/* constants */
export const name = "kd.collection";
export const collectionApiServiceName = "collectionApiService";
export const collectionServiceName = "collectionService";
const controllerName = "collectionController";

angular
    .module(name,
    [
        flterModule.name,
        commonModule.name,
        constantsModule.name,
        favoriteTriggerModule.name,
        bondModule.name
    ])
    .controller(controllerName,
    [
        collectionApiServiceName,
        collectionServiceName,
        commonModule.messageServiceName,
        constantsModule.serviceName,
        geolocationModule.geolocationServiceName,
        controller
    ])
    .service(collectionApiServiceName, ['$http', collectionApiService])
    .service(collectionServiceName, [
        '$document',
        flterModule.serviceName,
        furnitureTypeModule.serviceName,
        productModule.serviceName,
        cartModule.cartServiceName,
        collectionService
    ]);
