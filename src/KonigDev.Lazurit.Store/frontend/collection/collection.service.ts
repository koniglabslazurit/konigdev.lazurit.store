﻿export default class CollectionService implements Collection.Services.ICollectionService {

    constructor(private $document: angular.IDocumentService,
        private FilterService: Filter.Services.IFilterApiService,
        private FurnitureTypeService,
        private ProductService,
        private CartService) {
    };

    public getCookie(name: any): any {
        var matches = this.$document[0].cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    };

    public removeCookie(value: string): void {
        this.$document[0].cookie = value + "=;expires=Thu, 01 Jan 1970 00:00:01 GMT;";
    };
    public getElementById(name: string): HTMLElement {
        return this.$document[0].getElementById(name);
    };

    public getAttribute(formElement, attribute): string {
        return formElement.getAttribute(attribute).valueOf();
    }

    public getFilters(collectionId: string): angular.IPromise<Dto.Filter.IDtoFiltersResult> {
        return this.FilterService.getFilters({ CollectionId: collectionId });
    };

    public getFurnitureTypes(request: any): angular.IPromise<Array<any>> {
        return this.FurnitureTypeService.getFurnitureTypes(request);
    };

    public getProducts(request: any): any {
        return this.ProductService.getProducts(request);
    };

    public addProductToCart(model: any): any {
        return this.CartService.addProductToCart(model);
    };

    public addCollectionToCart(model: any): any {
        return this.CartService.addCollectionToCart(model);
    };

    public addComplectToCart(model: any): any {
        return this.CartService.addComplectToCart(model);
    };
};