﻿export default class CollectionCardService implements Collection.Services.ICollectionApiService {

    private collectionVariant = "/api/collection/variant-content";

    constructor(private $http: angular.IHttpService) {
    }

    getCollectionVariantsWithContent(request: any): angular.IPromise<Dto.CollectionPrice.ICollectionWithContentPrice> {
        return this.$http
            .get(this.collectionVariant, { params: request })
            .then(response => response.data);
    }
}