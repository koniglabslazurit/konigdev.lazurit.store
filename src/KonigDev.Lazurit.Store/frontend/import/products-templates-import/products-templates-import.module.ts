﻿import * as angular from "angular";

import controller from "./products-templates-import.controller";
/* modules */
import * as importServiceModule from "../products-import-service/products-import-service.module";
import * as flterModule from "../../filter/filter.module";
import * as targetAudienceModule from "../../target-audience/target-audience.module";
import * as stylesModule from "../../style/style.module";
import * as propertiesModule from "../../property/property.module";
import * as selectSingleChosenModule from "../../common/components/select-single-chosen/select-single-chosen.module";
import * as selectMultiChosenModule from "../../common/components/select-multi-chosen/select-multi-chosen.module";
import * as imageModule from "../../image/image.module";
import * as commonServiceModule from "../../common/common.module";

/*constants*/

export const name = "kd.products.templates.import";

const controllerName = "productsTemplatesImportController";
const componentName = "kdProductsTemplatesImport";

angular
    .module(name,
    [
        flterModule.name,
        targetAudienceModule.name,
        stylesModule.name,
        propertiesModule.name,
        selectSingleChosenModule.name,
        selectMultiChosenModule.name,
        importServiceModule.name,
        imageModule.name,
        commonServiceModule.name
    ])                        
    .controller(controllerName,
    [
        importServiceModule.apiServiceName,
        importServiceModule.importProductServiceName,
        targetAudienceModule.serviceName,
        stylesModule.serviceName,
        propertiesModule.serviceName,
        flterModule.serviceName,
        imageModule.serviceName,
        commonServiceModule.messageServiceName,
        controller
    ])
    .component(componentName,
    {
        bindings: {
            integrationCollectionId : "<"
        },
        controller: controllerName,
        template: require("./products-templates-import.view.html")
    });