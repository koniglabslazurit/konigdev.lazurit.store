﻿export default class ProductsImportController {
    public products: Array<Dto.Import.IProductImport>;
    public activeTabNumber: number = 1;
    public integrationCollectionId: string;
    //from api
    public backlightList: Array<any>;
    public materialList: Array<any>;
    public leftRightList: Array<any>;
    public mechanismList: Array<any>;
    public furnitureFormList: Array<any>;
    public productPartMarkerList: Array<any>;
    //from api
    public targetAudienceList: Array<Dto.TargetAudience.DtoTargetAudienceItem>;
    public stylesList: Array<Dto.Style.DtoStyleItem>;
    public disassemblyStatesList: Array<any>;
    //paging
    public paging: Dto.Paging.IPaging;
    public pageCountlList: Array<any>;
    //for mass editing
    public allSelected: boolean = false;
    public targetAudienceForMassEditing: Array<string>;
    public stylesForMassEditing: Array<string>;
    public disassemblyStateForAll: string;
    //for filters
    public furnitureType: Array<Dto.SyncCode.DtoSyncCode1CItem>;
    public chosenFurnitureType: string;
    public syncCode1C: Array<Dto.SyncCode.DtoSyncCode1CItem>;
    public chosenSyncCode1C: string;
    public frameColor: Array<Dto.Filter.IDtoFrameColorItem>;
    public chosenFrameColor: string;
    public facing: Array<Dto.Filter.IDtoFacingItem>;
    public chosenFacing: string;
    public facingColor: Array<Dto.Filter.IDtoFacingColorItem>;
    public chosenFacingColor: string;
    //for download images
    public productDownload: Dto.Import.IProductValidationImport;
    public openModal: boolean = false;

    constructor(private importApiService: Product.Import.ApiService,
        private productImportService: Product.Import.Service,
        private targetAudienceService: TargetAudience.Services.ITargetAudienceApiService,
        private stylesService: Style.Services.IStyleApiService,
        private propertiesService: Property.Services.IPropertyApiService,
        private filterService: Filter.Services.IFilterApiService,
        private imageService: Product.Import.IImageService,
        private messageService: Common.Message.IMessage) {
        this.init();
    }

    private init(): void {
        this.paging = {
            //must be in request for products
            itemsOnPage: 10,
            page: 1,
            //calculatable after server response
            totalCount: 0,
            pageCount: 0
        };
        this.getProducts(1);
        this.getProperties();
        this.getStyles();
        this.getTargetAudiences();
        this.getFilters();
        this.disassemblyStatesList = [{ name: "Да", value: "true" }, { name: "Нет", value: "false" }, { name: "", value: "" }];
        this.pageCountlList = [{ value: 10, name: "10" }, { value: "20", name: "20" }, { value: 50, name: "50" }, { value: 100, name: "100" }, { value: 200, name: "200" }, { value: 500, name: "500" }, { value: 0, name: "все" }];
    };

    public getProducts(page: number): void {
        var query: Dto.Import.IProductImportQuery = {
            itemsOnPage: this.paging.itemsOnPage,
            page: page,
            productStatus: "Template",
            vendorCode: "",
            integrationCollectionId: this.integrationCollectionId,
            furnitureType: this.chosenFurnitureType,
            syncCode1C: this.chosenSyncCode1C,
            frameColor: this.chosenFrameColor,
            facing: this.chosenFacing,
            facingColor: this.chosenFacingColor
        }
        this.importApiService.getValidationProducts(query)
            .then((data: any) => {
                //default value for
                this.allSelected = false;
                //paging calculation
                this.paging.page = page;
                //in the first two cases value will be NaN and in the third it will looks like "1 из 0"
                if (data.totalCount === "" || data.totalCount === 'undefined' || data.totalCount === 0) {
                    this.paging.totalCount = 1;
                }
                else {
                    this.paging.totalCount = data.totalCount;
                }
                if (this.paging.itemsOnPage !== 0) {
                    this.paging.pageCount = Math.ceil(this.paging.totalCount / this.paging.itemsOnPage);
                }
                else {
                    this.paging.pageCount = 1;
                }
                //mapping products
                this.products = data.items.map((p) => {
                    p.isSelected = false;
                    return p;
                });
                //mapping bool values in accordance to existence of some image types for integrationProduct
                this.products.forEach((product: Dto.Import.IProductImport) => {
                    this.imageService.getAvialableImages(product.productId)
                        .then((imagesList: any) => {
                            if (imagesList.length > 0) {
                                product.isImageWithoutInteriorExist = imagesList.some(imageInfo => imageInfo.type === "WithoutInterior");
                                product.isImageInInteriorExist = imagesList.some(imageInfo => imageInfo.type === "InInterior");
                                product.isImageSliderInInteriorExist = imagesList.some(imageInfo => imageInfo.type === "SliderInInterior");
                                product.isImageSliderMiddleExist = imagesList.some(imageInfo => imageInfo.type === "SliderMiddle");
                                product.isImageSliderOpenExist = imagesList.some(imageInfo => imageInfo.type === "SliderOpen");
                            }
                        })
                        .catch((error: any) => {
                            console.log(error);
                        });
                });
            });
    };

    public checkIsAllImagesExist(product: Dto.Import.IProductImport): boolean {
        if (product.isImageWithoutInteriorExist && product.isImageInInteriorExist && product.isImageSliderInInteriorExist && product.isImageSliderMiddleExist && product.isImageSliderOpenExist) {
            return true;
        }
        else {
            return false;
        }
    }

    public editAllProducts(): void {
        var selectedProducts = this.products.filter((product: Dto.Import.IProductImport) => { return product.isSelected === true });
        if (typeof this.targetAudienceForMassEditing !== 'undefined' && this.targetAudienceForMassEditing.length !== 0) {
            selectedProducts.forEach((product) => { product.targetTypes = this.targetAudienceForMassEditing });
        }
        if (typeof this.stylesForMassEditing !== 'undefined' && this.stylesForMassEditing.length !== 0) {
            selectedProducts.forEach((product) => { product.styles = this.stylesForMassEditing });
        }
        if (this.disassemblyStateForAll === "true") {
            selectedProducts.forEach((product) => { product.isDisassemblyState = true });
        }
        else if (this.disassemblyStateForAll === "false") {
            selectedProducts.forEach((product) => { product.isDisassemblyState = false });
        }
    }

    public saveChanges(): any {
        var selectedProducts = this.products.filter((product: Dto.Import.IProductImport) => { return product.isSelected === true });
        if (selectedProducts && selectedProducts.length > 0) {
            this.importApiService.updateProducts(selectedProducts)
                .then((data: any) => {
                    this.messageService.success("Продукты успешно сохранены");
                }).catch((error: Error) => {
                    this.messageService.error("Ошибка при сохранении", error.message);
                    console.log(error);
                });
        }
        else {
            this.messageService.info("Пожалуйста выберите продукты");
        }
    }

    public saveChangesAndPublish(): any {
        var selectedProducts = this.products.filter((product: Dto.Import.IProductImport) => { return product.isSelected === true });
        if (selectedProducts && selectedProducts.length > 0) {
            var synccodes = selectedProducts.map((i: Dto.Import.IProductImport) => { return i.syncCode1C });
            /* проверка возможности публикации продукта, и публикация если нет ошибок */
            this.productImportService
                .validationPublish(synccodes)
                .then((model: Dto.Import.IImportError) => {
                    if (!model.isValid) {
                        model.errors.forEach((error: string) => {
                            this.messageService.uiError(error);
                        });
                        throw new Error("Невалидные данные");
                    }
                })
                .then(() => {
                    selectedProducts.forEach((product) => { product.integrationStatus = "Published" });
                    return this.importApiService.updateProducts(selectedProducts);
                })
                .then(() => {
                    this.messageService.success("Продукты успешно опубликованы");
                    this.getProducts(1);
                })
                .catch((error: Error) => {
                    this.messageService.error("Ошибка при сохранении", error.message);
                });
        }
        else {
            this.messageService.info("Пожалуйста выберите продукты");
        }
    }

    private getTargetAudiences(): void {
        this.targetAudienceService.getTargetAudiences()
            .then((data: any) => {
                this.targetAudienceList = data.map((s: any) => { return { id: s.name, name: s.name } });
            });
    }

    private getStyles(): void {
        this.stylesService.getStyles().then((data) => {
            this.stylesList = data.map((s: any) => { return { id: s.name, name: s.name } });
        });
    }

    private getProperties(): void {
        var request = { propertyType: '' };
        this.propertiesService.getProperties(request)
            .then((data: Array<any>) => {

                this.backlightList = data
                    .filter((item: any) => { return item.syncCode1C === 'Backlight' })
                    .map((item: any) => {
                        return { id: item.value, name: item.value }
                    });

                this.materialList = data
                    .filter((item: any) => { return item.syncCode1C === 'Material' })
                    .map((item: any) => {
                        return { id: item.value, name: item.value }
                    });

                this.leftRightList = data
                    .filter((item: any) => { return item.syncCode1C === 'LeftRight' })
                    .map((item: any) => {
                        return { id: item.value, name: item.value }
                    });

                this.mechanismList = data
                    .filter((item: any) => { return item.syncCode1C === 'Mechanism' })
                    .map((item: any) => {
                        return { id: item.value, name: item.value }
                    });

                this.furnitureFormList = data
                    .filter((item: any) => { return item.syncCode1C === 'FurnitureForm' })
                    .map((item: any) => {
                        return { id: item.value, name: item.value }
                    });
            });

        this.importApiService.getProductPartMarkers()
            .then((data: any) => {
                this.productPartMarkerList = data.data;
            });
    }

    public changeActiveTabNumber(activeTabNumber: number): void {
        this.activeTabNumber = activeTabNumber;
    }

    public selectAll(): void {
        this.products = this.products.map((p) => {
            p.isSelected = this.allSelected;
            return p;
        });
    };

    //paging
    public next(): void {
        this.paging.page++;
        this.getProducts(this.paging.page);
    };

    public prev(): void {
        this.paging.page--;
        this.getProducts(this.paging.page);
    };

    public getFilters(): any {
        var queryForFurniture: Dto.FurnitureType.GetFurnitureTypesQuery = { integrationCollectionId: this.integrationCollectionId }
        this.importApiService.getFurnitureTypes(queryForFurniture)
            .then((data: Array<Dto.FurnitureType.DtoFurnitureTypeItem>) => {
                this.furnitureType = data.map((item: any) => { return { id: item.syncCode1C, name: item.syncCode1C } });
                this.furnitureType.push({ id: "", name: "Все" });
            });
        var queryForSynCode: Dto.SyncCode.GetSyncCode1CsQuery = { integrationCollectionId: this.integrationCollectionId }
        this.importApiService.getSyncCodes(queryForSynCode)
            .then((data: Array<Dto.SyncCode.DtoSyncCode1CItem>) => {
                this.syncCode1C = data.map((item: any) => { return { id: item.name, name: item.name } });
                this.syncCode1C.push({ id: "", name: "Все" });
            });
        this.filterService.getFrameColors()
            .then((data: Array<Dto.Filter.IDtoFrameColorItem>) => {
                this.frameColor = data.map((item: any) => { return { id: item.syncCode1C, name: item.syncCode1C } });
                this.frameColor.push({ id: "", name: "Все" });
            });
        this.filterService.getFacings()
            .then((data: Array<Dto.Filter.IDtoFacingItem>) => {
                this.facing = data.map((item: any) => { return { id: item.syncCode1C, name: item.syncCode1C } });
                this.facing.push({ id: "", name: "Все" });
            });
        this.filterService.getFacingColors()
            .then((data: Array<Dto.Filter.IDtoFacingColorItem>) => {
                this.facingColor = data.map((item: any) => { return { id: item.syncCode1C, name: item.syncCode1C } });
                this.facingColor.push({ id: "", name: "Все" });
            });

        //if you din't have api you can take all possible filters from products
        //var u = {};
        //var productsCount = this.products.length;
        //var furnitureType: Array<string> = [];
        //var syncCode1C: Array<string> = [];
        //var frameColor: Array<string> = [];
        //var facing: Array<string> = [];
        //var facingColor: Array<string> = [];
        //for (var i = 0; i < productsCount; ++i) {
        //    //furnitureType
        //    if (u.hasOwnProperty(this.products[i].furnitureType)) {
        //        continue;
        //    }
        //    furnitureType.push(this.products[i].furnitureType);
        //    u[this.products[i].furnitureType] = 1;
        //    //syncCode1C
        //    if (u.hasOwnProperty(this.products[i].syncCode1C)) {
        //        continue;
        //    }
        //    syncCode1C.push(this.products[i].syncCode1C);
        //    u[this.products[i].syncCode1C] = 1;
        //    //frameColor
        //    if (u.hasOwnProperty(this.products[i].frameColor)) {
        //        continue;
        //    }
        //    frameColor.push(this.products[i].facing);
        //    u[this.products[i].frameColor] = 1;
        //    //facing
        //    if (u.hasOwnProperty(this.products[i].facing)) {
        //        continue;
        //    }
        //    facing.push(this.products[i].facing);
        //    u[this.products[i].facing] = 1;
        //    //facingColor
        //    if (u.hasOwnProperty(this.products[i].facingColor)) {
        //        continue;
        //    }
        //    facingColor.push(this.products[i].facing);
        //    u[this.products[i].facingColor] = 1;
        //}
        //var total: any = { furnitureType, syncCode1C, frameColor, facing, facingColor };
    }

    public filterProducts = (obj: any, selected: string): void => {
        if (obj.type === "furnitureType")
            this.chosenFurnitureType = selected;
        else if (obj.type === "syncCode1C")
            this.chosenSyncCode1C = selected;
        else if (obj.type === "frameColor")
            this.chosenFrameColor = selected;
        else if (obj.type === "facing")
            this.chosenFacing = selected
        else if (obj.type === "facingColor")
            this.chosenFacingColor = selected;
        this.getProducts(1);
    };

    public cancelFilters(): void {
        this.chosenFacing = null;
        this.chosenFacingColor = null;
        this.chosenFrameColor = null;
        this.chosenFurnitureType = null;
        this.chosenSyncCode1C = null;
        this.getProducts(1);
    };

    public openDownloadingWindow(product: Dto.Import.IProductValidationImport): void {
        this.productDownload = product;
        this.openModal = true;
    };

    public closeModal(): void {
        this.openModal = false;
        this.productDownload = null;
    };
}