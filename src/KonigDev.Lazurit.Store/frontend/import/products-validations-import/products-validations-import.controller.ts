﻿export default class ProductsImportController {
    public products: Array<Dto.Import.IProductValidationImport>;
    public activeTabNumber: number = 1;
    public targetAudienceList: Array<Dto.TargetAudience.DtoTargetAudienceItem>;
    public selectedTargetAudienceList: Array<string> = [];
    public styleList: Array<Dto.Style.DtoStyleItem>;
    public selectedStyleList: Array<string> = [];
    public backlightList: Array<Dto.Select.Item.ISelectItem>;
    public materialList: Array<Dto.Select.Item.ISelectItem>;
    public leftRightList: Array<Dto.Select.Item.ISelectItem>;
    public mechanismList: Array<Dto.Select.Item.ISelectItem>;
    public furnitureFormList: Array<Dto.Select.Item.ISelectItem>;
    public articleFormList: Array<Dto.Select.Item.ISelectItem>;
    public productPartMarkerList: Array<Dto.Select.Item.ISelectItem>;
    public disassemblyStatesList: Array<any>;
    //todo
    public paging: any;
    public allSelected: boolean = false;
    public collectionId: string;
    public integrationStatus: string = "Validation";
    public itemsOnPageList: Array<Dto.Select.Item.ISelectItem>;
    public selectedItemsOnPage: Dto.Select.Item.ISelectItem;
    public allDisassembly: string;
    public page: number = 1;

    //for filters
    public furnitureType: Array<Dto.FurnitureType.DtoFurnitureTypeItem>;
    public chosenFurnitureType: string = "";
    public syncCode1C: Array<Dto.SyncCode.DtoSyncCode1CItem>;
    public chosenSyncCode1C: string = "";
    public frameColor: Array<Dto.Filter.IDtoFrameColorItem>;
    public chosenFrameColor: string = "";
    public facing: Array<Dto.Filter.IDtoFacingItem>;
    public chosenFacing: string = "";
    public facingColor: Array<Dto.Filter.IDtoFacingColorItem>;
    public chosenFacingColor: string = "";
    //for download images
    public productDownload: Dto.Import.IProductValidationImport;
    public openModal: boolean = false;

    constructor(private importApiService: Product.Import.ApiService,
        private productImportService: Product.Import.Service,
        private targetAudienceService: TargetAudience.Services.ITargetAudienceApiService,
        private stylesService: Style.Services.IStyleApiService,
        private propertiesService: Property.Services.IPropertyApiService,
        private filterService: Filter.Services.IFilterApiService,
        private messageService: Common.Message.IMessage,
        private imageService: Product.Import.IImageService) {
        this.init();
    }

    private init(): void {
        /*by default*/
        this.disassemblyStatesList = [{ name: "Да", value: "true" }, { name: "Нет", value: "false" }, { name: "", value: "" }];
        this.itemsOnPageList = [
            { id: "10", name: "10", imageUrl: "" },
            { id: "20", name: "20", imageUrl: "" },
            { id: "50", name: "50", imageUrl: "" },
            { id: "100", name: "100", imageUrl: "" },
            { id: "200", name: "200", imageUrl: "" },
            { id: "500", name: "500", imageUrl: "" },
            { id: "0", name: "Все", imageUrl: "" }
        ];
        this.selectedItemsOnPage = { id: "10", name: "10", imageUrl: "" };
        this.paging = {
            itemsOnPage: "10",
            page: this.page,
            totalCount: 0,
            pageCount: 0
        };
        this.getFilters();
        this.getPropertiesAndProducts();
    };

    public generateQuery() {
        this.paging.page = this.page;
        var query: Dto.Import.IProductImportQuery = {
            itemsOnPage: this.paging.itemsOnPage,
            page: this.paging.page,
            productStatus: this.integrationStatus,
            vendorCode: "",
            integrationCollectionId: this.collectionId,
            furnitureType: this.chosenFurnitureType,
            syncCode1C: this.chosenSyncCode1C,
            frameColor: this.chosenFrameColor,
            facing: this.chosenFacing,
            facingColor: this.chosenFacingColor
        };
        return query;
    }

    public selectItemsOnPage = (data: any) => {
        this.page = 1;
        this.paging.page = 1;
        this.getProducts(this.generateQuery());
    }

    private getProducts(query: Dto.Import.IProductImportQuery): void {
        this.importApiService.getValidationProducts(query)
            .then((data: Dto.Import.IProductValidationImportResponse) => {
                this.paging.totalCount = data.totalCount;
                this.paging.pageCount = Math.ceil(data.totalCount / parseInt(this.paging.itemsOnPage));
                this.products = data.items.map((p: any) => {
                    p.isSelected = false;
                    p.isPublished = false;
                    //what a fuck is this???
                    //p.materialDto = this.materialList.filter((m) => { return m.name === p.material })[0];
                    //p.backlightDto = this.backlightList.filter((m) => { return m.name === p.backlight })[0];
                    //p.leftRightDto = this.leftRightList.filter((m) => { return m.name === p.leftRight })[0];
                    //p.furnitureFormDto = this.articleFormList.filter((m) => { return m.name === p.furnitureForm })[0];
                    //p.productPartMarkerDto = this.productPartMarkerList.filter((m) => { return m.name === p.productPartMarker })[0];
                    //p.mechanismDto = this.mechanismList.filter((m) => { return m.name === p.mechanism })[0];
                    //p.stylesDto = this.styleList.filter((m) => { return p.styles.indexOf(m.name) !== -1 });
                    //p.targetTypesDto = this.targetAudienceList.filter((m) => { return p.targetTypes.indexOf(m.name) !== -1 });
                    return p;
                });
                //mapping bool values in accordance to existence of some image types for integrationProduct
                this.products.forEach((product: Dto.Import.IProductValidationImport) => {
                    this.imageService.getAvialableImages(product.productId)
                        .then((imagesList: any) => {
                            if (imagesList.length > 0) {
                                product.isImageWithoutInteriorExist = imagesList.some(imageInfo => imageInfo.type === "WithoutInterior");
                                product.isImageInInteriorExist = imagesList.some(imageInfo => imageInfo.type === "InInterior");
                                product.isImageSliderInInteriorExist = imagesList.some(imageInfo => imageInfo.type === "SliderInInterior");
                                product.isImageSliderMiddleExist = imagesList.some(imageInfo => imageInfo.type === "SliderMiddle");
                                product.isImageSliderOpenExist = imagesList.some(imageInfo => imageInfo.type === "SliderOpen");
                            }
                        })
                        .catch((error: any) => {
                            console.log(error);
                        });
                });
            });
    };

    public checkIsAllImagesExist(product: Dto.Import.IProductValidationImport): boolean {
        if (product.isImageWithoutInteriorExist && product.isImageInInteriorExist && product.isImageSliderInInteriorExist && product.isImageSliderMiddleExist && product.isImageSliderOpenExist) {
            return true;
        }
        else {
            return false;
        }
    }

    private getPropertiesAndProducts(): void {
        this.propertiesService.getProperties("")
            .then((data) => {
                /* материал обивки */
                this.materialList = data.filter((p: any) => { return p.syncCode1C === "Material" })
                    .map((item: any) => { return { id: item.value, name: item.value, imageUrl: "" } });
                /* подсветка */
                this.backlightList = data.filter((p: any) => { return p.syncCode1C === "Backlight" })
                    .map((item: any) => { return { id: item.value, name: item.value, imageUrl: "" } });
                /* механизм */
                this.mechanismList = data.filter((p: any) => { return p.syncCode1C === "Mechanism" })
                    .map((item: any) => { return { id: item.value, name: item.value, imageUrl: "" } });
                /* форма артикула */
                this.articleFormList = data.filter((p: any) => { return p.syncCode1C === "FurnitureForm" })
                    .map((item: any) => { return { id: item.value, name: item.value, imageUrl: "" } });
                /* левый правый */
                this.leftRightList = data.filter((p: any) => { return p.syncCode1C === "LeftRight" })
                    .map((item: any) => { return { id: item.value, name: item.value, imageUrl: "" } });
                /* наличие карнизов */
                var productPartMarkerList = data.filter((p: any) => { return p.syncCode1C === "ProductPartMarker" })
                    .map((item: any) => { return { id: item.value, name: item.value, imageUrl: "" } });
            });

        /* стили */
        this.stylesService.getStyles()
            .then((data) => {
                this.styleList = data.map((s: any) => { return { id: s.name, name: s.name } });
            });
        /*целевая аудитория*/
        this.targetAudienceService.getTargetAudiences()
            .then((data) => {
                this.targetAudienceList = data.map((s: any) => { return { id: s.name, name: s.name } });
            });
        /*достаем продукты*/
        this.getProducts(this.generateQuery());
    };

    public changeActiveTabNumber(activeTabNumber: number): void {
        this.activeTabNumber = activeTabNumber;
    }

    public allDisassemblyState(): void {
        var selected = this.getSelectedProducts();
        if (this.allDisassembly === "true") {
            selected.forEach((product) => { product.disassemblyState = true });
        }
        else if (this.allDisassembly === "false") {
            selected.forEach((product) => { product.disassemblyState = false });
        }
    };

    public getSelectedProducts(): Array<Dto.Import.IProductValidationImport> {
        var selected = this.products.filter((p) => { return p.isSelected === true });
        return selected;
    };

    public selectAll(): void {
        this.products = this.products.map((p) => {
            p.isSelected = this.allSelected;
            return p;
        });
    };

    public validateSleepingAreaAmount(syncCode1C: string, value: string) {
        var test = /^\d+$/.test(value);
        if (test === false) {
            var product = this.products.filter((p) => { return p.syncCode1C === syncCode1C })[0];
            product.sleepingAreaAmount = parseInt(value.substring(0, value.length - 1));
        }
    }

    public validateHeight(syncCode1C: string, value: string) {
        var test = /^\d+$/.test(value);
        if (test === false) {
            var product = this.products.filter((p) => { return p.syncCode1C === syncCode1C })[0];
            product.height = parseInt(value.substring(0, value.length - 1));
        }
    }

    public validateWidth(syncCode1C: string, value: string) {
        var test = /^\d+$/.test(value);
        if (test === false) {
            var product = this.products.filter((p) => { return p.syncCode1C === syncCode1C })[0];
            product.width = parseInt(value.substring(0, value.length - 1));
        }
    }

    public validateLength(syncCode1C: string, value: string) {
        var test = /^\d+$/.test(value);
        if (test === false) {
            var product = this.products.filter((p) => { return p.syncCode1C === syncCode1C })[0];
            product.length = parseInt(value.substring(0, value.length - 1));
        }
    }

    public validatePermissibleLoad(syncCode1C: string, value: string) {
        var test = /^\d+$/.test(value);
        if (test === false) {
            var product = this.products.filter((p) => { return p.syncCode1C === syncCode1C })[0];
            product.permissibleLoad = parseInt(value.substring(0, value.length - 1));
        }
    }

    public validateSleepingAreaWidth(syncCode1C: string, value: string) {
        var test = /^\d+$/.test(value);
        if (test === false) {
            var product = this.products.filter((p) => { return p.syncCode1C === syncCode1C })[0];
            product.sleepingAreaWidth = parseInt(value.substring(0, value.length - 1));
        }
    }

    public validateSleepingAreaLength(syncCode1C: string, value: string) {
        var test = /^\d+$/.test(value);
        if (test === false) {
            var product = this.products.filter((p) => { return p.syncCode1C === syncCode1C })[0];
            product.sleepingAreaLength = parseInt(value.substring(0, value.length - 1));
        }
    }

    public validateWarranty(syncCode1C: string, value: string) {
        var test = /^\d+$/.test(value);
        if (test === false) {
            var product = this.products.filter((p) => { return p.syncCode1C === syncCode1C })[0];
            product.warranty = parseInt(value.substring(0, value.length - 1));
        }
    }

    public changeAllTargetAudience = (obj: any, selected: Array<string>) => {
        var products = this.getSelectedProducts();
        if (selected && selected.length !== 0) {
            for (var product of products) {
                product.targetTypes = JSON.parse(JSON.stringify(selected));
            };
        }
    };

    public changeAllStyles = (obj: any, selected: Array<string>) => {
        var products = this.getSelectedProducts();
        if (selected && selected.length !== 0) {
            for (var product of products) {
                product.styles = JSON.parse(JSON.stringify(selected));
            };
        }
    }

    public next(): void {
        if (this.page < this.paging.pageCount) {
            this.page = this.page + 1;
            this.getProducts(this.generateQuery());
        }
    }

    public prev(): void {
        if (this.page > 1) {
            this.page = this.page - 1;
            this.getProducts(this.generateQuery());
        }
    }

    public getFilters(): any {
        var queryForFurniture: Dto.FurnitureType.GetFurnitureTypesQuery = { integrationCollectionId: this.collectionId }
        this.importApiService.getFurnitureTypes(queryForFurniture)
            .then((data: Array<Dto.FurnitureType.DtoFurnitureTypeItem>) => {
                this.furnitureType = data.map((item: any) => { return { id: item.syncCode1C, name: item.syncCode1C } });
                this.furnitureType.push({ id: "", name: "Все" });
            });

        var queryForSynCode: Dto.SyncCode.GetSyncCode1CsQuery = { integrationCollectionId: this.collectionId }
        this.importApiService.getSyncCodes(queryForSynCode)
            .then((data: Array<Dto.SyncCode.DtoSyncCode1CItem>) => {
                this.syncCode1C = data.map((item: any) => { return { id: item.name, name: item.name } });
                this.syncCode1C.push({ id: "", name: "Все" });
            });

        this.filterService.getFrameColors()
            .then((data: Array<Dto.Filter.IDtoFrameColorItem>) => {
                this.frameColor = data.map((item: any) => { return { id: item.syncCode1C, name: item.syncCode1C } });
                this.frameColor.push({ id: "", name: "Все" });
            });

        this.filterService.getFacings()
            .then((data: Array<Dto.Filter.IDtoFacingItem>) => {
                this.facing = data.map((item: any) => { return { id: item.syncCode1C, name: item.syncCode1C } });
                this.facing.push({ id: "", name: "Все" });
            });

        this.filterService.getFacingColors()
            .then((data: Array<Dto.Filter.IDtoFacingColorItem>) => {
                this.facingColor = data.map((item: any) => { return { id: item.syncCode1C, name: item.syncCode1C } });
                this.facingColor.push({ id: "", name: "Все" });
            });

        this.importApiService.getProductPartMarkers()
            .then((data: any) => {
                this.productPartMarkerList = data.data;
            });
    };

    public openDownloadingWindow(product: Dto.Import.IProductValidationImport): void {
        this.productDownload = product;
        this.openModal = true;
    };

    public closeModal(): void {
        this.openModal = false;
        this.productDownload = null;
    };

    public filterProducts = (obj: any, selected: string): void => {
        if (obj.type === "furnitureType")
            this.chosenFurnitureType = selected;
        else if (obj.type === "syncCode1C")
            this.chosenSyncCode1C = selected;
        else if (obj.type === "frameColor")
            this.chosenFrameColor = selected;
        else if (obj.type === "facing")
            this.chosenFacing = selected;
        else if (obj.type === "facingColor")
            this.chosenFacingColor = selected;
        /*вернуться на первую страницу*/
        this.page = 1;
        this.getProducts(this.generateQuery());
    };

    public publish() {
        var selectedProducts = this.getSelectedProducts();
        if (selectedProducts && selectedProducts.length > 0) {
            var synccodes = selectedProducts.map((i: Dto.Import.IProductValidationImport) => { return i.syncCode1C });

            /* проверка возможности публикации продукта, и публикация после валидной проверки */

            this.productImportService
                .validationPublish(synccodes)
                .then((model: Dto.Import.IImportError) => {
                    if (!model.isValid) {
                        //TODO: need to show validation errors on the view, not in toastr
                        model.errors.forEach((error: string) => {
                            this.messageService.uiError(error);
                        });
                        throw new Error("Невалидные данные");
                    }
                })
                .then(() => {
                    selectedProducts.forEach((item: any) => { item.integrationStatus = "Published" });
                    return this.importApiService.updateProducts(selectedProducts);
                })
                .then(() => {
                    this.messageService.success("Продукты успешно опубликованы");
                    this.getProducts(this.generateQuery());
                })
                .catch((error: Error) => {
                    this.messageService.error("Ошибка при сохранении", error.message);
                });
        }
        else {
            this.messageService.info("Пожалуйста выберите продукты");
        }
    };

    /*  сохранение и перевод в ченовики */
    public save() {
        var selectedProducts = this.getSelectedProducts();
        if (selectedProducts && selectedProducts.length > 0) {
            selectedProducts.forEach((item: any) => { item.integrationStatus = "Template" });
            this.importApiService.updateProducts(selectedProducts)
                .then((data: any) => {
                    this.messageService.success("Продукты успешно сохранены");
                    this.getProducts(this.generateQuery());
                })
                .catch((error: Error) => {
                    this.messageService.error("Ошибка при сохранении", error.message);
                });
        }
        else {
            this.messageService.info("Пожалуйста выберите продукты");
        }
    };
};