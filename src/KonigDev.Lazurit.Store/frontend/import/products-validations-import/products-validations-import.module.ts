﻿import * as angular from "angular";

import controller from "./products-validations-import.controller";
/* modules */
import * as importServiceModule from "../products-import-service/products-import-service.module";
import * as flterModule from "../../filter/filter.module";
import * as targetAudienceModule from "../../target-audience/target-audience.module";
import * as stylesModule from "../../style/style.module";
import * as propertiesModule from "../../property/property.module";
import * as selectSingleChosenModule from "../../common/components/select-single-chosen/select-single-chosen.module";
import * as selectMultiChosenModule from "../../common/components/select-multi-chosen/select-multi-chosen.module";
import * as commonModule from "../../common/common.module";
import * as imageModule from "../../image/image.module";


/*constants*/

export const name = "kd.products.import";

const controllerName = "productsValidationsImportController";
const componentName = "kdProductsValidationsImport";

angular
    .module(name,
    [
        flterModule.name,
        targetAudienceModule.name,
        stylesModule.name,
        propertiesModule.name,
        selectSingleChosenModule.name,
        selectMultiChosenModule.name,
        commonModule.name,
        importServiceModule.name,
        imageModule.name
    ])                        
    .controller(controllerName,
    [
        importServiceModule.apiServiceName,
        importServiceModule.importProductServiceName,
        targetAudienceModule.serviceName,
        stylesModule.serviceName,
        propertiesModule.serviceName,
        flterModule.serviceName,
        commonModule.messageServiceName,
        imageModule.serviceName,
        controller
    ])
    .component(componentName,
    {
        bindings: {
            collectionId: '@'
        },
        controller: controllerName,
        template: require("./products-validations-import.view.html")
    });