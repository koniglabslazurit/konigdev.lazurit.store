﻿export default class ProductsImagesImportController {
    public product: any;
    public furnitureType: string;
    public syncCode: string;

    /* todo сделать работу с изображениями директивой! */

    constructor(private importService: Product.Import.ApiService, private messageService: Common.Message.IMessage) {
    }
      
    public loadProductImage(product: any, imageType: string): void {
        var me = this;
        var inputElement = (<HTMLInputElement>document.getElementById("file_" + product.productId + "_" + imageType));
        if (!inputElement.files || inputElement.files.length === 0) {
            me.messageService.error("Не выбран файл!", "Didn't choose file");
            return;
        };
        var modelExtended: Dto.Image.IUploadFileModelExtended = {
            abstractProduct: product,
            imageType: imageType
        };
        var file = inputElement.files[0];
        if (imageType === "WithoutInterior" || imageType === "InInterior") {
            this.validateImage(file, modelExtended, 500, 600, 800);
        }
        else {
            /* для картинок 1200*1200 увеличиваем объем до 1Мб */
            this.validateImage(file, modelExtended, 1024, 1200, 1200);            
        }                     
    };

    /* почему тут и валидация и сразу загрузка идет? */
    private validateImage(file: any, modelExtended: Dto.Image.IUploadFileModelExtended, extendSize: number, extendWidth: number, extendHeight: number): void {
        /*валидация размера*/
        if (file.size > extendSize*1000) {
            this.messageService.error("Файл не должен превышать " + extendSize + "Кб", "Файл не должен превышать " + extendSize + "Кб");
            return;            
        }
        var img = new Image();
        img.src = window.URL.createObjectURL(file);
        var me = this;
        img.onload = function () {
            var width = img.naturalWidth;
            var height = img.naturalHeight;
            window.URL.revokeObjectURL(img.src);
            var validator = me.validateDimentions(extendWidth, extendHeight, width, height);
            if (validator.success === false) {
                me.messageService.error(validator.message, validator.message);
                return;
            }
            else {
                me.loadingFile(file, modelExtended);
            }
        }        
    };

    private loadingFile(file, modelExtended: Dto.Image.IUploadFileModelExtended) {
        var me = this;
        var formData = new FormData();
        formData.append("file", file);
        console.log(file);
        var model: Dto.Image.IUploadFileModel = {
            objectId: modelExtended.abstractProduct.productId,
            type: modelExtended.imageType
        };
        this.importService.uploadFile(model, formData)
            .then((response: any) => {
                if (response === 200) {
                    me.messageService.success("Файл загружен");
                    me.refreshImage(model);
                    me.updateProductImagesInfoFields(modelExtended);
                } else {
                    me.messageService.error(response.Message, response.Message);
                }
            });
    }; 

    private updateProductImagesInfoFields(modelExtended: Dto.Image.IUploadFileModelExtended): void {
        if (modelExtended.imageType === "WithoutInterior") {
            modelExtended.abstractProduct.isImageWithoutInteriorExist = true;
        }
        else if (modelExtended.imageType === "InInterior") {
            modelExtended.abstractProduct.isImageInInteriorExist = true;
        }
        else if (modelExtended.imageType === "SliderInInterior") {
            modelExtended.abstractProduct.isImageSliderInInteriorExist = true;
        }
        else if (modelExtended.imageType === "SliderMiddle") {
            modelExtended.abstractProduct.isImageSliderMiddleExist = true;
        }
        else if (modelExtended.imageType === "SliderOpen") {
            modelExtended.abstractProduct.isImageSliderOpenExist = true;
        }
    }

    private validateDimentions(extendWidth: number, extendHeight: number, actualWidth: number, actualHeight: number): Dto.Validate.IResponse {
        if (actualWidth < extendWidth || actualHeight < extendHeight) {
            return { success: false, message: "Файл не должен быть меньше " + extendWidth + "x" + extendHeight};
        }
        else {
            var extendRatio = extendWidth / extendHeight;
            var actualRatio = actualWidth / actualHeight;
            if (actualRatio !== extendRatio) {
                return { success: false, message: "Файл не пропорционален размеру " + extendWidth + "x" + extendHeight };
            }
        }
        return { success: true, message: "" };       
    };

    private refreshImage(model: Dto.Image.IUploadFileModel): void {
        var image = (<HTMLInputElement>document.getElementById("image_" + model.objectId + "_" + model.type));
        image.setAttribute("src", image.src + "/");
    };
}                                                                    