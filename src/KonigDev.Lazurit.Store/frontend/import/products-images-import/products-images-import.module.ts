﻿import * as angular from "angular";

import controller from "./products-images-import.controller";

/* modules */
import * as importServiceModule from "../products-import-service/products-import-service.module";
import * as commonModule from "../../common/common.module";

/*constants*/

export const name = "kd.products.images.import";

const controllerName = "productsImagesImportController";
const componentName = "kdProductsImagesImport";

angular
    .module(name,
    [
        commonModule.name,
        importServiceModule.name
    ])
    .controller(controllerName,
    [
        importServiceModule.apiServiceName,
        commonModule.messageServiceName,
        controller
    ])
    .component(componentName,
    {
        bindings: {
            product: "=",
            furnitureType: "@",
            syncCode: "@"
        },
        controller: controllerName,
        template: require("./products-images-import.view.html")
    });