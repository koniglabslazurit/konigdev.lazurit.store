﻿import * as angular from "angular";

import controller from "./filters-colors.controller";

/* modules */
import * as commonModule from "../../common/common.module";
import * as imageModule from "../../image/image.module";
import * as filterColorsModule from "../../filter/filter.module";

/*constants*/

export const name = "kd.filterscolors.images.import";

const controllerName = "filtersColorsImportController";
const componentName = "kdFilterColorsImport";

angular
    .module(name,
    [
        commonModule.name,
        filterColorsModule.name,
        imageModule.name
    ])
    .controller(controllerName,
    [        
        commonModule.messageServiceName,
        filterColorsModule.serviceName,
        imageModule.serviceName,
        controller
    ])
    .component(componentName,
    {
        bindings: {},
        controller: controllerName,
        template: require("./filters-colors.view.html")
    });