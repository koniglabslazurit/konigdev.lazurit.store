﻿export default class FiltersColorsImportController {

    public frameColors: Array<Dto.Filter.DtoFilterColorContentItem>;
    public facings: Array<Dto.Filter.DtoFilterColorContentItem>;
    public facingColors: Array<Dto.Filter.DtoFilterColorContentItem>;
    public activeTabNumber: number;

    private _extendSize: number;
    private _extendWidth: number;
    private _extendHeight: number;
    private _typeImage: string;

    /* todo сделать работу с изображениями директивой! */

    constructor(private messageService: Common.Message.IMessage,
        private filterColorsApiService: Filter.Services.IFilterApiService,
        private imageApiService: any) {

        this.activeTabNumber = 1;

        this._extendSize = 500;
        this._extendHeight = 50;
        this._extendWidth = 50;
        this._typeImage = 'TileMin';

        this.init();
    }

    private init() {

        this.filterColorsApiService.getFrameColors()
            .then((response: any) => {
                this.frameColors = response;
            })
            .catch((error: any) => {
                console.log(error);
            });

        this.filterColorsApiService.getFacings()
            .then((response: any) => {
                this.facings = response;
            })
            .catch((error: any) => {
                console.log(error);
            });

        this.filterColorsApiService.getFacingColors()
            .then((response: any) => {
                this.facingColors = response;
            })
            .catch((error: any) => {
                console.log(error);
            });
    };

    public loadImage(id: string) {
        /* переделатть получение файла на директиву или компоненту */
        var inputElement = (<HTMLInputElement>document.getElementById("file_" + id));
        if (!inputElement.files || inputElement.files.length === 0) {
            this.messageService.error("Не выбран файл!", "Didn't choose file");
            return;
        };
        var file = inputElement.files[0];

        /* разнести по сервисам, функциям, посмотреть как валидирвоать размеры, чтоб был отдельный сервис с каллбэком */

        /* проверки */
        if (file.size > this._extendSize * 1000) {
            this.messageService.error("Файл не должен превышать " + this._extendSize + "Кб", "Файл не должен превышать " + this._extendSize + "Кб");
            return;
        }

        var img = new Image();
        img.src = window.URL.createObjectURL(file);
        var _this = this;
        img.onload = function () {
            var width = img.naturalWidth;
            var height = img.naturalHeight;
            window.URL.revokeObjectURL(img.src);
            var validator = _this.validateSizes(width, height);
            if (validator.success === false) {
                _this.messageService.error(validator.message, validator.message);
                return;
            }
            else {
                _this.upLoadFile(file, id);
            }
        };

        img.onerror = () => {
            console.log("Error loading image as src");
        }
    };

    private validateSizes(width: number, height: number): any {
        var result = { success: true, message: "" }

        if (width !== this._extendWidth || height !== this._extendHeight) {
            result = { success: false, message: "Изображение должно быть 50x50" }
        }

        return result;
    };

    private upLoadFile(file: File, id: string) {
        var formData = new FormData();
        formData.append("file", file);
        var model: Dto.Image.IUploadFileModel = { objectId: id, type: this._typeImage };

        this.imageApiService.uploadImage(model, formData)
            .then((response: any) => {
                if (response === 200) {
                    this.refreshImage(model);
                    this.messageService.success("Файл загружен");
                } else {
                    this.messageService.error(response.Message, response.Message);
                }
            });
    };


    private refreshImage(model: Dto.Image.IUploadFileModel) {
        var item = this.frameColors.filter((item: any) => { return item.id === model.objectId })[0];
        if (!item)
            item = this.facings.filter((item: any) => { return item.id === model.objectId })[0];
        if (!item)
            item = this.facingColors.filter((item: any) => { return item.id === model.objectId })[0];

        if (item) {
            /* TODO обновление сделать не через елемент? */
            var image = <HTMLInputElement>document.getElementById("image_" + model.objectId);
            image.setAttribute("src", image.src + "/");
        }
    };

    public changeActiveTabNumber(tab: number) {
        this.activeTabNumber = tab;
    }
}                                                                