﻿namespace Product.Import {
    export interface ApiService {
        addIntegrationProductsContent(model: any): angular.IPromise<any>;
        saveIntegrationCoordinates(model: Dto.ImportCollection.ICoordinatesModel): angular.IPromise<any>;
        uploadFile(model: Dto.Image.IUploadFileModel, formData: any): angular.IPromise<any>;
        getCollection(request: any): angular.IPromise<any>;
        getCollections(query: Dto.Import.ICollectionsQuery): angular.IPromise<Array<Dto.Import.ICollections>>;
        getValidationProducts(query: Dto.Import.IProductImportQuery): angular.IPromise<Dto.Import.IProductValidationImportResponse>;
        updateProducts(products: Array<any>): angular.IPromise<any>;
        getPublishedProducts(query: Dto.Import.IPublishedProductsQuery): angular.IPromise<Dto.Import.IPublishedProductsResponse>;
        getSyncCodes(data: Dto.SyncCode.GetSyncCode1CsQuery): angular.IPromise<Array<Dto.SyncCode.DtoSyncCode1CItem>>;
        getFurnitureTypes(request: Dto.FurnitureType.GetFurnitureTypesQuery): angular.IPromise<Array<Dto.FurnitureType.DtoFurnitureTypeItem>>;
        getProductPartMarkers(): angular.IPromise<any>;
        makeDefault(model: any): angular.IPromise<any>;
        validatePublish(request: Dto.Import.IValidationRequest): angular.IPromise<Dto.Import.IValidationResponse>;
        validateUnPublish(request: any): angular.IPromise<Dto.Import.IValidationResponse>;
        unPublish(request: any): angular.IPromise<any>;
        validatePublishCollectionVariant(request: any): angular.IPromise<any>;
        publishVariantCollection(request: any): angular.IPromise<any>;
    }

    export interface Service {
        validationPublish(productsCodes: Array<string>): angular.IPromise<any>;
    }
}