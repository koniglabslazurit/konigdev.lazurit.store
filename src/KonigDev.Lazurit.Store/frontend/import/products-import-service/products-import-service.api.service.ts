﻿export default class ImportProductsApiService implements Product.Import.ApiService {
    //TODO: разнести этот ужс
    private getCollectionsUrl: string = "/api/import/collections";
    private urlImportProducts: string = "/api/import/product";
    private getImportProductsUrl: string = "/api/import/product";
    private updateProductsUrl: string = "/api/import/products";
    private getPublishedProductsUrl: string = "/api/import/published-products";
    private getSyncCodesUrl: string = "/api/import/sync-code";
    private getFurnitureTypesUrl: string = '/api/import/furniture-types';
    private getProductPartMarkersUrl: string = '/api/import/product-markers';
    private uploadUrl: string = "/api/images/upload";
    private getCollectionUrl: string = "/api/import/collection";
    private addIntegrationProductsContentUrl: string = "/api/productContent/addIntegration";
    private saveIntegrationCoordinatesUrl: string = "/api/productContent/updateIntegration";
    private makeDefaultUrl: string = "/api/import/make-default-variant";
    private validationPublishUrl: string = "/api/import/valid";
    private validateUnPublishUrl: string = "/api/import/validunpublish";
    private unPublishUrl: string = "/api/import/unpublish";
    private validatePublishCollectionVariantUrl: string = "/api/import/collection-variant/valid";
    private publishVariantCollectionUrl: string = "/api/import/collection-variant";

    constructor(public $http: angular.IHttpService) { }

    public addIntegrationProductsContent(model: any): angular.IPromise<any> {
        return this.$http({
            method: "POST",
            url: this.addIntegrationProductsContentUrl,
            data: model
        }).then((response: any) => {
            return response.data;
        });
    }

    public saveIntegrationCoordinates(model: Dto.ImportCollection.ICoordinatesModel): angular.IPromise<any> {
        return this.$http({
            method: "PUT",
            url: this.saveIntegrationCoordinatesUrl,
            data: model
        }).then((response: any) => {
            return response.data;
        });
    }

    public uploadFile(model: Dto.Image.IUploadFileModel, formData: any): angular.IPromise<any> {
        return this.$http({
            method: "POST",
            url: this.uploadUrl,
            params: model,
            data: formData,
            headers: {
                'Content-Type': undefined
            }
        }).then((response: any) => {
            return response.status;
        });
    }

    public getCollection(request: any): angular.IPromise<any> {
        return this.$http.get(this.getCollectionUrl, { params: request })
            .then((response: any) => {
                return response.data;
            });
    }

    public getCollections(query: Dto.Import.ICollectionsQuery): angular.IPromise<Array<Dto.Import.ICollections>> {
        return this.$http.get(this.getCollectionsUrl, { params: query })
            .then((response: any) => {
                return response.data;
            });
    }

    public getValidationProducts(query: Dto.Import.IProductImportQuery): angular.IPromise<Dto.Import.IProductValidationImportResponse> {
        return this.$http.get(this.getImportProductsUrl, { params: query })
            .then((response: any) => {
                return response.data;
            });
    }

    public updateProducts(products: Array<Dto.Import.IProductImportResponse>): angular.IPromise<any> {
        return this.$http
            .post(this.updateProductsUrl, products)
            .then((response: any) => {
                return response;
            });
    }

    public getPublishedProducts(query: Dto.Import.IPublishedProductsQuery): angular.IPromise<Dto.Import.IPublishedProductsResponse> {
        return this.$http
            .get(this.getPublishedProductsUrl, { params: query })
            .then((response: any) => {
                return response.data;
            });
    }

    public getSyncCodes(data: Dto.SyncCode.GetSyncCode1CsQuery): angular.IPromise<Array<Dto.SyncCode.DtoSyncCode1CItem>> {
        return this.$http
            .get(this.getSyncCodesUrl, { params: data })
            .then((response: angular.IHttpPromiseCallbackArg<Array<Dto.SyncCode.DtoSyncCode1CItem>>) => {
                return response.data;
            });
    }

    public getFurnitureTypes(request: Dto.FurnitureType.GetFurnitureTypesQuery): angular.IPromise<Array<Dto.FurnitureType.DtoFurnitureTypeItem>> {
        return this.$http
            .get(this.getFurnitureTypesUrl, { params: request })
            .then((response: angular.IHttpPromiseCallbackArg<Array<Dto.FurnitureType.DtoFurnitureTypeItem>>) => {
                return response.data;
            });
    }

    public getProductPartMarkers(): angular.IPromise<any> {
        return this.$http
            .get(this.getProductPartMarkersUrl)
            .then((response: any) => {
                return response;
            });
    }

    public makeDefault(model: any): angular.IPromise<any> {
        return this.$http
            .post(this.makeDefaultUrl, model)
            .then((response: any) => {
                return response;
            });
    }

    public validatePublish(request: Dto.Import.IValidationRequest): angular.IPromise<Dto.Import.IValidationResponse> {
        return this.$http
            .get(this.validationPublishUrl, { params: request })
            .then((response: Dto.Import.IValidationResponse) => {
                return response;
            });
    }

    public validateUnPublish(request: any): angular.IPromise<Dto.Import.IValidationResponse> {
        return this.$http
            .get(this.validateUnPublishUrl, { params: request })
            .then((response: any) => {
                return response.data;
            });
    }

    public unPublish(request: any): angular.IPromise<any> {
        return this.$http
            .post(this.unPublishUrl, request)
            .then((response: any) => {
                return response;
            });
    }

    public validatePublishCollectionVariant(request: any): angular.IPromise<any> {
        return this.$http
            .get(this.validatePublishCollectionVariantUrl, { params: request })
            .then((response: any) => response.data);
    }

    public publishVariantCollection(request: any): angular.IPromise<any> {
        return this.$http
            .post(this.publishVariantCollectionUrl, request)
            .then((response: any) => response);
    }
};