﻿import * as angular from "angular";

/* modules */
import * as commonServiceModule from "../../common/common.module";

/**/
import apiService from "./products-import-service.api.service";
import service from "./products-import-service.service";

export const name = "kd.import.products.serivce";
export const apiServiceName = "importProductsApiService";
export const importProductServiceName = "importProductsService";

angular
    .module(name,
    [
        commonServiceModule.name
    ]).service(apiServiceName, [
        "$http",
        apiService
    ]).service(importProductServiceName, [
        apiServiceName,
        commonServiceModule.messageServiceName,
        '$q',
        service
    ]);