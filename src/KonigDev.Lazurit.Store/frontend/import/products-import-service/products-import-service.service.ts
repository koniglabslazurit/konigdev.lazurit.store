﻿export default class ImportProductsService implements Product.Import.Service {

    public constructor(private apiService: Product.Import.ApiService, private messageService: Common.Message.IMessage, private $q: angular.IQService) {

    }

    public validationPublish(productsCodes: Array<string>): angular.IPromise<Dto.Import.IImportError> {
        var arrayRequestes = [];
        productsCodes.forEach((i: string) => {
            arrayRequestes.push(this.apiService.validatePublish({ syncCode1C: i }));
        });

        return this.$q.all(arrayRequestes)
            .then((response: Array<any>) => {
                var valid = true;
                const errors = [];
                response.forEach((i: any) => {
                    if (i.data && i.data.isValid === false) {
                        valid = false;
                        var message = "Невозможно опубликовать продукт '" + i.config.params.syncCode1C + "': ";
                        i.data.errors.forEach((e: string) => {
                            message = message + e + ";"
                        });
                        errors.push(message);
                    }
                });
                return { isValid: valid, errors: errors };
            });
    };
}