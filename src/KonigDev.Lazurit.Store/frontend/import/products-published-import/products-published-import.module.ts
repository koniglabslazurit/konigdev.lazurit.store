﻿import * as angular from "angular";

import controller from "./products-published-import.controller";

/* modules */
import * as importServiceModule from "../products-import-service/products-import-service.module";
import * as flterModule from "../../filter/filter.module";
import * as targetAudienceModule from "../../target-audience/target-audience.module";
import * as stylesModule from "../../style/style.module";
import * as propertiesModule from "../../property/property.module";
import * as dropdownSingleModule from "../../common/components/dropdown-single/dropdown-single.module";
import * as commonModule from "../../common/common.module";
import * as imageModule from "../../image/image.module";


/*constants*/

export const name = "kd.products.published.import";

const controllerName = "productsPublishedImportController";
const componentName = "kdProductsPublishedImport";

angular
    .module(name,
    [
        flterModule.name,
        targetAudienceModule.name,
        stylesModule.name,
        propertiesModule.name,
        dropdownSingleModule.name,
        importServiceModule.name,
        commonModule.name,
        imageModule.name
    ])
    .controller(controllerName,
    [
        importServiceModule.apiServiceName,
        targetAudienceModule.serviceName,
        stylesModule.serviceName,
        propertiesModule.serviceName,
        flterModule.serviceName,
        commonModule.messageServiceName,
        imageModule.serviceName,
        controller
    ])
    .component(componentName,
    {
        bindings: {
        },
        controller: controllerName,
        template: require("./products-published-import.view.html")
    });