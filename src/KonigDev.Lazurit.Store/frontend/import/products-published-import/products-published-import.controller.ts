﻿export default class ProductsPublishedController {
    public publishedProducts: Array<Dto.Import.IPublishedProduct>;
    public vendorCode: string;
    public activeTabNumber: number = 1;
    //from api
    public backlightList: Array<any>;
    public materialList: Array<any>;
    public leftRightList: Array<any>;
    public mechanismList: Array<any>;
    public furnitureFormList: Array<any>;
    public productPartMarkerList: Array<any>;
    //from api
    public targetAudienceList: Array<Dto.TargetAudience.DtoTargetAudienceItem>;
    public stylesList: Array<Dto.Style.DtoStyleItem>;
    //paging
    public paging: Dto.Paging.IPaging;
    public pageCountlList: Array<any>;
    //for download images
    public productDownload: Dto.Import.IPublishedProductsResponse;
    public openModal: boolean = false;


    constructor(private importService: Product.Import.ApiService,
        private targetAudienceService: TargetAudience.Services.ITargetAudienceApiService,
        private stylesService: Style.Services.IStyleApiService,
        private propertiesService: Property.Services.IPropertyApiService,
        private filterService: Filter.Services.IFilterApiService,
        private messageService: Common.Message.IMessage,
        private imageService: Product.Import.IImageService) {
        this.init();
    }

    private init(): void {
        this.paging = {
            //must be in request for products
            itemsOnPage: 10,
            page: 1,
            //calculatable after server response
            totalCount: 0,
            pageCount: 0
        };
        this.getProducts(1);
        this.getProperties();
        this.getStyles();
        this.getTargetAudiences();
        this.pageCountlList = [{ value: 10, name: "10" }, { value: "20", name: "20" }, { value: 50, name: "50" }, { value: 100, name: "100" }, { value: 200, name: "200" }, { value: 500, name: "500" }, { value: 0, name: "все" }];
    };

    public getProducts(page: number): void {
        var query: Dto.Import.IPublishedProductsQuery = {
            itemsOnPage: this.paging.itemsOnPage,
            page: page,
            vendorCode: this.vendorCode
        }
        this.importService.getPublishedProducts(query)
            .then((data: Dto.Import.IPublishedProductsResponse) => {
                //paging calculation
                this.paging.page = page;
                this.paging.totalCount = data.totalCount;
                if (this.paging.itemsOnPage !== 0) {
                    this.paging.pageCount = Math.ceil(this.paging.totalCount / this.paging.itemsOnPage);
                }
                else {
                    this.paging.pageCount = 1;
                }
                //mapping products
                this.publishedProducts = data.items;
                //mapping bool values in accordance to existence of some image types for integrationProduct
                this.publishedProducts
                    .map(p => p.products)
                    .reduce((a, b) => a.concat(b))
                    .forEach((product: Dto.Import.IProductImport) => {
                        this.imageService.getAvialableImages(product.productId)
                            .then((imagesList: any) => {
                                if (imagesList.length > 0) {
                                    product.isImageWithoutInteriorExist = imagesList.some(imageInfo => imageInfo.type === "WithoutInterior");
                                    product.isImageInInteriorExist = imagesList.some(imageInfo => imageInfo.type === "InInterior");
                                    product.isImageSliderInInteriorExist = imagesList.some(imageInfo => imageInfo.type === "SliderInInterior");
                                    product.isImageSliderMiddleExist = imagesList.some(imageInfo => imageInfo.type === "SliderMiddle");
                                    product.isImageSliderOpenExist = imagesList.some(imageInfo => imageInfo.type === "SliderOpen");
                                }
                            })
                            .catch((error: any) => {
                                console.log(error);
                            });
                    });

                });
    };

    public checkIsAllImagesExist(product: Dto.Import.IProductImport): boolean {
        if (product.isImageWithoutInteriorExist && product.isImageInInteriorExist && product.isImageSliderInInteriorExist && product.isImageSliderMiddleExist && product.isImageSliderOpenExist) {
            return true;
        }
        else {
            return false;
        }
    }

    public saveChanges(products: Array<Dto.Import.IProductImport>): any {
        this.importService.updateProducts(products)
            .then((data: any) => {
                this.messageService.success("Изменения опубликованы");
            })
            .catch((error: any) => {
                this.messageService.uiError("Произошла ошбика");
                console.log(error);
            });
    }

    public changeDefaultProduct(vendorCode: string, productId: string): void {
        var publishedProduct = this.publishedProducts.filter((pp) => { return pp.vendorCode === vendorCode })[0];
        var product = publishedProduct.products.filter((p) => { return p.productId === productId })[0];
        product.isDefault = true;
        var notDefaultProducts = publishedProduct.products.filter((p) => { return p.productId !== productId });
        notDefaultProducts.forEach((n) => { n.isDefault = false });
    }

    private getTargetAudiences(): void {
        this.targetAudienceService.getTargetAudiences()
            .then((data) => {
                this.targetAudienceList = data.map((item: any) => {
                    return {
                        id: item.syncCode1C,
                        name: item.syncCode1C
                    }
                });
            })
            .catch((error: any) => {
                console.log(error);
            });
    }

    private getStyles(): void {
        this.stylesService.getStyles()
            .then((data) => {
                this.stylesList = data.map((item) => {
                    return {
                        id: item.syncCode1C,
                        name: item.syncCode1C
                    }
                });
            })
            .catch((error: any) => {
                console.log(error);
            });
    }

    private getProperties(): void {
        var request = { propertyType: '' };
        this.propertiesService.getProperties(request)
            .then((data: Array<any>) => {

                this.backlightList = data
                    .filter((item: any) => { return item.syncCode1C === 'Backlight' })
                    .map((item: any) => {
                        return { id: item.value, name: item.value }
                    });

                this.materialList = data
                    .filter((item: any) => { return item.syncCode1C === 'Material' })
                    .map((item: any) => {
                        return { id: item.value, name: item.value }
                    });

                this.leftRightList = data
                    .filter((item: any) => { return item.syncCode1C === 'LeftRight' })
                    .map((item: any) => {
                        return { id: item.value, name: item.value }
                    });

                this.mechanismList = data
                    .filter((item: any) => { return item.syncCode1C === 'Mechanism' })
                    .map((item: any) => {
                        return { id: item.value, name: item.value }
                    });

                this.furnitureFormList = data
                    .filter((item: any) => { return item.syncCode1C === 'FurnitureForm' })
                    .map((item: any) => {
                        return { id: item.value, name: item.value }
                    });
            });

        this.importService.getProductPartMarkers()
            .then((data: any) => {
                this.productPartMarkerList = data.data;
            });
    }

    public changeActiveTabNumber(activeTabNumber: number): void {
        this.activeTabNumber = activeTabNumber;
    }

    //paging
    public next(): void {
        this.paging.page++;
        this.getProducts(this.paging.page);
    };

    public prev(): void {
        this.paging.page--;
        this.getProducts(this.paging.page);
    };

    public openDownloadingWindow(product: Dto.Import.IPublishedProductsResponse): void {
        this.productDownload = product;
        this.openModal = true;
    };

    public closeModal(): void {
        this.openModal = false;
        this.productDownload = null;
    };

    public unPublish(product: Dto.Import.IProductImport) {
        this.importService.validateUnPublish({ SyncCode1C: product.syncCode1C })
            .then((response: Dto.Import.IValidationResponse) => {
                /* проверка валидации на возможность снятия продукта с публикации */
                if (response.isValid === true) {
                    return response;
                } else {
                    var message = "";
                    if (response.errors) {
                        response.errors.forEach((i: string) => {
                            message = message + i + ';';
                        })
                    }
                    this.messageService.uiError(message !== "" ? message : "Произошла ошибка при снятии с публикации");
                    throw new Error("Невалидные данные");
                }
            })
            .then((response: any) => {
                /* снятие с публикации */
                return this.importService.unPublish({ SyncCode1C: product.syncCode1C });
            })
            .then((response: any) => {
                this.messageService.success("Товар успешно снят с публикации");
                this.getProducts(1);
            })
            .catch((response: any) => {
                console.log(response);
                this.messageService.uiError("Произошла ошибка.");
            });
    }
}