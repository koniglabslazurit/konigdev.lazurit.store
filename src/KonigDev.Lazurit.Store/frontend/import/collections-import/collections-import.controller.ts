﻿export default class CollectionsImportController {
    public collections: Array<Dto.Import.ICollections>;
    public paging: Dto.Paging.IPaging;

    constructor(
        public importApiService: Product.Import.ApiService,
        private $timeout: angular.ITimeoutService
    ) {
        this.init();
    }

    private init(): void {
        this.paging = {
            itemsOnPage: 10,
            page: 1,
            totalCount: 10,
            pageCount: 1
        };
        this.getCollections(this.generateQuery());
    }

    private generateQuery(): Dto.Import.ICollectionsQuery {
        return { itemsOnPage: this.paging.itemsOnPage, page: this.paging.page };
    }

    private getCollections(query: Dto.Import.ICollectionsQuery): void {
        this.importApiService.getCollections(query)
            .then((data: any) => {
                this.paging.totalCount = data.totalCount;
                if (this.paging.itemsOnPage !== 0) {
                    this.paging.pageCount = Math.ceil(this.paging.totalCount / this.paging.itemsOnPage);
                }
                else {
                    this.paging.pageCount = 1;
                }

                this.collections = data.items;
            });
    }

    public next(): void {
        if (this.paging.page < this.paging.pageCount) {
            this.paging.page++;
            this.getCollections(this.generateQuery());
        }
    }

    public prev(): void {
        if (this.paging.page > 1) {
            this.paging.page--;
            this.getCollections(this.generateQuery());
        }
    }
}