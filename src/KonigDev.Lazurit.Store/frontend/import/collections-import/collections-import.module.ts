﻿import * as angular from "angular";

/* modules */
import * as importServiceModule from "../products-import-service/products-import-service.module";

import controller from "./collections-import.controller";

/*constants*/
export const name = "kd.collections.import";
const controllerName = "collectionsImportController";
const componentName = "kdCollectionsImport";

angular
    .module(name,
    [
        importServiceModule.name
    ])
    .controller(controllerName,
    [
        importServiceModule.apiServiceName,
        "$timeout",
        controller
    ])
    .component(componentName,
    {
        bindings: {},
        controller: controllerName,
        template: require("./collections-import.view.html")
    });