﻿export default class ImportCollectionsVariants {
    public collectionId: string;
    public collection: Dto.ImportCollection.IIntegrationCollection;
    private productUrl: string = "/api/images/productinvariant/:objectId/InInterior";
    private editingProduct: Dto.ImportCollection.IIntegrationProduct;
    private isBindingAreas: boolean = false;

    constructor(public $scope: any,
        private importApiService: Product.Import.ApiService,
        private collectionApiService: any,
        private collectionService: any,
        private messageService: Common.Message.IMessage
    ) {
        this.init();
    }

    private init(): void {
        var me = this;
        me.getVariants(me.collectionId);
        this.$scope.selector = {};
        this.$scope.drawer = [];
        this.$scope.cropRect = function () {
            me.$scope.cropResult = me.$scope.selector.crop();
        };

    }

    public getVariants(collectionId: string): void {

        this.importApiService.getCollection({ collectionId: collectionId })
            .then((response: any) => {
                this.collection = response;
                var i = 1;
                this.collection.integrationVariants = response.integrationVariants.map((v) => {
                    v.number = i;
                    i = i + 1;
                    return v;
                });
                this.drawProducts();
            })
            .catch((error: any) => {
                console.log(error);
            });
    }

    public drawProducts(): void {
        this.collection.integrationVariants.forEach((v: Dto.ImportCollection.IIntegrationCollectionsVariant) => {
            if (v.integrationProducts) {
                v.integrationProducts.forEach((p: Dto.ImportCollection.IIntegrationProduct) => {
                    if (p.objectId) {
                        p.src = this.productUrl.replace(":objectId", p.objectId);
                    };
                });
            }
        });
    }

    public loadVariantImage(variantId: string, imageType: string): void {
        this.uploadVariantFile(variantId, imageType);
    }

    private uploadVariantFile(objectId: string, imageType: string): void {
        var me = this;
        var inputElement = (<HTMLInputElement>document.getElementById("file_" + objectId + "_" + imageType));
        if (!inputElement.files || inputElement.files.length === 0) {
            me.messageService.error("Не выбран файл!", "Didn't choose file");
            return;
        }
        var file = inputElement.files[0];
        var model: Dto.Image.IUploadFileModel = {
            objectId: objectId,
            type: imageType
        };
        if (imageType === "InInterior") {
            /* для больших картинок увелиыиваем ообъем до 1Мб */
            this.loadImageInAnimatedSlider(file, model);
            //this.validateImage(file, model, 1024, 1920, 0);
        }
        if (imageType === "SliderMiddle") {
            this.validateImage(file, model, 500, 900, 600);
        }
        if (imageType === "SliderMin") {
            this.validateImage(file, model, 100, 280, 130);
        }
    }

    private loadImageInAnimatedSlider(file: any, model: any) {
        if (file.size > 1024 * 1024) {
            this.messageService.error("Файл не должен превышать 1Мб", "Файл не должен превышать 1Мб");
            return;
        }
        var img = new Image();
        img.src = window.URL.createObjectURL(file);
        var me = this;
        img.onload = function () {
            var width = img.naturalWidth;
            var height = img.naturalHeight;
            window.URL.revokeObjectURL(img.src);
            if (width !== 1920 || height !== 890) {
                me.messageService.error("Изображение должно быть 1920*890", "Изображение должно быть 1920*890");
                return;
            }
            else {
                me.loadingFile(file, model);
            }
        }
    }

    public loadProductImage(product: any, variantId: string, type: string): void {

        var me = this;
        if (product.objectId) {
            me.uploadProductFile(variantId, product.productId, product.objectId, type);
        }
        else {
            //if there is no image - return
            var inputElement = (<HTMLInputElement>document.getElementById("file_product_" + variantId + "_" + product.productId));
            if (!inputElement || !inputElement.files || inputElement.files.length <= 0) {
                me.messageService.error("Не выбран файл!", "Didn't choose file");
                return;
            }

            var model: any = { productId: product.productId, collectionVariantId: variantId };
            me.importApiService.addIntegrationProductsContent(model)
                .then((response: any) => {
                    product.objectId = response.message;
                    me.uploadProductFile(variantId, product.productId, product.objectId, type);
                })
                .catch((err: any) => {
                    me.messageService.error("Не удалось сохранить изображение", "Can't save image");
                });
        }
    }

    private uploadProductFile(variantId: string, productId: string, objectId: string, type: string): void {
        var me = this;
        var inputElement = (<HTMLInputElement>document.getElementById("file_product_" + variantId + "_" + productId));
        if (!inputElement.files) {
            me.messageService.error("Не выбран файл!", "Didn't choose file");
            return;
        }
        var file = inputElement.files[0];

        if (!file) {
            me.messageService.error("Не выбран файл!", "Didn't choose file");
            return;
        }

        var model: Dto.Image.IUploadFileModel = {
            objectId: objectId,
            type: type
        };
        /*вытягиваем главное изображение анимированной картинки*/
        var animateImage = <HTMLImageElement>document.getElementById("image_" + variantId + "_InInterior");
        if (animateImage.naturalHeight === 0 || animateImage.naturalWidth === 0) {
            me.messageService.error("Загрузите сначала главный файл для анимированной картинки", "Загрузите сначала главный файл для анимированной картинки");
            return;
        }
        else {
            this.loadAndValidateProductImage(file, model, 500, animateImage.naturalWidth, animateImage.naturalHeight, productId, variantId);
        }
    }

    private validateImage(file: any, model: Dto.Image.IUploadFileModel, extendSize: number, extendWidth: number, extendHeight: number): void {
        /*валидация размера*/
        if (file.size > extendSize * 1000) {
            this.messageService.error("Файл не должен превышать " + extendSize + "Кб", "Файл не должен превышать " + extendSize + "Кб");
            return;
        }
        var img = new Image();
        img.src = window.URL.createObjectURL(file);
        var me = this;
        img.onload = function () {
            var width = img.naturalWidth;
            var height = img.naturalHeight;
            window.URL.revokeObjectURL(img.src);
            if (width !== extendWidth || height !== extendHeight) {
                const message = `Изображение должно быть ${extendWidth}*${extendHeight}`;
                me.messageService.error(message, message);
                return;
            }
            else {
                me.loadingFile(file, model);
            }
        }
    }

    private loadAndValidateProductImage(file: any, model: Dto.Image.IUploadFileModel, extendSize: number, extendWidth: number, extendHeight: number, productId, variantId): void {
        /*валидация размера*/
        if (file.size > 1024 * 1024) {
            this.messageService.error("Файл не должен превышать 1Мб", "Файл не должен превышать 1Мб");
            return;
        }
        var img = new Image();
        img.src = window.URL.createObjectURL(file);
        var me = this;
        img.onload = function () {
            var width = img.naturalWidth;
            var height = img.naturalHeight;
            window.URL.revokeObjectURL(img.src);

            if (width !== 1920 || height !== 890) {
                me.messageService.error("Изображение должно быть 1920*890", "Изображение должно быть 1920*890");
                return;
            }
            else {
                me.loadingProductFile(file, model, productId, variantId);
            }
        }
    }

    // obsolete
    private validateDimentions(extendWidth: number, extendHeight: number, actualWidth: number, actualHeight: number): Dto.Validate.IResponse {
        if (extendHeight === 0) {
            if (actualWidth > extendWidth) {
                return { success: false, message: "Файл не должен быть шире " + extendWidth };
            }
        }
        if (extendWidth === 0) {
            if (actualHeight < extendHeight) {
                return {
                    success: false, message: "Файл не должен быть меньше " + extendHeight + " по высоте"
                };
            }
        }
        if (extendHeight !== 0 && extendWidth !== 0) {
            if (actualWidth < extendWidth || actualHeight < extendHeight) {
                return { success: false, message: "Файл не должен быть меньше " + extendWidth + "x" + extendHeight };
            }
            else {
                var extendRatio = extendWidth / extendHeight;
                var actualRatio = actualWidth / actualHeight;
                if (actualRatio !== extendRatio) {
                    return { success: false, message: "Файл не пропорционален размеру " + extendWidth + "x" + extendHeight };
                }
            }
        }
        return { success: true, message: "" };
    }

    private loadingFile(file: any, model: Dto.Image.IUploadFileModel): void {
        var me = this;
        var formData = new FormData();
        formData.append("file", file);
        console.log(file);
        this.importApiService.uploadFile(model, formData)
            .then((response: any) => {
                if (response === 200) {
                    me.refreshImage(model);
                    me.messageService.success("Файл загружен");

                } else {
                    me.messageService.error(response.Message, response.Message);
                }
            });
    }

    private loadingProductFile(file: any, model: Dto.Image.IUploadFileModel, productId: string, variantId: string): void {
        var me = this;
        var formData = new FormData();
        formData.append("file", file);
        console.log(file);

        me.importApiService.uploadFile(model, formData)
            .then((response: any) => {
                if (response === 200) {
                    me.messageService.success("Файл загружен");
                    me.refreshImageProduct(model, productId, variantId);
                    me.addProductData(variantId, productId, model.objectId);
                } else {
                    me.messageService.error(response.Message, response.Message);
                }
            });
    }

    public refreshImage(model: Dto.Image.IUploadFileModel): void {
        var image = <HTMLInputElement>document.getElementById("image_" + model.objectId + "_" + model.type);
        image.setAttribute("src", image.src + "/");
    }

    public refreshImageProduct(model: Dto.Image.IUploadFileModel, productId: string, variantId: string): void {
        var image = <HTMLInputElement>document.getElementById("image_product_" + variantId + "_" + productId);
        image.setAttribute("src", image.src + "/");
    }

    private addProductData(variantId: string, productId: string, objectId: string): void {
        var variant = this.collection.integrationVariants.filter((v) => { return v.id === variantId })[0];
        var product = variant.integrationProducts.filter((p) => { return p.productId === productId })[0];
        product.objectId = objectId;
        product.src = this.productUrl.replace(":objectId", product.objectId);
    }

    public openModal(product: Dto.ImportCollection.IIntegrationProduct, variantId: string): void {
        var imageTag = <HTMLImageElement>document.getElementById("image_product_" + variantId + "_" + product.productId);
        if (imageTag.naturalHeight === 0 || imageTag.naturalWidth === 0) {
            this.messageService.error("Загрузите сначала картинку для выделения областей", "Загрузите сначала картинку для выделения областей");
            return;
        }
        else {
            this.editingProduct = product;
            this.$scope.src = product.src;
            var modalWindow = document.getElementById("modal-areas");
            modalWindow.style.display = "block";
            this.isBindingAreas = true;
        }
    }

    public closeModal(): void {
        var modalWindow = document.getElementById("modal-areas");
        modalWindow.style.display = "none";
        this.isBindingAreas = false;
    }

    public bindArea(objectId: string, itemId: string): void {
        var me = this;
        var image = document.getElementById("mr_image");
        var img_width = image.offsetWidth;
        var img_height = image.offsetHeight;
        var one_percent_width = img_width / 100;
        var one_percent_height = img_height / 100;
        var select = me.$scope.selector;
        var model: Dto.ImportCollection.ICoordinatesModel = {
            id: objectId,
            left: (select.x1 / one_percent_width).toFixed(),
            top: (select.y1 / one_percent_height).toFixed(),
            width: ((select.x2 - select.x1) / one_percent_width).toFixed(),
            height: ((select.y2 - select.y1) / one_percent_height).toFixed()
        };
        if (!isFinite(select.x1) || !isFinite(select.x2) || !isFinite(select.y1) || !isFinite(select.y2)) {
            me.messageService.error("Пожалуйста выберите область", "Please select an area");
            return;
        }
        me.importApiService.saveIntegrationCoordinates(model).then(
            (response: any) => {
                me.$scope.selector.clear();
                me.messageService.success("Область успешно прикреплена");
                this.editingProduct.contentHeight = parseInt(model.height);
                this.editingProduct.contentWidth = parseInt(model.width);
                this.editingProduct.contentLeft = parseInt(model.left);
                this.editingProduct.contentTop = parseInt(model.top);
            }, (err: any) => {
                me.messageService.error("Не удалось прикрепить область", "Can't bind picture area");
            });
    }

    /* функционал установки дефолтного варианта отложен */
    //public makeDefault(variantId: string): void {
    //    for (var variant of this.collection.integrationVariants) {
    //        if (variant.id !== variantId)
    //            variant.isDefault = false;
    //    };
    //    var model: any = { variantId: variantId };
    //    this.importApiService.makeDefault(model)
    //        .then((response: any) => {
    //            this.messageService.success("Вариант успешно установлен основным");
    //        }, (err: any) => {
    //            this.messageService.success("Не удалось установить вариант основным");
    //        });
    //}

    public changeProductsVisibility(variant: Dto.ImportCollection.IIntegrationCollectionsVariant): void {
        variant.isProductsVisible = !variant.isProductsVisible;
    }

    public publish(variant: Dto.ImportCollection.IIntegrationCollectionsVariant ) {
        this.importApiService
            .validatePublishCollectionVariant({ Id: variant.id })
            .then((response: Dto.Import.IImportError) => {
                if (response.isValid === true)
                    return this.importApiService.publishVariantCollection({ id: variant.id });
                else {
                    //TODO: показ ошибок toastr, подумать как показыать в ui
                    response.errors.forEach((error: string) => {
                        this.messageService.warning(error);
                    });                    
                    return false;
                }
            })
            .then((response: any) => {
                if (response) {
                    this.messageService.success("Вариант коллекции опубликован!");
                    variant.isPublished = true;
                }
            })
            .catch((error: any) => {
                this.messageService.error("Произошла ошибка при публикации варианта коллекции", error);
                console.log(error);
            });
    }
}