﻿import * as angular from "angular";

import controller from "./collections-variants.controller";

/*import other modules*/
import * as collectionModule from "../../collection/collection.module";
import * as commonModule from "../../common/common.module";
import * as importServiceModule from "../products-import-service/products-import-service.module";

/*constants*/
export const name = "kd.import.collections-variants";
const controllerName = "importCollectionsVariantsControllerName";
const componentName = "kdImportCollectionsVariants";

angular
    .module(name,
    [
        collectionModule.name,
        commonModule.name,
        importServiceModule.name
    ])
    .controller(controllerName,
    [
        "$scope",
        importServiceModule.apiServiceName,
        collectionModule.collectionApiServiceName,
        collectionModule.collectionServiceName,
        commonModule.messageServiceName,
        controller
    ])
    .component(componentName,
    {
        bindings: {
            collectionId: '@'
        },
        controller: controllerName,
        template: require("./collections-variants.view.html")
    });