﻿namespace TargetAudience.Services {
    export interface ITargetAudienceApiService {
        getTargetAudiences(): angular.IPromise<Array<Dto.TargetAudience.DtoTargetAudienceItem>>;
    }
}