﻿export default class TargetAudienceService implements TargetAudience.Services.ITargetAudienceApiService {

    private url: string = "/api/targetAudience";

    constructor(public $http: angular.IHttpService) {
    }

    public getTargetAudiences(): angular.IPromise<Array<Dto.TargetAudience.DtoTargetAudienceItem>> {
        return this.$http
            .get(this.url)
            .then((response: angular.IHttpPromiseCallbackArg<Array<Dto.TargetAudience.DtoTargetAudienceItem>>) => {
                return response.data;
            });
    }
}