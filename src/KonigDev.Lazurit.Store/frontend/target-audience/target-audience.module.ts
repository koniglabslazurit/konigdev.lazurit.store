﻿import * as angular from "angular";

import service from "./target-audience.api.service";

export const name = "kd.targetAudience";
export const serviceName = "targetAudienceService";

angular
    .module(name, [ ])
    .service(serviceName,
    [
        "$http",
        service
    ]);
