export const $http = "$http";
export const $element = "$element";
export const $timeout = "$timeout";
export const $log = "$log";

// ui-bootstrap
export const $uibModalInstance = "$uibModalInstance";
export const $uibModal = "$uibModal";