﻿namespace SyncCode.Services {
    export interface ISyncCodeApiService {
        getSyncCodes(params: Dto.SyncCode.GetSyncCode1CsQuery): angular.IPromise<Array<Dto.SyncCode.DtoSyncCode1CItem>>;
    }
}