﻿import * as angular from "angular";

import service from "./sync-code.api.service";

export const name = "kd.sync-code";
export const serviceName = "syncCodeService";

angular
    .module(name, [ ])
    .service(serviceName,
    [
        "$http",
        service
    ]);
