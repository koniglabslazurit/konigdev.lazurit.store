﻿export default class SyncCodeService  {

    private url: string = "/api/import/sync-code";

    constructor(public $http: angular.IHttpService) {
    }

    public getSyncCodes(data: Dto.SyncCode.GetSyncCode1CsQuery): angular.IPromise<Array<Dto.SyncCode.DtoSyncCode1CItem>> {
        return this.$http
            .get(this.url, { params: data })
            .then((response: angular.IHttpPromiseCallbackArg<Array<Dto.SyncCode.DtoSyncCode1CItem>>) => {
                return response.data;
            });
    }
}