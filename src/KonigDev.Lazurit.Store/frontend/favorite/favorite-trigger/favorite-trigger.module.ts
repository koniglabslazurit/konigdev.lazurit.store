﻿import * as angular from "angular";

import favoriteTriggerController from "./favorite-trigger.controller";
import * as favoriteServiceModule from "../favorite-service/favorite-service.module";
import * as constantsModule from "../../constants/constants.module";
import * as commonModule from "../../common/common.module";

/* constants */
export const name = "kd.favorite.trigger";
const favoriteButtonControllerName = "favoriteTriggerController";
const favoriteButtonComponentName = "kdFavoriteButton";
const favoriteLinkComponentName = "kdFavoriteLink";

angular
    .module(name,
    [
        favoriteServiceModule.name,
        constantsModule.name,
        commonModule.name
    ])
    .controller(favoriteButtonControllerName,
    [
        favoriteServiceModule.favoriteServiceName, constantsModule.serviceName, commonModule.messageServiceName,
        favoriteTriggerController
    ])
    .component(favoriteButtonComponentName, {
        bindings: {
            isInFavorite: '=',
            productId: '<',
            collectionId: '<',
            itemType: '<'
        },
        controller: favoriteButtonControllerName,
        template: require("./favorite-button.view.html")
    })
    .component(favoriteLinkComponentName, {
        bindings: {
            isInFavorite: '=',
            productId: '<',
            collectionId: '<',
            itemType: '<'
        },
        controller: favoriteButtonControllerName,
        template: require("./favorite-link.view.html")
    });
