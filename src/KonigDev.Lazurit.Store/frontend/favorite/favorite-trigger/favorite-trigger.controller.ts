﻿export default class FavoriteFullController {

    public isInFavorite: boolean;
    public productId: string;
    public collectionId: string;
    public itemType: string;
    public title: string;

    constructor(private favoriteService: Favorite.Services.IFavoriteService, public constants: Constants.IConstants, public messageService: Common.Message.IMessage) {       
    }

    public $onChanges() {
        this.updateTitle();
    }

    

    private runTrigger(): void {
        if (this.isInFavorite) {
            this.remove();
        } else {
            this.add();
        }
    }

    private remove(): void {
        if (this.itemType === "product") {
            this.deleteProduct(this.productId);
        } else if (this.itemType === "productInCollection") {
            this.deleteProductFromCollection(this.productId, this.collectionId);
        } else if (this.itemType === "collection") {
            this.deleteItemFullCollection(this.collectionId);
        } else if (this.itemType === "complect") {
            this.deleteComplect(this.collectionId);
        }
    }

    private add(): void {
        if (this.itemType === "product") {
            this.addProductToFavorite(this.productId);
        } else if (this.itemType === "collection") {
            this.addCollectionToFavorite(this.collectionId);
        } else if (this.itemType === "complect") {
            this.addComplectToFavorite(this.collectionId);
        }
    };

    //deleting items from favorite
    private deleteProduct(productId: string): void {
        this.favoriteService.deleteProductFromFavorite(productId)
            .then((favorite: any) => {
                if (favorite) {
                    this.messageService.success(this.constants.favorite.messages.productDeleted);
                    this.isInFavorite = false;
                    this.updateTitle();
                }
                else {
                    this.messageService.uiError(this.constants.favorite.messages.productNotDeleted);
                }
            })
            .catch((error: any) => {
                this.messageService.error(error.data.message, error);
            })
    };

    private deleteProductFromCollection(productId: string, collectionId: string): void {
        this.favoriteService.deleteItemCollectionFromFavorite(productId, collectionId)
            .then((favorite: any) => {
                if (favorite) {
                    this.messageService.success(this.constants.favorite.messages.productDeleted);
                    this.isInFavorite = false;
                    this.updateTitle();
                }
                else {
                    this.messageService.uiError(this.constants.favorite.messages.productNotDeleted);
                }
            })
            .catch((error: any) => {
                this.messageService.error(error.data.message, error);
            })
    };

    private deleteItemFullCollection(collectionId: string): void {
        this.favoriteService.deleteItemFullCollectionFromFavorite(collectionId)
            .then((favorite: any) => {
                if (favorite) {
                    this.messageService.success(this.constants.favorite.messages.collectionDeleted);
                    this.isInFavorite = false;
                    this.updateTitle();
                }
                else {
                    this.messageService.uiError(this.constants.favorite.messages.collectionNotDeleted);
                }
            })
            .catch((error: any) => {
                this.messageService.error(error.data.message, error);
            })
    };

    private deleteComplect(complectId: string): void {
        this.favoriteService.deleteItemComplectFromFavorite(complectId)
            .then((favorite: any) => {
                if (favorite) {
                    this.messageService.success(this.constants.favorite.messages.complectDeleted);
                    this.isInFavorite = false;
                    this.updateTitle();
                }
                else {
                    this.messageService.uiError(this.constants.favorite.messages.complectNotDeleted);
                }
            })
            .catch((error: any) => {
                this.messageService.error(error.data.message, error);
            })
    };


    //adding items to favorite
    private addProductToFavorite(productId: string): void {
        var model = { ProductId: productId };
        this.favoriteService.addProductToFavorite(model)
            .then((favorite: any) => {
                if (favorite) {
                    this.messageService.success(this.constants.favorite.messages.productAdded);
                    this.isInFavorite = true;
                    this.updateTitle();
                }
                else {
                    this.messageService.uiError(this.constants.favorite.messages.productNotAdded);
                }
            })
            .catch((error: any) => {
                this.messageService.error(error.data.message, error);
            })
    };

    private addCollectionToFavorite(collectionId: string): void {
        var model = { CollectionId: collectionId };
        this.favoriteService.addCollectionToFavorite(model)
            .then((favorite: any) => {
                if (favorite) {
                    this.messageService.success(this.constants.favorite.messages.collectionAdded);
                    this.isInFavorite = true;
                    this.updateTitle();
                }
                else {
                    this.messageService.uiError(this.constants.favorite.messages.collectionNotAdded);
                }
            })
            .catch((error: any) => {
                this.messageService.error(error.data.message, error);
            })
    };

    private addComplectToFavorite(collectionId: string): void {
        var model = { CollectionId: collectionId };
        this.favoriteService.addComplectToFavorite(model)
            .then((favorite: any) => {
                if (favorite) {
                    this.messageService.success(this.constants.favorite.messages.complectAdded);
                    this.isInFavorite = true;
                    this.updateTitle();
                }
                else {
                    this.messageService.uiError(this.constants.favorite.messages.complectNotAdded);
                }
            })
            .catch((error: any) => {
                this.messageService.error(error.data.message, error);
            })
    };

    private updateTitle(): void {
        if (this.isInFavorite) {
            this.title = this.constants.favorite.labels.isInFavorite;
        }
        else {
            this.title = this.constants.favorite.labels.IsNotInFavorite;
        }
    }
}