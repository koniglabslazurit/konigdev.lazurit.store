﻿import * as Clipboard from 'clipboard';

export default class FavoriteFullController implements angular.IController {

    public favorite: Dto.Favorite.IFavoriteContent;
    public shareLink: string;
    public isUserOwner: string;
    public linkId: string;
    public patch: string;
    public isUserOwnerBool: boolean = (this.isUserOwner === 'true');
    public shareLinkClipboardBtn: any;

    public constructor(
        private favoriteService: Favorite.Services.IFavoriteService,
        private constants: Constants.IConstants,
        private messageService: Common.Message.IMessage,
        private cartService: Cart.Services.ICartService,
        private $document: angular.IDocumentService
    ) {
        this.getFavoriteByLinkId();
        this.getShareLink();
    }

    $onInit() {
        this.initShareLink();
    }

    $onDestroy() {
        if (this.shareLinkClipboardBtn) {
            this.shareLinkClipboardBtn.destroy();
        }
    }

    private initShareLink(): void {
        this.shareLinkClipboardBtn = new Clipboard('.js-clipboard-btn', {
            target: trigger => {
                return this.$document[0].getElementsByClassName('js-clipboard-input')[0];
            }
        });

        //this part can be used for debug aims, or for information that will be shown for user
        this.shareLinkClipboardBtn.on('success', e => {
            e.clearSelection();
        });

        this.shareLinkClipboardBtn.on('error', error => {
            this.messageService.uiError("Ссылка не была скопированна в буфер, но вы можете сделать нажав Ctrl+C");
        });
    }

    private getFavoriteByLinkId(): void {
        var request = {
            LinkId: this.linkId,
            IsUserOwner: this.isUserOwner
        };
        this.favoriteService.getFavoriteByLinkId(request)
            .then((response) => {
                this.mappingFavorite(response);
            });
    };

    private mappingFavorite(data: any): void {

        this.favorite = <Dto.Favorite.IFavoriteContent>{};

        //если у пользователя нет избранного
        if (!data) return;

        this.favorite.products = <Array<Dto.Favorite.IFavoriteProduct>>data.products;
        this.favorite.complects = <Array<Dto.Favorite.IFavoriteComplect>>data.complects;
        this.favorite.collections = <Array<Dto.Favorite.IFavoriteCollection>>data.collections;


        this.favorite.collections.forEach((collection: Dto.Favorite.IFavoriteCollection) => {
            collection.price = 0;
            collection.salePrice = 0;
        });
        this.favorite.collections.forEach((collection: Dto.Favorite.IFavoriteCollection) => {
            collection.products.forEach((product: Dto.Favorite.IFavoriteProduct) => {
                collection.price += product.product.price.amount;
                collection.salePrice += product.product.salePrice.amount;
            });
        });
        this.favorite.id = data.id;
    };

    //deleting items from favorite
    private deleteProduct(productId: string): void {
        this.favoriteService.deleteProductFromFavorite(productId)
            .then((favorite: any) => {
                if (favorite) {
                    this.mappingFavorite(favorite);
                    this.messageService.success(this.constants.favorite.messages.productDeleted);
                }
                else {
                    this.messageService.uiError(this.constants.favorite.messages.productNotDeleted);
                }
            })
            .catch((error: any) => {
                this.messageService.error(error.data.message, error);
            })
    };

    private deleteProductFromCollection(productId: string, collectionId: string): void {
        this.favoriteService.deleteItemCollectionFromFavorite(productId, collectionId)
            .then((favorite: any) => {
                if (favorite) {
                    this.mappingFavorite(favorite);
                    this.messageService.success(this.constants.favorite.messages.productDeleted);
                }
                else {
                    this.messageService.uiError(this.constants.favorite.messages.productNotDeleted);
                }
            })
            .catch((error: any) => {
                this.messageService.error(error.data.message, error);
            })
    };

    private deleteItemFullCollection(collectionId: string): void {
        this.favoriteService.deleteItemFullCollectionFromFavorite(collectionId)
            .then((favorite: any) => {
                if (favorite) {
                    this.mappingFavorite(favorite);
                    this.messageService.success(this.constants.favorite.messages.collectionDeleted);
                }
                else {
                    this.messageService.uiError(this.constants.favorite.messages.collectionNotDeleted);
                }
            })
            .catch((error: any) => {
                this.messageService.error(error.data.message, error);
            })
    };

    private deleteComplect(complectId: string): void {
        this.favoriteService.deleteItemComplectFromFavorite(complectId)
            .then((favorite: any) => {
                if (favorite) {
                    this.mappingFavorite(favorite);
                    this.messageService.success(this.constants.favorite.messages.complectDeleted);
                }
                else {
                    this.messageService.uiError(this.constants.favorite.messages.complectNotDeleted);
                }
            })
            .catch((error: any) => {
                this.messageService.error(error.data.message, error);
            })
    };


    //adding items to !cart! from favorite
    private addProductToCart(productId: string): void {
        var model = { ProductId: productId };
        this.cartService.addProductToCart(model)
            .then((response: any) => {
            })
            .catch((error: any) => {
                this.messageService.error(error.data.message, error);
            });
    };

    private addCollectionToCart(collectionId: string): void {
        var model = { CollectionId: collectionId };
        this.cartService.addCollectionToCart(model)
            .then((response: any) => {
            })
            .catch((error: any) => {
                this.messageService.error(error.data.message, error);
            })
    };

    private addComplectToCart(collectionId: string): void {
        var model = { CollectionId: collectionId };
        this.cartService.addComplectToCart(model)
            .then((response: any) => {
            })
            .catch((error: any) => {
                this.messageService.error(error.data.message, error);
            })
    };

    public getShareLink(): void {
        if (this.isUserOwnerBool) {
            this.favoriteService.getShareLinkId()
                .then((data) => {
                    if (data) {
                        this.shareLink = this.patch + "/" + data.id;
                    }
                    else {
                        this.messageService.uiError("Ссылка на избранное не была сгенерирована");
                    }
                })
                .catch((error: any) => {
                    this.messageService.error(error.data.message, error);
                });
        }
        else {
            this.shareLink = this.patch + "/" + this.linkId;
        }
    };
}