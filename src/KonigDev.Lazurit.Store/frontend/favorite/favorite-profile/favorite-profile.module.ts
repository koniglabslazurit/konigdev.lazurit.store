﻿import * as angular from "angular";

import favoriteProfileController from "./favorite-profile.controller";
import * as favoriteServiceModule from "../favorite-service/favorite-service.module";
import * as constantsModule from "../../constants/constants.module";
import * as commonModule from "../../common/common.module";
import * as cartServiceModule from "../../cart/cart-service/cart-mini-service.module";

/* constants */
export const name = "kd.favorite.profile";
const favoriteProfileControllerName = "favoriteProfileController";
const favoriteProfileComponentName = "kdFavoriteProfile";

angular
    .module(name,
    [
        favoriteServiceModule.name,
        constantsModule.name,
        commonModule.name,
        cartServiceModule.name
    ])
    .controller(favoriteProfileControllerName,
    [
        favoriteServiceModule.favoriteServiceName,
        constantsModule.serviceName,
        commonModule.messageServiceName,
        cartServiceModule.cartServiceName,
        '$document',
        favoriteProfileController
    ])
    .component(favoriteProfileComponentName, {
        bindings: {
            isUserOwner: '@',
            linkId: '@',
            patch: '@'
        },
        controller: favoriteProfileControllerName,
        template: require("./favorite-profile.view.html")
    });