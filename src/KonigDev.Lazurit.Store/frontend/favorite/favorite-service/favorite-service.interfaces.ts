﻿namespace Favorite.Services {
    export interface IFavoriteService {
        getFavorite(): any;
        getShareLinkId(): any;
        getFavoriteByLinkId(request: any): any;
        addProductToFavorite(productId): any;
        addCollectionToFavorite(collectionId): any;
        addComplectToFavorite(complectId): any;
        deleteProductFromFavorite(productId): any;
        deleteItemCollectionFromFavorite(productId, collectionId): any;
        deleteItemFullCollectionFromFavorite(collectionId): any;
        deleteItemComplectFromFavorite(complectId): any;
    }
}
