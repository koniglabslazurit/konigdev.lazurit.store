﻿export default class FavoriteService implements Favorite.Services.IFavoriteService {

    private getFavoriteUrl: string = '/api/favorite/';
    private getShareLinkIdUrl: string = '/api/favorite/shareLinkId/';
    private getFavoriteByLinkIdUrl: string = '/api/favorite/getFavoriteByLinkId/';
    private addProductToFavoriteUrl: string = '/api/favorite/product/';
    private addCollectionToFavoriteUrl: string = '/api/favorite/collection/';
    private addComplectToFavoriteUrl: string = '/api/favorite/complect/';
    private deleteProductFromFavoriteUrl: string = '/api/favorite/product/';
    private deleteItemCollectionFromFavoriteUrl: string = '/api/favorite/collection/';
    private deleteItemComplectFromFavoriteUrl: string = '/api/favorite/complect/';

    constructor(public $http: angular.IHttpService) {
    }

    public getFavorite(): any {
        return this.$http
            .get(this.getFavoriteUrl)
            .then((response: any) => {
                return response.data;
            });
    };

    public getShareLinkId(): any {
        return this.$http
            .get(this.getShareLinkIdUrl)
            .then((response: any) => {
                return response.data;
            });
    };

    public getFavoriteByLinkId(request: any): any {
        return this.$http
            .get(this.getFavoriteByLinkIdUrl, {params: request})
            .then((response: any) => {
                return response.data;
            });
    };

    public addProductToFavorite(model: string): any {
        return this.$http.post(this.addProductToFavoriteUrl, model)
            .then((response: any) => {
                return response.data;
            });
    };

    public addCollectionToFavorite(model: string): any {
        return this.$http
            .post(this.addCollectionToFavoriteUrl, model)
            .then((response: any) => {
                return response.data;
            });
    };

    public addComplectToFavorite(model: string): any {
        return this.$http
            .post(this.addComplectToFavoriteUrl, model)
            .then((response: any) => {
                return response.data;
            });
    }

    public deleteProductFromFavorite(productId: string): any {
        return this.$http
            .delete(this.deleteProductFromFavoriteUrl + productId)
            .then((response: any) => {
                return response.data;
            });
    }

    public deleteItemCollectionFromFavorite(productId: string, collectionId: string): any {
        return this.$http
            .delete(this.deleteItemCollectionFromFavoriteUrl + productId + "/" + collectionId)
            .then((response: any) => {
                return response.data;
            });
    }

    public deleteItemFullCollectionFromFavorite(collectionId: string): any {
        return this.$http
            .delete(this.deleteItemCollectionFromFavoriteUrl + "full/" + collectionId)
            .then((response: any) => {
                return response.data;
            });
    }

    public deleteItemComplectFromFavorite(complectId: string): any {
        var me = this;
        return me.$http
            .delete(me.deleteItemComplectFromFavoriteUrl + complectId)
            .then((response) => {
                return response.data;
            });
    };
}