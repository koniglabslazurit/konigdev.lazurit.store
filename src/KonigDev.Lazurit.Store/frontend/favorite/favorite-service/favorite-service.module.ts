﻿import * as angular from "angular";

import favoriteService from "./favorite-service.service";

/* constants */
export const name = "kd.favorite.service";
export const favoriteServiceName = "FavoriteService";

angular
    .module(name, [ ])
    .service(favoriteServiceName,
    [
        "$http",
        favoriteService
    ]);
