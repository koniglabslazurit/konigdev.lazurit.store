﻿import * as angular from "angular";

import * as cartHubModule from "../cart/cart-hub/cart-hub.module";
import * as commonModule from "../common/common.module";
import * as customerRequestCreateModule from '../customer-request/create/customer-request.create.module';
import * as signalrModule from "../signalr/signalr.module";

import controller from "./header-tools.controller";

export const name = "kd.header";
export const controllerName = "headerToolsCtrl";
const componentName = "kdHeaderTools";

angular
    .module(name,
    [
        cartHubModule.name,
        commonModule.name,
        customerRequestCreateModule.name,
        signalrModule.name
    ])
    .controller(controllerName,
    [
        cartHubModule.cartIconHubServiceName,
        commonModule.eventServiceName,
        commonModule.cookieServiceName,
        signalrModule.serviceName,
        controller
    ])
    .component(componentName, {
        bindings: {
            phoneNumber: '<',
            isAuthenticated: '<',
            returnUrl: '<'
        },
        controller: controllerName,
        template: require("./header-tools.view.html")
    });
