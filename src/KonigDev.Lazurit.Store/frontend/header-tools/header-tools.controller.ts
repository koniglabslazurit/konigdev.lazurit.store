﻿export default class HeaderToolsCtrl {
    public phoneNumber: string;
    public isAuthenticated: boolean;
    public returnUrl: string;
    public cart: any;
    private productCount: number;
    private cityName: string;
    public loginFormIsShow: boolean = false;
    //field that controlls visibility of small icon with city name
    public isListOfCitiesEnabled: boolean = false;

    constructor(public CartIconHubService: any,
        public CommonService: any,
        public cookieService: any,
        public signalrService: any) {
        this.init();
        CommonService.suscribeOnEvent('writeCityInHeader', function (event) {
            this.cityName = this.cookieService.getCookie("cityName");
            this.isListOfCitiesEnabled = false;
        }.bind(this));
        CommonService.suscribeOnEvent('DontShowPopup', function (event) {
            this.isListOfCitiesEnabled = true;
        }.bind(this));
    }
    init() {
        this.CartIconHubService.updateCart();
        this.cityName = this.cookieService.getCookie("cityName");
        this.signalrService.start();
    };

    public showAllCities() {
        this.isListOfCitiesEnabled = true;
        this.CommonService.triggerEvent('showAllCities');
    };
}