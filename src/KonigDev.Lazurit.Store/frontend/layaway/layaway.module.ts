﻿import * as angular from "angular";

import layawayController from "./layaway.controller";
import * as layawayServiceModule from "./layaway-service/layaway-service.module";

/* constants */
export const name = "kd.layaway";
const layawayControllerName = "layawayController";
const layawayComponentName = "kdLayaway";

angular
    .module(name,
    [
        layawayServiceModule.name
    ])
    .controller(layawayControllerName,
    [
        layawayServiceModule.layawayServiceName,
        layawayController
    ])
    .component(layawayComponentName, {
        bindings: {
            onRefresh: '&',
            update: '<'
        },
        controller: layawayControllerName,
        template: require("./layaway.view.html")
    });