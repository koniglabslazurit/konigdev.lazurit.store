﻿namespace Layaway.Services {
    export interface ILayawayService {
        getLayaway(request: any): angular.IPromise<any>;
        moveToLayaway(data: any): angular.IPromise<any>;
        backFromLayaway(data: any): angular.IPromise<any>;
        deleteFromLayaway(data: any): angular.IPromise<any>;
    }
}