﻿export default class LayawayService implements Layaway.Services.ILayawayService {

    private urlCrud: string = "/api/layaway";

    constructor(public $http: angular.IHttpService) {
    }

    public getLayaway(request: any): angular.IPromise<any> {
        return this.$http
            .get(this.urlCrud, { params: request })
            .then((response) => {
                return response.data;
            });
    }

    public moveToLayaway(data: any): angular.IPromise<any> {
        return this.$http
            .post(this.urlCrud, data)
            .then(() => {

            });
    }

    public backFromLayaway(data: any): angular.IPromise<any> {
        return this.$http
            .put(this.urlCrud, data)
            .then(() => {

            });
    }

    public deleteFromLayaway(data: any): angular.IPromise<any>  {
        var url = this.urlCrud + "?LayawayId=" + data.layawayId + "&CollectionItemLayawayId=" + data.collectionId + "&ComplectItemLayawayId=" + data.complectId + "&ProductItemLayawayId=" + data.productId + "&ProductInCollectionId=" + data.productInCollectionId;
        return this.$http
            .delete(url) 
            .then(() => {

            });
    }
}