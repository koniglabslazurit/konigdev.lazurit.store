﻿import * as angular from "angular";

import service from "./layaway-service.service";

export const name = "kd.layaway.services";
export const layawayServiceName = "layawayService";

angular
    .module(name, [])
    .service(layawayServiceName, ['$http', service]);