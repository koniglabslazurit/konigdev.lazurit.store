﻿export default class LayawayController {
    private User: any;
    private UserId: any;
    public layaways: Dto.LayawayItem.ILayaway[];
    public layawaysLoaded: Array<any> = [];
    public onRefresh: any;
    public layawaysOnPageCount: any = 10;

    // для обновления отложенных товаров
    public update: number;

    constructor(public LayawayService: Layaway.Services.ILayawayService) {
        this.getLayaways();
    }

    $onChanges(changes: { update: ng.IChangesObject<number> }) {
        if (changes.update && !changes.update.isFirstChange()) {
            this.getLayaways();
        }
    }

    getLayaways(): void {
        this.LayawayService.getLayaway({ UserId: this.UserId }).then((response) => {
            this.layaways = response;
            console.log(response);
        });
    }

    public loadMoreLayaways(): void {
        if (this.layaways.length > this.layawaysLoaded.length) {
            var count = this.layawaysLoaded.length + this.layawaysOnPageCount;
            for (var i = this.layawaysLoaded.length; i < count; i++) {
                if (this.layaways[i]) {
                    this.layawaysLoaded.push(this.layaways[i]);
                }
            }
        }
    };

    getCurrentLayaway(layawayId: string): Dto.LayawayItem.ILayaway {
        return this.layaways.filter((f: any) => {
            return f.id === layawayId;
        })[0];
    }

    getCurrentCollection(collectionId: string, layawayId: string): Dto.LayawayItem.ILayawayCollection {
        var currentLayaway = this.getCurrentLayaway(layawayId);
        return currentLayaway.collections.filter((c: any) => {
            return c.id === collectionId;
        })[0];
    }
    getCurrentProductInCollection(productId: string, collectionId: string, layawayId: string): Dto.LayawayItem.ILayawayProductInCollection {
        var currentLayaway = this.getCurrentLayaway(layawayId);
        var currentCollection = this.getCurrentCollection(collectionId, layawayId)
        return currentCollection.productsInCollection.filter((c: any) => {
            return c.productId === productId;
        })[0];
    }

    getCurrentComplect(complectId: string, layawayId: string): Dto.LayawayItem.ILayawayComplect {
        var currentLayaway = this.getCurrentLayaway(layawayId);
        return currentLayaway.complects.filter((c: any) => {
            return c.id === complectId;
        })[0];
    }

    getCurrentProduct(productId: string, layawayId: string): Dto.LayawayItem.ILayawayProduct {
        var currentLayaway = this.getCurrentLayaway(layawayId);
        return currentLayaway.products.filter((c: any) => {
            return c.productId === productId;
        })[0];
    }

    public backProductFromLayaway(item: any, layawayId: string): void {
        var currentLayaway = this.getCurrentLayaway(layawayId);
        var currentProduct = this.getCurrentProduct(item, layawayId);
        var data = {
            Products: [],
            Collections: [],
            Complects: [],
            UserId: "",
            Id: layawayId
        };

        data.Products.push({
            ProductId: item,
            CollectionId: currentProduct.product.collectionId,
            Quantity: currentProduct.quantity
        });
        this.LayawayService.backFromLayaway(data).then(() => {
            if (!currentLayaway)
                return;
            var index = currentLayaway.products.indexOf(item);
            currentLayaway.products.splice(index, 1);
            if (currentLayaway.collections.length === 0 && currentLayaway.complects.length === 0 && currentLayaway.products.length === 0) {
                var indexOfLayaway = this.layaways.indexOf(currentLayaway);
                this.layaways.splice(indexOfLayaway, 1);
            }
            if (typeof (this.onRefresh()) === 'function') {
                this.onRefresh()();
            }
        });
    }

    public deleteProductFromLayaway(item: any, layawayId: string): void {
        var currentLayaway = this.getCurrentLayaway(layawayId);
        var currentProduct = this.getCurrentProduct(item, layawayId);
        var data = {
            layawayId: layawayId,
            collectionId: "",
            productInCollectionId: "",
            productId: item,
            complectId: ""
        };

        this.LayawayService.deleteFromLayaway(data).then(() => {
            if (!currentLayaway)
                return;
            var index = currentLayaway.products.indexOf(item);
            currentLayaway.products.splice(index, 1);
            if (typeof (this.onRefresh()) === 'function') {
                this.onRefresh()();
            }
        });
    }

    public backWholeCartFromLayaway(layawayId: string): void {
        var currentLayaway = this.getCurrentLayaway(layawayId);
        currentLayaway.collections.map((c) => {
            c.layawayItemCollectionId = c.id;
        });
        var data = {
            Products: currentLayaway.products,
            Collections: currentLayaway.collections,
            IsWholeCart: currentLayaway.isWholeCart,
            Complects: currentLayaway.complects,
            UserId: "",
            Id: layawayId
        };

        this.LayawayService.backFromLayaway(data).then(() => {
            if (!currentLayaway)
                return;
            var layawayindex = this.layaways.indexOf(currentLayaway);
            this.layaways.splice(layawayindex, 1);
            if (typeof (this.onRefresh()) === 'function') {
                this.onRefresh()();
            }
        });
    }

    public backCollectionFromLayaway(collectionId: string, layawayId: string): void {
        var currentLayaway = this.getCurrentLayaway(layawayId);
        var currentCollection = this.getCurrentCollection(collectionId, layawayId);
        var data = {
            Products: [],
            Collections: [],
            Complects: [],
            UserId: "",
            Id: layawayId
        };

        data.Collections.push({
            LayawayItemCollectionId: collectionId,
            CollectionVariantId: currentCollection.collectionVarinatId,
            Quantity: currentCollection.quantity,
            ProductsInCollection: currentCollection.productsInCollection
        });
        this.LayawayService.backFromLayaway(data).then(() => {
            if (!currentCollection)
                return;
            var index = currentLayaway.collections.indexOf(currentCollection);
            currentLayaway.collections.splice(index, 1);
            if (currentLayaway.collections.length === 0 && currentLayaway.complects.length === 0 && currentLayaway.products.length === 0) {
                var indexOfLayaway = this.layaways.indexOf(currentLayaway);
                this.layaways.splice(indexOfLayaway, 1);
            }
            if (typeof (this.onRefresh()) === 'function') {
                this.onRefresh()();
            }
        });
    }

    public deleteCollectionFromLayaway(collectionId: string, layawayId: string): void {
        var currentLayaway = this.getCurrentLayaway(layawayId);
        var currentCollection = this.getCurrentCollection(collectionId, layawayId);
        var data = {
            layawayId: layawayId,
            collectionId: collectionId,
            productInCollectionId: "",
            productId: "",
            complectId: ""
        };

        this.LayawayService.deleteFromLayaway(data).then(() => {
            if (!currentLayaway)
                return;
            var index = currentLayaway.collections.indexOf(currentCollection);
            currentLayaway.collections.splice(index, 1);
            if (currentLayaway.collections.length === 0 && currentLayaway.complects.length === 0 && currentLayaway.products.length === 0) {
                var indexOfLayaway = this.layaways.indexOf(currentLayaway);
                this.layaways.splice(indexOfLayaway, 1);
            }
            if (typeof (this.onRefresh()) === 'function') {
                this.onRefresh()();
            }
        });
    }

    public backProductInCollectionFromLayaway(productId: string, collectionId: string, layawayId: string): void {
        var currentLayaway = this.getCurrentLayaway(layawayId);
        var currentCollection = this.getCurrentCollection(collectionId, layawayId);
        var currentProductInCollection = this.getCurrentProductInCollection(productId, collectionId, layawayId);
        currentProductInCollection.ProductId = productId;
        currentProductInCollection.Quantity = currentProductInCollection.quantity;
        currentProductInCollection.IsAssemblyRequired = currentProductInCollection.isAssemblyRequired;
        var data = {
            Products: [],
            Collections: [],
            Complects: [],
            UserId: "",
            Id: layawayId
        };

        data.Collections.push({
            LayawayItemCollectionId: collectionId,
            CollectionVarinatId: currentCollection.collectionVarinatId,
            Quantity: currentCollection.quantity,
            ProductsInCollection: [currentProductInCollection]
        });

        this.LayawayService.backFromLayaway(data).then(() => {
            if (!currentCollection)
                return;
            var indexOfProduct = currentCollection.productsInCollection.indexOf(currentProductInCollection);
            currentCollection.productsInCollection.splice(indexOfProduct, 1);
            if (currentCollection.productsInCollection.length === 0) {
                var indexOfCollection = currentLayaway.collections.indexOf(currentCollection);
                currentLayaway.collections.splice(indexOfCollection, 1);
                if (currentLayaway.collections.length === 0 && currentLayaway.complects.length === 0 && currentLayaway.products.length === 0) {
                    var indexOfLayaway = this.layaways.indexOf(currentLayaway);
                    this.layaways.splice(indexOfLayaway, 1);
                }
            }
            if (typeof (this.onRefresh()) === 'function') {
                this.onRefresh()();
            }
        });
    }

    public deleteProductInCollectionFromLayaway(productId: string, collectionId: string, layawayId: string): void {
        var currentLayaway = this.getCurrentLayaway(layawayId);
        var currentCollection = this.getCurrentCollection(collectionId, layawayId);
        var currentProductInCollection = this.getCurrentProductInCollection(productId, collectionId, layawayId);
        var data = {
            layawayId: layawayId,
            collectionId: collectionId,
            productInCollectionId: productId,
            productId: "",
            complectId: ""
        };

        this.LayawayService.deleteFromLayaway(data).then(() => {
            if (!currentCollection)
                return;
            var index = currentCollection.productsInCollection.indexOf(currentProductInCollection);
            currentCollection.productsInCollection.splice(index, 1);
            if (currentCollection.productsInCollection.length === 0) {
                var indexOfCollection = currentLayaway.collections.indexOf(currentCollection);
                currentLayaway.collections.splice(indexOfCollection, 1);
                if (currentLayaway.collections.length === 0 && currentLayaway.complects.length === 0 && currentLayaway.products.length === 0) {
                    var indexOfLayaway = this.layaways.indexOf(currentLayaway);
                    this.layaways.splice(indexOfLayaway, 1);
                }
                if (typeof (this.onRefresh()) === 'function') {
                    this.onRefresh()();
                }
            }
        });
    }

    public backComplectFromLayaway(complectId: string, layawayId: string): void {
        var currentLayaway = this.getCurrentLayaway(layawayId);
        var currentComplect = this.getCurrentComplect(complectId, layawayId);
        var data = {
            Products: [],
            Collections: [],
            Complects: [],
            UserId: "",
            Id: layawayId
        };

        data.Complects.push({
            CollectionId: complectId,
            IsAssemblyRequired: currentComplect.isAssemblyRequired,
            Quantity: currentComplect.quantity,
        });
        this.LayawayService.backFromLayaway(data).then(() => {
            if (!currentComplect)
                return;
            var index = currentLayaway.complects.indexOf(currentComplect);
            currentLayaway.complects.splice(index, 1);
            if (typeof (this.onRefresh()) === 'function') {
                this.onRefresh()();
            }
            if (currentLayaway.collections.length === 0 && currentLayaway.complects.length === 0 && currentLayaway.products.length === 0) {
                var indexOfLayaway = this.layaways.indexOf(currentLayaway);
                this.layaways.splice(indexOfLayaway, 1);
            }
        });
    }

    public deleteComplectFromLayaway(complectId: string, layawayId: string): void {
        var currentLayaway = this.getCurrentLayaway(layawayId);
        var currentComplect = this.getCurrentComplect(complectId, layawayId);
        var data = {
            layawayId: layawayId,
            collectionId: "",
            productInCollectionId: "",
            complectId: complectId,
            productId: ""
        };
        this.LayawayService.deleteFromLayaway(data).then(() => {
            if (!currentComplect)
                return;
            var index = currentLayaway.complects.indexOf(currentComplect);
            currentLayaway.complects.splice(index, 1);
            if (currentLayaway.collections.length === 0 && currentLayaway.complects.length === 0 && currentLayaway.products.length === 0) {
                var indexOfLayaway = this.layaways.indexOf(currentLayaway);
                this.layaways.splice(indexOfLayaway, 1);
            }
            if (typeof (this.onRefresh()) === 'function') {
                this.onRefresh()();
            }
        });
    }
}