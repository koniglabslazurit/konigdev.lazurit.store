﻿namespace Dto.LayawayItem {
    export interface ILayaway {
        id: string;
        isWholeCart: boolean;
        complects: Array<ILayawayComplect>,
        products: Array<ILayawayProduct>,
        collections: Array<ILayawayCollection>
    }
    export interface ILayawayProduct {
        id: string;
        productId: string;
        quantity: number;
        isAssemblyRequired: boolean;
        product: IProductItem;
    }
    export interface IProductItem {
        id: string;
        summ: number;
        quantity: number;
        price: Dto.Money.IMoney;
        discount: number;
        isAssemblyRequired: boolean;
        priceForAssembly: Dto.Money.IMoney;
        collectionId: string;
    }
    export interface ILayawayCollection {
        id: string;
        quantity: number;
        productsInCollection: Array<ILayawayProductInCollection>;
        isAssemblyRequired: boolean;
        collectionVarinatId: string;
        layawayItemCollectionId: string;
        collectionVariantId: string;
        roomTypeName: string;
        collectionName: string;
    }
    export interface ILayawayProductInCollection {
        id: string;
        quantity: number;
        isAssemblyRequired: boolean;
        productId: string;
        Quantity: number;
        IsAssemblyRequired: boolean;
        ProductId: string;
        product: Dto.CartFull.IProductItem;
    }
    export interface ILayawayComplect {
        id: string;
        quantity: number;
        isAssemblyRequired: boolean;        
        salePrice: Dto.Money.IMoney;
        price: Dto.Money.IMoney;
        discount: number;
        priceForAssembly: Dto.Money.IMoney;
        roomTypeName: string;
        collectionName: string;
        collectionVarinatId: string;
    }
}
