﻿namespace Filter.Services {
    export interface IFilterApiService {
        getFilters(request: any): angular.IPromise<Dto.Filter.IDtoFiltersResult>;
        getFacings(): angular.IPromise<Array<Dto.Filter.IDtoFacingItem>>;
        getFacingColors(): angular.IPromise<Array<Dto.Filter.IDtoFacingColorItem>>;
        getFrameColors(): angular.IPromise<Array<Dto.Filter.IDtoFrameColorItem>>;
        getFiltersByProduct(request: Dto.Product.ProductRequest): angular.IPromise<Dto.Filter.IDtoFilterItem>;
    }
}