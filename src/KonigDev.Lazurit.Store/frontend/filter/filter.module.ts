﻿import * as angular from "angular";

import service from "./filter.service.api";

export const name = "kd.filter";
export const serviceName = "filterService";

angular
    .module(name, [ ])
    .service(serviceName, ["$http", service]);
