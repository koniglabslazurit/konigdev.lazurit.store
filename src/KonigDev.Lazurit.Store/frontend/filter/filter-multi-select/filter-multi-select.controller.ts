﻿export default class SelectFilterCtrl {    
    public settings: any;
    public kdModel: Array<any>;
    public listItems: Array<any>;
    public kdChange: any;
    public items: Array<any>;

    private isShow: boolean = false;

    public constructor() {
    }

    public isSelected(item: any): boolean {
        return this.kdModel.indexOf(item.id) > -1;
    }

    public itemSelect(item: any): void {
        if (item) {            
            this.kdModel.push(item.id);
            this.items.forEach((i: any) => {
                if (i.id === item.id) {
                    item.isSelected = true;
                }
            });
            if (typeof (this.kdChange) === 'function') {
                this.kdChange(this.kdModel);
            }
        }
    }

    public itemRemove(item: any): void {
        this.kdModel.splice(this.kdModel.indexOf(item.id), 1);
        if (typeof (this.kdChange) === 'function') {
            this.kdChange(this.kdModel);
        }
        console.log(this.kdModel);
        this.items.forEach((i: any) => {
            if (i.id === item.id) {
                item.isSelected = false;
            }
        });
    }

    $onChanges(changesObj: any): void {
        if (changesObj.listItems) {
            if (changesObj.listItems.currentValue) {
                this.items = JSON.parse(JSON.stringify(changesObj.listItems.currentValue));
            }
        }
    }
}