﻿import * as angular from "angular";

import controller from "./filter-multi-select.controller";

export const name = "kd.filter.multiselect";
const controllerName = "filterMultiSelectController";
const filterMultiSelectComponentName = "kdFilterMultiSelect";

angular
    .module(name, [ ])
    .controller(controllerName, [controller])
    .component(filterMultiSelectComponentName,{
        bindings: {
            settings: '<',
            kdModel: '=',
            kdChange: '=',
            listItems: '<'
        },
        controller: controller,
        template: require("./filter-multi-select.view.html")
    });