﻿export default class FilterService implements Filter.Services.IFilterApiService {
    private getFiltersUrl: string = "/api/filter/filters";
    private getFiltersByProductUrl: string = "/api/filter/single-product";
    private getFacingsUrl: string = "/api/filter/facings";
    private getFacingColorsUrl: string = "/api/filter/facingColors";
    private getFrameColorsUrl: string = "/api/filter/frameColors";

    constructor(private $http: angular.IHttpService) {
    }

    public getFilters(request: any): angular.IPromise<Dto.Filter.IDtoFiltersResult> {
        return this.$http
            .get(this.getFiltersUrl, { params: request })
            .then((response: angular.IHttpPromiseCallbackArg<Dto.Filter.IDtoFiltersResult>) => {
                return response.data;
            });
    };

    public getFiltersByProduct(request: Dto.Product.ProductRequest): angular.IPromise<Dto.Filter.IDtoFilterItem> {
        return this.$http
            .get(this.getFiltersByProductUrl, { params: request })
            .then((response: angular.IHttpPromiseCallbackArg<Dto.Filter.IDtoFilterItem>) => {
                return response.data;
            });
    };

    public getFacings(): angular.IPromise<Array<Dto.Filter.IDtoFacingItem>> {
        return this.$http
            .get(this.getFacingsUrl)
            .then((response: angular.IHttpPromiseCallbackArg<Array<Dto.Filter.IDtoFacingItem>>) => {
                return response.data;
            });
    };

    public getFacingColors(): angular.IPromise<Array<Dto.Filter.IDtoFacingColorItem>> {
        return this.$http
            .get(this.getFacingColorsUrl)
            .then((response: angular.IHttpPromiseCallbackArg<Array<Dto.Filter.IDtoFacingColorItem>>) => {
                return response.data;
            });
    };

    public getFrameColors(): angular.IPromise<Array<Dto.Filter.IDtoFrameColorItem>> {
        return this.$http
            .get(this.getFrameColorsUrl)
            .then((response: angular.IHttpPromiseCallbackArg<any>) => {
                return response.data;
            });
    };
};