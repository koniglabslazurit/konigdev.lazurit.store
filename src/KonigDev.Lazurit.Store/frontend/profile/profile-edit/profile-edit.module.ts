﻿import * as angular from "angular";

import * as profileServicesModule from "../profile-services/profile-services.module";
import * as accountServicesModule from "../../auth/common/auth.common.module";
import profileEditJQService from "./profile-edit.service.jq";
import * as commonModule from "../../common/common.module";
import * as constantsModule from "../../constants/constants.module";

import controller from "./profile-edit.controller";

export const name = "kd.profile.edit";
const controllerName = "profileEditController";
const componentName = "kdProfileEdit";
const profileEditJQServiceName = "profileEditJQService";

angular
    .module(name,
    [
        "ui.mask",
        profileServicesModule.name,
        commonModule.name,
        constantsModule.name
    ])
    .service(profileEditJQServiceName, [profileEditJQService])
    .controller(controllerName,
    [
        profileServicesModule.profileServiceName, accountServicesModule.accountServiceName, profileEditJQServiceName, "$window", commonModule.messageServiceName, constantsModule.serviceName,
        controller
    ])
    .component(componentName, {
        bindings: {
            kdModel: '=',
            kdEdited: '='
        },
        controller: controllerName,
        template: require("./profile-edit.view.html")
    });