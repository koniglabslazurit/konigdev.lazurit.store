﻿export default class ProfileEditController {
    public kdModel: Dto.Profile.IProfileItem;
    public kdEdited: boolean;
    public profileForm: angular.IFormController;
    public email: string;
    private amountOfPhones: number = 5;
    public amountOfAddresses: number = 10;
    public isShowModalChangeEmail: boolean = false;

    public constructor(
        private profileApiService: any,
        private accountApiService: Auth.Services.IApiService,
        private profileEditJQService,
        public $window: angular.IWindowService,
        public messageService: Common.Message.IMessage,
        public constants: Constants.IConstants
    ) {
        this.profileForm = this.profileForm || null;
    }

    public addPhone(): void {
        if (this.kdModel.userPhones.length < (this.amountOfPhones - 1)) {
            if (this.kdModel.userPhones.length === 0 || this.kdModel.userPhones[this.kdModel.userPhones.length - 1].phone !== "") {
                this.kdModel.userPhones.push({ phone: '' });
            }
        }
    }

    public addAddress(): void {
        if (this.kdModel.userAddresses.length < (this.amountOfAddresses - 1)) {
            if (this.kdModel.userAddresses.length === 0 || this.kdModel.userAddresses[this.kdModel.userAddresses.length - 1].address !== "") {
                this.kdModel.userAddresses.push({ address: '', id: this.makeGuid() });
                console.log(this.kdModel.userAddresses);
            }
        }
    }

    public createPartOfGuid(): string {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    public makeGuid(): string {
        return this.createPartOfGuid() + this.createPartOfGuid() + '-' + this.createPartOfGuid() + '-' + this.createPartOfGuid() + '-' + this.createPartOfGuid() + '-' + this.createPartOfGuid() + this.createPartOfGuid() + this.createPartOfGuid();
    }

    public deleteAddress(item: Dto.Profile.IUserAddressItem): void {
        item.isMarkedDeleted = true;
    }

    public deletePhone(item: Dto.Profile.IUserPhoneItem): void {
        item.isMarkedDeleted = true;
    }

    public saveData(): void {
        this.kdModel.userAddresses.forEach((s) => {
            s.address = this.profileEditJQService.getAddressFromKladr(s.id);
            if (s.address) {
                if (s.block) {
                    s.address += ', корп. ' + s.block;
                }
                if (s.building) {
                    s.address += ', строение ' + s.building;
                }
                if (s.house) {
                    s.address += ', дом ' + s.house;
                }
                if (s.entranceHall) {
                    s.address += ', подъезд ' + s.entranceHall;
                }
                if (s.floor) {
                    s.address += ', этаж ' + s.floor;
                }
                if (s.apartment) {
                    s.address += ', кв. ' + s.apartment;
                }
            }
        });
        this.kdModel.userPhones.forEach((p) => { if (p.phone === "") { this.deletePhone(p) } });
        this.kdModel.userPhones = this.kdModel.userPhones.filter((p) => !p.isMarkedDeleted);
        this.kdModel.userAddresses.forEach((a) => { if (a.address === "") { this.deleteAddress(a) } });
        this.kdModel.userAddresses = this.kdModel.userAddresses.filter((p) => !p.isMarkedDeleted);
        if (this.kdModel.email === undefined || this.kdModel.email === null || this.kdModel.email === "") {
            this.messageService.info(this.constants.auth.enterEmail);
        }
        else if (this.kdModel.lastName === undefined || this.kdModel.lastName === null || this.kdModel.lastName === "") {
            this.messageService.info(this.constants.auth.enterLastName);
        }
        else if (this.kdModel.firstName === undefined || this.kdModel.firstName === null || this.kdModel.firstName === "") {
            this.messageService.info(this.constants.auth.enterName);
        }
        else if (this.kdModel.unchangedMail !== this.kdModel.email) {
            this.profileApiService.updateProfile(this.kdModel)
                .then((response) => {
                    return this.accountApiService.userExist({ UserName: this.kdModel.email });                       
                }).then((response) => {
                    if (response.data.exist) {
                        this.messageService.info("Пользователь с тaким email уже существует");
                    } else {
                        this.isShowModalChangeEmail = true;
                    }
                })
                .catch((error) => {
                    console.log(error);
                });
        }
        else {
            this.updateProfile();
        }
    }

    updateProfile(): void {
        this.profileApiService.updateProfile(this.kdModel)
            .then((response) => {
                this.kdEdited = false;
                this.getProfile();
            });
    }

    notSaveEmail(): void {
        this.kdModel.email = this.kdModel.unchangedMail;
    }

    getProfile(): void {
        this.profileApiService.getProfile({ UserId: this.kdModel.id }).then((response) => {
            this.kdModel = response;
            this.kdModel.unchangedMail = this.kdModel.email;
        });
    }

    public cancel() {
        this.kdEdited = false;
        this.getProfile();
    }
}