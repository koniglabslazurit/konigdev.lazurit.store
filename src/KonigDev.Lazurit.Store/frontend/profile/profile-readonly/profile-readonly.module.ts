﻿import * as angular from "angular";

import controller from "./profile-readonly.controller";

export const name = "kd.profile.readonly";
const controllerName = "profileReadonlyController";
const componentName = "kdProfileReadonly";

angular
    .module(name, [ ])
    .controller(controllerName,
    [
        controller
    ])
    .component(componentName, {
        bindings : {
            kdModel: '=',
            kdEdited: '=',
            kdRestorePassword: '='
        },
        controller: controllerName,
        template: require("./profile-readonly.view.html")
    });