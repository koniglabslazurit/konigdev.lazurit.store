﻿export default class ProfileDataCtrl {
    private kdModel: any;
    private kdEdited: boolean;
    private kdRestorePassword: boolean;

    public constructor() {

    }

    private editData(): void {
        this.kdEdited = true;
    }

    public restoreSetVisible() {
        this.kdRestorePassword = true;
    }
};