﻿import * as angular from "angular";

import controller from "./profile.controller";

import * as profileServicesModule from "./profile-services/profile-services.module";
import * as profileReadonlyModule from "./profile-readonly/profile-readonly.module";
import * as profileEditModule from "./profile-edit/profile-edit.module";
import * as restorePasswordModule from "./profile-restore-password/profile-restore-password.module";

export const name = "kd.profile";

const controllerName = "profileController";
const componentName = "kdProfile";

angular
    .module(name,
    [
        profileEditModule.name,
        profileReadonlyModule.name,
        profileServicesModule.name,
        restorePasswordModule.name
    ])
    .controller(controllerName,
    [
        profileServicesModule.profileServiceName,
        controller
    ])
    .component(componentName, {
        bindings: {
        },
        controller: controllerName,
        template: require("./profile.view.html")
    });
