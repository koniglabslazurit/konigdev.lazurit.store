﻿import * as angular from "angular";

import * as authCommonModule from "../../auth/common/auth.common.module";

import profileRestorePasswordService from "./profile-restore-password.service";

import controller from "./profile-restore-password.controller";

import * as passwordValidator from "./profile-restore-password.validations"
import * as commonModule from "../../common/common.module";

export const name = "kd.profileRestorePassword";
const controllerName = "profileRestorePasswordController";
const componentName = "kdProfileRestorePassword";
const restorePasswordServiceName = "restorePasswordName";

angular
    .module(name, [
        "ui.mask",
        authCommonModule.name
    ])
    .controller(controllerName, [
        restorePasswordServiceName,
        commonModule.messageServiceName,
        authCommonModule.accountServiceName,
        authCommonModule.authNavigationFacadeName,
        controller
    ])
    .component(componentName, {
        bindings: {
            kdRestorePassword: '='
        },
        controller: controllerName,
        template: require("./profile-restore-password.view.html")
    })
    .service(restorePasswordServiceName, ["$http", profileRestorePasswordService])
    .directive("compareTo", passwordValidator.PasswordValidator.compareTo);