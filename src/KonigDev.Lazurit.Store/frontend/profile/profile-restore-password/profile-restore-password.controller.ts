﻿export default class ProfileRestorePasswordCtrl {
    public oldPassword: string;
    public newPassword: string;
    public confirmPassword: string;
    public kdRestorePassword: boolean;

    public constructor(private restorePasswordService: Profile.ProfileRestorePassword.IRestorePasswordService,
        private messageService: Common.Message.IMessage,
        private authApiService: Auth.Services.IApiService,
        private NavigationFacade: Auth.Services.INavigationFacade
    ) { }

    public restorePassword() {
        this.restorePasswordService
            .restorePassword(this.oldPassword, this.newPassword, this.confirmPassword)
            .then((response: any) => {
                this.kdRestorePassword = false;
                this.messageService.success("Пароль был успешно изменен");
                this.logout();
            })
            .catch((error: any) => {
                if (error.data.exceptionMessage) {
                    this.messageService.uiError(error.data.exceptionMessage);
                } else {
                    /* TODO вынести в константы */
                    this.messageService.uiError("Произошла ошибка при попытке изменения пароля");
                }
            });
    }

    logout() {
        this.authApiService.logout()
            .then((): void => {
                this.NavigationFacade.goToHome();
            })
            .catch((error: any) => {
                console.error("Ошибка обращения к серверу!", error);
            });
    }

    public cancelRestorePass() {
        this.kdRestorePassword = false;
    }
}