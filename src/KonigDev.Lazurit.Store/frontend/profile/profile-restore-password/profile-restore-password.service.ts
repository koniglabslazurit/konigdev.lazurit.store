﻿export default class RestorePasswordService implements Profile.ProfileRestorePassword.IRestorePasswordService {
    private restorePassUrl = '/api/account/restorePassword/';

    constructor(private $http: angular.IHttpService) { }

    restorePassword(oldPassword: string, newPassword: string, confirmPassword: string): angular.IPromise<any> {
        var formData = <Profile.ProfileRestorePassword.IDtoRestorePasswordRequest>{
            newPassword: newPassword,
            oldPassword: oldPassword,
            confirmPassword: confirmPassword
        }
        return this.$http
            .post(this.restorePassUrl, formData)
            .then((response) => {
                return response.data;
            });
    }
}