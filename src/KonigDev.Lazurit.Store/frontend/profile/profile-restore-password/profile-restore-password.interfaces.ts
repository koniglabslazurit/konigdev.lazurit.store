﻿namespace Profile.ProfileRestorePassword {
    export interface IDtoRestorePasswordRequest {
        oldPassword: string;
        newPassword: string;
        confirmPassword: string;
    }
    export interface IRestorePasswordService {
        restorePassword(oldPassword: string, newPassword: string, confirmPassword: string): angular.IPromise<any>;
    }
}