﻿export default class ProfileController {

    private isEdited: boolean = false;
    private isRestorePassword: boolean = false;
    private User: any;
    private UserId: any;
    private email: string;

    constructor(public ProfileService: any) {
        this.ProfileService.getProfile({ UserId: this.UserId }).then((response) => {
            this.User = response;
            this.User.unchangedMail = this.User.email;
        });
    }
}