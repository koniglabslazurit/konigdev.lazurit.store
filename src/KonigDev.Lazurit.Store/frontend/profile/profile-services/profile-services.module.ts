﻿import * as angular from "angular";

import service from "./profile.service";
import * as constantsModule from "../../constants/constants.module";
import * as commonModule from "../../common/common.module";

export const name = "kd.profile.services";
export const profileServiceName = "profileService";

angular
    .module(name, [ ])
    .service(profileServiceName, [
        '$http',
        constantsModule.serviceName,
        commonModule.messageServiceName,
        service
    ]);
