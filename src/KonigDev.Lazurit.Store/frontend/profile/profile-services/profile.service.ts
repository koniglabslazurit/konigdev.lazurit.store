﻿export default class ProfileService {

    private urlCrud: string = "/api/profile";

    constructor(public $http: angular.IHttpService,
        public constants: Constants.IConstants,
        public messageService: Common.Message.IMessage
    ) { }

    public getProfile(request: any): any {
        return this.$http
            .get(this.urlCrud, { params: request })
            .then((response) => {
                return response.data;
            });
    }

    public updateProfile(request: any): any {
        var data = request;
        return this.$http
            .put(this.urlCrud, data)
            .then((response) => {
                return response.data;
            });
    }

    public saveProfile(request: any): any {
        var data = request;
        return this.$http
            .post(this.urlCrud, data)
            .then((response) => {
                return response.data;
            });
    }

    public checkProfileValidityForCartFull(profile: any): boolean {
        let isValid = true;
        if (!profile) {
            this.messageService.info(this.constants.auth.registrateOrLogin);
            isValid = false;
        }
        else {
            if (profile.firstName === undefined || profile.firstName === null || profile.firstName === "") {
                this.messageService.info(this.constants.auth.enterName);
                isValid = false;
            }
            if (profile.lastName === undefined || profile.lastName === null || profile.lastName === "") {
                this.messageService.info(this.constants.auth.enterLastName);
                isValid = false;
            }
            //if (profile.middleName === undefined || profile.middleName === null || profile.middleName === "") {
            //    this.messageService.info(this.constants.auth.enterMiddleName);
            //    isValid = false;
            //}
            if (profile.email === undefined || profile.email === null || profile.email === "") {
                this.messageService.info(this.constants.auth.enterEmail);
                isValid = false;
            }
            //phones
            if (profile.userPhones === undefined || profile.userPhones === null || profile.userPhones.length === 0) {
                this.messageService.info(this.constants.auth.enterPhoneNumber);
                isValid = false;
            }
            else {
                var defaultPhoneNumber = profile.userPhones.filter((i: any) => { return i.isDefault === true })[0].phone;
                if (defaultPhoneNumber === undefined || defaultPhoneNumber === null || defaultPhoneNumber === "") {
                    this.messageService.info(this.constants.auth.enterPhoneNumber);
                    isValid = false;
                }
            }
        }
        return isValid;
    }

    public checkProfileValidityForCartMini(profile: any): boolean {
        let isValid = true;
        if (!profile) {
            isValid = false;
        }
        else {
            if (profile.firstName === undefined || profile.firstName === null || profile.firstName === "") {
                isValid = false;
            }
            if (profile.lastName === undefined || profile.lastName === null || profile.lastName === "") {
                isValid = false;
            }
            //if (profile.middleName === undefined || profile.middleName === null || profile.middleName === "") {
            //    isValid = false;
            //}
            if (profile.email === undefined || profile.email === null || profile.email === "") {
                isValid = false;
            }
            //phones
            if (profile.userPhones === undefined || profile.userPhones === null || profile.userPhones.length === 0) {
                isValid = false;
            }
            else {
                var defaultPhoneNumber = profile.userPhones.filter((i: any) => { return i.isDefault === true })[0].phone;
                if (defaultPhoneNumber === undefined || defaultPhoneNumber === null || defaultPhoneNumber === "") {
                    isValid = false;
                }
            }
            //addresses
            if (profile.userAddresses === undefined || profile.userAddresses === null || profile.userAddresses.length === 0) {
                isValid = false;
            }
            else {
                var defaultAddressString = profile.userAddresses.filter((i: any) => { return i.isDefault === true })[0].address
                if (defaultAddressString === undefined || defaultAddressString === null || defaultAddressString === "") {
                    isValid = false;
                }
            }
        }
        return isValid;
    }
}