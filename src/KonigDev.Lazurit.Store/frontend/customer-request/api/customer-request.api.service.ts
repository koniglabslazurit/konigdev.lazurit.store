﻿import commands = CustomerRequest.Commands;
import dto = CustomerRequest.Dto;
import services = CustomerRequest.Services;
import queries = CustomerRequest.Queries;

export default class CustomerRequestApiService implements services.ICustomerRequestApiService {
    private _http: ng.IHttpService;
    private _apiUrlPrefix: string;

    constructor($http: ng.IHttpService) {
        this._http = $http;
        this._apiUrlPrefix = '/api/requests/';
    }

    getAll(query: queries.IGetAllCustomerRequestsQuery): ng.IPromise<dto.IDtoCustomerRequests> {
        return this._http
            .get(this._apiUrlPrefix)
            .then(response => response.data);
    }

    createCallRequest(command: commands.ICreateCustomerRequestCommand): ng.IPromise<void> {
        const url = `${ this._apiUrlPrefix }call`;
        return this._http
            .post(url, command)
            .then(() => { });
    }

    createDesignerOnlineRequest(command: commands.ICreateCustomerRequestCommand): ng.IPromise<void> {
        const url = `${ this._apiUrlPrefix }designer-online`;
        return this._http
            .post(url, command)
            .then(() => { });
    }

    createDesignerToHomeRequest(command: commands.ICreateCustomerRequestCommand): ng.IPromise<void> {
        const url = `${ this._apiUrlPrefix }designer-to-home`;
        return this._http
            .post(url, command)
            .then(() => { });
    }

    update(command: commands.IUpdateCustomerRequestStatusCommand): ng.IPromise<void> {
        const url = `${ this._apiUrlPrefix }${ command.id }`;
        const data = {
            status: command.status
        };
        return this._http
            .put(url, data)
            .then(() => { });
    }

    remove(command: commands.IRemoveCustomerRequestCommand): ng.IPromise<void> {
        const url = `${ this._apiUrlPrefix }${ command.id }`;
        return this._http
            .delete(url)
            .then(() => { });
    }
}
