﻿import * as angular from 'angular';

import Service from './customer-request.api.service';

export const name = 'kd.customerRequest.api';
export const serviceName = 'kd.customerRequest.apiSrv';

angular
    .module(name, [ ])
    .service(serviceName, [
        '$http',
        Service
    ]);
