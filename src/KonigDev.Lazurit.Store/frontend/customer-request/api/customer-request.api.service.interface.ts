﻿namespace CustomerRequest.Services {
    import commands = CustomerRequest.Commands;
    import dto = CustomerRequest.Dto;
    import queries = CustomerRequest.Queries;

    export interface ICustomerRequestApiService {
        getAll(query: queries.IGetAllCustomerRequestsQuery): ng.IPromise<dto.IDtoCustomerRequests>;
        createCallRequest(command: commands.ICreateCustomerRequestCommand): ng.IPromise<void>;
        createDesignerOnlineRequest(command: commands.ICreateCustomerRequestCommand): ng.IPromise<void>;
        createDesignerToHomeRequest(command: commands.ICreateCustomerRequestCommand): ng.IPromise<void>;
        update(command: commands.IUpdateCustomerRequestStatusCommand): ng.IPromise<void>;
        remove(command: commands.IRemoveCustomerRequestCommand): ng.IPromise<void>;
    }
}
