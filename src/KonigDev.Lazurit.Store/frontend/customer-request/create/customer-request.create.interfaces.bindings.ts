﻿namespace CustomerRequest.Create {
    export interface ICreateBindings {
        [key: string]: any;

        type: any;
    }
}
