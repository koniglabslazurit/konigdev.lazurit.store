﻿import commands = CustomerRequest.Commands;
import create = CustomerRequest.Create;
import services = CustomerRequest.Services;

export default class CustomerRequestCreateController implements ng.IController, create.ICreateBindings {
    private _q: ng.IQService;
    private _apiSrv: services.ICustomerRequestApiService;
    private _uibModal: any;
    private _constants: Constants.IConstants;

    type: string;

    createBind: (request: commands.ICreateCustomerRequestCommand) => ng.IPromise<void>;

    constructor(
        $q: ng.IQService,
        apiSrv: services.ICustomerRequestApiService,
        $uibModal: any,
        constants: Constants.IConstants
    ) {
        this._q = $q;
        this._apiSrv = apiSrv;
        this._uibModal = $uibModal;
        this._constants = constants;

        this.createBind = this.create.bind(this);
    }

    openModal(): void {
        const self = this;
        this._uibModal.open({
            template: `<kd-customer-request-create-common header="${ this.getHeader() }"
                                                          create-request="vm.create"
                                                          close-modal="$close()">
                       </kd-customer-request-create-common>`,
            controller: function() {
                this.create = self.createBind;
            },
            controllerAs: 'vm'
        });
    }

    create(request: commands.ICreateCustomerRequestCommand): ng.IPromise<void> {
        let promise: ng.IPromise<void>;
        switch (this.type) {
            case this._constants.customerRequests.types.call:
                promise = this._apiSrv
                    .createCallRequest(request);
                break;
            case this._constants.customerRequests.types.designerOnline:
                promise = this._apiSrv
                    .createDesignerOnlineRequest(request);
                break;
            case this._constants.customerRequests.types.designerHome:
                promise = this._apiSrv
                    .createDesignerToHomeRequest(request);
                break;
            default:
                promise = this._q
                    .reject(new Error(this._constants.customerRequests.messages.notSupportedType));
        }
        return promise;
    }

    getHeader(): string {
        let header: string = null;
        switch (this.type) {
            case this._constants.customerRequests.types.call:
                header = this._constants.customerRequests.modalHeaders.call;
                break;
            case this._constants.customerRequests.types.designerOnline:
                header = this._constants.customerRequests.modalHeaders.designerOnline;
                break;
            case this._constants.customerRequests.types.designerHome:
                header = this._constants.customerRequests.modalHeaders.designerHome;
                break;
        }
        return header;
    }
}
