﻿namespace CustomerRequest.CreateCommon {
    export interface ICreateCommonBindings {
        [key: string]: any;

        header: any;
        createRequest: any;
        closeModal: any;
    }
}
