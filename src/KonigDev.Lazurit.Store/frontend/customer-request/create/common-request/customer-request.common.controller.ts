﻿import commands = CustomerRequest.Commands;
import createCommon = CustomerRequest.CreateCommon;
import services = CustomerRequest.Services;

export default class CustomerRequestCreateCommon implements ng.IController, createCommon.ICreateCommonBindings {
    private _messageService: Common.Message.IMessage;
    private _constants: Constants.IConstants;

    header: string;
    createRequest: () => ((command: commands.ICreateCustomerRequestCommand) => ng.IPromise<void>);
    closeModal: () => void;

    request: commands.ICreateCustomerRequestCommand;
    isBusy: boolean;

    constructor(
        messageService: Common.Message.IMessage,
        constants: Constants.IConstants
    ) {
        this._messageService = messageService;
        this._constants = constants;
    }

    $onInit(): void {
        this.request = {
            customerFullName: '',
            email: '',
            phone: ''
        };
    }

    create(): void {
        this.isBusy = true;
        this.createRequest()(this.request)
            .then(() => {
                this.isBusy = false;
                this.closeModal();
                 this._messageService.success(this._constants.customerRequests.messages.requestCreated);
            })
            .catch((error: any) => {
                this._messageService.error(error.message, error);
            });
    }
}
