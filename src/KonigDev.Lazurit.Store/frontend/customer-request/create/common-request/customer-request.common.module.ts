﻿import createCommon = CustomerRequest.CreateCommon;

import * as angular from 'angular';

import * as commonModule from '../../../common/common.module';
import * as constantsModule from '../../../constants/constants.module';

import Controller from './customer-request.common.controller';

export const name = 'kd.customerRequest.createCommon';
const componentName = 'kdCustomerRequestCreateCommon';
const controllerName = 'kd.customerRequest.createCommonCtrl';

const template = require('./customer-request.common.html');
const bindings: createCommon.ICreateCommonBindings = {
    header: '@',
    createRequest: '&',
    closeModal: '&'
};

angular
    .module(name, [
        'ui.mask',
        commonModule.name,
        constantsModule.name
    ])
    .controller(controllerName, [
        commonModule.messageServiceName,
        constantsModule.serviceName,
        Controller
    ])
    .component(componentName, {
        controller: controllerName,
        controllerAs: 'vm',
        template,
        bindings
    });
