﻿import create = CustomerRequest.Create;

import * as angular from 'angular';

import * as constantsModule from '../../constants/constants.module';
import * as customerRequestApiCommonModule from '../api/customer-request.api.module';
import * as customerRequestCreateCommonModule from './common-request/customer-request.common.module';

import Controller from './customer-request.create.controller';

export const name = 'kd.customerRequest.create';
const componentName = 'kdCustomerRequestCreate';
const controllerName = 'kd.customerRequest.createCtrl';

const template = require('./customer-request.create.html');
const bindings: create.ICreateBindings = {
    type: '@'
};

angular
    .module(name, [
        constantsModule.name,
        customerRequestApiCommonModule.name,
        customerRequestCreateCommonModule.name
    ])
    .controller(controllerName, [
        '$q',
        customerRequestApiCommonModule.serviceName,
        '$uibModal',
        constantsModule.serviceName,
        Controller
    ])
    .component(componentName, {
        controller: controllerName,
        controllerAs: 'vm',
        template,
        transclude: true,
        bindings
    });
