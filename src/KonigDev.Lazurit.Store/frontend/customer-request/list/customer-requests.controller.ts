﻿import dto = CustomerRequest.Dto;
import services = CustomerRequest.Services;
import queries = CustomerRequest.Queries;

export default class CustomerRequestsController implements ng.IController {
    private _apiSrv: services.ICustomerRequestApiService;
    private _messageService: Common.Message.IMessage;

    customerRequests: dto.IDtoCustomerRequest[];
    requestStatuses: dto.IDtoCustomerRequestStatus[];

    isLoading: boolean;

    onUpdateBind: (updatedRequest: dto.IDtoCustomerRequest) => void;
    onRemoveBind: (removedRequest: dto.IDtoCustomerRequest) => void;

    constructor(
        apiSrv: services.ICustomerRequestApiService,
        messageService: Common.Message.IMessage
    ) {
        this._apiSrv = apiSrv;
        this._messageService = messageService;

        this.onUpdateBind = this.onUpdate.bind(this);
        this.onRemoveBind = this.onRemove.bind(this);
    }

    $onInit(): void {
        this.load()
            .then(data => {
                this.customerRequests = data.customerRequests;
                this.requestStatuses = data.customerRequestStatuses;
            });
    }

    load(): ng.IPromise<dto.IDtoCustomerRequests> {
        this.isLoading = true;
        const query: queries.IGetAllCustomerRequestsQuery = { };
        return this._apiSrv
            .getAll(query)
            .then(data => {
                this.isLoading = false;
                return data;
            })
            .catch((error: any) => {
                this._messageService.error(error.message, error);
                return null;
            });
    }

    onUpdate(updatedRequest: dto.IDtoCustomerRequest): void {
        const request = this.customerRequests
            .filter(p => p.id === updatedRequest.id)[0];
        if (request) {
            const index = this.customerRequests.indexOf(request);
            this.customerRequests[index] = updatedRequest;
        }
    }

    onRemove(removedRequest: dto.IDtoCustomerRequest): void {
        const request = this.customerRequests
            .filter(p => p.id === removedRequest.id)[0];
        if (request) {
            const index = this.customerRequests.indexOf(request);
            this.customerRequests.splice(index, 1);
        }
    }
}
