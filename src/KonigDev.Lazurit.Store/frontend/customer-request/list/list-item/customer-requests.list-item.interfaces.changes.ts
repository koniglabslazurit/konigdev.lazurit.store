﻿namespace CustomerRequest.ListItem {
    import dto = CustomerRequest.Dto;

    export interface IListItemChanges extends ng.IOnChangesObject, IListItemBindings {
        [key: string]: any;

        request: ng.IChangesObject<dto.IDtoCustomerRequest>;
        statuses: ng.IChangesObject<dto.IDtoCustomerRequestStatus[]>;
        onUpdated: any;
        onRemoved: any;
    }
}
