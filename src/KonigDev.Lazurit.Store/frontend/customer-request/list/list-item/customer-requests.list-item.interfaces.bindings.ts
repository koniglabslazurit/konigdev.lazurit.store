﻿namespace CustomerRequest.ListItem {
    export interface IListItemBindings {
        [key: string]: any;

        request: any;
        statuses: any;
        onUpdated: any;
        onRemoved: any;
    }
}
