﻿import commands = CustomerRequest.Commands;
import dto = CustomerRequest.Dto;
import listItem = CustomerRequest.ListItem;
import services = CustomerRequest.Services;

export default class CustomerRequestsListItemController implements ng.IController, listItem.IListItemBindings {
    private _apiSrv: services.ICustomerRequestApiService;
    private _messageService: Common.Message.IMessage;
    private _constants: Constants.IConstants;

    request: dto.IDtoCustomerRequest;
    statuses: dto.IDtoCustomerRequestStatus[];
    onUpdated: () => ((p: dto.IDtoCustomerRequest) => void);
    onRemoved: () => ((p: dto.IDtoCustomerRequest) => void);

    isBusy: boolean;

    constructor(
        apiSrv: services.ICustomerRequestApiService,
        messageService: Common.Message.IMessage,
        constants: Constants.IConstants
    ) {
        this._apiSrv = apiSrv;
        this._messageService = messageService;
        this._constants = constants;
    }

    $onInit(): void {
        this.isBusy = false;
    }

    $onChange(changes: listItem.IListItemChanges): void {
        if (changes.request) {
            this.request = changes.request.currentValue;
        }
        if (changes.statuses) {
            this.statuses = changes.statuses.currentValue;
        }
    }

    update(status: string): void {
        this.isBusy = true;
        const command: commands.IUpdateCustomerRequestStatusCommand = {
            id: this.request.id,
            status
        };
        this._apiSrv
            .update(command)
            .then(() => {
                this.isBusy = false;
                this.onUpdated()(this.request);
                this._messageService.success(this._constants.customerRequests.messages.statusUpdateSuccess);
            })
            .catch((error: any) => {
                this._messageService.uiError(this._constants.customerRequests.messages.statusUpdateError);
                this._messageService.error(error.data.message, error);
            });
    }

    remove(): void {
        const command: commands.IRemoveCustomerRequestCommand = {
            id: this.request.id
        };
        this._apiSrv
            .remove(command)
            .then(() => {
                this.isBusy = false;
                this.onRemoved()(this.request);
                this._messageService.success(this._constants.customerRequests.messages.removeSuccess);
            })
            .catch((error: any) => {
                this._messageService.uiError(this._constants.customerRequests.messages.removeError);
                this._messageService.error(error.data.message, error);
            });
    }
}
