﻿import listItem = CustomerRequest.ListItem;

import * as angular from 'angular';

import * as commonModule from '../../../common/common.module';
import * as constantsModule from '../../../constants/constants.module';
import * as customerRequestApiModule from '../../api/customer-request.api.module';

import Controller from './customer-requests.list-item.controller';

export const name = 'kd.customerRequest.customerRequestsListItem';
const componentName = 'kdCustomerRequestListItem';
const controllerName = 'kd.customerRequest.customerRequestsListItemCtrl';

const template = require('./customer-requests.list-item.html');
const bindings: listItem.IListItemBindings = {
    request: '<',
    statuses: '<',
    onUpdated: '&',
    onRemoved: '&'
};

angular
    .module(name, [
        commonModule.name,
        constantsModule.name,
        customerRequestApiModule.name
    ])
    .controller(controllerName, [
        customerRequestApiModule.serviceName,
        commonModule.messageServiceName,
        constantsModule.serviceName,
        Controller
    ])
    .component(componentName, {
        controller: controllerName,
        controllerAs: 'vm',
        template,
        bindings
    });
