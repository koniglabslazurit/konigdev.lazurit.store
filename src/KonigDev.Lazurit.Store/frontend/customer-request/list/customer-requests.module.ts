﻿import * as angular from 'angular';

import * as constantsModule from '../../constants/constants.module';
import * as customerRequestApiModule from '../api/customer-request.api.module';
import * as customerRequestListItemModule from './list-item/customer-requests.list-item.module';

import Controller from './customer-requests.controller';

export const name = 'kd.customerRequest.list';
const componentName = 'kdCustomerRequests';
const controllerName = 'kd.customerRequest.listCtrl';

const template = require('./customer-requests.html');

angular
    .module(name, [
        constantsModule.name,
        customerRequestApiModule.name,
        customerRequestListItemModule.name
    ])
    .controller(controllerName, [
        customerRequestApiModule.serviceName,
        constantsModule.serviceName,
        Controller
    ])
    .component(componentName, {
        controller: controllerName,
        controllerAs: 'vm',
        template
    });
