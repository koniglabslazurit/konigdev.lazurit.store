﻿namespace CustomerRequest.Dto {
    export interface IDtoCustomerRequest {
        id: string;
        creationTime: Date;
        customerFullName: string;
        email: string;
        phone: string;
        typeTechName: string;
        type: string;
        statusTechName: string;
    }
}
