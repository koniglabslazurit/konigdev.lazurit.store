﻿namespace CustomerRequest.Dto {
    export interface IDtoCustomerRequestStatus {
        techName: string;
        name: string;
    }
}
