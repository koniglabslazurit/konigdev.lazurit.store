﻿namespace CustomerRequest.Dto {
    export interface IDtoCustomerRequests {
        customerRequests: IDtoCustomerRequest[],
        customerRequestStatuses: IDtoCustomerRequestStatus[]
    }
}
