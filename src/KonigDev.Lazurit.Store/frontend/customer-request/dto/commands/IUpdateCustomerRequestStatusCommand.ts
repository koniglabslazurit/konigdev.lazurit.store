﻿namespace CustomerRequest.Commands {
    export interface IUpdateCustomerRequestStatusCommand {
        id: string;
        status: string;
    }
}
