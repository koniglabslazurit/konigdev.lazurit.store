﻿namespace CustomerRequest.Commands {
    export interface IRemoveCustomerRequestCommand {
        id: string;
    }
}
