﻿namespace CustomerRequest.Commands {
    export interface ICreateCustomerRequestCommand {
        customerFullName: string;
        email: string;
        phone: string;
    }
}
