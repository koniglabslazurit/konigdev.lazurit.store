export interface IUploadFileDto {
    objectId: string;
    type: string;
}
