import { IUploadFileDto } from "../dto/upload-file.dto";

export const imageApiServiceName = "kd.image.api";
export interface IImageApiService {
    upload(parameters: IUploadFileDto, file: File): ng.IPromise<any>;
}