// dependencies
import * as angular from "angular";

import { ImageApiService } from "./image.api.service";
import { imageApiServiceName } from "./image.api.interface";

// constants
export const name = "kd.image.services";

// module
angular
    .module(name, [])
    .service(imageApiServiceName, ImageApiService);