import { IImageApiService } from "./image.api.interface";
import { $http } from "../../globals/angular.names";
import { IUploadFileDto } from "../dto/upload-file.dto";

export class ImageApiService implements IImageApiService {
    private readonly _http: ng.IHttpService;
    private readonly _baseUrl: string;

    constructor(http: ng.IHttpService) {
        this._http = http;
        this._baseUrl = "/api/images";
    }

    upload(parameters: IUploadFileDto, file: File): angular.IPromise<any> {
        let requestUrl = `${this._baseUrl}/upload`;
        let formData = new FormData();
        formData.append("file", file);

        let requestConfig: ng.IRequestShortcutConfig = {
            params: parameters,
            headers: {
                'Content-Type': undefined
            }
        };
        return this._http
            .post(requestUrl, formData, requestConfig);
    }
}
ImageApiService.$inject = [$http];