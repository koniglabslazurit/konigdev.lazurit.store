﻿export default class ImageManagerService {

    private urlCrud: string = "/api/productcontent/";
    private urlList: string = "/api/productcontent/list/";

    constructor(public $http: angular.IHttpService) { }

    getProductsContent(id: string): angular.IPromise<any> {
        return this.$http.get(this.urlList,
            {
                params: { "CollectionVariantId": id }
            })
            .then((response: any) => {
                return response.data;
            });
    };

    addProjectContent(model) {
        return this.$http({
            method: "POST",
            url: this.urlCrud,
            data: model
        }).then((response) => {
            return response.data;
        });
    };

    saveCoordinates(model) {
        return this.$http({
            method: "PUT",
            url: this.urlCrud,
            data: model
        }).then((response) => {
            return response.data;
        });
    };
};