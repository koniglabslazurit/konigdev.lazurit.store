﻿export default class ImageManagerAreasControler {

    private productsContext: Array<any>;
    private collectionVariantId: string;
    private productUrl: string;
    private modelAdding: Dto.Image.ProductContentAdding.IProductContentAdding;

    constructor(public $scope, public ImageManagerService) {
        var me = this;
        $scope.ctrl = me;
        me.productUrl = "/api/images/productinvariant/:objectId/InInterior";

        $scope.selector = {};
        $scope.drawer = [];
        $scope.cropRect = function () {
            $scope.cropResult = $scope.selector.crop();
        };

        me.collectionVariantId = document.getElementById("areaForm").getAttribute("collectionVariantId");

        me.getProductsAndDraw();
    }

    getProductsAndDraw() {
        var me = this;
        me.ImageManagerService.getProductsContent(me.collectionVariantId).then((products) => {
            me.productsContext = products.map((p) => {
                p.isEditing = false;
                return p;
            });                                                         
            me.drawProducts();
        });
    };

    edit(productId) {
        var me = this;
        var prod = me.productsContext.filter((p) => { return p.productId === productId })[0];
        prod.isEditing = true;
    };

    cancel(productId) {
        var me = this;
        var select = me.$scope.selector;
        select.clear();
        var prod = me.productsContext.filter((p) => { return p.productId === productId })[0];
        prod.isEditing = false;
    };

    bindArea(objectId, productId) {
        var me = this;
        var main_image = document.getElementById("main_image");
        var img_width = main_image.offsetWidth;
        var img_height = main_image.offsetHeight;
        var one_percent_width = img_width / 100;
        var one_percent_height = img_height / 100;
        var select = me.$scope.selector;
        var model = {
            id: objectId,
            left: (select.x1 / one_percent_width).toFixed(),
            top: (select.y1 / one_percent_height).toFixed(),
            width: ((select.x2 - select.x1) / one_percent_width).toFixed(),
            height: ((select.y2 - select.y1) / one_percent_height).toFixed()
        };
        me.ImageManagerService.saveCoordinates(model).then(
            (response) => {
                me.$scope.selector.clear();
                me.productsContext.filter((p) => { return p.productId === productId })[0].isEditing = false;
                alert("Область успешно прикреплена");
            }, (err) => {
                alert("Не удалось прикрепить область");
            });
    };

    drawProducts() {
        var me = this;
        for (var product of me.productsContext) {
            if (product.objectId) {
                product.src = me.productUrl.replace(":objectId", product.objectId);
            };
        }
    };

    loadProductImage(productId, type) {
        var me = this;
        me.modelAdding = { productId: productId, collectionVariantId: me.collectionVariantId };
        me.ImageManagerService.addProjectContent(me.modelAdding)
            .then(
            (response) => {
                me.uploadFile(productId, response.message, type);
            }, (err) => {
                alert("Не удалось сохранить изображение");
            });
    };

    uploadFile(productId, objectId, type) {
        var me = this;

        var inputElement = (<HTMLInputElement>document.getElementById("file_" + productId));
        if (!inputElement.files) {
            alert("Не выбран файл!");
            return;
        }
        var file = inputElement.files[0];

        var formData = new FormData();
        formData.append("file", file);

        console.log(file);

        $.ajax({
            url: '/ImageManager/upload?ObjectId=' + objectId + "&type=" + type,
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: (response) => {
                if (response.Success) {
                    me.showImage(productId, objectId);
                    alert("Файл загружен");
                }
                else {
                    alert(response.Message);
                }
            },
            error: (error) => {
                console.log(error);
            }
        });
    };

    showImage(productId, objectId) {

        var me = this;
        var prod = me.productsContext.filter((p) => { return p.productId === productId })[0];
        prod.objectId = objectId;
        prod.src = me.productUrl.replace(":objectId", prod.objectId);

        var imgElement = document.getElementById("image_" + productId);
        imgElement.setAttribute("src", me.productUrl.replace(":objectId", objectId));
    }
};