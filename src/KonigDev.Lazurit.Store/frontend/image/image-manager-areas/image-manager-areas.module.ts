﻿import * as angular from "angular";

import controller from "./image-manager-areas.controller";
import service from "./image-manager-areas.service";


export const name = "kd.image.manager.areas";
const imageManagerAreasControlerName = "imageManagerAreasControler";
export const imageManagerAreasServiceName = "imageManagerAreasService";

angular
    .module(name, [ ])
    .service(imageManagerAreasServiceName, ['$http', service])
    .controller(imageManagerAreasControlerName,
    [                                        
        "$scope", imageManagerAreasServiceName,
        controller
    ]);
