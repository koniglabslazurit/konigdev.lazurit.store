﻿import * as angular from "angular";

import service from "./image.service";

export const name = "kd.image";
export const serviceName = "imageService";

angular
    .module(name, [ ])
    .service(serviceName,
    [
        '$http',
        service
    ]);
