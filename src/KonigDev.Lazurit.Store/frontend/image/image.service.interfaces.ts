﻿namespace Product.Import {
    export interface IImageService {
        getAvialableImages(productId): angular.IPromise<any>;
        uploadImage(model: Dto.Image.IUploadFileModel, formData: FormData): angular.IPromise<any>;
    }
}