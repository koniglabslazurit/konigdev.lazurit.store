﻿export default class ImageService implements Product.Import.IImageService {

    private url: string = "/api/images/filecontent";
    private uploadImageUrl: string = "/api/images/upload"

    constructor(public $http: angular.IHttpService) { }

    public getAvialableImages(productId): angular.IPromise<any> {
        return this.$http
            .get(this.url + '/' + productId, { params: productId })
            .then((response) => {
                return response.data;
            });
    };

    public uploadImage(model: Dto.Image.IUploadFileModel, formData: FormData): angular.IPromise<any> {
        return this.$http({
            method: "POST",
            url: this.uploadImageUrl,
            params: model,
            data: formData,
            headers: {
                'Content-Type': undefined
            }
        }).then((response: any) => {
            return response.status;
        });
    };
};