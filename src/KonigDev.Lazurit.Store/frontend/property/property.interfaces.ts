﻿namespace Property.Services {
    export interface IPropertyApiService {
        getProperties(params: any): angular.IPromise<Array<Dto.Property.DtoPropertyItem>>;
    }
}