﻿export default class PropertyService implements Property.Services.IPropertyApiService {

    private url: string = "/api/property";

    constructor(public $http: angular.IHttpService) {
    }

    public getProperties(data: Dto.Property.GetPropertiesQuery): angular.IPromise<Array<Dto.Property.DtoPropertyItem>> {
        return this.$http
            .get(this.url, { params: data })
            .then((response: angular.IHttpPromiseCallbackArg<Array<Dto.Property.DtoPropertyItem>>) => {
                return response.data;
            });
    };
}