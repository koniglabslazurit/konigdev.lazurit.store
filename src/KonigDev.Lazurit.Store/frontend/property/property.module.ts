﻿import * as angular from "angular";

import service from "./property.api.service";

export const name = "kd.property";
export const serviceName = "propertyService";

angular
    .module(name, [ ])
    .service(serviceName,
    [
        "$http",
        service
    ]);
