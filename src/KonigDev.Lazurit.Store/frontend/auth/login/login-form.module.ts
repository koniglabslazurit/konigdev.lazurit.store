﻿import * as angular from "angular";

import * as authCommonModule from "../common/auth.common.module";
import controller from "./login-form.controller";
import messageService from "../../common/services/message.service";
import * as focusDirective from "../../common/directive/focus/focus.module";

export const name = "kd.login";

const loginControllerName = "loginFormController";
const loginComponentName = "kdLogin";
const messageFacadeName = "MessageFacade";

angular
    .module(name, [
        authCommonModule.name,
        focusDirective.name
    ])
    .controller(loginControllerName, [
        authCommonModule.accountServiceName,
        messageFacadeName,
        authCommonModule.authNavigationFacadeName,
        controller
    ])
    .component(loginComponentName, {
        bindings: {
            returnUrl: '<',
            loginFormIsShow: '<',
            isAuthenticated: '<'
        },
        controller: loginControllerName,
        template: require("./login-form.view.html")
    })
    .service(messageFacadeName, ["toastr", messageService]);
