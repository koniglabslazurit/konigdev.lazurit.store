﻿export default class LoginCtrl {
    public userLogin: string = "";
    public userPhone: string = "";
    public userPassword: string = "";
    public isAuthenticated: boolean;
    public loginFormIsShow: boolean;
    private returnUrl: string = "";
    private loginByUserPhone: boolean = false;

    public isAdmin: boolean = false;
    public isMarketer: boolean = false;
    public isContentManager: boolean = false;

    constructor(private authApiService: Auth.Services.IApiService, private MessageFacade: Common.Message.IMessage, private NavigationFacade: Auth.Services.INavigationFacade) {
        this.getCurrentUserInfo();
    }

    public login() {
        var _this = this;
        if (_this.loginByUserPhone) {
            _this.authApiService
                .loginByPhone(_this.userPhone, _this.userPassword, _this.returnUrl)
                .then((response: any) => {
                    _this.parseLoginResult(response);
                })
                .catch((error: any) => {
                    _this.MessageFacade.error("Ошибка обращения к серверу!", error);
                });
        } else {
            _this.authApiService
                .login(_this.userLogin, _this.userPassword, _this.returnUrl)
                .then((response: any) => {
                    _this.parseLoginResult(response)
                })
                .catch((error: any) => {
                    _this.MessageFacade.error("Ошибка обращения к серверу!", error);
                });
        }
    }

    public logout() {
        this.authApiService.logout()
            .then((): void => {
                this.getCurrentUserInfo();
                this.isAuthenticated = false;
                this.NavigationFacade.goToHome();
            })
            .catch((error: any) => {
                this.MessageFacade.error("Ошибка обращения к серверу!", error);
            });
    };

    public changeLoginFormVisible() {
        this.loginFormIsShow = !this.loginFormIsShow;
    }

    public goToRegisterState() {
        this.loginFormIsShow = false;
        this.NavigationFacade.goToRegister();
    }

    public goToRecoveryState() {
        this.loginFormIsShow = false;
        this.NavigationFacade.goToRecovery();
    }

    private goToRegionLoginState() {
        this.loginFormIsShow = false;
        this.NavigationFacade.goToRegionLogin();
    }

    private goToVerifSellerState() {
        this.loginFormIsShow = false;
        this.NavigationFacade.goToVerifSellerState();
    }

    private parseLoginResult(response: any) {
        if (response.isAuthentificate) {

            this.getCurrentUserInfo();

            this.loginFormIsShow = false;
            if (response.isRegion) {
                this.goToRegionLoginState();
            }
            if (response.requiresVerification) {
                this.goToVerifSellerState();
            }
            if (response.returnUrl) {
                window.location.href = response.returnUrl;
            }
        } else {
            if (response && response.message)
                this.MessageFacade.uiError(response.message);
        }
        this.isAuthenticated = response.isAuthentificate;
    }

    private getCurrentUserInfo() {
        this.authApiService.getCurrentUserInfo()
            .then((response: any) => {
                if (response.data.roles) {
                    this.mapRoles(response.data.roles);
                } else {
                    this.mapRoles([]);
                }
            }).catch((error: any) => {
                this.mapRoles([]);
                console.log(error);
            })
    };

    public functionalityInDev() {
        this.MessageFacade.functionalityInDev();
    };

    /* todo вынести в отдельный сервис, вынести роли в константы! */
    private mapRoles(roles: Array<string>) {
        this.isAdmin = false;
        this.isMarketer = false;
        this.isAdmin = roles.filter((i: string) => { return i === "admin" }).length > 0;
        this.isMarketer = roles.filter((i: string) => { return i === "marketer" }).length > 0;
        this.isContentManager = roles.filter((i: string) => { return i === "contentManager" }).length > 0;
    };
};
