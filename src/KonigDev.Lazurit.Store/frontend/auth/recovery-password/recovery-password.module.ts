import * as angular from "angular";

import * as authCommonModule from "../common/auth.common.module";
import controller from "./recovery-password.controller"
import messageService from "../../common/services/message.service"

export const name = "kd.recovery-password";

const recoveryControllerName = "recoveryPassController";
export const recoveryComponentName = "kdRecoveryPassword";
const messageFacadeName = "messageFacade";

angular
    .module(name, [
        authCommonModule.name
    ])
    .controller(recoveryControllerName, [
        authCommonModule.accountServiceName,
        messageFacadeName,
        controller
    ])
    .component(recoveryComponentName, {
        bindings: {
            modalInstance: "<"
        },
        controller: recoveryControllerName,
        template: require("./recovery-password.view.html")
    })
    .service(messageFacadeName, ["toastr", messageService]);
