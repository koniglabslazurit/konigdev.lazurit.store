﻿export default class RecoveryPasswordCtrl {
    private userEmail: string = "";
    private isInprogress: boolean = false;
    public modalInstance: any;
    private recoveryPasswordSuccess = "Ваш новый пароль отправлен по почте.";
    private recoveryPasswordFail = "Не получилось восстановить пароль.";
    private serverError = "Ошибка обращения к серверу!";

    constructor(private RecoveryPasswordService: any, private MessageFacade:any) {
    }

    private recoveryPassword() {

        this.isInprogress = true;

        this.RecoveryPasswordService
            .recoveryPassword(this.userEmail)
            .then(response => {
                if (response.isAccepted) {
                    this.modalInstance.dismiss('cancel');
                    this.MessageFacade.info(this.recoveryPasswordSuccess);
                    this.isInprogress = false;
                } else {
                    this.isInprogress = false;
                    if (response && response.message) {
                        this.MessageFacade.error(this.recoveryPasswordFail, response.message);
                    }
                    console.log(response);
                }
            }, error => {
                this.isInprogress = false;
                this.MessageFacade.error(this.serverError, error);
            });
    };

    public closePopup() {
        this.modalInstance.dismiss('cancel');
    }
};