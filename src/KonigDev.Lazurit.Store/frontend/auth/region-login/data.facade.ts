﻿//TODO подумать над названием
export default class DataFacade {
    constructor(private UserService: any, private CityService: any, private ShowRoomService: any, private AccountService: any) {
    }

    public getShowRoom(cityId: string) {
        return this.ShowRoomService.getShowRoom(cityId);
    }

    public getUsers(showroomId: string) {
        return this.UserService.getUsers(showroomId);
    }

    public getCities() {
        return this.CityService.getAll();
    }

    public login(user: any, password: string) {
        return this.AccountService.loginById(user, password);
    }
}