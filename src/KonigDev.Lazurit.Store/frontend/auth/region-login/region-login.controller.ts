﻿export  default class RegionLoginCtrl {
    private userPassword: string;
    private city: any;
    private cities: any = [];
    private shop: any;
    private shops: any = [];
    private user: any;
    private users: any = [];
    private serverErrorMessage = "Ошибка обращения к серверу!";
    private serverErrorResponseMessage = "Не правильный ответ от сервера!";

    constructor(private DataFacade:any, private MessageFacade:any) {
        this.userPassword = "";
        this.getCities();
    }

    private getCities() {
        this.DataFacade.getCities()
            .then(response => {
                this.cities = response;
                console.log(this.cities);
            }, error => {
                this.MessageFacade.error(this.serverErrorMessage, error);
            });
    }

    private getShops() {
        this.DataFacade.getShops(this.city)
            .then(response => {
                this.shops = response;
                this.shop = undefined;
            })
            .catch(error => {
                this.MessageFacade.error(this.serverErrorMessage, error);
            });
    }

    private getUsers() {
        this.DataFacade.getUsers(this.shop)
            .then(response => {
                this.users = response;
                this.user = undefined;
            }, error => {
                this.MessageFacade.error(this.serverErrorMessage, error);
            });
        this.user = undefined;
    }

    private login() {
        this.DataFacade.login(this.user, this.userPassword)
            .then(response => {
                if (response.isAuthentificate) {
                    window.location.href = response.returnUrl;
                } else {
                    if (response && response.message)
                        console.log(response.message);
                    this.MessageFacade.error(this.serverErrorResponseMessage, response.message);
                }
            }, error => {
                this.MessageFacade.error(this.serverErrorMessage, error);
            });
    }
}