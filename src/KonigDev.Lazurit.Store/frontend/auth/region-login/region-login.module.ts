﻿import * as angular from "angular";

import * as authCommonModule from "../common/auth.common.module";
import regController from "./region-login.controller";
import dataFacade from "./data.facade";
import userService from "../../common/services/user.service";
import showRoomService from "../../common/services/showRoom.service";
import cityService from "../../common/services/city.service";
import messageService from "../../common/services/message.service"

export const name = "kd.region-login";

const regLoginControllerName = "regLoginController";
export const regLoginComponentName = "kdRegionLogin";
const userServiceName = "UserService";
const showRoomServiceName = "ShowRoomService";
const cityServiceName = "CityService";
const dataFacadeName = "DataFacade";
const messageFacadeName = "MessageFacade";

angular
    .module(name, [
        authCommonModule.name
    ])
    .controller(regLoginControllerName, [
        dataFacadeName,
        messageFacadeName,
        regController
    ])
    .component(regLoginComponentName, {
        bindings: {
            isAuthenticated: '<',
            returnUrl: '<'
        },
        controller: regLoginControllerName,
        template: require("./region-login.view.html")
    })
    .service(messageFacadeName, ["toastr", messageService])
    .service(showRoomServiceName, ["$http", showRoomService])
    .service(userServiceName, ["$http", userService])
    .service(cityServiceName, ["$http", cityService])
    .service(dataFacadeName, [
        userServiceName,
        cityServiceName,
        showRoomServiceName,
        authCommonModule.accountServiceName,
        dataFacade
    ]);
