﻿export default class RegisterCtrl {
    public userEmail: string = "";
    public userName: string = "";
    public userPhone: string = "";
    public isInprogress: boolean = false;
    public modalInstance: any;
    private registrationFailMessage = "Не удалось зарегистрироваться";
    private serverFailMessage = "Ошибка обращения к серверу!";

    constructor(private accountApiService: Auth.Services.IApiService, private MessageFacade: any) { }

    public register(): void {
        this.isInprogress = true;
        if (!this.userEmail) {
            this.MessageFacade.error("Введите корректный email");
            return;
        }
        if (!this.userName) {
            this.MessageFacade.error("Введите имя");
            return;
        }
        if (!this.userPhone) {
            this.MessageFacade.error("Введите телефон");
            return;
        }

        this.isInprogress = true;
        this.accountApiService.userExist({ userName: this.userEmail, phone: this.userPhone })
            .then((response: any) => {
                if (response.data.exist === true) {
                    this.MessageFacade.error(response.data.message);
                    return { valid: false };
                } else return { valid: true };
            })
            .then((response: any) => {
                if (response.valid === true) {
                    return this.accountApiService.register(this.userEmail, this.userName, this.userPhone);
                } else return false;
            })
            .then((response: any) => {
                if (response.isAccepted) {
                    this.isInprogress = false;
                    this.MessageFacade.info(response.message);

                    this.modalInstance.dismiss('cancel');
                } else {
                    this.isInprogress = false;
                    if (response && response.message) {
                        this.MessageFacade.error(response.message, response.message);
                    }
                    console.log(response);
                }
            })
            .catch((error: any) => {
                this.isInprogress = false;
                this.MessageFacade.error(this.serverFailMessage, error);
            });
    }

    public closePopup(): void {
        this.modalInstance.dismiss('cancel');
    }
}
