﻿import * as angular from "angular";

import * as authCommonModule from "../common/auth.common.module";
import controller from "./register.controller";
import messageService from "../../common/services/message.service"

export const name = "kd.register";

const registerControllerName = "registerCtrl";
export const registerComponentName = "kdRegister";
const messageFacadeName = "messageFacade";

angular
    .module(name, [
        authCommonModule.name
    ])
    .controller(registerControllerName, [
        authCommonModule.accountServiceName,
        messageFacadeName,
        controller
    ])
    .component(registerComponentName, {
        bindings: {
            modalInstance: "<"
        },
        controller: registerControllerName,
        template: require("./register.view.html")
    })
    .service(messageFacadeName, ["toastr", messageService]);
