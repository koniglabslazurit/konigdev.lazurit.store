﻿export default class ChangeEmail {
    private isInprogress: boolean = false;
    public isShown: boolean;
    public email: any;
    public onAccept: () => () => void;
    public onReject: () => () => void;
    private changeEmailSuccess = "Ha Ваш новый e-mail отправлен запрос на подтверждение e-mail.";
    private changeEmailFail = "Не получилось отправить запрос на подтверждение e-mail.";
    private serverError = "Ошибка обращения к серверу!";

    constructor(private ChangeEmailService: any, private MessageFacade: any, private NavigationFacade: Auth.Services.INavigationFacade) {

    }

    private changeEmail() {

        this.isInprogress = true;
        var emailModel = { Email: this.email };
        this.ChangeEmailService
            .changeEmail(emailModel)
            .then((response: any) => {
                if (response.isAccepted) {
                    this.isShown = false;
                    this.MessageFacade.info(this.changeEmailSuccess);
                    this.isInprogress = false;
                    this.onAccept();
                    this.NavigationFacade.goToHome();
                } else {
                    this.isInprogress = false;
                    this.isShown = false;
                    if (response && response.message) {
                        this.MessageFacade.error(response.message);
                    }
                    console.log(response);
                    this.onReject();
                }
            })
            .catch((error: any) => {
                this.isShown = false;
                this.isInprogress = false;
                this.MessageFacade.error(this.serverError, error);
                this.onReject();
            });
    };

    public closePopup() {
        this.isShown = false;
    }
};