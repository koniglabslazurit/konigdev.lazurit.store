﻿import * as angular from "angular";

import controller from "./change-email.controller"
import * as authCommonModule from "../common/auth.common.module";
import messageService from "../../common/services/message.service"

export const name = "kd.changeEmail";

const changeEmailControllerName = "changeEmailController";
export const changeEmailComponentName = "kdChangeEmail";
const accountServiceName = "AccountService";
const messageFacadeName = "messageFacade";
const authNavigationFacadeName = "authNavigation";

angular
    .module(name, [
        authCommonModule.name
    ])
    .controller(changeEmailControllerName, [
        authCommonModule.accountServiceName,
        messageFacadeName,
        authCommonModule.authNavigationFacadeName,
        controller
    ])
    .component(changeEmailComponentName, {
        bindings: {
            email: "<",
            isShown: "=",
            onAccept: "&",
            onReject: "&"
        },
        controller: changeEmailControllerName,
        template: require("./change-email.view.html")
    })
    .service(messageFacadeName, ["toastr", messageService]);
