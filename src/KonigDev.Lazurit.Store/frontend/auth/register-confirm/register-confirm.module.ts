import * as angular from "angular";

import * as authCommonModule from "../common/auth.common.module";
import controller from "./register-confirm.controller";
import messageService from "../../common/services/message.service"

export const name = "kd.register-confirm";

const regConfirmControllerName = "regConfirmCtrl";
export const regConfirmComponentName = "kdRegisterConfirm";
const messageFacadeName = "MessageFacade";

angular
    .module(name, [
        authCommonModule.name
    ])
    .controller(regConfirmControllerName, [
        authCommonModule.accountServiceName,
        messageFacadeName,
        controller
    ])
    .component(regConfirmComponentName, {
        bindings: {
            login: '<',
            token: '<',
            id: '<',
            resolve: "<",
            modalInstance: "<"
        },
        controller: regConfirmControllerName,
        template: require("./register-confirm.view.html")
    })
    .service(messageFacadeName, ["toastr", messageService]);
