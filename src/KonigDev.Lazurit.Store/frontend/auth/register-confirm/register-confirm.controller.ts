﻿export default class RegConfirmController {
    public login: any;
    public token: any;
    public id: any;
    public resolve: any;
    public modalInstance: any;
    public phoneReadonly: boolean;

    private user: any = null;
    private somethingWrongMessage = "Что-то пошло не так. Обратитесь к администратору пожалуйста.";
    private passwordMustMuchMessage = "Пароль и подтверждение пароля должны совпадать!";
    private confirmFailMessage = "Не получилось подтвердить регистрацию";
    private serverErrorMessage = "Ошибка обращения к серверу!";
    private loginSuccess = "Регистрация прошла успешно. Пожалуйста авторизируйтесь";

    constructor(private AccountService: any, private MessageFacade: any) {
        this.phoneReadonly = true;

        this.user = {
            Phone: '',
            UserId: '',
            Token: '',
            Address: '',
            Login: '',
            Password: '',
            ConfirmPassword: '',
            IsAgreed: true
        }

        this.AccountService.getConfirmationData(this.resolve.routeParams.userId, this.resolve.routeParams.token)
            .then((response) => {
                this.user.Login = response.login;
                this.user.Token = response.token;
                this.user.UserId = response.userId;
                this.user.Phone = response.phoneNumber;
            })
            .catch((error) => {
                this.MessageFacade.error(this.somethingWrongMessage, error);
            });
    }

    get email() {
        return this.user.Login;
    }

    get phoneNumber() {
        return this.user.Phone;
    }

    private submit() {

        if (this.user.ConfirmPassword !== this.user.Password) {
            this.MessageFacade.error(this.passwordMustMuchMessage);
            return;
        }

        this.AccountService
            .regConfirm(this.user)
            .then((response: any) => {
                if (response.isAccepted) {
                    this.modalInstance.dismiss('cancel');
                    this.MessageFacade.success(this.loginSuccess);
                }
                else if (response && response.message)
                    this.MessageFacade.error(this.confirmFailMessage, response.message);
            })
            .catch((error: any) => {
                this.MessageFacade.error(this.serverErrorMessage, error);
            });
    }

    public closePopup(): void {
        this.modalInstance.dismiss('cancel');
    }
}