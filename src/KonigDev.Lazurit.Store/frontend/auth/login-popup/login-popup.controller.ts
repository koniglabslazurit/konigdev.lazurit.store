﻿export default class LoginPopupCtrl {
    public modalInstance: any;
    public userLogin: string = "";
    public userPhone: string = "";
    public userPassword: string = "";
    public isAuthenticated: boolean;
    private returnUrl: string = "";
    private loginByUserPhone: boolean = false;

    constructor(private AuthService: any,
        private MessageFacade: any,
        private NavigationFacade: Auth.Services.INavigationFacade,
        private $location: angular.ILocationService,
        private $window: angular.IWindowService) {
        var currentUrl = this.$location.absUrl();
        this.returnUrl = currentUrl.replace('#/auth/login-popup?', '');
    }

    public closePopup() {
        this.modalInstance.close('cancel');
    }

    public login() {
        if (this.loginByUserPhone) {
            this.AuthService
                .loginByPhone(this.userPhone, this.userPassword, this.returnUrl)
                .then((response: any) => {
                    this.parseLoginResult(response)
                })
                .catch((error: any) => {
                    this.MessageFacade.error("Ошибка обращения к серверу!", error);
                });
            return;
        }
        this.AuthService
            .login(this.userLogin, this.userPassword, this.returnUrl)
            .then((response: any) => {
                this.parseLoginResult(response)
            }).catch((error: any) => {
                this.MessageFacade.error("Ошибка обращения к серверу!", error);
            });
    }

    public logout() {
        this.AuthService.logout().then((): void => {
            this.isAuthenticated = false;
            this.NavigationFacade.goToHome();
        }, error => {
            this.MessageFacade.error("Ошибка обращения к серверу!", error);
        });

    }

    public goToRegisterState() {
        this.closePopup();
        this.NavigationFacade.goToRegister();
    }

    public goToRecoveryState() {
        this.closePopup();
        this.NavigationFacade.goToRecovery();
    }

    private goToRegionLoginState() {
        this.closePopup();
        this.NavigationFacade.goToRegionLogin();
    }
    private goToVerifSellerState() {
        this.closePopup();
        this.NavigationFacade.goToVerifSellerState();
    }

    private parseLoginResult(response: any) {
        if (response.isAuthentificate) {
            if (response.isRegion) {
                this.goToRegionLoginState();
            }
            if (response.requiresVerification) {
                this.goToVerifSellerState();
            }
            if (response.returnUrl) {
                this.$window.location.assign(response.returnUrl);
            }
        } else {
            if (response && response.message)
                this.MessageFacade.uiError(response.message);
        }
        this.isAuthenticated = response.isAuthentificate;
    }
}
