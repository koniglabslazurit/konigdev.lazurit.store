﻿import * as angular from "angular";

import * as authCommonModule from "../common/auth.common.module";
import controller from "./login-popup.controller";
import messageService from "../../common/services/message.service"
import * as constantsModule from "../../constants/constants.module";

export const name = "kd.login-popup";

const loginPopupControllerName = "loginPopupCtrl";
export const loginPopupComponentName = "kdLoginPopup";
const messageFacadeName = "MessageFacade";

angular
    .module(name, [
        constantsModule.name,
        authCommonModule.name
    ])
    .controller(loginPopupControllerName, [
        authCommonModule.accountServiceName,
        messageFacadeName,
        authCommonModule.authNavigationFacadeName,
        '$location',
        '$window',
        constantsModule.serviceName,
        controller
    ])
    .component(loginPopupComponentName, {
        bindings: {
            modalInstance: "<"
        },
        controller: loginPopupControllerName,
        template: require("./login-popup.view.html")
    })
    .service(messageFacadeName, ["toastr", messageService]);
