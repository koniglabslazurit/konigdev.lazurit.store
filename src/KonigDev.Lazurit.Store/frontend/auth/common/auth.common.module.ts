﻿import * as angular from "angular";

import { NavigationFacade } from "./facades/auth-navigation.facade";
import { AccountService } from "./services/account.api.service";

export const name = "kd.auth.common";
export const accountServiceName = "AccountService";
export const authNavigationFacadeName = "authNavigation";

angular
    .module(name, [ ])
    .service(accountServiceName, [
        "$http",
        AccountService
    ])
    .service(authNavigationFacadeName, [
        "$state",
        "$window",
        NavigationFacade
    ]);
