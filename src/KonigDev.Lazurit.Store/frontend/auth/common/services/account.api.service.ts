﻿export class AccountService implements Auth.Services.IApiService {
    private loginUrl = '/api/account/loginByUserName/';
    private loginByPhoneUrl = '/api/account/LoginByUserPhone/';
    private loginByIdUrl = '/api/account/LoginByUserId/';
    private logoutUrl = '/api/account/logout/';
    private recoveryPassUrl = '/api/account/recoverypassword/';
    private registerUrl = '/api/account/register/';
    private getConfirmDataUrl = '/api/account/getConfirmRegistrData/';
    private regConfirmUrl = '/api/account/confirmRegistration/';
    private verifySellerUrl = '/api/account/SellerVerification/';
    private getCurrentUserInfoUrl = '/api/account/CurrentUser';
    private changeEmailUrl = '/api/account/changeEmail';
    private userExistUrl: string = '/api/account/userexist';
        
    constructor(private $http: angular.IHttpService) { }

    public login(login: string, password: string, returnUrl: string) {
        return this.$http.post(this.loginUrl,
            {
                login: login,
                password: password,
                returnUrl: returnUrl
            })
            .then((response: any) => {
                return response.data;
            });
    }

    public loginByPhone(phone: string, password: string, returnUrl: string) {
        return this.$http.post(this.loginByPhoneUrl, {
            UserPhone: phone,
            password: password,
            returnUrl: returnUrl
        }).then((response: any) => {
            return response.data;
        });
    }

    public loginById(id: string, password: string) {
        return this.$http.post(this.loginByIdUrl,
            {
                UserID: id,
                Password: password
            }).then((response: any) => {
                return response.data;
            });
    }

    public logout() {
        return this.$http.post(this.logoutUrl, {});
    }

    public recoveryPassword(userEmail: string) {
        var request = {
            Email: userEmail
        };
        return this.$http.post(this.recoveryPassUrl, request)
            .then((response: any) => {
                return response.data;
            });
    }

    public register(userEmail: string, userName: string, userTel: string) {
        var request = {
            Email: userEmail,
            Name: userName,
            Phone: userTel
        };
        return this.$http.post(this.registerUrl, request).
            then((response: any) => {
                return response.data;
            });
    }

    public getConfirmationData(userId: string, token: string) {
        return this.$http.get(this.getConfirmDataUrl + "?userId=" + userId + "&token=" + token)
            .then((response: any) => {
                return response.data;
            });
    }

    //todo надо слать dto
    public regConfirm(user: any) {
        return this.$http.post(this.regConfirmUrl, user)
            .then((response: any) => {
                return response.data;
            });
    }

    public verifySeller(request: any) {
        return this.$http.post(this.verifySellerUrl, request)
            .then((response: any) => {
                return response.data;
            });
    }

    public getCurrentUserInfo() {
        return this.$http.get(this.getCurrentUserInfoUrl)
            .then((response: any) => {
                return response;
            });
    }

    public changeEmail(request: any) {
        return this.$http.post(this.changeEmailUrl, request)
            .then((response: any) => {
                return response.data;
            });
    }

    public userExist(request: any) {
        return this.$http.get(this.userExistUrl, { params: request })
            .then((response: any) => {
                return response;
            });
    }
}
