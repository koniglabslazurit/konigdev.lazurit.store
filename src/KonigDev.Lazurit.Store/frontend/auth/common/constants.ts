﻿export default class Constants {
    public static RootState = "app";
    public static AuthModalState = "authModal";
    public static AuthModalRegisterState = "authModal.register";
    public static AuthModalRecoveryState = "authModal.recovery";
    public static AuthModalConfRegState = "authModal.confirm-register";
    public static AuthModalRLoginState = "authModal.region-login";
    public static AuthModalVerSellerState = "authModal.verification-seller";
    public static AuthModalLoginPopup = "authModal.login-popup";
    public static AuthModalChangeEmail = "authModal.change-email";
}