﻿namespace Auth.Services {
    export interface IApiService {
        login(login: string, password: string, returnUrl: string): angular.IPromise<any>;
        loginByPhone(phone: string, password: string, returnUrl: string): angular.IPromise<any>;
        loginById(id: string, password: string): angular.IPromise<any>;
        logout(): angular.IPromise<any>;
        recoveryPassword(userEmail: string): angular.IPromise<any>;
        register(userEmail: string, userName: string, userTel: string): angular.IPromise<any>;
        getConfirmationData(userId: string, token: string): angular.IPromise<any>;
        regConfirm(user: any): angular.IPromise<any>;
        verifySeller(request: any): angular.IPromise<any>;
        getCurrentUserInfo(): angular.IPromise<any>;
        changeEmail(request: any): angular.IPromise<any>;
        userExist(request: any): angular.IPromise<any>;
    }

    export interface INavigationFacade {
        goToRegister(): void;
        goToRecovery(): void;
        goToRegionLogin(): void;
        goToVerifSellerState(): void;
        goToHome(): void;
    }
}