﻿import authConstants from "../constants"

export class NavigationFacade implements Auth.Services.INavigationFacade {
    constructor(private $state: any, private $window: angular.IWindowService) {
    }

    public goToRegister() {
        this.$state.go(authConstants.AuthModalRegisterState);
    };

    public goToRecovery() {
        this.$state.go(authConstants.AuthModalRecoveryState);
    };

    public goToRegionLogin() {
        this.$state.go(authConstants.AuthModalRLoginState);
    };

    public goToVerifSellerState() {
        this.$state.go(authConstants.AuthModalVerSellerState);
    };

    public goToHome() {
        this.$window.location.assign("/Home");
    };
}