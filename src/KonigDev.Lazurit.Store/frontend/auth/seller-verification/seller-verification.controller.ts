﻿export default class SellerVerificationController {
    private userEmail: string = "";
    private isInprogress: boolean = false;
    public modalInstance: any;
    private serverErrorMessage = "Ошибка обращения к серверу!";

    constructor(private AccountService:any, private MessageFacade:any) {
    }

    public verifySeller() {
        this.isInprogress = true;
        var request = {
            Email: this.userEmail
        };
        this.AccountService
            .verifySeller(request)
            .then(response => {
                if (response.isVerified) {
                    this.modalInstance.dismiss('cancel');
                    this.isInprogress = false;
                } else {
                    this.isInprogress = false;
                    if (response && response.message) {                     
                        this.MessageFacade.uiError(response.message);
                    }
                    console.log(response);
                }
            }, error => {
                this.isInprogress = false;
                this.MessageFacade.error(this.serverErrorMessage, error);
            });
    };
};