import * as angular from "angular";

import * as authCommonModule from "../common/auth.common.module";
import controller from "./seller-verification.controller";
import messageService from "../../common/services/message.service"

export const name = "kd.seller-verification";

const sellerVerivicationControllerName = "sellerVerivicationCtrl";
export const sellerVerificationComponentName = "kdSellerVerification";
const messageFacadeName = "MessageFacade";

angular
    .module(name, [
        authCommonModule.name
    ])
    .controller(sellerVerivicationControllerName, [
        authCommonModule.accountServiceName,
        messageFacadeName,
        controller
    ])
    .component(sellerVerificationComponentName, {
        bindings: {
            modalInstance: "<"
        },
        controller: sellerVerivicationControllerName,
        template: require("./seller-verification.view.html")
    })
    .service(messageFacadeName, ["toastr", messageService]);
