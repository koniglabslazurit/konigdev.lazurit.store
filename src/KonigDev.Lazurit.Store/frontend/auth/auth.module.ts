﻿import * as angular from "angular";

import * as loginformModule from "./login/login-form.module";
import * as recoveryModule from "./recovery-password/recovery-password.module";
import * as regConfirmModule from "./register-confirm/register-confirm.module";
import * as regLoginModule from "./region-login/region-login.module";
import * as sellerVerificationModule from "./seller-verification/seller-verification.module";
import * as loginPopupModule from "./login-popup/login-popup.module";
import * as registerModule from "./register/register.module";
import * as changeEmail from "./change-email/change-email.module";

import authConstants from "./common/constants"

export const name = "kd.auth";

export const authRoutingStates = [
    changeEmail.changeEmailComponentName,
    loginPopupModule.loginPopupComponentName,
    recoveryModule.recoveryComponentName,
    registerModule.registerComponentName,
    regConfirmModule.regConfirmComponentName,
    regLoginModule.regLoginComponentName,
    sellerVerificationModule.sellerVerificationComponentName
];

angular
    .module(name, [
        loginformModule.name,
        registerModule.name,
        regConfirmModule.name,
        recoveryModule.name,
        regLoginModule.name,
        sellerVerificationModule.name,
        loginPopupModule.name,
        changeEmail.name
    ])
    .config([
        "$stateProvider", "$stickyStateProvider", "$urlRouterProvider",
        ($stateProvider, $stickyStateProvider, $urlRouterProvider) => {
            $urlRouterProvider.otherwise("/");

            const states = [{
                name: authConstants.RootState,
                url: "/",
                sticky: true
            }, {
                name: authConstants.AuthModalState,
                url: '/auth'
            }, {
                name: authConstants.AuthModalRegisterState,
                url: "/register",
                componentName: registerModule.registerComponentName
            }, {
                name: authConstants.AuthModalRecoveryState,
                url: "/recovery",
                componentName: recoveryModule.recoveryComponentName
            }, {
                name: authConstants.AuthModalConfRegState,
                url: "/confirm-register?{userId}:token",
                componentName: regConfirmModule.regConfirmComponentName
            }, {
                name: authConstants.AuthModalRLoginState,
                url: "/region-login",
                componentName: regLoginModule.regLoginComponentName
            }, {
                name: authConstants.AuthModalVerSellerState,
                url: "/verification-seller",
                componentName: sellerVerificationModule.sellerVerificationComponentName
            }, {
                name: authConstants.AuthModalLoginPopup,
                url: "/login-popup",
                componentName: loginPopupModule.loginPopupComponentName
            }];

            // $stickyStateProvider.enableDebug(true);
            states.forEach(state => $stateProvider.state(state));
        }
    ])
    .run([
        "$rootScope", "$uibModal", "$state", "$previousState",
        ($rootScope, $uibModal, $state, $previousState) => {
            $rootScope.$state = $state;
            // Intercept state changes to authState throughout the whole app
            $rootScope.$on('$stateChangeStart', (event, toState, toParams) => {
                if (authRoutingStates.every(p => p !== toState.componentName)) {
                    return;
                }
                $previousState.memo("authModelInvoker");
                $uibModal
                    .open({
                        animation: true,
                        component: toState.componentName,
                        resolve: {
                            routeParams() {
                                return toParams;
                            }
                        }
                    })
                    .closed
                    .then(() => {
                        console.log("modal window is closed");
                        var previous = $previousState.get();
                        if (previous && previous.state) {
                            // please use it for debug if you will change this logic
                            // console.log(previous);
                            // console.log($state.current)
                            if (previous.state.url !== "/login-popup")
                                $previousState.go("authModelInvoker");
                            else
                                $state.go(authConstants.RootState);
                        } else {
                            $state.go(authConstants.RootState);
                        }
                    });
            });
        }
    ]);
