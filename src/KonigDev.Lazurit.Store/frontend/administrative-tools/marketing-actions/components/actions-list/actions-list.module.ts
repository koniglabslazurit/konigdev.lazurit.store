// dependencies
import * as angular from "angular";

import * as servicesModule from "../../services/actions.services.module";
import * as dialogsModule from "../../dialogs/actions.dialogs.module";

import { ActionsListController } from "./actions-list.controller";

// constants
export const name = "kd.marketing.actions.list";
const uiBootstrap = require("angular-ui-bootstrap");

// module
angular
    .module(name,
    [
        uiBootstrap,
        servicesModule.name,
        dialogsModule.name
    ])
    .component("kdMarketingActions",
    {
        template: require("./actions-list.html"),
        controller: ActionsListController
    });
