import { IActionsDialogsService, actionsDialogsService } from "../../dialogs/actions.dialogs.interface";
import { IActionsApiService, actionsApiService } from "../../services/actions.api.interface";
import { $log } from "../../../../globals/angular.names";

import mad = Marketing.Actions.Dto;

export class ActionsListController {
    private readonly _logService: angular.ILogService;
    private readonly _dialogsService: IActionsDialogsService;
    private readonly _apiService: IActionsApiService;
    private readonly _itemsOnPage: number;

    // view model    
    public actions: mad.IDtoMarketingActions;
    public filter: mad.IAllMarketingActionsQuery;
    public isLoading: boolean;

    constructor(
        logService: angular.ILogService,
        dialogsService: IActionsDialogsService,
        apiService: IActionsApiService
    ) {
        this._logService = logService;
        this._apiService = apiService;
        this._dialogsService = dialogsService;

        this._itemsOnPage = 10; // default items per page count
    }

    $onInit() {
        this.filter = {
            pageNumber: 1,
            take: this._itemsOnPage
        };

        this._updateActions();
    }

    create() {
        let modal = this._dialogsService.showCreateDialog();
        modal.result
            .then(result => this._updateActions())
            .catch(error => this._logService.error(error));
    }

    update(action: mad.IDtoMarketingAction) {
        let modal = this._dialogsService.showUpdateDialog(action);
        modal.result
            .then(result => this._updateActions())
            .catch(error => this._logService.error(error));
    }

    delete(action: mad.IDtoMarketingAction) {
        this._apiService
            .delete(action.id)
            .then(result => this._updateActions())
            .catch(error => this._logService.error(error));
    }

    private _updateActions() {
        this._apiService
            .getAll(this.filter)
            .then(result => this.actions = result)
            .catch(error => this._logService.error(error));
    }
}
ActionsListController.$inject = [$log, actionsDialogsService, actionsApiService]