import uib = angular.ui.bootstrap;
import mad = Marketing.Actions.Dto;

export const actionsDialogsService = "kd.marketing.actions.dialogs.service";
export interface IActionsDialogsService {
    showCreateDialog(): uib.IModalServiceInstance;
    showUpdateDialog(action: mad.IDtoMarketingAction): uib.IModalServiceInstance
}