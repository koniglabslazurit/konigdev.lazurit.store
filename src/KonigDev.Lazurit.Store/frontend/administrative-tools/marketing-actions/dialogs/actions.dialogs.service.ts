import uib = angular.ui.bootstrap;
import mad = Marketing.Actions.Dto;

import { ActionCreateModalController } from "./create/create.controller";
import { updateCommandInjectionName, ActionUpdateModalController } from "./update/update.controller";
import { $uibModal } from "../../../globals/angular.names";
import { IActionsDialogsService } from "./actions.dialogs.interface";

export class ActionsDialogsService implements IActionsDialogsService {
    private readonly _modalsService: uib.IModalService;

    constructor(modalsService: uib.IModalService) {
        this._modalsService = modalsService;
    }

    showCreateDialog(): uib.IModalServiceInstance {
        let options: uib.IModalSettings = {
            template: require("./create/create.html"),
            bindToController: true,
            controllerAs: "$ctrl",
            controller: ActionCreateModalController,
            animation: true,
            size: "lg"
        };

        return this._modalsService.open(options);
    }
    showUpdateDialog(action: mad.IDtoMarketingAction): uib.IModalServiceInstance {
        let command: mad.IUpdateMarketingActionCommand = {
            action: action
        };
        let options: uib.IModalSettings = {
            template: require("./update/update.html"),
            bindToController: true,
            controllerAs: "$ctrl",
            resolve: {},
            controller: ActionUpdateModalController,
            animation: true,
            size: "lg"
        };
        options.resolve[updateCommandInjectionName] = () => { return command; };

        return this._modalsService.open(options);
    }
}
ActionsDialogsService.$inject = [$uibModal]