// dependencies
import * as angular from "angular";

import * as servicesModule from "../services/actions.services.module";
import * as fileSelectModule from "../../../common/components/file-select/file-select.module";
import * as imageServicesModule from "../../../image/services/image.services.module";

import { ActionsDialogsService } from "./actions.dialogs.service";
import { actionsDialogsService } from "./actions.dialogs.interface";

// constants
export const name = "kd.marketing.actions.dialogs";
const uiBootstrap = require("angular-ui-bootstrap");

// module
angular
    .module(name,
    [
        uiBootstrap,
        servicesModule.name,
        fileSelectModule.name,
        imageServicesModule.name
    ])
    .service(actionsDialogsService, ActionsDialogsService)
