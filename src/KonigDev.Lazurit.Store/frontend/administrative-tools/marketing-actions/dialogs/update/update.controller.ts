import { $uibModalInstance } from "../../../../globals/angular.names";
import { IActionsApiService, actionsApiService } from "../../services/actions.api.interface";
import { IImageApiService, imageApiServiceName } from "../../../../image/services/image.api.interface";
import { IUploadFileDto } from "../../../../image/dto/upload-file.dto";

import uib = ng.ui.bootstrap;
import mad = Marketing.Actions.Dto;

export const updateCommandInjectionName = "updateActionCommand";
export class ActionUpdateModalController {
    private readonly _modalInstance: uib.IModalServiceInstance;
    private readonly _actionsApiService: IActionsApiService;
    private readonly _imageApiService: IImageApiService;

    private _imageFile: File | undefined;

    // view model
    public command: mad.IUpdateMarketingActionCommand;
    public imageFileChanged: (file: File) => void;

    constructor(
        modalInstance: uib.IModalServiceInstance,
        actionsApiService: IActionsApiService,
        imageApiService: IImageApiService,
        command: mad.IUpdateMarketingActionCommand
    ) {
        this._modalInstance = modalInstance;
        this._actionsApiService = actionsApiService;
        this._imageApiService = imageApiService;

        this.command = command;
        this.imageFileChanged = (file: File) => {
            this._imageFile = file;
        };
    }

    create() {
        this._actionsApiService
            .update(this.command)
            .then(result => {
                if (!this._imageFile) {
                    return;
                }
                let parameters: IUploadFileDto = {
                    objectId: this.command.action.id,
                    type: "SliderMiddle"
                }
                return this._imageApiService.upload(parameters, this._imageFile);
            })
            .then(result => this._modalInstance.close(result))
            .catch(error => this._modalInstance.dismiss(error));
    }

    cancel() {
        this._modalInstance.close("canceled");
    }
}
ActionUpdateModalController.$inject =
    [
        $uibModalInstance,
        actionsApiService,
        imageApiServiceName,
        updateCommandInjectionName
    ];