namespace Marketing.Actions.Dto {
    export interface ICreateMarketingActionCommand {
        action: IDtoMarketingAction;
    }
}