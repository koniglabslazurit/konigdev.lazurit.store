namespace Marketing.Actions.Dto {
    export interface IAllMarketingActionsQuery {
        pageNumber: number;
        take: number;
    }
}