namespace Marketing.Actions.Dto {
    export interface IUpdateMarketingActionCommand {
        action: IDtoMarketingAction;
    }
}