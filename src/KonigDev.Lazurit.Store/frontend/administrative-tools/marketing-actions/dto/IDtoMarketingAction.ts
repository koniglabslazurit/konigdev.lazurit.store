namespace Marketing.Actions.Dto {
    export interface IDtoMarketingAction {
        id?: any;
        seoId?: number;
        isVisible?: boolean;
        discount?: number;
        name?: string;
        code?: string;
        description?: string;
    }
}