namespace Marketing.Actions.Dto {
    export interface IDtoMarketingActions {
        collection: IDtoMarketingAction[];
        total: number;
    }
}