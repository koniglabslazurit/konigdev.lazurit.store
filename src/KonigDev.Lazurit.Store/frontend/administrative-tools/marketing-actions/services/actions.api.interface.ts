import mad = Marketing.Actions.Dto;

export const actionsApiService = "kd.marketing.actions.services.api";
export interface IActionsApiService {
    getAll(query: mad.IAllMarketingActionsQuery): ng.IPromise<mad.IDtoMarketingActions>;
    create(command: mad.ICreateMarketingActionCommand): ng.IPromise<any>;
    update(command: mad.IUpdateMarketingActionCommand): ng.IPromise<any>;
    delete(id: any): ng.IPromise<any>;
}