// dependencies
import * as angular from "angular";

import { ActionsApiService } from "./actions.api.service";
import { actionsApiService } from "./actions.api.interface";

// constants
export const name = "kd.marketing.actions.services";

// module
angular
    .module(name, [])
    .service(actionsApiService, ActionsApiService);