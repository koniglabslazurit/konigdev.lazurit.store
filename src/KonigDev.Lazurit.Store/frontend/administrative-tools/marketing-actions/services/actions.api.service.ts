import { $http } from "../../../globals/angular.names";
import { IActionsApiService } from "./actions.api.interface";

import mad = Marketing.Actions.Dto;
export class ActionsApiService implements IActionsApiService {
    private readonly _http: ng.IHttpService;
    private readonly _baseUrl: string;
    constructor(http: ng.IHttpService) {
        this._http = http;
        this._baseUrl = "/api/marketing/actions";
    }
    getAll(query: mad.IAllMarketingActionsQuery): ng.IPromise<mad.IDtoMarketingActions> {
        return this._http
            .get(this._baseUrl, { params: query })
            .then(result => result.data);
    }
    create(command: mad.ICreateMarketingActionCommand): ng.IPromise<any> {
        return this._http
            .post(this._baseUrl, command)
            .then(result => result.data);
    }
    update(command: mad.IUpdateMarketingActionCommand): ng.IPromise<any> {
        return this._http
            .put(this._baseUrl, command)
            .then(result => result.data);
    }
    delete(id: any): ng.IPromise<any> {
        let requestUrl = `${this._baseUrl}/${id}`;
        return this._http
            .delete(requestUrl)
            .then(result => result.data);
    }
}
ActionsApiService.$inject = [$http];