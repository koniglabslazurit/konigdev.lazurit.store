﻿export default class OrdersToolController {

    public orders: any;

    constructor(
        private constants: Constants.IConstants,
        private messageService: Common.Message.IMessage,
        private profileService: any,
        private orderApiService: Order.ApiService.IApiService
    ) {
        this.init();
    }

    init(): void {
        this.getOrders();
    }

    getOrders(): void {
        /* get all orders without paging */
        this.orderApiService.getOrders({ ItemsOnPage: 0 })
            .then((data: any) => {
                for (let item of data.items) {
                    if (item.creationTime1C) {
                        item.creationTime1C = new Date(item.creationTime1C)
                    }
                }
                this.orders = data.items;
            });
    }

    public updateApprove(order: any, isApproved: boolean): void {
        this.orderApiService.updateApprove(order.id, isApproved)
            .then((data) => {
                order.isApprovedBySeller = isApproved;
            })
            .catch((error: any) => {
                this.messageService.error(error.data.message, error);
            })
    }

    public updateOrder1CFields(order: any): void {
        this.orderApiService.updateOrder1CFields(order.id, order.orderNumber1C, order.creationTime1C)
            .then((data) => {
                //nothig to do
            })
            .catch((error: any) => {
                this.messageService.error(error.data.message, error);
            })
    }
}