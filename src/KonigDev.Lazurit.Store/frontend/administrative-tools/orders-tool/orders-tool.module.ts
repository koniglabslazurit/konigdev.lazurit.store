﻿import * as angular from "angular";

/* import internal modules of orders-tool */
import ordersToolController from "./orders-tool.controller";

/* import full modules */
import * as constantsModule from "../../constants/constants.module";
import * as commonModule from "../../common/common.module";
import * as profileServiceModule from "../../profile/profile-services/profile-services.module";
import * as orderModule from "../../order/order.module";

/* constants */
export const name = "kd.orders.tool";
const ordersToolControllerName = "ordersToolController";
const ordersToolComponentName = "kdOrdersTool";

angular
    .module(name, [
        constantsModule.name,
        commonModule.name,
        profileServiceModule.name,
        orderModule.name
    ])
    .controller(ordersToolControllerName, [
        constantsModule.serviceName,
        commonModule.messageServiceName,
        profileServiceModule.profileServiceName,
        orderModule.oderApiServiceName,
        ordersToolController
    ])
    .component(ordersToolComponentName, {
        bindings: {
        },
        controller: ordersToolControllerName,
        template: require("./orders-tool.view.html")
    });