﻿import * as angular from "angular";

/* */
import service from "./role-tool.api.service";

/* */
export const name = "kd.role";
export const serviceName = "roleService";
/*  */
angular
    .module(name, [ ])
    .service(serviceName,
    [
        "$http",
        service
    ]);