﻿import * as angular from "angular";

import controller from "./verify-sellers.controller";
import * as roleModule from "../role-tool.module";
import messageService from "../../../common/services/message.service"
/*constants*/

export const name = "kd.verify.sellers";
const verifySellersControllerName = "verifySellersController";
const verifySellerComponentName = "kdVerifySellers";
const messageFacadeName = "MessageFacade";

angular
    .module(name,
    [
        roleModule.name
    ])
    .controller(verifySellersControllerName,
    [
        roleModule.serviceName,
        messageFacadeName,
        '$q',
        controller
    ])
    .service(messageFacadeName, ["toastr", messageService])
    .component(verifySellerComponentName,
    {
        bindings: {},
        controller: verifySellersControllerName,
        template: require("./verify-sellers.view.html")
    });