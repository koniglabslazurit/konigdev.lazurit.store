﻿namespace Role.Services {
    export interface IApiRoleService {
        getAdministrativeRoles(): angular.IPromise<Array<Dto.Role.IRole>>;
        getSellerRoles(): angular.IPromise<Array<Dto.Role.IRole>>;
        getUsers(data: any): angular.IPromise<Array<Dto.Role.IUserRole>>;
        updateRoles(data: Array<Dto.Role.IUpdateRole>): any;
        deleteUser(data: string): any;
    }
}