﻿export default class RoleService implements Role.Services.IApiRoleService {

    private rolesAdministrativeUrl: string = "/api/users/role/administrative";
    private rolesSellerUrl: string = "/api/users/role/seller";
    private getUsersByRoleUrl: string = "/api/users/by-roles?";
    private changeUserRoleUrl: string = "/api/users/role";
    private deleteUserUrl: string = "/api/users";

    constructor(public $http: angular.IHttpService) {
    }

    public getAdministrativeRoles(): angular.IPromise<Array<Dto.Role.IRole>> {
        return this.$http
            .get(this.rolesAdministrativeUrl)
            .then((response) => {
                return response.data;
            });
    };

    public getSellerRoles(): angular.IPromise<Array<Dto.Role.IRole>> {
        return this.$http
            .get(this.rolesSellerUrl)
            .then((response) => {
                return response.data;
            });
    };

    public getUsers(data: any): angular.IPromise<Array<Dto.Role.IUserRole>> {
        return this.$http
            .get(this.getUsersByRoleUrl, { params: data })
            .then((response) => {
                return response.data;
            });
    };

    public updateRoles(users: Array<Dto.Role.IUpdateRole>): any {
        return this.$http
            .put(this.changeUserRoleUrl, users)
            .then((response: any) => {
                return response.status;
            });
    };

    public deleteUser(userId): angular.IPromise<any> {
        return this.$http
            .delete(this.deleteUserUrl + '?userId=' + userId)
            .then((response: any) => {
                return response;
            });
    };
}