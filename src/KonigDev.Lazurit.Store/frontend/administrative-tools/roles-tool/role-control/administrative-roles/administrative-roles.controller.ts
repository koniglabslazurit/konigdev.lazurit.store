﻿export default class AdministrativeRolesController {

    public selectedAll: boolean = false;
    public users: Array<Dto.Role.IUserRole> = [];
    public roles: Array<Dto.Role.IRole> = [];
    public paging: Dto.Paging.IPaging;
    public isAllChecked: boolean;
    public numberUsersForUpdating: number = 0;

    public constructor(
        private roleApiService: Role.Services.IApiRoleService,
        private $q: angular.IQService
    ) {
        this.paging = {
            itemsOnPage: 10,
            page: 1,
            totalCount: 0,
            pageCount: 1
        };
        this.getRoles();
        this.getUsers();
    }

    getUsers(): void {
        var rolesRequest = ['admin', 'marketer', 'contentManager'];
        this.roleApiService.getUsers({ roles: rolesRequest, itemsOnPage: this.paging.itemsOnPage, page: this.paging.page })
            .then((data: any) => {
                data.items.forEach((i: Dto.Role.IUserRole) => {
                    i.checked = false;
                });
                this.users = data.items;
                this.paging.totalCount = data.totalCount;
                this.paging.pageCount = Math.ceil(this.paging.totalCount / this.paging.itemsOnPage);
            });
    }

    public next(): void {
        this.paging.page++;
        this.getUsers();
    }

    public prev(): void {
        this.paging.page--;
        this.getUsers();
    }

    getRoles(): void {
        this.roleApiService.getAdministrativeRoles()
            .then((data: Array<Dto.Role.IRole>) => {
                this.roles = data;

            });
    }

    public checkUsers(): void {
        this.numberUsersForUpdating = this.users.filter((s: Dto.Role.IUserRole) => {
            return s.checked === true;
        }).length;
    }

    public selectAllCheckboxes(): void {
        this.users.forEach((s: Dto.Role.IUserRole) => {
            s.checked = this.isAllChecked;
        });
    }

    public deleteUser(): void {
        var promises: Array<angular.IPromise<any>> = [];
        var deletedUsers = this.users.filter((s: Dto.Role.IUserRole) => {
            return s.checked === true;
        });
        if (deletedUsers.length > 0) {
            deletedUsers.forEach((u: Dto.Role.IUserRole) => {
                promises.push(this.roleApiService.deleteUser(u.userId));
            });
            this.$q.all(promises).then((result) => {
                this.getUsers();
                console.log(result);
            }, (error) => {
                console.log(error);
            });
        } else {
            console.log("Выберите юзера");
        }
    }

    public accept(): void {
        var updatingUsers = this.users.filter((s: Dto.Role.IUserRole) => {
            return s.checked === true;
        });
        this.roleApiService.updateRoles(updatingUsers)
            .then(() => {
                this.getUsers();
            });
    }
}