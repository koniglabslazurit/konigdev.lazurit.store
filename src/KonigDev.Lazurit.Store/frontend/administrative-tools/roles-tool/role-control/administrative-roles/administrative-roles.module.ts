﻿import * as angular from "angular";

/* */
import * as roleModule from "../../role-tool.module";

import controller from "./administrative-roles.controller";

/* */
export const name = "kd.administrative.roles";
const controllerName = "administrativeRolesController";

/* components */
const componentName = "kdAdministrativeRoles";
/*  */
angular
    .module(name,
    [
        roleModule.name
    ])
    .controller(controllerName,
    [
        roleModule.serviceName,
        '$q',
        controller
    ])
    .component(componentName, {
        controller: controllerName,
        template: require("./administrative-roles.view.html")
    });