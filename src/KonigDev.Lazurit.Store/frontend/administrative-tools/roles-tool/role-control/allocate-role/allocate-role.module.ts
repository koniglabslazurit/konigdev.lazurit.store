﻿import * as angular from "angular";

/* */
import * as roleModule from "../../role-tool.module";

import controller from "./allocate-role.controller";

/* */
export const name = "kd.allocate.role";
const controllerName = "allocateRoleController";

/* components */
const componentName = "kdAllocateRole";
/*  */
angular
    .module(name,
    [
        roleModule.name
    ])
    .controller(controllerName,
    [
        roleModule.serviceName,
        '$q',
        controller
    ])
    .component(componentName, {
        controller: controllerName,
        template: require("./allocate-role.view.html")
    });