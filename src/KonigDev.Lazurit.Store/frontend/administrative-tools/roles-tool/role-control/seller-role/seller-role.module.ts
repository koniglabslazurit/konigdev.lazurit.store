﻿import * as angular from "angular";

/* */
import * as roleModule from "../../role-tool.module";
import messageService from "../../../../common/services/message.service";

import controller from "./seller-role.controller";

/* */
export const name = "kd.seller.role";
const controllerName = "sellerRoleController";
const messageFacadeName = "MessageFacade";

/* components */
const componentName = "kdSellerRole";
/*  */
angular
    .module(name,
    [
        roleModule.name
    ])
    .service(messageFacadeName, ["toastr", messageService])
    .controller(controllerName,
    [
        roleModule.serviceName,
        messageFacadeName,
        '$q',
        controller
    ])
    .component(componentName, {
        controller: controllerName,
        template: require("./seller-role.view.html")
    });