﻿export default class SellerRoleController {
    public users: Array<Dto.Role.IUserRole>;
    public roles: Array<Dto.Role.IRole>;
    public paging: any;
    public selectAll: boolean;
    constructor(
        public roleApiService: Role.Services.IApiRoleService,
        public messageFacade: any, private $q: angular.IQService
    ) {
        this.init();
    }
    init() {
        this.selectAll = false;
        this.paging = {
            itemsOnPage: 10,
            page: 1
        };
        this.fullUsersByRoles();
        this.fullSellersRoles();
    }

    fullUsersByRoles() {
        var roles = ["salonSaller", "shopSaller"];
        this.roleApiService.getUsers({ roles: roles, itemsOnPage: this.paging.itemsOnPage, page: this.paging.page })
            .then((data: any) => {
                data.items.forEach((i: any) => {
                    i.checked = false;
                });
                this.users = data.items;
                this.paging.totalCount = data.totalCount;
            });
    }

    fullSellersRoles() {
        this.roleApiService.getSellerRoles()
            .then((data) => {
                this.roles = data;
            });
    }

    updateRole(users: Array<Dto.Role.IUserRole>) {
        var updateUsers: Array<Dto.Role.IUpdateRole> = users.map((u) => {
            var model: Dto.Role.IUpdateRole = { userId: u.userId, role: u.role }
            return model;
        });
        this.roleApiService.updateRoles(updateUsers)
            .then((data: number) => {
                this.fullUsersByRoles();
                if (data === 200)
                    this.messageFacade.success("Роли успешно изменены");
            });
    }

    public selectingAll() {
        this.selectAll = !this.selectAll;
        this.users.forEach((u) => {
            u.checked = this.selectAll;
        });
    }

    public assept() {
        var usersForAccept: Array<Dto.Role.IUserRole> = [];
        var messageError = "";
        this.users.forEach((u) => {
            if (u.checked) {
                usersForAccept.push(u);
                if (u.role !== "salonSaller" && u.role !== "shopSaller") {
                    messageError = "Выберите роль для пользователя " + u.name;
                };
            };
        });
        if (messageError !== "") {
            this.messageFacade.warning(messageError);
            return;
        };
        if (usersForAccept.length === 0) {
            this.messageFacade.warning("Выберите пользователя");
            return;
        };
        this.updateRole(usersForAccept);
    }

    public checkedUser(userId: string) {
        var user = this.users.filter((u: Dto.Role.IUserRole) => { return u.userId === userId })[0];
        user.checked = !user.checked;
    }


    public deleteUser(): void {
        var promises: Array<angular.IPromise<any>> = [];
        var deletedUsers = this.users.filter((s: Dto.Role.IUserRole) => {
            return s.checked === true;
        });
        if (deletedUsers.length > 0) {
            deletedUsers.forEach((u: Dto.Role.IUserRole) => {
                promises.push(this.roleApiService.deleteUser(u.userId));
            });
            this.$q.all(promises).then((result) => {
                this.fullUsersByRoles();
                console.log(result);
            }, (error) => {
                console.log(error);
            });
        } else {
            console.log("Выберите юзера");
        }
    }
}
