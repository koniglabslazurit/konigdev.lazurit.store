﻿import * as angular from "angular";

/* */
import * as roleModule from "../role-tool.module";

import controller from "./role-control.controller";

/* */
export const name = "kd.role.control";
const controllerName = "roleControlController";

/* components */
const componentName = "kdRoleControl";
/*  */
angular
    .module(name,
    [
        roleModule.name
    ])
    .controller(controllerName,
    [
        controller
    ])
    .component(componentName, {
        controller: controllerName,
        bindings: {
            typecontrol: '<'
        },
        template: require("./role-control.view.html")
    });