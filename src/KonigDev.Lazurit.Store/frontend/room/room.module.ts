﻿import * as angular from "angular";

import controller from "./room.controller";
import service from "./room.api.service";
import serviceJQ from "./room.service.jq";
import serviceFilter from "./room.service.filter";

export const name = "kd.room";
const controllerName = "roomController";
export const serviceName = "roomApiService";
const serviceJQName = "roomJQService";
const serviceFilterName = "roomFilterService";

angular
    .module(name, [ ])
    .controller(controllerName,
    [
        serviceName,
        serviceJQName,
        serviceFilterName,
        controller
    ])
    .service(serviceJQName, [serviceJQ])
    .service(serviceFilterName, [serviceFilter])
    .service(serviceName, ["$http", service]);
