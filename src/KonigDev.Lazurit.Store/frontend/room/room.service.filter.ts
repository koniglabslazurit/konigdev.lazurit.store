﻿export default class RoomFilterService {

    constructor() {

    }

    public filterByFacingColor(collections: Array<Dto.RoomItem.IRoomCollection>, facingcolors: Array<Dto.RoomItem.IRoomFilter>): Array<Dto.RoomItem.IRoomCollection> {
        var checkedArray = facingcolors.filter((fc: Dto.RoomItem.IRoomFilter) => {
            return fc.checked === true;
        });

        if (checkedArray.length !== 0) {
            for (var collection of collections) {
                var totalvariants = [];
                var variants = [];
                for (var element of checkedArray) {
                    variants = collection.variants.filter((v: Dto.RoomItem.IRoomVariant) => {
                        return v.facingColors.some(el => (el.id === element.id));
                    });
                    totalvariants = totalvariants.concat(variants);
                }
                collection.variants = this.getUniqueById(totalvariants);
            };
        }
        return collections;
    }

    public filterByFacing(collections: Array<Dto.RoomItem.IRoomCollection>, facings: Array<Dto.RoomItem.IRoomFilter>): Array<Dto.RoomItem.IRoomCollection> {
        var checkedArray = facings.filter((fc: Dto.RoomItem.IRoomFilter) => {
            return fc.checked === true;
        });

        if (checkedArray.length !== 0) {
            for (var collection of collections) {
                var totalvariants = [];
                var variants = [];
                for (var element of checkedArray) {
                    variants = collection.variants.filter((v: Dto.RoomItem.IRoomVariant) => {
                        return v.facings.some(el => (el.id === element.id));
                    });
                    totalvariants = totalvariants.concat(variants);
                }
                collection.variants = this.getUniqueById(totalvariants);
            };
        };
        return collections;
    }

    public filterByFrameColor(collections: Array<Dto.RoomItem.IRoomCollection>, framecolors: Array<Dto.RoomItem.IRoomFilter>): Array<Dto.RoomItem.IRoomCollection> {
        var checkedArray = framecolors.filter((fc: Dto.RoomItem.IRoomFilter) => {
            return fc.checked === true;
        });

        if (checkedArray.length !== 0) {
            for (var collection of collections) {
                var totalvariants = [];
                var variants = [];
                for (var element of checkedArray) {
                    variants = collection.variants.filter((v: Dto.RoomItem.IRoomVariant) => {
                        return v.frameColors.some(el => (el.id === element.id));
                    });
                    totalvariants = totalvariants.concat(variants);
                }
                collection.variants = this.getUniqueById(totalvariants);
            };
        }
        return collections;
    }

    public filterByTargetAudience(collections: Array<Dto.RoomItem.IRoomCollection>, targetAudiences: Array<Dto.RoomItem.IRoomFilter>): Array<Dto.RoomItem.IRoomCollection> {
        var checkedArray = targetAudiences.filter((ta: Dto.RoomItem.IRoomFilter) => {
            return ta.checked === true;
        });

        if (checkedArray.length !== 0) {
            for (var collection of collections) {
                var totalvariants = [];
                var variants = [];
                for (var element of checkedArray) {
                    variants = collection.variants.filter((v: Dto.RoomItem.IRoomVariant) => {
                        return v.targetAudiences.some(el => (el.id === element.id));
                    });
                    totalvariants = totalvariants.concat(variants);
                }
                collection.variants = this.getUniqueById(totalvariants);
            };
        }
        return collections;
    }

    public filterByStyle(collections: Array<Dto.RoomItem.IRoomCollection>, styles: Array<Dto.RoomItem.IRoomFilter>): Array<Dto.RoomItem.IRoomCollection> {
        var checkedArray = styles.filter((s: Dto.RoomItem.IRoomFilter) => {
            return s.checked === true;
        });

        if (checkedArray.length !== 0) {
            for (var collection of collections) {
                var totalvariants = [];
                var variants = [];
                for (var element of checkedArray) {
                    variants = collection.variants.filter((v: Dto.RoomItem.IRoomVariant) => {
                        return v.styles.some(el => (el.id === element.id));
                    });
                    totalvariants = totalvariants.concat(variants);
                }
                collection.variants = this.getUniqueById(totalvariants);
            };
        }
        return collections;
    }

    getUniqueById(a: Array<any>): Array<any> {
        var n = {}, r = [];
        for (var i = 0; i < a.length; i++) {
            if (!n[a[i].id]) {
                n[a[i].id] = true;
                r.push(a[i]);
            }
        }
        return r;
    }
}