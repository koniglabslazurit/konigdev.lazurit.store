﻿namespace Room.Services {
    export interface IRoomApiService {
        getCollections(roomAlias): angular.IPromise<any>;
        getRoomTypesByProduct(request: Dto.Product.ProductRequest): angular.IPromise<Array<Dto.RooomType.IRoomByProductItem>>;
    }
}