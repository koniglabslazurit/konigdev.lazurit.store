﻿namespace Dto.RoomItem {

    export interface IRoomCollectionsData {
        room: any,
        collections: Array<IRoomCollection>,
        facings: Array<IRoomFilter>,
        facingColors: Array<IRoomFilter>,
        frameColors: Array<IRoomFilter>,
        targetAudiences: Array<any>,
        styles: Array<any>
    }

    export interface IRoomCollection {
        isEmpty: boolean,
        isHovered: boolean,
        variants: Array<IRoomVariant>
    }

    export interface IRoomVariant {
        facingColors: Array<IRoomFilter>,
        facings: Array<IRoomFilter>,
        frameColors: Array<IRoomFilter>,
        styles: Array<IRoomFilter>,
        targetAudiences: Array<IRoomFilter>
    }

    export interface IRoomFilter {
        id: string,
        name: string,
        syncCode1C: string,
        checked: boolean,
        isUnnecessary: boolean
    }

}