﻿import * as angular from "angular";

export default class RoomController {

    public collections: Array<Dto.RoomItem.IRoomCollection>;
    public originCollections: Array<Dto.RoomItem.IRoomCollection>;
    public room: string;
    public roomAlias: string;
    public facings: Array<Dto.RoomItem.IRoomFilter>;
    public facingColors: Array<Dto.RoomItem.IRoomFilter>;
    public frameColors: Array<Dto.RoomItem.IRoomFilter>;
    public targetAudiences: Array<Dto.RoomItem.IRoomFilter>;
    public styles: Array<Dto.RoomItem.IRoomFilter>;
    public collectionsOriginalCount: number;
    public collectionsFilteredCount: number;
    public taModeled: any;

    constructor(public RoomService: Room.Services.IRoomApiService,
        public RoomJQService: any,
        public RoomFilterService: any
    ) {
        this.roomAlias = RoomJQService.getRoomAlias();
        this.init();
    };

    private init(): void {
        this.RoomService.getCollections(this.roomAlias)
            .then((collectionsdata: Dto.RoomItem.IRoomCollectionsData) => {

                this.collections = angular.copy(collectionsdata.collections);
                this.collectionsOriginalCount = this.collections.length;
                this.originCollections = angular.copy(collectionsdata.collections);
                //TODO: зачем это поле?
                this.room = collectionsdata.room;

                this.facings = collectionsdata.facings.map((facing: Dto.RoomItem.IRoomFilter) => {
                    facing.checked = false;
                    return facing;
                });
                this.facingColors = collectionsdata.facingColors.map((facingColor: Dto.RoomItem.IRoomFilter) => {
                    facingColor.checked = false;
                    return facingColor;
                });
                this.frameColors = collectionsdata.frameColors.map((frameColor: Dto.RoomItem.IRoomFilter) => {
                    frameColor.checked = false;
                    return frameColor;
                });
                this.styles = collectionsdata.styles.map((style: Dto.RoomItem.IRoomFilter) => {
                    style.checked = false;
                    return style;
                });
                this.targetAudiences = collectionsdata.targetAudiences.map((targetAudience: Dto.RoomItem.IRoomFilter) => {
                    targetAudience.checked = false;
                    return targetAudience;
                });
                //так как ЦА на бэкэнде не смоделированны как сложный объект с внутренней иерархией по группам, то делаем это тут
                this.taModeled = {
                    forAll: {
                        man: this.targetAudiences.filter((ta: Dto.RoomItem.IRoomFilter) => ta.syncCode1C === "Для мужчин")[0],
                        woman: this.targetAudiences.filter((ta: Dto.RoomItem.IRoomFilter) => ta.syncCode1C === "Для женщин")[0],
                    },
                    forChild: {
                        forGirls: {
                            girlsYoung: this.targetAudiences.filter((ta: Dto.RoomItem.IRoomFilter) => ta.syncCode1C === "Для девочек до 12 лет")[0],
                            girlsOld: this.targetAudiences.filter((ta: Dto.RoomItem.IRoomFilter) => ta.syncCode1C === "Для девочек от 12 до 18 лет")[0]
                        },
                        forBoys: {
                            boysYoung: this.targetAudiences.filter((ta: Dto.RoomItem.IRoomFilter) => ta.syncCode1C === "Для мальчиков до 12 лет")[0],
                            boysOld: this.targetAudiences.filter((ta: Dto.RoomItem.IRoomFilter) => ta.syncCode1C === "Для мальчиков от 12 до 18 лет")[0]
                        }
                    },
                    forFamilies: {
                        couples: this.targetAudiences.filter((ta: Dto.RoomItem.IRoomFilter) => ta.syncCode1C === "Для молодых пар")[0],
                        families: this.targetAudiences.filter((ta: Dto.RoomItem.IRoomFilter) => ta.syncCode1C === "Для семей с детьми")[0]
                    }
                }

                this.changeFilter();
            });
    }

    public clickFiltersGroup(filters: { [key: string]: Dto.RoomItem.IRoomFilter }) {
        Object.keys(filters)
            .filter(p => !!filters[p])
            .forEach(p => {
                if (!filters[p].isUnnecessary) {
                    if (filters[p].checked === true) {
                        filters[p].checked = false;
                    }
                    else {
                        filters[p].checked = true;
                    }
                    //we need to do it with every single filter because they can be affected by eachother 
                    this.changeFilter();
                }
            });
    }

    public clickFiltersSuperGroup() {
        this.clickFiltersGroup(this.taModeled.forChild.forGirls);
        this.clickFiltersGroup(this.taModeled.forChild.forBoys);
    }

    private filterByFacingColor(collections: Array<Dto.RoomItem.IRoomCollection>): Array<Dto.RoomItem.IRoomCollection> {
        return this.RoomFilterService.filterByFacingColor(collections, this.facingColors);
    }

    private filterByFacing(collections: Array<Dto.RoomItem.IRoomCollection>): Array<Dto.RoomItem.IRoomCollection> {
        return this.RoomFilterService.filterByFacing(collections, this.facings);
    }

    private filterByFrameColor(collections: Array<Dto.RoomItem.IRoomCollection>): Array<Dto.RoomItem.IRoomCollection> {
        return this.RoomFilterService.filterByFrameColor(collections, this.frameColors);
    }

    private filterByTargetAudience(collections: Array<Dto.RoomItem.IRoomCollection>): Array<Dto.RoomItem.IRoomCollection> {
        return this.RoomFilterService.filterByTargetAudience(collections, this.targetAudiences);
    }

    private filterByStyle(collections: Array<Dto.RoomItem.IRoomCollection>): Array<Dto.RoomItem.IRoomCollection> {
        return this.RoomFilterService.filterByStyle(collections, this.styles);
    }

    public changeFilter(): void {
        this.collections = this.filterByTargetAudience(this.filterByStyle(this.filterByFacing(this.filterByFacingColor(this.filterByFrameColor(angular.copy(this.originCollections))))));
        this.calculateCollectionsFilteredCount();
        this.deleteUnnecessaryFilters();
    }

    private calculateCollectionsFilteredCount(): void {
        this.collectionsFilteredCount = 0;
        this.collections.forEach((collection: Dto.RoomItem.IRoomCollection) => {
            if (collection.variants === undefined || collection.variants.length <= 0) {
                collection.isEmpty = true;
            }
            else {
                collection.isEmpty = false;
                this.collectionsFilteredCount++;
            }
        });
    }

    private deleteUnnecessaryFilters() {
        var facings = [];
        this.collections.forEach((collection: Dto.RoomItem.IRoomCollection) => collection.variants.forEach((variant: Dto.RoomItem.IRoomVariant) => variant.facings.forEach((facing: Dto.RoomItem.IRoomFilter) => {
            facings.push(facing);
        })));
        facings = this.getUniqueById(facings);
        this.facings.forEach((facing: Dto.RoomItem.IRoomFilter) => {
            if (facings.filter((s: Dto.RoomItem.IRoomFilter) => { return s.id === facing.id }).length > 0)
                facing.isUnnecessary = false;
            else {
                facing.isUnnecessary = true;
            }
        });

        var facingColors = [];
        this.collections.forEach((collection: Dto.RoomItem.IRoomCollection) => collection.variants.forEach((variant: Dto.RoomItem.IRoomVariant) => variant.facingColors.forEach((facingColor: Dto.RoomItem.IRoomFilter) => {
            facingColors.push(facingColor);
        })));
        facingColors = this.getUniqueById(facingColors);
        this.facingColors.forEach((facingColor: Dto.RoomItem.IRoomFilter) => {
            if (facingColors.filter((s: Dto.RoomItem.IRoomFilter) => { return s.id === facingColor.id }).length > 0)
                facingColor.isUnnecessary = false;
            else {
                facingColor.isUnnecessary = true;
            }
        });

        var frameColors = [];
        this.collections.forEach((collection: Dto.RoomItem.IRoomCollection) => collection.variants.forEach((variant: Dto.RoomItem.IRoomVariant) => variant.frameColors.forEach((frameColor: Dto.RoomItem.IRoomFilter) => {
            frameColors.push(frameColor);
        })));
        frameColors = this.getUniqueById(frameColors);
        this.frameColors.forEach((frameColor: Dto.RoomItem.IRoomFilter) => {
            if (frameColors.filter((s: Dto.RoomItem.IRoomFilter) => { return s.id === frameColor.id }).length > 0)
                frameColor.isUnnecessary = false;
            else {
                frameColor.isUnnecessary = true;
            }
        });

        var targetAudiences = [];
        this.collections.forEach((collection: Dto.RoomItem.IRoomCollection) => collection.variants.forEach((variant: Dto.RoomItem.IRoomVariant) => variant.targetAudiences.forEach((targetAudience: Dto.RoomItem.IRoomFilter) => {
            targetAudiences.push(targetAudience);
        })));
        targetAudiences = this.getUniqueById(targetAudiences);
        this.targetAudiences.forEach((targetAudience: Dto.RoomItem.IRoomFilter) => {
            if (targetAudiences.filter((s: Dto.RoomItem.IRoomFilter) => { return s.id === targetAudience.id }).length > 0)
                targetAudience.isUnnecessary = false;
            else {
                targetAudience.isUnnecessary = true;
            }
        });

        var styles = [];
        this.collections.forEach((collection: Dto.RoomItem.IRoomCollection) => collection.variants.forEach((variant: Dto.RoomItem.IRoomVariant) => variant.styles.forEach((style: Dto.RoomItem.IRoomFilter) => {
            styles.push(style);
        })));
        styles = this.getUniqueById(styles);
        this.styles.forEach((style: Dto.RoomItem.IRoomFilter) => {
            if (styles.filter((s: Dto.RoomItem.IRoomFilter) => { return s.id === style.id }).length > 0)
                style.isUnnecessary = false;
            else {
                style.isUnnecessary = true;
            }
        });
    }

    private getUniqueById(a: Array<any>): Array<any> {
        var n = {}, r = [];
        for (var i = 0; i < a.length; i++) {
            if (!n[a[i].id]) {
                n[a[i].id] = true;
                r.push(a[i]);
            }
        }
        return r;
    }

    public goToCollectionVariant(collectionAlias: string, variantId: string): void {
        document.cookie = "variantId=" + variantId + "; path=/;";
        var element = document.getElementById("variant_link_" + variantId);
        element.setAttribute("href", "/catalog/" + this.roomAlias + "/" + collectionAlias);
    }
}