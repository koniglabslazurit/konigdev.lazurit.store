﻿export default class RoomApiService implements Room.Services.IRoomApiService {

    private url = "/api/collection";
    private roomTypesByProductUrl = "/api/room/single-product";

    constructor(public $http: angular.IHttpService) { }

    getCollections(roomAlias: string): angular.IPromise<any> {
        var me = this;
        return me.$http({
            url: this.url,
            method: 'GET',
            params: { alias: roomAlias }
        }).then((response: angular.IHttpPromiseCallbackArg<any>) => {
            return response.data;
        });
    };

    getRoomTypesByProduct(request: Dto.Product.ProductRequest): angular.IPromise<Array<Dto.RooomType.IRoomByProductItem>> {
        return this.$http({
            url: this.roomTypesByProductUrl,
            params: request,
            method: 'GET'
        }).then((response: angular.IHttpPromiseCallbackArg<Array<Dto.RooomType.IRoomByProductItem>>) => {
            return response.data;
        });
    }
}