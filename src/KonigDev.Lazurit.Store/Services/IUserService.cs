﻿using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;
using System;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Services
{
    //todo вынести в папку Services
    public interface IUserService
    {
        Task<UserDto> GetUserById(Guid id);
        Task<UserDto> GetUserByName(string userName);
        Task<UserDto> GetUserByPhone(string phone);
    }
    //todo вынести в папку Services
    public class UserService : IUserService
    {
        private readonly IAuthHandlersFactory _authFactory;

        public UserService(IAuthHandlersFactory authFactory)
        {
            _authFactory = authFactory;
        }

        public async Task<UserDto> GetUserById(Guid id)
        {
            var handler = _authFactory.CreateQueryHandler<GetUserByIdQuery, UserDto>();
            var result = await handler.Execute(new GetUserByIdQuery { Id = id });
            return result;
        }

        public async Task<UserDto> GetUserByName(string userName)
        {
            var user = await _authFactory.CreateQueryHandler<GetUserByEmailQuery, UserDto>().Execute(new GetUserByEmailQuery { Email = userName });
            return user;
        }

        public async Task<UserDto> GetUserByPhone(string phone)
        {
            var user = await _authFactory.CreateQueryHandler<GetUserByPhoneQuery, UserDto>().Execute(new GetUserByPhoneQuery { UserPhone = phone });
            return user;
        }
    }
}