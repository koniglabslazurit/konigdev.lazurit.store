﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.FullCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.MiniCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Products;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Query;
using KonigDev.Lazurit.Store.Dto.Cart.CartFull;
using KonigDev.Lazurit.Store.Dto.Cart.CartMini;
using KonigDev.Lazurit.Store.Dto.Prices.Interfaces;
using KonigDev.Lazurit.Store.Extensions;
using KonigDev.Lazurit.Store.Extensions.Converters.Cart.CartFull;
using KonigDev.Lazurit.Store.Extensions.Converters.Cart.CartMini;
using KonigDev.Lazurit.Store.Models.Prices;
using KonigDev.Lazurit.Store.Models.Prices.KonigDev.Lazurit.Store.Models.Prices;
using KonigDev.Lazurit.Store.Services.CookieService;

namespace KonigDev.Lazurit.Store.Services.PriceSrvice
{
    public class PriceService : IPriceService
    {
        private readonly IHubHandlersFactory _hubFactory;
        public PriceService(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        public DtoCartFullPriceResult CalculateCartFullPrice(Guid priceZoneID, decimal deliveryPriceForCity, Currency currency, DtoFullCart cartFull)
        {
            if (cartFull == null)
                throw new NotImplementedException(nameof(cartFull) + "in CalculateCartPriceAsync ==null");

            DtoCartFullPriceResult cartWithPrice = cartFull.ToPriceResult();
            var pricesForProductCompletedTask = new TaskCompletionSource<List<DtoCartFullItemPriceProduct>>();
            var pricesForProductResultTask = pricesForProductCompletedTask.Task;
            var pricesForComplectsCompletedTask = new TaskCompletionSource<List<DtoCartFullItemPriceComplect>>();
            var pricesForComplectsResultTask = pricesForComplectsCompletedTask.Task;
            var pricesForCollectionCompletedTask = new TaskCompletionSource<List<DtoCartFullItemPriceProductInCollection>>();
            var pricesForCollectionResultTask = pricesForCollectionCompletedTask.Task;
            //список задач для поралельной обработки
            var taskForWait = new List<Task>();

            if (cartWithPrice.Products != null && cartWithPrice.Products.Any())
            {
                pricesForProductResultTask = GetProductsPrices(priceZoneID, currency, cartWithPrice.Products.ToList());
                taskForWait.Add(pricesForProductResultTask);
            }
            if (cartWithPrice.Collections != null && cartWithPrice.Collections.Any())
            {
                pricesForCollectionResultTask = GetProductsPrices(priceZoneID, currency, cartWithPrice.Collections.SelectMany(x => x.Products).ToList());
                taskForWait.Add(pricesForCollectionResultTask);
            }
            if (cartWithPrice.Complects != null && cartWithPrice.Complects.Any())
            {
                pricesForComplectsResultTask = GetComplectPrices(priceZoneID, currency, cartWithPrice.Complects.ToList());
                taskForWait.Add(pricesForComplectsResultTask);
            }

            Task.WhenAll(taskForWait);

            Money complectTotalPrice = new Money(currency);
            Money productTotalPrice = new Money(currency);
            cartWithPrice.PriceForDelivery = cartWithPrice.IsDeliveryRequired ? deliveryPriceForCity.ToMoney(currency) : new Money(currency);

            //Todo сделать обработку ситуаций, когда у тасков статус равен Failed
            if (pricesForProductResultTask.Status == TaskStatus.RanToCompletion && pricesForProductResultTask.Result != null && pricesForProductResultTask.Result.Any())
            {
                productTotalPrice = pricesForProductResultTask.Result.Aggregate(productTotalPrice, (current, item) => current + item.SalePrice * item.Quantity);
            }
            if (pricesForCollectionResultTask.Status == TaskStatus.RanToCompletion && pricesForCollectionResultTask.Result != null && pricesForCollectionResultTask.Result.Any())
            {
                Debug.Assert(cartWithPrice.Collections != null, "cartWithPrice.Collections != null");
                foreach (var collection in cartWithPrice.Collections)
                {
                    collection.CollectionSalePrice = new Money(currency);
                    foreach (var product in collection.Products)
                    {
                        var collectionProduct = pricesForCollectionResultTask.Result.Single(x => x.ItemId == product.ItemId && x.CartItemCollectionId == collection.Id);
                        productTotalPrice = productTotalPrice + collectionProduct.SalePrice * collectionProduct.Quantity;
                        collection.CollectionSalePrice = collection.CollectionSalePrice + collectionProduct.SalePrice * collectionProduct.Quantity;
                    }
                }
            }

            if (pricesForComplectsResultTask.Status == TaskStatus.RanToCompletion && pricesForComplectsResultTask.Result != null && pricesForComplectsResultTask.Result.Any())
            {
                complectTotalPrice = pricesForComplectsResultTask.Result.Aggregate(complectTotalPrice, (current, item) => current + item.SalePrice * item.Quantity);
            }

            if (!cartWithPrice.Collections.Any() && !cartWithPrice.Complects.Any() && !cartWithPrice.Products.Any())
                cartWithPrice.TotalPrice = new Money(0d, currency);
            else
                cartWithPrice.TotalPrice = productTotalPrice + complectTotalPrice + cartWithPrice.PriceForDelivery;
            return cartWithPrice;
        }


        public DtoCartMiniPriceResult CalculateCartMiniPrice(Guid priceZoneID, decimal deliveryPriceForCity, Currency currency, DtoMiniCart cartMini)
        {
            if (cartMini == null)
                throw new NotImplementedException(nameof(cartMini) + "in CalculateCartPriceAsync ==null");

            DtoCartMiniPriceResult cartWithPrice = cartMini.ToPriceResult();
            var pricesForProductCompletedTask = new TaskCompletionSource<List<DtoCartMiniItemPriceProduct>>();
            var pricesForProductResultTask = pricesForProductCompletedTask.Task;
            var pricesForComplectsCompletedTask = new TaskCompletionSource<List<DtoCartMiniItemPriceComplect>>();
            var pricesForComplectsResultTask = pricesForComplectsCompletedTask.Task;
            var pricesForCollectionCompletedTask = new TaskCompletionSource<List<DtoCartMiniItemPriceProductInCollection>>();
            var pricesForCollectionResultTask = pricesForCollectionCompletedTask.Task;
            //список задач для поралельной обработки
            var taskForWait = new List<Task>();

            if (cartWithPrice.Products != null && cartWithPrice.Products.Any())
            {
                pricesForProductResultTask = GetProductsPrices(priceZoneID, currency, cartWithPrice.Products.ToList());
                taskForWait.Add(pricesForProductResultTask);
            }
            if (cartWithPrice.Collections != null && cartWithPrice.Collections.Any())
            {
                pricesForCollectionResultTask = GetProductsPrices(priceZoneID, currency, cartWithPrice.Collections.SelectMany(x => x.Products).ToList());
                taskForWait.Add(pricesForCollectionResultTask);
            }
            if (cartWithPrice.Complects != null && cartWithPrice.Complects.Any())
            {
                pricesForComplectsResultTask = GetComplectPrices(priceZoneID, currency, cartWithPrice.Complects.ToList());
                taskForWait.Add(pricesForComplectsResultTask);
            }

            Task.WhenAll(taskForWait);

            Money complectTotalPrice = new Money(currency);
            Money productTotalPrice = new Money(currency);
            cartWithPrice.PriceForDelivery = cartWithPrice.IsDeliveryRequired ? deliveryPriceForCity.ToMoney(currency) : new Money(currency);

            //Todo сделать обработку ситуаций, когда у тасков статус равен Failed
            if (pricesForProductResultTask.Status == TaskStatus.RanToCompletion && pricesForProductResultTask.Result != null && pricesForProductResultTask.Result.Any())
            {
                productTotalPrice = pricesForProductResultTask.Result.Aggregate(productTotalPrice, (current, item) => current + item.SalePrice * item.Quantity);
            }
            if (pricesForCollectionResultTask.Status == TaskStatus.RanToCompletion && pricesForCollectionResultTask.Result != null && pricesForCollectionResultTask.Result.Any())
            {
                Debug.Assert(cartWithPrice.Collections != null, "cartWithPrice.Collections != null");
                foreach (var collection in cartWithPrice.Collections)
                {
                    collection.CollectionSalePrice = new Money(currency);
                    foreach (var product in collection.Products)
                    {
                        var collectionProduct = pricesForCollectionResultTask.Result.Single(x => x.ItemId == product.ItemId && x.CartItemCollectionId == collection.Id);
                        productTotalPrice = productTotalPrice + collectionProduct.SalePrice * collectionProduct.Quantity;
                        collection.CollectionSalePrice = collection.CollectionSalePrice + collectionProduct.SalePrice * collectionProduct.Quantity;
                    }
                }
            }

            if (pricesForComplectsResultTask.Status == TaskStatus.RanToCompletion && pricesForComplectsResultTask.Result != null && pricesForComplectsResultTask.Result.Any())
            {
                complectTotalPrice = pricesForComplectsResultTask.Result.Aggregate(complectTotalPrice, (current, item) => current + item.SalePrice * item.Quantity);
            }

            if (!cartWithPrice.Collections.Any() && !cartWithPrice.Complects.Any() && !cartWithPrice.Products.Any())
                cartWithPrice.TotalPrice = new Money(0d, currency);
            else
                cartWithPrice.TotalPrice = productTotalPrice + complectTotalPrice + cartWithPrice.PriceForDelivery;
            return cartWithPrice;
        }


        public async Task<List<T>> GetProductsPrices<T>(Guid priceZoneId, Currency currency, List<T> products) where T : class, IProductPrice
        {
            var getPriceProduct = _hubFactory.CreateQueryHandler<GetProductsPricesQuery, List<DtoProductPricesResult>>();
            var productPrices = await getPriceProduct.Execute(new GetProductsPricesQuery { ProductIds = products.Select(x => x.ItemId).ToList(), CurrencyCode = currency.Code, PriceZoneId = priceZoneId });

            foreach (var price in productPrices)
            {
                foreach (var product in products)
                {
                    if (price.ProductId == product.ItemId)
                    {
                        var productPriceMoney = price.Price.ToMoney(currency);
                        product.Price = productPriceMoney;
                        var productPriceForAssemblyMoney = price.AssemblyPrice.ToMoney(currency);
                        product.PriceForAssembly = productPriceForAssemblyMoney;
                        price.Discount = price.Discount ?? 0;
                        product.Discount = (int)price.Discount.Value;
                        product.DiscountInstallment6Month = price.Installment6Month;
                        product.DiscountInstallment10Month = price.Installment10Month;
                        product.DiscountInstallment12Month = price.Installment12Month;
                        product.DiscountInstallment18Month = price.Installment18Month;
                        product.DiscountInstallment24Month = price.Installment24Month;
                        product.DiscountInstallment36Month = price.Installment36Month;
                        product.DiscountInstallment48Month = price.Installment48Month;

                        product.SalePrice = (productPriceMoney - (productPriceMoney * product.Discount / 100) + (productPriceForAssemblyMoney * Convert.ToInt16(product.IsAssemblyRequired)));
                        product.SalePriceWithAssembly = (productPriceMoney - (productPriceMoney * product.Discount / 100) + productPriceForAssemblyMoney);
                        product.SalePriceWithOutAssembly = (productPriceMoney - (productPriceMoney * product.Discount / 100));

                        /* рассрочки */
                        product.SalePriceInstallment6Month = (productPriceMoney - (productPriceMoney * price.Installment6Month / 100));
                        product.SalePriceInstallment6MonthWithAssembly = (productPriceMoney - (productPriceMoney * price.Installment6Month / 100) + (productPriceForAssemblyMoney * Convert.ToInt16(product.IsAssemblyRequired)));

                        product.SalePriceInstallment10Month = (productPriceMoney - (productPriceMoney * price.Installment10Month / 100));
                        product.SalePriceInstallment10MonthWithAssembly = (productPriceMoney - (productPriceMoney * price.Installment10Month / 100) + (productPriceForAssemblyMoney * Convert.ToInt16(product.IsAssemblyRequired)));

                        product.SalePriceInstallment12Month = (productPriceMoney - (productPriceMoney * price.Installment12Month / 100));
                        product.SalePriceInstallment12MonthWithAssembly = (productPriceMoney - (productPriceMoney * price.Installment12Month / 100) + (productPriceForAssemblyMoney * Convert.ToInt16(product.IsAssemblyRequired)));

                        product.SalePriceInstallment18Month = (productPriceMoney - (productPriceMoney * price.Installment18Month / 100));
                        product.SalePriceInstallment18MonthWithAssembly = (productPriceMoney - (productPriceMoney * price.Installment18Month / 100) + (productPriceForAssemblyMoney * Convert.ToInt16(product.IsAssemblyRequired)));

                        product.SalePriceInstallment24Month = (productPriceMoney - (productPriceMoney * price.Installment24Month / 100));
                        product.SalePriceInstallment24MonthWithAssembly = (productPriceMoney - (productPriceMoney * price.Installment24Month / 100) + (productPriceForAssemblyMoney * Convert.ToInt16(product.IsAssemblyRequired)));

                        product.SalePriceInstallment36Month = (productPriceMoney - (productPriceMoney * price.Installment36Month / 100));
                        product.SalePriceInstallment36MonthWithAssembly = (productPriceMoney - (productPriceMoney * price.Installment36Month / 100) + (productPriceForAssemblyMoney * Convert.ToInt16(product.IsAssemblyRequired)));

                        product.SalePriceInstallment48Month = (productPriceMoney - (productPriceMoney * price.Installment48Month / 100));
                        product.SalePriceInstallment48MonthWithAssembly = (productPriceMoney - (productPriceMoney * price.Installment48Month / 100) + (productPriceForAssemblyMoney * Convert.ToInt16(product.IsAssemblyRequired)));
                    }
                }
            }
            return await Task.FromResult(products);
        }

        public async Task<List<T>> GetComplectPrices<T>(Guid priceZoneId, Currency currency, List<T> complects) where T : class, IComplectPrice
        {
            var getPriceComplect = _hubFactory.CreateQueryHandler<GetComplectsPricesQuery, List<DtoComplectPricesResult>>();
            var complectPrices = await getPriceComplect.Execute(new GetComplectsPricesQuery { ComplectIds = complects.Select(x => x.ItemId).ToList(), CurrencyCode = currency.Code, PriceZoneId = priceZoneId });

            foreach (var price in complectPrices)
            {
                foreach (var complect in complects)
                {
                    if (price.ComplectId == complect.ItemId)
                    {
                        var complectPriceMoney = price.Price.ToMoney(currency);
                        complect.Price = complectPriceMoney;
                        var complectPriceForAssemblyMoney = price.AssemblyPrice.ToMoney(currency);
                        complect.PriceForAssembly = complectPriceForAssemblyMoney;
                        price.Discount = price.Discount ?? 0;
                        complect.Discount = (int)price.Discount.Value;
                        complect.SalePrice = (complectPriceMoney - (complectPriceMoney * complect.Discount / 100) + (complectPriceForAssemblyMoney * Convert.ToInt16(complect.IsAssemblyRequired)));
                        complect.SalePriceWithAssembly = (complectPriceMoney - (complectPriceMoney * complect.Discount / 100) + complectPriceForAssemblyMoney);
                        complect.SalePriceWithOutAssembly = (complectPriceMoney - (complectPriceMoney * complect.Discount / 100));
                    }
                }
            }
            return await Task.FromResult(complects);
        }
    }
}