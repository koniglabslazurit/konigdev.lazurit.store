﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.FullCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.MiniCartDto;
using KonigDev.Lazurit.Store.Dto.Cart.CartFull;
using KonigDev.Lazurit.Store.Dto.Cart.CartMini;
using KonigDev.Lazurit.Store.Dto.Prices.Interfaces;
using KonigDev.Lazurit.Store.Models.Prices;

namespace KonigDev.Lazurit.Store.Services.PriceSrvice
{
    public interface IPriceService
    {
        DtoCartFullPriceResult CalculateCartFullPrice(Guid priceZoneID, decimal deliveryPriceForCity, Currency currency, DtoFullCart cartMini);

        DtoCartMiniPriceResult CalculateCartMiniPrice(Guid priceZoneID, decimal deliveryPriceForCity, Currency currency, DtoMiniCart cartMini);

        Task<List<T>> GetProductsPrices<T>(Guid priceZoneId, Currency currency, List<T> products) where T : class, IProductPrice;

        Task<List<T>> GetComplectPrices<T>(Guid priceZoneId, Currency currency, List<T> complects) where T : class, IComplectPrice;
    }
}