﻿using System;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;
using KonigDev.Lazurit.Store.Dto.Cart;
using KonigDev.Lazurit.Store.Models.Localization;
using KonigDev.Lazurit.Store.Models.Prices;

namespace KonigDev.Lazurit.Store.Services.Cart.Interfaces
{
    public interface ICartService<T> where T : IDtoCart
    {
        #region Cart
        Task UpdateCart(Guid userId, Currency currency, Guid priceZoneId, decimal deliveryForCity);
        Task RemoveCart(Guid userId, Currency currency, Guid priceZoneId, decimal deliveryForCity);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cartId"></param>
        /// <param name="currency"></param>
        /// <param name="priceZoneId"></param>
        /// <param name="deliveryForCity"></param>
        /// <param name="waitWhileCacheUpdate">True-если необходимо дождаться, когда корзина обновится в кэше. 
        /// К примеру после запроса на получение сразу потребуется по каким-то причинам работать с кешем и корзиной. Пример Удалить.</param>
        /// <returns></returns>
        Task<T> GetCart(Guid cartId, Currency currency, Guid priceZoneId, decimal deliveryForCity, bool waitWhileCacheUpdate = false);
        #endregion

        #region Product

        Task AddProductToCart(DtoAddProductToCartRequest request, Guid cartId);
        Task DeleteProductFromCart(Guid id, Guid cartId);
        Task UpdateProductInCart(DtoUpdateProductInCartRequest request, Guid cartId);
        
        #endregion

        #region Collection

        Task AddCollectionToCart(DtoAddCollectionToCartRequest request, Guid cartId);
        Task DeleteItemCollectionFromCart(Guid product, Guid collection, Guid cartId);
        Task DeleteFullCollectionFromCart(Guid collection, Guid cartId);
        Task UpdateProductCollectionInCart(DtoUpdateCollectionProductInCartRequest request, Guid cartId);
      
        #endregion

        #region Complect
        Task AddComplectToCart(DtoAddComplectToCartRequest request, Guid cartId);
        Task DeleteItemComplectFromCart(Guid id, Guid cartId);
        Task UpdateComplectInCart(DtoUpdateComplectInCartRequest request, Guid cartId);

        #endregion

    }
}
