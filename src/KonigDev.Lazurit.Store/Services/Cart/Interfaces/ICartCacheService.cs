﻿using System;
using System.Threading.Tasks;
using KonigDev.Lazurit.Store.Dto.Cart.CartMini;

namespace KonigDev.Lazurit.Store.Services.Cart.Interfaces
{
    public interface ICartCacheService
    {
        void DeleteAllCartCache();

        /// <summary>
        /// Добавить или обновить корзину 
        /// </summary>
        /// <param name="cart"></param>
        /// <param name="cartId"></param>
        /// <param name="waitWhileCacheUpdate">True-если необходимо дождаться, когда корзина обновится в кэше. 
        /// К примеру после запроса на получение сразу потребуется по каким-то причинам работать с кешем и корзиной. Пример Удалить.</param>
        /// <returns></returns>
        Task SaveCart(DtoCartMiniPriceResult cart, Guid cartId, bool waitWhileCacheUpdate = true);

        /// <summary>
        /// Связать корзину с пользователем.
        /// </summary>
        /// <param name="userId">Id пользователя в системе</param>
        /// <param name="cartId">Id корзины</param>
        /// <returns></returns>
        Task RelateCartWithUser(Guid userId, Guid cartId);

        /// <summary>
        /// Получить корзину пользователя
        /// </summary>
        Task<DtoCartMiniPriceResult> GetCart(Guid cartId);
        /// <summary>
        /// Удалить корзину из кэша
        /// </summary>
        void RemoveCart(Guid cartId);
    }
}