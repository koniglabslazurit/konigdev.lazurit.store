﻿using System;

namespace KonigDev.Lazurit.Store.Services.Cart.Interfaces
{
    public interface ICommonCartService
    {
        void UpdateCarts(Guid userId);
    }
}