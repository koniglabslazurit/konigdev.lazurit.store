﻿using KonigDev.Lazurit.Store.Models.Prices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace KonigDev.Lazurit.Store.Services.Cart.Interfaces
{
    public interface ICartCommonService
    {
        Task RelateUserWithCart(Guid userId, Guid cartId, Currency currency, Guid priceZoneId, decimal deliveryForCity);
        void ResetCart();
    }
}