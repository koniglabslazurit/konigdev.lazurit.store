﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.MiniCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Query;
using KonigDev.Lazurit.Store.Dto.Cart.CartMini;
using KonigDev.Lazurit.Store.Models.Constants;
using KonigDev.Lazurit.Store.Models.Prices;
using KonigDev.Lazurit.Store.Services.Cart.Interfaces;
using KonigDev.Lazurit.Store.Services.CookieService;
using KonigDev.Lazurit.Store.Services.PriceSrvice;
using KonigDev.Lazurit.Store.SignalrService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace KonigDev.Lazurit.Store.Services.Cart.Implementations
{
    internal class CartCommonService : ICartCommonService
    {
        private readonly ICartCacheService _cartCacheService;
        private readonly IPriceService _priceService;
        private readonly IHubHandlersFactory _hubFactory;
        private readonly ICookieService _cookieService;
        private readonly ISignalrService _signalrService;
        private readonly ICartService<DtoCartMiniPriceResult> _cartMiniService;

        public CartCommonService(IHubHandlersFactory hubFactory, ICartCacheService cartCacheService, IPriceService priceService,
            ICookieService cookieService, ICartService<DtoCartMiniPriceResult> cartMiniService, ISignalrService signalrService)
        {
            _hubFactory = hubFactory;
            _cartCacheService = cartCacheService;
            _priceService = priceService;
            _cookieService = cookieService;
            _cartMiniService = cartMiniService;
            _signalrService = signalrService;
        }

        //TODO блокировать коризну на UI асинхронно пока идёт мердж. 
        public async Task RelateUserWithCart(Guid userId, Guid cartId, Currency currency, Guid priceZoneId, decimal deliveryForCity)
        {
            var userCart = await _hubFactory.CreateQueryHandler<GetMiniCartByUserIdQuery, DtoMiniCart>().Execute(new GetMiniCartByUserIdQuery
            {
                UserId = userId
            });
            //Забираем анонимную из корзину в память
            var anonimCart = await _hubFactory.CreateQueryHandler<GetMiniCartByIdQuery, DtoMiniCart>().Execute(new GetMiniCartByIdQuery
            {
                CartId = cartId
            });

            //Удаляем с источников хранения корзину
            await _hubFactory.CreateCommandHandler<DeleteCartCommand>().Execute(new DeleteCartCommand
            {
                CartId = cartId
            });
            _cartCacheService.RemoveCart(cartId);

            //Если нет ещё не одной из корзин, то надо для юзера создать корзину
            if (userCart == null && anonimCart == null)
            {
                userCart = new DtoMiniCart
                {
                    Id = cartId
                };
            }

            DtoMiniCart mergedCart = await MergeCarts(userCart, userId, anonimCart, cartId, currency, priceZoneId, deliveryForCity);
        }

        public void ResetCart()
        {
            var newCartId = Guid.NewGuid();
            var currentCartId = _cookieService.GetCookie(Common.CartCookieId);
            if (!string.IsNullOrWhiteSpace(currentCartId))
                _cookieService.UpdateCookie(Common.CartCookieId, newCartId.ToString());
            _signalrService.ChangeHubIdentity(currentCartId.ToString(), newCartId.ToString());
            _signalrService.UpdateCart(newCartId);
        }

        private async Task<DtoMiniCart> MergeCarts(DtoMiniCart userCart, Guid userId, DtoMiniCart anonimCart, Guid cookieCartId, Currency currency, Guid priceZoneId, decimal deliveryForCity)
        {
            var totalCart = GetAmountForCarts(userCart, anonimCart);
            await _hubFactory.CreateCommandHandler<CreateOrUpdateCommand>().Execute(new CreateOrUpdateCommand
            {
                NewCart = totalCart,
                UserId = userId
            });
            _cookieService.UpdateCookie(Common.CartCookieId, totalCart.Id.ToString());
            _signalrService.ChangeHubIdentity(cookieCartId.ToString(), totalCart.Id.ToString());
            await _cartMiniService.UpdateCart(totalCart.Id, currency, priceZoneId, deliveryForCity);
            return totalCart;
        }

        private DtoMiniCart GetAmountForCarts(DtoMiniCart userCart, DtoMiniCart anonimCart)
        {
            if (userCart == null)
                return anonimCart;
            if (anonimCart == null)
                return userCart;
            anonimCart.Collections.ForEach(x => x.CartId = userCart.Id);
            var newCollections = anonimCart.Collections.Where(x => !userCart.Collections.Any(y => x.CollectionVariantId == y.CollectionVariantId));
            userCart.Collections.AddRange(newCollections);

            anonimCart.Complects.ForEach(x => x.CartId = userCart.Id);
            var newComplects = anonimCart.Complects.Where(x => !userCart.Complects.Any(y => x.ItemId == y.ItemId));
            userCart.Complects.AddRange(newComplects);

            anonimCart.Products.ForEach(x => x.CartId = userCart.Id);
            var newProducts = anonimCart.Products.Where(x => !userCart.Products.Any(y => x.ItemId == y.ItemId));
            userCart.Products.AddRange(newProducts);
            return userCart;
        }
    }
}