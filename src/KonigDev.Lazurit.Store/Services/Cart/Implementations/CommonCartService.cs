﻿using System;
using System.Threading.Tasks;
using KonigDev.Lazurit.Store.Dto.Cart;
using KonigDev.Lazurit.Store.Dto.Cart.CartFull;
using KonigDev.Lazurit.Store.Dto.Cart.CartMini;
using KonigDev.Lazurit.Store.Services.Cart.Interfaces;

namespace KonigDev.Lazurit.Store.Services.Cart.Implementations
{
    /// <summary>
    /// Сервис, необхоимый на случай, если понадобится работать с двумя корзинами одновременно
    /// </summary>
    public class CommonCartService : ICommonCartService
    {
        private readonly ICartService<DtoCartFullPriceResult> _cartFullService;
        private readonly ICartService<DtoCartMiniPriceResult> _cartMiniService;

        public CommonCartService(ICartService<DtoCartFullPriceResult> cartFullService, ICartService<DtoCartMiniPriceResult> cartMiniService)
        {
            this._cartFullService = cartFullService;
            this._cartMiniService = cartMiniService;
        }

        public void UpdateCarts(Guid userId)
        {
            throw new NotImplementedException();
        }
    }
}