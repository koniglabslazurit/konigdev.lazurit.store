﻿using System;
using System.Threading.Tasks;
using KonigDev.Lazurit.Store.Dto.Cart.CartMini;
using KonigDev.Lazurit.Store.Dto.Cart.CartMini.Elastic;
using KonigDev.Lazurit.Store.Extensions.Filters;
using KonigDev.Lazurit.Store.Services.Cart.Interfaces;
using Nest;

namespace KonigDev.Lazurit.Store.Services.Cart.Implementations
{
    public class CartElasticCacheService : ICartCacheService
    {
        private readonly ElasticClient _clientByID;

        //TODO все считывания из конфига можно с обработкой ошибок в сервис запихнуть
        public CartElasticCacheService(string protokol, string domain, string cartIndex)
        {
            var settingsById = new ConnectionSettings(new Uri($"{protokol}://{domain}"))
               .DefaultIndex(cartIndex).InferMappingFor<DtoMiniCartCached>(x => x.IdProperty(p => p.CartId));
            _clientByID = new ElasticClient(settingsById);
        }
        public void DeleteAllCartCache()
        {
            _clientByID.DeleteIndex(_clientByID.ConnectionSettings.DefaultIndex);
        }

        public async Task SaveCart(DtoCartMiniPriceResult cart, Guid cartId,bool waitWhileCacheUpdate=true)
        {
            DtoMiniCartCached cachedCart = null;

            var response = await _clientByID.GetAsync<DtoMiniCartCached>(cartId);
            cachedCart = response?.Source ?? new DtoMiniCartCached();
            cachedCart.DtoCartMiniPriceResult = cart;
            cachedCart.CartId = cart.Id;
            await _clientByID.UpdateAsync(DocumentPath<DtoMiniCartCached>
             .Id(cachedCart.CartId), u => u
             .DocAsUpsert()
             .Doc(cachedCart).Refresh(waitWhileCacheUpdate?Elasticsearch.Net.Refresh.WaitFor:Elasticsearch.Net.Refresh.False));
        }

        /// <summary>
        /// Связать корзину с пользователем.
        /// </summary>
        /// <param name="userId">Id пользователя в системе</param>
        /// <param name="cartId">Id корзины</param>
        /// <returns></returns>
        public async Task RelateCartWithUser(Guid userId, Guid cartId)
        {
            var response = await _clientByID.GetAsync<DtoMiniCartCached>(cartId);
            DtoMiniCartCached cachedCart = response?.Source;
            if (cachedCart == null)
                throw new NotImplementedException("Не удаётся связать корзину, которой нет в кэше");

            cachedCart.UserId = userId.ToElasticGuidStore();
            await _clientByID.UpdateAsync(DocumentPath<DtoMiniCartCached>
            .Id(cachedCart.CartId), u => u
            .DocAsUpsert(false)
            .Doc(cachedCart).Refresh(Elasticsearch.Net.Refresh.WaitFor));
        }

        public async Task<DtoCartMiniPriceResult> GetCart(Guid cartId)
        {
            var response = await _clientByID.GetAsync<DtoMiniCartCached>(cartId);
            return response?.Source?.DtoCartMiniPriceResult;
        }

        public void RemoveCart(Guid cartId)
        {
            _clientByID.Delete<DtoMiniCartCached>(cartId);
        }
    }
}