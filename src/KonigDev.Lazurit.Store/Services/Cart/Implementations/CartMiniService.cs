﻿using System;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.MiniCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Query;
using KonigDev.Lazurit.Store.Dto.Cart.CartMini;
using KonigDev.Lazurit.Store.Models.Prices;
using KonigDev.Lazurit.Store.Services.Cart.Interfaces;
using KonigDev.Lazurit.Store.Services.PriceSrvice;
using KonigDev.Lazurit.Store.SignalrService;

namespace KonigDev.Lazurit.Store.Services.Cart.Implementations
{
    public class CartMiniService : CartBaseService, ICartService<DtoCartMiniPriceResult>
    {
        private readonly IHubHandlersFactory _hubFactory;
        private readonly ISignalrService _signalrService;
        private readonly ICartCacheService _cartCache;
        private readonly IPriceService _priceService;

        public CartMiniService(IHubHandlersFactory hubFactory, ISignalrService signalrService, ICartCacheService cartCache, IPriceService priceService) : base(hubFactory)
        {
            _hubFactory = hubFactory;
            _signalrService = signalrService;
            _cartCache = cartCache;
            _priceService = priceService;
        }

        public async Task UpdateCart(Guid cartId, Currency currency, Guid priceZoneId, decimal deliveryForCity)
        {
            var cart = await GetCurrentCart(cartId, currency, priceZoneId, deliveryForCity);
            if (cart == null)  return;
            await _cartCache.SaveCart(cart, cartId,false);
            _signalrService.UpdateCart(cart);
        }

        public async Task RemoveCart(Guid cartId, Currency currency, Guid priceZoneId, decimal deliveryForCity)
        {
            var cart = await GetCurrentCartWitCache(cartId, currency, priceZoneId, deliveryForCity);
            if (cart == null)
                return;
            await _hubFactory.CreateCommandHandler<DeleteCartCommand>().Execute(new DeleteCartCommand
            {
                CartId = cart.Id
            });
            _cartCache.RemoveCart(cartId);
            _signalrService.UpdateCart(cartId);
        }

        public virtual async Task<DtoCartMiniPriceResult> GetCart(Guid cartId, Currency currency, Guid priceZoneId, decimal deliveryForCity, bool waitWhileCacheUpdate = false)
        {
            var cart = await _cartCache.GetCart(cartId);
            if (cart == null)
            {
                cart = await GetCurrentCart(cartId, currency, priceZoneId, deliveryForCity);
                if (cart == null) return null;

                await _cartCache.SaveCart(cart, cartId, waitWhileCacheUpdate);
            }
            return cart;
        }

        private async Task<DtoCartMiniPriceResult> GetCurrentCartWitCache(Guid cartId, Currency currency, Guid priceZoneId, decimal deliveryForCity)
        {
            var cart = await _cartCache.GetCart(cartId);
            if (cart == null)
            {
                cart = await GetCurrentCart(cartId, currency, priceZoneId, deliveryForCity);
            }
            return cart;
        }

        private async Task<DtoCartMiniPriceResult> GetCurrentCart(Guid cartId, Currency currency, Guid priceZoneId, decimal deliveryForCity)
        {
            var cart = await _hubFactory.CreateQueryHandler<GetMiniCartByIdQuery, DtoMiniCart>().Execute(new GetMiniCartByIdQuery
            {
                CartId = cartId
            });

            if (cart != null)
                return _priceService.CalculateCartMiniPrice(priceZoneId, deliveryForCity, currency, cart);
            else
                return null;
        }
    }
}