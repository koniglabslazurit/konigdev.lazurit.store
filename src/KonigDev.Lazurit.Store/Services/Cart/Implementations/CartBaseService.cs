﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using KonigDev.Lazurit.Core.Enums;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;
using KonigDev.Lazurit.Store.Dto.Cart;

namespace KonigDev.Lazurit.Store.Services.Cart.Implementations
{
    public class CartBaseService
    {
        private readonly IHubHandlersFactory _hubFactory;

        public CartBaseService(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        #region Product
        public async Task AddProductToCart(DtoAddProductToCartRequest request, Guid cartId)
        {
            await _hubFactory.CreateCommandHandler<AddCartItemCommand>().Execute(new AddCartItemCommand
            {
                AddElementId = request.ProductId,
                CartItemType = EnumCartItemType.Product,
                CartId = cartId
            });
        }

        public async Task DeleteProductFromCart(Guid id, Guid cartId)
        {
            await _hubFactory.CreateCommandHandler<RemoveCartItemCommand>().Execute(new RemoveCartItemCommand
            {
                CartItemType = EnumCartItemType.Product,
                RemoveProductId = id,
                CartId = cartId
            });

        }
        public async Task UpdateProductInCart(DtoUpdateProductInCartRequest request, Guid cartId)
        {
            await _hubFactory.CreateCommandHandler<UpdateCartItemCommand>().Execute(new UpdateCartItemCommand
            {
                CartItemType = EnumCartItemType.Product,
                Quantity = request.Quantity,
                UpdateProductId = request.ProductId,
                CartId = cartId
            });
        }
        #endregion

        #region Collection

        public async Task AddCollectionToCart(DtoAddCollectionToCartRequest request, Guid cartId)
        {
            await _hubFactory.CreateCommandHandler<AddCartItemCommand>().Execute(new AddCartItemCommand
            {
                AddElementId = request.CollectionId,
                CartItemType = EnumCartItemType.Collection,
                CartId = cartId
            });
        }

        public async Task DeleteItemCollectionFromCart(Guid product, Guid collection, Guid cartId)
        {
            await _hubFactory.CreateCommandHandler<RemoveCartItemCommand>().Execute(new RemoveCartItemCommand
            {
                CartItemType = EnumCartItemType.ProductInCollection,
                RemoveProductId = product,
                RemoveCollectionId = collection,
                CartId = cartId
            });
        }

        public async Task DeleteFullCollectionFromCart(Guid collection, Guid cartId)
        {
            await _hubFactory.CreateCommandHandler<RemoveCartItemCommand>().Execute(new RemoveCartItemCommand
            {
                CartItemType = EnumCartItemType.Collection,
                RemoveCollectionId = collection,
                CartId = cartId
            });
        }

        public async Task UpdateProductCollectionInCart(DtoUpdateCollectionProductInCartRequest request, Guid cartId)
        {
            await _hubFactory.CreateCommandHandler<UpdateCartItemCommand>().Execute(new UpdateCartItemCommand
            {
                CartItemType = EnumCartItemType.Collection,
                Quantity = request.Quantity,
                UpdateProductId = request.ProductId,
                UpdateCollectionId = request.CollectionId,
                CartId = cartId
            });
        }
        #endregion

        #region Complect

        public async Task AddComplectToCart(DtoAddComplectToCartRequest request, Guid cartId)
        {
            await _hubFactory.CreateCommandHandler<AddCartItemCommand>().Execute(new AddCartItemCommand
            {
                AddElementId = request.CollectionId,
                CartItemType = EnumCartItemType.Complect,
                CartId = cartId
            });
        }

        public async Task DeleteItemComplectFromCart(Guid id, Guid cartId)
        {
            await _hubFactory.CreateCommandHandler<RemoveCartItemCommand>().Execute(new RemoveCartItemCommand
            {
                CartItemType = EnumCartItemType.Complect,
                RemoveCollectionId = id,
                CartId = cartId
            });
        }

        public async Task UpdateComplectInCart(DtoUpdateComplectInCartRequest request, Guid cartId)
        {
            await _hubFactory.CreateCommandHandler<UpdateCartItemCommand>().Execute(new UpdateCartItemCommand
            {
                CartItemType = EnumCartItemType.Collection,
                Quantity = request.Quantity,
                UpdateCollectionId = request.CollectionId,
                CartId = cartId
            });
        }
        #endregion

    }
}