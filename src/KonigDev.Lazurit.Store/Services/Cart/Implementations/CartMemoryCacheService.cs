﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using KonigDev.Lazurit.Store.Dto.Cart.CartMini;
using KonigDev.Lazurit.Store.Services.Cart.Interfaces;

namespace KonigDev.Lazurit.Store.Services.Cart.Implementations
{
    [Obsolete("Используйте кэш на основе ElasticSearch")]
    public class CartMemoryCacheService : ICartCacheService
    {
        private readonly Dictionary<Guid, DtoCartMiniPriceResult> _cache = new Dictionary<Guid, DtoCartMiniPriceResult>();
            
        public Task<DtoCartMiniPriceResult> GetCart(Guid userId)
        {
            //if (_cache.ContainsKey(userId))
            //{
            //    return Task.FromResult(_cache[userId]);
            //}
            //return null;
            throw new NotImplementedException();

        }

        public void RemoveCart(Guid userId)
        {
            //if (_cache.ContainsKey(userId))
            //{
            //    _cache.Remove(userId);
            //}
            throw new NotImplementedException();

        }

        public  Task SaveCart(DtoCartMiniPriceResult cart, Guid userId, bool waitWhileCacheUpdate = true)
        {
            //if (!_cache.ContainsKey(userId))
            //{
            //     _cache.Add(userId, cart);
            //}
            //else
            //{
            //    _cache[userId] = cart;
            //}
            //await Task.FromResult(0);
            throw new NotImplementedException();
        }

        public void DeleteAllCartCache()
        {
            _cache.Clear();
        }

        public Task RelateCartWithUser(Guid userId, Guid cartId)
        {
            throw new NotImplementedException();
        }
    }
}