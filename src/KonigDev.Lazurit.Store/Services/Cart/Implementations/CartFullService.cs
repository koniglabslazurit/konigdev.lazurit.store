﻿using System;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.FullCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Query;
using KonigDev.Lazurit.Store.Dto.Cart.CartFull;
using KonigDev.Lazurit.Store.Models.Prices;
using KonigDev.Lazurit.Store.Services.Cart.Interfaces;
using KonigDev.Lazurit.Store.Services.PriceSrvice;

namespace KonigDev.Lazurit.Store.Services.Cart.Implementations
{
    public class CartFullService : CartBaseService, ICartService<DtoCartFullPriceResult>
    {
        private readonly IHubHandlersFactory _hubFactory;
        private readonly IPriceService _priceService;

        public CartFullService(IHubHandlersFactory hubFactory, IPriceService priceService) :base(hubFactory)
        {
            _hubFactory = hubFactory;
            _priceService = priceService;
        }

        public Task UpdateCart(Guid userId, Currency currency, Guid priceZoneId, decimal deliveryForCity)
        {
            return Task.FromResult(0);
        }

        public async Task RemoveCart(Guid cartId, Currency currency, Guid priceZoneId, decimal deliveryForCity)
        {
            var cart = await GetCurrentCart(cartId, currency, priceZoneId, deliveryForCity);
            await _hubFactory.CreateCommandHandler<DeleteCartCommand>().Execute(new DeleteCartCommand
            {
                CartId = cart.Id
            });
        }
        public virtual async Task<DtoCartFullPriceResult> GetCart(Guid cartId, Currency currency,  Guid priceZoneId, decimal deliveryForCity, bool waitWhileCacheUpdate = true)
        {
            return await GetCurrentCart(cartId, currency, priceZoneId, deliveryForCity);
        }

        private async Task<DtoCartFullPriceResult> GetCurrentCart(Guid cartId, Currency currency, Guid priceZoneId, decimal deliveryForCity)
        {
            var cart = await _hubFactory.CreateQueryHandler<GetFullCartByIdQuery, DtoFullCart>().Execute(new GetFullCartByIdQuery
            {
                CartId = cartId
            });
            if (cart != null)
                return _priceService.CalculateCartFullPrice(priceZoneId, deliveryForCity,currency, cart);
            else
                return null;
        }
    }
}