﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace KonigDev.Lazurit.Store.Services.CustomRequest
{
    public class CustomRequestService: ICustomRequestService
    {
        public string CreateRequest(string url)
        {
            var response = (new WebClient()).DownloadString(url);
            return response;
        }
    }
}