﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Services.CustomRequest
{
    public interface ICustomRequestService
    {
        string CreateRequest(string url);
    }
}
