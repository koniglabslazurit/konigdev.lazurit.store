﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Services.CookieService
{
    public class CookieService : ICookieService
    {
        public string GetCookie(string cookieName)
        {
            HttpRequest request = HttpContext.Current.Request;
            if (request.Cookies.Get(cookieName) != null)
            {
                var httpCookie = request.Cookies.Get(cookieName);
                return httpCookie != null ? HttpUtility.UrlDecode(httpCookie.Value) : string.Empty;
            }
            else
            {
                return string.Empty;
            }
        }

        public void AddCookie(string cookieName,string value,string path)
        {
            var responce = HttpContext.Current.Response;
            responce.Cookies.Add(new HttpCookie(cookieName) {Value=value,Path=path});
        }

        public void UpdateCookie(string cookieName,string value)
        {
            HttpContext.Current.Response.Cookies[cookieName].Value = value;
        }

        public void DeleteCookie(string cookieName)
        {
            HttpRequest request = HttpContext.Current.Request;
            if (request.Cookies[cookieName] != null)
            {
                HttpCookie myCookie = new HttpCookie(cookieName);
                myCookie.Value = null;
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                request.Cookies.Add(myCookie);
            }
        }
    }
}