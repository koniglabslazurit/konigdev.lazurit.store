﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Services.CookieService
{
    public interface ICookieService
    {
        string GetCookie(string cookieName);
        void AddCookie(string cookieName, string value, string path);
        void UpdateCookie(string cookieName, string value);
        void DeleteCookie(string cookieName);
    }
}