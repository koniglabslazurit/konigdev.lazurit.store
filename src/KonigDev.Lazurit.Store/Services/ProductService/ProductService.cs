﻿using System;
using System.Collections.Generic;
using KonigDev.Lazurit.Store.ViewModels.ProductViewModels;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;
using System.Linq;
using KonigDev.Lazurit.Auth.Model.DTO.Common;
using KonigDev.Lazurit.Store.Dto.Prices;
using AutoMapper;

namespace KonigDev.Lazurit.Store.Services.ProductService
{
    public class ProductService : IProductService
    {
        public DtoCommonTableResponse<VmProductMarketerModel> MapProductsToViewModel(DtoProductMarketerTableResponse productMarketer)
        {
            var result = new DtoCommonTableResponse<VmProductMarketerModel> { TotalCount = productMarketer.TotalCount };
            var items = new List<VmProductMarketerModel>();

            foreach (var item in productMarketer.Items)
            {
                var marketerVendorCode = new VmProductMarketerModel
                {
                    VendorCode = item.VendorCode,
                    Range = item.Range
                };
                foreach (var product in item.Products.OrderByDescending(product => product.MarketerRange))
                {
                    /* для отображения продуктов в маркетинговом артикле выбираем уникальыне по размерам и берем максимальный продкут по весу, если их несколько */
                    if (!marketerVendorCode.Products.Where(pr => pr.Length == product.Length && pr.Width == product.Width && pr.Height == product.Height).Any())
                    {
                        marketerVendorCode.Products.Add(Mapper.Map<DtoProductPrice>(product)); /* мапинг автомапером */
                        marketerVendorCode.Sizes.Add(new VmProductSize { Height = product.Height, Length = product.Length, Width = product.Width, ProductId = product.Id });
                    }
                }
                /* отдаем как дефлотный продукт тот, у которого максимальный вес. */
                marketerVendorCode.MaxRangeProductId = marketerVendorCode.Products.OrderByDescending(p => p.MarketerRange).FirstOrDefault()?.Id ?? Guid.Empty;
                items.Add(marketerVendorCode);
            }
            result.Items = items;
            return result;
        }
    }
}