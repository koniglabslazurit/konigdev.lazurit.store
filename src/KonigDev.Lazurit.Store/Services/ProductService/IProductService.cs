﻿using KonigDev.Lazurit.Auth.Model.DTO.Common;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;
using KonigDev.Lazurit.Store.ViewModels.ProductViewModels;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.Services.ProductService
{
    public interface IProductService
    {
        DtoCommonTableResponse<VmProductMarketerModel> MapProductsToViewModel(DtoProductMarketerTableResponse productMarketer);
    }
}