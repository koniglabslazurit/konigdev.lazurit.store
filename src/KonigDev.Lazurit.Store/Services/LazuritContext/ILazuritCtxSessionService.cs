﻿using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;
using KonigDev.Lazurit.Store.Dto.Prices;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.Services.LazuritContext
{
    public interface ILazuritCtxSessionService
    {
        T GetFromSession<T>(string key);
        void SetInSession(string key, object value);
        ContextPriceInfo GetPriceInfoFromContext();
        System.Guid GetSingleCartId();
        List<System.Guid> GetLayawaytIds();
        void AddLayawaysCookie(List<System.Guid> setLayawayIds);
    }
}