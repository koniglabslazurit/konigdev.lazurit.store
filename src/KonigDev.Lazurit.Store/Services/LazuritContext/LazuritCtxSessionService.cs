﻿using System;
using System.Web;
using KonigDev.Lazurit.Store.Dto.Prices;
using KonigDev.Lazurit.Store.Models.Localization;
using KonigDev.Lazurit.Store.Models.Prices;
using KonigDev.Lazurit.Store.Services.CookieService;
using KonigDev.Lazurit.Store.Models.Constants;
using System.Collections.Generic;
using System.Linq;

namespace KonigDev.Lazurit.Store.Services.LazuritContext
{
    public class LazuritCtxSessionService : ILazuritCtxSessionService
    {
        private readonly ICookieService _cookieService;
        public LazuritCtxSessionService(ICookieService cookieService)
        {
            _cookieService = cookieService;
        }

        public T GetFromSession<T>(string key)
        {
            return (T)HttpContext.Current.Session[key];
        }

        public void SetInSession(string key, object value)
        {
            HttpContext.Current.Session[key] = value;
        }

        public ContextPriceInfo GetPriceInfoFromContext()
        {
            string zoneCookie = _cookieService.GetCookie("zoneId");
            decimal deliveryPriceForCity = decimal.Parse(_cookieService.GetCookie("delivery"));
            if (string.IsNullOrEmpty(zoneCookie))
                throw new NotImplementedException();
            var priceZoneId = Guid.Parse(zoneCookie);
            //TODO при локализации добавляем ILanguageProvider
            var lang = new Language(LocalizeConstant.DefaultCulture);
            var currency = new Currency(lang, LocalizeConstant.DefaultCurrencyCode, String.Empty, Common.RubSymbol, 1);
            return new ContextPriceInfo
            {
                Currency = currency,
                DeliveryPriceForCity = deliveryPriceForCity,
                PriceZoneId = priceZoneId
            };
        }
        /// <summary>
        /// Получает Cart Id из кук. Если там его нет, то создаёт новый Cart id
        /// </summary>
        /// <returns></returns>
        public Guid GetSingleCartId()
        {
            Guid cartFindId;
            string cartId = _cookieService.GetCookie(Common.CartCookieId);
            if (!string.IsNullOrWhiteSpace(cartId) && Guid.TryParse(cartId, out cartFindId))
            {
                return cartFindId;
            }
            else
            {
                cartFindId = Guid.NewGuid();
                _cookieService.AddCookie(Common.CartCookieId, cartFindId.ToString(), "/");
                return cartFindId;
            }
        }

        /// <summary>
        /// Получает массив Layaway Id из кук
        /// </summary>
        /// <returns></returns>
        public List<Guid> GetLayawaytIds()
        {
            string strLayaways = _cookieService.GetCookie(Common.LayawayCookieId);
            var layawayIds = strLayaways
                .Split(',')
                .Where(g => { Guid temp; return Guid.TryParse(g, out temp); })
                .Select(g => Guid.Parse(g))
                .ToList();
           
            return layawayIds;
        }

        public void AddLayawaysCookie(List<Guid> setLayawayIds)
        {
            var strLayaways = string.Join(",", setLayawayIds);
            _cookieService.AddCookie(Common.LayawayCookieId, strLayaways, "/");
        }
    }
}