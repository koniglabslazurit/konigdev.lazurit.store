﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Layaways;
using KonigDev.Lazurit.Hub.Model.DTO.Layaways.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Layaways.Query;
using KonigDev.Lazurit.Store.Models.Constants;
using KonigDev.Lazurit.Store.Services.CookieService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace KonigDev.Lazurit.Store.Services.LayawayService
{
    public class LayawayService : ILayawayService
    {
        private readonly IHubHandlersFactory _hubFactory;
        private readonly ICookieService _cookieService;

        public LayawayService(IHubHandlersFactory hubFactory, ICookieService cookieService)
        {
            _hubFactory = hubFactory;
            _cookieService = cookieService;
        }

        public async Task MergeLayaways(Guid userId, List<Guid> layawayIds)
        {
            if (layawayIds != null && layawayIds.Count != 0)
            {
                await _hubFactory.CreateCommandHandler<MergeLayawasCommand>().Execute(new MergeLayawasCommand
                {
                    LayawayIds = layawayIds,
                    UserId = userId
                });
                _cookieService.UpdateCookie(Common.LayawayCookieId, "");
            }
        }

        public void ResetLayaway()
        {
            var currentLayawaysId = _cookieService.GetCookie(Common.LayawayCookieId);
            if (!string.IsNullOrWhiteSpace(currentLayawaysId))
                _cookieService.UpdateCookie(Common.LayawayCookieId, "");
        }
    }
}