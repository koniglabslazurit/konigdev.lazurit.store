﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace KonigDev.Lazurit.Store.Services.LayawayService
{
    public interface ILayawayService
    {
        Task MergeLayaways(Guid userId, List<Guid> layawayIds);
        void ResetLayaway();
    }
}