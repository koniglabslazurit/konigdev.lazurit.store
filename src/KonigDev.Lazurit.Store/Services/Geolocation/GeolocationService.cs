﻿using KonigDev.Lazurit.Core.Services;
using KonigDev.Lazurit.Store.Dto.Geolocation;
using KonigDev.Lazurit.Store.Services.CustomRequest;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Linq;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Queries;

namespace KonigDev.Lazurit.Store.Services.Geolocation
{
    public class GeolocationService: IGeolocationService
    {        
        public GeolocationService(ICustomRequestService customRequest, ILoggingService logging)
        {          
        }

        public GetCityByCoordinates ParseCoordinates(DtoCoordinates coords)
        {
            double latitude = 0;
            double longitude = 0;
            Double.TryParse(coords.Latitude, out latitude);
            Double.TryParse(coords.Longitude, out longitude);
            return new GetCityByCoordinates
            {
                Latitude = Math.Round(latitude, 4),
                Longitude = Math.Round(longitude, 4)
            };
        }
        
    }
}