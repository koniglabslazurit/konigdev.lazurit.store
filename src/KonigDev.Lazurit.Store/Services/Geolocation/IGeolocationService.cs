﻿using KonigDev.Lazurit.Hub.Model.DTO.Cities.Queries;
using KonigDev.Lazurit.Store.Dto.Geolocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace KonigDev.Lazurit.Store.Services.Geolocation
{
    public interface IGeolocationService
    {
        GetCityByCoordinates ParseCoordinates(DtoCoordinates coords);        
    }
}