﻿using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Users;
using KonigDev.Lazurit.Hub.Model.DTO.Users.Queries;

namespace KonigDev.Lazurit.Store.Services.Profile
{
    public class ProfileService : IProfileService
    {
        private IHubHandlersFactory _hubFactory;
        public ProfileService(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }
        public async Task<DtoUserProfile> GetUserProfile(GetUserProfileByIdQuery getUserQuery)
        {
            var getProfileHandler = _hubFactory.CreateQueryHandler<GetUserProfileByIdQuery, DtoUserProfile>();
            var result = await getProfileHandler.Execute(getUserQuery);
            if (!string.IsNullOrEmpty(result.Email) && result.Email.StartsWith("Anonym"))
            {
                result.Email = null;
            }
            if (!string.IsNullOrEmpty(result.FirstName) && result.FirstName.StartsWith("Anonym"))
            {
                result.FirstName = null;
            }
            return result;
        }
    }
}