﻿using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Users;
using KonigDev.Lazurit.Hub.Model.DTO.Users.Queries;

namespace KonigDev.Lazurit.Store.Services.Profile
{
    public interface IProfileService
    {
        Task<DtoUserProfile> GetUserProfile(GetUserProfileByIdQuery getUserQuery);
    }
}