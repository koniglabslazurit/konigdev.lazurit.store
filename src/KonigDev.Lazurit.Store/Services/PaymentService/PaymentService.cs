﻿using KonigDev.Lazurit.Core.Services;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Payment.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Payment.Query;
using KonigDev.Lazurit.Store.Dto.Kkm;
using KonigDev.Lazurit.Store.Dto.Payment;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Services.PaymentService
{
    public class PaymentService : IPaymentService
    {
        private readonly IHubHandlersFactory _hubFactory;
        private readonly ILoggingService _loggingService;
        private readonly string _kkrDomainUrl;
        public PaymentService(string kkrDomainUrl, IHubHandlersFactory hubFactory, ILoggingService loggingService)
        {
            _kkrDomainUrl = kkrDomainUrl;
            _hubFactory = hubFactory;
            _loggingService = loggingService;
        }
        public DtoPaymentSettings GetPaymentSettings()
        {
            var handler = _hubFactory.CreateQueryHandler<GetPaymentSettingsQuery, DtoPaymentSettings>();
            return handler.Execute(new GetPaymentSettingsQuery()).Result;
        }
        public DtoPaymentSignatureValue GetPaymentSignature(DtoPaymentSettings settings, long orderNumber, string sum)
        {
            if (string.IsNullOrEmpty(settings.MerchantLogin) || string.IsNullOrEmpty(settings.Password1) || string.IsNullOrEmpty(settings.RobokassaUrl) || orderNumber <= 0)
            {
                _loggingService.InfoMessage("payment settings not found or sum less zero or invalid order number");
                return new DtoPaymentSignatureValue { SignatureValue = null };
            }
            var signature = string.Format("{0}:{1}:{2}:{3}",
                settings.MerchantLogin, sum,
                orderNumber, settings.Password1);
            var md5 = new MD5CryptoServiceProvider();
            var signatureValue = BitConverter.ToString(md5.ComputeHash(Encoding.UTF8.GetBytes(signature))).Replace("-", "").ToLower();
            return new DtoPaymentSignatureValue { SignatureValue = signatureValue };
        }

        public bool IsValidResultResponse(DtoRobokassaResult result)
        {
            var settings = this.GetPaymentSettings();
            if (settings.Password1 == null)
            {
                _loggingService.InfoMessage("Password1 not found");
                return false;
            }
            var signature = string.Format("{0}:{1}:{2}", result.OutSum, result.InvId, settings.Password1);
            var md5 = new MD5CryptoServiceProvider();
            var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(signature));
            var signatureValue = BitConverter.ToString(hash).Replace("-", "").ToLower();

            if (!String.Equals(result.SignatureValue, signatureValue, StringComparison.CurrentCultureIgnoreCase))
            {
                _loggingService.InfoMessage(string.Format("RoboKassa: invalid signature first: {0} , second: {1}", result.SignatureValue, signatureValue));
                return false;
            }

            var orderQuery = _hubFactory.CreateQueryHandler<GetOrderItemByNumberQuery, DtoOrderItem>();
            var order = orderQuery.Execute(new GetOrderItemByNumberQuery { Number = Convert.ToInt64(result.InvId) }).Result;
            if (order.Sum.ToString(CultureInfo.InvariantCulture).Replace(",", ".") != result.OutSum)
            {
                _loggingService.InfoMessage(string.Format("RoboKassa: invalid sum {0} {1}", result.OutSum, order.Sum));
                return false;
            }
            if (order.IsPaid == true)
            {
                _loggingService.InfoMessage(string.Format("RoboKassa: order №{0} is paid yet", order.Number));
                return false;
            }

            return true;
        }

        public void UpdateOrderPaid(DtoRobokassaResult result, bool isPaid)
        {
            var orderQuery = _hubFactory.CreateQueryHandler<GetOrderItemByNumberQuery, DtoOrderItem>();
            var order = orderQuery.Execute(new GetOrderItemByNumberQuery { Number = Convert.ToInt64(result.InvId) }).Result;
            if (order == null)
                _loggingService.InfoMessage("Order is null");
            var updateOrderPaidCommand = _hubFactory.CreateCommandHandler<UpdateOrderPaidCommand>();
            var updateResult = updateOrderPaidCommand.Execute(new UpdateOrderPaidCommand { Id = order.Id, IsPaid = isPaid });
        }

        public async Task<DtoKkmResponse> GetRestKkmResult(DtoKkmRequest kkmRequest)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_kkrDomainUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var httpFormContent = new FormUrlEncodedContent(new[]
               {
                    new KeyValuePair<string, string>("outSum", kkmRequest.OutSum),
                    new KeyValuePair<string, string>("invId", kkmRequest.InvId),
                    new KeyValuePair<string, string>("signatureValue", kkmRequest.SignatureValue),
                });

                var response = await client.PostAsync(string.Empty, httpFormContent);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    string responce = await response.Content.ReadAsStringAsync();
                    string kkmOrderRecordUrl = JObject.Parse(responce)["kkmOrderRecordUrl"].ToString();


                    if (string.IsNullOrEmpty(kkmOrderRecordUrl))
                    {
                        return null;
                    }
                    else return new DtoKkmResponse { KkmOrderRecordUrl = kkmOrderRecordUrl };
                }
                else
                    return null;
            }
        }
    }
}