﻿using KonigDev.Lazurit.Hub.Model.DTO.Order.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Payment.Dto;
using KonigDev.Lazurit.Store.Dto.Kkm;
using KonigDev.Lazurit.Store.Dto.Payment;
using System;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Services.PaymentService
{
    public interface IPaymentService
    {
        DtoPaymentSettings GetPaymentSettings();
        DtoPaymentSignatureValue GetPaymentSignature(DtoPaymentSettings settings, long orderNumber, string sum);
        bool IsValidResultResponse(DtoRobokassaResult result);
        void UpdateOrderPaid(DtoRobokassaResult result, bool isPaid);
        Task<DtoKkmResponse> GetRestKkmResult(DtoKkmRequest dto);
    }
}
