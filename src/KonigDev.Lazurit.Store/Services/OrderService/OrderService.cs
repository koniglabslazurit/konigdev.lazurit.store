﻿using System;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Dto;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Command;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Query;
using KonigDev.Lazurit.Store.Extensions.Converters.Cart.CartFull;
using KonigDev.Lazurit.Store.Providers;
using KonigDev.Lazurit.Store.ViewModels.OrderViewModels;
using KonigDev.Lazurit.Store.SignalrService;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;
using KonigDev.Lazurit.Store.Services.LazuritContext;

namespace KonigDev.Lazurit.Store.Services.OrderService
{
    public class OrderService : IOrderService
    {
        private readonly ICartProvider _cartProvider;
        private readonly IHubHandlersFactory _hubFactory;
        private readonly ILazuritCtxSessionService _lazuritCtxService;
        private readonly ISignalrService _signalrService;

        public OrderService(ICartProvider cartProvider, IHubHandlersFactory hubFactory, ILazuritCtxSessionService lazuritCtxService, ISignalrService signalrService)
        {
            _cartProvider = cartProvider;
            _hubFactory = hubFactory;
            _lazuritCtxService = lazuritCtxService;
            _signalrService = signalrService;
        }

        /* тут сервис который создает ордер и запрашивает по нему данные. поэтому возвращаемый */
        public async Task<DtoOrderItem> CreateOrder(Guid userId, VmOrderCreating model)
        {
            var priceInfo = _lazuritCtxService.GetPriceInfoFromContext();
            var cart = await _cartProvider.ForOnlyFullCart().GetCart(_lazuritCtxService.GetSingleCartId(), priceInfo.Currency, priceInfo.PriceZoneId, priceInfo.DeliveryPriceForCity,true);
            CreateOrderCommand command = cart.ToOrder();
            command.DeliveryAddress = model.DeliveryAddress;
            command.DeliveryKladrCode = model.DeliveryKladrCode;
            command.PaymentMethodId = model.PaymentMethodId;
            command.UserProfileId = userId;
            /* set need seller to verification order */
            command.IsApprovedBySeller = !model.IsRequiredSeller;

            /* create order */
            await _hubFactory.CreateCommandHandler<CreateOrderCommand>().Execute(command);
            /* delete cart */
            await _cartProvider.ForOnlyMiniCart().RemoveCart(cart.Id, priceInfo.Currency, priceInfo.PriceZoneId, priceInfo.DeliveryPriceForCity);

            //await _hubFactory.CreateCommandHandler<DeleteCartCommand>().Execute(new DeleteCartCommand { CartId = cart.Id });
            ///* call signalR to update cart */
            //_signalrService.UpdateCart(_lazuritCtxService.GetSingleCartId());

            /* get order detail */
            return await _hubFactory.CreateQueryHandler<GetOrderItemQuery, DtoOrderItem>().Execute(new GetOrderItemQuery { Id = command.Id });
        }

        public async Task UpdateApprove(Guid id, bool isApproved)
        {
            await _hubFactory.CreateCommandHandler<UpdateOrderApproveCommand>()
                .Execute(new UpdateOrderApproveCommand { Id = id, IsApproved = isApproved });
        }

        public async Task UpdateOrder1CFields(Guid id, string orderNumber1C, DateTime? creationTime1C)
        {
            await _hubFactory.CreateCommandHandler<UpdateOrder1CFieldsCommand>()
                .Execute(new UpdateOrder1CFieldsCommand { Id = id, OrderNumber1C = orderNumber1C, CreationTime1C = creationTime1C });
        }

        public async Task UpdatePaidStatus(Guid id, bool isPaid)
        {
            await _hubFactory.CreateCommandHandler<UpdateOrderPaidCommand>()
                .Execute(new UpdateOrderPaidCommand { Id = id, IsPaid = isPaid });
        }

        public async Task<DtoOrder> GetOrder(Guid id)
        {
            return await _hubFactory.CreateQueryHandler<GetOrderQuery, DtoOrder>().Execute(new GetOrderQuery { Id = id });
        }

        public async Task<DtoOrderItem> GetOrderDetails(Guid id)
        {
            return await _hubFactory.CreateQueryHandler<GetOrderItemQuery, DtoOrderItem>().Execute(new GetOrderItemQuery { Id = id});
        }
        public async Task<DtoOrderTableResponse> GetOrderList(GetOrdersQuery request)
        {
            return await _hubFactory.CreateQueryHandler<GetOrdersQuery, DtoOrderTableResponse>()
                .Execute(request);
        }

        public async Task UpdateOrderKkmFields(string invId, string kkmOrderRecordUrl)
        {
            await _hubFactory.CreateCommandHandler<UpdateOrderKkmFieldsCommand>()
                .Execute(new UpdateOrderKkmFieldsCommand { InvId = invId, KkmOrderRecordUrl = kkmOrderRecordUrl });
        }
    }
}