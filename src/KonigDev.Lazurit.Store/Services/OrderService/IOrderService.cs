﻿using System;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Query;
using KonigDev.Lazurit.Store.ViewModels.OrderViewModels;

namespace KonigDev.Lazurit.Store.Services.OrderService
{
    public interface IOrderService
    {
        Task<DtoOrderItem> CreateOrder(Guid userId, VmOrderCreating model);
        Task UpdateApprove(Guid id, bool isApproved);
        Task UpdatePaidStatus(Guid id, bool isPaid);
        /// <summary>
        /// Обновляет значения полей "Номер заказа в 1С" и "Дата оформления заказа в 1С" по входным параметрам
        /// </summary>
        /// <param name="id">Id заказа</param>
        /// <param name="orderNumber1C">Номер заказа в 1С</param>
        /// <param name="creationTime1C">Дата оформления заказа в 1С</param>
        /// <returns>Task</returns>
        Task UpdateOrder1CFields(Guid id, string orderNumber1C, DateTime? creationTime1C);
        Task<DtoOrder> GetOrder(Guid id);
        Task<DtoOrderItem> GetOrderDetails(Guid id);
        Task<DtoOrderTableResponse> GetOrderList(GetOrdersQuery request);
        /// <summary>
        /// Обновление значения поля заказа "Ссылка на страницу документа из Ккм"
        /// </summary>
        /// <param name="id">Целочисленный номер заказа</param>
        /// <param name="kkmOrderRecordUrl">Значение для обновляемого поля</param>
        /// <returns></returns>
        Task UpdateOrderKkmFields(string invId, string kkmOrderRecordUrl);
    }
}