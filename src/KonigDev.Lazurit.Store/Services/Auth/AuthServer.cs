﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using KonigDev.Lazurit.Auth.InternalAPI.AccountApi.Interfaces;
using KonigDev.Lazurit.Store.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Newtonsoft.Json.Linq;
using KonigDev.Lazurit.Core.Services;
using Constants = KonigDev.Lazurit.Auth.Model.DTO.Constants;
using KonigDev.Lazurit.Store.Extensions.Filters;
using KonigDev.Lazurit.Store.Models.AuthModels;
using Newtonsoft.Json;
using KonigDev.Lazurit.Store.Providers;
using KonigDev.Lazurit.Store.Services.LazuritContext;
using KonigDev.Lazurit.Store.KonigDev.Lazurit.Store.Services.Auth;
using KonigDev.Lazurit.Store.Services.LayawayService;

namespace KonigDev.Lazurit.Store.Services.Auth
{
    //todo использовать RestSharp , он удобнее и тестируем
    //todo вынести в папку Services
    public class AuthServer : IAuthServer
    {
        private readonly string _authdomaniUrl;
        private readonly string _authTokenEndPoint;
        private readonly string _clientId;
        private readonly string _secretKey;
        private readonly ILoggingService _loggingService;
        private readonly IAccountInternalAPI _accountInternalApi;
        private readonly ICartProvider _cartProvider;
        private readonly ILazuritCtxSessionService _lazuritCtxSesion;
        private readonly ILayawayService _layawayService;

        public AuthServer(string authdomaniUrl, string authTokenEndPoint, string clientId, string secretKey, ILoggingService loggingService,
            IAccountInternalAPI accountInternalApi, ICartProvider cartProvider, ILazuritCtxSessionService lazuritCtxService, ILayawayService layawayService)
        {
            _authdomaniUrl = authdomaniUrl;
            _authTokenEndPoint = authTokenEndPoint;
            _clientId = clientId;
            _secretKey = secretKey;
            _loggingService = loggingService;
            _accountInternalApi = accountInternalApi;
            _cartProvider = cartProvider;
            _lazuritCtxSesion = lazuritCtxService;
            _layawayService = layawayService;
        }

        public async Task<LoginResult> Login(string userName, string password)
        {
            var httpFormContent = new FormUrlEncodedContent(new[]
               {
                    new KeyValuePair<string, string>("username", userName),
                    new KeyValuePair<string, string>("password", password),
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("client_id", _clientId)
                });
            return await GetRestLoginResult(httpFormContent);
        }

        public async Task<LoginResult> Login(Guid userId, string password)
        {
            var httpFormContent = new FormUrlEncodedContent(new[]
               {
                    new KeyValuePair<string, string>("userId", userId.ToString()),
                    new KeyValuePair<string, string>("password", password),
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("client_id", _clientId)
                });
            return await GetRestLoginResult(httpFormContent);
        }

        public async Task<LoginResult> LoginByUserPhone(string userPhone, string password)
        {
            var httpFormContent = new FormUrlEncodedContent(new[]
               {
                    new KeyValuePair<string, string>("userPhone", userPhone),
                    new KeyValuePair<string, string>("password", password),
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("client_id", _clientId)
                });
            return await GetRestLoginResult(httpFormContent);
        }

        private async Task<LoginResult> GetRestLoginResult(FormUrlEncodedContent httpFormContent)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_authdomaniUrl + _authTokenEndPoint);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await client.PostAsync(string.Empty, httpFormContent);
                var responseContent = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    string tokenResponce = await response.Content.ReadAsStringAsync();
                    var token = JObject.Parse(tokenResponce)["access_token"].ToString();

                    var principal = ValidateTokenAndSignin(token);
                    if (principal != null)
                    {
                        var context = HttpContext.Current.Request.GetOwinContext();
                        var authenticationManager = context.Authentication;

                        var identity = (ClaimsIdentity)principal.Identity;
                        var userId = identity.GetUserId();
                        authenticationManager.SignIn(identity);

                        await RelateCurrentCartWithUser(Guid.Parse(userId));
                        await RelateCurrentLayawayWithUser(Guid.Parse(userId));

                        if (identity.Claims.Any(x => x.Type == ClaimTypes.UserData))
                        {
                            return new LoginResult(SignInStatus.RequiresVerification,
                                new LoginInfo(identity.GetUserName(), identity.GetUserId(),
                                identity.Claims.Where(x => x.Type == ClaimTypes.Role).Select(x => x.Value)));
                        }
                        else
                        {
                            return new LoginResult(SignInStatus.Success,
                                new LoginInfo(identity.GetUserName(), identity.GetUserId(),
                                identity.Claims.Where(x => x.Type == ClaimTypes.Role).Select(x => x.Value)));
                        }

                    }
                    else return new LoginResult(SignInStatus.Failure, "Произошла ошибка при попытке авторизации.");
                }
                else
                {
                    /* вынести в сервис парсинга ответа от ауса */
                    try
                    {
                        var errorResponse = (AuthErrorResponseModel)JsonConvert.DeserializeObject(responseContent, typeof(AuthErrorResponseModel));
                        return new LoginResult(SignInStatus.Failure, errorResponse.ErrorDescriptions);
                    }
                    catch (Exception ex) { _loggingService.Error(ex); }
                    return new LoginResult(SignInStatus.Failure, "Произошла ошибка при попытке авторизации.");
                }
            }
        }

        public void LogOut()
        {
            var context = HttpContext.Current.Request.GetOwinContext();
            var authentification = context.Authentication;

            _cartProvider.ForAllCarts().ResetCart();
            _layawayService.ResetLayaway();
            authentification.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }


        public List<Claim> GetRolesForIdentity()
        {
            var context = HttpContext.Current.Request.GetOwinContext();
            var authenticationManager = context.Authentication;
            var claims = authenticationManager.User.Claims;
            return claims.ToList();
        }

        /// <summary>
        /// Проверяем токе на корректность ключу и если он валидный, то аторизовываем пользователя
        /// </summary>
        /// <param name="token">токен авторизации</param>
        /// <returns></returns>
        private ClaimsPrincipal ValidateTokenAndSignin(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var keyByteArray = TextEncodings.Base64Url.Decode(_secretKey);
            var key = new InMemorySymmetricSecurityKey(keyByteArray);
            var validationParameters = new TokenValidationParameters
            {
                IssuerSigningKey = key,
                ValidAudience = _clientId,
                ValidIssuer = _authdomaniUrl,
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                RequireExpirationTime = true,
                ValidateLifetime = true
            };
            try
            {
                SecurityToken securityToken;
                return tokenHandler.ValidateToken(token, validationParameters, out securityToken);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private async Task RelateCurrentCartWithUser(Guid userId)
        {
            try
            {
                var cartId = _lazuritCtxSesion.GetSingleCartId();
                var priceInfo = _lazuritCtxSesion.GetPriceInfoFromContext();
                await _cartProvider.ForAllCarts().RelateUserWithCart(userId, cartId, priceInfo.Currency, priceInfo.PriceZoneId, priceInfo.DeliveryPriceForCity);
            }
            catch (Exception ex)
            {
                _loggingService.Error($"Ошибка связывания корзины с пользователем с ID = {userId}");
                _loggingService.Error(ex);
            }
        }

        private async Task RelateCurrentLayawayWithUser(Guid userId)
        {
            try
            {
                var layawayIds = _lazuritCtxSesion.GetLayawaytIds();
                await _layawayService.MergeLayaways(userId, layawayIds);
            }
            catch (Exception ex)
            {
                _loggingService.Error($"Ошибка связывания отложенных с пользователем с ID = {userId}");
                _loggingService.Error(ex);
            }
        }

        public List<string> GetCurrentUserRoles()
        {
            var result = new List<string>();
            var userRoles = GetRolesForIdentity();

            if (userRoles.Any(x => x.IsRegion()))
                result.Add(Constants.RegionRoleName);
            if (userRoles.Any(x => x.IsAdmin()))
                result.Add(Constants.AdminRoleName);
            if (userRoles.Any(x => x.IsBuyer()))
                result.Add(Constants.BuyerRoleName);
            if (userRoles.Any(x => x.IsContentManager()))
                result.Add(Constants.ContentManagerRoleName);
            if (userRoles.Any(x => x.IsMarketer()))
                result.Add(Constants.MarketerRoleName);
            if (userRoles.Any(x => x.IsSaller()))
                result.Add(Constants.SallerRoleName);
            if (userRoles.Any(x => x.IsSalonSaller()))
                result.Add(Constants.SalonSallerRoleName);
            if (userRoles.Any(x => x.IsShopSaller()))
                result.Add(Constants.ShopSallerRoleName);
            return result;
        }
    }
}