﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using KonigDev.Lazurit.Store.Models;

namespace KonigDev.Lazurit.Store.KonigDev.Lazurit.Store.Services.Auth
{
    //todo вынести в папку Services
    public interface IAuthServer
    {
        Task<LoginResult> Login(string userName, string password);
        Task<LoginResult> Login(Guid userId, string password);
        Task<LoginResult> LoginByUserPhone(string userPhone, string password);
        void LogOut();
        List<Claim> GetRolesForIdentity();
        List<string> GetCurrentUserRoles();
    }
}

