﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using KonigDev.Lazurit.Store.Dto.DtoHub;
using KonigDev.Lazurit.Store.Dto.Common;
using KonigDev.Lazurit.Store.Models.Constants;

namespace KonigDev.Lazurit.Store.Hubs
{
    /// <summary>
    /// Хаб для работы с корзиной
    /// </summary>
    /// TODO переименовать в CartHub, только аккуратно, надо смотерть что бы фронт не сломался
    public class CommonHub : Microsoft.AspNet.SignalR.Hub
    {
        private static List<HubIdentity> _hubIdentities = new List<HubIdentity>();
        private static string _connectionId;

        public IEnumerable<HubIdentity> GetHubIdentities()
        {
            return _hubIdentities;
        }

        public void Connect()
        {
            if (Context == null) return;

            var id = Context.ConnectionId;
            _connectionId = id;
            if (_hubIdentities.All(x => x.ConnectionId != id) && Context.Request.Cookies.ContainsKey(Common.CartCookieId) && !string.IsNullOrWhiteSpace(Context.Request.Cookies[Common.CartCookieId].Value))
            {
                var cartcookieId = Context.Request.Cookies[Common.CartCookieId].Value;
                _hubIdentities.Add(new HubIdentity { CartId = cartcookieId, ConnectionId = id });
                Clients.Caller.onConnected(id, cartcookieId);
            }
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var id = Context.ConnectionId;
            var item = _hubIdentities.FirstOrDefault(x => x.ConnectionId == id);
            if (item != null)
            {
                _hubIdentities.Remove(item);
                Clients.All.onUserDisconnected(id, item.CartId);
            }

            return base.OnDisconnected(stopCalled);
        }

        public string GetCurrentConnectionId()
        {
            return _connectionId;
        }

        public DtoResponse ChangeHubIdentity(string oldCartCookieId, string newCartCookieId)
        {
            if (string.IsNullOrEmpty(_connectionId))
                return new DtoResponse { IsValid = false, Message = "ConnectionId is Empty" };

            var hubIdentities = _hubIdentities.Where(x => x.CartId == oldCartCookieId);
            foreach (var identity in hubIdentities)
            {
                identity.CartId = newCartCookieId;
            }

            return new DtoResponse { IsValid = true };
        }
    }
}