﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Hubs
{
    public class HubIdentity
    {
        public string ConnectionId { get; set; }
        public string CartId { get; set; }
    }
}