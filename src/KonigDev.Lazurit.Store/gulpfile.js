/// <binding />
'use strict';

var gulp = require('gulp');
var notify = require('gulp-notify');

var requireDir = require("require-dir");
requireDir("./gulp-tasks");

gulp.task('build', [
    'frontend-tslint',
    'frontend-vendor',
    'frontend-browserify',
    'frontend-lib',
    'frontend-font',
    'frontend-less',
    'frontend-icons'
]);

gulp.task('default', ['build'], function () {
    return gulp.src('.').pipe(notify('Build finished!'));
});

gulp.task('build-dev', [
    'frontend-tslint',
    'frontend-vendor',
    'frontend-browserify',
    'frontend-lib',
    'frontend-font',
    'frontend-less',
    'frontend-icons'
]);

/* todo add env? minify js */
gulp.task('build-prod', [
    'frontend-tslint',
    'frontend-vendor',
    'frontend-browserify',
    'frontend-lib',
    'frontend-font',
    'frontend-less',
    'frontend-icons'
]);

gulp.task('watch', ['build'], function () {
    gulp.watch("./frontend/**/*.ts", ["frontend-tslint", "frontend-browserify"]);
    gulp.watch("./frontend/**/*.html", ["frontend-browserify"]);
});