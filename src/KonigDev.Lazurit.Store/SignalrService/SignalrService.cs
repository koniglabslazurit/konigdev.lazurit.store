﻿using KonigDev.Lazurit.Store.Dto.Cart.CartMini;
using KonigDev.Lazurit.Store.Hubs;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KonigDev.Lazurit.Store.SignalrService
{
    public class SignalrService : ISignalrService
    {
        private IHubContext _hubContext;
        private CommonHub _commonHub;
        public SignalrService(CommonHub commonHub)
        {
            _commonHub = commonHub;
            _hubContext = GlobalHost.ConnectionManager.GetHubContext<CommonHub>();
        }

        public void UpdateCart(Guid cartId)
        {
            var currentIdentities = GetCurrentIdentitiesForCart(cartId.ToString());
            foreach (var identities in currentIdentities)
            {
                _hubContext.Clients.Client(identities.ConnectionId).refreshCart();
            }
        }

        public void UpdateCart(DtoCartMiniPriceResult newCart)
        {
            var currentIdentities = GetCurrentIdentitiesForCart(newCart.Id.ToString());
            foreach (var identities in currentIdentities)
            {
                _hubContext.Clients.Client(identities.ConnectionId).updateMiniCartByData(newCart);
            }
        }

        public void ChangeHubIdentity(string oldCartId, string newCartId)
        {
            _commonHub.ChangeHubIdentity(oldCartId, newCartId);
        }

        private List<HubIdentity> GetCurrentIdentitiesForCart(string cartId)
        {
            List<HubIdentity> currentIdentities = new List<HubIdentity>();
            var hubIdentities = _commonHub.GetHubIdentities();
            return hubIdentities.Where(x => x.CartId == cartId).ToList();
        }
    }
}