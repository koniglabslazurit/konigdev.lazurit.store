﻿using KonigDev.Lazurit.Store.Dto.Cart.CartMini;
using System;

namespace KonigDev.Lazurit.Store.SignalrService
{
    public interface ISignalrService
    {
        void UpdateCart(Guid cartId);
        void ChangeHubIdentity(string oldCartCookieId, string cartCoockieId);
        void UpdateCart(DtoCartMiniPriceResult newCart);
    }
}