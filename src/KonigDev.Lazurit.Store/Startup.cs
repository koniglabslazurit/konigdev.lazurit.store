﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Http;
using KonigDev.Lazurit.Store.Extensions;
using KonigDev.Lazurit.Store.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.Owin.Security.DataProtection;
using System.Web;
using KonigDev.Lazurit.Store.Extensions.AppBuilderExtensions;

[assembly: OwinStartup(typeof(KonigDev.Lazurit.Store.Startup))]

namespace KonigDev.Lazurit.Store
{
    public partial class Startup
    {
        public static IDataProtectionProvider DataProtectionProvider { get; private set; }
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            DataProtectionProvider = app.GetDataProtectionProvider();

            config.MapHttpAttributeRoutes();

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseOAuthTokenForConsumer();
            var cookieOption = new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Test"),
                Provider = new CookieAuthenticationProvider
                {
                    //OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<UserManager<IdentityUser>, IdentityUser>(
                    //   validateInterval: TimeSpan.FromSeconds(121),
                    //   regenerateIdentity: (manager, user) =>
                    //   {
                    //       //var res=user.GenerateUserIdentityAsync(manager);
                    //       return Task.FromResult(new ClaimsIdentity());
                    //   })
                }
            };
            app.UseCookieAuthentication(cookieOption);
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));
            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);
            app.MapSignalR();
            app.UseCammelCaseSignalRJsonResolver();
        }
    }
}
