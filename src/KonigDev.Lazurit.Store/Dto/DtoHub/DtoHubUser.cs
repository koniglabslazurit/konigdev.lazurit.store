﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.DtoHub
{
    public class DtoHubUser
    {
        public string UserId { get; set; }
        public string ConnectionId { get; set; }
    }
}