﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Account
{
    public class DtoSellerVerificationResult
    {
        public bool IsAccepted { get; set; }
        public string Message { get; set; }

        public bool IsVerified { get; set; }
    }
}