﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Account
{
    public class DtoRecoveryPasswordResult
    {
        public bool IsAccepted { get; set; }
        public string Message { get; set; }
    }
}