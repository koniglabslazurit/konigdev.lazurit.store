﻿namespace KonigDev.Lazurit.Store.Dto.Account
{
    public class DtoUserAuthResult
    {
        public bool IsAuthentificate { get; set; }
        public string ReturnUrl { get; set; }
        public bool IsRegion { get; set; }
        public bool RequiresVerification { get; set; }
        public string Message { get; set; }
    }
}