﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Account
{
    public class DtoRegisterResult
    {
        public bool IsAccepted { get; set; }
        public string Message { get; set; }
    }
}