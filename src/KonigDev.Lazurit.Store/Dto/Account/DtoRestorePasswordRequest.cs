﻿using System.ComponentModel.DataAnnotations;
using KonigDev.Lazurit.Store.Models.Constants.Account;

namespace KonigDev.Lazurit.Store.Dto.Account
{
    public class DtoRestorePasswordRequest
    {
        [Required(ErrorMessage = Exception_Validations.RequiredField)]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = Exception_Validations.RequiredField)]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = Exception_Validations.RequiredField)]
        [DataType(DataType.Password)]
        [Compare("NewPassword",ErrorMessage = Exception_Validations.PasswordDoesnotCompare)]
        public string ConfirmPassword { get; set; }
    }
}