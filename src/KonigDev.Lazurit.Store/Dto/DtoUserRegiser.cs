﻿using System.ComponentModel.DataAnnotations;

namespace KonigDev.Lazurit.Store.Dto
{
    public class DtoUserRegiser
    {
        [Required]
        public string Name { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}