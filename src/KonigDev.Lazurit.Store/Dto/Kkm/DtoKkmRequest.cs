﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Kkm
{
    [Serializable]
    public class DtoKkmRequest
    {
        public string OutSum { get; set; }
        public string InvId { get; set; }
        public string SignatureValue { get; set; }
    }
}