﻿using System;

namespace KonigDev.Lazurit.Store.Dto.Kkm
{
    [Serializable]
    public class DtoKkmResponse
    {
        public string KkmOrderRecordUrl;
    }
}