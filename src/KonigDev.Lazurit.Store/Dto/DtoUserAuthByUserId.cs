﻿using System;

namespace KonigDev.Lazurit.Store.Dto
{
    public class DtoUserAuthByUserId
    {
        public string Password { get; set; }
        public string ReturnUrl { get; set; }
        public Guid UserID { get; set; }
    }
}