﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto
{

    public class DtoUserRestorePassword
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}