﻿using KonigDev.Lazurit.Hub.Model.DTO.Collections;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Facings;
using KonigDev.Lazurit.Hub.Model.DTO.Facings.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Styles.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Dto;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.Dto.Collection
{
    public class DtoRoomFiltersResult
    {
        public string Room { get; set; }
        public IEnumerable<DtoCollectionResultForCatalog> Collections { get; set; }
        public IEnumerable<DtoFacingItem> Facings { get; set; }
        public IEnumerable<DtoFacingColorItem> FacingColors { get; set; }
        public IEnumerable<DtoFrameColorItem> FrameColors { get; set; }
        public IEnumerable<DtoStyleItem> Styles { get; set; }
        public IEnumerable<DtoTargetAudienceItem> TargetAudiences { get; set; }
    }
}