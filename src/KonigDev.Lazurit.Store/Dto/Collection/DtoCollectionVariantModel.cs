﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Collection
{
    public class DtoCollectionVariantModel
    {
        public Guid CollectionVariantId { get; set; }
    }
}