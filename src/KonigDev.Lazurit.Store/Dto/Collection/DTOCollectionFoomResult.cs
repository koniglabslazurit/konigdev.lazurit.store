﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Collection
{
    public class DTOCollectionFoomResult
    {
        public string RommAlias { get; set; }
        [Obsolete("Must be rename to series alias")]
        public string CollectionAlias { get; set; }
    }
}