﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Import
{
    public class DtoImportProducts
    {
        public Guid Id { get; set; }
        public Guid? OriginalId { get; set; }
        public string Series { get; set; }
        public string Room { get; set; }
        public int ValidationProducts { get; set; }
        public int TemplateProducts { get; set; }
        public int PublishedProducts { get; set; }
    }
}