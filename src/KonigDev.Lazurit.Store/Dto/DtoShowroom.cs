﻿using System;

namespace KonigDev.Lazurit.Store.Dto
{
    public class DtoShowroom
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
    }
}