﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.UserRole
{
    public class UpdateUserRoleQuery
    {
        public Guid UserId{get;set;}
        public string Role { get; set; }
    }
}