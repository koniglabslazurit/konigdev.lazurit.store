﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.Dto.Layaway
{
    public class DtoLayawayWithPrices
    {
        public Guid Id { get; set; }
        public Guid UserId { set; get; }
        public bool IsWholeCart { set; get; }
        public DateTime CreationTime { get; set; }
        public List<DtoLayawayItemProductWithPrices> Products { get; set; }
        public List<DtoCollectionItemProductWithPrices> Collections { get; set; }
        public List<DtoComplectItemProductWithPrices> Complects { get; set; }
    }
}