﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Layaway
{
    public class DtoLayawayItemProductInCollectionWithPrices
    {
        public Guid Id { set; get; }
        public DtoProductLayawayWithPrices Product { get; set; }
        public Guid ProductId { set; get; }
        public Guid LayawayItemCollectionId { set; get; }
        public bool IsAssemblyRequired { get; set; }
        public int Quantity { set; get; }
    }
}