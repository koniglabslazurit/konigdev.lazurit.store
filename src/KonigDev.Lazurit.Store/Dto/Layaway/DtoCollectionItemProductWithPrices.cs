﻿using KonigDev.Lazurit.Store.Models.Prices.KonigDev.Lazurit.Store.Models.Prices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Layaway
{
    public class DtoCollectionItemProductWithPrices
    {
        public Guid Id { set; get; }

        public Guid CollectionVarinatId { set; get; }
        public Guid LayawayId { set; get; }

        public string CollectionName { get; set; }
        public string RoomTypeName { get; set; }

        public int Quantity { set; get; }

        public ICollection<DtoLayawayItemProductInCollectionWithPrices> ProductsInCollection { set; get; }

        public Money CollectionSalePrice { get; set; }
    }
}