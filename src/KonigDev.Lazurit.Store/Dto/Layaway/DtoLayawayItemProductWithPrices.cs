﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Layaway
{
    public class DtoLayawayItemProductWithPrices
    {
        public Guid Id { set; get; }
        public DtoProductLayawayWithPrices Product { get; set; }
        public Guid CollectionId { get; set; }

        public Guid ProductId { set; get; }
        public Guid LayawayId { set; get; }

        public bool IsAssemblyRequired { get; set; }
        public int Quantity { set; get; }
    }
}