﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Web;
using KonigDev.Lazurit.Hub.Model.DTO.Article;
using KonigDev.Lazurit.Hub.Model.DTO.Collections;
using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Dto;

namespace KonigDev.Lazurit.Store.Dto.Article
{
    public class DtoArticleWithFilesContent
    {
        public List<DtoProductFilesContent> ProductsfilesContent { get; set; }
        public DtoArticleByUniqueFieldsQueryResult Article { get; set; }
        public DtoCollectionByAliasResult Collection { get; set; }
    }

    public class DtoProductFilesContent
    {
        public Guid ProductId { get; set; }
        public List<DtoFilesContent> ProductFilesContent { get; set; }
    }
}