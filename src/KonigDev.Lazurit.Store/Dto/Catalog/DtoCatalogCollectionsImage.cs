﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Catalog
{
    public class DtoRoomType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<DtoCollection> Collections { get; set; }
    }
}