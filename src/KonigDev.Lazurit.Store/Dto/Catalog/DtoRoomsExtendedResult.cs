﻿using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes;
using KonigDev.Lazurit.Hub.Model.DTO.Styles.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Dto;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.Dto.Catalog
{
    [Serializable]
    public class DtoRoomsExtendedResult
    {
        public IEnumerable<DtoRoomTypesResult> Rooms { get; set; }
        public IEnumerable<DtoStyleItem> Styles { get; set; }
        public IEnumerable<DtoTargetAudienceItem> TargetAudiences { get; set; }
    }
}