﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Catalog
{
    public class DTORomByAliasResult
    {
        public string Alias { get; set; }
    }
}