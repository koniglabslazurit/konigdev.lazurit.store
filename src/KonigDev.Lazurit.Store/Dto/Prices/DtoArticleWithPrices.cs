﻿using KonigDev.Lazurit.Hub.Model.DTO.Article;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors;
using KonigDev.Lazurit.Hub.Model.DTO.Facings;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Prices
{
    public class DtoArticleWithPrices
    {
        public Guid ArticleId { get; set; }
        public string ArticleSyncCode1C { get; set; }
        public List<DtoFacingsResult> Facings { get; set; }
        public List<DtoFacingColorResult> FacingsColors { get; set; }
        public List<DtoFrameColorResult> FramesColors { get; set; }
        public string FurnitureTypeName { get; set; }
        public List<int> Heights { get; set; }
        public List<int> Lengths { get; set; }
        public List<DtoProductInCollectionVariantPrice> Products { get; set; }
        public List<int> Widths { get; set; }
    }
}