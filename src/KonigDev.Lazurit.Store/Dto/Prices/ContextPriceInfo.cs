﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KonigDev.Lazurit.Store.Models.Prices;

namespace KonigDev.Lazurit.Store.Dto.Prices
{
    public class ContextPriceInfo
    {
        public decimal DeliveryPriceForCity { get; set; }
        public Guid PriceZoneId { get; set; }
        public Currency Currency { get; set; }
    }
}