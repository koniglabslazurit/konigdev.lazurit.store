﻿using System;

namespace KonigDev.Lazurit.Store.Dto.Prices.Interfaces
{
    public interface IProductPrice : IPriceBase
    {
        /// <summary>
        /// Идентификатор продукта
        /// </summary>
        Guid ItemId { get; set; }

        bool IsAssemblyRequired { get; set; }
    }
}