﻿using System;

namespace KonigDev.Lazurit.Store.Dto.Prices.Interfaces
{
    public interface IComplectPrice : IPriceBase
    {
        //Guid Id { get; set; }
        bool IsAssemblyRequired { get; set; }
        Guid ItemId { get; set; }
    }
}