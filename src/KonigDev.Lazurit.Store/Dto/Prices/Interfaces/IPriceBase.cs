﻿using KonigDev.Lazurit.Store.Models.Prices.KonigDev.Lazurit.Store.Models.Prices;

namespace KonigDev.Lazurit.Store.Dto.Prices.Interfaces
{
    public interface IPriceBase
    {
        /// <summary>
        /// Цена продукта
        /// </summary>
        Money Price { get; set; }

        /// <summary>
        /// Цена за сборку
        /// </summary>
        Money PriceForAssembly { get; set; }

        /// <summary>
        /// Итоговая цена одного продукта для продажи (учитывая скидки и т.д. Учитывает сборку, если она необходима)
        /// </summary>
        Money SalePrice { get; set; }

        /// <summary>
        /// Итоговая цена со сборкой
        /// </summary>
        Money SalePriceWithAssembly { get; set; }

        /// <summary>
        /// Итоговая цена не учитывая сбоку
        /// </summary>
        Money SalePriceWithOutAssembly { get; set; }

        #region Скидки
        /// <summary>
        /// Скидка в процентах
        /// </summary>
        decimal Discount { get; set; }

        /// <summary>
        /// Скидка рассрочки на 6 мес
        /// </summary>
        int DiscountInstallment6Month { get; set; }

        /// <summary>
        /// Скидка рассрочки на 10 мес
        /// </summary>
        int DiscountInstallment10Month { get; set; }

        /// <summary>
        /// Скидка рассрочки на 12 мес
        /// </summary>
        int DiscountInstallment12Month { get; set; }

        /// <summary>
        /// Скидка рассрочки на 18 мес
        /// </summary>
        int DiscountInstallment18Month { get; set; }

        /// <summary>
        /// Скидка рассрочки на 24 мес
        /// </summary>
        int DiscountInstallment24Month { get; set; }

        /// <summary>
        /// Скидка рассрочки на 36 мес
        /// </summary>
        int DiscountInstallment36Month { get; set; }

        /// <summary>
        /// Скидка рассрочки на 48 мес
        /// </summary>
        int DiscountInstallment48Month { get; set; }

        #endregion

        #region стоимость с рассрочками

        /// <summary>
        /// Рассрочка на 6 месяцев
        /// </summary>
        Money SalePriceInstallment6Month { get; set; }

        /// <summary>
        /// Рассрочка на 10 месяцев
        /// </summary>
        Money SalePriceInstallment10Month { get; set; }

        /// <summary>
        /// Рассрочка на 6 месяцев
        /// </summary>
        Money SalePriceInstallment12Month { get; set; }

        /// <summary>
        /// Рассрочка на 18 месяцев
        /// </summary>
        Money SalePriceInstallment18Month { get; set; }

        /// <summary>
        /// Рассрочка на 24 месяцев
        /// </summary>
        Money SalePriceInstallment24Month { get; set; }

        /// <summary>
        /// Рассрочка на 36 месяцев
        /// </summary>м
        Money SalePriceInstallment36Month { get; set; }

        /// <summary>
        /// Рассрочка на 48 месяцев
        /// </summary>
        Money SalePriceInstallment48Month { get; set; }

        #endregion
        
        #region Стоимость для рассрочек со сборкой
        /// <summary>
        /// Рассрочка на 6 месяцев со сборкой
        /// </summary>
        Money SalePriceInstallment6MonthWithAssembly { get; set; }

        /// <summary>
        /// Рассрочка на 10 месяцев со сборкой
        /// </summary>
        Money SalePriceInstallment10MonthWithAssembly { get; set; }

        /// <summary>
        /// Рассрочка на 6 месяцев со сборкой
        /// </summary>
        Money SalePriceInstallment12MonthWithAssembly { get; set; }

        /// <summary>
        /// Рассрочка на 18 месяцев со сборкой
        /// </summary>
        Money SalePriceInstallment18MonthWithAssembly { get; set; }

        /// <summary>
        /// Рассрочка на 24 месяцев со сборкой
        /// </summary>
        Money SalePriceInstallment24MonthWithAssembly { get; set; }

        /// <summary>
        /// Рассрочка на 36 месяцев со сборкой
        /// </summary>м
        Money SalePriceInstallment36MonthWithAssembly { get; set; }

        /// <summary>
        /// Рассрочка на 48 месяцев со сборкой
        /// </summary>
        Money SalePriceInstallment48MonthWithAssembly { get; set; }

        #endregion
    }
}