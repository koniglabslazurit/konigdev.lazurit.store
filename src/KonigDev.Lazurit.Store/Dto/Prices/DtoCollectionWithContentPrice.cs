﻿using KonigDev.Lazurit.Hub.Model.DTO.FacingColors;
using KonigDev.Lazurit.Hub.Model.DTO.Facings;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;
using KonigDev.Lazurit.Store.Dto.Prices.Interfaces;
using KonigDev.Lazurit.Store.Models.Prices.KonigDev.Lazurit.Store.Models.Prices;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.Dto.Prices
{
    /* Это все создано для того чтобы получить цены для продуктов, для отображения их в анимированном слайдере. 
     * Нельзя просто так взять дто прилетающую из грейна и запилить ей цены */
    public class DtoCollectionWithContentPrice
    {
        public Guid Id { get; set; }
        public string RoomType { get; set; }
        public string SeriesName { get; set; }
        public IEnumerable<DtoFacingsResult> Facings { get; set; }
        public IEnumerable<DtoFacingColorResult> FacingColors { get; set; }
        public IEnumerable<DtoFrameColorResult> FrameColors { get; set; }
        public List<DtoCollectionVariantContentPrice> Variants { get; set; }
    }

    public class DtoCollectionVariantContentPrice
    {
        public Guid Id { get; set; }
        public Guid CollectionId { get; set; }
        public IEnumerable<DtoFacingsResult> Facings { get; set; }
        public IEnumerable<DtoFacingColorResult> FacingColors { get; set; }
        public IEnumerable<DtoFrameColorResult> FrameColors { get; set; }
        public string CollectionTypeName { get; set; }
        public bool IsComplect { get; set; }
        public bool IsFavorite { get; set; }
        public List<DtoProductContentPrice> Products { get; set; }
    }

    public class DtoProductContentPrice : IProductPrice
    {
        public Guid Id { get; set; }
        public string SyncCode1C { get; set; }
        public string ArticleName { get; set; }
        public string ArticleSyncCode1c { get; set; }
        public string FurnitureTypeAlias { get; set; }
        public string FurnitureTypeName { get; set; }
        public Guid? ObjectId { get; set; }
        public bool IsEnabled { get; set; }
        public int ContentLeft { get; set; }
        public int ContentTop { get; set; }
        public int ContentWidth { get; set; }
        public int ContentHeight { get; set; }
        public bool IsFavorite { get; set; }
        public Guid ProductId { get; set; }
        public decimal Discount { get; set; }
        /// <summary>
        /// Скидка в процентах
        /// </summary>
        public int DiscountInstallment6Month { get; set; }

        /// <summary>
        /// Скидка рассрочки на 10 мес
        /// </summary>
        public int DiscountInstallment10Month { get; set; }

        /// <summary>
        /// Скидка рассрочки на 12 мес
        /// </summary>
        public int DiscountInstallment12Month { get; set; }

        /// <summary>
        /// Скидка рассрочки на 18 мес
        /// </summary>
        public int DiscountInstallment18Month { get; set; }

        /// <summary>
        /// Скидка рассрочки на 24 мес
        /// </summary>
        public int DiscountInstallment24Month { get; set; }

        /// <summary>
        /// Скидка рассрочки на 36 мес
        /// </summary>
        public int DiscountInstallment36Month { get; set; }

        /// <summary>
        /// Скидка рассрочки на 48 мес
        /// </summary>
        public int DiscountInstallment48Month { get; set; }

        public bool IsAssemblyRequired { get; set; }
        public Guid ItemId { get; set; }
        public Money Price { get; set; }
        public Money PriceForAssembly { get; set; }
        public Money SalePrice { get; set; }
        public Money SalePriceWithAssembly { get; set; }
        public Money SalePriceWithOutAssembly { get; set; }

        /// <summary>
        /// Рассрочка на 6 месяцев
        /// </summary>
        public Money SalePriceInstallment6Month { get; set; }

        /// <summary>
        /// Рассрочка на 10 месяцев
        /// </summary>
        public Money SalePriceInstallment10Month { get; set; }

        /// <summary>
        /// Рассрочка на 6 месяцев
        /// </summary>
        public Money SalePriceInstallment12Month { get; set; }

        /// <summary>
        /// Рассрочка на 18 месяцев
        /// </summary>
        public Money SalePriceInstallment18Month { get; set; }

        /// <summary>
        /// Рассрочка на 24 месяцев
        /// </summary>
        public Money SalePriceInstallment24Month { get; set; }

        /// <summary>
        /// Рассрочка на 36 месяцев
        /// </summary>м
        public Money SalePriceInstallment36Month { get; set; }

        /// <summary>
        /// Рассрочка на 48 месяцев
        /// </summary>
        public Money SalePriceInstallment48Month { get; set; }

        /// <summary>
        /// Рассрочка на 6 месяцев со сборкой
        /// </summary>
        public Money SalePriceInstallment6MonthWithAssembly { get; set; }

        /// <summary>
        /// Рассрочка на 10 месяцев со сборкой
        /// </summary>
        public Money SalePriceInstallment10MonthWithAssembly { get; set; }

        /// <summary>
        /// Рассрочка на 6 месяцев со сборкой
        /// </summary>
        public Money SalePriceInstallment12MonthWithAssembly { get; set; }

        /// <summary>
        /// Рассрочка на 18 месяцев со сборкой
        /// </summary>
        public Money SalePriceInstallment18MonthWithAssembly { get; set; }

        /// <summary>
        /// Рассрочка на 24 месяцев со сборкой
        /// </summary>
        public Money SalePriceInstallment24MonthWithAssembly { get; set; }

        /// <summary>
        /// Рассрочка на 36 месяцев со сборкой
        /// </summary>м
        public Money SalePriceInstallment36MonthWithAssembly { get; set; }

        /// <summary>
        /// Рассрочка на 48 месяцев со сборкой
        /// </summary>
        public Money SalePriceInstallment48MonthWithAssembly { get; set; }
    }
}