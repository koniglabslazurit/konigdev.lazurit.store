﻿using KonigDev.Lazurit.Store.Dto.Prices.Interfaces;
using KonigDev.Lazurit.Store.Models.Prices.KonigDev.Lazurit.Store.Models.Prices;
using System;

namespace KonigDev.Lazurit.Store.Dto.Prices
{
    public class DtoProductPrice : IProductPrice
    {
        public Guid Id { get; set; }
        public string SyncCode1C { get; set; }
        public string SeriesName { get; set; }
        public string SeriesAlias { get; set; }
        public string ArticleName { get; set; }
        public string ArticleSyncCode1c { get; set; }
        public string FurnitureTypeAlias { get; set; }
        public string FurnitureTypeName { get; set; }
        public Guid FurnitureTypeId { get; set; }
        public bool IsFavorite { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public decimal Discount { get; set; }

        /// <summary>
        /// Скидка в процентах
        /// </summary>
        public int DiscountInstallment6Month { get; set; }

        /// <summary>
        /// Скидка рассрочки на 10 мес
        /// </summary>
        public int DiscountInstallment10Month { get; set; }

        /// <summary>
        /// Скидка рассрочки на 12 мес
        /// </summary>
        public int DiscountInstallment12Month { get; set; }

        /// <summary>
        /// Скидка рассрочки на 18 мес
        /// </summary>
        public int DiscountInstallment18Month { get; set; }

        /// <summary>
        /// Скидка рассрочки на 24 мес
        /// </summary>
        public int DiscountInstallment24Month { get; set; }

        /// <summary>
        /// Скидка рассрочки на 36 мес
        /// </summary>
        public int DiscountInstallment36Month { get; set; }

        /// <summary>
        /// Скидка рассрочки на 48 мес
        /// </summary>
        public int DiscountInstallment48Month { get; set; }

        public bool IsAssemblyRequired { get; set; }
        public Guid ItemId { get; set; }
        public Money Price { get; set; }
        public Money PriceForAssembly { get; set; }
        public Money SalePrice { get; set; }
        public Money SalePriceWithAssembly { get; set; }
        public Money SalePriceWithOutAssembly { get; set; }

        /// <summary>
        /// Рассрочка на 6 месяцев
        /// </summary>
        public Money SalePriceInstallment6Month { get; set; }

        /// <summary>
        /// Рассрочка на 10 месяцев
        /// </summary>
        public Money SalePriceInstallment10Month { get; set; }

        /// <summary>
        /// Рассрочка на 6 месяцев
        /// </summary>
        public Money SalePriceInstallment12Month { get; set; }

        /// <summary>
        /// Рассрочка на 18 месяцев
        /// </summary>
        public Money SalePriceInstallment18Month { get; set; }

        /// <summary>
        /// Рассрочка на 24 месяцев
        /// </summary>
        public Money SalePriceInstallment24Month { get; set; }

        /// <summary>
        /// Рассрочка на 36 месяцев
        /// </summary>м
        public Money SalePriceInstallment36Month { get; set; }

        /// <summary>
        /// Рассрочка на 48 месяцев
        /// </summary>
        public Money SalePriceInstallment48Month { get; set; }

        /// <summary>
        /// Рассрочка на 6 месяцев со сборкой
        /// </summary>
        public Money SalePriceInstallment6MonthWithAssembly { get; set; }

        /// <summary>
        /// Рассрочка на 10 месяцев со сборкой
        /// </summary>
        public Money SalePriceInstallment10MonthWithAssembly { get; set; }

        /// <summary>
        /// Рассрочка на 6 месяцев со сборкой
        /// </summary>
        public Money SalePriceInstallment12MonthWithAssembly { get; set; }

        /// <summary>
        /// Рассрочка на 18 месяцев со сборкой
        /// </summary>
        public Money SalePriceInstallment18MonthWithAssembly { get; set; }

        /// <summary>
        /// Рассрочка на 24 месяцев со сборкой
        /// </summary>
        public Money SalePriceInstallment24MonthWithAssembly { get; set; }

        /// <summary>
        /// Рассрочка на 36 месяцев со сборкой
        /// </summary>м
        public Money SalePriceInstallment36MonthWithAssembly { get; set; }

        /// <summary>
        /// Рассрочка на 48 месяцев со сборкой
        /// </summary>
        public Money SalePriceInstallment48MonthWithAssembly { get; set; }

        /// <summary>
        /// Маркетинговый вес
        /// </summary>
        public string MarketerRange { get; set; }
    }
}