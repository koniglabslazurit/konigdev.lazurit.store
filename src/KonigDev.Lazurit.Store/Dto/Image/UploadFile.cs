﻿using System;

namespace KonigDev.Lazurit.Store.Dto.Image
{
    public class UploadFile
    {
        public Guid ObjectId { get; set; }
        public string Type { get; set; }
    }
}