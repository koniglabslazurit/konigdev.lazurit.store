﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Geolocation
{
    public class DtoCoordinates
    {
        public string Longitude { get; set; }
        public string Latitude { get; set; }
    }
}