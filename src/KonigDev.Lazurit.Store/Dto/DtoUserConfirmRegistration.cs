﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto
{
    public class DtoUserConfirmRegistration
    {
        public string UserId { get; set; }
        public string Token { get; set; }
        public string Address { get; set; }        
        public string Login { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string Phone { get; set; }
        public bool IsAgreed { get; set; }
        public string PhoneNumber { get; set; }
    }
}