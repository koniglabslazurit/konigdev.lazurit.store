﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Product
{
    public class DtoProductContentAdding
    {
        public Guid ProductId { get; set; }
        public Guid CollectionVariantId { get; set; }
    }
}