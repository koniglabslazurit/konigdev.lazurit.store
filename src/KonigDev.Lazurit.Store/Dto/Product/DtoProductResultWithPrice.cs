﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KonigDev.Lazurit.Hub.Model.DTO.Products;

namespace KonigDev.Lazurit.Store.Dto.Product
{
    public class DtoProductResultWithPrice
    {
        public DtoProductResult DtoProductResult { get; set; }
        public string Price { get; set; }
        public string Discount { get; set; }
        public string SalePrice { get; set; }
        public string PriceForAssembly { get; set; }

    }
}