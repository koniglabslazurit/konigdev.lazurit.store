﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Product
{
    public class DtoProductsByFurnitureAlias
    {
        public string FurnitureAlias { get; set; }
        public string ParentFurnitureAlias { get; set; }
    }
}