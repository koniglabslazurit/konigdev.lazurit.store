﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Cart
{
    public class DtoAddComplectToCartRequest
    {
        public Guid CollectionId { set; get; }
    }
}