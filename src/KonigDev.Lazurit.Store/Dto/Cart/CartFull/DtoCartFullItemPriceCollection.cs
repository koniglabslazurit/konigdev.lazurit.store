using System;
using System.Collections.Generic;
using KonigDev.Lazurit.Store.Models.Prices.KonigDev.Lazurit.Store.Models.Prices;

namespace KonigDev.Lazurit.Store.Dto.Cart.CartFull
{
    public class DtoCartFullItemPriceCollection
    {
        public Guid Id { set; get; }

        public Guid CartId { set; get; }

        public int Quantity { set; get; }

        public Guid CollectionVariantId { set; get; }
        public List<DtoCartFullItemPriceProductInCollection> Products { get; set; }
        public Money CollectionSalePrice { get; set; }

    }
}