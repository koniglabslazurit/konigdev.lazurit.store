using System;
using System.Collections.Generic;
using KonigDev.Lazurit.Store.Dto.Prices.Interfaces;
using KonigDev.Lazurit.Store.Models.Prices.KonigDev.Lazurit.Store.Models.Prices;

namespace KonigDev.Lazurit.Store.Dto.Cart.CartFull
{
    public class DtoCartFullPriceResult : IDtoCart 
    {
        public Guid Id { get; set; }

        public List<DtoCartFullItemPriceComplect> Complects { get; set; }

        public List<DtoCartFullItemPriceProduct> Products { get; set; }

        public List<DtoCartFullItemPriceCollection> Collections { get; set; }

        public bool IsDeliveryRequired { get; set; }

        public Money PriceForDelivery { get; set; }

        public Money TotalPrice { get; set; }
    }
}