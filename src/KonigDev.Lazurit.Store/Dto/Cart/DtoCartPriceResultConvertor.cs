﻿using KonigDev.Lazurit.Hub.Model.DTO.Order.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Dto;
using System;
using System.Linq;

namespace KonigDev.Lazurit.Store.Dto.Cart
{
    public static class DtoCartPriceResultConvertor
    {
        public static CreateOrderCommand ToOrder(this DtoCartPriceResult cart, Guid userProfileId)
        {
            if (cart == null) return null;

            var result = new CreateOrderCommand();
            result.UserProfileId = userProfileId;
            result.Id = Guid.NewGuid();
            result.IsApprovedBySeller = false;
            result.Sum = cart.TotalPrice.Amount;
            result.IsDeliveryRequired = cart.IsDeliveryRequired;

            result.Collections = cart.Collections
                .Select(p => new DtoOrderCollectionCreating
                {
                    CollectionVariantId = p.CollectionVariantId,
                    Id = Guid.NewGuid(),
                    Quantity = p.Quantity,
                    Products = p.Products.Select(x => new DtoOrderProductCreating
                    {
                        Id = Guid.NewGuid(),
                        Discount = (byte)x.Discount,
                        IsAssemblyRequired = x.IsAssemblyRequired,
                        Price = x.SalePrice.Amount,
                        ProductId = x.ProductId,
                        Quantity = x.Quantity
                    }).ToList()
                }).ToList();

            result.Products = cart.Products.Select(x => new DtoOrderProductCreating
            {
                Id = Guid.NewGuid(),
                Discount = (byte)x.Discount,
                IsAssemblyRequired = x.IsAssemblyRequired,
                Price = x.SalePrice.Amount,
                ProductId = x.ProductId,
                Quantity = x.Quantity
            }).ToList();

            result.Complects = cart.Complects.Select(p => new DtoOrderComplectCreating
            {
                Quantity = p.Quantity,
                CollectionVariantId = p.CollectionVariant.Id,
                Discount = (byte)p.Discount,
                Id = Guid.NewGuid(),
                IsAssemblyRequired = p.IsAssemblyRequired,
                Price = p.SalePrice.Amount
            }).ToList();

            return result;
        }
    }
}