﻿using System;
using Nest;

namespace KonigDev.Lazurit.Store.Dto.Cart.CartMini.Elastic
{
    public class DtoMiniCartCached
    {
        public Guid CartId { get; set; }
        public string UserId { get; set; }
        public DtoCartMiniPriceResult DtoCartMiniPriceResult { get; set; }
    }
}