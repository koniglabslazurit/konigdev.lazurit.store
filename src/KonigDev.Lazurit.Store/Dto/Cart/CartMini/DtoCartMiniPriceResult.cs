﻿using System;
using System.Collections.Generic;
using KonigDev.Lazurit.Store.Dto.Prices.Interfaces;
using KonigDev.Lazurit.Store.Models.Prices.KonigDev.Lazurit.Store.Models.Prices;

namespace KonigDev.Lazurit.Store.Dto.Cart.CartMini
{
    public class DtoCartMiniPriceResult:IDtoCart
    {
        public Guid Id { set; get; }
        public bool IsDeliveryRequired { get; set; }

        public ICollection<DtoCartMiniItemPriceComplect> Complects { set; get; }
        public List<DtoCartMiniItemPriceProduct> Products { set; get; }
        public ICollection<DtoCartMiniItemPriceCollection> Collections { get; set; }

        public Money PriceForDelivery { get; set; }

        public Money TotalPrice { get; set; }
    }
}