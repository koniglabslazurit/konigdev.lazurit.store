﻿using System.Collections.Generic;
using System.Linq;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.MiniCartDto;
using KonigDev.Lazurit.Store.Dto.Prices.Interfaces;
using KonigDev.Lazurit.Store.Models.Prices.KonigDev.Lazurit.Store.Models.Prices;

namespace KonigDev.Lazurit.Store.Dto.Cart.CartMini
{
    public class DtoCartMiniItemPriceCollection : DtoCartMiniItemCollection
    {
        // конструктор для сериализации
        public DtoCartMiniItemPriceCollection() { }
        public DtoCartMiniItemPriceCollection(DtoCartMiniItemCollection x)
        {
            Id = x.Id;
            CartId = x.CartId;
            CollectionVariantId = x.CollectionVariantId;
            Quantity = x.Quantity;
            SeriesName = x.SeriesName;
            RoomTypeName = x.RoomTypeName;
            CollectionAlias = x.CollectionAlias;
            RoomTypeAlias = x.RoomTypeAlias;
            CollectionId = x.CollectionId;
            Products = new List<DtoCartMiniItemPriceProductInCollection>(x.Products.Select(p => new DtoCartMiniItemPriceProductInCollection(p, Id)));
        }

        public new List<DtoCartMiniItemPriceProductInCollection> Products { get; set; }

        public Money CollectionSalePrice { get; set; }
    }
}