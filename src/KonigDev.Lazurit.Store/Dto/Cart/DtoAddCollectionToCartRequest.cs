﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Cart
{
    public class DtoAddCollectionToCartRequest
    {
        public Guid CollectionId { set; get; }
    }
}