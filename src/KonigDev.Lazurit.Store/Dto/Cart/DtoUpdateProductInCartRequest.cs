﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Cart
{
    public class DtoUpdateProductInCartRequest
    {
        public Guid ProductId { set; get; }
        public int Quantity { set; get; }

    }
}