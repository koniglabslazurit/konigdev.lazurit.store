﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants;
using KonigDev.Lazurit.Hub.Model.DTO.Products;

namespace KonigDev.Lazurit.Store.Dto.Cart
{
    public class DtoGetPriceResult
    {
        public List<DtoProductPricesResult> ProductResult { get; set; }
        public List<DtoComplectPricesResult> ComplectResult { get; set; }
    }
}