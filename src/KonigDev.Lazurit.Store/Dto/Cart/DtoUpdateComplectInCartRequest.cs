﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Cart
{
    public class DtoUpdateComplectInCartRequest
    {
        public Guid CollectionId { set; get; }
        public int Quantity { set; get; }
    }
}