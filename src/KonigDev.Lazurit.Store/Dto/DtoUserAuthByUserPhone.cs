﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto
{
    public class DtoUserAuthByUserPhone
    {
        public string Password { get; set; }
        public string ReturnUrl { get; set; }
        public string UserPhone { get; set; }
    }
}