﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Common
{
    [DataContract]
    public class DtoResponse
    {
        [DataMember(Name ="message")]
        public string Message { get; set; }

        public bool IsValid { get; set; }
    }

    [DataContract]
    public class DtoResponseToken
    {
        [DataMember(Name = "userId")]
        public string UserId { get; set; }
        [DataMember(Name = "token")]
        public string Token { get; set; }
    }
}