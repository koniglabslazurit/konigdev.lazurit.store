﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Payment
{
    public class DtoPaymentSignatureValue
    {
        public string SignatureValue { get; set; }
    }
}