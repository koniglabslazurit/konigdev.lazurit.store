﻿namespace KonigDev.Lazurit.Store.Dto.Payment
{
    public class DtoRobokassaResult
    {
        public string OutSum { get; set; }
        public string InvId { get; set; }
        public string SignatureValue { get; set; }        
    }
}