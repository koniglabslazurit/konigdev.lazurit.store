﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Payment
{
    public class DtoRoboPaymentMethod
    {
        public string TechName { get; set; }
        public string IncCurrLabel { get; set; }
    }
}