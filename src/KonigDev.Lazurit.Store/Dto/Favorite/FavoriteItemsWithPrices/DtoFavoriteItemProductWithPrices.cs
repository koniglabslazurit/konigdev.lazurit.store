﻿using KonigDev.Lazurit.Hub.Model.DTO.Favorite;
using System;

namespace KonigDev.Lazurit.Store.Dto.Favorite.FavoriteItemsWithPrices
{
    [Serializable]
    public class DtoFavoriteItemProductWithPrices
    {
        public Guid Id { set; get; }
        public DtoFavoriteProductWithPrices Product { set; get; }
    }
}