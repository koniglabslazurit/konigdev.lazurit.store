﻿using KonigDev.Lazurit.Hub.Model.DTO.Favorite;
using System;

namespace KonigDev.Lazurit.Store.Dto.Favorite.FavoriteItemsWithPrices
{
    [Serializable]
    public class DtoFavoriteComplectProductItemWithPrices
    {
        public string Article { get; set; }
        public string FacingColorName { get; set; }
        public string FacingName { get; set; }
        public string FrameColorIdName { get; set; }
        public string FurnitureType { get; set; }
        public string FurnitureTypeAlias { get; set; }
        public int Height { get; set; }
        public Guid Id { get; set; }
        public int Length { get; set; }
        public string SyncCode1C { get; set; }
        public int Width { get; set; }
    }
}
