﻿using KonigDev.Lazurit.Hub.Model.DTO.Favorite;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.Dto.Favorite.FavoriteItemsWithPrices
{
    [Serializable]
    public class DtoFavoriteWithPrices
    {
        public DtoFavoriteWithPrices()
        {
            Complects = new List<DtoFavoriteItemComplectWithPrices>();
            Products = new List<DtoFavoriteItemProductWithPrices>();
            Collections = new List<DtoFavoriteItemCollectionWithPrices>();
        }

        public Guid Id { set; get; }

        public IEnumerable<DtoFavoriteItemComplectWithPrices> Complects { set; get; }
        public IEnumerable<DtoFavoriteItemProductWithPrices> Products { set; get; }
        public IEnumerable<DtoFavoriteItemCollectionWithPrices> Collections { get; set; }
    }
}