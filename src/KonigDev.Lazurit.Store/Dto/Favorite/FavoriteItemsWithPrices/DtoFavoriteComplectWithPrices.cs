﻿using KonigDev.Lazurit.Store.Dto.Prices.Interfaces;
using System;
using KonigDev.Lazurit.Store.Models.Prices.KonigDev.Lazurit.Store.Models.Prices;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.Dto.Favorite.FavoriteItemsWithPrices
{
    [Serializable]
    public class DtoFavoriteComplectWithPrices : IComplectPrice
    {
        public Guid Id { get; set; }
        public string CollectionName { get; set; }
        public string RoomTypeName { get; set; }
        public string CollectionAlias { get; set; }
        public string RoomTypeAlias { get; set; }

        public IEnumerable<DtoFavoriteComplectProductItemWithPrices> Products { get; set; }

        public decimal Discount { get; set; }
        public int DiscountInstallment10Month { get; set; }
        public int DiscountInstallment12Month { get; set; }
        public int DiscountInstallment18Month { get; set; }
        public int DiscountInstallment24Month { get; set; }
        public int DiscountInstallment36Month { get; set; }
        public int DiscountInstallment48Month { get; set; }
        public int DiscountInstallment6Month { get; set; }
        public bool IsAssemblyRequired { get; set; }
        public Guid ItemId { get; set; }
        public Money Price { get; set; }
        public Money PriceForAssembly { get; set; }
        public Money SalePrice { get; set; }
        public Money SalePriceInstallment10Month { get; set; }
        public Money SalePriceInstallment10MonthWithAssembly { get; set; }
        public Money SalePriceInstallment12Month { get; set; }
        public Money SalePriceInstallment12MonthWithAssembly { get; set; }
        public Money SalePriceInstallment18Month { get; set; }
        public Money SalePriceInstallment18MonthWithAssembly { get; set; }
        public Money SalePriceInstallment24Month { get; set; }
        public Money SalePriceInstallment24MonthWithAssembly { get; set; }
        public Money SalePriceInstallment36Month { get; set; }
        public Money SalePriceInstallment36MonthWithAssembly { get; set; }
        public Money SalePriceInstallment48Month { get; set; }
        public Money SalePriceInstallment48MonthWithAssembly { get; set; }
        public Money SalePriceInstallment6Month { get; set; }
        public Money SalePriceInstallment6MonthWithAssembly { get; set; }
        public Money SalePriceWithAssembly { get; set; }
        public Money SalePriceWithOutAssembly { get; set; }
    }
}
