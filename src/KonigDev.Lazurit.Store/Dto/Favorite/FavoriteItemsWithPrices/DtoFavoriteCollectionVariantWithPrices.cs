﻿using KonigDev.Lazurit.Hub.Model.DTO.Favorite;
using System;

namespace KonigDev.Lazurit.Store.Dto.Favorite.FavoriteItemsWithPrices
{
    [Serializable]
    public class DtoFavoriteCollectionVariantWithPrices
    {
        public string CollectionAlias { get; set; }
        public string CollectionName { get; set; }
        public Guid Id { get; set; }
        public string RoomTypeAlias { get; set; }
        public string RoomTypeName { get; set; }
    }
}
