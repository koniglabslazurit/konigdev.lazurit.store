﻿using System;

namespace KonigDev.Lazurit.Store.Dto.Favorite.FavoriteItemsWithPrices
{
    [Serializable]
    public class DtoFavoriteItemComplectWithPrices
    {
        public Guid Id { set; get; }
        public DtoFavoriteComplectWithPrices CollectionVariant { set; get; }
    }
}