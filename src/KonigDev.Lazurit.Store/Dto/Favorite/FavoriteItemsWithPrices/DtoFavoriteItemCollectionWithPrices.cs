﻿using KonigDev.Lazurit.Hub.Model.DTO.Favorite;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.Dto.Favorite.FavoriteItemsWithPrices
{
    [Serializable]
    public class DtoFavoriteItemCollectionWithPrices
    {
        public DtoFavoriteItemCollectionWithPrices()
        {
            Products = new List<DtoFavoriteItemProductInCollectionWithPrices>();
        }

        public Guid Id { set; get; }
        public DtoFavoriteCollectionVariant CollectionVariant { set; get; }
        public IEnumerable<DtoFavoriteItemProductInCollectionWithPrices> Products { get; set; }
    }
}