﻿using System;
using System.Runtime.Serialization;

namespace KonigDev.Lazurit.Store.Dto.Favorite
{
    public class DtoModelOfFavoriteRequest
    {
        public Guid LinkId { set; get; }
        public bool IsUserOwner { set; get; }
        public string Patch { set; get; }
    }
}