﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Favorite
{
    public class DtoAddComplectToFavoriteRequest
    {
        public Guid CollectionId { set; get; }
    }
}