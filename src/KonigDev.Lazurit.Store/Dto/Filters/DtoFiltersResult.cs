﻿using KonigDev.Lazurit.Hub.Model.DTO.FacingColors;
using KonigDev.Lazurit.Hub.Model.DTO.Facings;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Filters
{
    public class DtoFiltersResult
    { 
        public List<DtoFacingsResult> Facings { get; set; }
        public List<DtoFacingColorResult> FacingColors { get; set; }
        public List<DtoFrameColorResult> FrameColors { get; set; }
    }
}