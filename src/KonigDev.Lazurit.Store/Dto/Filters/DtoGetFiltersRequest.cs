﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.Filters
{
    public class DtoGetFiltersRequest
    {
        public Guid CollectionId { get; set; }
        public Guid ArticleId { get; set; }
    }
}