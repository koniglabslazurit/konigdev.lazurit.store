﻿using System;

namespace KonigDev.Lazurit.Store.Dto
{
    public class DtoUserAuthByUserName
    {
        public string Password { get; set; }
        public string Login { get; set; }
        public string ReturnUrl { get; set; }
    }
}