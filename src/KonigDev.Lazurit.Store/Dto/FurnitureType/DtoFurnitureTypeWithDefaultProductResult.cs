﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Dto.FurnitureType
{
    public class DtoFurnitureTypeWithDefaultProductResult
    {
        public Guid Id { get; set; }
        public string Alias { get; set; }
        public string Name { get; set; }
        public Guid? DefaultProductId { get; set; }
    }
}