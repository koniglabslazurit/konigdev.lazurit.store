﻿using System;

namespace KonigDev.Lazurit.Store.Dto
{
    public class DtoRegion
    {
        public Guid RegionId { get; set; }
    }
}