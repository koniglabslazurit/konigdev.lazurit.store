﻿using KonigDev.Lazurit.Hub.Model.DTO.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KonigDev.Lazurit.Store.ViewModels.ProfileViewModels
{
    public class VmProfileForUpdate
    {
        public VmProfileForUpdate()
        {
            UserPhones = new List<DtoUserPhone>();
            UserAddresses = new List<DtoUserAddress>();
        }

        public Guid Id { get; set; }
        public string Title { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        [Required]
        public string LastName { get; set; }
        public List<DtoUserPhone> UserPhones { get; set; }
        public List<DtoUserAddress> UserAddresses { get; set; }
    }
}