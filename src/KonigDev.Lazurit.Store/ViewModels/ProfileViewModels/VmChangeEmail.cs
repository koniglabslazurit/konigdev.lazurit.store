﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.ViewModels.ProfileViewModels
{
    public class VmChangeEmail
    {
        public Uri ConfirmUrl { get; set; }
    }
}