﻿using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;
using KonigDev.Lazurit.Store.Dto.Prices;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.ViewModels.ProductViewModels
{
    public class VmProductMarketerModel
    {
        public VmProductMarketerModel()
        {
            Products = new List<DtoProductPrice>();
            Sizes = new List<VmProductSize>();
        }

        public List<VmProductSize> Sizes { get; set; }
        public string VendorCode { get; set; }
        public Guid MaxRangeProductId { get; set; }
        public List<DtoProductPrice> Products { get; set; }
        public string Range { get; internal set; }
    }

    public class VmProductSize
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public Guid ProductId { get; set; }
    }
}