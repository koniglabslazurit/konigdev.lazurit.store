﻿using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.ViewModels.UserViewMOdels
{
    public class VmUserInformation
    {
        public List<string> Roles { get; set; }
    }
}