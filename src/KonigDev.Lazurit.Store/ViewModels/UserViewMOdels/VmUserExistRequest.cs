﻿namespace KonigDev.Lazurit.Store.ViewModels.UserViewMOdels
{
    public class VmUserExistRequest
    {
        public string UserName { get; set; }
        public string Phone { get; set; }
    }
}