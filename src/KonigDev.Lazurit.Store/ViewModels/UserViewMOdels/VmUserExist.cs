﻿namespace KonigDev.Lazurit.Store.ViewModels.UserViewMOdels
{
    public class VmUserExist
    {
        public bool Exist { get; set; }
        public string Message { get; set; }
    }
}