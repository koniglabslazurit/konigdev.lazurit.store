﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.ViewModels.CollectionViewModels
{
    public class VmCollectionItem
    {
        public string Series { get; set; }
        public string Room { get; set; }
    }
}