﻿using System;

namespace KonigDev.Lazurit.Store.ViewModels.CollectionViewModels
{
    public class VmCollectionId
    {
        public Guid CollectionId { get; set; }
    }
}