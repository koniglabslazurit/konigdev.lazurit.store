﻿using System;

namespace KonigDev.Lazurit.Store.ViewModels.OrderViewModels
{
    public class VmOrderCreating
    {
        public Guid PaymentMethodId { get; set; }
        public string DeliveryKladrCode { get; set; }
        public string DeliveryAddress { get; set; }
        public bool IsRequiredSeller { get; set; }
    }
}