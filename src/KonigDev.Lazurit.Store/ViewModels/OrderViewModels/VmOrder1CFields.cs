﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.ViewModels.OrderViewModels
{
    public class VmOrder1CFields
    {
        public Guid Id { get; set; }
        public DateTime? CreationTime1C { get; set; }
        public string OrderNumber1C { get; set; }
    }
}