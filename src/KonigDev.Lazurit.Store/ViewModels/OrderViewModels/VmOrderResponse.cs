﻿using System;

namespace KonigDev.Lazurit.Store.ViewModels.OrderViewModels
{
    public class VmOrderResponse
    {
        public Guid Id { get; set; }
        public string Sum { get; set; }
        public long Number { get; set; }
        public bool IsPaid { get; set; }
        public string PaymentMethod { get; set; }
        /* Для робокассы */
        public string RobokassaUrl { get; set; }
        public string MrchLogin { get; set; }
        public string SignatureValue { get; set; }
        public string Desc { get; set; }
        /*конец для робокассы*/
    }
}