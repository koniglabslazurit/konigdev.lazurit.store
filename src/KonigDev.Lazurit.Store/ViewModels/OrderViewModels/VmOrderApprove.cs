﻿using System;
using System.ComponentModel.DataAnnotations;

namespace KonigDev.Lazurit.Store.ViewModels.OrderViewModels
{
    public class VmOrderApprove
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        public bool IsApproved { get; set; }
    }
}
