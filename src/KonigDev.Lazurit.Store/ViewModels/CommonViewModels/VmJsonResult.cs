﻿namespace KonigDev.Lazurit.Store.ViewModels.CommonViewModels
{
    /// <summary>
    /// Класс для формирования json ответа на клиент.
    /// </summary>
    public class VmJsonResult
    {
        public VmJsonResult() 
        {
            Success = true;
        }
        /// <summary>
        /// Признак корректности обработки запроса, по умолчанию True
        /// </summary>        
        public bool Success { get; set; }
        /// <summary>
        /// Текствое сообщение ответа.
        /// </summary>
        public string Message { get; set; }
    }
}