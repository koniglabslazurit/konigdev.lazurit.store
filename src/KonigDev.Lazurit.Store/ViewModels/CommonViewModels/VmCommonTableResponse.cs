﻿using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.ViewModels.CommonViewModels
{
    /// <summary>
    /// Класс на возвращение списка по запросу
    /// </summary>
    /// <typeparam name="T">Возвращаемый тип коллекции</typeparam>
    public class VmCommonTableResponse<T>
    {
        /// <summary>
        /// Коллекция элементов
        /// </summary>
        public IEnumerable<T> Items { get; set; }
        /// <summary>
        /// Общее количество элементов по запросу
        /// </summary>
        public int TotalCount { get; set; }
    }
}