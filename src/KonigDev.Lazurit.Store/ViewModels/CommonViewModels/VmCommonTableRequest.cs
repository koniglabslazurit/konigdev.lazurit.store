﻿namespace KonigDev.Lazurit.Store.ViewModels.CommonViewModels
{
    /// <summary>
    /// Класс для запросов на получение списков
    /// </summary>
    public class VmCommonTableRequest
    {
        public int Page { get; set; }
        public int ItemsOnPage { get; set; }
        public int ItemCount { get; set; }
    }
}