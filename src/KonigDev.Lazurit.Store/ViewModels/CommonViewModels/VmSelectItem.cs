﻿namespace KonigDev.Lazurit.Store.ViewModels.CommonViewModels
{
    public class VmSelectItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}