﻿using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Dto;
using System.Collections.Generic;
using System.Web.Mvc;

namespace KonigDev.Lazurit.Store.ViewModels.FilesContentManagment
{
    public class FilesContentTable
    {
        public DtoFilesContentObjectsResponse ContentObjects { get; set; }
        public List<SelectListItem> Types { get; set; }
    }
}