﻿(function ($) {

    bootstrap();
    // load the page
    function bootstrap() {
        $(document).ready(function () {
            _initializeFullpagePlugin();
            _initializeYoutubeBackground();

            _hideLoader();
        });
    }

    function _hideLoader() {
        $('body').addClass('loaded');
    }

    function _initializeFullpagePlugin() {
        $('.fullpage').fullpage({
            navigation: true,
            navigationPosition: 'right',
            fitToSection: true,
            //continuousVertical: true,
            sectionsColor: ['', '#fff', '#fff'],
            slidesNavigation: true,
            scrollBar: false,
            normalScrollElements: '.fullpagejs-do-not-scroll'
        });

        $('#moveSectionDown').click(function (e) {
            e.preventDefault();
            $.fn.fullpage.moveSectionDown();
        });
    }

    function _initializeYoutubeBackground() {
        $('#background-video').YTPlayer({
            fitToBackground: true,
            videoId: 'QGFbMYFTkZM',
            pauseOnScroll: false,
            playerVars: {
                modestbranding: 0,
                autoplay: 1,
                controls: 0,
                showinfo: 0,
                branding: 0,
                rel: 0,
                autohide: 0
            }
        });

        var videoCallbackEvents = function () {
            var player = $('#background-video').data('ytPlayer').player;

            player.addEventListener('onStateChange', function (event) {
                console.log("Player State Change", event);

                // OnStateChange Data
                if (event.data === 0) {
                    console.log('video ended');
                } else if (event.data === 2) {
                    console.log('paused');
                }
            });
        }
    }


})(jQuery);