﻿using System;
using System.Collections.Generic;
using System.Linq;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Query;
using MvcSiteMapProvider;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Seriess.Queries;
using KonigDev.Lazurit.Hub.Model.DTO.Seriess.Dto;

namespace KonigDev.Lazurit.Store.Providers
{
    public class CollectionDynamicProvider : IDynamicNodeProvider
    {
        private readonly IHubHandlersFactory _hubFactory;
        public CollectionDynamicProvider(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }
        public IEnumerable<DynamicNode> GetDynamicNodeCollection(ISiteMapNode node)
        {
            /* реализовать свой хэндлер на получение списка румов!!! */
            var roomsHandler = _hubFactory.CreateQueryHandler<GetRoomTypesQuery, List<DtoRoomTypesResult>>();

            /* todo закоменчено - нам необходимо получать серию а не коллекцию. Разобраться детальнее */
            //var collectionHandler = _hubFactory.CreateQueryHandler<GetCollectionsForBreadcrumbsQuery, List<DtoCollectionItemBreadcrumbs>>();
            var rooms = roomsHandler.Execute(new GetRoomTypesQuery()).Result;
            var seriesHandler = _hubFactory.CreateQueryHandler<GetSeriesByRequestQuery, List<DtoSeriesItem>>();
            var series = seriesHandler.Execute(new GetSeriesByRequestQuery()).Result;

            foreach (var room in rooms.Select(p => new { p.Alias, p.Name }).Distinct().ToList())
            {
                /* todo закоменчено - нам необходимо получать серию а не коллекцию. Разобраться детальнее */
                //var collection = collectionHandler.Execute(new GetCollectionsForBreadcrumbsQuery { RoomAlias = room.Alias }).Result;
                foreach (var seriesitem in series.Select(p => new { p.Name, p.Alias }).Distinct().ToList())
                {
                    /* todo для ключа берем имя серии и имя комнаты для уникальности. Разобраться детальнее, что необходимо для ключа */
                    DynamicNode dynamicNode = new DynamicNode(seriesitem.Name + room.Name, seriesitem.Name);
                    dynamicNode.ParentKey = room.Name;
                    dynamicNode.RouteValues.Add("roomAlias", room.Alias);
                    dynamicNode.RouteValues.Add("collectionAlias", seriesitem.Alias);
                    yield return dynamicNode;
                }
            }
        }

        public bool AppliesTo(string providerName)
        {
            if (string.IsNullOrEmpty(providerName))
                return false;
            return this.GetType() == Type.GetType(providerName, false);
        }
    }
}