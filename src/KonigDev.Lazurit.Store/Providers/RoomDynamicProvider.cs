﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Query;
using MvcSiteMapProvider;

namespace KonigDev.Lazurit.Store.Providers
{
    public class RoomDynamicProvider : IDynamicNodeProvider
    {
        private readonly IHubHandlersFactory _hubFactory;
        public RoomDynamicProvider(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        public IEnumerable<DynamicNode> GetDynamicNodeCollection(ISiteMapNode node)
        {
            var roomsHandler = _hubFactory.CreateQueryHandler<GetRoomTypesQuery, List<DtoRoomTypesResult>>();
            var rooms = roomsHandler.Execute(new GetRoomTypesQuery()).Result;
            foreach (var room in rooms)
            {
                DynamicNode dynamicNode = new DynamicNode(room.Name, room.Name);
                dynamicNode.Controller = "Catalog";
                dynamicNode.Action = "Room";
                dynamicNode.RouteValues.Add("alias", room.Alias);
                yield return dynamicNode;
            }
        }

        public bool AppliesTo(string providerName)
        {
            if (string.IsNullOrEmpty(providerName))
                return false;
            return this.GetType() == Type.GetType(providerName, false);
        }
    }
}