﻿using System;
using System.Configuration;
using System.Threading;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Store.Dto.Cart;
using KonigDev.Lazurit.Store.Dto.Cart.CartFull;
using KonigDev.Lazurit.Store.Dto.Cart.CartMini;
using KonigDev.Lazurit.Store.Services;
using KonigDev.Lazurit.Store.Services.Cart.Implementations;
using KonigDev.Lazurit.Store.Services.Cart.Interfaces;
using KonigDev.Lazurit.Store.Services.PriceSrvice;
using KonigDev.Lazurit.Store.SignalrService;
using KonigDev.Lazurit.Store.Services.CookieService;

namespace KonigDev.Lazurit.Store.Providers
{
    public class CartProvider : ICartProvider
    {
        private readonly IHubHandlersFactory _hubFactory;
        private readonly ISignalrService _signalrService;
        private readonly IPriceService _priceService;
        private readonly ICartCacheService _cartCacheService;
        private readonly ICookieService _cookieService;
        private ICartCommonService _cartCommonService;
        private ICartService<DtoCartFullPriceResult> _cartFullService;
        private static volatile ICartService<DtoCartMiniPriceResult> _cartMiniService;
        private static object _cartInstanceServiceSynx = new object();


        public CartProvider(IHubHandlersFactory hubFactory, ISignalrService signalrService, IPriceService priceService, 
            ICartCacheService cartCacheService, ICookieService cookieService)
        {
            _hubFactory = hubFactory;
            _priceService = priceService;
            _cartCacheService = cartCacheService;
            _cookieService = cookieService;
            _signalrService = signalrService;
        }
        public ICartService<DtoCartMiniPriceResult> ForOnlyMiniCart()
        {
            //многопоточный синглтон. Тут он применяется потому что в корзину при старте приложения может одновременно с нескольких мест прийти запрос и с разных потоков
            if (_cartMiniService == null)
            {
                lock (_cartInstanceServiceSynx)
                {
                    if (_cartMiniService == null)
                    {
                        _cartMiniService = new CartMiniService(_hubFactory, _signalrService, _cartCacheService, _priceService);

                    }
                }
            }
            return _cartMiniService;
        }

        public ICartService<DtoCartFullPriceResult> ForOnlyFullCart()
        {
            return _cartFullService ?? (_cartFullService = new CartFullService(_hubFactory, _priceService));
        }

        public ICartCommonService ForAllCarts()
        {
            return _cartCommonService ?? (_cartCommonService = new CartCommonService(_hubFactory, _cartCacheService,_priceService,_cookieService, ForOnlyMiniCart(), _signalrService));
        }
    }
}