﻿using KonigDev.Lazurit.Store.Dto.Cart;
using KonigDev.Lazurit.Store.Dto.Cart.CartFull;
using KonigDev.Lazurit.Store.Dto.Cart.CartMini;
using KonigDev.Lazurit.Store.Services.Cart.Interfaces;

namespace KonigDev.Lazurit.Store.Providers
{
    public interface ICartProvider
    {
        ICartService<DtoCartMiniPriceResult> ForOnlyMiniCart();
        ICartService<DtoCartFullPriceResult> ForOnlyFullCart();
        ICartCommonService ForAllCarts();
    }
}