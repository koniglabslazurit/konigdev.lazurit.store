﻿using System.Web.Http;
using Newtonsoft.Json.Serialization;

namespace KonigDev.Lazurit.Store
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            ////Раскомментировать,если необходимо использовать Bearer для аунтификации в api
            //config.SuppressDefaultHostAuthentication();
            //config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Use camel case for JSON data.
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            // Web API routes
            config.MapHttpAttributeRoutes();


            /* закоментил рутинг в веб апи по алиасу
             * в веб апи получение необходимо реализовывать через параметры: api/product?alias="<value>"
             * иначе full rest не реализуем, а так же перекрывает дефолтный рутинг  */
            //config.Routes.MapHttpRoute(
            //    name: "AliasApi",
            //    routeTemplate: "api/{controller}/{action}/{alias}",
            //    defaults: new { alias = RouteParameter.Optional }
            //);

            config.Routes.MapHttpRoute(
              name: "DefaultApi",
              routeTemplate: "api/{controller}/{action}/{id}",
              defaults: new { action = RouteParameter.Optional, id = RouteParameter.Optional }
          );
        }
    }
}
