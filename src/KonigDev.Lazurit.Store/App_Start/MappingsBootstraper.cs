﻿using AutoMapper;
using KonigDev.Lazurit.Store.App_Start.MappingProfiles;

namespace KonigDev.Lazurit.Store.App_Start
{
    public static class MappingsBootstraper
    {
        public static void Bootstrap()
        {
            Mapper.Initialize(cnf =>
            {
                cnf.AddProfile<CollectionProfile>();
                cnf.AddProfile<ProductProfile>();
                cnf.AddProfile<FavoriteProfile>();
                cnf.AddProfile<LayawayProfile>();
            });
        }
    }
}