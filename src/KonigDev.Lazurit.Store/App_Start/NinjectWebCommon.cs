﻿using System.Configuration;
using System.Web.Hosting;
using System.Web.Routing;
using KonigDev.Lazurit.Auth.GrainResolver.NinjectModules;
using KonigDev.Lazurit.Auth.InternalAPI.AccountApi.Interfaces;
using KonigDev.Lazurit.Auth.InternalAPI.NinjectModules;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Store.DI.Ninject.Modules;
using KonigDev.Lazurit.Store.Services;
using KonigDev.Lazurit.Store.Providers;
using KonigDev.Lazurit.Store.Services.PriceSrvice;
using KonigDev.Lazurit.Store.Services.Profile;
using MvcSiteMapProvider.Loader;
using MvcSiteMapProvider.Web.Mvc;
using MvcSiteMapProvider.Xml;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(KonigDev.Lazurit.Store.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(KonigDev.Lazurit.Store.App_Start.NinjectWebCommon), "Stop")]

namespace KonigDev.Lazurit.Store.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Factories;
    using Auth.Grains.Interfaces;
    using Microsoft.Owin.Security.DataProtection;
    using Hub.GrainResolver.NinjectModules;
    using Core.Services.StorageServices;
    using Core.Services;
    using SignalrService;
    using Services.Geolocation;
    using Services.CustomRequest;
    using Services.CookieService;
    using Services.OrderService;
    using Services.PaymentService;
    using Services.LazuritContext;
    using Services.ProductService;
    using Services.Cart.Interfaces;
    using Services.Cart.Implementations;
    using KonigDev.Lazurit.Store.Services.Auth;
    using Services.Auth;
    using Services.LayawayService;

    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IAuthServer>().ToMethod(x => new AuthServer(GetAuthBaseUrl(), ConfigurationManager.AppSettings["auth:TokenEndPoint"], ConfigurationManager.AppSettings["auth:AudienceId"],
                ConfigurationManager.AppSettings["auth:AudienceSecret"], kernel.Get<ILoggingService>(), kernel.Get<IAccountInternalAPI>(), kernel.Get<ICartProvider>(), kernel.Get<ILazuritCtxSessionService>(),
                kernel.Get<ILayawayService>()));
            kernel.Bind<IDataProtectionProvider>().ToMethod(x => Startup.DataProtectionProvider);
            kernel.Bind<IUserService>().To<UserService>();

            //core modules, azure storage
            kernel.Bind<IAzureService>().To<AzureService>()
                  .WithConstructorArgument("storageConnectionString", "storageConnectionString");
            kernel.Bind<IStorageService>().To<AzureStorageService>()
                .WithConstructorArgument("fileStoragePath", ConfigurationManager.AppSettings["azureContainerPrefix"]);

            /* создание структуры для хранилища файлов в azure */
            var t = kernel.Get<IStorageService>();
            t.CreateStorageStructure();

            //hub modules
            kernel.Bind<IHubHandlersFactory>().To<NinjectGrainsFactory>();
            kernel.Load<HubArticlesHandlerModule>();
            kernel.Load<HubBaseModule>();
            kernel.Load<HubCartHandlerModule>();
            kernel.Load<HubCityHandlerModule>();
            kernel.Load<HubCollectionsHandlersModule>();
            kernel.Load<HubCustomerRequestHandlerModule>();
            kernel.Load<HubFavoriteHandlerModule>();
            kernel.Load<HubFileContentModule>();
            kernel.Load<HubFiltersHandlerModule>();
            kernel.Load<HubFurnitureTypesHandlerModule>();
            kernel.Load<HubInstallmentHandlerModule>();
            kernel.Load<HubIntegrationHandlerModule>();
            kernel.Load<HubLayawayHandlerModule>();
            kernel.Load<HubMarketingActionsModule>();
            kernel.Load<HubOrderHandlersModule>();
            kernel.Load<HubPaymentHandlerModule>();
            kernel.Load<HubPaymentMethodHandlerModule>();
            kernel.Load<HubPriceZoneHandlerModule>();
            kernel.Load<HubProductsHandlerModule>();
            kernel.Load<HubPropertiesHandlersModule>();
            kernel.Load<HubRegionHandlerModule>();
            kernel.Load<HubRoomHandlerModule>();
            kernel.Load<HubSeriesHandlerModule>();
            kernel.Load<HubShowRoomHandlersModule>();
            kernel.Load<HubUsersHandlersModule>();
            //auth modules
            kernel.Bind<IAuthHandlersFactory>().To<NinjectAuthGrainsFactory>();
            kernel.Load<BaseModule>();
            kernel.Load<CommandQueryModule>();
            kernel.Load<InternalApiModule>();


            #region SiteMap
            kernel.Load(new MvcSiteMapProviderModule());
            MvcSiteMapProvider.SiteMaps.Loader = kernel.Get<ISiteMapLoader>();
            var validator = kernel.Get<ISiteMapXmlValidator>();
            validator.ValidateXml(HostingEnvironment.MapPath("~/Mvc.sitemap"));
            // Регистрирование урлов Sitemaps для поискового движка
            XmlSiteMapController.RegisterRoutes(RouteTable.Routes);
            #endregion

            kernel.Bind<ICartProvider>().To<CartProvider>().InSingletonScope();
            kernel.Bind<ICartCacheService>().ToMethod(x => new CartElasticCacheService(ConfigurationManager.AppSettings["es:Protokol"], ConfigurationManager.AppSettings["es:Domain"], ConfigurationManager.AppSettings["es:cartDafaultIndex"])).InSingletonScope();
            kernel.Bind<ISignalrService>().To<SignalrService>();


            kernel.Bind<IProductService>().To<ProductService>();
            kernel.Bind<IPriceService>().To<PriceService>();
            kernel.Bind<IGeolocationService>().To<GeolocationService>();
            kernel.Bind<ICustomRequestService>().To<CustomRequestService>();
            kernel.Bind<ICookieService>().To<CookieService>();
            kernel.Bind<IOrderService>().To<OrderService>();
            kernel.Bind<IProfileService>().To<ProfileService>();
            kernel.Bind<IPaymentService>().To<PaymentService>()
                .WithConstructorArgument("kkrDomainUrl", ConfigurationManager.AppSettings["kkrDomainUrl"]);
            kernel.Bind<ILazuritCtxSessionService>().To<LazuritCtxSessionService>();
            kernel.Bind<ILayawayService>().To<LayawayService>();
            //kernel.Bind<ILoggingService>().To<LoggingService>();
        }

        private static string GetAuthBaseUrl()
        {
            return $"{ConfigurationManager.AppSettings["auth:Protokol"]}://{ConfigurationManager.AppSettings["auth:Domain"]}";
        }
    }
}
