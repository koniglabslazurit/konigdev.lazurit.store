﻿using System.Web.Mvc;
using System.Web.Routing;

namespace KonigDev.Lazurit.Store
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.Ignore("{resource}.axd/{*pathInfo}");
            routes.Ignore("favicon.png");
            routes.Ignore("favicon.ico");

            routes.MapRoute(
                name: "Catalog",
                url: "Catalog",
                defaults: new { controller = "Catalog", action = "Index", id = UrlParameter.Optional }
                );

            routes.MapRoute(
              name: "Furniture",
              url: "Catalog-Single",
              defaults: new { controller = "Catalog", action = "Furniture" }
              );

            routes.MapRoute(
              name: "ProductsInFurniture",
              url: "Catalog-Single/{furnitureAlias}",
              defaults: new { controller = "Catalog", action = "ProductsInFurniture" }
              );

            routes.MapRoute(
            name: "ProductsInSubFurniture",
            url: "Catalog-Single/{parentFurnitureAlias}/{furnitureAlias}",
            defaults: new { controller = "Catalog", action = "ProductsInSubFurniture" }
            );

            routes.MapRoute(
               name: "Room",
               url: "Catalog/{alias}",
               defaults: new { controller = "Catalog", action = "Room", alias = UrlParameter.Optional }
               );           

            routes.MapRoute(
               name: "Product",
               url: "Catalog/Product/{furnitureAlias}/{productAlias}",
               defaults: new { controller = "Catalog", action = "Product", furnitureAlias = UrlParameter.Optional, productAlias = UrlParameter.Optional }
               );

            routes.MapRoute(
                name: "Collection",
                url: "Catalog/{roomAlias}/{collectionAlias}",
                defaults: new { controller = "Catalog", action = "Collection", roomAlias = UrlParameter.Optional, collectionAlias = UrlParameter.Optional }
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
