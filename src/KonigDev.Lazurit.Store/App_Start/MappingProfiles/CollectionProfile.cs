﻿using AutoMapper;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;
using KonigDev.Lazurit.Store.Dto.Prices;

namespace KonigDev.Lazurit.Store.App_Start.MappingProfiles
{
    public class CollectionProfile : Profile
    {
        public CollectionProfile()
        {
            CreateMap<DtoCollectionWithContent, DtoCollectionWithContentPrice>();
            CreateMap<DtoCollectionVariantContent, DtoCollectionVariantContentPrice>();
            CreateMap<DtoProductContent, DtoProductContentPrice>()
                 .ForMember(dest => dest.ItemId, opts => opts.MapFrom(src => src.ProductId));
        }
    }
}