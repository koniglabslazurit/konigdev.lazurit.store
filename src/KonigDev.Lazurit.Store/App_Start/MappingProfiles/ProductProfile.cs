﻿using AutoMapper;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;
using KonigDev.Lazurit.Store.Dto.Prices;

namespace KonigDev.Lazurit.Store.App_Start.MappingProfiles
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<DtoProduct, DtoProductPrice>()
                .ForMember(dest => dest.ItemId, opts => opts.MapFrom(src => src.Id));
        }
    }
}