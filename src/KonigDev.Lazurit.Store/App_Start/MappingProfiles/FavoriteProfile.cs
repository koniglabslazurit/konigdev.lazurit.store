﻿using AutoMapper;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants;
using KonigDev.Lazurit.Hub.Model.DTO.Favorite;
using KonigDev.Lazurit.Hub.Model.DTO.Products;
using KonigDev.Lazurit.Store.Dto.Favorite.FavoriteItemsWithPrices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.App_Start.MappingProfiles
{
    public class FavoriteProfile : Profile
    {
        public FavoriteProfile()
        {
            CreateMap<DtoFavorite, DtoFavoriteWithPrices>();

            CreateMap<DtoFavoriteItemProduct, DtoFavoriteItemProductWithPrices>();
            CreateMap<DtoFavoriteProduct, DtoFavoriteProductWithPrices>().ForMember(dest => dest.ItemId, opts => opts.MapFrom(src => src.Id));

            CreateMap<DtoFavoriteItemComplect, DtoFavoriteItemComplectWithPrices>();
            CreateMap<DtoFavoriteComplect, DtoFavoriteComplectWithPrices>().ForMember(dest => dest.ItemId, opts => opts.MapFrom(src => src.Id));
            CreateMap<DtoFavoriteComplectProductItem, DtoFavoriteComplectProductItemWithPrices>();

            CreateMap<DtoFavoriteItemCollection, DtoFavoriteItemCollectionWithPrices>();
            CreateMap<DtoFavoriteCollectionVariant, DtoFavoriteCollectionVariantWithPrices>();
            CreateMap<DtoFavoriteItemProductInCollection, DtoFavoriteItemProductInCollectionWithPrices>();           
            CreateMap<DtoFavoriteProductInCollection, DtoFavoriteProductInCollectionWithPrices>().ForMember(dest => dest.ItemId, opts => opts.MapFrom(src => src.Id));
           
        }
    }
}