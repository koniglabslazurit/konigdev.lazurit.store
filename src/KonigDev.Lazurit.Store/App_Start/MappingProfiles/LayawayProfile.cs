﻿using AutoMapper;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants;
using KonigDev.Lazurit.Hub.Model.DTO.Layaways;
using KonigDev.Lazurit.Hub.Model.DTO.Products;
using KonigDev.Lazurit.Store.Dto.Layaway;

namespace KonigDev.Lazurit.Store.App_Start.MappingProfiles
{
    public class LayawayProfile : Profile
    {
        public LayawayProfile()
        {
            CreateMap<DtoLayaway, DtoLayawayWithPrices>();
            CreateMap<DtoLayawayItemProduct, DtoLayawayItemProductWithPrices>();
            CreateMap<DtoLayawayItemComplect, DtoComplectItemProductWithPrices>()
                 .ForMember(dest => dest.ItemId, opts => opts.MapFrom(src => src.CollectionVarinatId));
            CreateMap<DtoLayawayItemCollection, DtoCollectionItemProductWithPrices>();
            CreateMap<DtoLayawayItemProductInCollection, DtoLayawayItemProductInCollectionWithPrices>();
            CreateMap<DtoCollectionVariantResult, DtoCollectionVariantWithPrices>();
            CreateMap<DtoProductResult, DtoProductLayawayWithPrices>()
                 .ForMember(dest => dest.ItemId, opts => opts.MapFrom(src => src.Id));
        }
    }
}
