﻿module.exports = function (config) {
    config.set({

        frameworks: ["jasmine", "karma-typescript"],

        files: [
            { pattern: "./frontend/**/*.spec.ts" },
            //'./node_modules/angular/angular.js',
            //'./node_modules/angular-mocks/angular-mocks.js'
            // { pattern: "./frontend/specs/**/*.ts" },
            // { pattern: "./frontend/**/!(*.module).ts" },
            // { pattern: "./frontend/**/!(*app).ts" },
        ],

        preprocessors: {
            "./frontend/**/*.ts": ["karma-typescript"]
        },

        /* todo создать таски галповые на запус тестов и разнести репортеров в зависимотси от того где гоняются тесты */
        reporters: ["progress", "teamcity"],

        karmaTypescriptConfig: {
            tsconfig: "./tsconfig.json"
        },

        browsers: ['PhantomJS'], //, 'PhantomJS_flags'
        //browsers: ["Chrome"],

        singleRun: true,

        // customLaunchers: {
        //     'PhantomJS_flags': {
        //         base: 'PhantomJS',
        //         flags: ['--load-images=false']
        //     }
        // }
    });
};
