﻿using System;
using System.Security.Claims;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using KonigDev.Lazurit.Store.Controllers;
using KonigDev.Lazurit.Store.Controllers.ApiControllers;
using NLog;
using KonigDev.Lazurit.Store.App_Start;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;

namespace KonigDev.Lazurit.Store
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            MappingsBootstraper.Bootstrap();

            /* loging start with version */
            try
            {
                var version = typeof(AccountController).Assembly.GetName().Version.ToString();
                var log = LogManager.GetCurrentClassLogger();
                log.Info($"I_Start:{(!string.IsNullOrEmpty(version) ? $"; Version - {version}" : "version undefined")}");
            }
            catch (Exception ex)
            {
                var log = LogManager.GetCurrentClassLogger();
                log.Info($"I_Start: with error {0}", ex.Message);
            }
        }
    }
}
