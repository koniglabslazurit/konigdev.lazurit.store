﻿using KonigDev.Lazurit.Auth.Grains.Interfaces;
using Ninject;
using Ninject.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Store.Factories
{
    public class NinjectAuthGrainsFactory : IAuthHandlersFactory
    {
        private readonly IResolutionRoot _resolutionRoot;
        public NinjectAuthGrainsFactory(IResolutionRoot resolutionRoot)
        {
            _resolutionRoot = resolutionRoot;
        }
        public IAuthCommandHandler<TCommand> CreateCommandHandler<TCommand>()
        {
            return _resolutionRoot.Get<IAuthCommandHandler<TCommand>>();
        }

        public IAuthQueryHandler<TQuery, TResult> CreateQueryHandler<TQuery, TResult>()
        {
            return _resolutionRoot.Get<IAuthQueryHandler<TQuery, TResult>>();
        }
    }
}