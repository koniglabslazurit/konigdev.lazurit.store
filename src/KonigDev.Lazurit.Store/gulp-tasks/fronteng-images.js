﻿var gulp = require('gulp');
var flatten = require('gulp-flatten');
var del = require('del');

var imgSrc = './Content/**/*.{png,jpg,jpeg,svg,ico}';
var imgDest = './wwwroot/images/';

gulp.task('frontend-img', ['remove-img'], function () {
    return gulp.src(imgSrc)
        .pipe(flatten())
        .pipe(gulp.dest(imgDest));
});

gulp.task('remove-img', function () {
    return del(imgDest);
});