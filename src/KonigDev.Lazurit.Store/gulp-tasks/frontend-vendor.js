﻿var gulp = require("gulp");
var browserify = require('browserify');
var sourceStream = require('vinyl-source-stream');
var buffer = require("vinyl-buffer");
var uglify = require("gulp-uglify");

var constants = require("./constants");

gulp.task("frontend-vendor", function ()
{
    var bundleName = "vendor.js";    
    var destinationPath = "./wwwroot";
    
    return browserify()
        .require(constants.vendorLibs)
        .bundle()
        .pipe(sourceStream(bundleName))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(gulp.dest(destinationPath));
});