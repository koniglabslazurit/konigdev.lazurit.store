﻿var gulp = require('gulp');
var less = require('gulp-less');
var cssnano = require('gulp-cssnano');
var sourcemap = require('gulp-sourcemaps');
var del = require('del');

var lessPointPath = './Content/style.less';
var lessPath = "./Content/src/";
var cssDest = "./wwwroot/";

gulp.task('frontend-less', ['remove-css'], function () {
    return gulp.src(lessPointPath)
        .pipe(sourcemap.init())
        .pipe(less({ paths: [lessPath] }))
        .pipe(cssnano())
        .pipe(sourcemap.write())
        .pipe(gulp.dest(cssDest));
});

gulp.task('remove-css', function () {
    return del(cssDest + "/style.css");
});