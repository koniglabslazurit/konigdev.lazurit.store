﻿var gulp = require('gulp');
var del = require('del');

var nodePath = './node_modules/';
var libDest = './wwwroot/lib/';


gulp.task('frontend-lib', ['remove-lib'], function () {
    var node = {
        'angular': 'angular/angular*.{js,map}',
        'angular-ui-router': 'angular-ui-router/release/*.{js,map}',
        'angular-toastr': 'angular-toastr/dist/angular-toastr.{js,map,css,tpls.js}',
        'jquery': 'jquery/dist/jquery*.{js,map}',
        'jquerykladr': 'jquery.kladr/*.{min.js,min.css}',
        'angular-ui-bootstrap': 'angular-ui-bootstrap/dist/ui-bootstrap*.{js,tpls.js}',
        'bootstrap': 'bootstrap/dist/css/bootstrap*.{css,min.css,map}',
        'chosen': 'angular-chosen-localytics/dist/angular-chosen.{js,min.js}',
        'chosenjq': 'chosen-js/chosen*{jquery.js,css,png}',
        'ng-infinite-scroll': 'ng-infinite-scroll/build/ng-infinite-scroll.min.js',
        'ui-router-extras': 'ui-router-extras/release/*.{js,min.js}',
        'angular-ui-mask': 'angular-ui-mask/dist/mask.min.js'
    };

    for (var destinationDir in node) {
        gulp.src(nodePath + node[destinationDir])
            .pipe(gulp.dest(libDest + destinationDir));
    }
});

gulp.task('remove-lib', function () {
    return del(libDest);
});
