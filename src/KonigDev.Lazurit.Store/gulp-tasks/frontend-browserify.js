﻿var gulp = require("gulp");
var browserify = require("browserify");
var source = require("vinyl-source-stream");
var buffer = require("vinyl-buffer");
var sourcemaps = require("gulp-sourcemaps");
var uglify = require("gulp-uglify");
var stringify = require("stringify");
var tsify = require("tsify");

var constants = require("./constants");

gulp.task("frontend-browserify", function () {
    var bundleName = "app.js";
    var entryPointPath = "./frontend/app.ts";
    var destinationPath = "./wwwroot";

    var bundler = browserify(
        {
            entries: entryPointPath,
            debug: true
        })
        .plugin("tsify")
        .transform(stringify,
        {
            appliesTo: { includeExtensions: ['.html'] },
            minify: true
        })
        .external(constants.vendorLibs);

    return bundler
        .bundle()
        .pipe(source(bundleName))
        .pipe(buffer())
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest(destinationPath));
})