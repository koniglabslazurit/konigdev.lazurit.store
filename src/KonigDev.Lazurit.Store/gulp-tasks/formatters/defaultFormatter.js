"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};

var Lint = require('tslint');
require('colors');

var Formatter = (function (_super) {
  __extends(Formatter, _super);
  function Formatter() {
    _super.apply(this, arguments);
  }

  Formatter.prototype.formatFailure = function (failure) {
    var _a = failure
      .getStartPosition()
      .getLineAndCharacter();
    var line = _a.line;
    var character = _a.character;

    var position = (line + 1) + ":" + (character + 1);
    var message = failure.getFailure();
    var ruleName = failure.getRuleName();
    return "  " + position.grey + " " + message + " " + ruleName.grey;
  };

  Formatter.prototype.groupByFile = function (failures) {
    return failures.reduce(function (groups, failure) {
      var fileName = failure.getFileName();
      var fileFailures = groups[fileName] || [];
      groups[fileName] = fileFailures.concat([failure]);
      return groups;
    }, {});
  };

  Formatter.prototype.format = function (failures) {
    var _this = this;
    var failuresByFile = this.groupByFile(failures);
    return Object
      .keys(failuresByFile)
      .reduce(function (lines, fileName) {
        lines.push(fileName.red);
        var fileFailures = failuresByFile[fileName];
        return lines.concat(fileFailures.map(_this.formatFailure), ['\n']);
      }, []).join('\n');
  };
  return Formatter;
}(Lint.Formatters.AbstractFormatter));

exports.Formatter = Formatter;
