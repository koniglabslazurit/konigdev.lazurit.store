﻿var gulp = require('gulp');
var tslint = require("gulp-tslint");

var frontendTSFiles = "./frontend/**/*.ts";

gulp.task("frontend-tslint", function () {
    var tslintOptions =
        {
            formattersDirectory: "gulp-tasks/formatters",
            formatter: "default",
            configuration: "./tslint.json"
        };
    gulp.src(frontendTSFiles)
        .pipe(tslint(tslintOptions))
        .pipe(tslint.report({
            // emitError: true
            emitError: false
        }));
});