﻿var gulp = require('gulp');
var mkdirp = require('mkdirp');
var del = require('del');

var imgSrc = './Content/src/img/icons/**/*.{svg,ico}';
var imgDest = './wwwroot/src/img/icons/';

gulp.task('frontend-icons', ['remove-icons'], function () {
    mkdirp(imgDest, function (err) {
        if (err) console.log(err)
        else return gulp.src(imgSrc)
        .pipe(gulp.dest(imgDest));
    })    
});

gulp.task('remove-icons', function () {
    return del(imgDest);
});