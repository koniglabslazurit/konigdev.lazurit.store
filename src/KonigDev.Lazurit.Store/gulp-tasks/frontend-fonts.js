﻿var gulp = require('gulp');
var del = require('del');

var fontSrc = './Content/fonts/*.{ttf,woff,css,eot,svg}';
var fontDest = './wwwroot/fonts/';


gulp.task('frontend-font', ['remove-font'], function () {
    return gulp.src(fontSrc)
        .pipe(gulp.dest(fontDest));
});

gulp.task('remove-font', function () {
    return del(fontDest);
});

