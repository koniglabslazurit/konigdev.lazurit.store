﻿using System.Linq;
using System.Web.Http.ModelBinding;

namespace KonigDev.Lazurit.Store.Infrastructure
{
    public class HttpErrorsResponseBuilder
    {
        public static string GetModelStateErrors(ModelStateDictionary modelState)
        {
            var message = modelState.ToDictionary(p => p.Key, p => p.Value.Errors.Select(e => e.ErrorMessage))
                  .Where(p => !string.IsNullOrWhiteSpace(p.Key))
                  .Select(error => error.Value.ToList())
                  .Aggregate("", (current1, e) => e.Aggregate(current1, (current, str) => string.Format("{0}{1};", current, str)));
            return message;
        }
    }
}