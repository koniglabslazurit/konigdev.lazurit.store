﻿using System;
using System.Linq;
using KonigDev.Lazurit.Store.Models.Localization;
using KonigDev.Lazurit.Store.Models.Prices;
using KonigDev.Lazurit.Store.Models.Prices.KonigDev.Lazurit.Store.Models.Prices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace KonigDev.Lazurit.Store.Infrastructure
{
    public class MoneySerializer : JsonConverter
    {
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jsonObject = JObject.Load(reader);
            var properties = jsonObject.Properties().ToList();
            var currency = properties.Single(x => x.Name.ToLower() == "currency").Value;
            var cur = new Currency(new Language((string)currency["cultureName"]?? (string)currency["CultureName"]), (string)currency["code"]?? (string)currency["Code"], String.Empty, (string)currency["symbol"] ?? (string)currency["Symbol"], 1);
            var res = new Money((decimal)properties.Single(x => x.Name.ToLower() == "amount").Value, cur);
            return res;
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(Money).IsAssignableFrom(objectType);
        }

        public override bool CanWrite => false;
        public override bool CanRead => true;

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}