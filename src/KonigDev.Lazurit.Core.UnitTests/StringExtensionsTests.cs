﻿using System.Diagnostics;
using System.Linq;
using KonigDev.Lazurit.Core.Extensions;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace KonigDev.Lazurit.Core.UnitTests
{
    [TestFixture]
    public class StringExtensionsTests
    {
        [Test]
        [TestCase("Спальни")]
        [TestCase("Спальни Элионора")]
        [TestCase("Спальни трудная!23344!;%%&*()_+cстрока")]
        public void Transliterate_AnyString_AnyStringEqualTransliterateResult(string anyString)
        {
            //arr
            //act
            var translateAnyString=anyString.Transliterate();

            //ass
            Debug.WriteLine(translateAnyString);
            Assert.That(translateAnyString == "spalni" || translateAnyString == "spalni-elionora" || translateAnyString == "spalni-trudnaya23344_-cstroka");   
        }

        [Test]
        [TestCase("Спальни")]
        [TestCase("Гостиная")]
        [TestCase("Прихожая")]
        [TestCase("Спальни Элионора")]
        [TestCase("Спальни трудная!23344!;%%&*()_+cстрока")]
        public void Transliterate_AnyString_TransliterateResultReturnAllInLowerCamelCase(string anyString)
        {
            //arr
            //act
            var translateAnyString = anyString.Transliterate();

            //ass
            Debug.WriteLine(translateAnyString);
            Assert.That(translateAnyString.ToLower()== translateAnyString);
        }
    }
}
