-- remove data in integration tests data base
use LazuritDevIntegratedTests

delete from [Showrooms]
delete from [Cities]
delete from [Favorites]
delete from [Layaways]
delete from [Prices]
delete from [FilesContent]
delete from [FilesContentTypes]
delete from [AspNetUserRoles]
delete from [AspNetUsers]
delete from [AspNetRoles]
delete from [CollectionsVariantsContentProductsAreas]
delete from [ProductsToCollectionsVariants]
delete from [CollectionsVariants]
delete from [Collections]
delete from [Series]
delete from [RoomsTypes]
delete from [PaymentMethods]
delete from [UsersPhones]
delete from [UsersAddresses]
delete from [UsersProfiles]
delete from [Cities]
delete from [Regions]
delete from [PriceZones]
delete from [PropertiesToProductss]
delete from [Products]
delete from [FrameColors]
delete from [FacingsColors]
delete from [Articles]
delete from [FurnituresTypes]
delete from [Facings]
delete from [ProductPartMarkers]
delete from [Properties]
delete from [Styles]
delete from [TargetAudiences]

delete from [IntegrationsCollectionsVariants]
delete from [IntegrationsCollections]
delete from [IntegrationsRooms]
delete from [IntegrationsSeries]
delete from [IntegrationsProducts]