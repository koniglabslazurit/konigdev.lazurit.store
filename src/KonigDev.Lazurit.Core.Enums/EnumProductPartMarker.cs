﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Core.Enums
{
    public enum EnumProductPartMarker
    {
        /// <summary>
        /// Присутствует, обячзательно и требует участие продавца
        /// </summary>
        ExistedRequired = 0,

        /// <summary>
        /// Присутствует не обязательно и требует участия продавца
        /// </summary>
        ExistedNonRequired = 1,

        /// <summary>
        /// Отсутствует, не обязательно не требует участия продавца
        /// </summary>
        Missing = 2,
    }
}
