﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Core.Enums.Constants
{
    public static partial class Exceptions
    {
        public static class City
        {
            public const string RegionIsEmpty = "Регион пустой";
            public const string PriceZoneIsEmpty = "Ценовая зона пустая";
            public const string CityNotFound = "Город не найден";
        }
    }
}
