﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Core.Enums.Constants
{
    public static partial class Exceptions
    {
        public static class Common
        {
            public const string ModelEmpty = "Модель пустая";
            public const string ModelIdEmpty = "Ид модели пустой";
            public const string ModelNameEmpty = "Название пустое";
        }
    }
}
