﻿namespace KonigDev.Lazurit.Core.Enums.Constants
{
    public static partial class Exceptions
    {
        public static class Cart
        {
            public const string CartHasProduct = "Такой продукт уже есть в корзине";
            public const string CartHasComplekt = "Такой комплект уже есть в корзине";
            public const string CartHasCollection = "Такая коллекция уже есть в корзине";
            public const string CartNotFound = "Корзина не найдена";
            public const string CartItemNotFound = "Данный товар в корзине не найден";
        }
    }
}