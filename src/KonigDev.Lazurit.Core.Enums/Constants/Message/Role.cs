﻿namespace KonigDev.Lazurit.Core.Enums.Constants.Message
{
    public static class Role
    {
        public const string AdminRoleName = "Администратор";
        public const string AnonymRoleName = "Аноним";
        public const string BuyerRoleName = "Клиент";
        public const string ContentManagerRoleName = "Контент менеджер";
        public const string MarketerRoleName = "Маркетолог";
        public const string RegionRoleName = "Регион";
        public const string SallerRoleName = "Продавец";
        public const string SalonSallerRoleName = "Продавец ИМ";
        public const string ShopSallerRoleName = "Продавец салона";
    }
}
