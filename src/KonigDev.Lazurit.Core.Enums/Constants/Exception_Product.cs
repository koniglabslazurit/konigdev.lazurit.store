﻿namespace KonigDev.Lazurit.Core.Enums.Constants
{
    public static partial class Exceptions
    {
        public static class Product
        {
            public const string ProductNotFound = "Продукт не найден";
            public const string ProductInstallmentModelNotValid = "Невалидные данные";
        }
    }
}
