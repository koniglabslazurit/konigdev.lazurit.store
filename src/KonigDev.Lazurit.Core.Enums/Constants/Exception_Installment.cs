﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Core.Enums.Constants
{
    public static partial class Exceptions
    {
        public static class Installment
        {
            public const string InstallmentNotFound = "Скидка не найдена";
        }
    }
}
