﻿namespace KonigDev.Lazurit.Core.Enums.Constants
{
    public static partial class Exceptions
    {
        public static class User
        {
            public const string UserIsEmpty = "Юзер ид пустой";
            public const string RequiresRegistration = "Необходима регистрация";
        }
    }
}