﻿namespace KonigDev.Lazurit.Core.Enums.Constants
{
    public static partial class Exeptions
    {
        public static class Favorite
        {
            public const string FavoriteHasProduct = "Такой продукт уже есть в избранном";
            public const string FavoriteHasComplect = "Такой комплект уже есть в избранном";
            public const string FavoriteHasCollection = "Такая коллекция уже есть в избранном";
            public const string FavoriteHasNoProduct = "Такой продукт отсутствует в избранном";
            public const string FavoriteHasNoComplect = "Такой комплект отсутствует в избранном";
            public const string FavoriteHasNoCollection = "Такая коллекция отсутствует в избранном";
            public const string FavoriteNotFound = "Избранное не найдено";            
        }
    }
}