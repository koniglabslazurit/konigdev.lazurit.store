﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Core.Enums.Exeptions.Integrations
{
    public class IntegrationVariantNotFound: Exception
    {
        public IntegrationVariantNotFound(string message) : base(message) { }
    }
}
