﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Core.Enums.Exeptions.Installment
{
    [Serializable]
    public class InstallmentNotFoundException: Exception
    {
        public InstallmentNotFoundException(string message) : base(message) { }
    }
}
