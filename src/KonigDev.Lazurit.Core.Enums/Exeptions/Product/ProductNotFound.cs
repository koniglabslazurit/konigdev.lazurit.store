﻿using System;

namespace KonigDev.Lazurit.Core.Enums.Exeptions.Product
{
    public class ProductNotFound : Exception
    {
        public ProductNotFound(string message) : base(message)
        {
        }
    }
}
