﻿using System;

namespace KonigDev.Lazurit.Core.Enums.Exeptions.Product
{
    [Serializable]
    public class ProductInstallmentModelNotValid : Exception
    {
        public ProductInstallmentModelNotValid(string message) : base(message) { }
    }
}
