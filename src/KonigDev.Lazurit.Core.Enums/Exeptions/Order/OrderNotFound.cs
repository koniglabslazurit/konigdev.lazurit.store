﻿using System;

namespace KonigDev.Lazurit.Core.Enums.Exeptions.Order
{
    public class OrderNotFound : Exception
    {
        public OrderNotFound(string message) : base(message)
        {
        }
    }
}
