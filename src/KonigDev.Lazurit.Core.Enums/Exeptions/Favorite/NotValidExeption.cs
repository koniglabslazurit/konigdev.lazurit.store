﻿using System;

namespace KonigDev.Lazurit.Core.Enums.Exeptions.Favorite
{
    public class NotValidExeption : Exception
    {
        public NotValidExeption(string message) : base(message)
        {
        }
    }
}
