﻿using System;

namespace KonigDev.Lazurit.Core.Enums.Exeptions.Favorite
{
    public class FavoriteNotFound : Exception
    {
        public FavoriteNotFound(string message) : base(message)
        {
        }
    }
}