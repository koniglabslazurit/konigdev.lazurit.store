﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Core.Enums.Exeptions.Common
{
    public class NotValidCommandException: Exception
    {
        public NotValidCommandException(string message) : base(message)
        { }
    }
}
