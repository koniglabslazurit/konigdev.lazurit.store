﻿using System;

namespace KonigDev.Lazurit.Core.Enums.Exeptions.Cart
{
    public class CartNotFound : Exception
    {
        public CartNotFound(string message) : base(message)
        {
        }
    }
}