﻿using System;

namespace KonigDev.Lazurit.Core.Enums.Exeptions.Cart
{
    public class NotValidExeption : Exception
    {
        public NotValidExeption(string message) : base(message)
        {
        }
    }
}
