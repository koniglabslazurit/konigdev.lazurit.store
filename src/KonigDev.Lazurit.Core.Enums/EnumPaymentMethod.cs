﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Core.Enums
{
    /// <summary>
    /// Способы оплаты
    /// </summary>
    public enum EnumPaymentMethod
    {
       BankCard = 0,
       Cash = 1,
       Instalment = 2,
       OtherMethod = 3,
       QiwiWallet = 4,
       YandexMoney = 5,
       PayPal = 6,
    }
}
