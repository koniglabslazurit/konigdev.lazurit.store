﻿namespace KonigDev.Lazurit.Core.Enums
{
    public enum EnumFileContentType
    {
        /// <summary>
        /// Дефолтный тип - добавляется по умолчанию, нужно чтобы откидыват неверные запросы
        /// </summary>
        None = 0,

        /// <summary>
        /// Картинка в интерьере
        /// </summary>
        InInterior = 1,

        /// <summary>
        /// Картинка для слайдера маленькая
        /// </summary>
        SliderMin = 2,

        /// <summary>
        /// Картинка для слайдера средняя
        /// </summary>
        SliderMiddle = 3,

        /// <summary>
        /// Картинка для тайла маленькая размера ~ 50x50
        /// </summary>
        TileMin = 4,

        /// <summary>
        /// Картика для тайла большая (отображение на странице)
        /// </summary>
        TileBig = 5,

        /// <summary>
        /// Картинка без интерьера (на белом фоне)
        /// </summary>
        WithoutInterior = 6,
        
        /// <summary>
        /// Картинка для слайдера "открытый вид" (для продукта)
        /// </summary>
        SliderOpen = 7,

        /// <summary>
        /// Картинка для слайдера, в интерьере на странице продукта
        /// </summary>
        SliderInInterior = 8
    }
}