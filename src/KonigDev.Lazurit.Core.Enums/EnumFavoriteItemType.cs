﻿namespace KonigDev.Lazurit.Core.Enums
{
    /// <summary>
    /// Тип добавляемого/удаляемого элемента в избранное
    /// </summary>
    public enum EnumFavoriteItemType
    {
        Product,
        Collection,
        Complect,
        ProductInCollection
    }
}
