﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Core.Enums
{
    /// <summary>
    /// Тип добавляемого/удаляемого элемента в корзину
    /// </summary>
    public enum EnumCartItemType
    {
        Product,
        Collection,
        Complect,
        ProductInCollection
    }
}
