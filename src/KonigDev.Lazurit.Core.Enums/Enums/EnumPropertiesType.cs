﻿namespace KonigDev.Lazurit.Core.Enums.Enums
{
    public enum EnumPropertiesType
    {
        Backlight = 0,
        Material = 1,
        LeftRight = 2,
        ProductPartMarker = 3,
        Mechanism = 4,
        FurnitureForm = 5
    }
}
