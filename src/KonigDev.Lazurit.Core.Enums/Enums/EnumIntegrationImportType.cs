﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Core.Enums.Enums
{
    [Serializable]
    public enum EnumIntegrationImportType
    {
        Products,
        Zones
    }
}
