﻿namespace KonigDev.Lazurit.Core.Enums.Enums
{
    public enum EnumIntegrationStatus
    {
        /// <summary>
        /// Требует валидации
        /// </summary>
        Validation = 1,

        /// <summary>
        /// В черновиках
        /// </summary>
        Template = 3,

        /// <summary>
        /// Опубликовано
        /// </summary>
        Published = 4
    }
}
