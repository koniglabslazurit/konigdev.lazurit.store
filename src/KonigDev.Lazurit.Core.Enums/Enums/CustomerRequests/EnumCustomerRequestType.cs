﻿using System.ComponentModel;

namespace KonigDev.Lazurit.Core.Enums.Enums.CustomerRequests
{
    public enum EnumCustomerRequestType
    {
        /// <summary>
        /// Call request
        /// </summary>
        [Description("Заказ звонка")]
        Call,

        /// <summary>
        /// Designer request (online)
        /// </summary>
        [Description("Заказ дизайнера (онлайн)")]
        DesignerOnline,

        /// <summary>
        /// Designer request (to home)
        /// </summary>
        [Description("Заказ дизайнера (на дом)")]
        DesignerHome
    }
}
