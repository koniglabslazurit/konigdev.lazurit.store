﻿using System.ComponentModel;

namespace KonigDev.Lazurit.Core.Enums.Enums.CustomerRequests
{
    public enum EnumCustomerRequestStatus
    {
        /// <summary>
        /// In work
        /// </summary>
        [Description("В работе")]
        Processing,

        /// <summary>
        /// Not in work
        /// </summary>
        [Description("Не в работе")]
        Queued
    }
}
