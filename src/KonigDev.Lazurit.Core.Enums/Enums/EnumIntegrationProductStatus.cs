﻿namespace KonigDev.Lazurit.Core.Enums.Enums
{
    public enum EnumIntegrationProductStatus
    {        
        /// <summary>
        /// Отсутствует в базе
        /// </summary>
        Missing = 1,
        /// <summary>
        /// Не изменился
        /// </summary>
        NotChanged = 2,
        /// <summary>
        /// Изменился
        /// </summary>
        Changed = 3,
        /// <summary>
        /// По разным правилам не импортируется
        /// </summary>
        Skip=4
    }
}
