﻿namespace KonigDev.Lazurit.Core.Enums.Enums
{
    public enum EnumInstallmentPeriod
    {
        /// <summary>
        /// Рассрочка на 6 месяцев
        /// </summary>
        Installment6Month = 6,

        /// <summary>
        /// Рассрочка на 10 месяцев
        /// </summary>
        Installment10Month = 10,


        /// <summary>
        /// Рассрочка на 12 месяцев
        /// </summary>
        Installment12Month = 12,

        /// <summary>
        /// Рассрочка на 18 месяцев
        /// </summary>
        Installment18Month = 18,

        /// <summary>
        /// Рассрочка на 24 месяцев
        /// </summary>
        Installment24Month = 24,

        /// <summary>
        /// Рассрочка на 36 месяцев
        /// </summary>
        Installment36Month = 36,

        /// <summary>
        /// Рассрочка на 48 месяцев
        /// </summary>
        Installment48Month = 48,
    }
}
