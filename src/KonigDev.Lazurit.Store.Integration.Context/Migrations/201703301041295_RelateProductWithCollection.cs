namespace KonigDev.Lazurit.Store.Integration.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RelateProductWithCollection : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.IntegrationsCollectionsVariants", "SyncCode1C", c => c.String());
            AddColumn("dbo.IntegrationsProducts", "CollectionId", c => c.Guid(nullable: false));
            CreateIndex("dbo.IntegrationsProducts", "CollectionId");
            AddForeignKey("dbo.IntegrationsProducts", "CollectionId", "dbo.IntegrationsCollections", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.IntegrationsProducts", "CollectionId", "dbo.IntegrationsCollections");
            DropIndex("dbo.IntegrationsProducts", new[] { "CollectionId" });
            DropColumn("dbo.IntegrationsProducts", "CollectionId");
            DropColumn("dbo.IntegrationsCollectionsVariants", "SyncCode1C");
        }
    }
}
