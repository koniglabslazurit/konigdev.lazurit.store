namespace KonigDev.Lazurit.Store.Integration.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMarketingFiledsEditBaseTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.IntegrationsLoadingsItems",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IntegrationLoadingId = c.Guid(nullable: false),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IntegrationsLoadings", t => t.IntegrationLoadingId, cascadeDelete: true)
                .Index(t => t.IntegrationLoadingId);
            
            AddColumn("dbo.IntegrationsLoadings", "Type", c => c.String(nullable: false));
            AddColumn("dbo.IntegrationsProducts", "MarketerRange", c => c.String(nullable: false));
            AddColumn("dbo.IntegrationsProducts", "MarketerVendorCode", c => c.String(nullable: false));
            DropColumn("dbo.IntegrationsLoadings", "ErrorDescr");
            DropColumn("dbo.IntegrationsLoadings", "UpdatingDescr");
            DropColumn("dbo.IntegrationsLoadings", "ValidationDescr");
            DropColumn("dbo.IntegrationsProducts", "Cornice");
        }
        
        public override void Down()
        {
            AddColumn("dbo.IntegrationsProducts", "Cornice", c => c.String());
            AddColumn("dbo.IntegrationsLoadings", "ValidationDescr", c => c.String());
            AddColumn("dbo.IntegrationsLoadings", "UpdatingDescr", c => c.String());
            AddColumn("dbo.IntegrationsLoadings", "ErrorDescr", c => c.String());
            DropForeignKey("dbo.IntegrationsLoadingsItems", "IntegrationLoadingId", "dbo.IntegrationsLoadings");
            DropIndex("dbo.IntegrationsLoadingsItems", new[] { "IntegrationLoadingId" });
            DropColumn("dbo.IntegrationsProducts", "MarketerVendorCode");
            DropColumn("dbo.IntegrationsProducts", "MarketerRange");
            DropColumn("dbo.IntegrationsLoadings", "Type");
            DropTable("dbo.IntegrationsLoadingsItems");
        }
    }
}
