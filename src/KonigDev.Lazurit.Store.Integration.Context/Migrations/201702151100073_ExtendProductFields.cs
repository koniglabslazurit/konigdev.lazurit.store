namespace KonigDev.Lazurit.Store.Integration.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExtendProductFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.IntegrationsProducts", "ProductId", c => c.Guid(nullable: false));
            AddColumn("dbo.IntegrationsProducts", "Series", c => c.String(nullable: false));
            CreateIndex("dbo.IntegrationsProducts", "ProductId", unique: true);
            DropColumn("dbo.IntegrationsProducts", "Seria");
        }
        
        public override void Down()
        {
            AddColumn("dbo.IntegrationsProducts", "Seria", c => c.String(nullable: false));
            DropIndex("dbo.IntegrationsProducts", new[] { "ProductId" });
            DropColumn("dbo.IntegrationsProducts", "Series");
            DropColumn("dbo.IntegrationsProducts", "ProductId");
        }
    }
}
