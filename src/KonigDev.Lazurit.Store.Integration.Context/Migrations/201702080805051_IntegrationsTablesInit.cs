namespace KonigDev.Lazurit.Store.Integration.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IntegrationsTablesInit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.IntegrationsLoadings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DateLoad = c.DateTime(nullable: false),
                        FileName = c.String(),
                        ErrorDescr = c.String(),
                        UpdatingDescr = c.String(),
                        ValidationDescr = c.String(),
                        Descr = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.IntegrationsCollections",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IntegrationSeriesId = c.Guid(nullable: false),
                        IntegrationRoomId = c.Guid(nullable: false),
                        OriginalId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IntegrationsRooms", t => t.IntegrationRoomId, cascadeDelete: true)
                .ForeignKey("dbo.IntegrationsSeries", t => t.IntegrationSeriesId, cascadeDelete: true)
                .Index(t => t.IntegrationSeriesId)
                .Index(t => t.IntegrationRoomId);
            
            CreateTable(
                "dbo.IntegrationsRooms",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SyncCode1C = c.String(nullable: false, maxLength: 50),
                        OriginalId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.SyncCode1C, unique: true);
            
            CreateTable(
                "dbo.IntegrationsSeries",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SyncCode1C = c.String(nullable: false, maxLength: 50),
                        OriginalId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.SyncCode1C, unique: true);
            
            CreateTable(
                "dbo.IntegrationsCollectionsVariants",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        OriginalId = c.Guid(),
                        IntegrationCollectionId = c.Guid(nullable: false),
                        Facing = c.String(nullable: false, maxLength: 50),
                        FacingColor = c.String(nullable: false, maxLength: 50),
                        FrameColor = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IntegrationsCollections", t => t.IntegrationCollectionId, cascadeDelete: true)
                .Index(t => t.IntegrationCollectionId)
                .Index(t => t.Facing)
                .Index(t => t.FacingColor)
                .Index(t => t.FrameColor);
            
            CreateTable(
                "dbo.IntegrationsProducts",
                c => new
                    {
                        SyncCode1C = c.String(nullable: false, maxLength: 50),
                        OriginalId = c.Guid(),
                        Hash = c.String(),
                        FurnitureType = c.String(nullable: false),
                        VendorCode = c.String(nullable: false),
                        RoomType = c.String(nullable: false),
                        Seria = c.String(nullable: false),
                        Collection = c.String(nullable: false),
                        VendorCodeList = c.String(),
                        FrameColor = c.String(nullable: false),
                        Facing = c.String(nullable: false),
                        FacingColor = c.String(),
                        Backlight = c.String(),
                        Material = c.String(),
                        LeftRight = c.String(),
                        FurnitureForm = c.String(),
                        ProductPartMarker = c.String(),
                        Height = c.String(),
                        Width = c.String(),
                        Length = c.String(),
                        Mechanism = c.String(),
                        PermissibleLoad = c.String(),
                        SleepingAreaWidth = c.String(),
                        SleepingAreaLength = c.String(),
                        Descr = c.String(),
                        Type = c.String(),
                        Style = c.String(),
                        Packing = c.String(),
                        Warranty = c.String(),
                    })
                .PrimaryKey(t => t.SyncCode1C);
            
            CreateTable(
                "dbo.IntegrationsProductPack",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IntegrationProductId = c.String(nullable: false, maxLength: 50),
                        Height = c.Int(nullable: false),
                        Width = c.Int(nullable: false),
                        Length = c.Int(nullable: false),
                        Code = c.String(),
                        Weight = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Amont = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IntegrationsProducts", t => t.IntegrationProductId, cascadeDelete: true)
                .Index(t => t.IntegrationProductId);
            
            CreateTable(
                "dbo.IntegrationsStatuses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        SortNumber = c.Short(nullable: false),
                        TechName = c.String(nullable: false, maxLength: 25),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.TechName, unique: true);
            
            CreateTable(
                "dbo.IntegrationsProductToIntegrationCollectionsVariants",
                c => new
                    {
                        IntegrationProductId = c.String(nullable: false, maxLength: 50),
                        IntegrationCollectionVariantId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.IntegrationProductId, t.IntegrationCollectionVariantId })
                .ForeignKey("dbo.IntegrationsProducts", t => t.IntegrationProductId, cascadeDelete: true)
                .ForeignKey("dbo.IntegrationsCollectionsVariants", t => t.IntegrationCollectionVariantId, cascadeDelete: true)
                .Index(t => t.IntegrationProductId)
                .Index(t => t.IntegrationCollectionVariantId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.IntegrationsProductPack", "IntegrationProductId", "dbo.IntegrationsProducts");
            DropForeignKey("dbo.IntegrationsProductToIntegrationCollectionsVariants", "IntegrationCollectionVariantId", "dbo.IntegrationsCollectionsVariants");
            DropForeignKey("dbo.IntegrationsProductToIntegrationCollectionsVariants", "IntegrationProductId", "dbo.IntegrationsProducts");
            DropForeignKey("dbo.IntegrationsCollectionsVariants", "IntegrationCollectionId", "dbo.IntegrationsCollections");
            DropForeignKey("dbo.IntegrationsCollections", "IntegrationSeriesId", "dbo.IntegrationsSeries");
            DropForeignKey("dbo.IntegrationsCollections", "IntegrationRoomId", "dbo.IntegrationsRooms");
            DropIndex("dbo.IntegrationsProductToIntegrationCollectionsVariants", new[] { "IntegrationCollectionVariantId" });
            DropIndex("dbo.IntegrationsProductToIntegrationCollectionsVariants", new[] { "IntegrationProductId" });
            DropIndex("dbo.IntegrationsStatuses", new[] { "TechName" });
            DropIndex("dbo.IntegrationsProductPack", new[] { "IntegrationProductId" });
            DropIndex("dbo.IntegrationsCollectionsVariants", new[] { "FrameColor" });
            DropIndex("dbo.IntegrationsCollectionsVariants", new[] { "FacingColor" });
            DropIndex("dbo.IntegrationsCollectionsVariants", new[] { "Facing" });
            DropIndex("dbo.IntegrationsCollectionsVariants", new[] { "IntegrationCollectionId" });
            DropIndex("dbo.IntegrationsSeries", new[] { "SyncCode1C" });
            DropIndex("dbo.IntegrationsRooms", new[] { "SyncCode1C" });
            DropIndex("dbo.IntegrationsCollections", new[] { "IntegrationRoomId" });
            DropIndex("dbo.IntegrationsCollections", new[] { "IntegrationSeriesId" });
            DropTable("dbo.IntegrationsProductToIntegrationCollectionsVariants");
            DropTable("dbo.IntegrationsStatuses");
            DropTable("dbo.IntegrationsProductPack");
            DropTable("dbo.IntegrationsProducts");
            DropTable("dbo.IntegrationsCollectionsVariants");
            DropTable("dbo.IntegrationsSeries");
            DropTable("dbo.IntegrationsRooms");
            DropTable("dbo.IntegrationsCollections");
            DropTable("dbo.IntegrationsLoadings");
        }
    }
}
