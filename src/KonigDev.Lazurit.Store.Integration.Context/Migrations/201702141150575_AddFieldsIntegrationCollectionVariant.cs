namespace KonigDev.Lazurit.Store.Integration.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFieldsIntegrationCollectionVariant : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.IntegrationsCollectionsVariants", "IsComplekt", c => c.Boolean(nullable: false));
            AddColumn("dbo.IntegrationsCollectionsVariants", "IsDefault", c => c.Boolean(nullable: false));
            AddColumn("dbo.IntegrationsProducts", "SyncCode1CСonformity", c => c.String());
            AddColumn("dbo.IntegrationsProducts", "IsDisassemblyState", c => c.Boolean(nullable: false));
            AlterColumn("dbo.IntegrationsProducts", "SleepingAreaAmount", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.IntegrationsProducts", "SleepingAreaAmount", c => c.String());
            DropColumn("dbo.IntegrationsProducts", "IsDisassemblyState");
            DropColumn("dbo.IntegrationsProducts", "SyncCode1CСonformity");
            DropColumn("dbo.IntegrationsCollectionsVariants", "IsDefault");
            DropColumn("dbo.IntegrationsCollectionsVariants", "IsComplekt");
        }
    }
}
