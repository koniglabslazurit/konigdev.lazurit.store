namespace KonigDev.Lazurit.Store.Integration.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExtendIntegrationProductTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.IntegrationsProducts", "Cornice", c => c.String());
            AddColumn("dbo.IntegrationsProducts", "RoomSize", c => c.String());
            AddColumn("dbo.IntegrationsProducts", "SleepingAreaAmount", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.IntegrationsProducts", "SleepingAreaAmount");
            DropColumn("dbo.IntegrationsProducts", "RoomSize");
            DropColumn("dbo.IntegrationsProducts", "Cornice");
        }
    }
}
