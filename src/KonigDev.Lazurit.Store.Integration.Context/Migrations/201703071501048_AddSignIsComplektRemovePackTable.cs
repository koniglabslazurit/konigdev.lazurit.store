namespace KonigDev.Lazurit.Store.Integration.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSignIsComplektRemovePackTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.IntegrationsProductPack", "IntegrationProductId", "dbo.IntegrationsProducts");
            DropIndex("dbo.IntegrationsProductPack", new[] { "IntegrationProductId" });
            AddColumn("dbo.IntegrationsProducts", "IsComplect", c => c.Boolean(nullable: false));
            DropTable("dbo.IntegrationsProductPack");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.IntegrationsProductPack",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IntegrationProductId = c.String(nullable: false, maxLength: 50),
                        Height = c.Int(nullable: false),
                        Width = c.Int(nullable: false),
                        Length = c.Int(nullable: false),
                        Code = c.String(),
                        Weight = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Amont = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.IntegrationsProducts", "IsComplect");
            CreateIndex("dbo.IntegrationsProductPack", "IntegrationProductId");
            AddForeignKey("dbo.IntegrationsProductPack", "IntegrationProductId", "dbo.IntegrationsProducts", "SyncCode1C", cascadeDelete: true);
        }
    }
}
