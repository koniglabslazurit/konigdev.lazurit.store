namespace KonigDev.Lazurit.Store.Integration.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditIntegrationLoadingItemTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.IntegrationsLoadingsItems", "Type", c => c.String(nullable: false, maxLength: 50));
            CreateIndex("dbo.IntegrationsLoadingsItems", "Type");
        }
        
        public override void Down()
        {
            DropIndex("dbo.IntegrationsLoadingsItems", new[] { "Type" });
            DropColumn("dbo.IntegrationsLoadingsItems", "Type");
        }
    }
}
