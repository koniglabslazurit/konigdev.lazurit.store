namespace KonigDev.Lazurit.Store.Integration.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIntegrationVariantContextProductAreaTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.IntegrationsCollectionsVariantsContentProductsAreas",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IsEnabled = c.Boolean(nullable: false),
                        Top = c.Int(nullable: false),
                        Left = c.Int(nullable: false),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                        IntegrationCollectionVariantId = c.Guid(nullable: false),
                        ProductId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.IntegrationsCollectionsVariantsContentProductsAreas");
        }
    }
}
