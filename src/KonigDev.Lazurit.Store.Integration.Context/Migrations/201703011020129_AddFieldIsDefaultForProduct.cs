namespace KonigDev.Lazurit.Store.Integration.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFieldIsDefaultForProduct : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.IntegrationsProducts", "IsDefault", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.IntegrationsProducts", "IsDefault");
        }
    }
}
