namespace KonigDev.Lazurit.Store.Integration.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IntegrationPrices : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.IntegrationsPrices",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PriceZoneId = c.Guid(nullable: false),
                        ProductSyncCode1C = c.String(),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.IntegrationsPrices");
        }
    }
}
