﻿namespace KonigDev.Lazurit.Store.Integration.Context.Migrations
{
    using Core.Enums.Enums;
    using Entities;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DataContext.LazuritIntegrationContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DataContext.LazuritIntegrationContext context)
        {
            if (!context.Set<IntegrationStatus>().Any())
            {
                var integrationStatuses = new List<IntegrationStatus>
                {
                    new IntegrationStatus { Id = Guid.NewGuid(), Name = "Опубликовано", TechName = EnumIntegrationStatus.Published.ToString(), SortNumber = (short)EnumIntegrationStatus.Published },
                    new IntegrationStatus { Id = Guid.NewGuid(), Name = "Черновкики", TechName = EnumIntegrationStatus.Template.ToString(), SortNumber = (short)EnumIntegrationStatus.Template },
                    new IntegrationStatus { Id = Guid.NewGuid(), Name = "Требуется валидации", TechName = EnumIntegrationStatus.Validation.ToString(), SortNumber = (short)EnumIntegrationStatus.Validation }
                };
                context.Set<IntegrationStatus>().AddRange(integrationStatuses);
                context.SaveChanges();
            }
        }
    }
}
