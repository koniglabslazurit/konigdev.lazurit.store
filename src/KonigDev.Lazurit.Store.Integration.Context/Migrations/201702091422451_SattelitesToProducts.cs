namespace KonigDev.Lazurit.Store.Integration.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SattelitesToProducts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.IntegrationsProductsProperties",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IntegrationProductId = c.String(nullable: false, maxLength: 50),
                        Value = c.String(),
                        SyncCode1C = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IntegrationsProducts", t => t.IntegrationProductId, cascadeDelete: true)
                .Index(t => t.IntegrationProductId);
            
            CreateTable(
                "dbo.IntegrationsProductsToStyles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IntegrationProductId = c.String(nullable: false, maxLength: 50),
                        Style = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IntegrationsProducts", t => t.IntegrationProductId, cascadeDelete: true)
                .Index(t => t.IntegrationProductId);
            
            CreateTable(
                "dbo.IntegrationsProductsToTargetAudiences",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IntegrationProductId = c.String(nullable: false, maxLength: 50),
                        TargetAudience = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IntegrationsProducts", t => t.IntegrationProductId, cascadeDelete: true)
                .Index(t => t.IntegrationProductId);
            
            AddColumn("dbo.IntegrationsProducts", "IntegrationStatusId", c => c.Guid(nullable: false));
            CreateIndex("dbo.IntegrationsProducts", "IntegrationStatusId");
            AddForeignKey("dbo.IntegrationsProducts", "IntegrationStatusId", "dbo.IntegrationsStatuses", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.IntegrationsProducts", "IntegrationStatusId", "dbo.IntegrationsStatuses");
            DropForeignKey("dbo.IntegrationsProductsToTargetAudiences", "IntegrationProductId", "dbo.IntegrationsProducts");
            DropForeignKey("dbo.IntegrationsProductsToStyles", "IntegrationProductId", "dbo.IntegrationsProducts");
            DropForeignKey("dbo.IntegrationsProductsProperties", "IntegrationProductId", "dbo.IntegrationsProducts");
            DropIndex("dbo.IntegrationsProductsToTargetAudiences", new[] { "IntegrationProductId" });
            DropIndex("dbo.IntegrationsProductsToStyles", new[] { "IntegrationProductId" });
            DropIndex("dbo.IntegrationsProductsProperties", new[] { "IntegrationProductId" });
            DropIndex("dbo.IntegrationsProducts", new[] { "IntegrationStatusId" });
            DropColumn("dbo.IntegrationsProducts", "IntegrationStatusId");
            DropTable("dbo.IntegrationsProductsToTargetAudiences");
            DropTable("dbo.IntegrationsProductsToStyles");
            DropTable("dbo.IntegrationsProductsProperties");
        }
    }
}
