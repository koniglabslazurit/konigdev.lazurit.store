namespace KonigDev.Lazurit.Store.Integration.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeProductParametersTypes : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.IntegrationsProducts", "Height", c => c.Int(nullable: false));
            AlterColumn("dbo.IntegrationsProducts", "Width", c => c.Int(nullable: false));
            AlterColumn("dbo.IntegrationsProducts", "Length", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.IntegrationsProducts", "Length", c => c.String());
            AlterColumn("dbo.IntegrationsProducts", "Width", c => c.String());
            AlterColumn("dbo.IntegrationsProducts", "Height", c => c.String());
        }
    }
}
