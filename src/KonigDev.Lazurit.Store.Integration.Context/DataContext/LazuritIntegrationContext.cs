﻿using KonigDev.Lazurit.Store.Integration.Context.Entities;
using KonigDev.Lazurit.Store.Integration.Context.Migrations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;

namespace KonigDev.Lazurit.Store.Integration.Context.DataContext
{
    public class LazuritIntegrationContext : DbContext
    {
        public LazuritIntegrationContext() : base("name=LazuritIntegrationContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<LazuritIntegrationContext, Configuration>("LazuritIntegrationContext"));
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            #region IntegrationLoading
            modelBuilder.Entity<IntegrationLoading>()
                .ToTable("IntegrationsLoadings")
                .HasKey(p => p.Id);

            modelBuilder.Entity<IntegrationLoading>()
                .Property(p => p.DateLoad)
                .IsRequired();

            modelBuilder.Entity<IntegrationLoading>()
                .Property(p => p.Type)
                .IsRequired();
            #endregion

            #region IntegrationLoadingItem
            modelBuilder.Entity<IntegrationLoadingItem>()
                .ToTable("IntegrationsLoadingsItems")
                .HasKey(p => p.Id);

            modelBuilder.Entity<IntegrationLoadingItem>()
                .HasRequired(p => p.IntegrationLoading)
                .WithMany(p => p.IntegrationLoadingItems)
                .HasForeignKey(p => p.IntegrationLoadingId);

            modelBuilder.Entity<IntegrationLoadingItem>()
                .Property(p => p.Type)
                .HasMaxLength(50)
                .IsRequired()
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = false })); ;

            #endregion

            #region IntegrationCollection

            modelBuilder.Entity<IntegrationCollection>()
                .ToTable("IntegrationsCollections")
                .HasKey(p => p.Id);

            modelBuilder.Entity<IntegrationCollection>()
                .HasRequired(p => p.IntegrationRoom)
                .WithMany()
                .HasForeignKey(p => p.IntegrationRoomId);

            modelBuilder.Entity<IntegrationCollection>()
                .HasRequired(p => p.IntegrationSeries)
                .WithMany()
                .HasForeignKey(p => p.IntegrationSeriesId);
            #endregion

            #region IntegrationCollectionVariant

            modelBuilder.Entity<IntegrationCollectionVariant>()
                .ToTable("IntegrationsCollectionsVariants")
                .HasKey(p => p.Id);

            modelBuilder.Entity<IntegrationCollectionVariant>()
                .HasRequired(p => p.IntegrationCollection)
                .WithMany(p => p.IntegrationCollectionVariants)
                .HasForeignKey(p => p.IntegrationCollectionId);

            modelBuilder.Entity<IntegrationCollectionVariant>()
                .Property(p => p.Facing)
                .HasMaxLength(50)
                .IsRequired()
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = false }));

            modelBuilder.Entity<IntegrationCollectionVariant>()
                .Property(p => p.FacingColor)
                .HasMaxLength(50)
                .IsRequired()
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = false }));

            modelBuilder.Entity<IntegrationCollectionVariant>()
                .Property(p => p.FrameColor)
                .HasMaxLength(50)
                .IsRequired()
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = false }));

            #endregion

            #region IntegrationProduct

            modelBuilder.Entity<IntegrationProduct>()
                .ToTable("IntegrationsProducts")
                .HasKey(p => p.SyncCode1C)
                .Property(p => p.SyncCode1C)
                .HasMaxLength(50);

            modelBuilder.Entity<IntegrationProduct>()
                .Property(p => p.FurnitureType)
                .IsRequired();

            modelBuilder.Entity<IntegrationProduct>()
                .Property(p => p.VendorCode)
                .IsRequired();

            modelBuilder.Entity<IntegrationProduct>()
                .Property(p => p.RoomType)
                .IsRequired();

            modelBuilder.Entity<IntegrationProduct>()
                .Property(p => p.Series)
                .IsRequired();

            modelBuilder.Entity<IntegrationProduct>()
                .Property(p => p.Collection)
                .IsRequired();

            modelBuilder.Entity<IntegrationProduct>()
                .Property(p => p.FrameColor)
                .IsRequired();

            modelBuilder.Entity<IntegrationProduct>()
                .Property(p => p.Facing)
                .IsRequired();

            modelBuilder.Entity<IntegrationProduct>()
                .Property(p => p.MarketerRange)
                .IsRequired();

            modelBuilder.Entity<IntegrationProduct>()
                .Property(p => p.MarketerVendorCode)
                .IsRequired();

            modelBuilder.Entity<IntegrationProduct>()
                .Property(p => p.ProductId)
                .IsRequired()
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = true }));

            modelBuilder.Entity<IntegrationProduct>()
                .HasMany(p => p.IntegrationCollectionsVariants)
                .WithMany(p => p.IntegrationProducts)
                .Map(p =>
                {
                    p.ToTable("IntegrationsProductToIntegrationCollectionsVariants");
                    p.MapLeftKey("IntegrationProductId");
                    p.MapRightKey("IntegrationCollectionVariantId");
                });

            modelBuilder.Entity<IntegrationProduct>()
                .HasRequired(p => p.IntegrationStatus)
                .WithMany()
                .HasForeignKey(p => p.IntegrationStatusId);

            modelBuilder.Entity<IntegrationProduct>()
                .HasRequired(p => p.IntegrationCollection)
                .WithMany(p=>p.IntegrationProducts)
                .HasForeignKey(p => p.CollectionId)
                .WillCascadeOnDelete(false);

            #endregion
            
            #region IntegrationRoom

            modelBuilder.Entity<IntegrationRoom>()
                .ToTable("IntegrationsRooms")
                .HasKey(p => p.Id)
                .Property(p => p.SyncCode1C)
                .IsRequired()
                .HasMaxLength(50)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = true }));

            #endregion

            #region IntegrationSeries

            modelBuilder.Entity<IntegrationSeries>()
                .ToTable("IntegrationsSeries")
                .HasKey(p => p.Id).Property(p => p.SyncCode1C)
                .IsRequired()
                .HasMaxLength(50)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = true }));

            #endregion

            #region IntegrationStatus

            modelBuilder.Entity<IntegrationStatus>()
                .ToTable("IntegrationsStatuses")
                .HasKey(p => p.Id);

            modelBuilder.Entity<IntegrationStatus>()
                .Property(p => p.TechName)
                .IsRequired()
                .HasMaxLength(25)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = true }));

            #endregion

            #region IntegrationProductProperty

            modelBuilder.Entity<IntegrationProductProperty>()
                .ToTable("IntegrationsProductsProperties")
                .HasKey(p => p.Id);

            modelBuilder.Entity<IntegrationProductProperty>()
                .HasRequired(p => p.IntegrationProduct)
                .WithMany(p => p.IntegrationProductProperties)
                .HasForeignKey(p => p.IntegrationProductId);

            #endregion

            #region IntegrationProductToStyle
            modelBuilder.Entity<IntegrationProductToStyle>()
                .ToTable("IntegrationsProductsToStyles")
                .HasKey(p => p.Id);

            modelBuilder.Entity<IntegrationProductToStyle>()
                .HasRequired(p => p.IntegrationProduct)
                .WithMany(p => p.IntegrationProductStyles)
                .HasForeignKey(p => p.IntegrationProductId);
            #endregion

            #region IntegrationCollectionVariantContextProductArea

            modelBuilder.Entity<IntegrationCollectionVariantContentProductArea>()
             .ToTable("IntegrationsCollectionsVariantsContentProductsAreas")
             .HasKey(p => p.Id);

            #endregion

            #region IntegrationProductToTargetAudience
            modelBuilder.Entity<IntegrationProductToTargetAudience>()
                .ToTable("IntegrationsProductsToTargetAudiences")
                .HasKey(p => p.Id);
            modelBuilder.Entity<IntegrationProductToTargetAudience>()
                .HasRequired(p => p.IntegrationProduct)
                .WithMany(p => p.IntegrationProductTargetAudiencies)
                .HasForeignKey(p => p.IntegrationProductId);

            #endregion

            #region IntegrationProductToTargetAudience

            modelBuilder.Entity<IntegrationPrice>()
                .ToTable("IntegrationsPrices")
                .HasKey(p => p.Id);
            #endregion
        }
    }
}
