﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.Integration.Context.Entities
{
    public class IntegrationLoading
    {
        public IntegrationLoading()
        {
            IntegrationLoadingItems = new HashSet<IntegrationLoadingItem>();
        }
        public Guid Id { get; set; }

        public DateTime DateLoad { get; set; }
        public string FileName { get; set; }
        public string Type { get; set; }
        public string Descr { get; set; }

        public virtual ICollection<IntegrationLoadingItem> IntegrationLoadingItems { get; set; }
    }
}