﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Integration.Context.Entities
{
    public class IntegrationCollectionVariantContentProductArea
    {
        public Guid Id { get; set; }
        public bool IsEnabled { get; set; }
        public int Top { get; set; }
        public int Left { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public Guid IntegrationCollectionVariantId { get; set; }
        public Guid ProductId { get; set; }
    }
}
