﻿using System;

namespace KonigDev.Lazurit.Store.Integration.Context.Entities
{
    public class IntegrationSeries
    {
        public Guid Id { get; set; }

        /// <summary>
        /// Уникальный код из БД 1С = наименование.
        /// </summary>
        public string SyncCode1C { get; set; }
        /// <summary>
        /// Ид оригинальноый записи, существующей в БД
        /// </summary>
        public Guid? OriginalId { get; set; }
    }
}