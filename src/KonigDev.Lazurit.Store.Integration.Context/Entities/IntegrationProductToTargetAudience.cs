﻿using System;

namespace KonigDev.Lazurit.Store.Integration.Context.Entities
{
    public class IntegrationProductToTargetAudience
    {
        public Guid Id { get; set; }
        public string IntegrationProductId { get; set; }
        public string TargetAudience { get; set; }

        public virtual IntegrationProduct IntegrationProduct { get; set; }
    }
}
