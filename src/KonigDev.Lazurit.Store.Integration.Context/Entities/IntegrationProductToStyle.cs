﻿using System;

namespace KonigDev.Lazurit.Store.Integration.Context.Entities
{
    public class IntegrationProductToStyle
    {
        public Guid Id { get; set; }
        public string IntegrationProductId { get; set; }
        public string Style { get; set; }

        public virtual IntegrationProduct IntegrationProduct { get; set; }
    }
}
