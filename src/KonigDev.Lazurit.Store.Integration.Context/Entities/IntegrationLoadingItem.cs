﻿using System;

namespace KonigDev.Lazurit.Store.Integration.Context.Entities
{
    public class IntegrationLoadingItem
    {
        public Guid Id { get; set; }
        public Guid IntegrationLoadingId { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }

        public virtual IntegrationLoading IntegrationLoading { get; set; }
    }
}