﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.Integration.Context.Entities
{
    public class IntegrationCollection
    {
        public IntegrationCollection()
        {
            IntegrationCollectionVariants = new HashSet<IntegrationCollectionVariant>();
            IntegrationProducts = new HashSet<IntegrationProduct>();
        }

        /// <summary>
        /// ИД
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Ссылка на темповую серию
        /// </summary>
        public Guid IntegrationSeriesId { get; set; }

        /// <summary>
        /// Ссылка на темповую комнату
        /// </summary>
        public Guid IntegrationRoomId { get; set; }

        /// <summary>
        /// Ид оригинальноый записи, существующей в БД
        /// </summary>
        public Guid? OriginalId { get; set; }

        public virtual IntegrationRoom IntegrationRoom { get; set; }
        public virtual IntegrationSeries IntegrationSeries { get; set; }

        public virtual ICollection<IntegrationCollectionVariant> IntegrationCollectionVariants { get; set; }
        public virtual ICollection<IntegrationProduct> IntegrationProducts { get; set; }
    }
}
