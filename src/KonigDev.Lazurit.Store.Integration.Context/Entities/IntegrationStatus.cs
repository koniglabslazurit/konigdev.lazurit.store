﻿using System;

namespace KonigDev.Lazurit.Store.Integration.Context.Entities
{
    public class IntegrationStatus
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public short SortNumber { get; set; }
        public string TechName { get; set; }
    }
}