﻿using System;

namespace KonigDev.Lazurit.Store.Integration.Context.Entities
{
    public class IntegrationProductProperty
    {
        public Guid Id { get; set; }
        public string IntegrationProductId { get; set; }
        public string Value { get; set; }
        public string SyncCode1C { get; set; }

        public virtual IntegrationProduct IntegrationProduct { get; set; }
    }
}
