﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.Integration.Context.Entities
{
    public class IntegrationCollectionVariant
    {

        public IntegrationCollectionVariant()
        {
            IntegrationProducts = new HashSet<IntegrationProduct>();
        }

        /// <summary>
        /// ИД
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Ид оригинальноый записи, существующей в БД
        /// </summary>
        public Guid? OriginalId { get; set; }

        /// <summary>
        /// Ссылка на темповую коллекцию
        /// </summary>
        public Guid IntegrationCollectionId { get; set; }

        /// <summary>
        /// Отделка, ВариантОтделкиИзделия
        /// </summary>
        public string Facing { get; set; }

        /// <summary>
        /// Цвет отделки, ЦветФасадаИзделия
        /// </summary>
        public string FacingColor { get; set; }

        /// <summary>
        /// Цвет корпуса, ЦветКорпуса
        /// </summary>
        public string FrameColor { get; set; }

        /// <summary>
        /// Признак комплетка у варианта коллекции
        /// </summary>
        public bool IsComplekt { get; set; }

        /// <summary>
        /// Признак дефолтного варианта коллекции
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// Код 1с для комплектов
        /// </summary>
        public string SyncCode1C { get; set; }

        /// <summary>
        /// Темповая коллекция
        /// </summary>
        public virtual IntegrationCollection IntegrationCollection { get; set; }

        public virtual ICollection<IntegrationProduct> IntegrationProducts { get; set; }        
    }
}
