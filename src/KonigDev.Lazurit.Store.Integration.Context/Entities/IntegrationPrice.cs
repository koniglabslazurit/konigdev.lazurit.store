﻿using System;

namespace KonigDev.Lazurit.Store.Integration.Context.Entities
{
    public class IntegrationPrice
    {
        public Guid Id { get; set; }
        public Guid PriceZoneId { get; set; }
        public string ProductSyncCode1C { get; set; }
        public decimal Value { get; set; }
    }
}
