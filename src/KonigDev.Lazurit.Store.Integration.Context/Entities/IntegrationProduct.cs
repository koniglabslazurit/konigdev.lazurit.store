﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.Integration.Context.Entities
{
    /// <summary>
    /// ИД выступает SyncCode1C
    /// </summary>
    public class IntegrationProduct
    {
        public IntegrationProduct()
        {
            IntegrationProductStyles = new HashSet<IntegrationProductToStyle>();
            IntegrationCollectionsVariants = new HashSet<IntegrationCollectionVariant>();
            IntegrationProductProperties = new HashSet<IntegrationProductProperty>();
            IntegrationProductTargetAudiencies = new HashSet<IntegrationProductToTargetAudience>();
        }

        /* ИД выступает SyncCode1C  */

        /// <summary>
        /// Ид оригинальноый записи, существующей в БД
        /// </summary>
        public Guid? OriginalId { get; set; }
        
        /// <summary>
        /// Идентификатор продукта. Записывается в таблицу продукта при публикации. 
        /// </summary>
        // todo поле необходимо для того, чтобы была возможность грузить изображения для товаров загруженных, и использовать его у реальных товаров. OriginalId для этого не подойдет
        public Guid ProductId { get; set; }

        /// <summary>
        /// Хэш функция, по которой идёт определение факта изменения продукта
        /// </summary>
        public string Hash { get; set; }

        /// <summary>
        /// ТипАртикула
        /// </summary>
        public string FurnitureType { get; set; }

        /// <summary>
        /// Артикул
        /// </summary>
        public string VendorCode { get; set; }
        /// <summary>
        /// ТипКомнаты
        /// </summary>
        public string RoomType { get; set; }

        //todo переименовать в series!!
        /// <summary>
        /// Серия
        /// </summary>
        public string Series { get; set; }
        /// <summary>
        /// Коллекция
        /// </summary>
        public string Collection { get; set; }
        /// <summary>
        /// СписокАртикуловКомплекта
        /// </summary>
        public string VendorCodeList { get; set; }
        /// <summary>
        /// Цвет корпуса, ЦветКорпуса
        /// </summary>
        public string FrameColor { get; set; }
        /// <summary>
        /// Отделка, ВариантОтделкиИзделия
        /// </summary>
        public string Facing { get; set; }
        /// <summary>
        /// Цвет отделки, ЦветФасадаИзделия
        /// </summary>
        public string FacingColor { get; set; }
        /// <summary>
        /// Код1с
        /// </summary>
        public string SyncCode1C { get; set; }
        /// <summary>
        /// Подсветка
        /// </summary>
        public string Backlight { get; set; }
        /// <summary>
        /// МатериалОбивки
        /// </summary>
        public string Material { get; set; }
        /// <summary>
        /// ЛевыйПравый
        /// </summary>
        public string LeftRight { get; set; }
        /// <summary>
        /// ФормаАртикула
        /// </summary>
        public string FurnitureForm { get; set; }
        /// <summary>
        /// НаличиеОбязательностьКарнизов
        /// </summary>
        public string ProductPartMarker { get; set; }
        /// <summary>
        /// Высота
        /// </summary>
        public int Height { get; set; }
        /// <summary>
        /// Ширина
        /// </summary>
        public int Width { get; set; }
        /// <summary>
        /// Длина
        /// </summary>
        public int Length { get; set; }
        /// <summary>
        /// Механизм
        /// </summary>
        public string Mechanism { get; set; }
        /// <summary>
        /// ДопустимаяНагрузкаНаКровать
        /// </summary>
        public string PermissibleLoad { get; set; }
        /// <summary>
        /// РазмерСпальногоМестаШирина
        /// </summary>
        public string SleepingAreaWidth { get; set; }
        /// <summary>
        /// РазмерСпальногоМестаДлина
        /// </summary>
        public string SleepingAreaLength { get; set; }
        /// <summary>
        /// МаркетинговоеОписание
        /// </summary>
        public string Descr { get; set; }
        /// <summary>
        /// ТипЦА
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// Стиль
        /// </summary>
        public string Style { get; set; }
        /// <summary>
        /// КолУпаковок
        /// </summary>
        public string Packing { get; set; }
        /// <summary>
        /// Гарантия
        /// </summary>
        public string Warranty { get; set; }       

        /// <summary>
        /// Размер комнаты
        /// </summary>
        public string RoomSize { get; set; }

        /// <summary>
        /// Количество спальных мест
        /// </summary>
        public short SleepingAreaAmount { get; set; }

        /// <summary>
        /// Код соответствия
        /// </summary>
        public string SyncCode1CСonformity { get; set; }

        /// <summary>
        /// Признак разобранного вида
        /// </summary>
        public bool IsDisassemblyState { get; set; }

        /// <summary>
        /// Является ли товар дефолтным в артикуле
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// МаркетинговыйВес
        /// </summary>        
        public string MarketerRange { get; set; }

        /// <summary>
        /// МаркетинговыйВес
        /// </summary>
        public string MarketerVendorCode { get; set; }

        /// <summary>
        /// Признак единичного товара как комплект
        /// </summary>
        public bool IsComplect { get; set; }

        public Guid IntegrationStatusId { get; set; }

        public Guid CollectionId { get; set; }

        public virtual IntegrationCollection IntegrationCollection { get; set; }
        public virtual IntegrationStatus IntegrationStatus { get; set; }
        public virtual ICollection<IntegrationCollectionVariant> IntegrationCollectionsVariants { get; set; }
        public virtual ICollection<IntegrationProductProperty> IntegrationProductProperties { get; set; }
        public virtual ICollection<IntegrationProductToTargetAudience> IntegrationProductTargetAudiencies { get; set; }
        public virtual ICollection<IntegrationProductToStyle> IntegrationProductStyles { get; set; }
    }
}