﻿using System;

namespace KonigDev.Lazurit.Store.Integration.Context.Entities
{
    public class IntegrationRoom
    {
        public Guid Id { get; set; }
        /// <summary>
        /// Код 1с = наименование 
        /// </summary>
        public string SyncCode1C { get; set; }
        /// <summary>
        /// Ид оригинальноый записи, существующей в БД
        /// </summary>
        public Guid? OriginalId { get; set; }
    }
}