﻿using KonigDev.Lazurit.Store.Integration.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Store.Integration.Contracts.Zones;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Queries;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Dto;

namespace KonigDev.Lazurit.Store.Integration.AnalysisServices
{
    public class AnalysisZoneService : IAnalysisZoneService
    {
        private readonly IHubHandlersFactory _hubFactory;

        public AnalysisZoneService(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        public async Task<List<IntegrationZoneContract>> AnalysisZone(List<Zone> zones)
        {
            var originalZones = await _hubFactory.CreateQueryHandler<GetPrizeZonesQuery, List<DtoPriceZoneItem>>().Execute(new GetPrizeZonesQuery());
            var result = new List<IntegrationZoneContract>();

            foreach (var item in zones)
            {
                var originalZone = originalZones.FirstOrDefault(p => p.SyncCode1C == item.Id);
                if (originalZone != null)
                    result.Add(new IntegrationZoneContract { IsNew = false, Id = originalZone.Id, SyncCode1C = originalZone.SyncCode1C });
                else result.Add(new IntegrationZoneContract { IsNew = true, Id = Guid.NewGuid(), SyncCode1C = item.Id });
            }
            return result;
        }
    }
}
