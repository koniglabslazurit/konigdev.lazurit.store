﻿using KonigDev.Lazurit.Store.Integration.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using KonigDev.Lazurit.Store.Integration.Contracts;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using System.Linq;
using KonigDev.Lazurit.Hub.Model.DTO.Styles.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Styles.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Query;
using KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Dto;

namespace KonigDev.Lazurit.Store.Integration.AnalysisServices
{
    public class AnalysisSatellite : IAnalysisSatellite
    {
        private readonly IHubHandlersFactory _hubFactory;

        public AnalysisSatellite(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }
        public async Task<List<IntegrationStyleContract>> AnalysisStyles(List<IntegrationStyleContract> styles)
        {
            var originalStyles = await _hubFactory.CreateQueryHandler<GetStylesQuery, List<DtoStyleItem>>().Execute(new GetStylesQuery());

            originalStyles = originalStyles.OrderBy(p => p.Name).ToList();
            styles = styles.OrderBy(p => p.SyncCode1C).ToList();

            foreach (var item in styles)
            {
                var styleOriginal = originalStyles.FirstOrDefault(p => p.SyncCode1C == item.SyncCode1C);
                if (styleOriginal != null)
                {
                    item.Id = styleOriginal.Id; item.IsNew = false;
                }
                else
                {
                    item.Id = Guid.NewGuid(); item.IsNew = true;
                }
            }
            return styles;
        }

        public async Task<List<IntegrationTargetAudienceContract>> AnalysisTargetAudiences(List<IntegrationTargetAudienceContract> targetAudiences)
        {
            var originalTargetAudience = await _hubFactory.CreateQueryHandler<GetTargetAudiencesQuery, List<DtoTargetAudienceItem>>().Execute(new GetTargetAudiencesQuery());
            foreach (var item in targetAudiences)
            {
                var targetAudienceOriginal = originalTargetAudience.FirstOrDefault(p => p.SyncCode1C == item.SyncCode1C);
                if (targetAudienceOriginal != null)
                {
                    item.Id = targetAudienceOriginal.Id; item.IsNew = false;
                }
                else
                {
                    item.Id = Guid.NewGuid(); item.IsNew = true;
                }
            }
            return targetAudiences;
        }
    }
}