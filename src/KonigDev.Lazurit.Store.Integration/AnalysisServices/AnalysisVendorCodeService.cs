﻿using KonigDev.Lazurit.Core.Enums.Enums;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Integration.Contracts;
using KonigDev.Lazurit.Store.Integration.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Integration.AnalysisServices
{
    [Serializable]
    public class AnalysisVendorCodeService : IAnalysisVendorCodeService
    {
        private readonly IHubHandlersFactory _hubFactory;

        public AnalysisVendorCodeService(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        public async Task<List<IntegrationVendorCodeContract>> AnalysisVendorCode(List<IntegrationVendorCodeContract> vendorCodes, List<IntegrationFurnitureContract> furnutireTypes)
        {
            var originalVendorCodes = await _hubFactory
                .CreateQueryHandler<GetIntegrationVendorCodeQuery, List<DtoIntegrationVendorCodeItem>>()
                .Execute(new GetIntegrationVendorCodeQuery());

            foreach (var item in vendorCodes)
            {
                var vendorCode = originalVendorCodes.FirstOrDefault(p => p.SyncCode1C == item.SyncCode1C);
                if (vendorCode != null)
                {
                    item.Id = vendorCode.Id;
                    item.IsNew = false;
                    if (item.RangeVendorCode != vendorCode.Range || item.FurnitureTypeId != vendorCode.FurnitureTypeId)
                        item.IsChanged = true;
                }
                else
                {
                    item.Id = Guid.NewGuid();
                    item.IsNew = true;
                }
            }
            return Map(vendorCodes, furnutireTypes);
        }

        private static List<IntegrationVendorCodeContract> Map(List<IntegrationVendorCodeContract> list, List<IntegrationFurnitureContract> furnutireTypes)
        {
            foreach (var item in list)
            {
                var furnitureType = furnutireTypes.FirstOrDefault(p => p.SyncCode1C == item.FurnitureTypeSyncCode1C);
                if (furnitureType != null)
                    item.FurnitureTypeId = furnitureType.Id;
                else
                {
                    item.IsNew = false;
                    item.IsChanged = false;
                }
            }
            return list;
        }
    }
}
