﻿using KonigDev.Lazurit.Core.Enums.Enums;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Facings.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Facings.Query;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Property.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Property.Query;
using KonigDev.Lazurit.Store.Integration.Contracts;
using KonigDev.Lazurit.Store.Integration.Interfaces;
using KonigDev.Lazurit.Store.Integration.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Integration.AnalysisServices
{
    //todo разнесит по разным сервисам?
    public class AnalysisProductsService : IAnalysisProductsService
    {
        private readonly IHubHandlersFactory _hubFactory;

        public AnalysisProductsService(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        public async Task<List<IntegrationProductContract>> AnalysisProducts(List<IntegrationProductContract> products, List<IntegrationCollectionContract> collections)
        {
            var result = await _hubFactory.CreateQueryHandler<GetIntegrationProductsQuery, List<DtoIntegrationProduct>>().Execute(new GetIntegrationProductsQuery());

            foreach (var item in products)
            {
                /* определение коллекции для продукта */
                item.CollectionId = collections.FirstOrDefault(p => p.Room == item.RoomType && p.Series == item.Series)?.Id ?? Guid.Empty;

                var originalItem = result.FirstOrDefault(p => p.SyncCode1C == item.SyncCode1C);
                if (originalItem == null) /* отсутсвуют в БД integration */
                    item.IntegrationStatus = EnumIntegrationProductStatus.Missing;
                else if (originalItem.Hash == item.Hash) /* не изменились и есть в БД integration */
                    item.IntegrationStatus = EnumIntegrationProductStatus.NotChanged;
                else if (originalItem.IntegrationStatus == EnumIntegrationStatus.Validation.ToString()) /* Были изменения по отношению с бд integration */
                    item.IntegrationStatus = EnumIntegrationProductStatus.Changed;
                else item.IntegrationStatus = EnumIntegrationProductStatus.Skip;
            }

            return products;
        }
        public async Task<List<IntegrationRoomContract>> AnalysisRooms(List<IntegrationRoomContract> rooms)
        {
            var originalRooms = await _hubFactory.CreateQueryHandler<GetIntegrationRoomsQuery, List<DtoIntegrationRoomItem>>().Execute(new GetIntegrationRoomsQuery());
            foreach (var item in rooms)
            {
                var room = originalRooms.FirstOrDefault(p => p.SyncCode1C == item.SyncCode1C);
                if (room != null)
                {
                    item.Id = room.Id; item.IsNew = false; item.OriginalId = room.OriginalId;
                }
                else
                {
                    item.IsNew = true; item.Id = Guid.NewGuid();
                }
            }
            return rooms;
        }

        public async Task<List<IntegrationCollectionContract>> AnalysisCollections(List<IntegrationCollectionContract> collections)
        {
            var originalCollections = await _hubFactory.CreateQueryHandler<GetIntegrationCollectionsQuery, List<DtoIntegrationCollectionItem>>().Execute(new GetIntegrationCollectionsQuery());
            foreach (var item in collections)
            {
                var collection = originalCollections.FirstOrDefault(p => p.Room == item.Room && p.Series == item.Series);
                if (collection != null)
                {
                    item.Id = collection.Id; item.IsNew = false; item.OriginalId = collection.OriginalId;
                }
                else
                {
                    item.Id = Guid.NewGuid(); item.IsNew = true;
                }
            }
            return collections;
        }


        public async Task<List<IntegrationCollectionVariantContract>> AnalysisCollectionVariant(List<IntegrationCollectionVariantContract> collectionsVariants)
        {
            /* todo вытягиваем варианты коллекции для анализа вариантов и комплектов как вариантов, подумать как объединить для оптимизации */
            var originalCollectionsVariants = await _hubFactory.CreateQueryHandler<GetIntegrationCollectionsVariantsQuery, List<DtoIntegrationCollectionVariantItem>>()
                .Execute(new GetIntegrationCollectionsVariantsQuery { IsComplekt = false });
            foreach (var item in collectionsVariants)
            {
                var collectionVariant = originalCollectionsVariants.FirstOrDefault(p => p.Facing == item.Facing && p.FacingColor == item.FacingColor && p.FrameColor == item.FrameColor);
                if (collectionVariant != null)
                {
                    item.Id = collectionVariant.Id;
                    item.IsNew = false;
                }
                else
                {
                    item.Id = Guid.NewGuid();
                    item.IsNew = true;
                }
            }
            return collectionsVariants;
        }

        public async Task<List<IntegrationCollectionComplektContract>> AnalysisCollectionComplekts(List<IntegrationCollectionComplektContract> collectionComplekts)
        {
            var originalCollectionsVariants = await _hubFactory.CreateQueryHandler<GetIntegrationCollectionsVariantsQuery, List<DtoIntegrationCollectionVariantItem>>()
                .Execute(new GetIntegrationCollectionsVariantsQuery { IsComplekt = true });
            foreach (var item in collectionComplekts)
            {
                var complekt = originalCollectionsVariants.FirstOrDefault(p => p.SyncCode1C == item.SyncCode1C);
                if (complekt != null)
                {
                    item.Id = complekt.Id;
                    item.IsNew = false;
                }
                else
                {
                    item.Id = Guid.NewGuid();
                    item.IsNew = true;
                }
            }
            return collectionComplekts;
        }

        public async Task<List<IntegrationSeriesContract>> AnalysisSeries(List<IntegrationSeriesContract> seriesList)
        {
            var originalSeries = await _hubFactory.CreateQueryHandler<GetIntegrationSeriesQuery, List<DtoIntegrationSeriesItem>>().Execute(new GetIntegrationSeriesQuery());
            foreach (var item in seriesList)
            {
                var series = originalSeries.FirstOrDefault(p => p.SyncCode1C == item.SyncCode1C);
                if (series != null)
                {
                    item.Id = series.Id; item.IsNew = false; item.OriginalId = series.Id;
                }
                else
                {
                    item.Id = Guid.NewGuid(); item.IsNew = true;
                }
            }
            return seriesList;
        }

        /* TODO если необходимо делать паренты, деалем их тут с обновлением */
        public async Task<List<IntegrationFurnitureContract>> AnalysisFurniture(List<IntegrationFurnitureContract> furnitureTypes)
        {
            var originalFurnitureTypes = await _hubFactory.CreateQueryHandler<GetIntegrationFurnitureTypeQuery, List<DtoIntegrationFurnitureTypeItem>>().Execute(new GetIntegrationFurnitureTypeQuery());
            foreach (var item in furnitureTypes)
            {
                var furnitureType = originalFurnitureTypes.FirstOrDefault(p => p.SyncCode1C == item.SyncCode1C);
                if (furnitureType != null)
                {
                    item.Id = furnitureType.Id;
                    item.IsNew = false;
                }
                else
                {
                    item.Id = Guid.NewGuid();
                    item.IsNew = true;
                }
            }

            /* анализ и проверка на родителя для типа мебели */
            foreach (var item in furnitureTypes.Where(f => !string.IsNullOrEmpty(f.Parent)).ToList())
            {
                var parent = furnitureTypes.FirstOrDefault(p => p.SyncCode1C == item.Parent);
                if (parent != null)
                    item.ParentId = parent.Id;
            }
            return furnitureTypes;
        }

        public async Task<List<IntegrationPropertiesContract>> AnalysisProperties(List<IntegrationPropertiesContract> properties)
        {
            var originalProperties = await _hubFactory.CreateQueryHandler<GetPropertiesQuery, List<DtoPropertyItem>>().Execute(new GetPropertiesQuery());
            foreach (var item in properties)
            {
                var property = originalProperties.FirstOrDefault(p => p.SyncCode1C == item.PropertyType.ToString() && p.Value == item.Value);
                if (property != null)
                {
                    item.IsNew = false; item.Id = property.Id; item.GroupeId = property.GroupeId;
                }
                else
                {
                    item.IsNew = true; item.Id = Guid.NewGuid();
                }
            }

            /* выделение групп у свойств для объединения */
            foreach (var item in properties)
            {
                if (item.GroupeId == null)
                {
                    var groupe = properties.FirstOrDefault(p => p.PropertyType == item.PropertyType && item.Value != p.Value);
                    if (groupe != null)
                    {
                        if (groupe.GroupeId.HasValue)
                            item.GroupeId = groupe.GroupeId;
                        else item.GroupeId = Guid.NewGuid();
                    }
                }
            }
            return properties;
        }

        public async Task<List<IntegrationFacingColorContract>> AnalysisFacingColors(List<IntegrationFacingColorContract> facingColors)
        {
            /* добавление в цвет отделки тех отделок, которые являются группирующими, если они отсутствуют в выгрузке смотри ColorMatrix */
            var matrix = new ColorMatrix();
            var missingFacingColors = matrix.GetMatrixColors().FacingColors.Where(p => !facingColors.Select(f => f.SyncCode1C).ToList().Contains(p.Name));
            foreach (var item in missingFacingColors)
            {
                facingColors.Add(new IntegrationFacingColorContract { SyncCode1C = item.Name, IsUniversal = item.Name.Contains(ImportConstants.MirrorName) });
            }

            var originalfacingColors = await _hubFactory.CreateQueryHandler<GetFacingColorQuery, List<DtoFacingColorItem>>().Execute(new GetFacingColorQuery());
            foreach (var item in facingColors)
            {
                var facingColor = originalfacingColors.FirstOrDefault(p => p.SyncCode1C == item.SyncCode1C);
                if (facingColor != null)
                {
                    item.Id = facingColor.Id; item.IsNew = false;
                }
                else
                {
                    item.Id = Guid.NewGuid();
                    item.IsNew = true;
                    if (item.SyncCode1C.Contains(ImportConstants.MirrorName))
                        item.IsUniversal = true;
                }
            }
            return facingColors;
        }

        public async Task<List<IntegrationFrameColorContract>> AnalysisFrameColors(List<IntegrationFrameColorContract> frameColorColors)
        {
            var originalFrameColors = await _hubFactory.CreateQueryHandler<GetFrameColorQuery, List<DtoFrameColorItem>>().Execute(new GetFrameColorQuery());

            var result = new List<IntegrationFrameColorContract>();
            foreach (var item in frameColorColors)
            {
                var frameColor = originalFrameColors.FirstOrDefault(p => p.SyncCode1C == item.SyncCode1C);
                if (frameColor != null)
                {
                    item.Id = frameColor.Id; item.IsNew = false;
                }
                else
                {
                    item.Id = Guid.NewGuid();
                    item.IsNew = true;
                    if (item.SyncCode1C.Contains(ImportConstants.MirrorName))
                        item.IsUniversal = true;
                }
            }
            return frameColorColors;
        }

        public async Task<List<IntegrationFacingContract>> AnalysisFacings(List<IntegrationFacingContract> facings)
        {
            /* добавление в отделку тех отделок, которые являются группирующими, если они отсутствуют в выгрузке. Смотри ColorMatrix */
            var matrix = new ColorMatrix();
            var missingFacings = matrix.GetMatrixColors().Facings.Where(p => !facings.Select(f => f.SyncCode1C).ToList().Contains(p.Name));
            foreach (var item in missingFacings)
            {
                facings.Add(new IntegrationFacingContract { SyncCode1C = item.Name });
            }

            var originalFacings = await _hubFactory.CreateQueryHandler<GetFacingsQuery, List<DtoFacingItem>>().Execute(new GetFacingsQuery());
            foreach (var item in facings)
            {
                var facing = originalFacings.FirstOrDefault(p => p.SyncCode1C == item.SyncCode1C);
                if (facing != null)
                {
                    item.Id = facing.Id; item.IsNew = false;
                }
                else
                {
                    item.Id = Guid.NewGuid(); item.IsNew = true;
                }
            }

            return facings;
        }

        public async Task<List<IntegrationProductPackContract>> AnalysisProductsPacks(List<IntegrationProductPackContract> productsPacks)
        {
            /* возвращаем тот же список. Анализ тут пока не требуется. Расширять по необходимости */
            return productsPacks;
        }

        /// <summary>
        /// проверка новых вариантов на условие: Для вариантов коллекций с типами комнат Спальня, Гостиная, Прихожая, Детская, Кабинет если в варианте коллекции отсутсвует хоть один продукт с формой "Шкаф", то вариант коллекции не формируется, игнорим его
        /// </summary>
        /// <param name="collectionsVariants"></param>
        /// <param name=""></param>
        /// <param name="products"></param>
        /// <returns></returns>
        public List<IntegrationCollectionVariantContract> ValidateCollectionVariantByProductForRoom(List<IntegrationCollectionVariantContract> collectionsVariants, List<IntegrationProductContract> products)
        {
            /* проверка новых вариантов на условие: 
             * Для вариантов коллекций с типами комнат Спальня, Гостиная, Прихожая, Детская, Кабинет если в варианте коллекции отсутсвует хоть один продукт с формой "Шкаф", то вариант коллекции не формируется, игнорим его */

            List<string> checkedRooms = new List<string> { "Спальня", "Гостиная", "Прихожая", "Детская", "Кабинет" };
            var count = collectionsVariants.Count - 1;
            for (int i = count; i >= 0; i--)
            {
                if (!checkedRooms.Contains(collectionsVariants[i].Room))
                    continue;

                var forDelete = false;

                if (collectionsVariants[i].Room == "Детская" && collectionsVariants[i].Series == "Тиана")
                {
                    var tiana = true;
                }

                var productsInVariant = products.Where(p => p.CollectionVariants.Contains(collectionsVariants[i].Id)).ToList();
                var cv = productsInVariant.SelectMany(p => p.CollectionVariants).Select(p => p).ToList();
                if (!productsInVariant.Any(p => p.FurnitureType == "Шкаф" || p.FurnitureForm == "Шкаф"))
                {
                    forDelete = true;
                }
                if (forDelete)
                    collectionsVariants.RemoveAt(i);
            }

            return collectionsVariants;
        }

        /// <summary>
        /// Соотношение продуктов с вариантам коллекций, существующих в БД
        /// </summary>
        /// <param name="products"></param>
        /// <returns></returns>
        public async Task<List<IntegrationProductContract>> RelateProductWithCollectionVariant(List<IntegrationProductContract> products)
        {
            var colorMatrix = new ColorMatrix();
            var variants = await _hubFactory.CreateQueryHandler<GetIntegrationCollectionsVariantsQuery, List<DtoIntegrationCollectionVariantItem>>().Execute(new GetIntegrationCollectionsVariantsQuery { IsComplekt = false });
            foreach (var item in products)
            {
                var frameColor = item.FrameColor;
                var facing = item.Facing;
                var facingColor = item.FacingColor;

                var facingMatrix = colorMatrix.GetFacingsMatrix(facing);
                if (facingMatrix != null)
                    facing = facingMatrix.Name;

                if (colorMatrix.IsFacingIsIgnored(facing))
                    facing = "";

                var facingColorMatrix = colorMatrix.GetFacingColorsMatrix(facingColor);
                if (facingColorMatrix != null)
                    facingColor = facingColorMatrix.Name;
                if (colorMatrix.IsFacingColorIsIgnored(facingColor))
                    facingColor = "";

                var variantsIds = new List<Guid>();

                /* если все цвета есть */
                if (!string.IsNullOrEmpty(facing) && !string.IsNullOrEmpty(facingColor))
                    variantsIds = variants.Where(v => v.Room == item.RoomType
                        && v.Series == item.Series
                        && v.FrameColor == frameColor
                        && v.Facing == facing
                        && v.FacingColor == facingColor)
                        .Select(v => v.Id)
                        .ToList();

                /* если нет отделки и цвета отделки, берем по цвету корпуса */
                if (string.IsNullOrEmpty(facing) && string.IsNullOrEmpty(facingColor))
                    variantsIds = variants.Where(v => v.Room == item.RoomType
                        && v.Series == item.Series
                        && v.FrameColor == frameColor)
                        .Select(v => v.Id)
                        .ToList();
                /* если нет отделки, берем по цвету корпуса и цвету отделки */
                else if (string.IsNullOrEmpty(facing))
                    variantsIds = variants
                        .Where(v => v.Room == item.RoomType
                        && v.Series == item.Series
                        && v.FrameColor == frameColor
                        && v.FacingColor == facingColor)
                        .Select(v => v.Id)
                        .ToList();
                /* если нет цвета отделки, берем по цвету корпуса и по отделке */
                else if (string.IsNullOrEmpty(facingColor))
                    variantsIds = variants
                        .Where(v => v.Room == item.RoomType
                        && v.Series == item.Series
                        && v.FrameColor == frameColor
                        && v.Facing == facing)
                        .Select(v => v.Id)
                        .ToList();

                /* если не найдено ни одного варианта, то тогда изделие идет в те варианты у которых совпадают цвета корпуса. */
                if (variantsIds.Count == 0)
                    variantsIds = variants.Where(v => v.FrameColor == item.FrameColor && v.Room == item.RoomType && v.Series == item.Series)
                        .Select(v => v.Id)
                        .ToList();

                item.CollectionVariants = variantsIds;
            }
            return products;
        }

        /* todo Подумать над оптимизаций в совокупности с методом RelateProductWithCollectionVariant!! */
        /// <summary>
        /// соотношение вариантов и продуктов. Сделано для того чтобы проанализировать варианты, до того как записать в бд.
        /// </summary>
        /// <param name="products"></param>
        /// <param name="variants"></param>
        /// <returns></returns>
        public List<IntegrationProductContract> RelateProductWithCollectionVariantByProducts(List<IntegrationProductContract> products, List<IntegrationCollectionVariantContract> variants)
        {
            var colorMatrix = new ColorMatrix();
            foreach (var item in products)
            {
                var frameColor = item.FrameColor;
                var facing = item.Facing;
                var facingColor = item.FacingColor;

                var facingMatrix = colorMatrix.GetFacingsMatrix(facing);
                if (facingMatrix != null)
                    facing = facingMatrix.Name;

                if (colorMatrix.IsFacingIsIgnored(facing))
                    facing = "";

                var facingColorMatrix = colorMatrix.GetFacingColorsMatrix(facingColor);
                if (facingColorMatrix != null)
                    facingColor = facingColorMatrix.Name;
                if (colorMatrix.IsFacingColorIsIgnored(facingColor))
                    facingColor = "";

                var variantsIds = new List<Guid>();

                /* если все цвета есть */
                if (!string.IsNullOrEmpty(facing) && !string.IsNullOrEmpty(facingColor))
                    variantsIds = variants.Where(v => v.Room == item.RoomType
                        && v.Series == item.Series
                        && v.FrameColor == frameColor
                        && v.Facing == facing
                        && v.FacingColor == facingColor)
                        .Select(v => v.Id)
                        .ToList();

                /* если нет отделки и цвета отделки, берем по цвету корпуса */
                if (string.IsNullOrEmpty(facing) && string.IsNullOrEmpty(facingColor))
                    variantsIds = variants.Where(v => v.Room == item.RoomType
                        && v.Series == item.Series
                        && v.FrameColor == frameColor)
                        .Select(v => v.Id)
                        .ToList();
                /* если нет отделки, берем по цвету корпуса и цвету отделки */
                else if (string.IsNullOrEmpty(facing))
                    variantsIds = variants
                        .Where(v => v.Room == item.RoomType
                        && v.Series == item.Series
                        && v.FrameColor == frameColor
                        && v.FacingColor == facingColor)
                        .Select(v => v.Id)
                        .ToList();
                /* если нет цвета отделки, берем по цвету корпуса и по отделке */
                else if (string.IsNullOrEmpty(facingColor))
                    variantsIds = variants
                        .Where(v => v.Room == item.RoomType
                        && v.Series == item.Series
                        && v.FrameColor == frameColor
                        && v.Facing == facing)
                        .Select(v => v.Id)
                        .ToList();

                /* если не найдено ни одного варианта, то тогда изделие идет в те варианты у которых совпадают цвета корпуса. */
                if (variantsIds.Count == 0)
                    variantsIds = variants.Where(v => v.FrameColor == item.FrameColor && v.Room == item.RoomType && v.Series == item.Series)
                        .Select(v => v.Id)
                        .ToList();

                item.CollectionVariants = variantsIds;
            }
            return products;
        }

        public async Task<List<IntegrationProductContract>> RelateProductWithCollectionComplekts(List<IntegrationProductContract> products, List<IntegrationCollectionComplektContract> complekts)
        {
            var variants = await _hubFactory.CreateQueryHandler<GetIntegrationCollectionsVariantsQuery, List<DtoIntegrationCollectionVariantItem>>().Execute(new GetIntegrationCollectionsVariantsQuery { IsComplekt = true });
            foreach (var item in products)
            {
                var complektsIds = complekts.Where(p => p.Products.Contains(item.SyncCode1C)).Select(p => p.Id).ToList();
                item.CollectionVariants.AddRange(complektsIds);
            }
            return products;
        }
    }
}