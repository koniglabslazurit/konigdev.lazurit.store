﻿using KonigDev.Lazurit.Store.Integration.Contracts;
using System.Collections.Generic;
using KonigDev.Lazurit.Store.Integration.Contracts.Zones;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;

namespace KonigDev.Lazurit.Store.Integration.Interfaces
{
    public interface IMappingService
    {
        IntegrationAnalysisContract MapProducts(List<ProductNode> source);
        List<DtoRegionUpdating> MapRegions(List<Department> departments, List<IntegrationZoneContract> zones);
        List<IntegrationZoneContract> MapPricesZones(List<Zone> zones);
        List<DtoProductPricesUpdating> MapProductPrices(List<IntegrationZoneContract> priceZones, List<Zone> zones);
    }
}
