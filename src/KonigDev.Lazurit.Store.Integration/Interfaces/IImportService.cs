﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Integration.Interfaces
{
    public interface IImportService
    {
        void ImportProducts(string path);

        void ImportPricesZones(string path);
    }
}
