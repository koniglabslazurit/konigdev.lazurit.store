﻿using KonigDev.Lazurit.Store.Integration.Contracts.Zones;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Integration.Interfaces
{
    /// <summary>
    /// Сервис анализа по всем данным для зон/регионов/ценам
    /// </summary>
    public interface IAnalysisZoneService
    {
        Task<List<IntegrationZoneContract>> AnalysisZone(List<Zone> zones);
    }
}
