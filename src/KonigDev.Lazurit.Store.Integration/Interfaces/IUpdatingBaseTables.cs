﻿using KonigDev.Lazurit.Store.Integration.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Integration.Interfaces
{
    public interface IUpdatingBaseTablesService
    {
        Task UpdateRooms(List<IntegrationRoomContract> list);
        Task UpdateSeries(List<IntegrationSeriesContract> list);
        Task UpdateCollections(List<IntegrationCollectionContract> list);
        Task UpdateVendorCodes(List<IntegrationVendorCodeContract> list);
        Task UpdateOrCreateProductPackages(List<IntegrationProductPackContract> list);
    }
}
