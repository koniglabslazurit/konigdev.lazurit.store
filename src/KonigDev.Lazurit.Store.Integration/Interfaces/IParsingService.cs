﻿using KonigDev.Lazurit.Store.Integration.Contracts;
using KonigDev.Lazurit.Store.Integration.Contracts.Zones;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.Integration.Interfaces
{
    public interface IParsingService
    {
        /// <summary>
        /// Парсинг продуктов
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        List<ProductNode> ParsingProducts(string fileName);

        /// <summary>
        /// Парсинг ценовыъ зон, цен, регионы, магазины
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        DataNode ParsingPrices(string fileName);
    }
}
