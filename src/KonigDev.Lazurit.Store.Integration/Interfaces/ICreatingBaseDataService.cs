﻿using KonigDev.Lazurit.Hub.Model.DTO.Regions.Dto;
using KonigDev.Lazurit.Store.Integration.Contracts;
using KonigDev.Lazurit.Store.Integration.Contracts.Zones;
using System.Collections.Generic;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;

namespace KonigDev.Lazurit.Store.Integration.Interfaces
{
    public interface ICreatingBaseDataService
    {
        Task CreateVendorCode(List<IntegrationVendorCodeContract> list);
        Task CreateOrUpdateFurnitureTypes(List<IntegrationFurnitureContract> list);
        Task CreateFacingColors(List<IntegrationFacingColorContract> list);
        Task CreateFrameColors(List<IntegrationFrameColorContract> list);
        Task CreateFacings(List<IntegrationFacingContract> list);
        Task CreateTargetAudiences(List<IntegrationTargetAudienceContract> list);
        Task CreateStyles(List<IntegrationStyleContract> list);
        Task CreateProperties(List<IntegrationPropertiesContract> properties);

        /// <summary>
        /// Обновление данныъ по регионам, зонам, городам, салонам
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        Task CreateRegions(List<DtoRegionUpdating> regions);

        /// <summary>
        /// Обновление/создание ценовых зон
        /// </summary>
        /// <param name="priceZones"></param>
        /// <returns></returns>
        Task CreatePriceZones(List<IntegrationZoneContract> priceZones);

        /// <summary>
        /// Обновление данных по ценам товаров
        /// </summary>
        /// <param name="productPrices"></param>
        /// <returns></returns>
        Task CreatePrices(List<DtoProductPricesUpdating> productPrices);
    }
}