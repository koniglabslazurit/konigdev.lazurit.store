﻿using KonigDev.Lazurit.Store.Integration.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Integration.Interfaces
{
    public interface IAnalysisProductsService
    {
        Task<List<IntegrationProductContract>> AnalysisProducts(List<IntegrationProductContract> productsNodes, List<IntegrationCollectionContract> collections);
        Task<List<IntegrationRoomContract>> AnalysisRooms(List<IntegrationRoomContract> products);
        Task<List<IntegrationCollectionContract>> AnalysisCollections(List<IntegrationCollectionContract> products);
        Task<List<IntegrationCollectionVariantContract>> AnalysisCollectionVariant(List<IntegrationCollectionVariantContract> products);
        Task<List<IntegrationSeriesContract>> AnalysisSeries(List<IntegrationSeriesContract> products);        
        Task<List<IntegrationFurnitureContract>> AnalysisFurniture(List<IntegrationFurnitureContract> products);
        Task<List<IntegrationPropertiesContract>> AnalysisProperties(List<IntegrationPropertiesContract> products);
        Task<List<IntegrationFacingColorContract>> AnalysisFacingColors(List<IntegrationFacingColorContract> products);
        Task<List<IntegrationFrameColorContract>> AnalysisFrameColors(List<IntegrationFrameColorContract> products);
        Task<List<IntegrationFacingContract>> AnalysisFacings(List<IntegrationFacingContract> products);
        Task<List<IntegrationProductPackContract>> AnalysisProductsPacks(List<IntegrationProductPackContract> productsPacks);

        
        // Вынести в другие сервисы        
        List<IntegrationCollectionVariantContract> ValidateCollectionVariantByProductForRoom(List<IntegrationCollectionVariantContract> collectionsVariants, List<IntegrationProductContract> products);
        List<IntegrationProductContract> RelateProductWithCollectionVariantByProducts(List<IntegrationProductContract> products, List<IntegrationCollectionVariantContract> variants);
        Task<List<IntegrationProductContract>> RelateProductWithCollectionVariant(List<IntegrationProductContract> products);
        Task<List<IntegrationCollectionComplektContract>> AnalysisCollectionComplekts(List<IntegrationCollectionComplektContract> collectionComplekts);
        Task<List<IntegrationProductContract>> RelateProductWithCollectionComplekts(List<IntegrationProductContract> products, List<IntegrationCollectionComplektContract> complekts);
    }
}
