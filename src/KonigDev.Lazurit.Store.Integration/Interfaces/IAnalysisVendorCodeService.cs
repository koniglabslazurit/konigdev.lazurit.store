﻿using KonigDev.Lazurit.Store.Integration.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Integration.Interfaces
{
    //todo Объединить с сервисом анализа IAnalysisProductsService
    public interface IAnalysisVendorCodeService
    {
        Task<List<IntegrationVendorCodeContract>> AnalysisVendorCode(List<IntegrationVendorCodeContract> products, List<IntegrationFurnitureContract> furnutireTypes);
    }
}
