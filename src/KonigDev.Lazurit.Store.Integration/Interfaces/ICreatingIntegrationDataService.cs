﻿using KonigDev.Lazurit.Store.Integration.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Integration.Interfaces
{
    public interface ICreatingIntegrationDataService
    {
        Task CreatingSeries(List<IntegrationSeriesContract> series);
        Task CreatingRooms(List<IntegrationRoomContract> list);
        Task CreatingCollections(List<IntegrationCollectionContract> list);
        Task CreatingCollectionVariants(List<IntegrationCollectionVariantContract> list);
        Task CreatingProducts(List<IntegrationProductContract> products);
        Task CreatingCollectionComplects(List<IntegrationCollectionComplektContract> collectionComplekts);
    }
}
