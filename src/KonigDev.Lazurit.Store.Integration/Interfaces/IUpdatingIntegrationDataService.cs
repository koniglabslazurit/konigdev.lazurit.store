﻿using KonigDev.Lazurit.Store.Integration.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Integration.Interfaces
{
    public interface IUpdatingIntegrationDataService
    {        
        Task UpdatingProducts(List<IntegrationProductContract> list);
    }
}
