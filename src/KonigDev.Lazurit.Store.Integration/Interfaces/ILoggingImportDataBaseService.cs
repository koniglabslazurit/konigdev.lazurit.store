﻿using KonigDev.Lazurit.Store.Integration.Contracts;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Integration.Interfaces
{
    public interface ILoggingImportDataBaseService
    {
        Task LoggingProductsImport(LoggingImportDataBaseContract contract,string fileName);
    }
}
