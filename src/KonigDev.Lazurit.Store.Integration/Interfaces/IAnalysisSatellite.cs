﻿using KonigDev.Lazurit.Store.Integration.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Integration.Interfaces
{
    public interface IAnalysisSatellite
    {
        Task<List<IntegrationStyleContract>> AnalysisStyles(List<IntegrationStyleContract> products);
        Task<List<IntegrationTargetAudienceContract>> AnalysisTargetAudiences(List<IntegrationTargetAudienceContract> products);
    }
}

