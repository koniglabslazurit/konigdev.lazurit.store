﻿using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Store.Integration.AnalysisServices;
using KonigDev.Lazurit.Store.Integration.CreatingServices;
using KonigDev.Lazurit.Store.Integration.CrudServices.UpdatingServices;
using KonigDev.Lazurit.Store.Integration.Factories;
using KonigDev.Lazurit.Store.Integration.Implemetations;
using KonigDev.Lazurit.Store.Integration.Interfaces;
using KonigDev.Lazurit.Store.Integration.UpdatingServices;
using Ninject.Modules;

namespace KonigDev.Lazurit.Store.Integration.NinjectModules
{
    public class IntegrationModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ITransactionFactory>().To<TransactionFactory>();
            Bind<IHubHandlersFactory>().To<NinjectGrainsFactory>();

            Bind<IParsingService>().To<ParsingService>();
            Bind<IImportService>().To<ImportService>();
            Bind<IMappingService>().To<MappingService>();
            Bind<ILoggingImportDataBaseService>().To<LoggingImportDataBaseService>();

            Bind<IAnalysisProductsService>().To<AnalysisProductsService>();
            Bind<IAnalysisSatellite>().To<AnalysisSatellite>();
            Bind<IAnalysisZoneService>().To<AnalysisZoneService>();
            Bind<IAnalysisVendorCodeService>().To<AnalysisVendorCodeService>();

            Bind<ICreatingIntegrationDataService>().To<CreatingIntegrationDataService>();
            Bind<IUpdatingIntegrationDataService>().To<UpdatingIntegrationDataService>();
            
            Bind<ICreatingBaseDataService>().To<CreatingBaseDataService>();
            Bind<IUpdatingBaseTablesService>().To<UpdatingBaseTablesService>();

        }
    }
}