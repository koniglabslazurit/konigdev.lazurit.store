﻿using KonigDev.Lazurit.Store.Integration.Contracts;
using KonigDev.Lazurit.Store.Integration.Contracts.AnalisysContracts;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace KonigDev.Lazurit.Store.Integration.Services
{
    public static class InfrastructureServices
    {
        public static string CalculateMD5Hash(string input)
        {
            MD5 md5 = MD5.Create();
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        public static IntegrationProductContract ConvertToContract(ProductNode node, ProductConvertContract productConvertContract)
        {
            var result = new IntegrationProductContract
            {
                Backlight = node.Backlight,
                Collection = node.Collection,
                Descr = node.Descr,
                Facing = node.Facing,
                FacingColor = node.FacingColor,
                FrameColor = node.FrameColor,
                FurnitureForm = node.FurnitureForm,
                FurnitureType = node.FurnitureType,
                LeftRight = node.LeftRight,
                VendorCodeList = node.VendorCodeList,
                Material = node.Material,
                Mechanism = node.Mechanism,
                Packing = node.Packing,
                PermissibleLoad = node.PermissibleLoad,
                ProductPartMarker = node.ProductPartMarker,
                RoomType = node.Room,
                Series = node.Series,
                SleepingAreaLength = node.SleepingAreaLength,
                SleepingAreaWidth = node.SleepingAreaWidth,
                Style = node.Style,
                SyncCode1C = node.SyncCode1C,
                TargetAudience = node.Type,
                VendorCode = node.VendorCode,
                Warranty = node.Warranty,
                MarketerRange = node.MarketerRange,
                MarketerVendorCode = node.MarketerVendorCode,
                IsComplect = productConvertContract.IsComplekt,
                /*  */
                Height = productConvertContract.Height,
                Length = productConvertContract.Length,
                Width = productConvertContract.Width,
                RangeTotal = productConvertContract.RangeTotal,
                RangeVendorCode = productConvertContract.RangeVendorCode
            };

            return result;
        }
        public static List<IntegrationProductPackContract> ConvertProductPackToContract(List<ProductPackNode> productPacks, string productSyncCode)
        {
            var result = new List<IntegrationProductPackContract>();
            foreach (var item in productPacks)
            {
                int height = 0;
                int length = 0;
                int width = 0;
                decimal weight = 0M;
                int amont = 0;

                if (!string.IsNullOrWhiteSpace(item.Height))
                {
                    int h = 0;
                    if (int.TryParse(item.Height.Replace(" ", ""), out h))
                        height = h;
                    //else result.Errors.Add(new IntegrationErrorsContract { Error = $"Ошибка конвертации поля Height: '{item.Height}'. ТипАртикула: '{item.FurnitureType}'; Артикул: '{item.VendorCode}'; ТипКомнаты: '{item.Room}'; Серия: '{item.Series}';" });
                }

                if (!string.IsNullOrWhiteSpace(item.Length))
                {
                    int l = 0;
                    if (int.TryParse(item.Length.Replace(" ", ""), out l))
                        length = l;
                    //else result.Errors.Add(new IntegrationErrorsContract { Error = $"Ошибка конвертации поля Length: '{item.Length}'. ТипАртикула: '{item.FurnitureType}'; Артикул: '{item.VendorCode}'; ТипКомнаты: '{item.Room}'; Серия: '{item.Series}';" });
                }

                if (!string.IsNullOrWhiteSpace(item.Width))
                {
                    int w = 0;
                    if (int.TryParse(item.Width.Replace(" ", ""), out w))
                        width = w;
                    //else result.Errors.Add(new IntegrationErrorsContract { Error = $"Ошибка конвертации поля Width: '{item.Width}'. ТипАртикула: '{item.FurnitureType}'; Артикул: '{item.VendorCode}'; ТипКомнаты: '{item.Room}'; Серия: '{item.Series}';" });
                }

                if (!string.IsNullOrWhiteSpace(item.Weight))
                {
                    decimal w = 0;
                    if (decimal.TryParse(item.Weight.Replace(" ", "").Replace(",", "."), out w))
                        weight = w;
                }

                if (!string.IsNullOrWhiteSpace(item.Weight))
                {
                    int a = 0;
                    if (int.TryParse(item.Amont.Replace(" ", ""), out a))
                        amont = a;
                }

                result.Add(new IntegrationProductPackContract
                {
                    ProductSyncCode = productSyncCode,
                    Code = item.Code,
                    Height = height,
                    Length = length,
                    Width = width,
                    Weight = weight,
                    Amont = amont
                });
            }
            return result;
        }
    }
}