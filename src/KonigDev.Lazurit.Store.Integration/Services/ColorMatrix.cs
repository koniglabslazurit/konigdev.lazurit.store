﻿using System.Collections.Generic;
using System.Linq;

namespace KonigDev.Lazurit.Store.Integration.Services
{
    /// <summary>
    /// Матрица цветов для формирвоания вариантов коллекции
    /// </summary>
    public class ColorMatrix
    {
        private Matrix MatrixColors { get; set; }

        public ColorMatrix()
        {
            var result = new Matrix();
            result.Facings.AddRange(new[] { FacingBambuk, FacingAkrilMatovyj, FacingGljanec, FacingRattan, FacingShpon, FacingTekstil, FacingZerkalo, FacingSteklo });
            result.FacingColors.Add(new ColorFilterMatrix { Name = "артемида", Childs = new List<string> { "орех артемида" } });
            MatrixColors = result;
        }

        /// <summary>
        /// Проверка вхождения отделки в соответствие по матрице
        /// </summary>
        /// <param name="facingName"></param>
        /// <returns></returns>
        public ColorFilterMatrix GetFacingsMatrix(string facingName)
        {
            return MatrixColors.Facings.Where(p => p.Name.ToLower() == facingName.ToLower() || p.Childs.Any(c => c.ToLower() == facingName.ToLower())).FirstOrDefault();
        }

        /// <summary>
        /// Поверка вхождения цвета отделки в соответсвие по матрице
        /// </summary>
        /// <param name="facingColorName"></param>
        /// <returns></returns>
        public ColorFilterMatrix GetFacingColorsMatrix(string facingColorName)
        {
            return MatrixColors.FacingColors.Where(p => p.Name.ToLower() == facingColorName.ToLower() || p.Childs.Any(c => c.ToLower() == facingColorName.ToLower())).FirstOrDefault();
        }

        public Matrix GetMatrixColors()
        {
            return MatrixColors;
        }

        public bool IsFacingColorIsIgnored(string facingColor)
        {
            var ignoredFacingColors = new List<string> { "зеркало" };
            return ignoredFacingColors.Contains(facingColor.ToLower());
        }

        public bool IsFacingIsIgnored(string facing)
        {
            var ignoredFacings = new List<string> { "экокожа", "кожа", "экокожа ткань" };
            return ignoredFacings.Contains(facing);
        }

        /// <summary>
        /// Бамбук
        /// </summary>
        private static ColorFilterMatrix FacingBambuk
        {
            get
            {
                return new ColorFilterMatrix
                {
                    Name = "бамбук",
                    Childs = new List<string> { "бамбук б", "бамбук б2", "бамбук б1", "бамбук с пультом", "бамбук б зеркало", "шпон бамбук", "мдф бамбук", "бамбук стекло", "мдф глянцевая бамбук", "шкаф-купе бамбук с 3 зерк", "шкаф-купе бамбук с 1 зерк" }
                };
            }
        }

        /// <summary>
        /// Акрил матовый
        /// </summary>
        private static ColorFilterMatrix FacingAkrilMatovyj
        {
            get
            {
                return new ColorFilterMatrix
                {
                    Name = "акрил матовый",
                    Childs = new List<string> { "акрил матовый акрил матов", "шпон акрил матовый", "акрил матовый акрил гляне", "акрил матовый", "акрил матовый тёмное зерк" }
                };
            }
        }

        /// <summary>
        /// Глянец
        /// </summary>
        private static ColorFilterMatrix FacingGljanec
        {
            get
            {
                return new ColorFilterMatrix
                {
                    Name = "глянец",
                    Childs = new List<string> { "акрил глянец акрил глянец", "акрил глянец", "шпон / глянец", "акрил глянец акрил глянец", "акрил матовый акрил гляне", "мдф глянцевая бамбук", "мдф глянцевая стекло", "мдф глянцевая", "мдф глянцевая с пультом", "акрил глянец тёмное зерка", "мдф глянцевая шпон", "акрил глянец тёмное зерка" }
                };
            }
        }
        /// <summary>
        /// раттан
        /// </summary>
        private static ColorFilterMatrix FacingRattan
        {
            get
            {
                return new ColorFilterMatrix
                {
                    Name = "раттан",
                    Childs = new List<string> { "раттан", "раттан стекло" }
                };
            }
        }

        /// <summary>
        /// Шпон
        /// </summary>
        private static ColorFilterMatrix FacingShpon
        {
            get
            {
                return new ColorFilterMatrix
                {
                    Name = "шпон",
                    Childs = new List<string> { "шпон / глянец", "шпон акрил матовый", "шпон", "шпон шпон", "шпон тёмное зеркало", "шпон / зеркало", "шпон бамбук", "мдф глянцевая шпон" }
                };
            }
        }

        /// <summary>
        /// Текстиль
        /// </summary>
        private static ColorFilterMatrix FacingTekstil
        {
            get
            {
                return new ColorFilterMatrix
                {
                    Name = "текстиль",
                    Childs = new List<string> { "текстиль", "микровелюр" }
                };
            }
        }

        /// <summary>
        /// Зеркало
        /// </summary>
        private static ColorFilterMatrix FacingZerkalo
        {
            get
            {
                return new ColorFilterMatrix
                {
                    Name = "зеркало",
                    Childs = new List<string> { "зеркало с фацетом", "зеркало с пультом", "мдф зеркало", "зеркало в алюм. рамке", "зеркало", "тёмное зеркало в алюм. ра", "акрил глянец зеркало", "акрил матовый тёмное зерк", "акрил глянец тёмное зерка", "бамбук б зеркало", "шкаф-купе бамбук с 3 зерк", "шкаф-купе бамбук с 1 зерк", "дсп зеркало" }
                };
            }
        }

        /// <summary>
        /// Стекло
        /// </summary>
        private static ColorFilterMatrix FacingSteklo
        {
            get
            {
                return new ColorFilterMatrix
                {
                    Name = "стекло",
                    Childs = new List<string> { "дсп стекло", "мдф глянцевая стекло", "раттан стекло", }
                };
            }
        }
    }

    public class Matrix
    {
        public Matrix()
        {
            Facings = new List<ColorFilterMatrix>();
            FacingColors = new List<ColorFilterMatrix>();
        }

        public List<ColorFilterMatrix> Facings { get; set; }
        public List<ColorFilterMatrix> FacingColors { get; set; }
    }

    public class ColorFilterMatrix
    {
        public string Name { get; set; }
        public List<string> Childs { get; set; }
    }



}
