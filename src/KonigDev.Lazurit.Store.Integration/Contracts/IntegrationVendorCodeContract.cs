﻿using System;

namespace KonigDev.Lazurit.Store.Integration.Contracts
{
    public class IntegrationVendorCodeContract
    {
        public Guid Id { get; set; }
        public bool IsNew { get; set; }
        public string SyncCode1C { get; set; }
        public string FurnitureTypeSyncCode1C { get; set; }
        public string RangeVendorCode { get; set; }
        public Guid FurnitureTypeId { get; set; }
        public bool IsChanged { get; set; }
    }
}
