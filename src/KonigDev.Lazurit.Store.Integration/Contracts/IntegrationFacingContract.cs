﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Integration.Contracts
{
    [Serializable]
    public class IntegrationFacingContract
    {
        public Guid Id { get; internal set; }
        public bool IsNew { get; set; }
        public string SyncCode1C { get; set; }
    }
}
