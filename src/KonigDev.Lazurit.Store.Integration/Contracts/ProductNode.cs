﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace KonigDev.Lazurit.Store.Integration.Contracts
{
    [Serializable]
    public class ProductNode
    {
        /// <summary>
        /// ТипАртикула
        /// </summary>
        [XmlAttribute(AttributeName = "ТипАртикула")]
        public string FurnitureType { get; set; }
        /// <summary>
        /// Артикул
        /// </summary>
        [XmlAttribute(AttributeName = "Артикул")]
        public string VendorCode { get; set; }

        /// <summary>
        /// ТипКомнаты
        /// </summary>
        [XmlAttribute(AttributeName = "ТипКомнаты")]
        public string Room { get; set; }

        /// <summary>
        /// МаркетинговыйВес
        /// </summary>
        [XmlAttribute(AttributeName = "МаркетинговыйВес")]
        public string MarketerRange { get; set; }

        /// <summary>
        /// МаркетинговыйАртикул
        /// </summary>
        [XmlAttribute(AttributeName = "МаркетинговыйАртикул")]
        public string MarketerVendorCode { get; set; }

        /// <summary>
        /// Серия
        /// </summary>
        [XmlAttribute(AttributeName = "Серия")]
        public string Series { get; set; }

        /// <summary>
        /// Коллекция
        /// </summary>
        [XmlAttribute(AttributeName = "Коллекция")]
        public string Collection { get; set; }

        /// <summary>
        /// СписокАртикуловКомплекта
        /// </summary>
        [XmlAttribute(AttributeName = "СписокАртикуловКомплекта")]
        public string VendorCodeList { get; set; }

        /// <summary>
        /// Цвет корпуса, ЦветКорпуса
        /// </summary>
        [XmlAttribute(AttributeName = "ЦветКорпуса")]
        public string FrameColor { get; set; }

        /// <summary>
        /// Отделка, ВариантОтделкиИзделия
        /// </summary>
        [XmlAttribute(AttributeName = "ВариантОтделкиИзделия")]
        public string Facing { get; set; }

        /// <summary>
        /// Цвет отделки, ЦветФасадаИзделия
        /// </summary>
        [XmlAttribute(AttributeName = "ЦветФасадаИзделия")]
        public string FacingColor { get; set; }

        /// <summary>
        /// Код1с
        /// </summary>
        [XmlAttribute(AttributeName = "Код1с")]
        public string SyncCode1C { get; set; }

        /// <summary>
        /// Подсветка
        /// </summary>
        [XmlAttribute(AttributeName = "Подсветка")]
        public string Backlight { get; set; }

        /// <summary>
        /// МатериалОбивки
        /// </summary>
        [XmlAttribute(AttributeName = "МатериалОбивки")]
        public string Material { get; set; }

        /// <summary>
        /// ЛевыйПравый
        /// </summary>
        [XmlAttribute(AttributeName = "ЛевыйПравый")]
        public string LeftRight { get; set; }

        /// <summary>
        /// ФормаАртикула
        /// </summary>
        [XmlAttribute(AttributeName = "ФормаАртикула")]
        public string FurnitureForm { get; set; }

        /// <summary>
        /// НаличиеОбязательностьКарнизов
        /// </summary>
        [XmlAttribute(AttributeName = "НаличиеОбязательностьКарнизов")]
        public string ProductPartMarker { get; set; }

        /// <summary>
        /// Высота
        /// </summary>
        [XmlAttribute(AttributeName = "Высота")]
        public string Height { get; set; }

        /// <summary>
        /// Ширина
        /// </summary>
        [XmlAttribute(AttributeName = "Ширина")]
        public string Width { get; set; }

        /// <summary>
        /// Длина
        /// </summary>
        [XmlAttribute(AttributeName = "Длина")]
        public string Length { get; set; }

        /// <summary>
        /// Механизм
        /// </summary>
        [XmlAttribute(AttributeName = "Механизм")]
        public string Mechanism { get; set; }

        /// <summary>
        /// ДопустимаяНагрузкаНаКровать
        /// </summary>
        [XmlAttribute(AttributeName = "ДопустимаяНагрузкаНаКровать")]
        public string PermissibleLoad { get; set; }

        /// <summary>
        /// РазмерСпальногоМестаШирина
        /// </summary>
        [XmlAttribute(AttributeName = "РазмерСпальногоМестаШирина")]
        public string SleepingAreaWidth { get; set; }

        /// <summary>
        /// РазмерСпальногоМестаДлина
        /// </summary>
        [XmlAttribute(AttributeName = "РазмерСпальногоМестаДлина")]
        public string SleepingAreaLength { get; set; }

        /// <summary>
        /// МаркетинговоеОписание
        /// </summary>
        [XmlAttribute(AttributeName = "МаркетинговоеОписание")]
        public string Descr { get; set; }

        /// <summary>
        /// ТипЦА
        /// </summary>
        [XmlAttribute(AttributeName = "ТипЦА")]
        public string Type { get; set; }

        /// <summary>
        /// Стиль
        /// </summary>
        [XmlAttribute(AttributeName = "Стиль")]
        public string Style { get; set; }

        /// <summary>
        /// КолУпаковок
        /// </summary>
        [XmlAttribute(AttributeName = "КолУпаковок")]
        public string Packing { get; set; }

        /// <summary>
        /// Гарантия
        /// </summary>
        [XmlAttribute(AttributeName = "Гарантия")]
        public string Warranty { get; set; }

        /// <summary>
        /// Упаковки продукта, Упаковка
        /// </summary>

        [XmlElement("Упаковка")]
        public List<ProductPackNode> ProductPacks { get; set; }

        [XmlIgnore]
        public bool IsValid
        {
            get
            {
                return !string.IsNullOrWhiteSpace(FurnitureType)
                    && !string.IsNullOrWhiteSpace(VendorCode)
                    && !string.IsNullOrWhiteSpace(Room)
                    && !string.IsNullOrWhiteSpace(Series)
                    && !string.IsNullOrWhiteSpace(Collection)
                    && !string.IsNullOrWhiteSpace(FrameColor)
                    && !string.IsNullOrWhiteSpace(Facing)
                    && ValidCollection();
            }
        }
        
        private bool ValidCollection()
        {
            if (string.IsNullOrWhiteSpace(Series) || string.IsNullOrWhiteSpace(Room) || string.IsNullOrWhiteSpace(Series))
                return false;
            return Collection.Contains(Series) && Collection.Contains(Room);
        }
    }
}
