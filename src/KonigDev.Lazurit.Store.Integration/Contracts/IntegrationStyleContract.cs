﻿using System;

namespace KonigDev.Lazurit.Store.Integration.Contracts
{
    [Serializable]
    public class IntegrationStyleContract
    {
        public Guid Id { get; set; }
        public bool IsNew { get; internal set; }
        public string SyncCode1C { get; set; }
    }
}
