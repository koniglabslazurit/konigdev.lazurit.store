﻿using System;
using System.Xml.Serialization;

namespace KonigDev.Lazurit.Store.Integration.Contracts
{
    [Serializable]
    public class ProductPackNode
    {
        /// <summary>
        /// Высота
        /// </summary>
        [XmlAttribute(AttributeName = "Высота")]
        public string Height { get; set; }

        /// <summary>
        /// Ширина
        /// </summary>
        [XmlAttribute(AttributeName = "Ширина")]
        public string Width { get; set; }

        /// <summary>
        /// Длина
        /// </summary>
        [XmlAttribute(AttributeName = "Длина")]
        public string Length { get; set; }

        /// <summary>
        /// Код
        /// </summary>
        [XmlAttribute(AttributeName = "Код")]
        public string Code { get; set; }

        /// <summary>
        /// Вес
        /// </summary>
        [XmlAttribute(AttributeName = "Вес")]
        public string Weight { get; set; }

        /// <summary>
        /// Количество упаковок
        /// </summary>
        [XmlAttribute(AttributeName = "КолУпаковок")]
        public string Amont { get; set; }
    }
}
