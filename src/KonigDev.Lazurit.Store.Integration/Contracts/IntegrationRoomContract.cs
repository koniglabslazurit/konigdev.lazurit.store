﻿using System;

namespace KonigDev.Lazurit.Store.Integration.Contracts
{
    public class IntegrationRoomContract
    {
        public Guid Id { get; set; }
        public string SyncCode1C { get; set; }

        /// <summary>
        /// Признак того, что комната присутсвует в интеграционных таблицах
        /// </summary>
        public bool IsNew { get; set; }

        /// <summary>
        /// Ид оригинальной записи в основных таблицах
        /// </summary>
        public Guid? OriginalId { get; set; }
        public string RangeRoom { get; set; }
    }
}