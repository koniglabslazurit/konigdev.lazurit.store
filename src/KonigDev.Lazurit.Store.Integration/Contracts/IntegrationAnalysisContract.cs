﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.Integration.Contracts
{
    [Serializable]
    public class IntegrationAnalysisContract
    {
        public IntegrationAnalysisContract()
        {
            Collections = new List<IntegrationCollectionContract>();
            VendorCodes = new List<IntegrationVendorCodeContract>();
            TargetAudiencies = new List<IntegrationTargetAudienceContract>();
            Styles = new List<IntegrationStyleContract>();
            Series = new List<IntegrationSeriesContract>();
            Rooms = new List<IntegrationRoomContract>();
            Properties = new List<IntegrationPropertiesContract>();
            Furnitures = new List<IntegrationFurnitureContract>();
            FrameColors = new List<IntegrationFrameColorContract>();
            Facings = new List<IntegrationFacingContract>();
            FacingColors = new List<IntegrationFacingColorContract>();
            CollectionVariants = new List<IntegrationCollectionVariantContract>();
            Errors = new List<IntegrationErrorsContract>();
            Products = new List<IntegrationProductContract>();
            ProductsPacks = new List<IntegrationProductPackContract>();
            CollectionComplekts = new List<IntegrationCollectionComplektContract>();
        }

        public List<IntegrationCollectionContract> Collections { get; set; }
        public List<IntegrationVendorCodeContract> VendorCodes { get; set; }
        public List<IntegrationTargetAudienceContract> TargetAudiencies { get; set; }
        public List<IntegrationStyleContract> Styles { get; set; }
        public List<IntegrationSeriesContract> Series { get; set; }
        public List<IntegrationRoomContract> Rooms { get; set; }
        public List<IntegrationPropertiesContract> Properties { get; set; }
        public List<IntegrationProductContract> Products { get; set; }
        public List<IntegrationFurnitureContract> Furnitures { get; set; }
        public List<IntegrationFrameColorContract> FrameColors { get; set; }
        public List<IntegrationFacingContract> Facings { get; set; }
        public List<IntegrationFacingColorContract> FacingColors { get; set; }
        public List<IntegrationCollectionVariantContract> CollectionVariants { get; set; }
        public List<IntegrationCollectionComplektContract> CollectionComplekts { get; set; }
        public List<IntegrationErrorsContract> Errors { get; set; }
        public List<IntegrationProductPackContract> ProductsPacks { get; set; }
    }
}
