﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.Integration.Contracts.AnalisysContracts
{
    [Serializable]
    public class ProductConvertContract
    {        
        public int Height { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public string RangeTotal { get; set; }
        public string RangeVendorCode { get; set; }

        /// <summary>
        /// Признак единичного товара как комплекта.
        /// </summary>
        public bool IsComplekt { get; set; }
    }
}
