﻿using System;

namespace KonigDev.Lazurit.Store.Integration.Contracts
{
    [Serializable]
    public class IntegrationFurnitureContract
    {
        public Guid Id { get;  set; }
        public bool IsNew { get; set; }
        public string SyncCode1C { get; set; }
        public string Parent { get; set; }
        public Guid? ParentId { get; set; }
    }
}
