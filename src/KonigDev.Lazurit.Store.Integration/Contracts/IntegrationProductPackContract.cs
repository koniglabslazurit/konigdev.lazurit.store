﻿namespace KonigDev.Lazurit.Store.Integration.Contracts
{
    public class IntegrationProductPackContract
    {
        public string ProductSyncCode { get; set; }

        public int Height { get; set; }

        /// <summary>
        /// Ширина
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Длина
        /// </summary>        
        public int Length { get; set; }

        /// <summary>
        /// Код
        /// </summary>        
        public string Code { get; set; }

        /// <summary>
        /// Вес
        /// </summary>        
        public decimal Weight { get; set; }

        /// <summary>
        /// Количество упаковок
        /// </summary>  
        public int Amont { get; set; }

    }
}
