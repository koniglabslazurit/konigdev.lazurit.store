﻿using System;

namespace KonigDev.Lazurit.Store.Integration.Contracts
{
    public class IntegrationSeriesContract
    {
        public Guid Id { get; set; }
        public bool IsNew { get; set; }
        public Guid? OriginalId { get; set; }
        public string RangeSeries { get; set; }
        public string SyncCode1C { get; set; }
    }
}
