﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Integration.Contracts
{
    /// <summary>
    /// Целевая аудитория
    /// </summary>
    [Serializable]
    public class IntegrationTargetAudienceContract
    {
        public Guid Id { get; internal set; }
        public bool IsNew { get; internal set; }
        public string SyncCode1C { get; set; }
    }
}
