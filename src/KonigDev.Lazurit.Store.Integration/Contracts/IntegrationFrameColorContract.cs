﻿using System;

namespace KonigDev.Lazurit.Store.Integration.Contracts
{
    [Serializable]
    public class IntegrationFrameColorContract
    {
        public Guid Id { get; set; }
        public bool IsNew { get; set; }
        public bool IsUniversal { get; internal set; }
        public string SyncCode1C { get; set; }
    }
}
