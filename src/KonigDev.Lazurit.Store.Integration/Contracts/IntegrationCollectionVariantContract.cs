﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.Integration.Contracts
{
    public class IntegrationCollectionVariantContract
    {

        public IntegrationCollectionVariantContract()
        {
            FacingChilds = new List<string>();
            FacingColorsChilds = new List<string>();
        }

        public Guid Id { get; set; }
        public string Room { get; set; }
        public string Series { get; set; }
        public string Facing { get; set; }
        public string FacingColor { get; set; }
        public string FrameColor { get; set; }
        public bool IsNew { get; set; }
        public bool IsComplekt { get; set; }

        public List<string> FacingChilds { get; set; }
        public List<string> FacingColorsChilds { get; set; }
    }
}
