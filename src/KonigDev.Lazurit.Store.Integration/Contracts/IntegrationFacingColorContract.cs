﻿using System;

namespace KonigDev.Lazurit.Store.Integration.Contracts
{
    [Serializable]
    public class IntegrationFacingColorContract
    {
        public Guid Id { get; set; }
        public bool IsNew { get; set; }
        public bool IsUniversal { get; set; }
        public string SyncCode1C { get; set; }
    }
}
