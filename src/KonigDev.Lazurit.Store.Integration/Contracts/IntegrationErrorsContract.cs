﻿using System;

namespace KonigDev.Lazurit.Store.Integration.Contracts
{
    [Serializable]
    public class IntegrationErrorsContract
    {
        public string Error { get; set; }
    }
}
