﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Integration.Contracts
{
    public class IntegrationCollectionContract
    {
        public Guid Id { get; set; }
        public bool IsNew { get; set; }
        public Guid? OriginalId { get; set; }
        public string RangeCollection { get; set; }
        public string Room { get; set; }
        public string Series { get; set; }
    }

    internal class CollectionItem
    {
        public string RoomType { get; set; }
        public string Series { get; set; }
    }

    internal class CollectionVariantItem
    {
        public string FrameColor { get; set; }
        public string Facing { get; set; }
        public string FacingColor { get; set; }
        public string Room { get; set; }
        public string Series { get; set; }
    }
}
