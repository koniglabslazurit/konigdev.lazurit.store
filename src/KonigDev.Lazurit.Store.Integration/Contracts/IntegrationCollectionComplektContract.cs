﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.Integration.Contracts
{
    [Serializable]
    public class IntegrationCollectionComplektContract
    {
        public string Facing { get; set; }
        public string FacingColor { get; set; }
        public string FrameColor { get; set; }
        public Guid Id { get; set; }
        public bool IsNew { get; set; }
        public List<string> Products { get; set; }
        public string Room { get; set; }
        public string Series { get; set; }
        public string SyncCode1C { get; set; }
    }
}