﻿using System;
using KonigDev.Lazurit.Core.Enums.Enums;

namespace KonigDev.Lazurit.Store.Integration.Contracts
{
    public class IntegrationPropertiesContract
    {
        public string Value { get; set; }
        public EnumPropertiesType PropertyType { get; set; }
        public bool IsNew { get; set; }
        public Guid Id { get; set; }
        public Guid? GroupeId { get; set; }
        public bool IsMultiValue { get; set; }

        public string Title
        {
            get
            {
                /* TODO в константы? вынести от сюда? */
                if (PropertyType == EnumPropertiesType.Backlight)
                    return "Подсветка";
                if (PropertyType == EnumPropertiesType.FurnitureForm)
                    return "Форма артикула";
                if (PropertyType == EnumPropertiesType.LeftRight)
                    return "Левый Правый";
                if (PropertyType == EnumPropertiesType.Material)
                    return "Материал обивки";
                if (PropertyType == EnumPropertiesType.Mechanism)
                    return "Механизм";
                if (PropertyType == EnumPropertiesType.ProductPartMarker)
                    return "Наличие карнизов";
                return "";
            }
        }
    }
}
