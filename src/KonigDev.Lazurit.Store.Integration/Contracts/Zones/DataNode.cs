﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace KonigDev.Lazurit.Store.Integration.Contracts.Zones
{
    [Serializable]
    public class DataNode
    {
        public DataNode()
        {
            Zones = new List<Zone>();
            Departments = new List<Department>();
        }

        public List<Zone> Zones { get; set; }
        public List<Department> Departments { get; set; }
    }

    [XmlType("Costs")]
    public class Zone
    {
        public Zone()
        {
            Costs = new List<Cost>();
        }

        [XmlAttribute(AttributeName = "ID")]
        public string Id { get; set; }

        [XmlElement("Cost")]
        public List<Cost> Costs { get; set; }
    }

    [XmlType("Cost")]
    public class Cost
    {
        [XmlAttribute(AttributeName = "ID")]
        public string Product { get; set; }

        [XmlAttribute(AttributeName = "v")]
        public string Value { get; set; }
    }


    [XmlType("Department")]
    public class Department
    {
        public Department()
        {
            Offices = new List<Office>();
        }

        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }

        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = "web")]
        public string WebAddress { get; set; }

        [XmlElement("Office")]
        public List<Office> Offices { get; set; }
    }

    [XmlType("Office")]
    public class Office
    {
        public Office()
        {
            Shops = new List<Shop>();
        }

        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }

        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = "costs")]
        public string PriceZone { get; set; }
                
        [XmlElement("Shop")]
        public List<Shop> Shops { get; set; }
    }   
    
    public class Shop
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }

        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = "address")]
        public string Address { get; set; }

        [XmlAttribute(AttributeName = "phone")]
        public string Phones { get; set; }

        [XmlAttribute(AttributeName = "mail")]
        public string Emails { get; set; }

        /// <summary>
        /// Геокоординаты магазина. Широта и долгота в одном поле, разделенные запятой. Первое значение - широта
        /// </summary>
        [XmlAttribute(AttributeName = "geo")]
        public string GeoCoordinats { get; set; }
    }
}
