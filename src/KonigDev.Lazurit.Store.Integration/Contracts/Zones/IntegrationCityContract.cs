﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.Integration.Contracts.Zones
{
    [Serializable]
    public class IntegrationCityContract
    {
        public IntegrationCityContract()
        {
            Showrooms = new List<IntegrationShowroomContract>();
        }

        public Guid Id { get; set; }
        public bool IsNew { get; set; }
        public Guid PriceZoneId { get; set; }        
        public string SyncCode1C { get; set; }
        public string Title { get; set; }

        public List<IntegrationShowroomContract> Showrooms { get; set; }
    }
}
