﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Integration.Contracts.Zones
{
    [Serializable]
    public class IntegrationShowroomContract
    {
        public string Address { get; set; }
        public string Emails { get; set; }
        public Guid Id { get; set; }
        public bool IsNew { get; set; }
        public string Phones { get; set; }
        public string SyncCode1C { get; set; }
        public string Title { get; set; }
    }
}
