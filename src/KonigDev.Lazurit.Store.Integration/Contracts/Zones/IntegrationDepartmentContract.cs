﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Store.Integration.Contracts.Zones
{
    [Serializable]
    public class IntegrationRegionContract
    {
        public IntegrationRegionContract()
        {
            Cities = new List<IntegrationCityContract>();
        }

        public List<IntegrationCityContract> Cities { get; internal set; }
        public Guid Id { get; set; }
        public bool IsNew { get; set; }
        public string SyncCode1C { get; set; }
        public string Title { get; set; }
    }
}
