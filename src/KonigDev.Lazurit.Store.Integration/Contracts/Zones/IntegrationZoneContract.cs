﻿using System;

namespace KonigDev.Lazurit.Store.Integration.Contracts.Zones
{
    [Serializable]
    public class IntegrationZoneContract
    {
        public Guid Id { get; set; }
        public bool IsNew { get; set; }
        public string SyncCode1C { get; set; }
    }
}
