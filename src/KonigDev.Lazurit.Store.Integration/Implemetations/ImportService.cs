﻿using KonigDev.Lazurit.Core.Services;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;
using KonigDev.Lazurit.Store.Integration.Contracts;
using KonigDev.Lazurit.Store.Integration.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KonigDev.Lazurit.Store.Integration.Implemetations
{
    public class ImportService : IImportService
    {
        private readonly IParsingService _parsingService;
        private readonly IAnalysisProductsService _analysisProductsService;
        private readonly IAnalysisVendorCodeService _vendorCodeAnalysisService;
        private readonly ICreatingBaseDataService _creatingBaseDataService;
        private readonly IAnalysisSatellite _analysisSatellite;
        private readonly ICreatingIntegrationDataService _updatingIntegrationTablesService;
        private readonly IMappingService _mappingService;
        private readonly IAnalysisZoneService _analysisZoneService;
        private readonly IUpdatingBaseTablesService _updatingBaseTables;
        private readonly ILoggingImportDataBaseService _loggingImportDataBaseService;
        private readonly IUpdatingIntegrationDataService _updatingIntegrationDataService;
        private readonly ILoggingService _loggingService;

        public ImportService(IParsingService parsingService,
            IAnalysisProductsService analysisService,
            IAnalysisVendorCodeService vendorCodeAnalysisService,
            IAnalysisSatellite analysisSatellite,
            ICreatingBaseDataService creatingBaseDataService,
            ICreatingIntegrationDataService updatingIntegrationTablesService,
            IMappingService mappingService,
            IAnalysisZoneService analysisZoneService,
            IUpdatingBaseTablesService updatingBaseTables,
            ILoggingImportDataBaseService loggingImportDataBaseService,
            IUpdatingIntegrationDataService updatingIntegrationDataService,
            ILoggingService loggingService)
        {
            _parsingService = parsingService;
            _analysisProductsService = analysisService;
            _vendorCodeAnalysisService = vendorCodeAnalysisService;
            _creatingBaseDataService = creatingBaseDataService;
            _analysisSatellite = analysisSatellite;
            _updatingIntegrationTablesService = updatingIntegrationTablesService;
            _mappingService = mappingService;
            _analysisZoneService = analysisZoneService;
            _updatingBaseTables = updatingBaseTables;
            _loggingImportDataBaseService = loggingImportDataBaseService;
            _updatingIntegrationDataService = updatingIntegrationDataService;
            _loggingService = loggingService;
        }

        public async void ImportProducts(string path)
        {
            var timeStart = DateTime.Now;

            /* парсинг xml */
            _loggingService.InfoMessage($"IMPORT_PRODUCTS: Start parsing file {path}");
            var parsingResult = _parsingService.ParsingProducts(path);

            _loggingService.InfoMessage($"IMPORT_PRODUCTS: End parsing file {path}");
            /* мапинг в объект с нужными сущностями */
            _loggingService.InfoMessage($"IMPORT_PRODUCTS: Start mapping data");
            var integrationModel = _mappingService.MapProducts(parsingResult);
            parsingResult.Clear();
            _loggingService.InfoMessage($"IMPORT_PRODUCTS: End mapping data");
            /* анализ интеграционных данных */
            /* это все можно запускать параллельно */
            _loggingService.InfoMessage($"IMPORT_PRODUCTS: Start analysis data");

            var series = await _analysisProductsService.AnalysisSeries(integrationModel.Series);
            var properties = await _analysisProductsService.AnalysisProperties(integrationModel.Properties);
            var collections = await _analysisProductsService.AnalysisCollections(integrationModel.Collections);
            var collectionsVariants = await _analysisProductsService.AnalysisCollectionVariant(integrationModel.CollectionVariants);

            var collectionComplekts = await _analysisProductsService.AnalysisCollectionComplekts(integrationModel.CollectionComplekts);
            /* анализ продуктов */
            var products = await _analysisProductsService.AnalysisProducts(integrationModel.Products, collections); /* todo передаем коллекции для сопоставления продуктов с ними */

            /* TODO!!! посмотреть в сторону оптимизации */
            /* сопоставление продуктов и вариантов */
            products = _analysisProductsService.RelateProductWithCollectionVariantByProducts(products, collectionsVariants);
            /* вызов валидации вариантов коллекции после сопоставления, проверка на наличие необходимых изделий в определенных типах комнат */
            collectionsVariants = _analysisProductsService.ValidateCollectionVariantByProductForRoom(collectionsVariants, products);
            /* пересопоставляем продукты с вариантами после валидации вариантов. Подумать, как лучше сделать */
            products = _analysisProductsService.RelateProductWithCollectionVariantByProducts(products, collectionsVariants);

            var rooms = await _analysisProductsService.AnalysisRooms(integrationModel.Rooms);
            var productPacks = await _analysisProductsService.AnalysisProductsPacks(integrationModel.ProductsPacks);

            /* анализ основных данных */
            var frameColors = await _analysisProductsService.AnalysisFrameColors(integrationModel.FrameColors);
            var facingColors = await _analysisProductsService.AnalysisFacingColors(integrationModel.FacingColors);
            var facings = await _analysisProductsService.AnalysisFacings(integrationModel.Facings);
            var styles = await _analysisSatellite.AnalysisStyles(integrationModel.Styles);
            var targetAudiences = await _analysisSatellite.AnalysisTargetAudiences(integrationModel.TargetAudiencies);
            /* тут зависимость */
            var furnitureTypes = await _analysisProductsService.AnalysisFurniture(integrationModel.Furnitures);
            var vendorCodes = await _vendorCodeAnalysisService.AnalysisVendorCode(integrationModel.VendorCodes, furnitureTypes);

            /* Запись в бд результатов анализа */
            await _loggingImportDataBaseService.LoggingProductsImport(new LoggingImportDataBaseContract
            {
                Collections = collections,
                CollectionVariants = collectionsVariants,
                Errors = integrationModel.Errors,
                FacingColors = facingColors,
                Facings = facings,
                FrameColors = frameColors,
                Furnitures = furnitureTypes,
                Products = products,
                Properties = properties,
                Rooms = rooms,
                Series = series,
                Styles = styles,
                TargetAudiencies = targetAudiences,
                VendorCodes = vendorCodes
            }, path);
            _loggingService.InfoMessage($"IMPORT_PRODUCTS: End analysis data");

            integrationModel = null;

            _loggingService.InfoMessage($"IMPORT_PRODUCTS: Start write data to dase DB");
            /* Запись в БД в оригинальные таблицы то что можем записать. Можем запускать параллельно */
            await _creatingBaseDataService.CreateFacingColors(facingColors);
            await _creatingBaseDataService.CreateFacings(facings);
            await _creatingBaseDataService.CreateFrameColors(frameColors);
            await _creatingBaseDataService.CreateOrUpdateFurnitureTypes(furnitureTypes);
            await _creatingBaseDataService.CreateVendorCode(vendorCodes);
            await _creatingBaseDataService.CreateTargetAudiences(targetAudiences);
            await _creatingBaseDataService.CreateStyles(styles);
            await _creatingBaseDataService.CreateProperties(properties);
            _loggingService.InfoMessage($"IMPORT_PRODUCTS: End write data to dase DB");

            /* Запись в интеграционные таблицы все что было пропарсено */
            _loggingService.InfoMessage($"IMPORT_PRODUCTS: Start write data to integration DB");
            #region это можно запускать параллельно
            _loggingService.InfoMessage($"IMPORT_PRODUCTS: CreatingRooms");
            await _updatingIntegrationTablesService.CreatingRooms(rooms);

            _loggingService.InfoMessage($"IMPORT_PRODUCTS: CreatingSeries");
            await _updatingIntegrationTablesService.CreatingSeries(series);
            #endregion

            #region это зависимые, запускаем один после другого, после румов и серий
            _loggingService.InfoMessage($"IMPORT_PRODUCTS: CreatingCollections");
            await _updatingIntegrationTablesService.CreatingCollections(collections);

            _loggingService.InfoMessage($"IMPORT_PRODUCTS: CreatingCollectionVariants");
            await _updatingIntegrationTablesService.CreatingCollectionVariants(collectionsVariants);

            _loggingService.InfoMessage($"IMPORT_PRODUCTS: CreatingCollectionComplects");
            await _updatingIntegrationTablesService.CreatingCollectionComplects(collectionComplekts);

            /* после записи вариантов необходимо сопоставить продукты с вариантами котрые мы смогли записать */
            products = await _analysisProductsService.RelateProductWithCollectionVariant(products);
            /* сопоставление продктов с вариантами коллекции, которые являются вариантами как комплект */
            products = await _analysisProductsService.RelateProductWithCollectionComplekts(products, collectionComplekts);

            #endregion
            _loggingService.InfoMessage($"IMPORT_PRODUCTS: UpdatingProducts");
            await _updatingIntegrationDataService.UpdatingProducts(products);

            _loggingService.InfoMessage($"IMPORT_PRODUCTS: CreatingProducts");
            await _updatingIntegrationTablesService.CreatingProducts(products);

            _loggingService.InfoMessage($"IMPORT_PRODUCTS: End write data to integration DB");


            _loggingService.InfoMessage($"IMPORT_PRODUCTS: Start updating data to base DB");
            /* обновление базовых таблиц, обновлене тех записей, что есть Обновление ранга/веса. Если надо будует обновлять другие данные, расширяем контаркт */
            _loggingService.InfoMessage($"IMPORT_PRODUCTS: UpdateOrCreateProductPackages");

            await _updatingBaseTables.UpdateOrCreateProductPackages(productPacks);
            _loggingService.InfoMessage($"IMPORT_PRODUCTS: UpdateSeries");
            await _updatingBaseTables.UpdateSeries(series);
            _loggingService.InfoMessage($"IMPORT_PRODUCTS: UpdateRooms");
            await _updatingBaseTables.UpdateRooms(rooms);
            _loggingService.InfoMessage($"IMPORT_PRODUCTS: UpdateCollections");
            await _updatingBaseTables.UpdateCollections(collections);
            _loggingService.InfoMessage($"IMPORT_PRODUCTS: UpdateVendorCodes");
            await _updatingBaseTables.UpdateVendorCodes(vendorCodes);

            _loggingService.InfoMessage($"IMPORT_PRODUCTS: End updating data to base DB");

            _loggingService.InfoMessage($"IMPORT_PRODUCTS: End all tasks products parsing");


            var dateEnd = DateTime.Now;
            var ts = dateEnd - timeStart;
            _loggingService.InfoMessage($"IMPORT_PRODUCTS: Time in minutes: {Math.Round(ts.TotalSeconds / 60, 2)} Time in sec: { Math.Round(ts.TotalSeconds, 2)}");
        }

        public async void ImportPricesZones(string path)
        {
            var timeStart = DateTime.Now;

            _loggingService.InfoMessage($"IMPORT_ZONES: Start parsing file");
            var paringResult = _parsingService.ParsingPrices(path);

            _loggingService.InfoMessage($"IMPORT_ZONES: AnalysisZone");
            var priceZones = await _analysisZoneService.AnalysisZone(paringResult.Zones);

            _loggingService.InfoMessage($"IMPORT_ZONES: MapProductPrices");
            var productPrices = _mappingService.MapProductPrices(priceZones, paringResult.Zones);

            _loggingService.InfoMessage($"IMPORT_ZONES: MapRegions");
            var regions = _mappingService.MapRegions(paringResult.Departments, priceZones);

            _loggingService.InfoMessage($"IMPORT_ZONES: CreatePriceZones");
            await _creatingBaseDataService.CreatePriceZones(priceZones);
            _loggingService.InfoMessage($"IMPORT_ZONES: CreateRegions");
            await _creatingBaseDataService.CreateRegions(regions);
            _loggingService.InfoMessage($"IMPORT_ZONES: CreatePrices");
            await _creatingBaseDataService.CreatePrices(productPrices);

            var dateEnd = DateTime.Now;
            var ts = dateEnd - timeStart;
            _loggingService.InfoMessage("IMPORT_ZONES: Total time in minutes: " + Math.Round(ts.TotalSeconds / 60, 2) + "Time in sec: " + Math.Round(ts.TotalSeconds, 2));
        }
    }
}
