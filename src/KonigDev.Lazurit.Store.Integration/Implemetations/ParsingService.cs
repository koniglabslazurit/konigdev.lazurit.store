﻿using KonigDev.Lazurit.Store.Integration.Contracts;
using KonigDev.Lazurit.Store.Integration.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using KonigDev.Lazurit.Store.Integration.Contracts.Zones;

namespace KonigDev.Lazurit.Store.Integration.Implemetations
{
    public class ParsingService : IParsingService
    {
        private const string NodeProduct = "Изделие";
        private const string NodeProductPack = "Упаковка";
        private const string NodePriceList = "Costs";
        private const string NodeDepartment = "Department";
        private const string NodeOffice = "Office";
        private const string NodeShopt = "Shop";

        /// <summary>
        /// Прасинг файла с продуктами
        /// </summary>
        /// <param name="fileName"></param>
        public List<ProductNode> ParsingProducts(string fileName)
        {
            //todo for tests
            //var timeStart = DateTime.Now;

            var products = new List<ProductNode>();
            using (XmlReader reader = XmlReader.Create(fileName))
            {
                reader.MoveToContent();
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == NodeProduct)
                    {
                        var otherXml = reader.ReadOuterXml();
                        var product = ParseProductNode(otherXml);
                        product.ProductPacks = ParseProductPackNode(otherXml);
                        products.Add(product);
                    }
                }
            }

            //todo for tests
            //var dateEnd = DateTime.Now;
            //var ts = dateEnd - timeStart;
            //Console.WriteLine("Time in minutes: " + ts.TotalSeconds / 60);
            //Console.WriteLine("Time in sec: " + ts.TotalSeconds);
            //Console.WriteLine("Products count: " + products.Count);
            //Console.WriteLine("---------------------------------");
            //todo for tests
            //long size = 0;
           // using (Stream s = new MemoryStream())
           // {
           //     BinaryFormatter formatter = new BinaryFormatter();
           //     formatter.Serialize(s, products);
           //     size = s.Length;
                //todo for tests
            //    Console.WriteLine("size in KB: " + size / 1024);
            //}
            //todo for tests
            //Console.Read();

            return products;
        }

        /// <summary>
        /// Парсинг элемента продукт
        /// </summary>
        /// <param name="otherXml"></param>
        /// <returns></returns>
        private ProductNode ParseProductNode(string otherXml)
        {
            ProductNode result = null;
            XmlRootAttribute xRoot = new XmlRootAttribute();
            xRoot.IsNullable = true;
            xRoot.ElementName = NodeProduct;

            XmlSerializer serializer = new XmlSerializer(typeof(ProductNode), xRoot);
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(otherXml)))
            {
                using (XmlReader streamReader = XmlReader.Create(stream))
                {
                    result = (ProductNode)serializer.Deserialize(streamReader);
                }
            }

            return result;
        }

        //Парсинг даных по упаковке у продуктов
        private List<ProductPackNode> ParseProductPackNode(string otherXml)
        {
            List<ProductPackNode> result = new List<ProductPackNode>();
            XmlRootAttribute xRoot = new XmlRootAttribute();
            xRoot.IsNullable = true;
            xRoot.ElementName = NodeProductPack;

            XmlSerializer serializer = new XmlSerializer(typeof(ProductPackNode), xRoot);
            XmlDocument documentProduct = new XmlDocument();
            documentProduct.LoadXml(otherXml);
            var nodes = documentProduct.GetElementsByTagName(NodeProductPack);

            foreach (XmlNode node in nodes)
            {
                var t = node.OuterXml;
                using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(t)))
                {
                    using (XmlReader streamReader = XmlReader.Create(stream))
                    {
                        var productPack = (ProductPackNode)serializer.Deserialize(streamReader);
                        result.Add(productPack);
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Парсинг ценовыъ зон, цен, регионы, магазины
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public DataNode ParsingPrices(string fileName)
        {

            var result = new DataNode();
            using (XmlReader reader = XmlReader.Create(fileName))
            {
                reader.MoveToContent();
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == NodePriceList)
                    {              
                        var otherXml = reader.ReadOuterXml();
                        var zone = ParseZoneCosts(otherXml);
                        result.Zones.Add(zone);
                    }

                    if(reader.NodeType == XmlNodeType.Element && reader.Name == NodeDepartment)
                    {
                        var otherXml = reader.ReadOuterXml();
                        var department = ParseDepartment(otherXml);
                        result.Departments.Add(department);
                    }
                }
            }

            return result;
        }

        private Zone ParseZoneCosts(string otherXml)
        {
            XmlRootAttribute xRoot = new XmlRootAttribute();
            xRoot.IsNullable = true;
            xRoot.ElementName = NodePriceList;
            var result = new Zone();

            XmlSerializer serializer = new XmlSerializer(typeof(Zone), xRoot);
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(otherXml)))
            {
                using (XmlReader streamReader = XmlReader.Create(stream))
                {
                    result = (Zone)serializer.Deserialize(streamReader);
                }
            }
            return result;
        }

        private Department ParseDepartment(string otherXml)
        {
            XmlRootAttribute xRoot = new XmlRootAttribute();
            xRoot.IsNullable = true;
            xRoot.ElementName = NodeDepartment;
            var result = new Department();

            XmlSerializer serializer = new XmlSerializer(typeof(Department), xRoot);
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(otherXml)))
            {
                using (XmlReader streamReader = XmlReader.Create(stream))
                {
                    result = (Department)serializer.Deserialize(streamReader);
                }
            }
            return result;
        }        
    }
}