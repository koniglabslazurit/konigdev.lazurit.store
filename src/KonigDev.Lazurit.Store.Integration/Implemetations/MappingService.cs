﻿using KonigDev.Lazurit.Store.Integration.Interfaces;
using System.Collections.Generic;
using System.Linq;
using KonigDev.Lazurit.Store.Integration.Contracts;
using KonigDev.Lazurit.Store.Integration.Services;
using KonigDev.Lazurit.Core.Enums.Enums;
using KonigDev.Lazurit.Store.Integration.Contracts.Zones;
using System;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;
using KonigDev.Lazurit.Store.Integration.Contracts.AnalisysContracts;
using KonigDev.Lazurit.Hub.Model.Mappings.Integrations;

namespace KonigDev.Lazurit.Store.Integration.Implemetations
{

    public class MappingService : IMappingService
    {
        private const char ShopEmailSeparator = ',';
        private const char ShopPhoneSeparator = ',';
        private const char StyleSeparator = ',';
        private const char TargetAudiencesSeparator = ',';
        private const char VendorCodesListSeparator = ';';

        /* todo если будет разрастаться, использовать stap mapping by Kovalenko */
        public IntegrationAnalysisContract MapProducts(List<ProductNode> source)
        {
            var result = new IntegrationAnalysisContract();

            var matrix = new ColorMatrix();

            foreach (var item in source)
            {
                if (!item.IsValid)
                {
                    result.Errors.Add(new IntegrationErrorsContract { Error = $"ТипАртикула: '{item.FurnitureType}'; Артикул: '{item.VendorCode}'; МаркетинговыйАртикул:'{item.MarketerVendorCode}'; ТипКомнаты: '{item.Room}'; Серия: '{item.Series}'; Коллекция: '{item.Collection}'; ЦветКорпуса: '{item.FrameColor}'; ВариантОтделкиИзделия: '{item.Facing}'", });
                    continue;
                }

                int height = 0;
                int length = 0;
                int width = 0;

                IntegrationMarketingRange range = new IntegrationMarketingRange(item.MarketerRange);

                /* размеры */
                /* todo подумать куда их вынести */
                if (!string.IsNullOrWhiteSpace(item.Height))
                {
                    int h = 0;
                    if (int.TryParse(item.Height.Replace(" ", ""), out h))
                        height = h;
                    else result.Errors.Add(new IntegrationErrorsContract { Error = $"Ошибка конвертации поля Height: '{item.Height}'. ТипАртикула: '{item.FurnitureType}'; Артикул: '{item.VendorCode}'; ТипКомнаты: '{item.Room}'; Серия: '{item.Series}';" });
                }

                if (!string.IsNullOrWhiteSpace(item.Length))
                {
                    int l = 0;
                    if (int.TryParse(item.Length.Replace(" ", ""), out l))
                        length = l;
                    else result.Errors.Add(new IntegrationErrorsContract { Error = $"Ошибка конвертации поля Length: '{item.Length}'. ТипАртикула: '{item.FurnitureType}'; Артикул: '{item.VendorCode}'; ТипКомнаты: '{item.Room}'; Серия: '{item.Series}';" });
                }

                if (!string.IsNullOrWhiteSpace(item.Width))
                {
                    int w = 0;
                    if (int.TryParse(item.Width.Replace(" ", ""), out w))
                        width = w;
                    else result.Errors.Add(new IntegrationErrorsContract { Error = $"Ошибка конвертации поля Width: '{item.Width}'. ТипАртикула: '{item.FurnitureType}'; Артикул: '{item.VendorCode}'; ТипКомнаты: '{item.Room}'; Серия: '{item.Series}';" });
                }

                /* series */
                if (!result.Series.Any(p => p.SyncCode1C == item.Series))
                    result.Series.Add(new IntegrationSeriesContract { SyncCode1C = item.Series, RangeSeries = range.RangeSeries });

                /* rooms */
                if (!result.Rooms.Any(p => p.SyncCode1C == item.Room))
                    result.Rooms.Add(new IntegrationRoomContract { SyncCode1C = item.Room, RangeRoom = range.RangeRoom });

                /* collection */
                if (!result.Collections.Any(p => p.Room == item.Room && p.Series == item.Series))
                    result.Collections.Add(new IntegrationCollectionContract { Room = item.Room, Series = item.Series, RangeCollection = range.RangeCollection });

                /* collectionsVariants */
                /* получение вариантов коллекции идет по матрице вариантов коллекции
                 * сборка вариантов коллекции не являющимися комплектами, проверяем что тип артикула не является комплектом */
                if (item.FurnitureType != "Комплект")
                    if (!string.IsNullOrWhiteSpace(item.FacingColor) && !matrix.IsFacingColorIsIgnored(item.FacingColor) && !matrix.IsFacingIsIgnored(item.Facing))
                        if (!result.CollectionVariants.Any(p => p.FacingColor == item.FacingColor && p.Facing == item.Facing && p.FrameColor == item.FrameColor && p.Room == item.Room && p.Series == item.Series))
                        {
                            var facing = item.Facing;
                            var facingColor = item.FacingColor;

                            var facingMatrix = matrix.GetFacingsMatrix(item.Facing);
                            var facingColorMatrix = matrix.GetFacingColorsMatrix(item.FacingColor);

                            if (facingMatrix != null)
                                facing = facingMatrix.Name;
                            if (facingColorMatrix != null)
                                facingColor = facingColorMatrix.Name;

                            if (!result.CollectionVariants.Any(p => p.FacingColor == facingColor && p.Facing == facing && p.FrameColor == item.FrameColor && p.Room == item.Room && p.Series == item.Series))
                            {
                                result.CollectionVariants.Add(new IntegrationCollectionVariantContract
                                {
                                    Facing = facing,
                                    FacingColor = facingColor,
                                    FrameColor = item.FrameColor,
                                    Room = item.Room,
                                    Series = item.Series,
                                    FacingChilds = facingMatrix != null ? facingMatrix.Childs : new List<string>(),
                                    FacingColorsChilds = facingColorMatrix != null ? facingColorMatrix.Childs : new List<string>()
                                });
                            }
                        }
                /* формирование комплектов как вариантов коллекции */
                if (item.FurnitureType == "Комплект" && item.FurnitureForm == "Комплект мебели" && !string.IsNullOrWhiteSpace(item.VendorCodeList))
                {
                    if (!result.CollectionComplekts.Any(p => p.SyncCode1C == item.SyncCode1C))
                    {
                        var facing = item.Facing;
                        var facingColor = item.FacingColor;

                        var facingMatrix = matrix.GetFacingsMatrix(item.Facing);
                        var facingColorMatrix = matrix.GetFacingColorsMatrix(item.FacingColor);

                        if (facingMatrix != null)
                            facing = facingMatrix.Name;
                        if (facingColorMatrix != null)
                            facingColor = facingColorMatrix.Name;

                        result.CollectionComplekts.Add(new IntegrationCollectionComplektContract
                        {
                            Facing = facing,
                            FacingColor = facingColor,
                            FrameColor = item.FrameColor,
                            Room = item.Room,
                            Series = item.Series,
                            SyncCode1C = item.SyncCode1C,
                            Products = item.VendorCodeList.Split(VendorCodesListSeparator).Where(p => !string.IsNullOrWhiteSpace(p)).ToList()
                                .Select(p => p.Trim()).ToList()
                        });
                    }
                }

                /* frameColors */
                if (!result.FrameColors.Any(p => p.SyncCode1C == item.FrameColor))
                    result.FrameColors.Add(new IntegrationFrameColorContract { SyncCode1C = item.FrameColor });                

                /* facings */
                if (!result.Facings.Any(p => p.SyncCode1C == item.Facing))
                    result.Facings.Add(new IntegrationFacingContract { SyncCode1C = item.Facing });                

                /* FacingColor */
                if (!string.IsNullOrWhiteSpace(item.FacingColor))
                    if (!result.FacingColors.Any(p => p.SyncCode1C == item.FacingColor))
                        result.FacingColors.Add(new IntegrationFacingColorContract { SyncCode1C = item.FacingColor });

                /* styles */
                var styles = new List<string>();
                if (!string.IsNullOrWhiteSpace(item.Style))
                {
                    styles = item.Style.Split(StyleSeparator).Where(p => !string.IsNullOrWhiteSpace(p)).ToList()
                        .Select(p => p.Trim()).ToList();
                    foreach (var style in styles)
                    {
                        var s = style;
                        /* чтобы избежать задвоения стилей, у которых наименование различается в ловер кейсе, подменяем один дргуим */
                        if (!result.Styles.Any(p => p.SyncCode1C == style) && result.Styles.Any(p => p.SyncCode1C.ToLower() == style.ToLower()))
                        {
                            s = result.Styles.FirstOrDefault(p => p.SyncCode1C.ToLower() == style.ToLower()).SyncCode1C;
                        }
                        if (!result.Styles.Any(p => p.SyncCode1C == s))
                            result.Styles.Add(new IntegrationStyleContract { SyncCode1C = s });
                    }
                }

                /* targetAudiences */
                var targetAudiences = new List<string>();
                if (!string.IsNullOrWhiteSpace(item.Type))
                {
                    targetAudiences = item.Type.Split(TargetAudiencesSeparator).Where(p => !string.IsNullOrWhiteSpace(p)).ToList()
                         .Select(p => p.Trim()).ToList();
                    foreach (var ta in targetAudiences)
                    {
                        if (!result.TargetAudiencies.Any(p => p.SyncCode1C == ta))
                            result.TargetAudiencies.Add(new IntegrationTargetAudienceContract { SyncCode1C = ta });
                    }
                }

                /* furnitureTypes */
                /* получение типа мебели (=тип артикула). Получение родителя, контейнера для типов идет по полю ТипАртикула FurnitureType. 
                 * если поле заполнено, то тип артикула, который является родителем берется из этого поля, если отсутсвует, то значит нет родителя.
                 * В качестве родителя не могу выступать типы с наименованием Карниз, Комплект, все Корпуса */

                if (!result.Furnitures.Any(p => p.SyncCode1C == item.FurnitureType))
                {
                    var parent = "";
                    if (!string.IsNullOrWhiteSpace(item.FurnitureForm)
                        && !item.FurnitureType.Contains("Карниз") /* todo вынести в константы? */
                        && !item.FurnitureType.Contains("Комплект")
                        && !item.FurnitureType.Contains("Корпус"))
                        parent = item.FurnitureForm;
                    result.Furnitures.Add(new IntegrationFurnitureContract { SyncCode1C = item.FurnitureType, Parent = parent });
                }

                /* vendorCodes */
                /* код для артикула - код маркетинговый, прилетающий из выгрузки!!!
                 * !!! если отсутсвует маркетинговый, берем просто артикул!!
                 *  */
                if (string.IsNullOrEmpty(item.MarketerVendorCode))
                    item.MarketerVendorCode = item.VendorCode;
                if (!result.VendorCodes.Any(p => p.SyncCode1C == item.MarketerVendorCode))
                    result.VendorCodes.Add(new IntegrationVendorCodeContract { SyncCode1C = item.MarketerVendorCode, RangeVendorCode = range.RangeVendorCode, FurnitureTypeSyncCode1C = item.FurnitureType });

                /* properties */
                MapProperties(item, result.Properties);

                /* products */
                /* получаем продукты. Игнорим те изделия, что являются комплектами как вариант исполнения "Комплект мебели". Определяем комплекты как изделие с типом "Комплект" */
                /* todo подумать о преобразовании целочисленных значений! Сейчас берем и закидываем */
                if (item.FurnitureForm != "Комплект мебели")
                    if (!result.Products.Any(p => p.SyncCode1C == item.SyncCode1C))
                    {
                        bool isComplekt = false;
                        bool isValid = true;
                        List<string> productsArray = new List<string>();
                        if (item.FurnitureType == "Комплект")
                        {
                            /* определение комплекта как единичного товара */
                            if (!string.IsNullOrEmpty(item.VendorCodeList))
                            {
                                productsArray = item.VendorCodeList.Split(VendorCodesListSeparator).Where(p => !string.IsNullOrWhiteSpace(p)).ToList()
                                .Select(p => p.Trim()).ToList();
                                if (productsArray.Any()) /* если товар - единичный товар как комплект, то проверяем что у него должен быть список артикулов, иначе невалидныая запись */
                                    isComplekt = true;
                                else
                                {
                                    isValid = false;
                                    result.Errors.Add(new IntegrationErrorsContract { Error = $"Комплект: изделие с кодом {item.SyncCode1C} - единичный товар как комплект, но у него отсутсвует СписокАртикуловКомплекта, ошибка данных" });
                                }
                            } /* todo подумать как проверку сделать оптимальнее */
                            else
                            {
                                isValid = false;
                                result.Errors.Add(new IntegrationErrorsContract { Error = $"Комплект: изделие с кодом {item.SyncCode1C} - единичный товар как комплект, но у него отсутсвует СписокАртикуловКомплекта, ошибка данных" });
                            }
                        }
                        if (isValid)
                        {
                            var productConvertContract = new ProductConvertContract { IsComplekt = isComplekt, Height = height, Length = length, Width = width, RangeTotal = range.RangeTotal, RangeVendorCode = range.RangeVendorCode };
                            var product = InfrastructureServices.ConvertToContract(item, productConvertContract);
                            product.Styles = styles;
                            product.TargetAudiences = targetAudiences;
                            result.Products.Add(product);
                        }
                    }

                /* упаковки */
                result.ProductsPacks.AddRange(InfrastructureServices.ConvertProductPackToContract(item.ProductPacks, item.SyncCode1C));
            }
            return result;
        }

        private static List<IntegrationPropertiesContract> MapProperties(ProductNode node, List<IntegrationPropertiesContract> properties)
        {
            if (!string.IsNullOrWhiteSpace(node.Backlight))
                if (!properties.Any(p => p.Value == node.Backlight))
                    properties.Add(new IntegrationPropertiesContract { Value = node.Backlight, PropertyType = EnumPropertiesType.Backlight });

            if (!string.IsNullOrWhiteSpace(node.Material))
                if (!properties.Any(p => p.Value == node.Material))
                    properties.Add(new IntegrationPropertiesContract { Value = node.Material, PropertyType = EnumPropertiesType.Material });

            if (!string.IsNullOrWhiteSpace(node.LeftRight))
                if (!properties.Any(p => p.Value == node.LeftRight))
                    properties.Add(new IntegrationPropertiesContract { Value = node.LeftRight, PropertyType = EnumPropertiesType.LeftRight });

            if (!string.IsNullOrWhiteSpace(node.ProductPartMarker))
                if (!properties.Any(p => p.Value == node.ProductPartMarker))
                    properties.Add(new IntegrationPropertiesContract { Value = node.ProductPartMarker, PropertyType = EnumPropertiesType.ProductPartMarker });

            /* TODO с механизмом не понятно. Пока нет в примере выгрузки */
            if (!string.IsNullOrWhiteSpace(node.Mechanism))
                if (!properties.Any(p => p.Value == node.Mechanism))
                    properties.Add(new IntegrationPropertiesContract { Value = node.Mechanism, PropertyType = EnumPropertiesType.Mechanism });

            if (!string.IsNullOrWhiteSpace(node.FurnitureForm))
                if (!properties.Any(p => p.Value == node.FurnitureForm))
                    properties.Add(new IntegrationPropertiesContract { Value = node.FurnitureForm, PropertyType = EnumPropertiesType.FurnitureForm });

            return properties;
        }

        public List<DtoRegionUpdating> MapRegions(List<Department> departments, List<IntegrationZoneContract> zones)
        {
            /* мапим регионы, разбираем телеофны и адреса */
            var regions = departments.Select(d => new DtoRegionUpdating
            {
                SyncCode1C = d.Id,
                Title = d.Name,
                WebAddress = d.WebAddress,
                Cities = d.Offices.Select(c => new DtoCityUpdating
                {
                    SyncCode1C = c.Id,
                    Title = c.Name,
                    PriceZoneId = zones.FirstOrDefault(z => z.SyncCode1C == c.PriceZone)?.Id ?? Guid.Empty,
                    Showrooms = c.Shops.Select(s => new DtoShowroomUpdating
                    {
                        Address = s.Address,
                        Emails = string.IsNullOrEmpty(s.Emails) ? new List<string>() : s.Emails.Split(ShopEmailSeparator).Where(p => !string.IsNullOrEmpty(p)).ToList(),
                        Phones = string.IsNullOrEmpty(s.Phones) ? new List<string>() : s.Phones.Split(ShopPhoneSeparator).Where(p => !string.IsNullOrEmpty(p)).ToList(),
                        Title = s.Name,
                        SyncCode1C = s.Id,
                        /* логика парсинга координат заложена в геттер todo подумать куда и как вынести */
                        GeoCoordinats = s.GeoCoordinats
                    }).ToList()
                }).ToList()
            }).ToList();
            return regions;
        }

        public List<IntegrationZoneContract> MapPricesZones(List<Zone> zones)
        {
            throw new NotImplementedException();
        }

        public List<DtoProductPricesUpdating> MapProductPrices(List<IntegrationZoneContract> priceZones, List<Zone> zones)
        {
            var result = new List<DtoProductPricesUpdating>();

            foreach (var item in zones)
            {
                var zoneId = priceZones.FirstOrDefault(p => p.SyncCode1C == item.Id)?.Id ?? Guid.Empty;
                if (zoneId == Guid.Empty)
                    continue; //todo write to log!!!

                foreach (var cost in item.Costs.Where(i => !string.IsNullOrWhiteSpace(i.Value)).ToList())
                {
                    decimal price = 0;
                    if (decimal.TryParse(cost.Value, out price))
                    {
                        var product = result.FirstOrDefault(p => p.ProductSyncCode == cost.Product);
                        if (product == null)
                        {
                            product = new DtoProductPricesUpdating { ProductSyncCode = cost.Product };
                            result.Add(product);
                        }
                        product.Prices.Add(new DtoProductPricesItemUpdating { PriceZoneId = zoneId, Price = price });
                    }
                }
            }

            return result;
        }
    }
}
