﻿using KonigDev.Lazurit.Store.Integration.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Store.Integration.Contracts;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Core.Enums.Enums;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto.DtoIntegrationLoadings;
using KonigDev.Lazurit.Hub.Grains.Interfaces;

namespace KonigDev.Lazurit.Store.Integration.Implemetations
{
    public class LoggingImportDataBaseService : ILoggingImportDataBaseService
    {
        private readonly IHubHandlersFactory _hubFactory;

        public LoggingImportDataBaseService(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }
        public async Task LoggingProductsImport(LoggingImportDataBaseContract contract, string fileName)
        {
            var command = new CreateIntegrationLoadingCommand
            {
                Id = Guid.NewGuid(),
                DateLoad = DateTime.UtcNow,
                FileName = fileName,
                Type = EnumIntegrationImportType.Products.ToString()
            };

            foreach (var item in contract.Errors)
            {
                command.Items.Add(new DtoIntegrationLoadingCreateItem
                {
                    Id = Guid.NewGuid(),
                    IntegrationLoadingId = command.Id,
                    Type = EnumIntegrationLodingItemType.Error.ToString(),
                    Value = item.Error
                });
            }

            command.Items.Add(new DtoIntegrationLoadingCreateItem
            {
                Id = Guid.NewGuid(),
                IntegrationLoadingId = command.Id,
                Type = EnumIntegrationLodingItemType.Info.ToString(),
                Value = $"Всего коллекций:{contract.Collections.Count()}; Новых коллекци: {contract.Collections.Where(p=>p.IsNew).Count()}"
            });


            command.Items.Add(new DtoIntegrationLoadingCreateItem
            {
                Id = Guid.NewGuid(),
                IntegrationLoadingId = command.Id,
                Type = EnumIntegrationLodingItemType.Info.ToString(),
                Value = $"Всего вариантов коллекций:{contract.CollectionVariants.Count()}; Новых вариантов коллекци: {contract.CollectionVariants.Where(p => p.IsNew).Count()}"
            });

            command.Items.Add(new DtoIntegrationLoadingCreateItem
            {
                Id = Guid.NewGuid(),
                IntegrationLoadingId = command.Id,
                Type = EnumIntegrationLodingItemType.Info.ToString(),
                Value = $"Всего цветов отделки:{contract.FacingColors.Count()}; Новых цветов отделки: {contract.FacingColors.Where(p => p.IsNew).Count()}"
            });

            command.Items.Add(new DtoIntegrationLoadingCreateItem
            {
                Id = Guid.NewGuid(),
                IntegrationLoadingId = command.Id,
                Type = EnumIntegrationLodingItemType.Info.ToString(),
                Value = $"Всего типов отделок:{contract.Facings.Count()}; Новых типов отделок: {contract.Facings.Where(p => p.IsNew).Count()}"
            });

            command.Items.Add(new DtoIntegrationLoadingCreateItem
            {
                Id = Guid.NewGuid(),
                IntegrationLoadingId = command.Id,
                Type = EnumIntegrationLodingItemType.Info.ToString(),
                Value = $"Всего цветов корпуса:{contract.FrameColors.Count()}; Новых цветов корпуса: {contract.FrameColors.Where(p => p.IsNew).Count()}"
            });
            command.Items.Add(new DtoIntegrationLoadingCreateItem
            {
                Id = Guid.NewGuid(),
                IntegrationLoadingId = command.Id,
                Type = EnumIntegrationLodingItemType.Info.ToString(),
                Value = $"Всего типов мебели:{contract.Furnitures.Count()}; Новых типов мебели: {contract.Furnitures.Where(p => p.IsNew).Count()}"
            });
            command.Items.Add(new DtoIntegrationLoadingCreateItem
            {
                Id = Guid.NewGuid(),
                IntegrationLoadingId = command.Id,
                Type = EnumIntegrationLodingItemType.Info.ToString(),
                Value = $"Всего продуктов:{contract.Products.Count()}; Новых продуктов: {contract.Products.Where(p => p.IntegrationStatus == EnumIntegrationProductStatus.Missing).Count()}; Продуктов без изменений: {contract.Products.Where(p => p.IntegrationStatus == EnumIntegrationProductStatus.Missing).Count()}; Были изменены: {contract.Products.Where(p => p.IntegrationStatus == EnumIntegrationProductStatus.Changed).Count()}; Пропущено для записи: {contract.Products.Where(p => p.IntegrationStatus == EnumIntegrationProductStatus.Skip).Count()}"
            });

            command.Items.Add(new DtoIntegrationLoadingCreateItem
            {
                Id = Guid.NewGuid(),
                IntegrationLoadingId = command.Id,
                Type = EnumIntegrationLodingItemType.Info.ToString(),
                Value = $"Всего типов комнат:{contract.Rooms.Count()}; Новых типов комнат: {contract.Rooms.Where(p => p.IsNew).Count()}"
            });

            command.Items.Add(new DtoIntegrationLoadingCreateItem
            {
                Id = Guid.NewGuid(),
                IntegrationLoadingId = command.Id,
                Type = EnumIntegrationLodingItemType.Info.ToString(),
                Value = $"Всего серий:{contract.Series.Count()}; Новых серий: {contract.Series.Where(p => p.IsNew).Count()}"
            });

            command.Items.Add(new DtoIntegrationLoadingCreateItem
            {
                Id = Guid.NewGuid(),
                IntegrationLoadingId = command.Id,
                Type = EnumIntegrationLodingItemType.Info.ToString(),
                Value = $"Всего стилей:{contract.Styles.Count()}; Новых стилей: {contract.Styles.Where(p => p.IsNew).Count()}"
            });

            command.Items.Add(new DtoIntegrationLoadingCreateItem
            {
                Id = Guid.NewGuid(),
                IntegrationLoadingId = command.Id,
                Type = EnumIntegrationLodingItemType.Info.ToString(),
                Value = $"Всего целевых аудиторий:{contract.TargetAudiencies.Count()}; Новых целевых аудиторий: {contract.TargetAudiencies.Where(p => p.IsNew).Count()}"
            });
            command.Items.Add(new DtoIntegrationLoadingCreateItem
            {
                Id = Guid.NewGuid(),
                IntegrationLoadingId = command.Id,
                Type = EnumIntegrationLodingItemType.Info.ToString(),
                Value = $"Всего марткетинговых артикулов (значений):{contract.VendorCodes.Count()}; Новых марткетинговых артикулов (значений): {contract.VendorCodes.Where(p => p.IsNew).Count()}"
            });


            await _hubFactory.CreateCommandHandler<CreateIntegrationLoadingCommand>().Execute(command);
        }
    }
}
