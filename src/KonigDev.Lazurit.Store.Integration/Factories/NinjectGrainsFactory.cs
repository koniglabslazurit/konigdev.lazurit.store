﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using Ninject;
using Ninject.Syntax;

namespace KonigDev.Lazurit.Store.Integration.Factories
{
    public class NinjectGrainsFactory : IHubHandlersFactory
    {
        private readonly IResolutionRoot _resolutionRoot;

        public NinjectGrainsFactory(IResolutionRoot resolutionRoot)
        {
            _resolutionRoot = resolutionRoot;
        }

        public IHubCommandHandler<TCommand> CreateCommandHandler<TCommand>()
        {
            return _resolutionRoot.Get<IHubCommandHandler<TCommand>>();
        }

        public IHubQueryHandler<TQuery, TResult> CreateQueryHandler<TQuery, TResult>()
        {
            return _resolutionRoot.Get<IHubQueryHandler<TQuery, TResult>>();
        }
    }
}
