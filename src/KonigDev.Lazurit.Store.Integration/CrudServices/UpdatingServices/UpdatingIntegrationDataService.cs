﻿using KonigDev.Lazurit.Store.Integration.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Store.Integration.Contracts;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;

namespace KonigDev.Lazurit.Store.Integration.CrudServices.UpdatingServices
{
    [Serializable]
    public class UpdatingIntegrationDataService : IUpdatingIntegrationDataService
    {
        private readonly IHubHandlersFactory _hubFactory;

        public UpdatingIntegrationDataService(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        public async Task UpdatingProducts(List<IntegrationProductContract> list)
        {
            var command = new UpdateIntegrationProductsAfterImportCommand
            {
                Products = list.Where(p=>p.IntegrationStatus == Core.Enums.Enums.EnumIntegrationProductStatus.Changed)
                .Select(item => new DtoIntegrationProductItem
                {
                    SyncCode1C = item.SyncCode1C, /* id */
                    Backlight = item.Backlight,
                    Collection = item.Collection,
                    Descr = item.Descr,
                    Facing = item.Facing,
                    FacingColor = item.FacingColor,
                    FrameColor = item.FrameColor,
                    FurnitureForm = item.FurnitureForm,
                    FurnitureType = item.FurnitureType,
                    Hash = item.Hash,
                    Height = item.Height,
                    LeftRight = item.LeftRight,
                    Length = item.Length,
                    MarketerRange = item.MarketerRange,
                    MarketerVendorCode = item.MarketerVendorCode,
                    Material = item.Material,
                    Mechanism = item.Mechanism,
                    Packing = item.Packing,
                    PermissibleLoad = item.PermissibleLoad,
                    ProductPartMarker = item.ProductPartMarker,
                    RoomType = item.RoomType,
                    Series = item.Series,
                    SleepingAreaLength = item.SleepingAreaLength,
                    SleepingAreaWidth = item.SleepingAreaWidth,
                    Style = item.Style,
                    TargetAudience = item.TargetAudience,
                    VendorCode = item.VendorCode,
                    VendorCodeList = item.VendorCodeList,
                    Warranty = item.Warranty,
                    Width = item.Width,
                    IntegrationStatus = item.IntegrationStatus
                }).ToList()
            };

            if(command.Products.Count > 0)
                await _hubFactory.CreateCommandHandler<UpdateIntegrationProductsAfterImportCommand>().Execute(command);
        }
    }
}
