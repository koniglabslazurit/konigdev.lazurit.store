﻿using KonigDev.Lazurit.Store.Integration.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Store.Integration.Contracts;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Seriess.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Seriess.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Article.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Article.Dto;
using System;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;

namespace KonigDev.Lazurit.Store.Integration.UpdatingServices
{
    public class UpdatingBaseTablesService : IUpdatingBaseTablesService
    {
        private readonly IHubHandlersFactory _hubFactory;

        public UpdatingBaseTablesService(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        public async Task UpdateRooms(List<IntegrationRoomContract> list)
        {
            var updateCommand = new UpdateRoomTypesCommand
            {
                RoomTypes = list.Where(p => !p.IsNew)
                .Where(p => !string.IsNullOrEmpty(p.RangeRoom))
                .Where(p => p.OriginalId.HasValue)
                .Select(p => new DtoUpdatingRoomType
                {
                    Id = p.OriginalId.Value,
                    SyncCode1C = p.SyncCode1C,
                    RangeRoom = p.RangeRoom
                }).ToList()
            };

            if (updateCommand.RoomTypes.Any())
                await _hubFactory.CreateCommandHandler<UpdateRoomTypesCommand>().Execute(updateCommand);
        }

        public async Task UpdateSeries(List<IntegrationSeriesContract> list)
        {
            var updateCommand = new UpdateSeriesCommand
            {
                Series = list.Where(p => !p.IsNew)
                .Where(p => !string.IsNullOrEmpty(p.RangeSeries))
                .Where(p => p.OriginalId.HasValue)
                .Select(p => new DtoUpdatingSeries
                {
                    Id = p.OriginalId.Value,
                    SyncCode1C = p.SyncCode1C,
                    RangeSeries = p.RangeSeries
                }).ToList()
            };

            if (updateCommand.Series.Any())
                await _hubFactory.CreateCommandHandler<UpdateSeriesCommand>().Execute(updateCommand);
        }

        public async Task UpdateCollections(List<IntegrationCollectionContract> list)
        {
            var updateCommand = new UpdateCollectionCommand
            {
                Collections = list.Where(p => !p.IsNew)
                .Where(p => !string.IsNullOrEmpty(p.RangeCollection))
                .Where(p => p.OriginalId.HasValue)
                .Select(p => new DtoUpdatingCollection
                {
                    Id = p.OriginalId.Value,
                    RangeCollection = p.RangeCollection
                }).ToList()
            };

            if (updateCommand.Collections.Any())
                await _hubFactory.CreateCommandHandler<UpdateCollectionCommand>().Execute(updateCommand);
        }

        public async Task UpdateVendorCodes(List<IntegrationVendorCodeContract> list)
        {
            var updateCommand = new UpdateVendorCodeCommand
            {
                VendorCodes = list.Where(p => !p.IsNew)
               .Where(p => p.IsChanged)
               .Where(p=>p.FurnitureTypeId != Guid.Empty)
               .Select(p => new DtoUpdatingVendorCode
               {
                   Id = p.Id,
                   RangeVendorCode = p.RangeVendorCode,
                   FurnitureTypeId = p.FurnitureTypeId
               }).ToList()
            };

            if (updateCommand.VendorCodes.Any())
                await _hubFactory.CreateCommandHandler<UpdateVendorCodeCommand>().Execute(updateCommand);
        }

        public async Task UpdateOrCreateProductPackages(List<IntegrationProductPackContract> list)
        {
            var command = new UpdateOrCreateProductsPackCommand
            {
                ProductsPacks = list.Select(p => new DtoProductPack
                {
                    Amount = p.Amont,
                    Height = p.Height,
                    Length = p.Length,
                    ProductSyncCode1C = p.ProductSyncCode,
                    SyncCode1C = p.Code,
                    Weight = p.Weight,
                    Width = p.Width
                }).ToList()
            };
            if (command.ProductsPacks.Any())
                await _hubFactory.CreateCommandHandler<UpdateOrCreateProductsPackCommand>().Execute(command);
        }
    }
}
