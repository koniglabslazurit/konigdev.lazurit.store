﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto.Creating;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Integration.Contracts;
using KonigDev.Lazurit.Store.Integration.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Core.Enums.Enums;
using KonigDev.Lazurit.Store.Integration.Services;

namespace KonigDev.Lazurit.Store.Integration.CreatingServices
{
    public class CreatingIntegrationDataService : ICreatingIntegrationDataService
    {
        private readonly IHubHandlersFactory _hubFactory;

        public CreatingIntegrationDataService(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        public async Task CreatingSeries(List<IntegrationSeriesContract> series)
        {
            var command = new CreateIntegrationSeriesCommand
            {
                Series = series
                .Where(p => p.IsNew)
                .Select(p => new DtoIntegrationSeriesItem
                {
                    Id = p.Id,
                    SyncCode1C = p.SyncCode1C
                }).ToList()
            };
            if (!command.Series.Any())
                return;

            await _hubFactory.CreateCommandHandler<CreateIntegrationSeriesCommand>().Execute(command);
        }

        public async Task CreatingRooms(List<IntegrationRoomContract> list)
        {
            var command = new CreateIntegrationRoomsCommand
            {
                Rooms = list
                .Where(p => p.IsNew)
                .Select(p => new DtoIntegrationRoomItem
                {
                    Id = p.Id,
                    SyncCode1C = p.SyncCode1C
                }).ToList()
            };
            if (command.Rooms.Any())
                await _hubFactory.CreateCommandHandler<CreateIntegrationRoomsCommand>().Execute(command);
        }

        public async Task CreatingCollections(List<IntegrationCollectionContract> list)
        {
            /* поулчение всех серий и комнат для сопоставления коллекци с ними */
            var series = await _hubFactory.CreateQueryHandler<GetIntegrationSeriesQuery, List<DtoIntegrationSeriesItem>>().Execute(new GetIntegrationSeriesQuery());
            var rooms = await _hubFactory.CreateQueryHandler<GetIntegrationRoomsQuery, List<DtoIntegrationRoomItem>>().Execute(new GetIntegrationRoomsQuery());

            var command = new CreateIntegrationCollectionsCommand
            {
                Collections = list
                .Where(p => p.IsNew)
                .Select(p => new DtoIntegrationCollectionCreating
                {
                    Id = p.Id,
                    RoomId = rooms.FirstOrDefault(r => r.SyncCode1C == p.Room)?.Id ?? Guid.Empty,
                    SeriesId = series.FirstOrDefault(s => s.SyncCode1C == p.Series)?.Id ?? Guid.Empty
                }).ToList()
                .Where(p => p.RoomId != Guid.Empty)
                .Where(p => p.SeriesId != Guid.Empty)
                .ToList()
            };

            if (!command.Collections.Any())
                return;

            await _hubFactory.CreateCommandHandler<CreateIntegrationCollectionsCommand>().Execute(command);
        }

        public async Task CreatingCollectionVariants(List<IntegrationCollectionVariantContract> list)
        {
            var collections = await _hubFactory.CreateQueryHandler<GetIntegrationCollectionsQuery, List<DtoIntegrationCollectionItem>>().Execute(new GetIntegrationCollectionsQuery());

            var command = new CreateIntegrationCollectionVariantsCommand
            {
                CollectionsVariants = list
                .Where(p => p.IsNew)
                .Select(p => new DtoIntegrationCollectionVariantCreating
                {
                    Id = p.Id,
                    CollectionId = collections.FirstOrDefault(c => c.Room == p.Room && c.Series == p.Series)?.Id ?? Guid.Empty,
                    Facing = p.Facing,
                    FacingColor = p.FacingColor,
                    FrameColor = p.FrameColor,
                    IsComplekt = p.IsComplekt
                }).ToList()
                .Where(p => p.CollectionId != Guid.Empty)
                .ToList()
            };
            if (!command.CollectionsVariants.Any())
                return;

            await _hubFactory.CreateCommandHandler<CreateIntegrationCollectionVariantsCommand>().Execute(command);
        }

        public async Task CreatingProducts(List<IntegrationProductContract> products)
        {
            /* создаем новые продукты и те у которых присутсвует вариант коллекции */
            var dtoProducts = products
                .Where(p => p.IntegrationStatus == EnumIntegrationProductStatus.Missing)
                .Where(p => p.CollectionId != Guid.Empty)
                .Select(item => new DtoIntegrationProductCreating
                {
                    ProductId = Guid.NewGuid(),
                    SyncCode1C = item.SyncCode1C, /* id */
                    Backlight = item.Backlight,
                    Collection = item.Collection,
                    Descr = item.Descr,
                    Facing = item.Facing,
                    FacingColor = item.FacingColor,
                    FrameColor = item.FrameColor,
                    FurnitureForm = item.FurnitureForm,
                    FurnitureType = item.FurnitureType,
                    Hash = item.Hash,
                    Height = item.Height,
                    LeftRight = item.LeftRight,
                    Length = item.Length,
                    MarketerRange = item.MarketerRange,
                    MarketerVendorCode = item.MarketerVendorCode,
                    Material = item.Material,
                    Mechanism = item.Mechanism,
                    Packing = item.Packing,
                    PermissibleLoad = item.PermissibleLoad,
                    ProductPartMarker = item.ProductPartMarker,
                    RoomType = item.RoomType,
                    Series = item.Series,
                    SleepingAreaLength = item.SleepingAreaLength,
                    SleepingAreaWidth = item.SleepingAreaWidth,
                    Style = item.Style,
                    TargetAudience = item.TargetAudience,
                    VendorCode = item.VendorCode,
                    VendorCodeList = item.VendorCodeList,
                    Warranty = item.Warranty,
                    Width = item.Width,
                    IntegrationStatus = item.IntegrationStatus,
                    /* списки */
                    Styles = item.Styles,
                    TargetAudiences = item.TargetAudiences,
                    CollectionVariants = item.CollectionVariants,
                    CollectionId = item.CollectionId
                }).ToList();

            if (dtoProducts.Count == 0)
                return;

            await _hubFactory.CreateCommandHandler<CreateIntegrationProductsCommand>().Execute(new CreateIntegrationProductsCommand { Products = dtoProducts });
        }

        public async Task CreatingCollectionComplects(List<IntegrationCollectionComplektContract> list)
        {
            var collections = await _hubFactory.CreateQueryHandler<GetIntegrationCollectionsQuery, List<DtoIntegrationCollectionItem>>().Execute(new GetIntegrationCollectionsQuery());

            var command = new CreateIntegrationCollectionVariantsCommand
            {
                CollectionsVariants = list
                .Where(p => p.IsNew)
                .Select(p => new DtoIntegrationCollectionVariantCreating
                {
                    Id = p.Id,
                    CollectionId = collections.FirstOrDefault(c => c.Room == p.Room && c.Series == p.Series)?.Id ?? Guid.Empty,
                    Facing = p.Facing,
                    FacingColor = p.FacingColor,
                    FrameColor = p.FrameColor,
                    IsComplekt = true,
                    SyncCode1C = p.SyncCode1C
                }).ToList()
                .Where(p => p.CollectionId != Guid.Empty)
                .ToList()
            };
            if (!command.CollectionsVariants.Any())
                return;
            await _hubFactory.CreateCommandHandler<CreateIntegrationCollectionVariantsCommand>().Execute(command);
        }
    }
}