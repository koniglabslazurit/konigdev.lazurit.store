﻿using KonigDev.Lazurit.Store.Integration.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using KonigDev.Lazurit.Store.Integration.Contracts;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Commands;
using System.Linq;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Facings.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Facings.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Dto;
using KonigDev.Lazurit.Core.Extensions;
using KonigDev.Lazurit.Hub.Model.DTO.Article.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Article.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Styles.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Styles.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Command;
using KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Property.Command;
using KonigDev.Lazurit.Hub.Model.DTO.Property.Dto;
using KonigDev.Lazurit.Store.Integration.Contracts.Zones;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Command;

namespace KonigDev.Lazurit.Store.Integration.CreatingServices
{
    public class CreatingBaseDataService : ICreatingBaseDataService
    {
        private readonly IHubHandlersFactory _hubFactory;

        public CreatingBaseDataService(IHubHandlersFactory hubFactory)
        {
            _hubFactory = hubFactory;
        }

        public async Task CreateFacingColors(List<IntegrationFacingColorContract> list)
        {
            var command = new CreateFacingColorsCommand
            {
                FacingColors = list
                .Where(p => p.IsNew)
                .Select(p => new DtoFacingColorItem
                {
                    Id = p.Id,
                    Name = p.SyncCode1C,/* наименование равно коду 1с при выгрузке */
                    SyncCode1C = p.SyncCode1C,
                    IsUniversal = p.IsUniversal
                }).ToList()
            };
            if (!command.FacingColors.Any())
                return;

            await _hubFactory.CreateCommandHandler<CreateFacingColorsCommand>().Execute(command);
        }

        public async Task CreateFacings(List<IntegrationFacingContract> list)
        {
            var command = new CreateFacingsCommand
            {
                Facings = list
                .Where(p => p.IsNew)
                .Select(p => new DtoFacingItem
                {
                    Id = p.Id,
                    Name = p.SyncCode1C,/* наименование равно коду 1с при выгрузке */
                    SyncCode1C = p.SyncCode1C
                }).ToList()
            };
            if (!command.Facings.Any())
                return;

            await _hubFactory.CreateCommandHandler<CreateFacingsCommand>().Execute(command);
        }

        public async Task CreateFrameColors(List<IntegrationFrameColorContract> list)
        {
            var command = new CreateFrameColorsCommand
            {
                FrameColors = list
               .Where(p => p.IsNew)
               .Select(p => new DtoFrameColorItem
               {
                   Id = p.Id,
                   Name = p.SyncCode1C, /* наименование равно коду 1с при выгрузке */
                   SyncCode1C = p.SyncCode1C,
                   IsUniversal = p.IsUniversal
               }).ToList()
            };

            if (!command.FrameColors.Any())
                return;

            await _hubFactory.CreateCommandHandler<CreateFrameColorsCommand>().Execute(command);
        }

        public async Task CreateOrUpdateFurnitureTypes(List<IntegrationFurnitureContract> list)
        {
            var command = new CreateFurnitureTypesCommand
            {
                FurnitureTypes = list
               .Select(p => new DtoFurnitureTypeItem
               {
                   Id = p.Id,
                   SyncCode1C = p.SyncCode1C,
                   Alias = p.SyncCode1C.Transliterate(),
                   ParentId = p.ParentId,
                   Name = p.SyncCode1C/* наименование равно коду 1с при выгрузке */
               }).ToList()
            };

            if (!command.FurnitureTypes.Any())
                return;

            await _hubFactory.CreateCommandHandler<CreateFurnitureTypesCommand>().Execute(command);
        }

        public async Task CreateVendorCode(List<IntegrationVendorCodeContract> list)
        {
            var command = new CreateVendorCodeCommand
            {
                VendorCodes = list
                .Where(p => p.IsNew)
                .Select(p => new DtoVendorCodeItem
                {
                    Id = p.Id,
                    SyncCode1C = p.SyncCode1C,
                    FurnitureTypeId = p.FurnitureTypeId,
                    Range = p.RangeVendorCode,
                    Name = p.SyncCode1C, /* наименование равно коду 1с при выгрузке */
                }).ToList()
            };

            if (!command.VendorCodes.Any())
                return;

            await _hubFactory.CreateCommandHandler<CreateVendorCodeCommand>().Execute(command);
        }

        public async Task CreateStyles(List<IntegrationStyleContract> list)
        {
            var command = new CreateStylesCommand
            {
                Styles = list
                .Where(p => p.IsNew)
                .Select(p => new DtoStyleItem
                {
                    Id = p.Id,
                    SyncCode1C = p.SyncCode1C,
                    Name = p.SyncCode1C /* наименование равно коду 1с при выгрузке */
                }).ToList()
            };

            if (!command.Styles.Any())
                return;

            await _hubFactory.CreateCommandHandler<CreateStylesCommand>().Execute(command);
        }

        public async Task CreateTargetAudiences(List<IntegrationTargetAudienceContract> list)
        {
            var command = new CreateTargetsAudiencesCommand
            {
                TargetAudiences = list
                .Where(p => p.IsNew)
                .Select(p => new DtoTargetAudienceItem
                {
                    Id = p.Id,
                    SyncCode1C = p.SyncCode1C,
                    Name = p.SyncCode1C /* наименование равно коду 1с при выгрузке */
                }).ToList()
            };

            if (!command.TargetAudiences.Any())
                return;

            await _hubFactory.CreateCommandHandler<CreateTargetsAudiencesCommand>().Execute(command);
        }

        public async Task CreateProperties(List<IntegrationPropertiesContract> properties)
        {
            var command = new CreatePropertiesCommand
            {
                Properties = properties
                .Where(p => p.IsNew)
                .Select(p => new DtoPropertyCreating
                {
                    Id = p.Id,
                    GroupeId = p.GroupeId,
                    SyncCode1C = p.PropertyType.ToString(),
                    Title = p.Title,
                    Value = p.Value,
                    IsMultiValue = p.IsMultiValue
                }).ToList()
            };
            if (!command.Properties.Any())
                return;

            await _hubFactory.CreateCommandHandler<CreatePropertiesCommand>().Execute(command);
        }

        /// <summary>
        /// Обновление по регионам, гододам, салонам
        /// </summary>
        /// <param name="departaments"></param>
        /// <param name="zones"></param>
        /// <returns></returns>
        public async Task CreateRegions(List<DtoRegionUpdating> regions)
        {
            UpdateRegionsCommand command = new UpdateRegionsCommand
            {
                Regions = regions
            };
            if (command.Regions.Count > 0)
                await _hubFactory.CreateCommandHandler<UpdateRegionsCommand>().Execute(command);
        }

        public async Task CreatePriceZones(List<IntegrationZoneContract> priceZones)
        {
            var command = new CreatePriceZonesCommand
            {
                PrizeSones = priceZones
                .Where(p => p.IsNew)
                .Select(p => new DtoPriceZoneItem
                {
                    Id = p.Id,
                    SyncCode1C = p.SyncCode1C
                }).ToList()
            };

            await _hubFactory.CreateCommandHandler<CreatePriceZonesCommand>().Execute(command);
        }

        public async Task CreatePrices(List<DtoProductPricesUpdating> productPrices)
        {
            var command = new UpdateProductPricesCommand
            {
                ProductPrices = productPrices.Select(p => new DtoProductPricesUpdating
                {
                    Prices = p.Prices,
                    ProductSyncCode = p.ProductSyncCode
                }).ToList()
            };
            await _hubFactory.CreateCommandHandler<UpdateProductPricesCommand>().Execute(command);
        }
    }
}