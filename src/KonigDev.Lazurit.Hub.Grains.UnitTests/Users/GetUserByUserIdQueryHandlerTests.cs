﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Users.Exceptions;
using KonigDev.Lazurit.Hub.Grains.Users.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Users.Queries;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.Users
{
   public class GetUserByUserIdQueryHandlerTests
    {
        Mock<IDBContextFactory> _contextFactoryMock;
        Mock<DbContext> _contextMock;
        Guid _userId;
        GetUserProfileByIdQueryHandler _getUserByUserIdQueryHandler;
        UserNotFoundException _exception;

        [SetUp]
        public void SetUp()
        {
            _userId = Guid.NewGuid();
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _exception = null;
            _contextMock.Setup(x => x.Set<UserProfile>()).Returns(new List<UserProfile>()
            {
                new UserProfile
                {
                    Id = _userId,
                    LinkId = Guid.NewGuid(),
                    Title = "",
                    UserAddresses = new List<UserAddress>(),
                    Email = "",
                    FirstName = "",
                    LastName = "",
                    UserPhones = new List<UserPhone>()
                 },
                 new UserProfile
                {
                    Id = Guid.NewGuid(),
                    LinkId = Guid.NewGuid(),
                    Title = "",
                    UserAddresses = new List<UserAddress>(),
                    Email = "",
                    FirstName = "",
                    LastName = "",
                    UserPhones = new List<UserPhone>()
                 },
            }.GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _getUserByUserIdQueryHandler = new GetUserProfileByIdQueryHandler(_contextFactoryMock.Object);
        }


        [Test]
        public async Task Execute_UserIdIsExistedGuid_Result()
        {
            //arrange

            //act
            var res = await _getUserByUserIdQueryHandler.Execute(new GetUserProfileByIdQuery { UserId = _userId });

            //assert
            Assert.IsTrue(res.Id == _userId);

        }

        [Test]
        public async Task Execute_UserIdIsNewGuid_Result()
        {
            //arrange

            //act
            try
            {
                var res = await _getUserByUserIdQueryHandler.Execute(new GetUserProfileByIdQuery { UserId = Guid.NewGuid() });
            }
            catch(UserNotFoundException ex)
            {
                _exception = ex;
            }

            //assert
            Assert.IsNotNull(_exception);
        }
    }
}
