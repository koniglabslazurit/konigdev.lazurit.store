﻿using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Users.CommandHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Users.Commands;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.Users
{
    [TestFixture]
    public class UpdateCityForUserCommandHandlerTests
    {
        Mock<IDBContextFactory> _contextFactoryMock;
        Mock<DbContext> _contextMock;
        UpdateCityForUserProfileCommandHandler _updateUserCommandHandler;
        Guid _userId;
        Guid _city1Id;
        Guid _city2Id;
        UserProfile _user;

        [SetUp]
        public void Setup()
        {
            _userId = Guid.NewGuid();
            _city1Id = Guid.NewGuid();
            _city2Id = Guid.NewGuid();
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _user = new UserProfile
            {
                Id = _userId,
                CityId = _city1Id
            };
            _contextMock.Setup(x => x.Set<UserProfile>()).Returns(new List<UserProfile>()
            {
                _user
            }.GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _updateUserCommandHandler = new UpdateCityForUserProfileCommandHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task UpdateCityForUserProfileCommandHandler_ShouldUpdate_IfUserExist()
        {
            //arrange
            var query = new UpdateCityUserProfile
            {
                UserId = _userId,
                CityId = _city2Id
            };
            //act
            await _updateUserCommandHandler.Execute(query);
            //assert
            Assert.AreEqual(_city2Id, _user.CityId);
        }

        [Test]
        public void UpdateCityForUserProfileCommandHandler_ReturnException_IfCommandIsNull()
        {
            //arrange
            UpdateCityUserProfile query = null;
            //act

            //assert
            Assert.ThrowsAsync<NotValidCommandException>(async () => await _updateUserCommandHandler.Execute(query));
        }

        [Test]
        public void UpdateCityForUserProfileCommandHandler_ReturnException_IfUserIsNull()
        {
            //arrange
            UpdateCityUserProfile query = new UpdateCityUserProfile
            {
                UserId = Guid.Empty,
                CityId = _city1Id
            };
            //act

            //assert
            Assert.ThrowsAsync<NotValidCommandException>(async () => await _updateUserCommandHandler.Execute(query));
        }
    }
}
