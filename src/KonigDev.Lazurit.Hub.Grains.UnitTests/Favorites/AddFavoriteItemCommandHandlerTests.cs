﻿using KonigDev.Lazurit.Core.Enums.Exeptions.Favorite;
using KonigDev.Lazurit.Hub.Grains.Favorites.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Model.DTO.Favorite.Command;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using KonigDev.Lazurit.Core.Transactions;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.Favorites
{
    [TestFixture]
    public class AddFavoriteItemCommandHandlerTests
    {
        private Mock<IDBContextFactory> _contextFactoryMock;
        private Mock<DbContext> _contextMock;
        private Guid _itemId;
        private Guid _userId;
        private AddFavoriteItemCommandHandler _classForTest;
        private Mock<ITransactionFactory> _transactionFactoryMock;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _itemId = Guid.NewGuid();
            _userId = Guid.NewGuid();
            _transactionFactoryMock = new Mock<ITransactionFactory>();

            _contextMock.Setup(p => p.Set<Favorite>()).Returns(new List<Favorite>
            {
                new Favorite {
                    Id = _userId,
                    Products = new List<FavoriteItemProduct> { new FavoriteItemProduct { ProductId = _itemId } },
                    Complects = new List<FavoriteItemComplect> { new FavoriteItemComplect { CollectionVariantId = _itemId} },
                    Collections = new List<FavoriteItemCollection> { new FavoriteItemCollection { CollectionVariantId = _itemId} }
                }
            }.GetQueryableMockDbSet());
            
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _classForTest = new AddFavoriteItemCommandHandler(_contextFactoryMock.Object, _transactionFactoryMock.Object);
        }

        [Test]
        public void AddFavoriteItemCommandHandler_HasProduct()
        {
            //arrange
            var command = new AddFavoriteItemCommand { UserId = _userId, AddElementId = _itemId, FavoriteItemType = Core.Enums.EnumFavoriteItemType.Product };
            //act
            //assert    
            Assert.ThrowsAsync<NotValidExeption>(async () => await _classForTest.Execute(command));
        }


        [Test]
        public void AddFavoriteItemCommandHandler_HasCollection()
        {
            //arrange
            var command = new AddFavoriteItemCommand { UserId = _userId, AddElementId = _itemId, FavoriteItemType = Core.Enums.EnumFavoriteItemType.Collection };
            //act
            //assert    
            Assert.ThrowsAsync<NotValidExeption>(async () => await _classForTest.Execute(command));
        }


        [Test]
        public void AddFavoriteItemCommandHandler_HasComplekt()
        {
            //arrange
            var command = new AddFavoriteItemCommand { UserId = _userId, AddElementId = _itemId, FavoriteItemType = Core.Enums.EnumFavoriteItemType.Complect };
            //act
            //assert    
            Assert.ThrowsAsync<NotValidExeption>(async () => await _classForTest.Execute(command));
        }
    }
}
