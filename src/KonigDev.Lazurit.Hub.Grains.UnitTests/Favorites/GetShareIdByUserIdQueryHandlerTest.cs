﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Favorites.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Favorite.Query;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.Favorites
{
    [TestFixture]
    public class GetShareIdByUserIdQueryHandlerTest
    {
        private Mock<IDBContextFactory> _contextFactoryMock;
        private Mock<DbContext> _contextMock;
        private Guid _linkId;
        private Guid _userId;
        private GetShareIdByUserIdQueryHandler _classForTest;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _userId = Guid.NewGuid();
            _linkId = Guid.NewGuid();

            _contextMock.Setup(p => p.Set<UserProfile>()).Returns(new List<UserProfile>
            {
                new UserProfile {
                    Id = _userId,
                    LinkId = _linkId
        }
            }.GetQueryableMockDbSet());

            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _classForTest = new GetShareIdByUserIdQueryHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task GetShareIdByUserIdQueryHandler_CorrectId_HasUserWithId()
        {
            //arrange
            var command = new GetShareIdByUserIdQuery { UserId = _userId };
            //act
            var res = await _classForTest.Execute(command);
            //assert    
            Assert.IsTrue(res.Id == _linkId);
        }


        [Test]
        public async Task GetShareIdByUserIdQueryHandler_IncorrectId_HasNoUserWithId()
        {
            //arrange
            var command = new GetShareIdByUserIdQuery { UserId = Guid.NewGuid() };
            //act
            var res = await _classForTest.Execute(command);
            //assert    
            Assert.IsNull(res);
        }
    }
}
