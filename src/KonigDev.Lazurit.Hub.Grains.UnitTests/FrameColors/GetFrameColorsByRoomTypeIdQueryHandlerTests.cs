﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.FrameColors.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Query;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.FrameColors
{
    [TestFixture]
    public class GetFrameColorsByRoomTypeIdQueryHandlerTests
    {
        Mock<IDBContextFactory> _contextFactoryMock;
        Mock<DbContext> _contextMock;
        Guid _roomTypeId;
        Guid _collectionId;        
        GetFrameColorsByRoomTypeIdQueryHandler _getFrameColorsByRoomTypeIdQueryHandler;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _roomTypeId = Guid.NewGuid();
            _collectionId = Guid.NewGuid();
            var collection = new Collection
            {
                Id = _collectionId,
                RoomTypeId = _roomTypeId
            };
            _contextMock.Setup(x => x.Set<Collection>()).Returns(new List<Collection>
            {
                collection
            }.GetQueryableMockDbSet());
            _contextMock.Setup(x => x.Set<CollectionVariant>()).Returns(new List<CollectionVariant>
            {
                new CollectionVariant {Id = Guid.NewGuid(), CollectionId =  _collectionId}
            }.GetQueryableMockDbSet());
            _contextMock.Setup(x => x.Set<FrameColor>()).Returns(new List<FrameColor>
            {
                new FrameColor
                {
                    Id = Guid.NewGuid()       ,
                     CollectionsVariants = new List<CollectionVariant> {new CollectionVariant {  Id = _collectionId, Collection = new Collection { RoomType = new RoomType { Id = _roomTypeId} } } }
                 }
            }.GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _getFrameColorsByRoomTypeIdQueryHandler = new GetFrameColorsByRoomTypeIdQueryHandler(_contextFactoryMock.Object);
        }

        [Test]        
        public async Task Execute_RequestIsValid_GetFrameColors()
        {
            //arrange

            //act
            var res = await _getFrameColorsByRoomTypeIdQueryHandler.Execute(new GetFrameColorsByRoomTypeIdQuery
            {
                RoomTypeId = _roomTypeId
            });

            //assert
            Assert.True(res.Count == 1);
        }

        [Test]
        public async Task Execute_RequestIsEmpty_FrameColorsAreEmpty()
        {
            //arrange
           
            //act
            var res = await _getFrameColorsByRoomTypeIdQueryHandler.Execute(new GetFrameColorsByRoomTypeIdQuery());

            //assert
            Assert.True(res.Count == 0);
        }
    }
}
