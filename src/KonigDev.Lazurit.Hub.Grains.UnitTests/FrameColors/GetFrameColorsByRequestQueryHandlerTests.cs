﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.FrameColors.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Query;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.FrameColors
{
    [TestFixture]
    public class GetFrameColorsByRequestQueryHandlerTests
    {
        Mock<IDBContextFactory> _contextFactoryMock;
        Mock<DbContext> _contextMock;
        Guid _articleId;
        Guid _collectionId;
        GetFrameColorsByCollectionQueryHandler _getFacingColorsByRequestQueryHandler;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _articleId = Guid.NewGuid();
            _collectionId = Guid.NewGuid();

            _contextMock.Setup(x => x.Set<FrameColor>()).Returns(new List<FrameColor>()
            {
                new FrameColor
                {
                    Id = Guid.NewGuid(),
                    CollectionsVariants = new List<CollectionVariant> {new CollectionVariant {  CollectionId = _collectionId} }
                 },
                 new FrameColor
                {
                    Id = Guid.NewGuid(),
                     CollectionsVariants = new List<CollectionVariant> {new CollectionVariant {  CollectionId = Guid.NewGuid()} }
                 },
            }.GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _getFacingColorsByRequestQueryHandler = new GetFrameColorsByCollectionQueryHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task Execute_RequestIsValid_GetFacingColors()
        {
            //arrange
           

            //act
            var res = await _getFacingColorsByRequestQueryHandler.Execute(new GetFrameColorsByCollectionQuery
            {
                ArticleId = _articleId,
                CollectionId = _collectionId
            });

            //assert
            Assert.True(res.Count == 1);
        }

        [Test]
        public async Task Execute_RequestIsEmpty_FacingColorsAreEmpty()
        {
            //arrange
           
            //act
            var res = await _getFacingColorsByRequestQueryHandler.Execute(new GetFrameColorsByCollectionQuery());

            //assert
            Assert.True(res.Count == 0);
        }
    }
}
