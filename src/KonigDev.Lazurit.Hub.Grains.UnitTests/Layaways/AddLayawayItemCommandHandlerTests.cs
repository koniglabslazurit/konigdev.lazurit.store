﻿using KonigDev.Lazurit.Core.Enums.Exeptions.Cart;
using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Layaways.CommandHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Layaways.Command;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.Layaways
{
    [TestFixture]
    public class AddLayawayItemCommandHandlerTests
    {
        private Mock<IDBContextFactory> _contextFactoryMock;
        private Mock<DbContext> _contextMock;

        private Guid _layawayId;
        private AddLayawayItemCommandHandler _classForTest;
        private Mock<ITransactionFactory> _transactionFactoryMock;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _transactionFactoryMock = new Mock<ITransactionFactory>();

            _layawayId = Guid.NewGuid();

            _contextMock.Setup(p => p.Set<Layaway>()).Returns(new List<Layaway> { new Layaway() }.GetQueryableMockDbSet());

            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _classForTest = new AddLayawayItemCommandHandler(_contextFactoryMock.Object, _transactionFactoryMock.Object);
        }

        [Test]
        public void AddLayawayItemCommandHandler_HasCollection()
        {
            //arrange
            var command = new AddLayawayItemCommand();
            //act
            var result = _classForTest.Execute(command);
            //assert    
            Assert.True(result.IsCompleted);
        }
    }
}
