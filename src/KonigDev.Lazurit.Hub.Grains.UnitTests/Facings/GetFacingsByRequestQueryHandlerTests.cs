﻿using KonigDev.Lazurit.Hub.Grains.Facings.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Model.DTO.Facings.Query;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.Facings
{
    [TestFixture]
    public class GetFacingsByRequestQueryHandlerTests
    {
        Mock<IDBContextFactory> _contextFactoryMock;
        Mock<DbContext> _contextMock;
        Guid _articleId;
        Guid _collectionId;
        GetFacingsByCollectionQueryHandler _getFacingsByRequestQueryHandler;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _articleId = Guid.NewGuid();
            _collectionId = Guid.NewGuid();

            _contextMock.Setup(x => x.Set<Facing>()).Returns(new List<Facing>()
            {
                new Facing
                {
                    Id = Guid.NewGuid()      ,
                      CollectionsVariants = new List<CollectionVariant> {new CollectionVariant {  CollectionId = _collectionId} }
                 },
                 new Facing
                {
                    Id = Guid.NewGuid()        ,
                     CollectionsVariants = new List<CollectionVariant> {new CollectionVariant {  CollectionId = Guid.NewGuid()} }
                 },
            }.GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _getFacingsByRequestQueryHandler = new GetFacingsByCollectionQueryHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task Execute_RequestIsValid_GetFacings()
        {
            //arrange

            //act
            var res = await _getFacingsByRequestQueryHandler.Execute(new GetFacingsByCollectionQuery
            {
                ArticleId = _articleId,
                CollectionId = _collectionId
            });

            //assert
            Assert.True(res.Count == 1);
        }

        [Test]
        public async Task Execute_RequestIsEmpty_FacingsAreEmpty()
        {
            //arrange

            //act
            var res = await _getFacingsByRequestQueryHandler.Execute(new GetFacingsByCollectionQuery());

            //assert
            Assert.True(res.Count == 0);
        }
    }
}
