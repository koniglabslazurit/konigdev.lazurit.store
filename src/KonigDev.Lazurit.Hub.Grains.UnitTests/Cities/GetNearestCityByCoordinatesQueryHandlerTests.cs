﻿using KonigDev.Lazurit.Hub.Grains.Cities.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Factories;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Model.Entities;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Queries;
using KonigDev.Lazurit.Core.Enums.Exeptions.Common;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.Cities
{
    [TestFixture]
    public class GetNearestCityByCoordinatesQueryHandlerTests
    {
        private Mock<IDBContextFactory> _contextFactoryMock;
        private Mock<DbContext> _contextMock;
        private GetNearestCityByCoordinatesQueryHandler _serviceForTest;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _serviceForTest = new GetNearestCityByCoordinatesQueryHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task GetNearestCityByCoordinates_ShouldReturnNearestCity_IfValidQuery()
        {
            //arrange
            var cities = new List<City>
            {
                new City
                {
                    Id  =Guid.NewGuid(),
                    Title = "Kaliningrad",
                    Latitude = 54,
                    Longitude = 20,
                    PriceZoneId = Guid.NewGuid(),
                    RegionId = Guid.NewGuid()
                },
                new City
                {
                    Id  =Guid.NewGuid(),
                    Title = "Leningrad",
                    Latitude = 59,
                    Longitude = 30,
                    PriceZoneId = Guid.NewGuid(),
                    RegionId = Guid.NewGuid()
                },
                new City
                {
                    Id  =Guid.NewGuid(),
                    Title = "Moscow",
                    Latitude = 55,
                    Longitude = 37,
                    PriceZoneId = Guid.NewGuid(),
                    RegionId = Guid.NewGuid()
                }
            };
            _contextMock.Setup(x => x.Set<City>()).Returns(cities.GetQueryableMockDbSet());
            var query = new GetCityByCoordinates
            {
                Latitude = 54.70F,
                Longitude = 20.51F
            };
            //act
            var actual = await _serviceForTest.Execute(query);
            //assert
            Assert.IsInstanceOf<DtoCityCoordinates>(actual);
            Assert.AreEqual("Kaliningrad", actual.Title);
        }

        [Test]
        public void GetNearestCityByCoordinates_ShouldReturnException_IfCitiesNotFound()
        {
            //arrange
            var cities = new List<City>();
            _contextMock.Setup(x => x.Set<City>()).Returns(cities.GetQueryableMockDbSet());
            var query = new GetCityByCoordinates
            {
                Latitude = 54.70F,
                Longitude = 20.51F
            };
            //act
            //assert
            Assert.Throws<NotValidCommandException>(() => _serviceForTest.Execute(query));
        }

        [Test]
        public void GetNearestCityByCoordinates_ShouldReturnException_IfQueryIsNull()
        {
            //arrange
            GetCityByCoordinates query = null;
            //act
            //assert
            Assert.Throws<NotValidCommandException>(() => _serviceForTest.Execute(query));
        }
    }
}
