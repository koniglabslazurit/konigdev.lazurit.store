﻿using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Hub.Grains.Cities.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Queries;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.Cities
{
    [TestFixture]
    public class GetAllCitiesQueryHandlerTests
    {
        private Mock<IDBContextFactory> _contextFactoryMock;
        private Mock<DbContext> _contextMock;
        private GetAllCitiesQueryHandler _serviceForTest;
        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _serviceForTest = new GetAllCitiesQueryHandler(_contextFactoryMock.Object);
        }
        [Test]
        public async Task GetAllCitiesQueryHandler_ShouldReturnAll_IfTheyExist()
        {
            //arrange
            var cities = new List<City>
            {
                new City
                {
                    Id  =Guid.NewGuid(),
                    Title = "Kaliningrad",
                    Latitude = 54,
                    Longitude = 20,
                    PriceZoneId = Guid.NewGuid(),
                    RegionId = Guid.NewGuid(),
                    CodeKladr = "123",
                    SyncCode1C ="0001"
                },
                new City
                {
                    Id  =Guid.NewGuid(),
                    Title = "Leningrad",
                    Latitude = 59,
                    Longitude = 30,
                    PriceZoneId = Guid.NewGuid(),
                    RegionId = Guid.NewGuid(),
                    CodeKladr = "555",
                    SyncCode1C ="0002"
                },
                new City
                {
                    Id  =Guid.NewGuid(),
                    Title = "Moscow",
                    Latitude = 55,
                    Longitude = 37,
                    PriceZoneId = Guid.NewGuid(),
                    RegionId = Guid.NewGuid(),
                    SyncCode1C = "0003",
                    CodeKladr = "999"
                }
            };
            _contextMock.Setup(x => x.Set<City>()).Returns(cities.GetQueryableMockDbSet());
            var query = new GetAllCitiesQuery();
            //act
            var actual = await _serviceForTest.Execute(query);
            //assert
            Assert.IsInstanceOf<List<DtoCity>>(actual);
            Assert.AreEqual(3, actual.Count);
        }
        [Test]
        public void GetAllCitiesQueryHandler_ShouldReturnException_IfCitiesNotFound()
        {
            //arrange
            var cities = new List<City>();
            _contextMock.Setup(x => x.Set<City>()).Returns(cities.GetQueryableMockDbSet());
            var query = new GetAllCitiesQuery();
            //act
            //assert
            Assert.Throws<NotValidCommandException>(() => _serviceForTest.Execute(query));
        }
    }
}
