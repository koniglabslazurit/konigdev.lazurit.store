﻿using KonigDev.Lazurit.Hub.Grains.Collections.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Model.DTO.Collections;
using KonigDev.Lazurit.Model.Context.DataContext;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Core.Extensions;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Dto;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.Collections
{
    [TestFixture]
    public class GetCollectionByRequestQueryHandlerTests
    {
        private Mock<IDBContextFactory> _contextFactory;
        private Mock<DbContext> _context;
        private GetCollectionsByRequestQueryHandler _service;
        DbSet<CollectionVariant> _variables;
        DbSet<Collection> _collections;
        Guid _roomTypeId;
        string _roomAlias;
        [SetUp]
        public void SetUp()
        {
            _contextFactory = new Mock<IDBContextFactory>();
            _context = new Mock<DbContext>();
            var context = new LazuritBaseContext();
            _contextFactory.Setup(x => x.CreateLazuritContext()).Returns(_context.Object);
            _service = new GetCollectionsByRequestQueryHandler(_contextFactory.Object);
            string testRoomName = "Bedroom";
            _roomTypeId = Guid.NewGuid();
            _roomAlias = testRoomName.Transliterate();
            var col1 = Guid.NewGuid();
            var col2 = Guid.NewGuid();
            var col3 = Guid.NewGuid();
            var col4 = Guid.NewGuid();
            _variables = new List<CollectionVariant>
            {
                new CollectionVariant
                {
                    Id = Guid.NewGuid(),
                    CollectionId = col1,

                },
                new CollectionVariant
                {
                    Id = Guid.NewGuid(),
                    CollectionId = col2
                },
                new CollectionVariant
                {
                    Id = Guid.NewGuid(),
                    CollectionId = col3,
                },
                new CollectionVariant
                {
                    Id = Guid.NewGuid(),
                    CollectionId = col4
                }
            }.GetQueryableMockDbSet();

            _collections = new List<Collection>
            {
                new Collection {
                    Id = col1,
                    Series = new Series {Id = Guid.NewGuid(), Name= "Magna" },
                    RoomTypeId =_roomTypeId,
                    RoomType = new RoomType {Id = _roomTypeId, Name =testRoomName,Alias = _roomAlias},
                   CollectionVariants = _variables.ToList()
                },
                new Collection {
                    Id = col2,
                    Series  =new Series {Id = Guid.NewGuid(),Name="Gabriel" },
                    RoomTypeId =_roomTypeId,
                    RoomType = new RoomType {Id = _roomTypeId, Name =testRoomName,Alias = _roomAlias },
                     CollectionVariants = _variables.ToList()
                },
                new Collection {
                    Id = col3,
                    Series = new Series {Id = Guid.NewGuid(),Name="Sara" },
                    RoomTypeId =_roomTypeId,
                    RoomType = new RoomType {Id = _roomTypeId, Name =testRoomName,Alias = _roomAlias },
                     CollectionVariants = _variables.ToList()
                },
                new Collection {
                    Id = col4,
                    Series = new Series {Id = Guid.NewGuid(),Name="Vina" },
                    RoomTypeId =Guid.NewGuid(),
                    RoomType = new RoomType {Id = _roomTypeId, Name ="Childrens",Alias = "Childrens".Transliterate() },
                     CollectionVariants = _variables.ToList()
                }
            }.GetQueryableMockDbSet();
        }

        [Test]
        public async Task ReturnCollectionWhenValidId()
        {
            //arrange
            _context.Setup(c => c.Set<Collection>()).Returns(_collections);
            _context.Setup(c => c.Set<CollectionVariant>()).Returns(_variables);

            //act
            var actual = await _service.Execute(new GetCollectionsQuery { RoomAlias = _roomAlias });
            //assert
            Assert.NotNull(actual);
            Assert.IsInstanceOf<List<DtoCollectionResultForCatalog>>(actual);
            Assert.AreEqual(3, actual.Count);
        }

        [Test]

        public async Task ReturnCollectionWithoutVariantsWhenCollectionHaveNotVariants()
        {
            //arrange
            var emptyCollections = new List<Collection>().GetQueryableMockDbSet();
            _context.Setup(c => c.Set<Collection>()).Returns(emptyCollections);

            //act
            var actual = await _service.Execute(new GetCollectionsQuery { RoomAlias = _roomAlias });
            //assert
            Assert.NotNull(actual);
            Assert.IsInstanceOf<List<DtoCollectionResultForCatalog>>(actual);
            Assert.AreEqual(0, actual.Count);
        }
    }
}
