﻿using KonigDev.Lazurit.Hub.Grains.Collections.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Query;
using KonigDev.Lazurit.Model.Context.DataContext;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.Collections
{
    [TestFixture]
    public class GetCollectionsByFilterQueryHandlerTests
    {
        private Mock<IDBContextFactory> _contextFactory;
        private Mock<DbContext> _context;
        private GetCollectionsByFilterQueryHandler _classForTest;
        Guid _roomTypeId;
        [SetUp]
        public void SetUp()
        {
            _contextFactory = new Mock<IDBContextFactory>();
            _context = new Mock<DbContext>();
            _contextFactory.Setup(x => x.CreateLazuritContext()).Returns(_context.Object);
            _classForTest = new GetCollectionsByFilterQueryHandler(_contextFactory.Object);
            _roomTypeId = Guid.NewGuid();
        }
        [Test]
        public async Task Execute_FacingIdIsValid_ResponseIsNotEmpty()
        {
            //arrange
            var collectionId = Guid.NewGuid();
            var facingId = Guid.NewGuid();
            _context.Setup(x => x.Set<Collection>()).Returns(new List<Collection>
            {
               new Collection
               {
                   Id = collectionId,
                   RoomTypeId = _roomTypeId,
                   Series = new Series { Name = ""},
                   RoomType = new RoomType { Name = ""},
                   CollectionVariants = new List<CollectionVariant>
                   {
                       new CollectionVariant
                       {
                           Id = Guid.NewGuid(),
                           CollectionId = collectionId,                               
                           FacingId = facingId,
                           Facing = new Facing(),
                           FrameColor = new FrameColor(),
                           FacingColor = new FacingColor()
                       }
                   }
               },
                new Collection
               {
                   Id = collectionId,
                   RoomTypeId = _roomTypeId,
                   CollectionVariants = new List<CollectionVariant>
                   {
                       new CollectionVariant
                       {
                           Id = Guid.NewGuid(),
                           CollectionId = collectionId,
                           Facing = new Facing(),
                           FrameColor = new FrameColor(),
                           FacingColor = new FacingColor()
                       }
                   }
               },
            }.GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(new GetCollectionsByFilterQuery { RoomTypeId = _roomTypeId, FacingId = facingId });
            //assert
            Assert.True(result.Count == 1);
        }

        [Test]
        public async Task Execute_WithOutFilter_ReturnAllCollections()
        {
            //arrange
            var collectionId = Guid.NewGuid();
            var facingId = Guid.NewGuid();
            _context.Setup(x => x.Set<Collection>()).Returns(new List<Collection>
            {
               new Collection
               {
                   Id = collectionId,
                   RoomTypeId = _roomTypeId,
                   Series = new Series { Name = ""},
                   RoomType = new RoomType { Name = ""},
                   CollectionVariants = new List<CollectionVariant>
                   {
                       new CollectionVariant
                       {
                           Id = Guid.NewGuid(),
                           CollectionId = collectionId
                       }
                   }
               },
                new Collection
               {
                   Id = collectionId,
                   RoomTypeId = _roomTypeId,
                   Series = new Series { Name = ""},
                   RoomType = new RoomType { Name = ""},
                   CollectionVariants = new List<CollectionVariant>
                   {
                       new CollectionVariant
                       {
                           Id = Guid.NewGuid(),
                           CollectionId = collectionId,
                       }
                   }
               },
            }.GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(new GetCollectionsByFilterQuery { RoomTypeId = _roomTypeId });
            //assert
            Assert.True(result.Count == 2);
        }
        [Test]
        public async Task Execute_FacingColorIdIsValid_ResponseIsNotEmpty()
        {
            //arrange
            var collectionId = Guid.NewGuid();
            var facingColorId = Guid.NewGuid();
            _context.Setup(x => x.Set<Collection>()).Returns(new List<Collection>
            {
               new Collection
               {
                   Id = collectionId,
                   RoomTypeId = _roomTypeId,
                   Series = new Series { Name = ""},
                   RoomType = new RoomType { Name = ""},
                   CollectionVariants = new List<CollectionVariant>
                   {
                       new CollectionVariant
                       {
                           Id = Guid.NewGuid(),
                           CollectionId = collectionId,
                           FacingColorId = facingColorId,
                           Facing = new Facing(),
                           FrameColor = new FrameColor(),
                           FacingColor = new FacingColor()
                       }
                   }
               },
                new Collection
               {
                   Id = collectionId,
                   RoomTypeId = _roomTypeId,
                   CollectionVariants = new List<CollectionVariant>
                   {
                       new CollectionVariant
                       {
                           Id = Guid.NewGuid(),
                           CollectionId = collectionId,
                           Facing = new Facing(),
                           FrameColor = new FrameColor(),
                           FacingColor = new FacingColor()
                       }
                   }
               },
            }.GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(new GetCollectionsByFilterQuery { RoomTypeId = _roomTypeId, FacingColorId = facingColorId });
            //assert
            Assert.True(result.Count == 1);
        }
        [Test]
        public async Task Execute_FrameColorIdIsValid_ResponseIsNotEmpty()
        {
            //arrange
            var collectionId = Guid.NewGuid();
            var frameColorId = Guid.NewGuid();
            _context.Setup(x => x.Set<Collection>()).Returns(new List<Collection>
            {
               new Collection
               {
                   Id = collectionId,
                   RoomTypeId = _roomTypeId,
                   Series = new Series { Name = ""},
                   RoomType = new RoomType { Name = ""},
                   CollectionVariants = new List<CollectionVariant>
                   {
                       new CollectionVariant
                       {
                           Id = Guid.NewGuid(),
                           CollectionId = collectionId,
                             FrameColorId = frameColorId,
                           Facing = new Facing(),
                           FrameColor = new FrameColor(),
                           FacingColor = new FacingColor()
                       }
                   }
               },
                new Collection
               {
                   Id = collectionId,
                   RoomTypeId = _roomTypeId,
                   CollectionVariants = new List<CollectionVariant>
                   {
                       new CollectionVariant
                       {
                           Id = Guid.NewGuid(),
                           CollectionId = collectionId,
                           Facing = new Facing(),
                           FrameColor = new FrameColor(),
                           FacingColor = new FacingColor()
                       }
                   }
               },
            }.GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(new GetCollectionsByFilterQuery { RoomTypeId = _roomTypeId, FrameColorId = frameColorId });
            //assert
            Assert.True(result.Count == 1);
        }
    }
}
