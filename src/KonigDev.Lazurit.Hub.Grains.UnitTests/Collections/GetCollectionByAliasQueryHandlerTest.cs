﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Articles.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Collections.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Model.DTO.Article.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Query;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.Collections
{
    [TestFixture]
    class GetCollectionByAliasQueryHandlerTest
    {
        Mock<IDBContextFactory> _contextFactoryMock;
        Mock<DbContext> _contextMock;
        GetCollectionByAliasQueryHandler _getCollectionByAliasQueryHandler;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();

            _contextMock.Setup(x => x.Set<Collection>()).Returns(new List<Collection>()
            {
                new Collection
                {
                    Series = new Series
                    {
                        Alias = "valid"
                    },
                    RoomType = new RoomType {Alias = "" }
                },
                new Collection
                {
                    Series = new Series
                    {
                        Alias = "test"
                    },
                    RoomType = new RoomType {Alias = "" }
                }
            }.GetQueryableMockDbSet());

            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _getCollectionByAliasQueryHandler = new GetCollectionByAliasQueryHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task Execute_CorrectAlias_ResponseIsNotNull()
        {
            //arrange

            //act
            var res = await _getCollectionByAliasQueryHandler.Execute(new GetCollectionByAliasQuery
            {
                Alias = "valid"
            });

            //assert
            Assert.IsNotNull(res);
        }

        [Test]
        public async Task Execute_IncorrectAlias_ResponseIsNull()
        {
            //arrange

            //act
            var res = await _getCollectionByAliasQueryHandler.Execute(new GetCollectionByAliasQuery
            {
                Alias = "invalid"
            });

            //assert
            Assert.IsNull(res);
        }

        [Test]
        public void Execute_AliasIsNull_Exeption()
        {
            //arrange

            //act

            //assert
            Assert.ThrowsAsync<NotSupportedException>(
                async () => await _getCollectionByAliasQueryHandler.Execute(new GetCollectionByAliasQuery()));

        }
    }
}
