﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Articles.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Model.DTO.Article;
using KonigDev.Lazurit.Hub.Model.DTO.Article.Query;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Query;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.Articles
{
    [TestFixture]
    class GetArticleByUniqueFieldsQueryHandlerTests
    {
        Mock<IDBContextFactory> _contextFactoryMock;
        Mock<DbContext> _contextMock;
        Guid _articleId;
        string _articleSyncCode1C;
        GetArticleByUniqueFieldsQueryHandler _getArticleByUniqueFieldsQueryHandler;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _articleId = Guid.NewGuid();
            _articleSyncCode1C = "0001";

            _contextMock.Setup(x => x.Set<ProductPack>()).Returns(new List<ProductPack>()
            {
             new ProductPack
             {
                 ProductSyncCode1C = _articleSyncCode1C
             }   
            }.GetQueryableMockDbSet());
            _contextMock.Setup(x => x.Set<Article>()).Returns(new List<Article>()
            {
                new Article
                {
                    Id = _articleId,
                    SyncCode1C = _articleSyncCode1C,
                    FurnitureType = new FurnitureType
                    {
                        Name = "Диван"
                    },
                    Products = new List<Product>
                    {
                        new Product
                        {
                            Facing = new Facing(),
                            FacingColor = new FacingColor(),
                            FrameColor = new FrameColor()
                        },
                        new Product
                        {
                            Facing = new Facing(),
                            FacingColor = new FacingColor(),
                            FrameColor = new FrameColor()
                        }
                    }
                },
                new Article
                {
                    Id = Guid.NewGuid(),
                    SyncCode1C = "test",
                    FurnitureType = new FurnitureType
                    {
                        Name = "Шкаф"
                    },
                    Products = new List<Product>
                    {
                        new Product
                        {
                            Facing = new Facing(),
                            FacingColor = new FacingColor(),
                            FrameColor = new FrameColor()
                        },
                        new Product
                        {
                            Facing = new Facing(),
                            FacingColor = new FacingColor(),
                            FrameColor = new FrameColor()
                        }
                    }
                }
            }.GetQueryableMockDbSet());

            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _getArticleByUniqueFieldsQueryHandler = new GetArticleByUniqueFieldsQueryHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task Execute_ArticleIdIsValid_ResponseIsNotNull()
        {
            //arrange

            //act
            var res = await _getArticleByUniqueFieldsQueryHandler.Execute(new GetArticleByUniqueFieldsQuery
            {
                ArticleId = _articleId,
                ArticleSyncCode1C = string.Empty
            });

            //assert
            Assert.IsNotNull(res);
            Assert.True(res.Facings.Count >= 1);
            Assert.True(res.FacingsColors.Count >= 1);
            Assert.True(res.FramesColors.Count >= 1);
        }

        [Test]
        public async Task Execute_ArticleSyncCodeIsValid_ResponseIsNotNull()
        {
            //arrange

            //act
            var res = await _getArticleByUniqueFieldsQueryHandler.Execute(new GetArticleByUniqueFieldsQuery
            {
                ArticleId = null,
                ArticleSyncCode1C = _articleSyncCode1C
            });

            //assert
            Assert.IsNotNull(res);
            Assert.True(res.Facings.Count >= 1);
            Assert.True(res.FacingsColors.Count >= 1);
            Assert.True(res.FramesColors.Count >= 1);
        }

        [Test]
        public async Task Execute_ArticleIdIsInvalid_ResponseIsNull()
        {
            //arrange

            //act
            var res = await _getArticleByUniqueFieldsQueryHandler.Execute(new GetArticleByUniqueFieldsQuery
            {
                ArticleId = Guid.NewGuid(),
                ArticleSyncCode1C = string.Empty
            });

            //assert
            Assert.IsNull(res);
        }

        [Test]
        public async Task Execute_ArticleSyncCodeIsInvalid_ResponseIsNull()
        {
            //arrange

            //act
            var res = await _getArticleByUniqueFieldsQueryHandler.Execute(new GetArticleByUniqueFieldsQuery
            {
                ArticleId = null,
                ArticleSyncCode1C = "invalid"
            });

            //assert
            Assert.IsNull(res);
        }

        [Test]
        public void Execute_BothArgumentsAreSended_Exeption()
        {
            //arrange

            //act

            //assert
            Assert.ThrowsAsync<NotSupportedException>(async () => await _getArticleByUniqueFieldsQueryHandler.Execute(new GetArticleByUniqueFieldsQuery
            {
                ArticleId = _articleId,
                ArticleSyncCode1C = _articleSyncCode1C
            }));
        }

        [Test]
        public void Execute_BothArgumentsAreNulls_Exeption()
        {
            //arrange

            //act

            //assert
            Assert.ThrowsAsync<NotSupportedException>(
                async () => await _getArticleByUniqueFieldsQueryHandler.Execute(new GetArticleByUniqueFieldsQuery
                {
                    ArticleId = null,
                    ArticleSyncCode1C = string.Empty
                }));
        }
    }
}
