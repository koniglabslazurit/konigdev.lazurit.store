﻿using KonigDev.Lazurit.Hub.Grains.Articles.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Model.DTO.Article.Query;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.Articles
{
    [TestFixture]
    public class GetAvailableFiltersByArticleQueryHandlerTests
    {
        private Mock<IDBContextFactory> _contextFactoryMock;
        private Mock<DbContext> _contextMock;
        private Guid _articleId;
        private GetAvailableFiltersByArticleQueryHandler _classForTest;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _articleId = Guid.NewGuid();

            _contextMock.Setup(p => p.Set<Product>()).Returns(GetProducts.GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _classForTest = new GetAvailableFiltersByArticleQueryHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task GetAvailableFiltersByArticleQueryHandler_Execute_ExistProducts_AliavableFacingColorsIsCotrrect()
        {
            //arrange
            var request = new GetAvailableFiltersByArticleQuery { ArticleId = _articleId };
            //act
            var result = await _classForTest.Execute(request);
            var aliavableFacingColors = result.Where(p => p.Name == "FrameColorBlack").FirstOrDefault();
            //assert
            Assert.IsTrue(result.Count == 1);

            Assert.IsTrue(aliavableFacingColors.AliavableFacings.Where(p => p.Name == "FacingBamboo").Count() == 1);
            Assert.IsTrue(aliavableFacingColors.AliavableFacings.Where(p => p.Name == "FacingGloss").Count() == 0);
            Assert.IsTrue(aliavableFacingColors.AliavableFacings.Any(p => p.AliavableFacingColors.Any(d => d.Name == "FacingColorMilk")));
        }

        [Test]
        public async Task GetAvailableFiltersByArticleQueryHandler_Execute_MissingProducts_AliavableFiltersIsEmpty()
        {
            //arrange
            var request = new GetAvailableFiltersByArticleQuery { ArticleId = Guid.NewGuid() };
            //act
            var result = await _classForTest.Execute(request);
            //assert
            Assert.IsTrue(result.Count == 0);
        }

        private List<Product> GetProducts
        {
            get
            {
                return new List<Product>
                {
                    new Product {
                        ArticleId = _articleId,

                        FrameColor = FrameColorBlack,
                        FrameColorId = FrameColorBlack.Id,

                        Facing = FacingBamboo,
                        FacingId = FacingBamboo.Id,

                        FacingColor = FacingColorCofee,
                        FacingColorId = FacingColorCofee.Id
                    },

                    new Product {
                        ArticleId = _articleId,

                        FrameColor = FrameColorBlack,
                        FrameColorId = FrameColorBlack.Id,

                        Facing = FacingBamboo,
                        FacingId = FacingBamboo.Id,

                        FacingColor = FacingColorMilk,
                        FacingColorId = FacingColorMilk.Id
                    },
                    new Product{
                        ArticleId = Guid.NewGuid(),

                        FrameColor = FrameColorBlack,
                        FrameColorId = FrameColorBlack.Id,

                        Facing = FacingBamboo,
                        FacingId = FacingBamboo.Id,

                        FacingColor = FacingColorMilk,
                        FacingColorId = FacingColorMilk.Id
                    },
                };
            }
        }

        /* цвета корпуса */
        private FrameColor FrameColorBlack = new FrameColor { Id = Guid.NewGuid(), Name = "FrameColorBlack" };
        private FrameColor FrameColorWhite = new FrameColor { Id = Guid.NewGuid(), Name = "FrameColorWhite" };

        /* отделка */
        private Facing FacingGloss = new Facing { Id = Guid.NewGuid(), Name = "FacingGloss" };
        private Facing FacingBamboo = new Facing { Id = Guid.NewGuid(), Name = "FacingBamboo" };

        /* цвет отделки */
        private FacingColor FacingColorMilk = new FacingColor { Id = Guid.NewGuid(), Name = "FacingColorMilk" };
        private FacingColor FacingColorCofee = new FacingColor { Id = Guid.NewGuid(), Name = "FacingColorCofee" };

    }
}
