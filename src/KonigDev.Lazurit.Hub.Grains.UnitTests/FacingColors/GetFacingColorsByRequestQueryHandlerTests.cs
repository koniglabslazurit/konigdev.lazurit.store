﻿using KonigDev.Lazurit.Hub.Grains.FacingColors.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Query;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.FacingColors
{
    [TestFixture]
    public class GetFacingColorsByRequestQueryHandlerTests
    {
        Mock<IDBContextFactory> _contextFactoryMock;
        Mock<DbContext> _contextMock;
        Guid _articleId;
        Guid _collectionId;
        GetFacingColorsByCollectionQueryHandler _getFacingColorsByRequestQueryHandler;

       [SetUp]
        public void SetUp()
        {
            _articleId = Guid.NewGuid();
            _collectionId = Guid.NewGuid();
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _contextMock.Setup(x => x.Set<FacingColor>()).Returns(new List<FacingColor>()
            {
                new FacingColor
                {
                    Id = Guid.NewGuid(), CollectionsVariants = new List<CollectionVariant> { new CollectionVariant {CollectionId = _collectionId } }
                 },
                 new FacingColor
                {
                    Id = Guid.NewGuid(), CollectionsVariants = new List<CollectionVariant> { { new CollectionVariant { CollectionId = _collectionId } } }
                 },
            }.GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);

            _getFacingColorsByRequestQueryHandler = new GetFacingColorsByCollectionQueryHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task Execute_RequestIsValid_GetFacingColors()
        {
            //arrange
            
            //act
            var res = await _getFacingColorsByRequestQueryHandler.Execute(new GetFacingColorsByCollectionQuery
            {
                ArticleId = _articleId,
                CollectionId = _collectionId
            });

            //assert            
            Assert.True(res.Count == 2);
        }

        [Test]
        public async Task Execute_RequestIsEmpty_FacingColorsAreEmpty()
        {
            //arrange

            //act
            var res = await _getFacingColorsByRequestQueryHandler.Execute(new GetFacingColorsByCollectionQuery());

            //assert
            Assert.True(res.Count == 0);
        }
    }
}
