﻿using KonigDev.Lazurit.Hub.Grains.FacingColors.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Query;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.FacingColors
{
    [TestFixture]
    public class GetFacingColorsByRoomTypeIdQueryHandlerTests
    {
        Mock<IDBContextFactory> _contextFactoryMock;
        Mock<DbContext> _contextMock;
        Guid _roomTypeId;
        private Guid collectionId = Guid.NewGuid();
        GetFacingColorsByRoomTypeIdQueryHandler _getFacingColorsByRoomTypeIdQueryHandler;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _roomTypeId = Guid.NewGuid();
            var collection = new Collection
            {
                Id = collectionId,
                RoomTypeId = _roomTypeId
            };
            _contextMock.Setup(x => x.Set<Collection>()).Returns(new List<Collection>
            {
                collection
            }.GetQueryableMockDbSet());
            _contextMock.Setup(x => x.Set<FacingColor>()).Returns(new List<FacingColor>
            {
                new FacingColor
                {
                    Id = Guid.NewGuid(),
                     CollectionsVariants = new List<CollectionVariant> {new CollectionVariant {  Id = collectionId, Collection = new Collection { RoomType = new RoomType { Id = _roomTypeId} } } }
                 }
            }.GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _getFacingColorsByRoomTypeIdQueryHandler = new GetFacingColorsByRoomTypeIdQueryHandler(_contextFactoryMock.Object);
        }

        [Test]        
        public async Task Execute_RequestIsValid_GetFacings()
        {
            //arrange
           
            //act
            var res = await _getFacingColorsByRoomTypeIdQueryHandler.Execute(new GetFacingColorsByRoomTypeIdQuery
            {
              RoomTypeId = _roomTypeId
            });

            //assert
            Assert.True(res.Count == 1);
        }

        [Test]        
        public async Task Execute_RequestIsEmpty_FacingsAreEmpty()
        {
            //arrange
           
            //act
            var res = await _getFacingColorsByRoomTypeIdQueryHandler.Execute(new GetFacingColorsByRoomTypeIdQuery());

            //assert
            Assert.True(res.Count == 0);
        }
    }
}
