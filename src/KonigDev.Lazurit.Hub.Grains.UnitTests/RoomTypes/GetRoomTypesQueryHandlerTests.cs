﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.RoomTypes.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Query;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.RoomTypes
{
    [TestFixture]
    public class GetRoomTypesQueryHandlerTests
    {
        Mock<IDBContextFactory> _contextFactoryMock;
        Mock<DbContext> _contextMock;
        GetRoomTypesQueryHandler _getRoomTypesQueryHandler;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();

            _contextMock.Setup(x => x.Set<RoomType>()).Returns(new List<RoomType>()
            {
                new RoomType
                {
                    Id = Guid.NewGuid(),
                    Name = "",
                    Alias = "",
                    Collections = new List<Collection> { new Collection
                        {
                            Id = Guid.NewGuid(), RoomTypeId = Guid.NewGuid(), SeriesId = Guid.NewGuid(), Series = new Series(), RoomType = new RoomType(), CollectionVariants = new List<CollectionVariant>()
                        }
                    }
                 },
                 new RoomType
                {
                    Id = Guid.NewGuid(),
                    Name = "",
                    Alias = "",
                    Collections = new List<Collection> { new Collection
                        {
                            Id = Guid.NewGuid(), RoomTypeId = Guid.NewGuid(), SeriesId = Guid.NewGuid(), Series = new Series(), RoomType = new RoomType(), CollectionVariants = new List<CollectionVariant>()
                        }
                    }
                 },
            }.GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _getRoomTypesQueryHandler = new GetRoomTypesQueryHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task Execute_RequestIsValid_GetRoomTypes()
        {
            //arrange

            //act
            var res = await _getRoomTypesQueryHandler.Execute(new GetRoomTypesQuery());

            //assert
            Assert.True(res.Count == 2);
        }
    }
}
