﻿using KonigDev.Lazurit.Core.Enums;
using KonigDev.Lazurit.Hub.Grains.Carts.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Query;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.CartTests
{
    [TestFixture]
    public class GetCartIsApprovalRequiredQueryHandlerTests
    {
        private Mock<IDBContextFactory> _contextFactoryMock;
        private Mock<DbContext> _contextMock;
        private GetCartIsApprovalRequiredQueryHandler _getCartIsApprovalRequiredQueryHandler;
        private Guid _productMarkerSellerRequiredId;
        private Guid _userId;
        private Guid _productId = Guid.NewGuid();
        private Guid _cartId = Guid.NewGuid();
        private Guid _collectionId = Guid.NewGuid();
        private Guid _complectId = Guid.NewGuid();

        private DbSet<Cart> _cart
        {
            get
            {
                return new List<Cart>
                {
                    new Cart
                    {
                        Id = _cartId,
                        UserId = _userId,
                        Products = new List<CartItemProduct>
                        {
                            new CartItemProduct
                            {
                                Id = _productId,
                                Product = new Product
                                {
                                    ProductPartMarkerId = Guid.NewGuid()
                                }
                            }
                        },
                        Collections = new List<CartItemCollection>
                        {
                            new CartItemCollection
                            {
                                Id = _collectionId,
                                Products = new List<CartItemProductInCollection>
                                {
                                    new CartItemProductInCollection
                                    {
                                        Product = new Product
                                        {
                                            Id = Guid.NewGuid(),
                                            ProductPartMarkerId = Guid.NewGuid()
                                        }
                                    }
                                }
                            }
                        },
                        Complects = new List<CartItemComplect>
                        {
                            new CartItemComplect
                            {
                                Id = _complectId,
                                CollectionVariant = new CollectionVariant
                                {
                                    Products = new List<Product>
                                    {
                                        new Product
                                        {
                                            ProductPartMarkerId = Guid.NewGuid()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }.GetQueryableMockDbSet();
            }
        }

        [SetUp]
        public void SetUp()
        {
            _productMarkerSellerRequiredId = Guid.NewGuid();
            _userId = Guid.NewGuid();
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _contextMock.Setup(x => x.Set<ProductPartMarker>()).Returns(new List<ProductPartMarker> { new ProductPartMarker { Id = _productMarkerSellerRequiredId, Name = "", SortNumber = 0, TechName = EnumProductPartMarker.ExistedRequired.ToString() } }.GetQueryableMockDbSet());

            _contextMock.Setup(x => x.Set<Cart>()).Returns(_cart);
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _getCartIsApprovalRequiredQueryHandler = new GetCartIsApprovalRequiredQueryHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task Execute_MarkerInProducts_IsApprovalTrue()
        {
            //arrange
            var cart = _cart.FirstOrDefault(p => p.Id == _cartId);
            cart.Products.FirstOrDefault(p => p.Id == _productId).Product.ProductPartMarkerId = _productMarkerSellerRequiredId;
            var cartList = new List<Cart>();
            cartList.Add(cart);
            _contextMock.Setup(x => x.Set<Cart>()).Returns(cartList.GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            //act
            var result = await _getCartIsApprovalRequiredQueryHandler.Execute(new GetCartIsApprovalRequiredQuery
            {
                CartId = _cartId
            });

            //assert
            Assert.True(result.IsApprovalRequired);
        }

        [Test]
        public async Task Execute_NoMarkers_IsApprovalFalse()
        {
            //arrange            

            //act
            var result = await _getCartIsApprovalRequiredQueryHandler.Execute(new GetCartIsApprovalRequiredQuery
            {
                CartId = _cartId
            });

            //assert
            Assert.False(result.IsApprovalRequired);
        }

        [Test]
        public async Task Execute_MarkerInCollections_IsApprovalTrue()
        {
            //arrange
            var cart = _cart.FirstOrDefault(p => p.Id == _cartId);
            cart.Collections.FirstOrDefault(p => p.Id == _collectionId).Products.FirstOrDefault().Product.ProductPartMarkerId = _productMarkerSellerRequiredId;
            var cartList = new List<Cart>();
            cartList.Add(cart);
            _contextMock.Setup(x => x.Set<Cart>()).Returns(cartList.GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            //act
            var result = await _getCartIsApprovalRequiredQueryHandler.Execute(new GetCartIsApprovalRequiredQuery
            {
                CartId = _cartId
            });

            //assert
            Assert.True(result.IsApprovalRequired);
        }

        [Test]
        public async Task Execute_MarkerInComplects_IsApprovalTrue()
        {
            //arrange
            var cart = _cart.FirstOrDefault(p => p.Id == _cartId);
            cart.Complects.FirstOrDefault(p => p.Id == _complectId).CollectionVariant.Products.FirstOrDefault().ProductPartMarkerId = _productMarkerSellerRequiredId;
            var cartList = new List<Cart>();
            cartList.Add(cart);
            _contextMock.Setup(x => x.Set<Cart>()).Returns(cartList.GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            //act
            var result = await _getCartIsApprovalRequiredQueryHandler.Execute(new GetCartIsApprovalRequiredQuery
            {
                CartId = _cartId
            });

            //assert
            Assert.True(result.IsApprovalRequired);
        }

        [Test]
        public async Task Execute_MarkerInAllCartItems_IsApprovalTrue()
        {
            //arrange
            var cart = _cart.FirstOrDefault(p => p.Id == _cartId);

            cart.Collections.FirstOrDefault(p => p.Id == _collectionId).Products.FirstOrDefault().Product.ProductPartMarkerId = _productMarkerSellerRequiredId;
            cart.Products.FirstOrDefault(p => p.Id == _productId).Product.ProductPartMarkerId = _productMarkerSellerRequiredId;
            cart.Complects.FirstOrDefault(p => p.Id == _complectId).CollectionVariant.Products.FirstOrDefault().ProductPartMarkerId = _productMarkerSellerRequiredId;
            var cartList = new List<Cart>();
            cartList.Add(cart);
            _contextMock.Setup(x => x.Set<Cart>()).Returns(cartList.GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            //act
            var result = await _getCartIsApprovalRequiredQueryHandler.Execute(new GetCartIsApprovalRequiredQuery
            {
                CartId = _cartId
            });

            //assert
            Assert.True(result.IsApprovalRequired);
        }
    }
}
