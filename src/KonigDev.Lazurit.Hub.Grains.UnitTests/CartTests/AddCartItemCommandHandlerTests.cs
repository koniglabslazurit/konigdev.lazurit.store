﻿using KonigDev.Lazurit.Core.Enums.Exeptions.Cart;
using KonigDev.Lazurit.Hub.Grains.Carts.CommandHandlers;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Command;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.CartTests
{
    [TestFixture]
    public class AddCartItemCommandHandlerTests
    {
        private Mock<IDBContextFactory> _contextFactoryMock;
        private Mock<DbContext> _contextMock;
        private Guid _itemId;
        private AddCartItemCommandHandler _classForTest;
        private Guid _cartId;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _itemId = Guid.NewGuid();
            _cartId = Guid.NewGuid();

            _contextMock.Setup(p => p.Set<Cart>()).Returns(new List<Cart>
            {
                new Cart {
                    Id = _cartId,
                    Products = new List<CartItemProduct> { new CartItemProduct { ProductId = _itemId } },
                    Complects = new List<CartItemComplect> { new CartItemComplect { CollectionVariantId = _itemId} },
                    Collections = new List<CartItemCollection> { new CartItemCollection { CollectionVariantId = _itemId} }
                }
            }.GetQueryableMockDbSet());
            
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _classForTest = new AddCartItemCommandHandler(_contextFactoryMock.Object);
        }

        [Test]
        public void AddCartItemCommandHandler_HasProduct()
        {
            //arrange
            var command = new AddCartItemCommand { CartId = _cartId, AddElementId = _itemId, CartItemType = Core.Enums.EnumCartItemType.Product };
            //act
            //assert    
            Assert.ThrowsAsync<NotValidExeption>(async () => await _classForTest.Execute(command));
        }


        [Test]
        public void AddCartItemCommandHandler_HasCollection()
        {
            //arrange
            var command = new AddCartItemCommand { CartId = _cartId, AddElementId = _itemId, CartItemType = Core.Enums.EnumCartItemType.Collection };
            //act
            //assert    
            Assert.ThrowsAsync<NotValidExeption>(async () => await _classForTest.Execute(command));
        }


        [Test]
        public void AddCartItemCommandHandler_HasComplekt()
        {
            //arrange
            var command = new AddCartItemCommand { CartId = _cartId, AddElementId = _itemId, CartItemType = Core.Enums.EnumCartItemType.Complect };
            //act
            //assert    
            Assert.ThrowsAsync<NotValidExeption>(async () => await _classForTest.Execute(command)); 
        }
    }
}
