﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Payments.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.UnitTests;
using KonigDev.Lazurit.Hub.Model.DTO.Payment.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Payment.Query;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationTests.Payment
{
    [TestFixture]
    public class GetPaymentSettingsQueryHandlerTests
    {
        private Mock<IDBContextFactory> _contextFactoryMock;
        private Mock<DbContext> _contextMock;
        private GetPaymentSettingsQueryHandler _getSettings;
        private DbSet<Settings> _settings
        {
            get
            {
                return new List<Settings>
                {
                    new Settings
                    {
                        Id = Guid.NewGuid(),
                        Name = "RobokassaUrl", 
                        Value = "url"
                    },
                    new Settings
                    {
                        Id = Guid.NewGuid(),
                        Name = "RobokassaMerchantLogin",
                        Value = "login"
                    },
                    new Settings
                    {
                        Id = Guid.NewGuid(),
                        Name = "RobokassaPassword1",
                        Value = "password"
                    },
                     new Settings
                    {
                        Id = Guid.NewGuid(),
                        Name = "RobokassaPassword2",
                        Value = "password2"
                    }
                }.GetQueryableMockDbSet();
            }
        }

        private DbSet<Settings> _settingsEmpty
        {
            get
            {
                return new List<Settings>().GetQueryableMockDbSet();
            }
        }

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
                        
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _getSettings= new GetPaymentSettingsQueryHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task GetPaymentSettings_ShouldReturnSettings_WhenTheyExistsInDb()
        {
            //arrange
            _contextMock.Setup(x => x.Set<Settings>()).Returns(_settings);
            //act
            var actual = await _getSettings.Execute(new GetPaymentSettingsQuery());
            //assert
            Assert.IsInstanceOf<DtoPaymentSettings>(actual);
            Assert.AreEqual("url", actual.RobokassaUrl);
            Assert.AreEqual("login", actual.MerchantLogin);
            Assert.AreEqual("password", actual.Password1);
            Assert.AreEqual("password2", actual.Password2);
        }

        [Test]
        public async Task GetPaymentSettings_ShouldReturnNullSettings_WhenTheyNotExistsInDb()
        {
            //arrange
            _contextMock.Setup(x => x.Set<Settings>()).Returns(_settingsEmpty);
            //act
            var actual = await _getSettings.Execute(new GetPaymentSettingsQuery());
            //assert
            Assert.IsInstanceOf<DtoPaymentSettings>(actual);
            Assert.AreEqual(null, actual.RobokassaUrl);
            Assert.AreEqual(null, actual.MerchantLogin);
            Assert.AreEqual(null, actual.Password1);
            Assert.AreEqual(null, actual.Password2);
        }
    }
}