﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.FurnitureTypes.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Query;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.FurnitureTypes
{
    [TestFixture]
    public class GetFurnitureTypesByRequestQueryHandlerTests
    {
        Mock<IDBContextFactory> _contextFactoryMock;
        Mock<DbContext> _contextMock;
        GetFurnitureTypesByCollectionQueryHandler _getFurnitureTypesByRequestQueryHandler;
        
        Guid _furnitureTypeId;
        Guid _collectionId;
        Guid _collecionVariantId;

        [SetUp]
        public void SetUp()
        {
            _furnitureTypeId = Guid.NewGuid();
            _collectionId = Guid.NewGuid();
            _collecionVariantId = Guid.NewGuid();
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            var collectionVariants = new List<CollectionVariant>()
            {
                new CollectionVariant
                {
                    Id =_collecionVariantId, Collection = new Collection {Id = _collectionId, CollectionVariants =
                    new List<CollectionVariant>() { new CollectionVariant { Id = _collecionVariantId} } } 
                },
                new CollectionVariant
                {
                    Id = Guid.NewGuid(), Collection = new Collection {Id = _collectionId, CollectionVariants =
                     new List<CollectionVariant>() { new CollectionVariant { Id = _collecionVariantId} } }
                }
            };
            var products = new List<Product>
            {
                new Product { CollectionsVariants = collectionVariants, Article = new Article { FurnitureTypeId = _furnitureTypeId} },
                new Product { CollectionsVariants = collectionVariants, Article = new Article { FurnitureTypeId = _furnitureTypeId} }
            };
            var articles = new List<Article> { new Article { Products = products, FurnitureTypeId = _furnitureTypeId } };
            _contextMock.Setup(x => x.Set<FurnitureType>()).Returns(new List<FurnitureType>
            {
               new FurnitureType
               {
                   Id = _furnitureTypeId,
                   Name = "sofa",
                   Articles = articles
               }
            }.GetQueryableMockDbSet());
            _contextMock.Setup(x => x.Set<Product>()).Returns(products.GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _getFurnitureTypesByRequestQueryHandler = new GetFurnitureTypesByCollectionQueryHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task Execute_RequestIsValid_GetProductTypes()
        {
            //arrange

            //act
            var res = await _getFurnitureTypesByRequestQueryHandler.Execute(new GetFurnitureTypesByCollectionQuery { CollectionVariantId = _collecionVariantId});

            //assert
            Assert.True(res.Select(p => p.Name).FirstOrDefault() == "sofa");
            Assert.True(res.Select(p => p.Amount).FirstOrDefault() == 1);
            Assert.True(res.Count == 1);
        }
        [Test]
        public async Task Execute_RequestIsNewGuid_GetProductTypes()
        {
            //arrange

            //act
            var res = await _getFurnitureTypesByRequestQueryHandler.Execute(new GetFurnitureTypesByCollectionQuery { CollectionVariantId = Guid.NewGuid() });

            //assert
            Assert.True(res.Count == 0);
        }
    }
}
