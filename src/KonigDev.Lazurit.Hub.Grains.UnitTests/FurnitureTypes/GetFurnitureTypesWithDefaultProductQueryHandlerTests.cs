﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.FurnitureTypes.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Query;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.FurnitureTypes
{
    public class GetFurnitureTypesWithDefaultProductQueryHandlerTests
    {
        Mock<IDBContextFactory> _contextFactoryMock;
        Mock<DbContext> _contextMock;
        GetFurnitureTypesWithDefaultProductQueryHandler _classForTest;
        IEnumerable<FurnitureType> furnitureTypes;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();

            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());

            //В тесте временно получаем продукты, так как потом у типов мебели появятся дефолтные продукты
            var products = fixture.Build<Product>()
                .Without(p => p.CollectionsVariants)
                .Without(p => p.FavoriteItemProducts)
                .Without(p => p.Properties)
                .Without(p => p.Styles)
                .Without(p => p.TargetAudiences).CreateMany(1).ToList();
            var arts = fixture.Build<Article>().With(p => p.Products, products).CreateMany(1).ToList();

            furnitureTypes = fixture.Build<FurnitureType>().With(p => p.Articles, arts).CreateMany(50); //fixture.CreateMany<FurnitureType>(50);
            
            _contextMock.Setup(p => p.Set<FurnitureType>()).Returns(furnitureTypes.ToList().GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _classForTest = new GetFurnitureTypesWithDefaultProductQueryHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task GetFurnitureTypesWithDefaultProductQueryHandler_FurnitureTypeIsNotParent_ReturnAllItems()
        {
            //arrange          
            var query = new GetFurnitureTypesWithDefaultProductQuery();
            
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.True(result.Count == 0);
        }

        [Test]
        public async Task GetFurnitureTypesWithDefaultProductQueryHandler_GetFurnitureTypes_OneItemIsEqual()
        {
            //arrange          
            var query = new GetFurnitureTypesWithDefaultProductQuery();
            var type = furnitureTypes.First();
            var typeId = Guid.NewGuid();
            type.ParentId = typeId;
            type.Id = typeId;
            type.Name = "Sofa";

            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.True(result.First(p => p.Id == typeId).Name == "Sofa");
        }
    }
}
