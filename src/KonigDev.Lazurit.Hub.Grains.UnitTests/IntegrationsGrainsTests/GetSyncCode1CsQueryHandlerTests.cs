﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.IntegrationsGrainsTests
{
    [TestFixture]
    public class GetSyncCode1CsQueryHandlerTests
    {
        private Mock<IDBContextFactory> _contextFactoryMock;
        private Mock<DbContext> _contextMock;
        private GetSyncCode1CsQueryHandler _classForTest;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(new List<IntegrationProduct>().GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritIntegrationContext()).Returns(_contextMock.Object);

            _classForTest = new GetSyncCode1CsQueryHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task GetSyncCode1CsQueryHandler_SyncCodeEqual_ReturnOneItem()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            var products = fixture.CreateMany<IntegrationProduct>(50);

            foreach (var product in products)
            {
                product.SyncCode1C = "code";
            }

            var query = new GetSyncCode1CsQuery();
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.True(result.Count == 1);
            Assert.True(result.Any(p => p.Name == "code"));
        }

        [Test]
        public async Task GetSyncCode1CsQueryHandler_TwoSyncCodes_ReturnTwoItems()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            var products = fixture.CreateMany<IntegrationProduct>(50);

            foreach (var product in products)
            {
                product.SyncCode1C = "code";
            }

            products.First().SyncCode1C = "1C";
            products.Last().SyncCode1C = "1C";

            var query = new GetSyncCode1CsQuery();
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.True(result.Count == 2);
            Assert.True(result.Any(p => p.Name == "code"));
            Assert.True(result.Any(p => p.Name == "1C"));
        }

        [Test]
        public async Task GetSyncCode1CsQueryHandler_ByCollectionId_ReturnOneItems()
        {
            //arrange
            var fixture = new Fixture();
            var collectionId = Guid.NewGuid();

            var listFirst = fixture.Build<IntegrationProduct>()
                .Without(p => p.IntegrationProductTargetAudiencies)
                .Without(p => p.IntegrationProductStyles)
                .Without(p => p.IntegrationProductProperties)
                .Without(p => p.IntegrationCollectionsVariants)
                .Without(p=>p.IntegrationCollection)
                .With(p => p.SyncCode1C, "code")
                .CreateMany(20).ToList();
            listFirst.ForEach(p => p.IntegrationCollectionsVariants.Add(new IntegrationCollectionVariant { IntegrationCollectionId = collectionId }));

            var listTwo = fixture.Build<IntegrationProduct>()
                .Without(p => p.IntegrationProductTargetAudiencies)
                .Without(p => p.IntegrationProductStyles)
                .Without(p => p.IntegrationProductProperties)
                .Without(p => p.IntegrationCollectionsVariants)
                .Without(p => p.IntegrationCollection)
                .With(p => p.SyncCode1C, "code2")
                .CreateMany(20).ToList();
            listTwo.ForEach(p => p.IntegrationCollectionsVariants.Add(new IntegrationCollectionVariant { IntegrationCollectionId = Guid.NewGuid() }));

            var products = new List<IntegrationProduct>();
            products.AddRange(listFirst);
            products.AddRange(listTwo);

            var query = new GetSyncCode1CsQuery { IntegrationCollectionId = collectionId };
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.True(result.Count == 1);
            Assert.True(result.Any(p => p.Name == "code"));
        }

        [Test]
        public async Task GetSyncCode1CsQueryHandler_ByCollectionId_ReturnTwoItems()
        {
            //arrange
            var fixture = new Fixture();
            var collectionId = Guid.NewGuid();

            var listFirst = fixture.Build<IntegrationProduct>()
                .Without(p => p.IntegrationProductTargetAudiencies)
                .Without(p => p.IntegrationProductStyles)
                .Without(p => p.IntegrationProductProperties)
                .Without(p => p.IntegrationCollectionsVariants)
                .Without(p => p.IntegrationCollection)
                .With(p => p.SyncCode1C, "code")
                .CreateMany(20).ToList();
            listFirst.ForEach(p => p.IntegrationCollectionsVariants.Add(new IntegrationCollectionVariant { IntegrationCollectionId = collectionId }));

            var listTwo = fixture.Build<IntegrationProduct>()
                .Without(p => p.IntegrationProductTargetAudiencies)
                .Without(p => p.IntegrationProductStyles)
                .Without(p => p.IntegrationProductProperties)
                .Without(p => p.IntegrationCollectionsVariants)
                .Without(p => p.IntegrationCollection)
                .With(p => p.SyncCode1C, "code2")
                .CreateMany(20).ToList();
            listTwo.ForEach(p => p.IntegrationCollectionsVariants.Add(new IntegrationCollectionVariant { IntegrationCollectionId = collectionId }));

            var products = new List<IntegrationProduct>();
            products.AddRange(listFirst);
            products.AddRange(listTwo);

            var query = new GetSyncCode1CsQuery { IntegrationCollectionId = collectionId };
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.True(result.Count == 2);
            Assert.True(result.Any(p => p.Name == "code"));
            Assert.True(result.Any(p => p.Name == "code2"));
        }
    }
}
