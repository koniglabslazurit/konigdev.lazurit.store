﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.IntegrationGrains.CommandHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using Moq;
using NUnit.Framework;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.IntegrationsGrainsTests
{
    [TestFixture]
    public class CreateVariantByIntegrationVariantCommandHandlerTests
    {
        private Mock<IDBContextFactory> _mockContextFactory;
        private Mock<DbContext> _mockContext;
        private Mock<ITransactionFactory> _mockTransactionFactory;
        private CreateVariantByIntegrationVariantCommandHandler _classForTest;

        [SetUp]
        public void SetUp()
        {
            _mockContextFactory = new Mock<IDBContextFactory>();
            _mockTransactionFactory = new Mock<ITransactionFactory>();
            _mockContext = new Mock<DbContext>();
            _mockContextFactory.Setup(x => x.CreateLazuritIntegrationContext()).Returns(_mockContext.Object);
            _classForTest = new CreateVariantByIntegrationVariantCommandHandler(_mockContextFactory.Object, _mockTransactionFactory.Object);
        }

        [Test]
        public void CreateVariantByIntegrationVariantCommandHandler_ReturnException_IfNullCommand()
        {
            //arrange
            CreateVariantByIntegrationVariantCommand command = null;
            
            //act
            //assert
            Assert.Throws<AggregateException>(() => { _classForTest.Execute(command).Wait(); });
        }

        [Test]
        public void CreateVariantByIntegrationVariantCommandHandler_ReturnException_IfInvalidCommand()
        {
            //arrange
            var command = new CreateVariantByIntegrationVariantCommand
            {
                Id = Guid.Empty
            };

            //act
            //assert
            Assert.Throws<AggregateException>(() => { _classForTest.Execute(command).Wait(); });
        }

        [Test]
        public void CreateVariantByIntegrationVariantCommandHandler_ReturnException_IfIntegrationVariantNotFound()
        {
            //arrange
            var command = new CreateVariantByIntegrationVariantCommand
            {
                Id = Guid.NewGuid()
            };
            List<IntegrationCollectionVariant> variant = new List<IntegrationCollectionVariant>();
            _mockContext.Setup(x => x.Set<IntegrationCollectionVariant>()).Returns(variant.GetQueryableMockDbSet());
            //act
            //assert
            Assert.Throws<AggregateException>(() => { _classForTest.Execute(command).Wait(); });
        }
    }
}
