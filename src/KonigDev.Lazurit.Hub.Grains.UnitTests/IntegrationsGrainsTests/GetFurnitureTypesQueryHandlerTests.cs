﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.IntegrationsGrainsTests
{
    [TestFixture]
    public class GetFurnitureTypesQueryHandlerTests
    {
        private Mock<IDBContextFactory> _contextFactoryMock;
        private Mock<DbContext> _contextMock;
        private GetFurnitureTypesQueryHandler _classForTest;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(new List<IntegrationProduct>().GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritIntegrationContext()).Returns(_contextMock.Object);

            _classForTest = new GetFurnitureTypesQueryHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task GetFurnitureTypesQueryHandler_FurnitureType_ReturnOneItem()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            var products = fixture.CreateMany<IntegrationProduct>(50);

            foreach (var product in products)
            {
                product.FurnitureType = "шкаф";
            }

            var query = new GetFurnitureTypesQuery();
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.True(result.Count == 1);
            Assert.True(result.Any(p => p.Name == "шкаф"));
        }

        [Test]
        public async Task GetFurnitureTypesQueryHandler_FurnitureType_ReturnTwoItems()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            var products = fixture.CreateMany<IntegrationProduct>(50);

            foreach (var product in products)
            {
                product.FurnitureType = "шкаф";
            }

            products.First().FurnitureType = "комод";
            products.Last().FurnitureType = "комод";

            var query = new GetFurnitureTypesQuery();
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.True(result.Count == 2);
            Assert.True(result.Any(p => p.Name == "шкаф"));
            Assert.True(result.Any(p => p.Name == "комод"));
        }

        [Test]
        public async Task GetFurnitureTypesQueryHandler_GetByCollection_ReturnCorrectList()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var collectionId = Guid.NewGuid();

            var listFirst = fixture.Build<IntegrationProduct>()
                .Without(p => p.IntegrationProductTargetAudiencies)
                .Without(p => p.IntegrationProductStyles)
                .Without(p => p.IntegrationProductProperties)
                .Without(p => p.IntegrationCollectionsVariants)
                .Without(p => p.IntegrationCollection)
                .With(p => p.FurnitureType, "шкаф")
                .CreateMany(20).ToList();
            listFirst.ForEach(p => p.IntegrationCollectionsVariants.Add(new IntegrationCollectionVariant { IntegrationCollectionId = collectionId }));

            var products = new List<IntegrationProduct>();
            products.AddRange(listFirst);

            var query = new GetFurnitureTypesQuery { IntegrationCollectionId = collectionId };
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.True(result.Count == 1);
            Assert.True(result.All(p => p.Name == "шкаф"));
        }

        [Test]
        public async Task GetFurnitureTypesQueryHandler_GetByCollection_ReturnCorrectListWithTwoItems()
        {
            //arrange
            var fixture = new Fixture();
            var collectionId = Guid.NewGuid();

            var listFirst = fixture.Build<IntegrationProduct>()
                .Without(p => p.IntegrationProductTargetAudiencies)
                .Without(p => p.IntegrationProductStyles)
                .Without(p => p.IntegrationProductProperties)
                .Without(p => p.IntegrationCollectionsVariants)
                .Without(p => p.IntegrationCollection)
                .With(p => p.FurnitureType, "шкаф")
                .CreateMany(20).ToList();
            listFirst.ForEach(p => p.IntegrationCollectionsVariants.Add(new IntegrationCollectionVariant { IntegrationCollectionId = collectionId }));

            var listTwo = fixture.Build<IntegrationProduct>()
                .Without(p => p.IntegrationProductTargetAudiencies)
                .Without(p => p.IntegrationProductStyles)
                .Without(p => p.IntegrationProductProperties)
                .Without(p => p.IntegrationCollectionsVariants)
                .Without(p => p.IntegrationCollection)
                .With(p => p.FurnitureType, "комод")
                .CreateMany(20).ToList();
            listTwo.ForEach(p => p.IntegrationCollectionsVariants.Add(new IntegrationCollectionVariant { IntegrationCollectionId = collectionId }));

            var products = new List<IntegrationProduct>();
            products.AddRange(listFirst);
            products.AddRange(listTwo);

            var query = new GetFurnitureTypesQuery { IntegrationCollectionId = collectionId };
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.True(result.Count == 2);
            Assert.True(result.Any(p => p.Name == "шкаф"));
            Assert.True(result.Any(p => p.Name == "комод"));
        }
    }
}
