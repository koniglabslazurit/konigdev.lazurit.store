﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Products.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Products;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Query;
using KonigDev.Lazurit.Model.Context.DataContext;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers.IntegrationProductContentQueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Integration.Context.Entities;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.IntegrationsGrainsTests.IntegrationProductContent
{
    [TestFixture]
    public class GetIntegrationProductsContextByVariantQueryHandlerTests
    {
        private Mock<IDBContextFactory> _contextFactory;
        private Mock<DbContext> _context;
        private GetIntegrationProductsContextByVariantQueryHandler _service;

        [SetUp]
        public void SetUp()
        {
            _contextFactory = new Mock<IDBContextFactory>();
            _context = new Mock<DbContext>();
            _contextFactory.Setup(x => x.CreateLazuritIntegrationContext()).Returns(_context.Object);
            _service = new GetIntegrationProductsContextByVariantQueryHandler(_contextFactory.Object);
        }

        [Test]
        public void Execute_ReturnIntegrationProductContextListWhenProductContextIsEnabled()
        {
            //arrange
            var p1 = Guid.NewGuid();
            var p2 = Guid.NewGuid();
            var p3 = Guid.NewGuid();
            var collVarId = Guid.NewGuid();

            var products = new List<IntegrationProduct>
            {
                new IntegrationProduct {ProductId = p1, IntegrationCollectionsVariants= new List<IntegrationCollectionVariant> {new IntegrationCollectionVariant { Id= collVarId} }, SyncCode1C = "0001" },
                new IntegrationProduct {ProductId = p2, IntegrationCollectionsVariants= new List<IntegrationCollectionVariant> {new IntegrationCollectionVariant { Id= collVarId} }, SyncCode1C = "0001" },
                new IntegrationProduct {ProductId = p3, IntegrationCollectionsVariants= new List<IntegrationCollectionVariant> {new IntegrationCollectionVariant { Id= collVarId} }, SyncCode1C = "0001" }
            }.GetQueryableMockDbSet();

            var productContexts = new List<IntegrationCollectionVariantContentProductArea>
            {
                new IntegrationCollectionVariantContentProductArea {Id = Guid.NewGuid(), IntegrationCollectionVariantId = collVarId, ProductId = p1, IsEnabled = true, Height = 1, Left = 1, Top =1, Width=1},
                new IntegrationCollectionVariantContentProductArea {Id = Guid.NewGuid(), IntegrationCollectionVariantId = collVarId, ProductId = p2, IsEnabled = true, Height = 1, Left = 1, Top =1, Width=1},
                new IntegrationCollectionVariantContentProductArea {Id = Guid.NewGuid(), IntegrationCollectionVariantId = collVarId, ProductId = p3, IsEnabled = false, Height = 1, Left = 1, Top =1, Width=1} ,
            }.GetQueryableMockDbSet();

            _context.Setup(x => x.Set<IntegrationProduct>()).Returns(products);
            _context.Setup(x => x.Set<IntegrationCollectionVariantContentProductArea>()).Returns(productContexts);

            //act
            var actual = _service.Execute(new GetIntegrationProductsContextByCollectionVariantQuery { IntegrationCollectionVariantId = collVarId });

            //assert
            Assert.NotNull(actual);
            Assert.IsInstanceOf<Task<List<DtoIntegrationProductContentResult>>>(actual);
            Assert.AreEqual(3, actual.Result.Count);
            Assert.AreEqual(1, actual.Result.First().Height);
            Assert.IsNull(actual.Result.Last().ObjectId);
            Assert.AreEqual(0, actual.Result.Last().Height);
        }

        [Test]
        public async Task Execute_ReturnEmptyIntegrationProductContextListWhenHavnotProducts()
        {
            //arrange
            var collVarId = Guid.NewGuid();
            var products = new List<IntegrationProduct>().GetQueryableMockDbSet();
            var productContexts = new List<IntegrationCollectionVariantContentProductArea>().GetQueryableMockDbSet();
            _context.Setup(x => x.Set<IntegrationProduct>()).Returns(products);
            _context.Setup(x => x.Set<IntegrationCollectionVariantContentProductArea>()).Returns(productContexts);

            //act
            var actual = await _service.Execute(new GetIntegrationProductsContextByCollectionVariantQuery { IntegrationCollectionVariantId = collVarId });

            //assert
            Assert.NotNull(actual);
            Assert.IsInstanceOf<List<DtoIntegrationProductContentResult>>(actual);
            Assert.AreEqual(0, actual.Count);
        }
    }
}
