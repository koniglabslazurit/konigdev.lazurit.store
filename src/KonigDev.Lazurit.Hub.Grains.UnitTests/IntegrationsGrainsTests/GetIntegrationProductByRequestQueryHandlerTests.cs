﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers.ProductsQueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.IntegrationsGrainsTests
{
    [TestFixture]
    public class GetIntegrationProductByRequestQueryHandlerTests
    {
        private Mock<IDBContextFactory> _contextFactoryMock;
        private Mock<DbContext> _contextMock;
        private GetIntegrationProductByRequestQueryHandler _classForTest;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(new List<IntegrationProduct>().GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritIntegrationContext()).Returns(_contextMock.Object);

            _classForTest = new GetIntegrationProductByRequestQueryHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task GetIntegrationProductByRequestQueryHandler_RequestEmpty_ReturnAll()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            var products = fixture.CreateMany<IntegrationProduct>(50);

            var query = new GetIntegrationProductByRequestQuery();
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.AreEqual(result.Items.Count(), products.Count());
            Assert.AreEqual(result.TotalCount, products.Count());
        }

        [Test]
        public async Task GetIntegrationProductByRequestQueryHandler_Get10PerPage_Return10Items()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            var products = fixture.CreateMany<IntegrationProduct>(50);

            var query = new GetIntegrationProductByRequestQuery { ItemsOnPage = 10 };
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.AreEqual(result.Items.Count(), 10);
            Assert.AreEqual(result.TotalCount, products.Count());
        }

        [Test]
        public async Task GetIntegrationProductByRequestQueryHandler_Get10PerPagePage1_Return10Items()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            var products = fixture.CreateMany<IntegrationProduct>(50);

            var query = new GetIntegrationProductByRequestQuery { ItemsOnPage = 10, Page = 1 };
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.AreEqual(result.Items.Count(), 10);
            Assert.AreEqual(result.TotalCount, products.Count());
        }

        [Test]
        public async Task GetIntegrationProductByRequestQueryHandler_Get2PerPagePage2_ReturnCorrectFirstItems()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var products = fixture.CreateMany<IntegrationProduct>(50);
            products.Take(2).ToList().ForEach(p => p.SyncCode1C = "2");
            products.Skip(2).ToList().ForEach(p => p.SyncCode1C = "1");

            var query = new GetIntegrationProductByRequestQuery { ItemsOnPage = 2, Page = 2 };
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.AreEqual(result.Items.Count(), 2);
            Assert.IsTrue(result.Items.All(p => p.SyncCode1C == "1"));
            Assert.AreEqual(result.TotalCount, products.Count());
        }

        [Test]
        public async Task GetIntegrationProductByRequestQueryHandler_Get2PerPagePage2_ReturnCorrectItems()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var products = fixture.CreateMany<IntegrationProduct>(50);
            products.Take(2).ToList().ForEach(p => p.SyncCode1C = "1");
            products.Skip(2).ToList().ForEach(p => p.SyncCode1C = "2");

            var query = new GetIntegrationProductByRequestQuery { ItemsOnPage = 2, Page = 1 };
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.AreEqual(result.Items.Count(), 2);
            Assert.IsTrue(result.Items.FirstOrDefault().SyncCode1C == "1");
            Assert.AreEqual(result.TotalCount, products.Count());
        }

        [Test]
        public async Task GetIntegrationProductByRequestQueryHandler_GetMoreThatExist_ReturnCorrectItems()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var products = fixture.CreateMany<IntegrationProduct>(50);

            var query = new GetIntegrationProductByRequestQuery { ItemsOnPage = 50, Page = 3 };
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.AreEqual(result.Items.Count(), 0);
            Assert.AreEqual(result.TotalCount, products.Count());
        }

        [Test]
        public async Task GetIntegrationProductByRequestQueryHandler_GetByVendorCode_ReturnCorrectItems()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var products = fixture.CreateMany<IntegrationProduct>(50);
            products.Skip(10).ToList().ForEach(p => p.VendorCode = "VendorCode");

            var query = new GetIntegrationProductByRequestQuery { ItemsOnPage = 50, Page = 1, VendorCode = "VendorCode" };
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.AreEqual(result.Items.Count(), 40);
            Assert.IsTrue(result.Items.All(p => p.VendorCode == "VendorCode"));
            Assert.AreEqual(result.TotalCount, 40);
        }

        [Test]
        public async Task GetIntegrationProductByRequestQueryHandler_GetByCollection_ReturnCorrectItems()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            Guid integrationCollectionId = Guid.NewGuid();

            var products = fixture.CreateMany<IntegrationProduct>(50);
            foreach (var item in products.Skip(14).ToList())
            {
                item.SyncCode1C = integrationCollectionId.ToString().ToLower();
                item.CollectionId = integrationCollectionId;
                foreach (var cv in item.IntegrationCollectionsVariants.ToList())
                    cv.IntegrationCollectionId = integrationCollectionId;
            }

            var query = new GetIntegrationProductByRequestQuery { ItemsOnPage = 50, Page = 1, IntegrationCollectionId = integrationCollectionId };
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.AreEqual(result.Items.Count(), 36);
            Assert.IsTrue(result.Items.All(p => p.SyncCode1C == integrationCollectionId.ToString().ToLower()));
            Assert.AreEqual(result.TotalCount, 36);
        }

        [Test]
        public async Task GetIntegrationProductByRequestQueryHandler_GetByCollectionAndVendorCode_ReturnCorrectItems()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            Guid integrationCollectionId = Guid.NewGuid();

            var products = fixture.CreateMany<IntegrationProduct>(50);
            foreach (var item in products.Skip(14).ToList())
            {
                item.SyncCode1C = integrationCollectionId.ToString().ToLower();
                item.CollectionId = integrationCollectionId;
                foreach (var cv in item.IntegrationCollectionsVariants.ToList())
                    cv.IntegrationCollectionId = integrationCollectionId;
            }

            foreach (var item in products.Skip(10).Take(20).ToList())
                item.VendorCode = "VendorCode";

            var query = new GetIntegrationProductByRequestQuery { ItemsOnPage = 50, Page = 1, IntegrationCollectionId = integrationCollectionId, VendorCode = "VendorCode" };
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.AreEqual(result.Items.Count(), 16);
            Assert.IsTrue(result.Items.All(p => p.SyncCode1C == integrationCollectionId.ToString().ToLower() && p.VendorCode == "VendorCode"));
            Assert.AreEqual(result.TotalCount, 16);
        }

        [Test]
        public async Task GetIntegrationProductByRequestQueryHandler_GetByIntegrationStatus_ReturnCorrectItems()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            string integrationStatus = "integrationStatus";

            var products = fixture.CreateMany<IntegrationProduct>(50);
            foreach (var item in products.Skip(25).ToList())
            {
                item.IntegrationStatus.TechName = integrationStatus;
            }

            var query = new GetIntegrationProductByRequestQuery { ItemsOnPage = 50, Page = 1,  ProductStatus = integrationStatus };
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.AreEqual(result.Items.Count(), 25);
            Assert.IsTrue(result.Items.All(p => p.IntegrationStatus == integrationStatus));
            Assert.AreEqual(result.TotalCount, 25);
        }

        [Test]
        public async Task GetIntegrationProductByRequestQueryHandler_GetRedFacingColor_ReturnCorrectItems()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            var products = fixture.CreateMany<IntegrationProduct>(50);

            var query = new GetIntegrationProductByRequestQuery { FacingColor = "Red" };
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.IsTrue(result.Items.All(p => p.FacingColor == "Red"));
        }

        [Test]
        public async Task GetIntegrationProductByRequestQueryHandler_GetGreenFacing_ReturnCorrectItems()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
          //  var product = fixture.Build<IntegrationProduct>().Without(p => p.IntegrationCollectionsVariants);


            var products = fixture.CreateMany<IntegrationProduct>(50);

            var query = new GetIntegrationProductByRequestQuery { Facing = "Green" };
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.IsTrue(result.Items.All(p => p.FacingColor == "Green"));
        }
    }
}
