﻿using Castle.Core.Internal;
using KonigDev.Lazurit.Core.Enums.Enums;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers.ProductsQueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.IntegrationsGrainsTests
{
    public class GetPublishedIntegrationProductsByVendorCodeQueryHandlerTests
    {
        private Mock<IDBContextFactory> _contextFactoryMock;
        private Mock<DbContext> _contextMock;
        private GetPublishedIntegrationProductsByVendorCodeQueryHandler _classForTest;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(new List<IntegrationProduct>().GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritIntegrationContext()).Returns(_contextMock.Object);

            _classForTest = new GetPublishedIntegrationProductsByVendorCodeQueryHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task GetPublishedIntegrationProductsQueryHandler_VendorCodeEqual_Return1Item()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            var products = fixture.CreateMany<IntegrationProduct>(50);

            foreach (var product in products.Take(20))
            {
                product.VendorCode = "vendor";
                product.FurnitureType = "кровать";
                product.IntegrationStatus.TechName = EnumIntegrationStatus.Published.ToString();
            }

            var query = new GetPublishedIntegrationProductsByVendorCodeQuery { VendorCode = "vendor", ItemsOnPage = 30, Page = 1 };
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.True(result.TotalCount == 1);
            Assert.True(result.Items.All(p => p.VendorCode == "vendor"));
            Assert.True(result.Items.Where(p => p.VendorCode == "vendor").SingleOrDefault().Products.Count() == 20);
            Assert.True(result.Items.Where(p => p.VendorCode == "vendor").Count() == 1);
        }

        [Test]
        public async Task GetPublishedIntegrationProductsQueryHandler_10ItemsOnPage_Return10Item()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            var products = fixture.CreateMany<IntegrationProduct>(50);

            foreach (var product in products.Take(20))
            {
                product.IntegrationStatus.TechName = EnumIntegrationStatus.Published.ToString();
            }

            var query = new GetPublishedIntegrationProductsByVendorCodeQuery { ItemsOnPage = 10, Page = 1 };
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.True(result.Items.Count() == 10);
            Assert.True(result.TotalCount == 20);
            Assert.True(result.Items.All(p => p.Products.Count() == 1));
        }

        [Test]
        public async Task GetPublishedIntegrationProductsQueryHandler_2VendorCodes_Return2Item()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            var products = fixture.CreateMany<IntegrationProduct>(50);

            foreach (var product in products.Take(20))
            {
                product.VendorCode = "vendor";
                product.FurnitureType = "кровать";
                product.IntegrationStatus.TechName = EnumIntegrationStatus.Published.ToString();
            }
            foreach (var product in products.Skip(20).Take(30))
            {
                product.VendorCode = "vendor2";
                product.FurnitureType = "шкаф";
            }

            var query = new GetPublishedIntegrationProductsByVendorCodeQuery { ItemsOnPage = 50, Page = 1 };
            _contextMock.Setup(p => p.Set<IntegrationProduct>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.True(result.TotalCount == 1);
            Assert.True(result.Items.Where(p => p.VendorCode == "vendor").FirstOrDefault().Products.Count() == 20);
            Assert.True(result.Items.Where(p => p.VendorCode == "vendor2").Count() == 0);

        }
    }
}
