﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.PropertiesGrains.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Property.Query;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.PropertiesGrainsTests
{
    [TestFixture]
    public class GetPropertiesQueryHandlerTests
    {
        private Mock<IDBContextFactory> _contextFactoryMock;
        private Mock<DbContext> _contextMock;
        private GetPropertiesQueryHandler _classForTest;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _contextMock.Setup(p => p.Set<Property>()).Returns(new List<Property>().GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);

            _classForTest = new GetPropertiesQueryHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task GetPropertiesQueryHandlerTest_RequestEmpty_ReturnAll()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            var products = new List<Property>();
            for (var i = 0; i < 50; i++)
                products.Add(fixture.Build<Property>().Without(p => p.Products).Create());

            var query = new GetPropertiesQuery();
            _contextMock.Setup(p => p.Set<Property>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.AreEqual(result.Count(), products.Count());
        }

        [Test]
        public async Task GetPropertiesQueryHandlerTest_ByPropertyType_ReturnCorrectList()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            var propertyType = "propertyType";
            var products = new List<Property>();
            for (var i = 0; i < 50; i++)
                products.Add(fixture.Build<Property>().Without(p => p.Products).Create());
            products.ToList().ForEach(p => p.SyncCode1C = propertyType);

            var query = new GetPropertiesQuery { PropertyType = propertyType };
            _contextMock.Setup(p => p.Set<Property>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.AreEqual(result.Count(), products.Count());
            Assert.IsTrue(result.All(p => p.SyncCode1C == propertyType));
        }

        [Test]
        public async Task GetPropertiesQueryHandlerTest_ByPropertyType_ReturnCorrectListCount()
        {
            //arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            var propertyType = "propertyType";
            var products = new List<Property>();
            for (var i = 0; i < 50; i++)
                products.Add(fixture.Build<Property>().Without(p => p.Products).Create());

            products.Skip(12).ToList().ForEach(p => p.SyncCode1C = propertyType);

            var query = new GetPropertiesQuery { PropertyType = propertyType };
            _contextMock.Setup(p => p.Set<Property>()).Returns(products.ToList().GetQueryableMockDbSet());
            //act
            var result = await _classForTest.Execute(query);
            //assert
            Assert.AreEqual(result.Count(), 38);
            Assert.IsTrue(result.All(p => p.SyncCode1C == propertyType));
        }
    }
}
