﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using KonigDev.Lazurit.Hub.Grains.Orders.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Query;
using KonigDev.Lazurit.Core.Enums.Exeptions.Order;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.OrderTests
{
    [TestFixture]
    public class GetOrderQueryHandlerTests
    {
        private  Mock<IDBContextFactory> _contextFactoryMock;
        private Mock<DbContext> _contextMock;
        private Guid orderId = Guid.NewGuid();
        private GetOrderQueryHandler classForTest;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _contextMock.Setup(x => x.Set<Order>()).Returns(Orders.GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            classForTest = new GetOrderQueryHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task GetOrderQueryHandlerTests_OrderExist_ResultIsNotNull()
        {
            //arrange
            var command = new GetOrderQuery { Id = orderId };
            //act
            var result = await classForTest.Execute(command);
            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, orderId);
        }


        [Test]
        public void GetOrderQueryHandlerTests_OrderMissing_ThrowExeption()
        {
            //arrange
            var command = new GetOrderQuery { Id = Guid.NewGuid() };
            //act
            //assert
            Assert.ThrowsAsync<OrderNotFound>(async () => await classForTest.Execute(command));            
        }


        private List<Order> Orders
        {
            get
            {
                return new List<Order>
                {
                    new Order
                    {
                        Id = orderId
                    }
                };
            }

        }
    }
}
