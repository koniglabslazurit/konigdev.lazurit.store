﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using KonigDev.Lazurit.Hub.Grains.Orders.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Query;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.OrderTests
{

    public class GetOrdersQueryHandlerTests
    {
        private Mock<IDBContextFactory> _contextFactoryMock;
        private Mock<DbContext> _contextMock;
        private Guid orderId = Guid.NewGuid();
        private Guid userProfileId = Guid.NewGuid();
        private GetOrdersQueryHandler classForTest;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _contextMock.Setup(x => x.Set<Order>()).Returns(Orders.GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            classForTest = new GetOrdersQueryHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task GetOrdersQueryHandler_OrdersExistByUser_NotEmptyList()
        {
            //arrange
            var command = new GetOrdersQuery { UserProfileId = userProfileId };
            //act
            var result = await classForTest.Execute(command);
            //assert
            Assert.IsNotEmpty(result.Items);
        }

        [Test]
        public async Task GetOrdersQueryHandler_OrdersExistByUser_EmptyList()
        {
            //arrange
            var command = new GetOrdersQuery { UserProfileId = Guid.NewGuid() };
            //act
            var result = await classForTest.Execute(command);
            //assert
            Assert.IsEmpty(result.Items);
        }

        private List<Order> Orders
        {
            get
            {
                return new List<Order>
                {
                    new Order
                    {
                        Id = orderId,
                        UserProfileId  = userProfileId
                    }
                };
            }

        }
    }
}
