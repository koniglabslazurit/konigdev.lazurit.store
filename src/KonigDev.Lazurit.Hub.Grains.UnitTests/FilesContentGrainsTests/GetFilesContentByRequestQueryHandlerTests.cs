﻿using System;
using System.Collections.Generic;
using Moq;
using KonigDev.Lazurit.Model.Entities;
using KonigDev.Lazurit.Hub.Grains.Factories;
using System.Data.Entity;
using FluentAssertions;
using KonigDev.Lazurit.Hub.Grains.FilesContent.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Queries;
using NUnit.Framework;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.FilesContentGrainsTests
{
    [TestFixture]
    public class GetFilesContentByRequestQueryHandlerTests
    {
        private Mock<IDBContextFactory> _contextFactoryMock;
        private Mock<DbContext> _contextMock;
        private GetFilesContentByRequestQueryHandler _classForTest;
        private Guid _fileContentId;
        private Guid _objectId;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _classForTest = new GetFilesContentByRequestQueryHandler(_contextFactoryMock.Object);

            _fileContentId = Guid.NewGuid();
            _objectId = Guid.NewGuid();

            _contextMock.Setup(x => x.Set<FileContent>()).Returns(new List<FileContent>()
            {
                new FileContent
                {
                    Id = _fileContentId,
                    ObjectId = _objectId,
                    Type = new FileContentType {TechName = Core.Enums.EnumFileContentType.InInterior.ToString()}
                },
                new FileContent
                {
                    Id = Guid.NewGuid(),
                    ObjectId = _objectId,
                    Type = new FileContentType {TechName = Core.Enums.EnumFileContentType.InInterior.ToString()}
                },
                new FileContent
                {
                    Id = Guid.NewGuid(),
                    ObjectId = _objectId,
                    Type = new FileContentType {TechName = Core.Enums.EnumFileContentType.SliderMiddle.ToString()}
                }
            }.GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
        }

        [Test]
        public async Task Execute_GetByExistObjectId_ReturnNotEmptyList()
        {
            //arrange
            var query = new GetFilesContentByRequestQuery { ObjectId = _objectId };
            //act
            var result = await _classForTest.Execute(query);
            //assert
            result.Should().HaveCount(3);
        }

        [Test]
        public async Task Execute_GetByExistObjectIdAndExistType_ReturnNotEmptyList()
        {
            //arrange
            var query = new GetFilesContentByRequestQuery { ObjectId = _objectId, Type = Core.Enums.EnumFileContentType.InInterior };
            //act
            var result = await _classForTest.Execute(query);
            //assert
            result.Should().HaveCount(2);
        }

        [Test]
        public async Task Execute_GetByMissingObjectId_ReturnEmptyList()
        {
            //arrange
            var query = new GetFilesContentByRequestQuery { ObjectId = Guid.NewGuid() };
            //act
            var result = await _classForTest.Execute(query);
            //assert
            result.Should().BeEmpty();
            result.Should().HaveCount(0);
        }

        [Test]
        public async Task Execute_GetByMissingObjectIdAndType_ReturnEmptyList()
        {
            //arrange
            var query = new GetFilesContentByRequestQuery { ObjectId = Guid.NewGuid(), Type = Core.Enums.EnumFileContentType.InInterior };
            //act
            var result = await _classForTest.Execute(query);
            //assert
            result.Should().BeEmpty();
            result.Should().HaveCount(0);
        }

        [Test]
        public async Task Execute_GetByExistObjectIdAndMissingType_ReturnEmptyList()
        {
            //arrange
            var query = new GetFilesContentByRequestQuery { ObjectId = _objectId, Type = Core.Enums.EnumFileContentType.TileBig };
            //act
            var result = await _classForTest.Execute(query);
            //assert
            result.Should().BeEmpty();
            result.Should().HaveCount(0);
        }
    }
}
