﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Products.QueryHandlers;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Query;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.Products
{
    [TestFixture]
    class GetProductByUnicFieldsQueryHandlerTests
    {
        Mock<IDBContextFactory> _contextFactoryMock;
        Mock<DbContext> _contextMock;
        string _productSyncCode1C;
        GetProductByUnicFieldsQueryHandler _getProductByUnicFieldsQueryHandler;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _productSyncCode1C = "0001";

            _contextMock.Setup(x => x.Set<Product>()).Returns(new List<Product>()
            {
                new Product
                {
                    SyncCode1C = _productSyncCode1C,
                    Facing = new Facing(),
                    FacingColor = new FacingColor(),
                    FrameColor = new FrameColor()
                },
                new Product
                {
                    SyncCode1C = "test",
                    Facing = new Facing(),
                    FacingColor = new FacingColor(),
                    FrameColor = new FrameColor()
                }
            }.GetQueryableMockDbSet());

            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _getProductByUnicFieldsQueryHandler = new GetProductByUnicFieldsQueryHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task Execute_SyncCodeIsValid_ResponseIsNotNull()
        {
            //arrange

            //act
            var res = await _getProductByUnicFieldsQueryHandler.Execute(new GetProductByUniqueFieldsQuery
            {
                SyncCode1C = _productSyncCode1C
            });

            //assert
            Assert.IsNotNull(res);
            Assert.IsNotNull(res.Facing);
            Assert.IsNotNull(res.FacingColor);
            Assert.IsNotNull(res.FrameColor);
        }


        [Test]
        public async Task Execute_SyncCodeIsInvalid_ResponseIsNull()
        {
            //arrange

            //act
            var res = await _getProductByUnicFieldsQueryHandler.Execute(new GetProductByUniqueFieldsQuery
            {
                SyncCode1C = "invalid"
            });

            //assert
            Assert.IsNull(res);
        }

        [Test]
        public void Execute_SyncCodeIsEmpty_Exeption()
        {
            //arrange

            //act

            //assert
            Assert.ThrowsAsync<NotSupportedException>(async () => await _getProductByUnicFieldsQueryHandler
            .Execute(new GetProductByUniqueFieldsQuery { SyncCode1C = string.Empty }));
        }
    }
}
