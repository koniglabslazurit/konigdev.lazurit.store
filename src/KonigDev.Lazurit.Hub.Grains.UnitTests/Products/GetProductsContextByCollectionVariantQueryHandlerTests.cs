﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Products.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Products;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Query;
using KonigDev.Lazurit.Model.Context.DataContext;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.Products
{
    [TestFixture]
    public class GetProductsContextByCollectionVariantQueryHandlerTests
    {
        private Mock<IDBContextFactory> _contextFactory;
        private Mock<DbContext> _context;
        private GetProductsContentByCollectionVariantQueryHandler _service;

        [SetUp]

        public void SetUp()
        {
            _contextFactory = new Mock<IDBContextFactory>();
            _context = new Mock<DbContext>();
            var context = new LazuritBaseContext();
            _contextFactory.Setup(x => x.CreateLazuritContext()).Returns(_context.Object);
            _service = new GetProductsContentByCollectionVariantQueryHandler(_contextFactory.Object);
        }

        [Test]
        public void Execute_ReturnProductContextListWhenProductContextIsEnabled()
        {
            //arrange
            var p1 = Guid.NewGuid();
            var p2 = Guid.NewGuid();
            var p3 = Guid.NewGuid();
            var collVarId = Guid.NewGuid();

            var products = new List<Product>
            {
                new Product {Id = p1, CollectionsVariants= new List<CollectionVariant> {new CollectionVariant { Id= collVarId} }, Article = new Article { SyncCode1C="0001"} },
                new Product {Id = p2, CollectionsVariants= new List<CollectionVariant> {new CollectionVariant { Id= collVarId} }, Article = new Article { SyncCode1C="0001"}},
                new Product {Id = p3, CollectionsVariants= new List<CollectionVariant> {new CollectionVariant { Id= collVarId} }, Article = new Article { SyncCode1C="0001"}}
            }.GetQueryableMockDbSet();

            var productContexts = new List<CollectionVariantContentProductArea>
            {
                new CollectionVariantContentProductArea {Id = Guid.NewGuid(), CollectionVariantId = collVarId, ProductId = p1, IsEnabled = true, Height = 1, Left = 1, Top =1, Width=1},
                new CollectionVariantContentProductArea {Id = Guid.NewGuid(), CollectionVariantId = collVarId, ProductId = p2, IsEnabled = true, Height = 1, Left = 1, Top =1, Width=1},
                new CollectionVariantContentProductArea {Id = Guid.NewGuid(), CollectionVariantId = collVarId, ProductId = p3, IsEnabled = false, Height = 1, Left = 1, Top =1, Width=1} ,
            }.GetQueryableMockDbSet();

            _context.Setup(x => x.Set<Product>()).Returns(products);
            _context.Setup(x => x.Set<CollectionVariantContentProductArea>()).Returns(productContexts);

            //act
            var actual = _service.Execute(new GetProductsContextByCollectionVariantQuery { CollectionVariantId = collVarId });
            
            //assert
            Assert.NotNull(actual);
            Assert.IsInstanceOf<Task<List<DtoProductContentResult>>>(actual);
            Assert.AreEqual(3, actual.Result.Count);
            Assert.AreEqual(1, actual.Result.First().Height);
            Assert.IsNull(actual.Result.Last().ObjectId);
            Assert.AreEqual(0, actual.Result.Last().Height);
        }

        [Test]
        public async Task Execute_ReturnEmptyProductContextListWhenHavnotProducts()
        {
            //arrange
            var collVarId = Guid.NewGuid();
            var products = new List<Product>().GetQueryableMockDbSet();
            var productContexts = new List<CollectionVariantContentProductArea>().GetQueryableMockDbSet();
            _context.Setup(x => x.Set<Product>()).Returns(products);
            _context.Setup(x => x.Set<CollectionVariantContentProductArea>()).Returns(productContexts);

            //act
            var actual = await _service.Execute(new GetProductsContextByCollectionVariantQuery { CollectionVariantId = collVarId });

            //assert
            Assert.NotNull(actual);
            Assert.IsInstanceOf<List<DtoProductContentResult>>(actual);
            Assert.AreEqual(0, actual.Count);            
        }
    }
}
