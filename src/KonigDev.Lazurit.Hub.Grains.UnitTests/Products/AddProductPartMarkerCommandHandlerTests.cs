﻿using KonigDev.Lazurit.Core.Enums.Exeptions.Cart;
using KonigDev.Lazurit.Core.Enums.Exeptions.Product;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Products.CommandHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Command;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.Products
{
    public class AddProductPartMarkerCommandHandlerTests
    {
        Mock<IDBContextFactory> _contextFactoryMock;
        Mock<DbContext> _contextMock;
        AddProductPartMarkerCommandHandler _addProductPartMarkerCommandHandler;

        [SetUp]
        public void SetUp()
        {
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _contextMock.Setup(x => x.Set<Product>()).Returns(new List<Product>
            {
                new Product
                {
                    Id = Guid.NewGuid()
                }
            }.GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _addProductPartMarkerCommandHandler = new AddProductPartMarkerCommandHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task Execute_ProductNotFound_ThrownException()
        {
            //arrange
            var command = new AddProductPartMarkerCommand { ProductId = Guid.NewGuid(), ProductPartMarkerId = Guid.NewGuid() };
            //act
            //assert
            Assert.ThrowsAsync<ProductNotFound>(async () => await _addProductPartMarkerCommandHandler.Execute(command));
        }
    }
}
