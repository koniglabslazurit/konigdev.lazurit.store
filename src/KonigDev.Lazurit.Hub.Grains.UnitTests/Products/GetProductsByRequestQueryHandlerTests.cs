﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Products.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Query;
using KonigDev.Lazurit.Model.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.UnitTests.Products
{
    [TestFixture]
    public class GetProductsByRequestQueryHandlerTests
    {
        Mock<IDBContextFactory> _contextFactoryMock;
        Mock<DbContext> _contextMock;
        GetProductsByRequestQueryHandler _getProductsByRequestQueryHandler;
        Guid _collectionId;
        Guid _facingId;
        Guid _facingColorId;
        Guid _facingColorId2;
        Guid _frameColorId;
        Guid _furnitureTypeId;
        Guid _collectionVariantId;
        List<Guid?> _facingIds;
        List<Guid?> _facingColorIds;
        List<Guid?> _frameColorIds;

        [SetUp]
        public void SetUp()
        {
            _collectionId = Guid.NewGuid();

            _facingId = Guid.NewGuid();
            _facingIds = new List<Guid?> { _facingId };
            _facingColorId = Guid.NewGuid();
            _facingColorId2 = Guid.NewGuid();
            _facingColorIds = new List<Guid?> { _facingColorId, _facingColorId2 };
            _frameColorId = Guid.NewGuid();
            _frameColorIds = new List<Guid?> { _frameColorId };
            _furnitureTypeId = Guid.NewGuid();
            _collectionVariantId = Guid.NewGuid();
            _contextFactoryMock = new Mock<IDBContextFactory>();
            _contextMock = new Mock<DbContext>();
            _contextMock.Setup(x => x.Set<Product>()).Returns(new List<Product>
            {
                new Product
                {
                    Id = Guid.NewGuid(),
                    FacingColorId = _facingColorId,
                    FrameColorId = _frameColorId,
                    FacingId = _facingId,
                    FavoriteItemProducts = new List<FavoriteItemProduct>(),
                     FacingColor = new FacingColor {Id = _facingColorId, IsUniversal = false},
                    Article = new Article { FurnitureTypeId = _furnitureTypeId, FurnitureType = new FurnitureType { Name = "Sofa"} },
                    CollectionId = _collectionId,
                    Collection = new Collection {Id = _collectionId, Series = new Series { Name = "series"}, RoomType = new RoomType { Name = "room"} },
                    CollectionsVariants = new List <CollectionVariant>                    {
                        new CollectionVariant {Id = _collectionVariantId, CollectionId = _collectionId,
                            Collection = new Collection
                            { Series = new Series{ Alias = "" } } } }
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    FacingColorId = _facingColorId2,
                    FrameColorId = Guid.NewGuid(),
                    FacingId = _facingId,
                    FavoriteItemProducts = new List<FavoriteItemProduct>(),
                    FacingColor = new FacingColor {Id = _facingColorId2, IsUniversal = false},
                    Article = new Article { FurnitureTypeId = _furnitureTypeId, FurnitureType = new FurnitureType { Name = "Sofa"}},
                    CollectionId = _collectionId,
                    Collection = new Collection {Id = _collectionId, Series = new Series { Name = "series"}, RoomType = new RoomType { Name = "room"} },
                    CollectionsVariants = new List <CollectionVariant> { new CollectionVariant { Id = _collectionVariantId, CollectionId = _collectionId, Collection = new Collection { Series = new Series { Alias = "" } } } }
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    FacingColorId = _facingColorId,
                    FrameColorId = Guid.NewGuid(),
                    FacingId = Guid.NewGuid(),
                    FavoriteItemProducts = new List<FavoriteItemProduct>(),
                    FacingColor = new FacingColor {Id = _facingColorId, IsUniversal = false},
                    Article = new Article { FurnitureTypeId = _furnitureTypeId, FurnitureType = new FurnitureType { Name = "Sofa"}},
                    CollectionId = _collectionId,
                    Collection = new Collection {Id = _collectionId, Series = new Series { Name = "series"}, RoomType = new RoomType { Name = "room"} },
                    CollectionsVariants = new List <CollectionVariant> { new CollectionVariant { Id = _collectionVariantId, CollectionId = _collectionId, Collection = new Collection {Series = new Series { Alias = "" } } } }
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    FacingColorId = _facingColorId2,
                    FrameColorId = Guid.NewGuid(),
                    FacingId = Guid.NewGuid(),
                    FavoriteItemProducts = new List<FavoriteItemProduct>(),
                    FacingColor = new FacingColor {Id = _facingColorId2, IsUniversal = true},
                    Article = new Article { FurnitureTypeId = Guid.NewGuid(), FurnitureType = new FurnitureType { Name = " "}},
                    CollectionId = _collectionId,
                    Collection = new Collection {Id = _collectionId, Series = new Series { Name = "series"}, RoomType = new RoomType { Name = "room"} },
                    CollectionsVariants = new List <CollectionVariant> { new CollectionVariant { Id = _collectionVariantId, CollectionId = _collectionId, Collection = new Collection { Series = new Series { Alias = "" } } } }
                },
                 new Product
                {
                    Id = Guid.NewGuid(),
                    FacingColorId = _facingColorId,
                    FrameColorId = Guid.NewGuid(),
                    FacingId = Guid.NewGuid(),
                    FavoriteItemProducts = new List<FavoriteItemProduct>(),
                     FacingColor = new FacingColor {Id = _facingColorId, IsUniversal = false},
                    Article = new Article { FurnitureTypeId = Guid.NewGuid(), FurnitureType = new FurnitureType { Name = " "}},
                    CollectionId = Guid.NewGuid(), 
                    Collection = new Collection {Id = Guid.NewGuid(), Series = new Series { Name = "series"}, RoomType = new RoomType { Name = "room"} },
                    CollectionsVariants = new List <CollectionVariant> { new CollectionVariant { Id = _collectionVariantId, CollectionId = Guid.NewGuid(), Collection = new Collection { Series = new Series { Alias = "" } } } }
                },
            }.GetQueryableMockDbSet());
            _contextFactoryMock.Setup(x => x.CreateLazuritContext()).Returns(_contextMock.Object);
            _getProductsByRequestQueryHandler = new GetProductsByRequestQueryHandler(_contextFactoryMock.Object);
        }

        [Test]
        public async Task Execute_RequestIsValid_GetProducts()
        {
            //arrange

            //act
            var res = await _getProductsByRequestQueryHandler.Execute(new GetProductsByRequestQuery
            {
                CollectionId = _collectionId,
                FacingColorIds = _facingColorIds,
                FacingIds = _facingIds,
                FrameColorIds = _frameColorIds,
                FurnitureTypeId = _furnitureTypeId
            });

            //assert
            Assert.True(res.Count == 1);
        }
        [Test]
        public async Task Execute_RequestIsCollectionId_GetProducts()
        {
            //arrange

            //act
            var res = await _getProductsByRequestQueryHandler.Execute(new GetProductsByRequestQuery { CollectionId = _collectionId });

            //assert
            Assert.AreEqual(res.Count, 4);
        }
        [Test]
        public async Task Execute_RequestIsFacingId_GetProducts()
        {
            //arrange

            //act
            var res = await _getProductsByRequestQueryHandler.Execute(new GetProductsByRequestQuery { FacingIds = _facingIds });

            //assert
            Assert.True(res.Count == 2);
        }
        [Test]
        public async Task Execute_RequestIsFacingColorId_GetProducts()
        {
            //arrange

            //act
            var res = await _getProductsByRequestQueryHandler.Execute(new GetProductsByRequestQuery { FacingColorIds = _facingColorIds });

            //assert
            Assert.True(res.Count == 5);
        }
        [Test]
        public async Task Execute_RequestIsFrameColorId_GetProducts()
        {
            //arrange

            //act
            var res = await _getProductsByRequestQueryHandler.Execute(new GetProductsByRequestQuery { FrameColorIds = _frameColorIds });

            //assert
            Assert.True(res.Count == 1);
        }
        [Test]
        public async Task Execute_RequestIsFurnitureTypeId_GetProducts()
        {
            //arrange

            //act
            var res = await _getProductsByRequestQueryHandler.Execute(new GetProductsByRequestQuery { FurnitureTypeId = _furnitureTypeId });

            //assert
            Assert.True(res.Count == 3);
        }
        [Test]
        public async Task Execute_RequestIsEmpty_GetProducts()
        {
            //arrange

            //act
            var res = await _getProductsByRequestQueryHandler.Execute(new GetProductsByRequestQuery());

            //assert
            Assert.True(res.Count == 5);
        }
        [Test]
        public async Task Execute_RequestIsCollectionVariantId_GetProducts()
        {
            //arrange

            //act
            var res = await _getProductsByRequestQueryHandler.Execute(new GetProductsByRequestQuery { CollectionVariantId = _collectionVariantId });

            //assert
            Assert.True(res.Count == 5);
        }
    }
}
