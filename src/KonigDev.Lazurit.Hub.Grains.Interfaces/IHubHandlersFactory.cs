﻿namespace KonigDev.Lazurit.Hub.Grains.Interfaces
{
    /// <summary>
    /// Универсальая фабрика для создания запросов.
    /// Должна быть реализована в CompositionRoot(web api проект, главный проект сайта и т.д.)
    /// </summary>
    public interface IHubHandlersFactory
    {
        IHubQueryHandler<TQuery, TResult> CreateQueryHandler<TQuery, TResult>();
        IHubCommandHandler<TCommand> CreateCommandHandler<TCommand>();
    }
}
