﻿using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Interfaces
{
    public interface IHubQueryHandler<TQuery, TResult>: IGrainWithGuidKey
    {
        Task<TResult> Execute(TQuery query);
    }
}
