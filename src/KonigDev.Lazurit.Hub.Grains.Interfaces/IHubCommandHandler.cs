﻿using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Interfaces
{
    public interface IHubCommandHandler<TCommand> : IGrainWithGuidKey
    {
        Task Execute(TCommand command);
    }
}