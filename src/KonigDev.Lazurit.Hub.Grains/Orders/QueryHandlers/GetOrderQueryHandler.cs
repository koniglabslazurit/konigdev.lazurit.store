﻿using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Query;
using Orleans;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Model.Entities;
using System.Linq;
using KonigDev.Lazurit.Core.Enums.Exeptions.Order;

namespace KonigDev.Lazurit.Hub.Grains.Orders.QueryHandlers
{
    public class GetOrderQueryHandler : Grain, IHubQueryHandler<GetOrderQuery, DtoOrder>
    {

        private readonly IDBContextFactory _dbContextFactory;

        public GetOrderQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<DtoOrder> Execute(GetOrderQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var result = context.Set<Order>()
                    .Where(p => p.Id == query.Id)
                    .Select(p => new DtoOrder
                    {
                        Id = p.Id,
                        CreationTime = p.CreationTime,
                        IsApprovedBySeller = p.IsApprovedBySeller,
                        IsDeliveryRequired = p.IsDeliveryRequired,
                        Sum = p.Sum,
                        UserProfileId = p.UserProfileId,
                        Collections = p.Collections.Select(c => new DtoOrderCollection
                        {
                            Quantity = c.Quantity,
                            RoomName = c.CollectionVariant.Collection.RoomType.Name,
                            SeriaName = c.CollectionVariant.Collection.Series.Name,
                            SeriaAlias = c.CollectionVariant.Collection.Series.Alias,
                            Products = c.Products.Select(x => new DtoOrderProductInCollection
                            {
                                Article = x.Product.Article.SyncCode1C,
                                SyncCode1C = x.Product.SyncCode1C,
                                Discount = x.Discount,
                                FurnitureType = x.Product.Article.FurnitureType.Name,
                                IsAssemblyRequired = x.IsAssemblyRequired,
                                Id = x.Id,
                                Price = x.Price,
                                Quantity = x.Quantity,
                            }).ToList()
                        }).ToList(),
                        Complects = p.Complects.Select(c => new DtoOrderComplect
                        {
                            Id = c.Id,
                            IsAssemblyRequired = c.IsAssemblyRequired,
                            Price = c.Price,
                            Quantity = c.Quantity,
                            RoomName = c.CollectionVariant.Collection.RoomType.Name,
                            SeriaName = c.CollectionVariant.Collection.Series.Name,
                            Discount = c.Discount
                        }).ToList(),
                        Products = p.Products.Select(c => new DtoOrderProduct
                        {
                            Article = c.Product.Article.Name,
                            Discount = c.Discount,
                            FurnitureType = c.Product.Article.FurnitureType.Name,
                            Id = c.Id,
                            IsAssemblyRequired = c.IsAssemblyRequired,
                            Price = c.Price,
                            Quantity = c.Quantity,
                            SeriaName = c.Product.Collection.Series.Name,
                            SeriaAlias = c.Product.Collection.Series.Alias,
                            SyncCode1C = c.Product.SyncCode1C
                        }).ToList()
                    }).SingleOrDefault();

                if (result == null)
                    throw new OrderNotFound(Core.Enums.Constants.Exceptions.Order.OrderNotFound);
                return Task.FromResult(result);
            }
        }
    }
}
