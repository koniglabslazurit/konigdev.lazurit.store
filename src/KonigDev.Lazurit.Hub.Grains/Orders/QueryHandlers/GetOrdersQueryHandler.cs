﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Orders.QueryHandlers
{
    public class GetOrdersQueryHandler : Grain, IHubQueryHandler<GetOrdersQuery, DtoOrderTableResponse>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetOrdersQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public Task<DtoOrderTableResponse> Execute(GetOrdersQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var queryDb = context.Set<Order>().AsQueryable();
                if (query.IsPaid.HasValue)
                    queryDb = queryDb.Where(p => p.IsPaid == query.IsPaid.Value);

                if (query.IsApprovedBySeller.HasValue)
                    queryDb = queryDb.Where(p => p.IsApprovedBySeller == query.IsApprovedBySeller.Value);

                if (query.UserProfileId != Guid.Empty)
                    queryDb = queryDb.Where(p => p.UserProfileId == query.UserProfileId);

                var result = queryDb.Select(p => new DtoOrder
                {
                    Id = p.Id,
                    CreationTime = p.CreationTime,
                    IsApprovedBySeller = p.IsApprovedBySeller,
                    IsDeliveryRequired = p.IsDeliveryRequired,
                    Sum = p.Sum,
                    UserProfileId = p.UserProfileId,
                    CreationTime1C = p.CreationTime1C,
                    OrderNumber1C = p.OrderNumber1C,
                    Collections = p.Collections.Select(c => new DtoOrderCollection
                    {
                        Quantity = c.Quantity,
                        RoomName = c.CollectionVariant.Collection.RoomType.Name,
                        SeriaName = c.CollectionVariant.Collection.Series.Name,
                        SeriaAlias = c.CollectionVariant.Collection.Series.Alias,
                        Products = c.Products.Select(x => new DtoOrderProductInCollection
                        {
                            Article = x.Product.Article.SyncCode1C,
                            SyncCode1C = x.Product.SyncCode1C,
                            Discount = x.Discount,
                            FurnitureType = x.Product.Article.FurnitureType.Name,
                            IsAssemblyRequired = x.IsAssemblyRequired,
                            Id = x.Id,
                            Price = x.Price,
                            Quantity = x.Quantity,
                        }).ToList()
                    }).ToList(),
                    Complects = p.Complects.Select(c => new DtoOrderComplect
                    {
                        Id = c.Id,
                        IsAssemblyRequired = c.IsAssemblyRequired,
                        Price = c.Price,
                        Quantity = c.Quantity,
                        RoomName = c.CollectionVariant.Collection.RoomType.Name,
                        SeriaName = c.CollectionVariant.Collection.Series.Name,
                        Discount = c.Discount
                    }).ToList(),
                    Products = p.Products.Select(c => new DtoOrderProduct
                    {
                        Article = c.Product.Article.Name,
                        Discount = c.Discount,
                        FurnitureType = c.Product.Article.FurnitureType.Name,
                        Id = c.Id,
                        IsAssemblyRequired = c.IsAssemblyRequired,
                        Price = c.Price,
                        Quantity = c.Quantity,
                        SeriaName = c.Product.Collection.Series.Name,
                        SeriaAlias = c.Product.Collection.Series.Alias,
                        SyncCode1C = c.Product.SyncCode1C
                    }).ToList()
                }).ToList();

                return Task.FromResult(new DtoOrderTableResponse { Items = result, TotalCount = result.Count });
            }
        }
    }
}
