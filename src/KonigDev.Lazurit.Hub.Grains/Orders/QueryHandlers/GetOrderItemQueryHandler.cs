﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Linq;
using System.Threading.Tasks;


namespace KonigDev.Lazurit.Hub.Grains.Orders.QueryHandlers
{
    public class GetOrderItemQueryHandler : Grain, IHubQueryHandler<GetOrderItemQuery, DtoOrderItem>
    {
        private readonly IDBContextFactory _dbContextFactory;
        public GetOrderItemQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<DtoOrderItem> Execute(GetOrderItemQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {                
                var result = context.Set<Order>()
                        .Where(p => p.Id == query.Id)
                        .Select(p => new DtoOrderItem
                        {
                            Id = p.Id,
                            IsPaid = p.IsPaid,
                            Number = p.Number,
                            Sum = p.Sum,
                            PaymentMethod = p.PaymentMethod.TechName
                        }).SingleOrDefault();                
                return Task.FromResult(result);
            }
        }
    }
}
