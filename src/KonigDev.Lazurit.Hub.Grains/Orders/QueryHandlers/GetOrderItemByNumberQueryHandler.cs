﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Orders.QueryHandlers
{
    public class GetOrderItemByNumberQueryHandler : Grain, IHubQueryHandler<GetOrderItemByNumberQuery, DtoOrderItem>
    {
        private readonly IDBContextFactory _dbContextFactory;
        public GetOrderItemByNumberQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<DtoOrderItem> Execute(GetOrderItemByNumberQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var result = context.Set<Order>()
                        .Where(p => p.Number == query.Number)
                        .Select(p => new DtoOrderItem
                        {
                            Id = p.Id,
                            IsPaid = p.IsPaid,
                            Number = p.Number,
                            Sum = p.Sum,
                            PaymentMethod = p.PaymentMethod.TechName
                        }).SingleOrDefault();
                return Task.FromResult(result);
            }
        }
    }
}
