﻿using System.Linq;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Command;
using Orleans;
using System;
using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using System.Threading.Tasks;
using KonigDev.Lazurit.Model.Entities;
using KonigDev.Lazurit.Core.Enums.Exeptions.Order;

namespace KonigDev.Lazurit.Hub.Grains.Orders.CommandHandlers
{
    [Serializable]
    public class UpdateOrder1CFieldsCommandHandler : Grain, IHubCommandHandler<UpdateOrder1CFieldsCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public UpdateOrder1CFieldsCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task Execute(UpdateOrder1CFieldsCommand command)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var order = context.Set<Order>().SingleOrDefault(p => p.Id == command.Id);
                if (order == null)
                    throw new OrderNotFound(Core.Enums.Constants.Exceptions.Order.OrderNotFound);
                order.CreationTime1C = command.CreationTime1C;
                order.OrderNumber1C = command.OrderNumber1C;
                await context.SaveChangesAsync();
            }
        }
    }
}
