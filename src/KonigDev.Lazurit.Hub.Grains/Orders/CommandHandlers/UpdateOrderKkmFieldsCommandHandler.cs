﻿using System.Linq;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Command;
using Orleans;
using System;
using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using System.Threading.Tasks;
using KonigDev.Lazurit.Model.Entities;
using KonigDev.Lazurit.Core.Enums.Exeptions.Order;

namespace KonigDev.Lazurit.Hub.Grains.Orders.CommandHandlers
{
    [Serializable]
    public class UpdateOrderKkmFieldsCommandHandler : Grain, IHubCommandHandler<UpdateOrderKkmFieldsCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public UpdateOrderKkmFieldsCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task Execute(UpdateOrderKkmFieldsCommand command)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var number = Convert.ToInt64(command.InvId);
                var order = context.Set<Order>().SingleOrDefault(p => p.Number == number);
                if (order == null)
                    throw new OrderNotFound(Core.Enums.Constants.Exceptions.Order.OrderNotFound);
                order.KkmOrderRecordUrl = command.KkmOrderRecordUrl;
                await context.SaveChangesAsync();
            }
        }
    }
}
