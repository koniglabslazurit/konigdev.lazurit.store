﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Command;
using Orleans;
using System;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Model.Entities;
using System.Linq;
using KonigDev.Lazurit.Core.Enums.Exeptions.Order;

namespace KonigDev.Lazurit.Hub.Grains.Orders.CommandHandlers
{
    [Serializable]
    public class UpdateOrderPaidCommandHandler : Grain, IHubCommandHandler<UpdateOrderPaidCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public UpdateOrderPaidCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task Execute(UpdateOrderPaidCommand command)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var order = context.Set<Order>().SingleOrDefault(p => p.Id == command.Id);
                if (order == null)
                    throw new OrderNotFound(Core.Enums.Constants.Exceptions.Order.OrderNotFound);
                order.IsPaid = command.IsPaid;
                await context.SaveChangesAsync();
            }
        }
    }
}
