﻿using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Command;
using Orleans;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using KonigDev.Lazurit.Model.Entities;

namespace KonigDev.Lazurit.Hub.Grains.Orders.CommandHandlers
{
    public class CreateOrderCommandHandler : Grain, IHubCommandHandler<CreateOrderCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;
        private readonly ITransactionFactory _transactionFactory;

        public CreateOrderCommandHandler(IDBContextFactory dbContextFactory, ITransactionFactory transactionFactory)
        {
            _dbContextFactory = dbContextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(CreateOrderCommand command)
        {
            Order order = new Order
            {
                Id = command.Id,
                CreationTime = DateTime.UtcNow,
                IsApprovedBySeller = command.IsApprovedBySeller,
                IsDeliveryRequired = command.IsDeliveryRequired,
                Sum = command.Sum,
                DeliveryAddress = command.DeliveryAddress,
                DeliveryKladrCode = command.DeliveryKladrCode,
                PaymentMethodId = command.PaymentMethodId,
                UserProfileId = command.UserProfileId,                
                Collections = command.Collections.Select(p => new OrderItemCollection
                {
                    CollectionVariantId = p.CollectionVariantId,
                    Id = p.Id,
                    OrderId = command.Id,
                    Quantity = p.Quantity,
                    Products = p.Products.Select(x => new OrderItemProductInCollection
                    {
                        Id = x.Id,
                        Quantity = x.Quantity,
                        Discount = x.Discount,
                        IsAssemblyRequired = x.IsAssemblyRequired,
                        OrderItemCollectionId = p.Id,
                        Price = x.Price,
                        ProductId = x.ProductId
                    }).ToList()
                }).ToList(),
                Complects = command.Complects.Select(p => new OrderItemComplect
                {
                    Id = p.Id,
                    CollectionVariantId = p.CollectionVariantId,
                    Discount = p.Discount,
                    IsAssemblyRequired = p.IsAssemblyRequired,
                    OrderId = command.Id,
                    Quantity = p.Quantity,
                    Price = p.Price
                }).ToList(),
                Products = command.Products.Select(p=>new OrderItemProduct {
                    Id = p.Id,
                    Quantity = p.Quantity,
                    Discount = p.Discount,
                    IsAssemblyRequired = p.IsAssemblyRequired,                    
                    Price = p.Price,
                    ProductId = p.ProductId, 
                    OrderId = command.Id
                })
                .ToList()
            };

            using (var transaction = _transactionFactory.CreateScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var context = _dbContextFactory.CreateLazuritContext())
                {
                    context.Set<Order>().Add(order);
                    await context.SaveChangesAsync();
                }
                transaction.Complete();
            }
        }
    }
}
