﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Queries;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Dto;

namespace KonigDev.Lazurit.Hub.Grains.PriceZones.QueryHandlers
{
    /// <summary>
    /// Получение списка всех ценовых зон
    /// </summary>
    public class GetPrizeZonesQueryHandler : Grain, IHubQueryHandler<GetPrizeZonesQuery, List<DtoPriceZoneItem>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetPrizeZonesQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoPriceZoneItem>> Execute(GetPrizeZonesQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var result = context.Set<PriceZone>()
                    .Select(p => new DtoPriceZoneItem
                    {
                        Id = p.Id,
                        SyncCode1C = p.SyncCode1C
                    }).ToList();
                    
                return Task.FromResult(result);
            }
        }
    }
}
