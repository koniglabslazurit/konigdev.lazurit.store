﻿using KonigDev.Lazurit.Core.Enums.Constants;
using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Queries;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.PriceZones.QueryHandlers
{
    public class GetPriceZoneQueryHandler: Grain, IHubQueryHandler<GetPriceZoneQuery, DtoPriceZone>
    {
        private readonly IDBContextFactory _contextFactory;
        public GetPriceZoneQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }
        public Task<DtoPriceZone> Execute(GetPriceZoneQuery query)
        {
            if (query==null)
                throw new NotValidCommandException(Exceptions.Common.ModelEmpty);
            if (query.Id == Guid.Empty)
                throw new NotValidCommandException(Exceptions.Common.ModelIdEmpty);
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var zone = context.Set<PriceZone>()
                    .Select(x => new DtoPriceZone
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Number = x.Number,
                        SyncCode1C = x.SyncCode1C
                    }).SingleOrDefault(x=>x.Id==query.Id);
                return Task.FromResult(zone);
            }
        }
    }
}
