﻿using System;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Commands;
using System.Linq;
using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Core.Enums.Constants;

namespace KonigDev.Lazurit.Hub.Grains.PriceZones.CommandHandlers
{
    public class UpdatePriceZoneCommandHandler : Grain, IHubCommandHandler<UpdatePriceZoneCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        public UpdatePriceZoneCommandHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public async Task Execute(UpdatePriceZoneCommand command)
        {
            if (command == null)
                throw new NotValidCommandException(Exceptions.Common.ModelEmpty);
            if (command.Id == Guid.Empty)
                throw new NotValidCommandException(Exceptions.Common.ModelIdEmpty);
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var zone = context.Set<PriceZone>().SingleOrDefault(x => x.Id == command.Id);
                if (zone == null)
                    throw new NotValidCommandException(Exceptions.PriceZone.PriceZoneNotFound);
                zone.Name = command.Name;
                zone.Number = command.Number;
                zone.SyncCode1C = command.SyncCode1C;
                await context.SaveChangesAsync();
            }
        }
    }
}
