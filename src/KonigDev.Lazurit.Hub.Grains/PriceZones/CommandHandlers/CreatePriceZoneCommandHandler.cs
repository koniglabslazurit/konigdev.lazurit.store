﻿using System;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Commands;
using System.Linq;
using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Core.Enums.Constants;

namespace KonigDev.Lazurit.Hub.Grains.PriceZones.CommandHandlers
{
    public class CreatePriceZoneCommandHandler : Grain, IHubCommandHandler<CreatePriceZoneCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        public CreatePriceZoneCommandHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public async Task Execute(CreatePriceZoneCommand command)
        {
            if (command == null)
                throw new NotValidCommandException(Exceptions.Common.ModelEmpty);
            if (command.Id == Guid.Empty)
                throw new NotValidCommandException(Exceptions.Common.ModelIdEmpty);
            if (string.IsNullOrEmpty(command.Name))
                throw new NotValidCommandException(Exceptions.Common.ModelNameEmpty);
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var zone = new PriceZone
                {
                    Id = command.Id,
                    Name = command.Name,
                    Number = command.Number,
                    SyncCode1C = command.SyncCode1C
                };
                context.Set<PriceZone>().Add(zone);
                await context.SaveChangesAsync();                
            }
        }
    }
}