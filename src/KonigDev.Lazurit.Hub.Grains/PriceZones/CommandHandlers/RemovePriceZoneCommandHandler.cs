﻿using KonigDev.Lazurit.Core.Enums.Constants;
using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Commands;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.PriceZones.CommandHandlers
{
    public class RemovePriceZoneCommandHandler: Grain, IHubCommandHandler<RemovePriceZoneCommand> 
    {
        private readonly IDBContextFactory _contextFactory;
        public RemovePriceZoneCommandHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }
        public async Task Execute(RemovePriceZoneCommand command)
        {
            if (command ==null)
                throw new NotValidCommandException(Exceptions.Common.ModelEmpty);
            if (command.Id == Guid.Empty)
                throw new NotValidCommandException(Exceptions.Common.ModelIdEmpty);
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var zone = context.Set<PriceZone>()
                        .SingleOrDefault(x => x.Id == command.Id);
                if (zone == null)
                    throw new NotValidCommandException(Exceptions.PriceZone.PriceZoneNotFound);
                context.Set<PriceZone>().Remove(zone);
                await context.SaveChangesAsync();
            }
        }
    }
}
