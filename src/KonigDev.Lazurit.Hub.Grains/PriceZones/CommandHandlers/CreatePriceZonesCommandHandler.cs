﻿using System;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Commands;
using KonigDev.Lazurit.Core.Transactions;
using System.Transactions;
using System.Linq;

namespace KonigDev.Lazurit.Hub.Grains.PriceZones.CommandHandlers
{
    [Serializable]
    public class CreatePriceZonesCommandHandler : Grain, IHubCommandHandler<CreatePriceZonesCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;
        public CreatePriceZonesCommandHandler(IDBContextFactory contextFactory, ITransactionFactory transactionFactory)
        {
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(CreatePriceZonesCommand command)
        {
            var list = command.PrizeSones.Select(p => new PriceZone
            {
                Id = p.Id,
                Name = p.SyncCode1C,
                SyncCode1C = p.SyncCode1C
            }).ToList();
            using (var transaction = _transactionFactory.CreateScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var context = _contextFactory.CreateLazuritContext())
                {

                    context.Set<PriceZone>().AddRange(list);
                    //await context.SaveChangesAsync();
                    try
                    {
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                transaction.Complete();
            }
        }
    }
}
