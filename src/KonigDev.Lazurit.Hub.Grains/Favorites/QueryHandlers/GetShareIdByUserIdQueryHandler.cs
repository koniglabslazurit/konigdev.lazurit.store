﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Favorite;
using KonigDev.Lazurit.Hub.Model.DTO.Favorite.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Favorites.QueryHandlers
{
    public class GetShareIdByUserIdQueryHandler : Grain, IHubQueryHandler<GetShareIdByUserIdQuery, DtoShareId>
    {
        private readonly IDBContextFactory _dbContextFactory;
        public GetShareIdByUserIdQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public Task<DtoShareId> Execute(GetShareIdByUserIdQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {


                var userProfile = context.Set<UserProfile>().FirstOrDefault(x => x.Id == query.UserId);
                if (userProfile == null)
                    return Task.FromResult<DtoShareId>(null);

                return Task.FromResult(new DtoShareId
                {
                    Id = userProfile.LinkId
                });
            }
        }
    }
}
