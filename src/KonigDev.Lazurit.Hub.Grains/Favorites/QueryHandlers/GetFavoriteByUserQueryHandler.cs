﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Favorite;
using KonigDev.Lazurit.Hub.Model.DTO.Favorite.Query;
using Orleans;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Model.Entities;
using KonigDev.Lazurit.Hub.Model.DTO.Products;

namespace KonigDev.Lazurit.Hub.Grains.Favorites.QueryHandlers
{
    public class GetFavoriteByUserQueryHandler : Grain, IHubQueryHandler<GetFavoriteByUserQuery, DtoFavorite>
    {
        private readonly IDBContextFactory _dbContextFactory;
        public GetFavoriteByUserQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public Task<DtoFavorite> Execute(GetFavoriteByUserQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var dbQuery = context.Set<Favorite>()
                    .Where(p => p.Id == query.UserId)
                    .Select(favorite => new DtoFavorite
                    {
                        Id = favorite.Id,
                        Products = favorite.Products.Select(productItem => new DtoFavoriteItemProduct
                        {
                            Id = productItem.Id,
                            Product = new DtoFavoriteProduct
                            {
                                Id = productItem.Product.Id,
                                Article = productItem.Product.Article.SyncCode1C,
                                Height = productItem.Product.Height,
                                Width = productItem.Product.Width,
                                Length = productItem.Product.Length,
                                FacingColorName = productItem.Product.FacingColor.Name,
                                FacingName = productItem.Product.Facing.Name,
                                FrameColorIdName = productItem.Product.FrameColor.Name,
                                FurnitureType = productItem.Product.Article.FurnitureType.Name,
                                FurnitureTypeAlias = productItem.Product.Article.FurnitureType.Alias,
                                SyncCode1C = productItem.Product.SyncCode1C,
                                CollectionAlias = productItem.Product.Collection.Series.Alias,
                                CollectionName = productItem.Product.Collection.Series.Name,
                            }
                        }).ToList(),
                        Collections = favorite.Collections.Select(collection => new DtoFavoriteItemCollection
                        {
                            Id = collection.Id,
                            CollectionVariant = new DtoFavoriteCollectionVariant
                            {
                                Id = collection.CollectionVariant.Id,
                                CollectionAlias = collection.CollectionVariant.Collection.Series.Alias,
                                CollectionName = collection.CollectionVariant.Collection.Series.Name,
                                RoomTypeAlias = collection.CollectionVariant.Collection.RoomType.Alias,
                                RoomTypeName = collection.CollectionVariant.Collection.RoomType.Name
                            },
                            Products = collection.Products.Select(productItem => new DtoFavoriteItemProductInCollection
                            {
                                Id = productItem.Id,
                                Product = new DtoFavoriteProductInCollection
                                {
                                    Id = productItem.Product.Id,
                                    FurnitureType = productItem.Product.Article.FurnitureType.Name,
                                    FurnitureTypeAlias = productItem.Product.Article.FurnitureType.Alias,
                                    Article = productItem.Product.Article.SyncCode1C,
                                    SyncCode1C = productItem.Product.SyncCode1C,
                                    FacingColorName = productItem.Product.FacingColor.Name,
                                    FacingName = productItem.Product.Facing.Name,
                                    FrameColorIdName = productItem.Product.FrameColor.Name,
                                    Height = productItem.Product.Height,
                                    Width = productItem.Product.Width,
                                    Length = productItem.Product.Length
                                }
                            }).ToList()
                        }).ToList(),
                        Complects = favorite.Complects.Select(complect => new DtoFavoriteItemComplect
                        {
                            Id = complect.Id,
                            CollectionVariant = new DtoFavoriteComplect
                            {
                                Id = complect.CollectionVariant.Id,
                                CollectionName = complect.CollectionVariant.Collection.Series.Name,
                                CollectionAlias = complect.CollectionVariant.Collection.Series.Alias,
                                RoomTypeName = complect.CollectionVariant.Collection.RoomType.Name,
                                RoomTypeAlias = complect.CollectionVariant.Collection.RoomType.Alias,
                                Products = complect.CollectionVariant.Products.Select(product => new DtoFavoriteComplectProductItem
                                {
                                    Id = product.Id,
                                    Article = product.Article.SyncCode1C,
                                    Height = product.Height,
                                    Width = product.Width,
                                    Length = product.Length,
                                    FacingColorName = product.FacingColor.Name,
                                    FacingName = product.Facing.Name,
                                    FrameColorIdName = product.FrameColor.Name,
                                    FurnitureType = product.Article.FurnitureType.Name,
                                    FurnitureTypeAlias = product.Article.FurnitureType.Alias,
                                    SyncCode1C = product.SyncCode1C
                                })
                            }
                        }).ToList()
                    });

                var result = dbQuery.FirstOrDefault();
                if (result == null)
                    return Task.FromResult<DtoFavorite>(null);

                return Task.FromResult(result);
            }
        }
    }
}
