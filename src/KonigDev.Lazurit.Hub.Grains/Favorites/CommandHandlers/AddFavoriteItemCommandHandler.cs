﻿using KonigDev.Lazurit.Core.Enums;
using KonigDev.Lazurit.Core.Enums.Exeptions.Favorite;
using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Favorite.Command;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace KonigDev.Lazurit.Hub.Grains.Favorites.CommandHandlers
{
    public class AddFavoriteItemCommandHandler : Grain, IHubCommandHandler<AddFavoriteItemCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;
        private readonly ITransactionFactory _transactionFactory;

        public AddFavoriteItemCommandHandler(IDBContextFactory dbContextFactory, ITransactionFactory transactionFactory)
        {
            _dbContextFactory = dbContextFactory;
            _transactionFactory = transactionFactory;
        }
        public async Task Execute(AddFavoriteItemCommand command)
        {
            if (command == null || command.UserId == Guid.Empty)
                return;
            using (var transaction = _transactionFactory.CreateScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var context = _dbContextFactory.CreateLazuritContext())
                {
                    Favorite favorite = context.Set<Favorite>().FirstOrDefault(x => x.Id == command.UserId);
                    if (favorite == null)
                    {
                        favorite = new Favorite()
                        {
                            Id = command.UserId
                        };
                        context.Set<Favorite>().Add(favorite);
                    }
                    switch (command.FavoriteItemType)
                    {
                        case EnumFavoriteItemType.Collection:

                            if (favorite.Collections.Any(p => p.CollectionVariantId == command.AddElementId))
                                throw new NotValidExeption(Core.Enums.Constants.Exeptions.Favorite.FavoriteHasCollection);

                            var collection = context.Set<CollectionVariant>().Include("Products").FirstOrDefault(x => x.Id == command.AddElementId);
                            if (collection == null)
                                return;
                            var favoriteItemCollectionId = Guid.NewGuid();
                            var favoriteItemProductsInCollection = new List<FavoriteItemProductInCollection>();
                            foreach (var product in collection.Products)
                            {
                                var lineItem = new FavoriteItemProductInCollection
                                {
                                    Id = Guid.NewGuid(),
                                    ProductId = product.Id,
                                    Quantity = 1,
                                    FavoriteItemCollectionId = favoriteItemCollectionId,
                                    CreationDate = DateTime.Now
                                };
                                favoriteItemProductsInCollection.Add(lineItem);
                            }
                            favorite.Collections.Add(new FavoriteItemCollection
                            {
                                CollectionVariantId = collection.Id,
                                FavoriteId = favorite.Id,
                                Quantity = 1,
                                Id = Guid.NewGuid(),
                                Products = favoriteItemProductsInCollection,
                                CreationDate = DateTime.Now
                            });
                            break;
                        case EnumFavoriteItemType.Complect:
                            if (favorite.Complects.Any(p => p.CollectionVariantId == command.AddElementId))
                                throw new NotValidExeption(Core.Enums.Constants.Exeptions.Favorite.FavoriteHasComplect);
                            var complect = context.Set<CollectionVariant>().FirstOrDefault(x => x.Id == command.AddElementId);
                            favorite.Complects.Add(new FavoriteItemComplect
                            {
                                CollectionVariantId = complect.Id,
                                FavoriteId = favorite.Id,
                                Quantity = 1,
                                Id = Guid.NewGuid(),
                                CreationDate = DateTime.Now
                            });
                            break;
                        case EnumFavoriteItemType.Product:
                            {
                                if (favorite.Products.Any(p => p.ProductId == command.AddElementId))
                                    throw new NotValidExeption(Core.Enums.Constants.Exeptions.Favorite.FavoriteHasProduct);
                                var product = context.Set<Product>().FirstOrDefault(x => x.Id == command.AddElementId);
                                if (product == null)
                                    return;
                                var lineItem = new FavoriteItemProduct
                                {
                                    Id = Guid.NewGuid(),
                                    FavoriteId = favorite.Id,
                                    ProductId = product.Id,
                                    Quantity = 1,
                                    CreationDate = DateTime.Now
                                };
                                favorite.Products.Add(lineItem);
                            }
                            break;
                    }
                    await context.SaveChangesAsync();
                }
                transaction.Complete();
            }
        }
    }
}
