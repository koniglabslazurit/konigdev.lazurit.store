﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Favorite.Command;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Favorites.CommandHandlers
{
    public class CreateFavoriteCommandHandler : Grain, IHubCommandHandler<CreateFavoriteCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public CreateFavoriteCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task Execute(CreateFavoriteCommand command)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var favorite = context.Set<Favorite>().FirstOrDefault(x => x.Id == command.UserId);
                if (favorite != null)
                    return;
                favorite = new Favorite
                {
                    Id = command.UserId
                };
                context.Set<Favorite>().Add(favorite);
                await context.SaveChangesAsync();
            }
        }
    }
}
