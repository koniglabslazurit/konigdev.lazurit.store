﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Favorite.Command;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Favorites.CommandHandlers
{
    public class RemoveFavoriteCommandHandler : Grain, IHubCommandHandler<RemoveFavoriteCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;
        public RemoveFavoriteCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task Execute(RemoveFavoriteCommand command)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                context.Set<Favorite>().Remove(context.Set<Favorite>().FirstOrDefault(x => x.Id == command.FavoriteId));
                await context.SaveChangesAsync();
            }
        }
    }
}
