﻿using KonigDev.Lazurit.Core.Enums;
using KonigDev.Lazurit.Core.Enums.Exeptions.Favorite;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Favorite.Command;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Favorites.CommandHandlers
{
    public class RemoveFavoriteItemCommandHandler : Grain, IHubCommandHandler<RemoveFavoriteItemCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public RemoveFavoriteItemCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task Execute(RemoveFavoriteItemCommand command)
        {
            if (command == null)
                return;
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var favorite = context.Set<Favorite>().FirstOrDefault(x => x.Id == command.UserId);
                if (favorite == null)
                    return;
                switch (command.FavoriteItemType)
                {
                    case EnumFavoriteItemType.ProductInCollection:
                        if (!favorite.Collections.Any(p => p.CollectionVariantId == command.RemoveCollectionId))
                            throw new NotValidExeption(Core.Enums.Constants.Exeptions.Favorite.FavoriteHasNoCollection);
                        else if (!favorite.Collections.Any(c => c.Products.Any(p => p.ProductId == command.RemoveProductId)))
                            throw new NotValidExeption(Core.Enums.Constants.Exeptions.Favorite.FavoriteHasNoProduct);
                        var products = context.Set<FavoriteItemProductInCollection>().Where(x=>x.FavoriteItemCollection.CollectionVariantId==command.RemoveCollectionId).ToList();
                        context.Set<FavoriteItemProductInCollection>().Remove(products.FirstOrDefault(x=>x.ProductId==command.RemoveProductId));
                        if (products.Count <= 1) {
                            context.Set<FavoriteItemCollection>().Remove(favorite.Collections.FirstOrDefault(x => x.CollectionVariantId == command.RemoveCollectionId));
                        }
                        break;
                    case EnumFavoriteItemType.Collection:
                        if (!favorite.Collections.Any(p => p.CollectionVariantId == command.RemoveCollectionId))
                            throw new NotValidExeption(Core.Enums.Constants.Exeptions.Favorite.FavoriteHasNoCollection);
                        context.Set<FavoriteItemCollection>().Remove(favorite.Collections.FirstOrDefault(x => x.CollectionVariantId == command.RemoveCollectionId));                        
                        break;
                    case EnumFavoriteItemType.Product:
                        if (!favorite.Products.Any(p => p.ProductId == command.RemoveProductId))
                            throw new NotValidExeption(Core.Enums.Constants.Exeptions.Favorite.FavoriteHasNoProduct);
                        context.Set<FavoriteItemProduct>().Remove(favorite.Products.FirstOrDefault(x => x.ProductId == command.RemoveProductId));
                        break;
                    case EnumFavoriteItemType.Complect:
                        if (!favorite.Complects.Any(p => p.CollectionVariantId == command.RemoveCollectionId))
                            throw new NotValidExeption(Core.Enums.Constants.Exeptions.Favorite.FavoriteHasNoComplect);
                        context.Set<FavoriteItemComplect>().Remove(favorite.Complects.FirstOrDefault(x => x.CollectionVariantId == command.RemoveCollectionId));
                        break;
                }
                await context.SaveChangesAsync();
            }
        }
    }
}

