﻿using KonigDev.Lazurit.Core.Enums;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Query;
using KonigDev.Lazurit.Model.Entities;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.CollectionsVariants.QueryHandlers
{
    [Serializable]
    public class GetCollectionVariantValidatorQueryHandler : Grain, IHubQueryHandler<GetCollectionVariantValidatorQuery, DtoCollectionVariantValidation>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetCollectionVariantValidatorQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<DtoCollectionVariantValidation> Execute(GetCollectionVariantValidatorQuery query)
        {
            var result = new DtoCollectionVariantValidation { IsValid = true };
            using (var importContext = _dbContextFactory.CreateLazuritIntegrationContext())
            {
                var variant = importContext.Set<IntegrationCollectionVariant>().FirstOrDefault(p => p.Id == query.Id);
                if (variant == null)
                {
                    result.IsValid = false;
                    result.Errors.Add("Не найден вариант коллекции.");
                    return Task.FromResult(result);
                }

                /* если не опубликованы продукты варианта */
                if (variant.IntegrationProducts.Any(p => !p.OriginalId.HasValue))
                {
                    result.IsValid = false;
                    result.Errors.Add("Не все продкуты в варианте коллекции опубликованы.");
                }
                /* если у варианта не загружены изображения в трех видах */
                using (var baseContext = _dbContextFactory.CreateLazuritContext())
                {
                    var imagesCount = baseContext.Set<FileContent>()
                        .Where(p => p.ObjectId == variant.Id)
                        .Where(p => p.Type.TechName == EnumFileContentType.InInterior.ToString()
                        || p.Type.TechName == EnumFileContentType.SliderMiddle.ToString()
                        || p.Type.TechName == EnumFileContentType.SliderMin.ToString())
                        .Count();
                    if (imagesCount < 3)
                    {
                        result.IsValid = false;
                        result.Errors.Add("У варианта коллекции не загружены изображения.");
                    }
                }
            }
            return Task.FromResult(result);
        }
    }
}
