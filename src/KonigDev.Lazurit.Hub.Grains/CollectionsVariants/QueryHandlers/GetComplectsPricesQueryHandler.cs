﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Query;
using Orleans;
using System.Linq;
using KonigDev.Lazurit.Model.Entities;
using System;

namespace KonigDev.Lazurit.Hub.Grains.CollectionsVariants.QueryHandlers
{
    public class GetComplectsPricesQueryHandler : Grain, IHubQueryHandler<GetComplectsPricesQuery, List<DtoComplectPricesResult>>
    {
        private readonly IDBContextFactory _contextFactory;

        public GetComplectsPricesQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public async Task<List<DtoComplectPricesResult>> Execute(GetComplectsPricesQuery query)
        {
            string sqlQuery = @"SELECT CollectionVariantId, Value, Discount, PriceForAssembly FROM Prices WHERE CollectionVariantId=@CollectionVariantId AND PriceZoneId=@PriceZoneId AND CurrencyCode=@CurrencyCode";

            ConcurrentBag<DtoComplectPricesResult> productPricesBag = new ConcurrentBag<DtoComplectPricesResult>();
            Parallel.ForEach(query.ComplectIds, complectId =>
            {
                using (IDbConnection db = _contextFactory.CreateIdentityConnection())
                {
                    dynamic queryEntity = db.Query<dynamic>(sqlQuery, new { CollectionVariantId = complectId, PriceZoneId = query.PriceZoneId, CurrencyCode = query.CurrencyCode}).Single();
                    var queryResult = new DtoComplectPricesResult
                    {
                        AssemblyPrice = queryEntity.PriceForAssembly,
                        ComplectId = (Guid)queryEntity.CollectionVariantId,
                        Discount = queryEntity.Discount,
                        Price = queryEntity.Value
                    };                    
                    productPricesBag.Add(queryResult);
                }
            });
            return await Task.FromResult(productPricesBag.AsList());
        }
    }
}
