﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Query;
using Orleans;
using System.Linq;
using KonigDev.Lazurit.Model.Entities;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Dto;

namespace KonigDev.Lazurit.Hub.Grains.CollectionsVariants.QueryHandlers
{
    public class GetCollectionsVariantsQueryHandler : Grain, IHubQueryHandler<GetCollectionsVariantsQuery, List<DtoCollectionVariantItem>>
    {
        private readonly IDBContextFactory _contextFactory;

        public GetCollectionsVariantsQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public Task<List<DtoCollectionVariantItem>> Execute(GetCollectionsVariantsQuery query)
        {
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var result = context.Set<CollectionVariant>()
                    .Select(p => new DtoCollectionVariantItem
                    {
                        Id = p.Id,
                        CollectionId = p.CollectionId,
                        Facing = p.Facing.SyncCode1C,
                        FacingColor = p.FacingColor.SyncCode1C,
                        FrameColor = p.FrameColor.SyncCode1C
                    }).ToList();
                return Task.FromResult(result);
            }
        }
    }
}
