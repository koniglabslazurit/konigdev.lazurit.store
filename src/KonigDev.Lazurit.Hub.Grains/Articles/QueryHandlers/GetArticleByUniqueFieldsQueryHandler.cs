﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Article;
using KonigDev.Lazurit.Hub.Model.DTO.Article.Query;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors;
using KonigDev.Lazurit.Hub.Model.DTO.Facings;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors;
using KonigDev.Lazurit.Hub.Model.DTO.Products;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using KonigDev.Lazurit.Hub.Model.DTO.ProductPackages;

namespace KonigDev.Lazurit.Hub.Grains.Articles.QueryHandlers
{
    public class GetArticleByUniqueFieldsQueryHandler : Grain, IHubQueryHandler<GetArticleByUniqueFieldsQuery, DtoArticleByUniqueFieldsQueryResult>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetArticleByUniqueFieldsQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public Task<DtoArticleByUniqueFieldsQueryResult> Execute(GetArticleByUniqueFieldsQuery query)
        {
            if (!query.ArticleId.HasValue && string.IsNullOrEmpty(query.ArticleSyncCode1C) ||
                (query.ArticleId.HasValue && !string.IsNullOrEmpty(query.ArticleSyncCode1C)))
                throw new NotSupportedException();
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                Article resultArtice = null;
                var articles = context.Set<Article>().AsQueryable();

                if (query.ArticleId.HasValue)
                    resultArtice = articles.SingleOrDefault(a => a.Id == query.ArticleId);
                else if (!string.IsNullOrEmpty(query.ArticleSyncCode1C))
                    resultArtice = articles.SingleOrDefault(a => a.SyncCode1C == query.ArticleSyncCode1C);

                if (resultArtice != null)
                {
                    return Task.FromResult(new DtoArticleByUniqueFieldsQueryResult
                    {
                        ArticleId = resultArtice.Id,
                        ArticleSyncCode1C = resultArtice.SyncCode1C,
                        FurnitureTypeName = resultArtice.FurnitureType.Name,
                        Products = resultArtice.Products.Select(p => new DtoProductResult
                        {
                            Id = p.Id,
                            SyncCode1C = p.SyncCode1C,
                            ArticleId = p.ArticleId,
                            Descr = p.Descr,
                            IsAssemblyRequired = p.IsAssemblyRequired,
                            Width = p.Width,
                            Height = p.Height,
                            Length = p.Length,
                            FacingId = p.FacingId,
                            FacingColorId = p.FacingColorId,
                            FrameColorId = p.FrameColorId,
                            Propirties = p.Properties.Select(x => x.Id).ToList(),
                            IsFavorite = query.UserId.HasValue && p.FavoriteItemProducts.Any(x => x.Favorite.UserProfile.Id == query.UserId),
                            Warranty = p.Warranty,
                            PackagesAmount = p.Packing,
                            IsDisassemblyState = p.IsDisassemblyState,
                            PackageWeight = context.Set<ProductPack>().Where(pp => pp.ProductSyncCode1C == p.SyncCode1C).Select(pp => pp.Weight).ToList().Sum(),
                            ProductPackage = context.Set<ProductPack>().Where(pp => pp.ProductSyncCode1C == p.SyncCode1C)
                            .OrderByDescending(pp => pp.Length + pp.Width + pp.Height)
                            .Select(pp => new DtoProductPackageResult
                            {
                                Height = pp.Height,
                                Length = pp.Length,
                                Width = pp.Width
                            }).FirstOrDefault(),
                        }).ToList(),
                        Facings = resultArtice.Products.Select(f => new DtoFacingsResult
                        {
                            Id = f.Facing.Id,
                            Name = f.Facing.Name
                        }).Distinct().ToList(),
                        FacingsColors = resultArtice.Products.Where(p => p.FacingColor != null).Select(f => new DtoFacingColorResult
                        {
                            Id = f.FacingColor.Id,
                            Name = f.FacingColor.Name
                        }).Distinct().ToList(),
                        FramesColors = resultArtice.Products.Select(f => new DtoFrameColorResult
                        {
                            Id = f.FrameColor.Id,
                            Name = f.FrameColor.Name
                        }).Distinct().ToList(),
                        Heights = resultArtice.Products.Select(f => f.Height).Distinct().ToList(),
                        Widths = resultArtice.Products.Select(f => f.Width).Distinct().ToList(),
                        Lengths = resultArtice.Products.Select(f => f.Length).Distinct().ToList()

                    });
                }
                return Task.FromResult<DtoArticleByUniqueFieldsQueryResult>(null);
            }
        }
    }
}
