﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Property.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Property.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Articles.QueryHandlers
{
    public class GetAvailablePropertiesByArticleQueryHandler : Grain, IHubQueryHandler<GetAvailablePropertiesByArticleQuery, DtoProperties>
    {
        private readonly IDBContextFactory _dbContextFactory;
        public GetAvailablePropertiesByArticleQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<DtoProperties> Execute(GetAvailablePropertiesByArticleQuery query)
        {
            var result = new DtoProperties();
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var properties = context.Set<Article>()
                    .Where(p => p.Id == query.ArticleId)
                    .SelectMany(p => p.Products)
                    .SelectMany(p => p.Properties)
                    .Select(p => new DtoPropertyItem
                    {
                        Alias = p.Alias, AliasInPlural = p.AliasInPlural, GroupeId = p.GroupeId, Id = p.Id, IsMultiValue = p.IsMultiValue, IsRequired = p.IsRequired, SortNumber = p.SortNumber, SyncCode1C = p.SyncCode1C, Title = p.Title, TitleInPlural = p.TitleInPlural, Value = p.Value
                    }).Distinct().ToList();

                var groups = properties.Where(p => p.IsMultiValue && p.GroupeId.HasValue).Select(p=>p.GroupeId).Distinct().ToArray();
                foreach (var item in groups)
                {
                    var list = new DtoPropertyList
                    {
                        ListPropirties = properties.Where(p => p.GroupeId == item).Distinct().ToList()
                    };
                    result.Properties.Add(list);
                }
                result.PropertiesSingle = properties.Where(p => !p.IsMultiValue && !p.GroupeId.HasValue).ToList();
            }

            return Task.FromResult(result);
        }
    }
}
