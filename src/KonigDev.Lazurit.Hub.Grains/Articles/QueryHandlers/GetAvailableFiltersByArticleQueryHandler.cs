﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Article.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Filters;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Articles.QueryHandlers
{
    public class GetAvailableFiltersByArticleQueryHandler : Grain, IHubQueryHandler<GetAvailableFiltersByArticleQuery, List<DtoFilterFrameColor>>
    {
        private readonly IDBContextFactory _dbContextFactory;
        public GetAvailableFiltersByArticleQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoFilterFrameColor>> Execute(GetAvailableFiltersByArticleQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var queryDb = context.Set<Product>().AsQueryable();
                if (query.ArticleId != Guid.Empty)
                    queryDb = queryDb.Where(p => p.ArticleId == query.ArticleId);

                var filters = queryDb
                    //basicaly facingColor could be null
                    //.Where(p => p.FacingColorId.HasValue)
                    .Select(p => new { p.FrameColorId, p.FrameColor.Name })
                    .Distinct()
                    .Select(frc => new DtoFilterFrameColor
                    {
                        Id = frc.FrameColorId,
                        Name = frc.Name,
                        AliavableFacings = queryDb
                            .Where(p => p.FrameColorId == frc.FrameColorId)
                            .Select(p => new { p.FacingId, p.Facing.Name })
                            .Distinct()
                            .Select(f => new DtoFilterFacing
                            {
                                Id = f.FacingId,
                                Name = f.Name,
                                AliavableFacingColors = queryDb
                                .Where(p => p.FacingId == f.FacingId)
                                .Where(p => p.FacingColorId != null)
                                .Select(p => new { p.FacingColorId, p.FacingColor })
                                .Distinct()
                                .Select(fc => new DtoFilterFacingColor
                                {
                                    Id = fc.FacingColorId.Value,
                                    Name = fc.FacingColor.Name
                                }).ToList()
                            }).ToList()
                    }).ToList();

                return Task.FromResult(filters);
            }
        }
    }
}