﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Article.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Article.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Articles.QueryHandlers
{
    [Serializable]
    public class GetVendorCodesForInstallmentQueryHandler : Grain, IHubQueryHandler<GetVendorCodesForInstallmentQuery, List<DtoVendorCodeItem>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetVendorCodesForInstallmentQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoVendorCodeItem>> Execute(GetVendorCodesForInstallmentQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {                                
                /* деалем отдельными запросами, иначе один получается тяжелым и со сложным планом выполнения */
                var result = new List<DtoVendorCodeItem>();

                /* получаем по сериям */
                var seriesCodes =context.Set<Series>()
                    .Where(p => query.SeriesIds.Contains(p.Id))
                    .SelectMany(p => p.Collections)
                    .SelectMany(p => p.CollectionVariants)
                    .SelectMany(p => p.Products)
                    .Select(p => new { Id = p.ArticleId, p.Article.SyncCode1C })
                    .Distinct()
                    .Select(p => new DtoVendorCodeItem
                    {
                        Id = p.Id,
                        SyncCode1C = p.SyncCode1C
                    }).ToList();

                /* получаем по комнатам */
                var roomsCodes = context.Set<RoomType>().Where(p => query.Rooms.Contains(p.Id))
                    .SelectMany(p => p.Collections)
                    .SelectMany(p => p.CollectionVariants)
                    .SelectMany(p => p.Products)
                    .Select(p => new { Id = p.ArticleId, p.Article.SyncCode1C })
                    .Distinct()
                    .Select(p => new DtoVendorCodeItem
                    {
                        Id = p.Id,
                        SyncCode1C = p.SyncCode1C
                    }).ToList();

                /* получаем по типу фурнитуры */
                var furnitureCodes = context.Set<Article>()
                    .Where(a => a.Products.Any())
                    .Where(p => query.FurnitureTypesIds.Contains(p.FurnitureTypeId))
                    .Select(p => new { p.Id, p.SyncCode1C })
                    .Distinct()
                    .Select(p => new DtoVendorCodeItem
                    {
                        Id = p.Id,
                        SyncCode1C = p.SyncCode1C
                    }).ToList();
                /* мапинг в один результирующий массив */

                result = seriesCodes.Where(p => roomsCodes.Any(r => r.SyncCode1C == p.SyncCode1C) && furnitureCodes.Any(f => f.SyncCode1C == p.SyncCode1C))
                    .Select(p => new { p.Id, p.SyncCode1C })
                    .Distinct()
                    .OrderBy(p => p.SyncCode1C)
                     .Select(p => new DtoVendorCodeItem
                     {
                         Id = p.Id,
                         SyncCode1C = p.SyncCode1C
                     })
                    .ToList();
                return Task.FromResult(result);
            }
        }
    }
}
