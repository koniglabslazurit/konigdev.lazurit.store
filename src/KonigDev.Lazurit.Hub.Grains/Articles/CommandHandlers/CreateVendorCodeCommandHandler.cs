﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using Orleans;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Article.Commands;
using KonigDev.Lazurit.Core.Transactions;
using System.Data;
using System.Transactions;
using Dapper;
using System.Linq;

namespace KonigDev.Lazurit.Hub.Grains.Articles.CommandHandlers
{
    public class CreateVendorCodeCommandHandler : Grain, IHubCommandHandler<CreateVendorCodeCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;
        public CreateVendorCodeCommandHandler(IDBContextFactory contextFactory, ITransactionFactory transactionFactory)
        {
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(CreateVendorCodeCommand command)
        {
            var query = "";
            var count = 0;
                        
            var totalCount = command.VendorCodes.Count();

            foreach (var item in command.VendorCodes)
            {
                ++count;
                query += $"INSERT INTO Articles (Id,Name,SyncCode1C,FurnitureTypeId,Range) VALUES('{item.Id}', N'{item.Name}', N'{item.SyncCode1C}', N'{item.FurnitureTypeId}', N'{item.Range}');";
                if (count % 30 == 0 || totalCount - count < 30)
                {
                    using (IDbConnection connection = _contextFactory.CreateDbConnection("LazuritBaseContext"))
                    {
                        connection.Open();

                        TransactionOptions to = new TransactionOptions();
                        to.IsolationLevel = System.Transactions.IsolationLevel.Snapshot;
                        using (var transaction = _transactionFactory.CreateScope(TransactionScopeOption.Required, to))
                        {
                            try
                            {
                                connection.Execute(query, transaction);
                                transaction.Complete();
                            }
                            catch (System.Exception ex)
                            {
                                throw ex;
                            }
                        }
                    }
                    query = "";
                }
            }
        }
    }
}