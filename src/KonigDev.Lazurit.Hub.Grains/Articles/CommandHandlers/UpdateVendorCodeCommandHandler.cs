﻿using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using Orleans;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Transactions;
using System;
using KonigDev.Lazurit.Hub.Model.DTO.Article.Commands;
using System.Linq;

namespace KonigDev.Lazurit.Hub.Grains.Articles.CommandHandlers
{
    public class UpdateVendorCodeCommandHandler : Grain, IHubCommandHandler<UpdateVendorCodeCommand>
    {

        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;

        public UpdateVendorCodeCommandHandler(IDBContextFactory contextFactory, ITransactionFactory transactionFactory)
        {
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(UpdateVendorCodeCommand command)
        {
            var query = "UPDATE Articles SET Range=@RangeVendorCode, FurnitureTypeId=@FurnitureTypeId  WHERE Id = @Id;";

            using (IDbConnection connection = _contextFactory.CreateDbConnection("LazuritBaseContext"))
            {
                connection.Open();

                TransactionOptions to = new TransactionOptions();
                to.IsolationLevel = System.Transactions.IsolationLevel.Snapshot;
                using (var transaction = _transactionFactory.CreateScope(TransactionScopeOption.Required, to))
                {
                    try
                    {
                        connection.Execute(query, command.VendorCodes.Select(vendorCode => new { vendorCode.Id, vendorCode.RangeVendorCode, vendorCode.FurnitureTypeId }));
                        transaction.Complete();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }
    }
}
