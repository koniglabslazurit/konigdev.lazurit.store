﻿using KonigDev.Lazurit.Core.Enums.Constants;
using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Commands;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Cities.CommandHandlers
{
    public class CreateCityCommandHandler: Grain, IHubCommandHandler<CreateCityCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        public CreateCityCommandHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public async Task Execute(CreateCityCommand command)
        {
            if (command == null)
                throw new NotValidCommandException(Exceptions.Common.ModelEmpty);
            if (command.Id == Guid.Empty)
                throw new NotValidCommandException(Exceptions.Common.ModelIdEmpty);
            if (string.IsNullOrEmpty(command.Title))
                throw new NotValidCommandException(Exceptions.Common.ModelNameEmpty);
            if (command.RegionId == Guid.Empty)
                throw new NotValidCommandException(Exceptions.City.RegionIsEmpty);
            if (command.PriceZoneId== Guid.Empty)
                throw new NotValidCommandException(Exceptions.City.PriceZoneIsEmpty);
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var city = new City
                {
                    Id = command.Id,
                    Title = command.Title,
                    CodeKladr = command.CodeKladr,
                    SyncCode1C = command.SyncCode1C,
                    Latitude = command.Latitude,
                    Longitude = command.Longitude,
                    PriceZoneId = command.PriceZoneId,
                    RegionId = command.RegionId,
                    DeliveryPrice = command.DeliveryPrice
                };
                context.Set<City>().Add(city);
                await context.SaveChangesAsync();
            }
        }
    }
}
