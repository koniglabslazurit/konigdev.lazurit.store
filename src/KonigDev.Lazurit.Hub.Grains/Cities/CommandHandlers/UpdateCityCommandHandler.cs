﻿using KonigDev.Lazurit.Core.Enums.Constants;
using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Commands;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Cities.CommandHandlers
{
    public class UpdateCityCommandHandler : Grain, IHubCommandHandler<UpdateCityCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        public UpdateCityCommandHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public async Task Execute(UpdateCityCommand command)
        {
            if (command == null)
                throw new NotValidCommandException(Exceptions.Common.ModelEmpty);
            if (command.Id == Guid.Empty)
                throw new NotValidCommandException(Exceptions.Common.ModelIdEmpty);
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var city = context.Set<City>().SingleOrDefault(x => x.Id == command.Id);
                if (city == null)
                    throw new NotValidCommandException(Exceptions.City.CityNotFound);
                city.Title = command.Title;
                city.CodeKladr = command.CodeKladr;
                city.SyncCode1C = command.SyncCode1C;
                city.Latitude = command.Latitude;
                city.Longitude = command.Longitude;
                city.PriceZoneId = command.PriceZoneId;
                city.RegionId = command.RegionId;
                city.DeliveryPrice = command.DeliveryPrice;
                
                await context.SaveChangesAsync();
            }
        }
    }
}
