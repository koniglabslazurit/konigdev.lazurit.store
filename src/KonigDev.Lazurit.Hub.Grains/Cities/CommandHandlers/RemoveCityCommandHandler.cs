﻿using KonigDev.Lazurit.Core.Enums.Constants;
using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Commands;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Cities.CommandHandlers
{
    public class RemoveCityCommandHandler : Grain, IHubCommandHandler<RemoveCityCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        public RemoveCityCommandHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public async Task Execute(RemoveCityCommand command)
        {
            if (command==null)
                throw new NotValidCommandException(Exceptions.Common.ModelEmpty);
            if (command.Id == Guid.Empty)
                throw new NotValidCommandException(Exceptions.Common.ModelIdEmpty);
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var city = context.Set<City>()
                            .SingleOrDefault(x => x.Id == command.Id);
                if (city == null)
                    throw new NotValidCommandException(Exceptions.City.CityNotFound);
                context.Set<City>().Remove(city);
                
                await context.SaveChangesAsync();
            }
        }
    }
}
