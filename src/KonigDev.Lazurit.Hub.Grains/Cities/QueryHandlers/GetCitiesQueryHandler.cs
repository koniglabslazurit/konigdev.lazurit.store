﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Queries;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Dto;

namespace KonigDev.Lazurit.Hub.Grains.Cities.QueryHandlers
{
    /// <summary>
    /// Получение списка всех городов
    /// </summary>
    public class GetCitiesQueryHandler : Grain, IHubQueryHandler<GetCitiesQuery, List<DtoCityItem>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetCitiesQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoCityItem>> Execute(GetCitiesQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var result = context.Set<City>()
                    .Select(p => new DtoCityItem
                    {
                        Id = p.Id,
                        SyncCode1C = p.SyncCode1C
                    }).ToList();

                return Task.FromResult(result);
            }
        }
    }
}
