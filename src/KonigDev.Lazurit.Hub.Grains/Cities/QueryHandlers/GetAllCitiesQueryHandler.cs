﻿using KonigDev.Lazurit.Core.Enums.Constants;
using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Queries;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Cities.QueryHandlers
{
    public class GetAllCitiesQueryHandler: Grain, IHubQueryHandler<GetAllCitiesQuery, List<DtoCity>>
    {
        private readonly IDBContextFactory _contextFactory;
        public GetAllCitiesQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }
        public Task<List<DtoCity>> Execute(GetAllCitiesQuery query)
        {            
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var citiesDto = context.Set<City>().Select(x => new DtoCity
                {
                    Id = x.Id,
                    Title = x.Title,
                    Latitude = x.Latitude,
                    Longitude = x.Longitude,
                    PriceZoneId = x.PriceZoneId,
                    RegionId = x.RegionId,
                    CodeKladr = x.CodeKladr,
                    SyncCode1C = x.SyncCode1C,
                    IsNearest = false,
                    DeliveryPrice = x.DeliveryPrice
                }).OrderBy(x=>x.Title).ToList();
                if (citiesDto.Count == 0)
                    throw new NotValidCommandException(Exceptions.City.CityNotFound);
                
                return Task.FromResult(citiesDto);
            }
        }
    }
}
