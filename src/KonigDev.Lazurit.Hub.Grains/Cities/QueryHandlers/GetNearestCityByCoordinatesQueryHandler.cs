﻿using KonigDev.Lazurit.Core.Enums.Constants;
using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Queries;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Model.Entities;

namespace KonigDev.Lazurit.Hub.Grains.Cities.QueryHandlers
{
    public class GetNearestCityByCoordinatesQueryHandler: Grain, IHubQueryHandler<GetCityByCoordinates, DtoCityCoordinates>
    {
        private readonly IDBContextFactory _contextFactory;
        public GetNearestCityByCoordinatesQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }
        public Task<DtoCityCoordinates> Execute(GetCityByCoordinates query)
        {
            if (query == null)
                throw new NotValidCommandException(Exceptions.Common.ModelEmpty);
            using (var context = _contextFactory.CreateLazuritContext())
            {
                //if coordinates are zero we must return default city
                if (query.Latitude == 0 && query.Longitude == 0)
                {
                    var defaultCity = context.Set<City>().Where(x => x.Title == "Москва").Select(x => new DtoCityCoordinates
                    {
                        Id = x.Id,
                        Title = x.Title,
                        Latitude = x.Latitude,
                        Longitude = x.Longitude,
                        PriceZoneId = x.PriceZoneId,
                        RegionId = x.RegionId,
                        DeliveryPrice = x.DeliveryPrice
                    }).FirstOrDefault();
                    return Task.FromResult(defaultCity);
                }
                var citiesDto = context.Set<City>().Select(x => new DtoCityCoordinates
                {
                    Id = x.Id,
                    Title = x.Title,
                    Latitude = x.Latitude,
                    Longitude = x.Longitude,
                    PriceZoneId = x.PriceZoneId,
                    RegionId = x.RegionId,
                    DeliveryPrice = x.DeliveryPrice
                }).ToList();
                if (citiesDto.Count==0)
                    throw new NotValidCommandException(Exceptions.City.CityNotFound);
                foreach (var cityDto in citiesDto)
                {
                    var sum1 = Math.Pow((query.Latitude - cityDto.Latitude), 2);
                    var sum2 = Math.Pow((query.Longitude - cityDto.Longitude), 2);
                    var sqrt = Math.Sqrt(sum1 + sum2);
                    cityDto.Distance = Math.Round(sqrt, 4);
                }
                citiesDto = citiesDto.OrderBy(x => x.Distance).ToList();
                var nearestCity = citiesDto[0];
                return Task.FromResult(nearestCity);
            }
        }
    }
}
