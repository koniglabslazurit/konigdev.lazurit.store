﻿using KonigDev.Lazurit.Core.Enums.Constants;
using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cities.Queries;
using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Queries;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Cities.QueryHandlers
{
    public class GetCityQueryHandler : Grain, IHubQueryHandler<GetCityQuery, DtoCity>
    {
        private readonly IDBContextFactory _contextFactory;
        public GetCityQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }
        public Task<DtoCity> Execute(GetCityQuery query)
        {
            if (query == null)
                throw new NotValidCommandException(Exceptions.Common.ModelEmpty);
            if (query.Id == Guid.Empty)
                throw new NotValidCommandException(Exceptions.Common.ModelIdEmpty);
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var city = context.Set<City>()
                    .Select(x => new DtoCity
                    {
                        Id = x.Id,
                        Title  = x.Title,
                        CodeKladr = x.CodeKladr,
                        SyncCode1C = x.SyncCode1C,
                        Latitude = x.Latitude,
                        Longitude = x.Longitude,
                        PriceZoneId =x.PriceZoneId,
                        RegionId = x.RegionId,
                        DeliveryPrice = x.DeliveryPrice
                    }).SingleOrDefault(x => x.Id == query.Id);
                return Task.FromResult(city);
            }
        }
    }
}
