﻿using System;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Command;
using KonigDev.Lazurit.Model.Entities;
using Orleans;

namespace KonigDev.Lazurit.Hub.Grains.FilesContent.CommandHandlers
{
    public class CreateUpdateFileContentCommandHandler : Grain, IHubCommandHandler<CreateUpdateFileContentCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public CreateUpdateFileContentCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task Execute(CreateUpdateFileContentCommand command)
        {
            if (command.ObjectId == Guid.Empty)
                throw new NullReferenceException("Не указан ид объекта");
            if (command.Type == Core.Enums.EnumFileContentType.None)
                throw new NullReferenceException("Не указан тип файла");
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var fileContent = context.Set<FileContent>().FirstOrDefault(p => p.Type.TechName == command.Type.ToString() && p.ObjectId == command.ObjectId);
                if (fileContent != null)
                    context.Set<FileContent>().Remove(fileContent);

                fileContent = new FileContent
                {
                    Id = command.Id,
                    Size = command.Size,
                    DateCreating = DateTime.UtcNow,
                    FileExtension = command.FileExtension.Replace(".", ""),
                    FileName = command.FileName,
                    ObjectId = command.ObjectId,
                    TypeId = context.Set<FileContentType>()
                        .Where(p => p.TechName == command.Type.ToString())
                        .Select(p => p.Id).FirstOrDefault()
                };

                context.Set<FileContent>().Add(fileContent);
                await context.SaveChangesAsync();
            }
        }
    }
}
