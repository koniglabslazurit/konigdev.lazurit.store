﻿using System;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using Orleans;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Model.Entities;
using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Dto.Command;

namespace KonigDev.Lazurit.Hub.Grains.FilesContent.CommandHandlers
{
    public class CreateFileContentCommandHandler: Grain, IHubCommandHandler<CreateFileContentCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public CreateFileContentCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task Execute(CreateFileContentCommand command)
        {
            if (command.ObjectId == Guid.Empty)
                throw new NullReferenceException("Не указан ид объекта");
            if(command.Type == Core.Enums.EnumFileContentType.None)
                throw new NullReferenceException("Не указан тип файла");
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var fileContent = new FileContent
                {
                    Id = command.Id,
                    Size = command.Size,
                    DateCreating = DateTime.UtcNow,
                    FileExtension = command.FileExtension.Replace(".", ""),
                    FileName = command.FileName,
                    ObjectId = command.ObjectId,
                    TypeId = context.Set<FileContentType>()
                            .Where(p => p.TechName == command.Type.ToString())
                            .Select(p => p.Id).FirstOrDefault()
                };

                context.Set<FileContent>().Add(fileContent);
                await context.SaveChangesAsync();
            }
        }
    }
}
