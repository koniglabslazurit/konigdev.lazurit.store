﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Collections;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors;
using KonigDev.Lazurit.Hub.Model.DTO.Facings;
using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Queries;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Products;

namespace KonigDev.Lazurit.Hub.Grains.FilesContent.QueryHandlers
{
    /// <summary>
    /// Получение всех списков для возможности загрузки изображений в дальнейшем, с привязкой к объекту
    /// </summary>
    public class GetFilesContentObjectsQueryHandler : Grain, IHubQueryHandler<GetFilesContentObjectsQuery, DtoFilesContentObjectsResponse>
    {
        private readonly IDBContextFactory _dbContextFactory;
        public GetFilesContentObjectsQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        /// <summary>
        /// Получение всех списков для возможности загрузки изображений в дальнейшем, с привязкой к объекту
        /// </summary>
        public async Task<DtoFilesContentObjectsResponse> Execute(GetFilesContentObjectsQuery query)
        {
            var result = new DtoFilesContentObjectsResponse();            
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                result.Collections = context.Set<Collection>()
                    .Select(p => new DtoCollectionResult
                {
                    Id = p.Id,
                    Name = p.RoomType.Name + " "+ p.Series.Name
                }).ToList();

                result.CollectionVariants = context.Set<CollectionVariant>()
                    .Select(p => new DtoCollectionVariant
                    {
                        Id = p.Id
                    }).ToList();

                result.FacingColors = context.Set<FacingColor>()
                    .Select(p => new DtoFacingColorResult
                    {
                        Id = p.Id,
                        Name = p.Name
                    }).ToList();

                result.Facings = context.Set<Facing>()
                    .Select(p => new DtoFacingsResult
                    {
                        Id = p.Id,
                        Name = p.Name
                    }).ToList();

                result.FrameColors = context.Set<FrameColor>()
                    .Select(p => new DtoFrameColorResult
                    {
                        Id = p.Id,
                        Name = p.Name
                    }).ToList();
                result.Products = context.Set<Product>()
                    .Select(p => new DtoProductResult
                    {
                        Id = p.Id,
                        SyncCode1C = p.SyncCode1C
                    }).ToList();
            };
            return await Task.FromResult(result);
        }
    }
}
