﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Queries;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using KonigDev.Lazurit.Model.Entities;
using Orleans;

namespace KonigDev.Lazurit.Hub.Grains.FilesContent.QueryHandlers
{
    public class GetFilesContentByRequestQueryHandler : Grain, IHubQueryHandler<GetFilesContentByRequestQuery, List<DtoFilesContent>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetFilesContentByRequestQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        
        public async Task<List<DtoFilesContent>> Execute(GetFilesContentByRequestQuery requestQuery)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {                
                var query = context.Set<FileContent>()
                    .Where(p=>p.ObjectId == requestQuery.ObjectId);
                                
                if(requestQuery.Type != Core.Enums.EnumFileContentType.None)
                    query = query.Where(p => p.Type.TechName == requestQuery.Type.ToString());

                var result = query.Select(p => new DtoFilesContent
                {
                    Id = p.Id,
                    Type = p.Type.TechName,
                    Extensions = p.FileExtension,
                    FileName = p.FileName
                }).ToList();
                return await Task.FromResult(result);
            }               
        }
        
    }
}
