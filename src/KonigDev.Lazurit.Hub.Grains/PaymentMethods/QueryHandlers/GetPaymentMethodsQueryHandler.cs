﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.PaymentMethods;
using KonigDev.Lazurit.Hub.Model.DTO.PaymentMethods.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.PaymentMethods.QueryHandlers
{
   public class GetPaymentMethodsQueryHandler : Grain, IHubQueryHandler<GetPaymentMethodsQuery, List<DtoPaymentMethod>>
    {
        private readonly IDBContextFactory _dbContextFactory;
        public GetPaymentMethodsQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoPaymentMethod>> Execute(GetPaymentMethodsQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var paymentMethods = context.Set<PaymentMethod>().Select(p => new DtoPaymentMethod
                {
                    Id = p.Id,
                    Name = p.Name,
                    SortNumber = p.SortNumber,
                    TechName = p.TechName
                }).OrderBy(p=>p.SortNumber).ToList();
                return Task.FromResult(paymentMethods);
            }
        }
    }
}
