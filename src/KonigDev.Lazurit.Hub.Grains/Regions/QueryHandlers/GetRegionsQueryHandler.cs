﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Queries;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Dto;

namespace KonigDev.Lazurit.Hub.Grains.Regions.QueryHandlers
{
    /// <summary>
    /// Получение списка всех регионов
    /// </summary>
    public class GetRegionsQueryHandler : Grain, IHubQueryHandler<GetRegionsQuery, List<DtoRegionItem>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetRegionsQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public Task<List<DtoRegionItem>> Execute(GetRegionsQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var result = context.Set<Region>()
                    .Select(p => new DtoRegionItem
                    {
                        Id = p.Id,
                        SyncCode1C = p.SyncCode1C
                    }).ToList();

                return Task.FromResult(result);
            }
        }
    }
}
