﻿using KonigDev.Lazurit.Core.Enums.Constants;
using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Queries;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Regions.QueryHandlers
{
    public class GetRegionQueryHandler: Grain, IHubQueryHandler<GetRegionQuery, DtoRegion>
    {
        private readonly IDBContextFactory _contextFactory;
        public GetRegionQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public Task<DtoRegion> Execute(GetRegionQuery query)
        {
            if (query == null)
                throw new NotValidCommandException(Exceptions.Common.ModelEmpty);
            if (query.Id == Guid.Empty)
                throw new NotValidCommandException(Exceptions.Common.ModelIdEmpty);
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var region = context.Set<Region>()
                    .Select(x=>new DtoRegion {
                        Id= x.Id,
                        Title = x.Title,
                        LazuritPasswordHash = x.LazuritPasswordHash,
                        SyncCode1C = x.SyncCode1C
                    }).SingleOrDefault(x => x.Id == query.Id);
                return Task.FromResult(region);    
            }
        }
    }
}
