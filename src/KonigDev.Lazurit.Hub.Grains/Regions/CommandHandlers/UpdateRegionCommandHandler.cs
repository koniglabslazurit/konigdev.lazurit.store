﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Commands;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Core.Enums.Constants;

namespace KonigDev.Lazurit.Hub.Grains.Regions.CommandHandlers
{
    public class UpdateRegionCommandHandler : Grain, IHubCommandHandler<UpdateRegionCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        public UpdateRegionCommandHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public async Task Execute(UpdateRegionCommand command)
        {
            if (command == null)
                throw new NotValidCommandException(Exceptions.Common.ModelEmpty);
            if (command.Id == Guid.Empty)
                throw new NotValidCommandException(Exceptions.Common.ModelIdEmpty);
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var region = context.Set<Region>().SingleOrDefault(x => x.Id == command.Id);
                if (region == null)
                    throw new NotValidCommandException(Exceptions.Region.RegionNotFound);

                region.Title = command.Title;
                region.LazuritPasswordHash = command.LazuritPasswordHash;
                region.SyncCode1C = command.SyncCode1C;

                await context.SaveChangesAsync();
            }
        }
    }
}
