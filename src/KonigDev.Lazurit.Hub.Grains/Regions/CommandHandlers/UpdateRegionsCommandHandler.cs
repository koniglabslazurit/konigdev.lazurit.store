﻿using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Commands;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace KonigDev.Lazurit.Hub.Grains.Regions.CommandHandlers
{
    /// <summary>
    /// Обновление данных по регионам, городам, салонам
    /// </summary>
    public class UpdateRegionsCommandHandler : Grain, IHubCommandHandler<UpdateRegionsCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;

        public UpdateRegionsCommandHandler(IDBContextFactory contextFactory, ITransactionFactory transactionFactory)
        {
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }
        public async Task Execute(UpdateRegionsCommand command)
        {
            #region save regions
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var regions = context.Set<Region>().ToList();
                foreach (var regionItem in command.Regions)
                {
                    var region = regions.FirstOrDefault(p => p.SyncCode1C == regionItem.SyncCode1C);
                    if (region == null)
                    {
                        region = new Region { Id = Guid.NewGuid(), SyncCode1C = regionItem.SyncCode1C };
                        context.Set<Region>().Add(region);
                    }
                    region.Title = regionItem.Title;
                    regionItem.Id = region.Id;
                }
                try
                {
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            #endregion

            #region Save cities
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var cities = context.Set<City>().ToList();
                foreach (var regionItem in command.Regions)
                    foreach (var cityItem in regionItem.Cities)
                    {
                        var city = cities.FirstOrDefault(p => p.SyncCode1C == cityItem.SyncCode1C);
                        if (city == null)
                        {
                            city = new City { Id = Guid.NewGuid(), SyncCode1C = cityItem.SyncCode1C };
                            context.Set<City>().Add(city);
                        }
                        city.RegionId = regionItem.Id;
                        city.Title = cityItem.Title;
                        city.PriceZoneId = cityItem.PriceZoneId;
                        cityItem.Id = city.Id;
                        city.Longitude = cityItem.Longitude;
                        city.Latitude = cityItem.Latitude;
                    }
                try
                {
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            #endregion

            #region Save showrooms                        
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var showrooms = context.Set<Showroom>().ToList();
                foreach (var regionItem in command.Regions)
                    foreach (var cityItem in regionItem.Cities)
                    {
                        foreach (var showroomItem in cityItem.Showrooms)
                        {
                            var showroom = showrooms.FirstOrDefault(p => p.SyncCode1C == showroomItem.SyncCode1C);
                            if (showroom == null)
                            {
                                showroom = new Showroom { Id = Guid.NewGuid(), SyncCode1C = showroomItem.SyncCode1C };
                                context.Set<Showroom>().Add(showroom);
                            }
                            showroom.Title = showroomItem.Title;
                            showroom.RegionId = regionItem.Id;
                            showroom.CityId = cityItem.Id;
                        }
                        try
                        {
                            context.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
            }
            #endregion

            // Удаление городов, отсутсвующих в выгрузке
            using (var context = _contextFactory.CreateLazuritContext())
            {
                try
                {
                    var cities = context.Set<City>().ToList();
                    var importCities = command.Regions.SelectMany(p => p.Cities).Select(p => p.Id).Distinct().ToList();
                    var forDeleting = cities.Where(c => !importCities.Contains(c.Id)).ToList();
                    context.Set<Showroom>().RemoveRange(forDeleting.SelectMany(p=>p.Showrooms).ToList());
                    context.Set<City>().RemoveRange(forDeleting);
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

            /* todo посмотреть в сторону опитимации через даппер */

            //UPDATE Regions SET Id = @Id,Title = @Title,SyncCode1C = @SyncCode1C
            //WHERE SyncCode1C = @SyncCode1C
            //    IF @@ROWCOUNT = 0
            //	 INSERT INTO Regions(Id, Title, SyncCode1C) VALUES(@Id, @Title, @SyncCode1C)

            // -- cities
            // UPDATE Cities SET Id = @Id,Title = @Title,SyncCode1C = @SyncCode1C, Longitude = @Longitude,Latitude = @Latitude,DeliveryPrice = @DeliveryPrice,RegionId = @RegionId,PriceZoneId = @PriceZoneId
            //WHERE SyncCode1C = @SyncCode1C
            //     IF @@ROWCOUNT = 0
            //		INSERT INTO Cities(Id, Title, SyncCode1C, Longitude, Latitude, DeliveryPrice, RegionId, PriceZoneId)

            //        VALUES(@Id, @Title, @SyncCode1C, @Longitude, @Latitude, @DeliveryPrice, @RegionId, @PriceZoneId)

            //-- Showrooms
            //UPDATE Showrooms SET Id = @Id,Title = @Title, SyncCode1C = @SyncCode1C,RegionId = @RegionId,CityId = @CityId
            //WHERE SyncCode1C = @SyncCode1C
            //         IF @@ROWCOUNT = 0		 
            //			INSERT INTO Showrooms(Id, Title, SyncCode1C, RegionId, CityId)

            //            VALUES(@Id, @Title, @SyncCode1C, @RegionId, @CityId)


            //            UPDATE IntegrationsPrices SET Id = @Id, PriceZoneId = @PriceZoneId, ProductSyncCode1C = @@ProductSyncCode1C, Value = @@Value
            //WHERE PriceZoneId = @@PriceZoneId and ProductSyncCode1C = @@ProductSyncCode1C;
            //            IF @@ROWCOUNT = @0
            //			    INSERT INTO IntegrationsPrices(Id, PriceZoneId, ProductSyncCode1C, Value)

            //                        VALUES(@Id, @PriceZoneId, @ProductSyncCode1C, @Value)


        }
    }
