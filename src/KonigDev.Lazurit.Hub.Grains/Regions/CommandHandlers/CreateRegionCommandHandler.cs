﻿using KonigDev.Lazurit.Core.Enums.Constants;
using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Regions.Commands;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Regions.CommandHandlers
{
    public class CreateRegionCommandHandler : Grain, IHubCommandHandler<CreateRegionCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        public CreateRegionCommandHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public async Task Execute(CreateRegionCommand command)
        {
            if (command == null)
                throw new NotValidCommandException(Exceptions.Common.ModelEmpty);
            if (command.Id == Guid.Empty)
                throw new NotValidCommandException(Exceptions.Common.ModelIdEmpty);
            if (string.IsNullOrEmpty(command.Title))
                throw new NotValidCommandException(Exceptions.Common.ModelNameEmpty);
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var region = new Region
                {
                    Id = command.Id,
                    Title = command.Title,
                    LazuritPasswordHash = command.LazuritPasswordHash,
                    SyncCode1C = command.SyncCode1C
                };
                context.Set<Region>().Add(region);
                await context.SaveChangesAsync();            
            }
        }
    }

}
