﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Query;
using Orleans;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Installment.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Installment.Query;
using System;
using KonigDev.Lazurit.Model.Entities;

namespace KonigDev.Lazurit.Hub.Grains.InstallmentGrains.QueryHandlers
{
    public class GetInstallmentQueryHandler : Grain, IHubQueryHandler<GetInstallmentQuery, DtoInstallmentTableResponse>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetInstallmentQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<DtoInstallmentTableResponse> Execute(GetInstallmentQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var dbQuery = context.Set<Installment>().AsQueryable();

                if (query.IsFuture)
                {
                    var today = DateTime.UtcNow.Date;
                    dbQuery = dbQuery.Where(p => p.DateEnd >= today);
                }

                var result = dbQuery.Select(p => new DtoInstallmentItem
                {
                    Id = p.Id,
                    DateEnd = p.DateEnd,
                    DateStart = p.DateStart,
                    Discount = p.Discount,
                    Installment10Month = p.Installment10Month,
                    Installment12Month = p.Installment12Month,
                    Installment18Month = p.Installment18Month,
                    Installment24Month = p.Installment24Month,
                    Installment36Month = p.Installment36Month,
                    Installment48Month = p.Installment48Month,
                    Installment6Month = p.Installment6Month,
                    Name = p.Name,
                    FurnitureTypes = p.InstallmentItems.Select(i => i.Product.Article.FurnitureTypeId).ToList(),
                    Rooms = p.InstallmentItems.Select(i => i.Product.Collection.RoomTypeId).ToList(),
                    Series = p.InstallmentItems.Select(i => i.Product.Collection.SeriesId).ToList(),
                    VendorCodes = p.InstallmentItems.Select(i => i.Product.ArticleId).ToList()
                }).ToList();

                /* если понадобится пагинация, внедрить пагинацию в запрос, количество брать до отбработки пагинации */
                var totalCount = result.Count;

                return Task.FromResult(new DtoInstallmentTableResponse { Items = result, TotalCount = totalCount });
            }
        }
    }
}
