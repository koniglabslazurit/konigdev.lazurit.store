﻿using KonigDev.Lazurit.Core.Enums.Constants;
using KonigDev.Lazurit.Core.Enums.Exeptions.Product;
using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Installment.Commands;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace KonigDev.Lazurit.Hub.Grains.InstallmentGrains.CommandHandlers
{
    /// <summary>
    /// Создание записи скидок
    /// </summary>
    public class CreateInstallmentCommandHandler : Grain, IHubCommandHandler<CreateInstallmentCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;
        private readonly ITransactionFactory _transactionFactory;

        public CreateInstallmentCommandHandler(IDBContextFactory dbContextFactory, ITransactionFactory transactionFactory)
        {
            _dbContextFactory = dbContextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(CreateInstallmentCommand command)
        {
            if (command.VendorCodes.Count == 0 && command.FurnitureTypes.Count == 0 && command.Series.Count == 0 && command.Rooms.Count == 0)
                throw new ProductInstallmentModelNotValid(Exceptions.Product.ProductInstallmentModelNotValid);

            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var installment = new Installment
                {
                    Id = Guid.NewGuid(),
                    DateStart = command.DateStart.Date,
                    DateEnd = command.DateEnd.Date,
                    Discount = command.Discount,
                    Installment10Month = command.Installment10Month,
                    Installment12Month = command.Installment12Month,
                    Installment18Month = command.Installment18Month,
                    Installment24Month = command.Installment24Month,
                    Installment36Month = command.Installment36Month,
                    Installment48Month = command.Installment48Month,
                    Installment6Month = command.Installment6Month,
                    Name = command.Name
                };

                context.Set<Installment>().Add(installment);

                /* отбиаем продукты, попадающие под запрос */
                var productsQuery = context.Set<Product>().AsQueryable();
                if (command.Series.Count > 0)
                    productsQuery = productsQuery.Where(p => command.Series.Contains(p.Collection.SeriesId));
                if (command.Rooms.Count > 0)
                    productsQuery = productsQuery.Where(p => command.Rooms.Contains(p.Collection.RoomTypeId));
                if (command.FurnitureTypes.Count > 0)
                    productsQuery = productsQuery.Where(p => command.FurnitureTypes.Contains(p.Article.FurnitureTypeId));
                if (command.VendorCodes.Count > 0)
                    productsQuery = productsQuery.Where(p => command.VendorCodes.Contains(p.ArticleId));

                var products = productsQuery.Select(p => p.Id).Distinct().ToList();

                foreach (var item in products)
                {
                    installment.InstallmentItems.Add(new InstallmentItem
                    {
                        ProductId = item,
                        Id = Guid.NewGuid(),
                        InstallmentId = installment.Id,
                        DateStart = command.DateStart.Date,
                        DateEnd = command.DateEnd.Date,
                        Discount = command.Discount,
                        Installment10Month = command.Installment10Month,
                        Installment12Month = command.Installment12Month,
                        Installment18Month = command.Installment18Month,
                        Installment24Month = command.Installment24Month,
                        Installment36Month = command.Installment36Month,
                        Installment48Month = command.Installment48Month,
                        Installment6Month = command.Installment6Month,
                    });
                }
                using (var transaction = _transactionFactory.CreateScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    try
                    {
                        //await context.SaveChangesAsync();
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    transaction.Complete();
                }
            }
        }
    }
}