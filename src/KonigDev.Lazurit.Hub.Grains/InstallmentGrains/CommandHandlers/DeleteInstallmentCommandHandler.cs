﻿using KonigDev.Lazurit.Core.Enums.Constants;
using KonigDev.Lazurit.Core.Enums.Exeptions.Installment;
using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Installment.Commands;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace KonigDev.Lazurit.Hub.Grains.InstallmentGrains.CommandHandlers
{
    public class DeleteInstallmentCommandHandler : Grain, IHubCommandHandler<DeleteInstallmentCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;
        private readonly ITransactionFactory _transactionFactory;

        public DeleteInstallmentCommandHandler(IDBContextFactory dbContextFactory, ITransactionFactory transactionFactory)
        {
            _dbContextFactory = dbContextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(DeleteInstallmentCommand command)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var item = context.Set<Installment>().FirstOrDefault(i => i.Id == command.Id);
                if (item == null)
                    throw new InstallmentNotFoundException(Exceptions.Installment.InstallmentNotFound);
                context.Set<Installment>().Remove(item);

                try
                {
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
