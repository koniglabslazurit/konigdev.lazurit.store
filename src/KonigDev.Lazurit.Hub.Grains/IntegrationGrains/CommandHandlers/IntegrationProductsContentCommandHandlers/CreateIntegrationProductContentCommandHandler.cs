﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Command;
using Orleans;
using System;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Model.Entities;
using KonigDev.Lazurit.Store.Integration.Context.Entities;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.CommandHandlers.IntegrationProductsContentCommandHandlers
{
    public class CreateIntegrationProductContentCommandHandler : Grain, IHubCommandHandler<CreateIntegrationProductContentCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public CreateIntegrationProductContentCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task Execute(CreateIntegrationProductContentCommand command)
        {
            if (command.Id == Guid.Empty)
                throw new NullReferenceException("Не указан ид объекта");
            if (command.ProductId == Guid.Empty)
                throw new NullReferenceException("Не указан ид продукта");
            if (command.IntegrationCollectionVariantId == Guid.Empty)
                throw new NullReferenceException("Не указан ид варианта коллекции");
            using (var context = _dbContextFactory.CreateLazuritIntegrationContext())
            {
                var productContent = context.Set<IntegrationCollectionVariantContentProductArea>().FirstOrDefault(p => p.ProductId == command.ProductId && p.IntegrationCollectionVariantId == command.IntegrationCollectionVariantId);
                if (productContent != null)
                    context.Set<IntegrationCollectionVariantContentProductArea>().Remove(productContent);

                productContent = new IntegrationCollectionVariantContentProductArea
                {
                    Id = command.Id,
                    ProductId = command.ProductId,
                    IntegrationCollectionVariantId = command.IntegrationCollectionVariantId,
                    IsEnabled = true
                };

                context.Set<IntegrationCollectionVariantContentProductArea>().Add(productContent);
                await context.SaveChangesAsync();
            }
        }
    }
}
