﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Command;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using KonigDev.Lazurit.Core.Transactions;
using System.Transactions;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.CommandHandlers.IntegrationProductsContentCommandHandlers
{
    public class UpdateIntegrationProductContentCommandHandler : Grain, IHubCommandHandler<UpdateIntegrationProductContentCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;
        private readonly ITransactionFactory _transactionFactory;

        public UpdateIntegrationProductContentCommandHandler(IDBContextFactory dbContextFactory, ITransactionFactory transactionFactory)
        {
            _dbContextFactory = dbContextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(UpdateIntegrationProductContentCommand command)
        {
            if (command.Id == Guid.Empty)
                throw new NullReferenceException("Не указан ид объекта");
            using (var transaction = _transactionFactory.CreateScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var context = _dbContextFactory.CreateLazuritIntegrationContext())
                {
                    var productContent = context.Set<IntegrationCollectionVariantContentProductArea>().FirstOrDefault(p => p.Id == command.Id);
                    if (productContent != null)
                    {
                        productContent.Left = command.Left;
                        productContent.Top = command.Top;
                        productContent.Width = command.Width;
                        productContent.Height = command.Height;

                        context.SaveChanges();
                    }
                }
                using (var baseContext = _dbContextFactory.CreateLazuritContext())
                {
                    var baseContent = baseContext.Set<CollectionVariantContentProductArea>().FirstOrDefault(p => p.Id == command.Id);
                    if (baseContent != null)
                    {
                        baseContent.Left = command.Left;
                        baseContent.Top = command.Top;
                        baseContent.Width = command.Width;
                        baseContent.Height = command.Height;

                        baseContext.SaveChanges();
                    }
                }
                transaction.Complete();
            }
        }
    }
}

