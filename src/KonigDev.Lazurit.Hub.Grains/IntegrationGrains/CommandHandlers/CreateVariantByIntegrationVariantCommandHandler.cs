﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using KonigDev.Lazurit.Core.Enums.Constants;
using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Core.Enums.Exeptions.Integrations;
using KonigDev.Lazurit.Core.Extensions;
using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Model.Entities;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using Orleans;
using KonigDev.Lazurit.Hub.Model.Mappings.Integrations;
using System.Data.Entity;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.CommandHandlers
{
    [Serializable]
    public class CreateVariantByIntegrationVariantCommandHandler : Grain, IHubCommandHandler<CreateVariantByIntegrationVariantCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;

        public CreateVariantByIntegrationVariantCommandHandler(IDBContextFactory contextFactory,
            ITransactionFactory transactionFactory)
        {
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(CreateVariantByIntegrationVariantCommand command)
        {
            if (command == null || command.Id == Guid.Empty)
                throw new NotValidCommandException(Exceptions.Common.ModelEmpty);

            using (var transaction = _transactionFactory.CreateScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var context = _contextFactory.CreateLazuritIntegrationContext())
                {
                    using (var baseContext = _contextFactory.CreateLazuritContext())
                    {
                        var integrationVariant = context.Set<IntegrationCollectionVariant>()
                        .SingleOrDefault(x => x.Id == command.Id);

                        if (integrationVariant == null)
                            throw new IntegrationVariantNotFound(Exceptions.Integrations.IntegrationVariantNotFound);

                        if (integrationVariant.OriginalId == null)
                        {
                            /* вытягиваем первый попавшийся продукт для варианта, чтобы записать маркетинговый вес подумать, как сделать по дургому? хранить в ипорт таблицах? */
                            var marketerRange = string.Empty;
                            var product = integrationVariant.IntegrationProducts.Where(p => !string.IsNullOrEmpty(p.MarketerRange)).FirstOrDefault();
                            if (product != null)
                                marketerRange = product.MarketerRange;
                            IntegrationMarketingRange range = new IntegrationMarketingRange(marketerRange);

                            /* room */
                            if (integrationVariant.IntegrationCollection.IntegrationRoom.OriginalId == null)
                            {
                                var room = new RoomType
                                {
                                    Id = Guid.NewGuid(),
                                    Alias = integrationVariant.IntegrationCollection.IntegrationRoom.SyncCode1C.Transliterate(),
                                    SyncCode1C = integrationVariant.IntegrationCollection.IntegrationRoom.SyncCode1C,
                                    Name = integrationVariant.IntegrationCollection.IntegrationRoom.SyncCode1C,
                                    Range = range.RangeRoom
                                };
                                baseContext.Set<RoomType>().Add(room);
                                integrationVariant.IntegrationCollection.IntegrationRoom.OriginalId = room.Id;
                            }

                            /* series */
                            if (integrationVariant.IntegrationCollection.IntegrationSeries.OriginalId == null)
                            {
                                var series = new Series
                                {
                                    Id = Guid.NewGuid(),
                                    Range = range.RangeSeries,
                                    Alias = integrationVariant.IntegrationCollection.IntegrationSeries.SyncCode1C.Transliterate(),
                                    SyncCode1C = integrationVariant.IntegrationCollection.IntegrationSeries.SyncCode1C,
                                    Name = integrationVariant.IntegrationCollection.IntegrationSeries.SyncCode1C
                                };
                                baseContext.Set<Series>().Add(series);
                                integrationVariant.IntegrationCollection.IntegrationSeries.OriginalId = series.Id;
                            }

                            /* collection */
                            if (integrationVariant.IntegrationCollection.OriginalId == null)
                            {
                                var collection = new Collection
                                {
                                    Id = Guid.NewGuid(),
                                    RoomTypeId = integrationVariant.IntegrationCollection.IntegrationRoom.OriginalId.Value,
                                    SeriesId = integrationVariant.IntegrationCollection.IntegrationSeries.OriginalId.Value,
                                    Range = range.RangeCollection
                                };
                                baseContext.Set<Collection>().Add(collection);
                                integrationVariant.IntegrationCollection.OriginalId = collection.Id;
                            }

                            //    /* создание варианта коллекции */
                            var facingId = baseContext.Set<Facing>().Where(x => x.SyncCode1C == integrationVariant.Facing)
                                    .Select(x => x.Id).SingleOrDefault();
                            var facingColorId = baseContext.Set<FacingColor>().Where(x => x.SyncCode1C == integrationVariant.FacingColor)
                                    .Select(x => x.Id).SingleOrDefault();
                            var frameColorId = baseContext.Set<FrameColor>().Where(x => x.SyncCode1C == integrationVariant.FrameColor)
                                    .Select(x => x.Id).SingleOrDefault();

                            var variant = new CollectionVariant
                            {
                                Id = integrationVariant.Id,
                                CollectionId = integrationVariant.IntegrationCollection.OriginalId.Value,
                                FacingId = facingId,
                                FacingColorId = facingColorId,
                                FrameColorId = frameColorId,
                                IsComplect = integrationVariant.IsComplekt,
                                SyncCode1C = integrationVariant.SyncCode1C
                            };

                            baseContext.Set<CollectionVariant>().Add(variant);
                            integrationVariant.OriginalId = integrationVariant.Id;

                            /* связывание опубликованных продуктов с вариантом коллекции */
                            var productsIds = integrationVariant.IntegrationProducts.Select(p => p.ProductId).ToList();
                            var products = baseContext.Set<Product>().Where(p => productsIds.Contains(p.Id)).ToList();
                            foreach (var item in products)
                            {
                                variant.Products.Add(item);
                            }

                            SaveImages(context, baseContext, variant.Id);
                        }

                        try
                        {
                            baseContext.SaveChanges();
                            context.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }

                        /* закоменчена старая реализация публикации варианта коллекции. Удалить после проверки */
                        /* если варианта не существует */
                        #region
                        //if (integrationVariant.OriginalId == null || originalVariant == null)
                        //{
                        //    /* проверка существования комнаты */
                        //    var roomCode = context.Set<IntegrationRoom>().Where(x => x.Id == integrationVariant.IntegrationCollection.IntegrationRoomId).Select(x => x.SyncCode1C).SingleOrDefault();
                        //    var room = baseContext.Set<RoomType>()
                        //        .SingleOrDefault(x => x.SyncCode1C == roomCode);

                        //    if (room == null)
                        //    {
                        //        room = new RoomType
                        //        {
                        //            Id = Guid.NewGuid(),
                        //            Alias = integrationVariant.IntegrationCollection.IntegrationRoom.SyncCode1C.Transliterate(),
                        //            SyncCode1C = integrationVariant.IntegrationCollection.IntegrationRoom.SyncCode1C,
                        //            Name = integrationVariant.IntegrationCollection.IntegrationRoom.SyncCode1C
                        //        };
                        //        baseContext.Set<RoomType>().Add(room);
                        //        var integrationRoom = context.Set<IntegrationRoom>()
                        //            .SingleOrDefault(x => x.Id == integrationVariant.IntegrationCollection.IntegrationRoom.Id);
                        //        if (integrationRoom != null)
                        //            integrationRoom.OriginalId = room.Id;
                        //    }

                        //    /* проверка существования серии */
                        //    var seriesCode = context.Set<IntegrationSeries>().Where(x => x.Id == integrationVariant.IntegrationCollection.IntegrationSeriesId).Select(x => x.SyncCode1C).SingleOrDefault();
                        //    var series = baseContext.Set<Series>()
                        //        .SingleOrDefault(x => x.SyncCode1C == seriesCode);
                        //    if (series == null)
                        //    {
                        //        series = new Series
                        //        {
                        //            Id = Guid.NewGuid(),
                        //            Alias = integrationVariant.IntegrationCollection.IntegrationSeries.SyncCode1C.Transliterate(),
                        //            SyncCode1C = integrationVariant.IntegrationCollection.IntegrationSeries.SyncCode1C,
                        //            Name = integrationVariant.IntegrationCollection.IntegrationSeries.SyncCode1C
                        //        };
                        //        baseContext.Set<Series>().Add(series);
                        //        var integrationSeries = context.Set<IntegrationSeries>()
                        //            .SingleOrDefault(x => x.Id == integrationVariant.IntegrationCollection.IntegrationSeries.Id);
                        //        if (integrationSeries != null)
                        //            integrationSeries.OriginalId = series.Id;
                        //    }

                        //    /*проверка существования коллекции*/
                        //    Collection collection = null;
                        //    if (integrationVariant.IntegrationCollection.OriginalId != null)
                        //    {
                        //        collection = baseContext.Set<Collection>()
                        //            .SingleOrDefault(x => x.Id == integrationVariant.IntegrationCollection.OriginalId);
                        //    }
                        //    if (integrationVariant.IntegrationCollection.OriginalId == null || collection == null)
                        //    {
                        //        collection = new Collection
                        //        {
                        //            Id = Guid.NewGuid(),
                        //            RoomTypeId = room.Id,
                        //            SeriesId = series.Id
                        //        };
                        //        baseContext.Set<Collection>().Add(collection);
                        //        var integrationCollection = context.Set<IntegrationCollection>()
                        //            .SingleOrDefault(x => x.Id == integrationVariant.IntegrationCollection.Id);
                        //        if (integrationCollection != null)
                        //            integrationCollection.OriginalId = collection.Id;
                        //    }

                        //    /* создание варианта коллекции */
                        //    var facingId = baseContext.Set<Facing>().Where(x => x.SyncCode1C == integrationVariant.Facing)
                        //            .Select(x => x.Id).SingleOrDefault();
                        //    var facingColorId = baseContext.Set<FacingColor>().Where(x => x.SyncCode1C == integrationVariant.FacingColor)
                        //            .Select(x => x.Id).SingleOrDefault();
                        //    var frameColorId = baseContext.Set<FrameColor>().Where(x => x.SyncCode1C == integrationVariant.FrameColor)
                        //            .Select(x => x.Id).SingleOrDefault();

                        //    baseContext.Set<CollectionVariant>()
                        //        .Add(new CollectionVariant
                        //        {
                        //            Id = integrationVariant.Id,
                        //            CollectionId = collection.Id,
                        //            FacingId = facingId,
                        //            FacingColorId = facingColorId,
                        //            FrameColorId = frameColorId,
                        //            IsComplect = integrationVariant.IsComplekt
                        //        });
                        //    integrationVariant.OriginalId = integrationVariant.Id;
                        //}
                        #endregion
                    }
                }
                transaction.Complete();
            }
        }

        private void SaveImages(DbContext integrationContext, DbContext baseContext, Guid variantId)
        {
            var integrationImages = integrationContext.Set<IntegrationCollectionVariantContentProductArea>().Where(x => x.IntegrationCollectionVariantId == variantId).ToList();
            foreach (var integrationImage in integrationImages)
            {
                var image = baseContext.Set<CollectionVariantContentProductArea>().Where(x => x.Id == integrationImage.Id).SingleOrDefault();
                if (image == null)
                {
                    /*create*/
                    baseContext.Set<CollectionVariantContentProductArea>()
                        .Add(new CollectionVariantContentProductArea
                        {
                            Id = integrationImage.Id,
                            CollectionVariantId = integrationImage.IntegrationCollectionVariantId,
                            ProductId = integrationImage.ProductId,
                            Height = integrationImage.Height,
                            Width = integrationImage.Width,
                            Left = integrationImage.Left,
                            Top = integrationImage.Top,
                            IsEnabled = integrationImage.IsEnabled
                        });
                }
                else
                {
                    /*update*/
                    image.Height = integrationImage.Height;
                    image.Width = integrationImage.Width;
                    image.Top = integrationImage.Top;
                    image.Left = integrationImage.Left;
                }
            }
        }
    }
}