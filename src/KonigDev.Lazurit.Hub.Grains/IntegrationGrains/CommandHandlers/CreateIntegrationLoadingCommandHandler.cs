﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using Orleans;
using System.Threading.Tasks;
using System;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Core.Transactions;
using System.Data;
using System.Transactions;
using Dapper;
using System.Linq;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.CommandHandlers
{    
    public class CreateIntegrationLoadingCommandHandler : Grain, IHubCommandHandler<CreateIntegrationLoadingCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;
        public CreateIntegrationLoadingCommandHandler(IDBContextFactory contextFactory, ITransactionFactory transactionFactory)
        {
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }
        public async Task Execute(CreateIntegrationLoadingCommand command)
        {
            using (IDbConnection connection = _contextFactory.CreateDbConnection("LazuritIntegrationContext"))
            {
                connection.Open();
                TransactionOptions to = new TransactionOptions();
                to.IsolationLevel = System.Transactions.IsolationLevel.Snapshot;
                using (var transaction = _transactionFactory.CreateScope(TransactionScopeOption.Required, to))
                {
                    try
                    {
                        connection.Execute($"INSERT INTO IntegrationsLoadings(Id,DateLoad,[FileName],Descr,[Type])VALUES('{command.Id}','{command.DateLoad.ToString("yyyy-MM-dd HH:mm:ss.fff")}',N'{command.FileName}',N'{command.Descr}',N'{command.Type}');");                        
                        connection.Execute("INSERT INTO IntegrationsLoadingsItems(Id,IntegrationLoadingId,Value,[Type])VALUES(@Id,@IntegrationLoadingId,@Value,@Type)", command.Items.Select(p => new { p.Id, p.IntegrationLoadingId, p.Value, p.Type }));
                        transaction.Complete();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }
    }
}
