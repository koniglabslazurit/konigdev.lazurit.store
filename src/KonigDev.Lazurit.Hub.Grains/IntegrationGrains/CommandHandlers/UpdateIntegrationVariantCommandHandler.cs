﻿using KonigDev.Lazurit.Core.Enums.Constants;
using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Core.Enums.Exeptions.Integrations;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.CommandHandlers
{
    [Serializable]
    public class UpdateIntegrationVariantCommandHandler : Grain, IHubCommandHandler<UpdateIntegrationVariantCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        public UpdateIntegrationVariantCommandHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public async Task Execute(UpdateIntegrationVariantCommand command)
        {
            if (command == null || command.CollectionVariantId == Guid.Empty)
            {
                throw new NotValidCommandException(Exceptions.Common.ModelEmpty);
            }
            using (var context = _contextFactory.CreateLazuritIntegrationContext())
            {
                var variant = context.Set<IntegrationCollectionVariant>()
                    .SingleOrDefault(x => x.Id == command.CollectionVariantId);
                if (variant == null)
                {
                    throw new IntegrationVariantNotFound(Exceptions.Integrations.IntegrationVariantNotFound);
                }
                /*все варианты поставить не основными*/
                var otherVariants = context.Set<IntegrationCollectionVariant>()
                    .Where(x => x.IntegrationCollectionId == variant.IntegrationCollectionId).ToList();
                foreach (var otherVariant in otherVariants)
                {
                    otherVariant.IsDefault = false;
                }
                /*нужный поставить основным*/
                variant.IsDefault = true;
                try
                {
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
