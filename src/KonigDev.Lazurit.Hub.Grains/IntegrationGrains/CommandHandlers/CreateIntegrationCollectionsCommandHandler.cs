﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using Orleans;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Core.Transactions;
using System.Transactions;
using System;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using System.Data;
using Dapper;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.CommandHandlers
{
    public class CreateIntegrationCollectionsCommandHandler : Grain, IHubCommandHandler<CreateIntegrationCollectionsCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;
        public CreateIntegrationCollectionsCommandHandler(IDBContextFactory contextFactory, ITransactionFactory transactionFactory)
        {
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(CreateIntegrationCollectionsCommand command)
        {

            var query = "";
            foreach (var item in command.Collections)
            {
                query += $"INSERT INTO IntegrationsCollections(Id,IntegrationSeriesId,IntegrationRoomId)VALUES('{item.Id}','{item.SeriesId}','{item.RoomId}');";
            }

            using (IDbConnection connection = _contextFactory.CreateDbConnection("LazuritIntegrationContext"))
            {
                connection.Open();
                TransactionOptions to = new TransactionOptions();
                to.IsolationLevel = System.Transactions.IsolationLevel.Snapshot;
                using (var transaction = _transactionFactory.CreateScope(TransactionScopeOption.Required, to))
                {
                    try
                    {
                        connection.Execute(query, transaction);
                        transaction.Complete();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }            
        }
    }
}
