﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using Orleans;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Core.Transactions;
using System.Transactions;
using System;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using KonigDev.Lazurit.Hub.Model.Mappings.Integrations;
using KonigDev.Lazurit.Core.Enums.Exeptions.Product;
using KonigDev.Lazurit.Core.Enums.Enums;
using KonigDev.Lazurit.Model.Entities;
using KonigDev.Lazurit.Core.Extensions;
using KonigDev.Lazurit.Core.Enums;
using System.Data.Entity;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.CommandHandlers
{
    /// <summary>
    /// Обновление данных по продукту. Тсполняется в рамках одного грейна.
    /// </summary>    
    public class UpdateIntegrationProductsCommandHandler : Grain, IHubCommandHandler<UpdateIntegrationProductsCommand>
    {
        /* Обновление данных по продукту. 
         * Реализуется в рамках одного грейна для целостности данных
         * Если где-то падает, не обновляется ни одна таблица
         * Используются два контекста и транзакция
         * todo подмать как разнести по разным методам, но приэтом не нарушить контексты, транзацкии итп.
         * todo подумать, о транзакции, очень большое количество запросов, будет лочить БД на долго. Попрбовать вынетси запросы на чтение отдельно. Сделать премап? */

        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;

        public UpdateIntegrationProductsCommandHandler(IDBContextFactory contextFactory, ITransactionFactory transactionFactory)
        {
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(UpdateIntegrationProductsCommand command)
        {
            using (var transaction = _transactionFactory.CreateScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                IntegrationProduct integrationProduct = null;
                using (var integrationContext = _contextFactory.CreateLazuritIntegrationContext())
                {
                    integrationProduct = integrationContext.Set<IntegrationProduct>()
                        .FirstOrDefault(p => p.SyncCode1C == command.SyncCode1C);

                    if (integrationProduct == null)
                        throw new ProductNotFound(Core.Enums.Constants.Exceptions.Product.ProductNotFound);

                    integrationProduct = command.MapCommandToEntity(integrationProduct);
                    /* обновление статуса */
                    integrationProduct.IntegrationStatusId = integrationContext.Set<IntegrationStatus>()
                        .Where(p => p.TechName == command.IntegrationStatus).Select(p => p.Id).FirstOrDefault();

                    /* styles */
                    integrationContext.Set<IntegrationProductToStyle>().RemoveRange(integrationProduct.IntegrationProductStyles);
                    foreach (var item in command.Styles)
                        integrationProduct.IntegrationProductStyles.Add(new IntegrationProductToStyle { Id = Guid.NewGuid(), Style = item, IntegrationProductId = command.SyncCode1C });

                    /* TargetAudiences */
                    integrationContext.Set<IntegrationProductToTargetAudience>().RemoveRange(integrationProduct.IntegrationProductTargetAudiencies);
                    foreach (var item in command.TargetTypes)
                        integrationProduct.IntegrationProductTargetAudiencies.Add(new IntegrationProductToTargetAudience { Id = Guid.NewGuid(), TargetAudience = item, IntegrationProductId = command.SyncCode1C });
                    integrationProduct.IntegrationStatusId = integrationContext.Set<IntegrationStatus>().Where(p => p.TechName == command.IntegrationStatus).Select(p => p.Id).FirstOrDefault();

                    #region Обновление данных в опубликованных товарах
                    /* обновление основных таблиц продукта
                     * выполняется если необходимо опубликовать продукт, или обновить данные у опубликованного продукта */
                    if (command.IntegrationStatus == EnumIntegrationStatus.Published.ToString())
                    {
                        using (var baseContext = _contextFactory.CreateLazuritContext())
                        {
                            Product product = baseContext.Set<Product>().FirstOrDefault(p => p.SyncCode1C == command.SyncCode1C);

                            /* вытягиваем фильтры, так как они должны совпадать для варианта коллекции и для продукта */
                            Guid? facingColorId = null;
                            if (!string.IsNullOrEmpty(integrationProduct.FacingColor))
                                facingColorId = baseContext.Set<FacingColor>().Where(p => p.SyncCode1C == integrationProduct.FacingColor).Select(p => p.Id).FirstOrDefault();
                            var facingId = baseContext.Set<Facing>().Where(p => p.SyncCode1C == integrationProduct.Facing).Select(p => p.Id).FirstOrDefault();
                            var frameColorId = baseContext.Set<FrameColor>().Where(p => p.SyncCode1C == integrationProduct.FrameColor).Select(p => p.Id).FirstOrDefault();

                            if (product == null) /* новый продукт, создаем его */
                            {
                                /* TODO вынести логику парсинга? */
                                Guid productPartMarkerId = Guid.Empty;
                                var productPartMarker = integrationProduct.ProductPartMarker.ToLower().Replace(" ", "");
                                if (productPartMarker == "нетнет")
                                    productPartMarkerId = baseContext.Set<ProductPartMarker>().Where(p => p.TechName == EnumProductPartMarker.Missing.ToString()).Select(p => p.Id).FirstOrDefault();
                                else if (productPartMarker == "данет")
                                    productPartMarkerId = baseContext.Set<ProductPartMarker>().Where(p => p.TechName == EnumProductPartMarker.ExistedNonRequired.ToString()).Select(p => p.Id).FirstOrDefault();
                                else
                                    productPartMarkerId = baseContext.Set<ProductPartMarker>().Where(p => p.TechName == EnumProductPartMarker.ExistedRequired.ToString()).Select(p => p.Id).FirstOrDefault();

                                product = new Product
                                {
                                    Id = integrationProduct.ProductId, /* в продукт записываем ид из integrationProduct, чтобы была возможность получения картинок по ид, загруженных для импортируемых товаров */
                                    FacingColorId = facingColorId,
                                    FacingId = facingId,
                                    FrameColorId = frameColorId,
                                    /* поиск артикула идет по маркетиновому вендор коду */
                                    ArticleId = baseContext.Set<Article>().Where(p => p.SyncCode1C == integrationProduct.MarketerVendorCode).Select(p => p.Id).FirstOrDefault(),
                                    ProductPartMarkerId = productPartMarkerId,
                                    IsDisassemblyState = integrationProduct.IsDisassemblyState                                                                   
                                };
                                integrationProduct.OriginalId = integrationProduct.ProductId;
                                baseContext.Set<Product>().Add(product);
                            }
                            product = integrationProduct.MapToProductEntity(product);

                            /* todo посмотреть по логике и БП, нужно ли этот признак дефолтного продукта */
                            if (product.IsDefault)
                            {
                                if (baseContext.Set<Product>()
                                    .Where(p => p.ArticleId == product.ArticleId)
                                    .Where(p => p.Id != product.Id).Any(p => p.IsDefault))
                                {
                                    var defaultProduts = baseContext.Set<Product>()
                                        .Where(p => p.ArticleId == product.ArticleId)
                                        .Where(p => p.Id != product.Id)
                                        .Where(p => p.IsDefault == true);
                                    foreach (var defaultProdut in defaultProduts)
                                    {
                                        defaultProdut.IsDefault = false;
                                    }
                                }
                            }

                            /* styles */
                            for (int i = 0; i < product.Styles.Count; i++)
                            {
                                if (!command.Styles.Contains(product.Styles.ElementAt(i).SyncCode1C))
                                    product.Styles.Remove(product.Styles.ElementAt(i));
                            }

                            foreach (var style in command.Styles)
                            {
                                if (!product.Styles.Select(p => p.SyncCode1C).ToList().Contains(style))
                                    product.Styles.Add(baseContext.Set<Style>().FirstOrDefault(p => p.SyncCode1C == style));
                            }

                            /* TargetAudiences */
                            for (int i = 0; i < product.TargetAudiences.Count; i++)
                            {
                                if (!command.TargetTypes.Contains(product.TargetAudiences.ElementAt(i).SyncCode1C))
                                    product.TargetAudiences.Remove(product.TargetAudiences.ElementAt(i));
                            }

                            foreach (var targetAudience in command.TargetTypes)
                            {
                                if (!product.TargetAudiences.Select(p => p.SyncCode1C).ToList().Contains(targetAudience))
                                    product.TargetAudiences.Add(baseContext.Set<TargetAudience>().FirstOrDefault(p => p.SyncCode1C == targetAudience));
                            }

                            /* properties */
                            SaveProperties(baseContext, product, command);
                            /* сохранение коллекции */
                            SaveCollection(integrationContext, baseContext, integrationProduct, product, facingColorId, facingId, frameColorId);
                            /* сохранение цен */
                            SavePrices(integrationContext, baseContext, integrationProduct);
                            /*обновить изображения для анимированной картинки*/
                            /* закоменчено: логика публикации продукта и варианта коллекции. */
                            /* ------------------------------------------------------------- */
                            //SaveImages(integrationContext, baseContext, product);

                            // для проверки валидации моделей
                            //foreach (var i in baseContext.GetValidationErrors())
                            //{
                            //    var t = i.ValidationErrors;
                            //}
                            baseContext.SaveChanges();
                        }
                    }
                    #endregion

                    /* сохранение изменений в контексте интегрейшен */
                    integrationContext.SaveChanges();
                }
                transaction.Complete();
            }
        }

        /// <summary>
        /// Сохранение цен для продукта
        /// </summary>
        /// <param name="integrationContext"></param>
        /// <param name="baseContext"></param>
        /// <param name="product"></param>
        private void SavePrices(DbContext integrationContext, DbContext baseContext, IntegrationProduct product)
        {
            var integrationPrices = integrationContext.Set<IntegrationPrice>().Where(p => p.ProductSyncCode1C == product.SyncCode1C).ToList();
            var productPrices = baseContext.Set<Price>().Where(p => p.Product.SyncCode1C == product.SyncCode1C).ToList();
            foreach (var integrationPrice in integrationPrices)
            { 
                var price = productPrices.FirstOrDefault(p => p.PriceZoneId == integrationPrice.PriceZoneId);
                if (price == null)
                {
                    price = new Price
                    {
                        Id = Guid.NewGuid(),
                        CurrencyCode = "RUB", //todo про умолчанию валюта в рублях
                        ProductId = product.ProductId,
                        PriceZoneId = integrationPrice.PriceZoneId
                    };
                    baseContext.Set<Price>().Add(price);
                }
                price.Value = integrationPrice.Value;
            }
        }


        private void SaveCollection(DbContext integrationContext, DbContext baseContext, IntegrationProduct integrationProduct, Product product, Guid? facingColorId, Guid facingId, Guid frameColorId)
        {
            IntegrationMarketingRange range = new IntegrationMarketingRange(product.Range);

            /* при публикации продукта, не публикуем варант коллекции, только коллекцию, серию, комнату, если отсутсвуют */

            /* room */
            if (integrationProduct.IntegrationCollection.IntegrationRoom.OriginalId == null)
            {
                var room = new RoomType
                {
                    Id = Guid.NewGuid(),
                    Alias = integrationProduct.IntegrationCollection.IntegrationRoom.SyncCode1C.Transliterate(),
                    SyncCode1C = integrationProduct.IntegrationCollection.IntegrationRoom.SyncCode1C,
                    Name = integrationProduct.IntegrationCollection.IntegrationRoom.SyncCode1C,
                    Range = range.RangeRoom
                };
                baseContext.Set<RoomType>().Add(room);
                integrationProduct.IntegrationCollection.IntegrationRoom.OriginalId = room.Id;
            }

            /* series */
            if (integrationProduct.IntegrationCollection.IntegrationSeries.OriginalId == null)
            {
                var series = new Series
                {
                    Id = Guid.NewGuid(),
                    Range = range.RangeSeries,
                    Alias = integrationProduct.IntegrationCollection.IntegrationSeries.SyncCode1C.Transliterate(),
                    SyncCode1C = integrationProduct.IntegrationCollection.IntegrationSeries.SyncCode1C,
                    Name = integrationProduct.IntegrationCollection.IntegrationSeries.SyncCode1C
                };
                baseContext.Set<Series>().Add(series);
                integrationProduct.IntegrationCollection.IntegrationSeries.OriginalId = series.Id;
            }

            /* collection */
            if (integrationProduct.IntegrationCollection.OriginalId == null)
            {
                var collection = new Collection
                {
                    Id = Guid.NewGuid(),
                    RoomTypeId = integrationProduct.IntegrationCollection.IntegrationRoom.OriginalId.Value,
                    SeriesId = integrationProduct.IntegrationCollection.IntegrationSeries.OriginalId.Value,
                    Range = range.RangeCollection
                };
                baseContext.Set<Collection>().Add(collection);
                integrationProduct.IntegrationCollection.OriginalId = collection.Id;
            }

            /* задаем ид коллекции для продукта */
            product.CollectionId = integrationProduct.IntegrationCollection.OriginalId.Value;

            /* закоменчено: логика публикации продукта и варианта коллекции. */
            /* ------------------------------------------------------------- */
            ///* записывает новые варианты коллекции */
            //foreach (var cv in integrationProduct.IntegrationCollectionsVariants)
            //{
            //    /* room */
            //    if (cv.IntegrationCollection.IntegrationRoom.OriginalId == null)
            //    {
            //        var room = new RoomType
            //        {
            //            Id = Guid.NewGuid(),
            //            Alias = cv.IntegrationCollection.IntegrationRoom.SyncCode1C.Transliterate(),
            //            SyncCode1C = cv.IntegrationCollection.IntegrationRoom.SyncCode1C,
            //            Name = cv.IntegrationCollection.IntegrationRoom.SyncCode1C,
            //            Range = range.RangeRoom
            //        };
            //        baseContext.Set<RoomType>().Add(room);
            //        cv.IntegrationCollection.IntegrationRoom.OriginalId = room.Id;
            //    }

            //    /* series */
            //    if (cv.IntegrationCollection.IntegrationSeries.OriginalId == null)
            //    {
            //        var series = new Series
            //        {
            //            Id = Guid.NewGuid(),
            //            Range = range.RangeSeries,
            //            Alias = cv.IntegrationCollection.IntegrationSeries.SyncCode1C.Transliterate(),
            //            SyncCode1C = cv.IntegrationCollection.IntegrationSeries.SyncCode1C,
            //            Name = cv.IntegrationCollection.IntegrationSeries.SyncCode1C
            //        };
            //        baseContext.Set<Series>().Add(series);
            //        cv.IntegrationCollection.IntegrationSeries.OriginalId = series.Id;
            //    }

            //    /* collection */
            //    if (cv.IntegrationCollection.OriginalId == null)
            //    {
            //        var collection = new Collection
            //        {
            //            Id = Guid.NewGuid(),
            //            RoomTypeId = cv.IntegrationCollection.IntegrationRoom.OriginalId.Value,
            //            SeriesId = cv.IntegrationCollection.IntegrationSeries.OriginalId.Value,
            //            Range = range.RangeCollection
            //        };
            //        baseContext.Set<Collection>().Add(collection);
            //        cv.IntegrationCollection.OriginalId = collection.Id;
            //    }

            //    /* collections variants */
            //    if (cv.OriginalId == null)
            //    {
            //        var variant = new CollectionVariant
            //        {
            //            Id = cv.Id,
            //            CollectionId = cv.IntegrationCollection.OriginalId.Value,
            //            FacingColorId = !facingColorId.HasValue ? baseContext.Set<FacingColor>().Where(p => p.SyncCode1C == cv.FacingColor).Select(p => p.Id).FirstOrDefault() : facingColorId.Value,
            //            FacingId = facingId,
            //            FrameColorId = frameColorId,
            //            IsComplect = cv.IsComplekt,
            //            IsDefault = cv.IsDefault
            //        };
            //        cv.OriginalId = variant.Id;
            //        product.CollectionsVariants.Add(variant);
            //    }
            //    else
            //    {
            //        var variant = baseContext.Set<CollectionVariant>().FirstOrDefault(p => p.Id == cv.OriginalId);
            //        if (variant != null && !product.CollectionsVariants.Any(p => p.Id == cv.Id))
            //            product.CollectionsVariants.Add(variant);
            //    }
            //}
        }

        private void SaveImages(DbContext integrationContext, DbContext baseContext, Product product)
        {
            var integrationImages = integrationContext.Set<IntegrationCollectionVariantContentProductArea>().Where(x => x.ProductId == product.Id).ToList();
            foreach (var integrationImage in integrationImages)
            {
                var image = baseContext.Set<CollectionVariantContentProductArea>().Where(x => x.Id == integrationImage.Id).SingleOrDefault();
                if (image == null)
                {
                    /*create*/
                    baseContext.Set<CollectionVariantContentProductArea>().Add(new CollectionVariantContentProductArea
                    {
                        Id = integrationImage.Id,
                        CollectionVariantId = integrationImage.IntegrationCollectionVariantId,
                        ProductId = integrationImage.ProductId,
                        Height = integrationImage.Height,
                        Width = integrationImage.Width,
                        Left = integrationImage.Left,
                        Top = integrationImage.Top,
                        IsEnabled = integrationImage.IsEnabled
                    });
                }
                else
                {
                    /*update*/
                    image.Height = integrationImage.Height;
                    image.Width = integrationImage.Width;
                    image.Top = integrationImage.Top;
                    image.Left = integrationImage.Left;
                }
            }
        }

        private void SaveProperties(DbContext baseContext, Product product, UpdateIntegrationProductsCommand command)
        {
            /* remove all properties */
            for (int i = 0; i < product.Properties.Count; i++)
                product.Properties.Remove(product.Properties.ElementAt(i));

            /* Backlight */
            if (!string.IsNullOrEmpty(command.Backlight))
                product.Properties.Add(baseContext.Set<Property>().FirstOrDefault(p => p.Value == command.Backlight && p.SyncCode1C == EnumPropertiesType.Backlight.ToString()));
            /* FurnitureForm */
            if (!string.IsNullOrEmpty(command.FurnitureForm))
                product.Properties.Add(baseContext.Set<Property>().FirstOrDefault(p => p.Value == command.FurnitureForm && p.SyncCode1C == EnumPropertiesType.FurnitureForm.ToString()));
            /* LeftRight */
            if (!string.IsNullOrEmpty(command.LeftRight))
                product.Properties.Add(baseContext.Set<Property>().FirstOrDefault(p => p.Value == command.LeftRight && p.SyncCode1C == EnumPropertiesType.LeftRight.ToString()));
            /* Material */
            if (!string.IsNullOrEmpty(command.Material))
                product.Properties.Add(baseContext.Set<Property>().FirstOrDefault(p => p.Value == command.Material && p.SyncCode1C == EnumPropertiesType.Material.ToString()));
            /* Mechanism */
            if (!string.IsNullOrEmpty(command.Mechanism))
                product.Properties.Add(baseContext.Set<Property>().FirstOrDefault(p => p.Value == command.Mechanism && p.SyncCode1C == EnumPropertiesType.Mechanism.ToString()));

        }
    }
}
