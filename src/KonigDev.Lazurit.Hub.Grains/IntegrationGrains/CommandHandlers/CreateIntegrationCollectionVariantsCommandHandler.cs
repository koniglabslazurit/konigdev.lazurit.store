﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using Orleans;
using System.Threading.Tasks;
using KonigDev.Lazurit.Core.Transactions;
using System.Transactions;
using System;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using System.Data;
using Dapper;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.CommandHandlers
{
    public class CreateIntegrationCollectionVariantsCommandHandler : Grain, IHubCommandHandler<CreateIntegrationCollectionVariantsCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;
        public CreateIntegrationCollectionVariantsCommandHandler(IDBContextFactory contextFactory, ITransactionFactory transactionFactory)
        {
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(CreateIntegrationCollectionVariantsCommand command)
        {
            var query = "";
            foreach (var item in command.CollectionsVariants)
            {
                query += $"INSERT INTO IntegrationsCollectionsVariants(Id,IntegrationCollectionId,Facing,FacingColor,FrameColor,IsComplekt,IsDefault, SyncCode1C)VALUES('{item.Id}','{item.CollectionId}',N'{item.Facing}',N'{item.FacingColor}',N'{item.FrameColor}', {Convert.ToInt32(item.IsComplekt)} ,0, N'{item.SyncCode1C}')";
            }

            using (IDbConnection connection = _contextFactory.CreateDbConnection("LazuritIntegrationContext"))
            {
                connection.Open();
                TransactionOptions to = new TransactionOptions();
                to.IsolationLevel = System.Transactions.IsolationLevel.Snapshot;
                using (var transaction = _transactionFactory.CreateScope(TransactionScopeOption.Required, to))
                {
                    try
                    {
                        connection.Execute(query, transaction);
                        transaction.Complete();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }
    }
}
