﻿using Dapper;
using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using Orleans;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.CommandHandlers
{
    [Serializable]
    public class UpdateIntegrationProductsAfterImportCommandHandler : Grain, IHubCommandHandler<UpdateIntegrationProductsAfterImportCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;

        public UpdateIntegrationProductsAfterImportCommandHandler(IDBContextFactory contextFactory, ITransactionFactory transactionFactory)
        {
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(UpdateIntegrationProductsAfterImportCommand command)
        {
            using (IDbConnection connection = _contextFactory.CreateDbConnection("LazuritIntegrationContext"))
            {
                connection.Open();
                TransactionOptions to = new TransactionOptions();
                to.IsolationLevel = System.Transactions.IsolationLevel.Snapshot;
                using (var transaction = _transactionFactory.CreateScope(TransactionScopeOption.Required, to))
                {
                    try
                    {
                        connection.Execute(@"UPDATE IntegrationsProducts
                                            SET Hash = @Hash
                                            , FurnitureType = @FurnitureType
                                            , VendorCode = @VendorCode
                                            , RoomType = @RoomType
                                            , Collection = @Collection
                                            , VendorCodeList = @VendorCodeList
                                            , FrameColor = @FrameColor
                                            , Facing = @Facing
                                            , FacingColor = @FacingColor
                                            , Backlight = @Backlight
                                            , Material = @Material
                                            , LeftRight = @LeftRight
                                            , FurnitureForm = @FurnitureForm
                                            , ProductPartMarker = @ProductPartMarker
                                            , Height = @Height
                                            , Width = @Width
                                            , Length = @Length
                                            , Mechanism = @Mechanism
                                            , PermissibleLoad = @PermissibleLoad
                                            , SleepingAreaWidth = @SleepingAreaWidth
                                            , SleepingAreaLength = @SleepingAreaLength
                                            , Descr = @Descr
                                            , Type = @Type
                                            , Style = @Style
                                            , Packing = @Packing
                                            , Warranty = @Warranty                                                                                        
                                            , Series = @Series
                                            ,MarketerRange=@MarketerRange
                                            ,MarketerVendorCode = @MarketerVendorCode
                                             WHERE SyncCode1C = @SyncCode1C",
                                             command.Products.Select(p => new { p.SyncCode1C, p.Hash, p.FurnitureType, p.VendorCode, p.RoomType, p.Collection, p.VendorCodeList, p.FrameColor, p.Facing, p.FacingColor, p.Backlight,
                                             p.Material, p.LeftRight, p.FurnitureForm, p.ProductPartMarker, p.Height, p.Width, p.Length, p.Mechanism, p.PermissibleLoad, p.SleepingAreaWidth, p.SleepingAreaLength,
                                             p.Descr, p.TargetAudience, p.Style,p.Packing, p.Warranty, p.Series, p.MarketerRange, p.MarketerVendorCode}));
                        transaction.Complete();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }
    }
}
