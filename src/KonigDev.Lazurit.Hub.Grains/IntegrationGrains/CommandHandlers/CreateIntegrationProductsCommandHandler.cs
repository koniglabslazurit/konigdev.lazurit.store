﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using Orleans;
using System.Linq;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using KonigDev.Lazurit.Core.Enums.Enums;
using System.Data;
using System.Transactions;
using KonigDev.Lazurit.Core.Transactions;
using Dapper;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.CommandHandlers
{
    /// <summary>
    /// Запись новых продуктов в интеграционные таблицы, со связванием с вариантами, стилями, ца
    /// </summary>
    [Serializable]
    public class CreateIntegrationProductsCommandHandler : Grain, IHubCommandHandler<CreateIntegrationProductsCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;

        /// <summary>
        /// Запись новых продуктов в интеграционные таблицы, со связванием с вариантами, стилями, ца
        /// </summary>
        public CreateIntegrationProductsCommandHandler(IDBContextFactory contextFactory, ITransactionFactory transactionFactory)
        {
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }

        /// <summary>
        /// Запись новых продуктов в интеграционные таблицы, со связванием с вариантами, стилями, ца
        /// </summary>
        public async Task Execute(CreateIntegrationProductsCommand command)
        {
            var totalCount = command.Products.Count();
            var iteration = 0;
            var integrationStatusId = Guid.NewGuid();
            using (var context = _contextFactory.CreateLazuritIntegrationContext())
            {
                integrationStatusId = context.Set<IntegrationStatus>().Where(p => p.TechName == EnumIntegrationStatus.Validation.ToString()).Select(p => p.Id).First();
            }

            for (int i = 0; i < command.Products.Count; i = i + 30)
            {
                using (IDbConnection connection = _contextFactory.CreateDbConnection("LazuritIntegrationContext"))
                {
                    connection.Open();
                    TransactionOptions to = new TransactionOptions();
                    to.IsolationLevel = System.Transactions.IsolationLevel.Snapshot;
                    using (var transaction = _transactionFactory.CreateScope(TransactionScopeOption.Required, to))
                    {
                        try
                        {
                            var products = command.Products.OrderBy(p => p.SyncCode1C).Skip(30 * iteration).Take(30);

                            #region создание продукта
                            connection.Execute(updateProductQuery, products.Select(p => new
                            {
                                p.SyncCode1C,
                                p.Hash,
                                p.FurnitureType,
                                p.VendorCode,
                                p.RoomType,
                                p.Collection,
                                p.VendorCodeList,
                                p.FrameColor,
                                p.Facing,
                                p.FacingColor,
                                p.Backlight,
                                p.Material,
                                p.LeftRight,
                                p.FurnitureForm,
                                p.ProductPartMarker,
                                p.Height,
                                p.Width,
                                p.Length,
                                p.Mechanism,
                                p.PermissibleLoad,
                                p.SleepingAreaWidth,
                                p.SleepingAreaLength,
                                p.Descr,
                                Type = p.TargetAudience,
                                p.Style,
                                p.Packing,
                                p.Warranty,
                                IntegrationStatusId = integrationStatusId,
                                p.ProductId,
                                p.Series,
                                IsDefault = false,
                                p.MarketerRange,
                                p.MarketerVendorCode,
                                p.CollectionId
                            }));
                            #endregion

                            var variants = new List<dynamic>();
                            foreach (var item in products.ToList())
                            {
                                variants.AddRange(item.CollectionVariants.Select(p => new { VariantId = p, ProductSyncCode = item.SyncCode1C }));
                            }

                            if (variants.Count > 0)
                                connection.Execute(relateVariantsQuery, variants.Select(p => new { IntegrationProductId = p.ProductSyncCode, IntegrationCollectionVariantId = p.VariantId }));

                            /* стили */
                            var styles = new List<dynamic>();
                            foreach (var item in products.ToList())
                                styles.AddRange(item.Styles.Select(p => new { Style = p, IntegrationProductId = item.SyncCode1C, Id = Guid.NewGuid() }));

                            if (styles.Count > 0)
                                connection.Execute(relatestyles, styles);

                            /* целевая аудитория */
                            var ta = new List<dynamic>();
                            foreach (var item in products.ToList())
                                ta.AddRange(item.TargetAudiences.Select(p => new { Id = Guid.NewGuid(), IntegrationProductId = item.SyncCode1C, TargetAudience = p }));

                            if (ta.Count > 0)
                                connection.Execute(relativeTargetAudiences, ta);

                            transaction.Complete();
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }
                iteration++;
            }           
        }

        private void RelateProductWithCollectionVariant(IntegrationProduct productEntity, List<IntegrationCollectionVariant> collectionVariants)
        {
            /* добавление вариантов коллекций продукту
             * если у продукта отсутсвует FacingColor, то добавляем его во все коллекции, где совпадает Facing и FrameColor */

            var variants = new List<IntegrationCollectionVariant>();
            if (string.IsNullOrEmpty(productEntity.FacingColor))
                variants = collectionVariants.Where(x => x.Facing == productEntity.Facing && x.FrameColor == productEntity.FrameColor && x.IntegrationCollection.IntegrationRoom.SyncCode1C == productEntity.RoomType && x.IntegrationCollection.IntegrationSeries.SyncCode1C == productEntity.Series).ToList();
            else
                variants = collectionVariants.Where(x => x.Facing == productEntity.Facing && x.FacingColor == productEntity.FacingColor && x.FrameColor == productEntity.FrameColor && x.IntegrationCollection.IntegrationRoom.SyncCode1C == productEntity.RoomType && x.IntegrationCollection.IntegrationSeries.SyncCode1C == productEntity.Series).ToList();

            foreach (var colVar in variants)
            {
                if (!productEntity.IntegrationCollectionsVariants.Any(p => p.Id == colVar.Id))
                    productEntity.IntegrationCollectionsVariants.Add(colVar);
            }
        }

        /* соотношение с ца */
        private void RelateProductsWithTargetAudience(IntegrationProduct productEntity)
        {
            if (!string.IsNullOrEmpty(productEntity.Type))
                if (!productEntity.IntegrationProductTargetAudiencies.Any(p => p.TargetAudience == productEntity.Type))
                    productEntity.IntegrationProductTargetAudiencies.Add(new IntegrationProductToTargetAudience { Id = Guid.NewGuid(), IntegrationProductId = productEntity.SyncCode1C, TargetAudience = productEntity.Type });
        }

        /* соотношение со стилями */
        private void RelateProductsWithStyles(IntegrationProduct productEntity)
        {
            if (!string.IsNullOrEmpty(productEntity.Style))
                if (!productEntity.IntegrationProductStyles.Any(p => p.Style == productEntity.Style))
                    productEntity.IntegrationProductStyles.Add(new IntegrationProductToStyle { Id = Guid.NewGuid(), Style = productEntity.Style, IntegrationProductId = productEntity.SyncCode1C });
        }


        private string updateProductQuery
        {
            get
            {
                return @"INSERT INTO IntegrationsProducts
                            (SyncCode1C
                            ,[Hash]
                            ,FurnitureType
                            ,VendorCode
                            ,RoomType
                            ,[Collection]
                            ,VendorCodeList
                            ,FrameColor
                            ,Facing
                            ,FacingColor
                            ,Backlight
                            ,Material
                            ,LeftRight
                            ,FurnitureForm
                            ,ProductPartMarker
                            ,Height
                            ,Width
                            ,[Length]
                            ,Mechanism
                            ,PermissibleLoad
                            ,SleepingAreaWidth
                            ,SleepingAreaLength
                            ,Descr
                            ,[Type]
                            ,Style
                            ,Packing
                            ,Warranty
                            ,IntegrationStatusId 
                            ,ProductId
                            ,Series
                            ,IsDefault
                            ,MarketerRange
                            ,MarketerVendorCode
                            ,SleepingAreaAmount
                            ,CollectionId )
                        VALUES
                            (@SyncCode1C
                            ,@Hash
                            ,@FurnitureType
                            ,@VendorCode
                            ,@RoomType
                            ,@Collection
                            ,@VendorCodeList
                            ,@FrameColor
                            ,@Facing
                            ,@FacingColor
                            ,@Backlight
                            ,@Material
                            ,@LeftRight
                            ,@FurnitureForm
                            ,@ProductPartMarker
                            ,@Height
                            ,@Width
                            ,@Length
                            ,@Mechanism
                            ,@PermissibleLoad
                            ,@SleepingAreaWidth
                            ,@SleepingAreaLength
                            ,@Descr
                            ,@Type
                            ,@Style
                            ,@Packing
                            ,@Warranty
                            ,@IntegrationStatusId                         
                            ,@ProductId
                            ,@Series
                            ,@IsDefault
                            ,@MarketerRange
                            ,@MarketerVendorCode
                            , 0
                            , @CollectionId)";
            }
        }

        private string relateVariantsQuery
        {
            get
            {
                return @"INSERT INTO IntegrationsProductToIntegrationCollectionsVariants(IntegrationProductId,IntegrationCollectionVariantId)VALUES(@IntegrationProductId,@IntegrationCollectionVariantId)";
            }
        }

        private string relatestyles
        {
            get
            {
                return @"INSERT INTO IntegrationsProductsToStyles(Id,IntegrationProductId,Style)
                        VALUES(@Id,@IntegrationProductId,@Style)";
            }
        }

        private string relativeTargetAudiences
        {
            get
            {
                return @"INSERT INTO IntegrationsProductsToTargetAudiences(Id,IntegrationProductId,TargetAudience)
                        VALUES (@Id,@IntegrationProductId,@TargetAudience)";
            }
        }

    }
}