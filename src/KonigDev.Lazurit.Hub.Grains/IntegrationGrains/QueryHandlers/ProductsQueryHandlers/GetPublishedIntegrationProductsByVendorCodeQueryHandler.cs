﻿using KonigDev.Lazurit.Core.Enums.Enums;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto.Products;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using Orleans;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers.ProductsQueryHandlers
{
    public class GetPublishedIntegrationProductsByVendorCodeQueryHandler : Grain, IHubQueryHandler<GetPublishedIntegrationProductsByVendorCodeQuery, DtoPublishedProductResponse>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetPublishedIntegrationProductsByVendorCodeQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<DtoPublishedProductResponse> Execute(GetPublishedIntegrationProductsByVendorCodeQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritIntegrationContext())
            {
                var products = context.Set<IntegrationProduct>()
                    .AsQueryable()
                    .Where(p => p.IntegrationStatus.TechName == EnumIntegrationStatus.Published.ToString());
                var vendorCodes = products
                    .Select(p => new { p.FurnitureType, p.VendorCode })
                    .Distinct();

                //пагинация
                var totalCount = vendorCodes.Count();

                if (!string.IsNullOrEmpty(query.VendorCode))
                {
                    vendorCodes = vendorCodes.Where(p => p.VendorCode == query.VendorCode);
                }

                if (query.ItemsOnPage != 0)
                    vendorCodes = vendorCodes.OrderBy(p => p.VendorCode).Skip((query.Page - 1) * query.ItemsOnPage).Take(query.ItemsOnPage);

                var result = new DtoPublishedProductResponse();
                var list = new List<DtoPublishedProductItem>();

                var vendorCodesList = vendorCodes.ToList();

                foreach (var vendorCode in vendorCodesList)
                {
                    var item = new DtoPublishedProductItem
                    {
                        VendorCode = vendorCode.VendorCode,
                        FurnitureType = vendorCode.FurnitureType,
                        Products = products
                        .Where(p => p.VendorCode == vendorCode.VendorCode)                       
                        .Select(p => new DtoIntegrationProductItemResponse
                        {
                            ProductId = p.ProductId,
                            Backlight = p.Backlight,
                            ProductPartMarker = p.ProductPartMarker,
                            Descr = p.Descr,
                            Facing = p.Facing,
                            FacingColor = p.FacingColor,
                            FrameColor = p.FrameColor,
                            FurnitureForm = p.FurnitureForm,
                            FurnitureType = p.FurnitureType,
                            Height = p.Height,
                            IntegrationStatus = p.IntegrationStatus.TechName,
                            IsDefault = p.IsDefault,
                            IsDisassemblyState = p.IsDisassemblyState,
                            LeftRight = p.LeftRight,
                            Length = p.Length,
                            MarketerRange = p.MarketerRange,
                            MarketerVendorCode = p.MarketerVendorCode,
                            Material = p.Material,
                            Mechanism = p.Mechanism,
                            PermissibleLoad = p.PermissibleLoad,
                            RoomSize = p.RoomSize,
                            SleepingAreaAmount = p.SleepingAreaAmount,
                            SleepingAreaLength = p.SleepingAreaLength,
                            SleepingAreaWidth = p.SleepingAreaWidth,
                            Styles = p.IntegrationProductStyles.Select(s => s.Style).ToList(),
                            TargetTypes = p.IntegrationProductTargetAudiencies.Select(t => t.TargetAudience).ToList(),
                            SyncCode1C = p.SyncCode1C,
                            SyncCode1CConformity = p.SyncCode1CСonformity,
                            VendorCode = p.VendorCode,
                            Warranty = p.Warranty,
                            Width = p.Width
                        }).ToList()
                    };

                    list.Add(item);
                }
                result.Items = list;
                result.TotalCount = totalCount;

                return Task.FromResult(result);
            }
        }
    }
}
