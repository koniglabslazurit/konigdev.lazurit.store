﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using Orleans;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto.Products;
using System;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers.ProductsQueryHandlers
{
    /// <summary>
    /// Получение продуктов из интеграционных таблиц по запросу
    /// </summary>
    public class GetIntegrationProductByRequestQueryHandler : Grain, IHubQueryHandler<GetIntegrationProductByRequestQuery, DtoIntegrationProductResponse>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetIntegrationProductByRequestQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<DtoIntegrationProductResponse> Execute(GetIntegrationProductByRequestQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritIntegrationContext())
            {
                var resultQuery = context.Set<IntegrationProduct>()
                    .OrderBy(p => p.SyncCode1C) /* сортировка для пагинации */
                    .AsQueryable();

                if (!string.IsNullOrWhiteSpace(query.ProductStatus))
                    resultQuery = resultQuery.Where(p => p.IntegrationStatus.TechName == query.ProductStatus);

                if (!string.IsNullOrWhiteSpace(query.VendorCode))
                    resultQuery = resultQuery.Where(p => p.VendorCode == query.VendorCode);

                if (query.IntegrationCollectionId != Guid.Empty)
                    resultQuery = resultQuery.Where(p => p.CollectionId == query.IntegrationCollectionId);

                if (!string.IsNullOrWhiteSpace(query.SyncCode1C))
                    resultQuery = resultQuery.Where(p => p.SyncCode1C == query.SyncCode1C);

                if (!string.IsNullOrWhiteSpace(query.FurnitureType))
                    resultQuery = resultQuery.Where(p => p.FurnitureType == query.FurnitureType);

                if (!string.IsNullOrWhiteSpace(query.Facing))
                    resultQuery = resultQuery.Where(p => p.Facing == query.Facing);

                if (!string.IsNullOrWhiteSpace(query.FacingColor))
                    resultQuery = resultQuery.Where(p => p.FacingColor == query.FacingColor);

                if (!string.IsNullOrWhiteSpace(query.FrameColor))
                    resultQuery = resultQuery.Where(p => p.FrameColor == query.FrameColor);

                var totalCount = resultQuery.Count();

                /* пагинация */
                /* TODO подумать о максимальном количестве отдаваемых записей  */
                if (query.ItemsOnPage != 0)
                    resultQuery = resultQuery.Skip((query.Page - 1) * query.ItemsOnPage).Take(query.ItemsOnPage);

                var result = resultQuery.Select(p => new DtoIntegrationProductItemResponse
                {
                    MarketerVendorCode = p.MarketerVendorCode,
                    MarketerRange = p.MarketerRange,
                    ProductId = p.ProductId,
                    SyncCode1C = p.SyncCode1C,
                    Backlight = p.Backlight,
                    Descr = p.Descr,
                    Facing = p.Facing,
                    FacingColor = p.FacingColor,
                    FrameColor = p.FrameColor,
                    FurnitureForm = p.FurnitureForm,
                    FurnitureType = p.FurnitureType,
                    ProductPartMarker = p.ProductPartMarker,
                    Height = p.Height,
                    IntegrationStatus = p.IntegrationStatus.TechName,
                    LeftRight = p.LeftRight,
                    Length = p.Length,
                    Material = p.Material,
                    Mechanism = p.Mechanism,
                    PermissibleLoad = p.PermissibleLoad,
                    RoomSize = p.RoomSize,
                    SleepingAreaAmount = p.SleepingAreaAmount,
                    SleepingAreaLength = p.SleepingAreaLength,
                    SleepingAreaWidth = p.SleepingAreaWidth,
                    Styles = p.IntegrationProductStyles.Select(s => s.Style).ToList(),
                    TargetTypes = p.IntegrationProductTargetAudiencies.Select(t => t.TargetAudience).ToList(),
                    VendorCode = p.VendorCode,
                    Warranty = p.Warranty,
                    Width = p.Width,
                    SyncCode1CConformity = p.SyncCode1CСonformity,
                    IsDisassemblyState = p.IsDisassemblyState
                }).ToList();

                return Task.FromResult(new DtoIntegrationProductResponse { Items = result, TotalCount = totalCount });
            }
        }
    }
}
