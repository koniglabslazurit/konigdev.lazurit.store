﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers
{
    public class GetIntegrationSeriesQueryHandler : Grain, IHubQueryHandler<GetIntegrationSeriesQuery, List<DtoIntegrationSeriesItem>>
    {
        private readonly IDBContextFactory _contextFactory;

        public GetIntegrationSeriesQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public Task<List<DtoIntegrationSeriesItem>> Execute(GetIntegrationSeriesQuery query)
        {
            using (var context = _contextFactory.CreateLazuritIntegrationContext())
            {
                var result = context.Set<IntegrationSeries>()
                    .Select(p => new DtoIntegrationSeriesItem
                    {
                        Id = p.Id,
                        SyncCode1C = p.SyncCode1C,
                        OriginalId = p.OriginalId
                    }).ToList();
                return Task.FromResult(result);
            }
        }
    }
}
