﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Query;
using Orleans;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers
{
    public class GetIntegratedRoomsQueryHandler : Grain, IHubQueryHandler<GetIntegrationRoomsQuery, List<DtoIntegrationRoomItem>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetIntegratedRoomsQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoIntegrationRoomItem>> Execute(GetIntegrationRoomsQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritIntegrationContext())
            {
                var result = context.Set<IntegrationRoom>()
                    .Select(p => new DtoIntegrationRoomItem
                    {
                        Id = p.Id,
                        SyncCode1C = p.SyncCode1C,
                        OriginalId = p.OriginalId
                    }).ToList();

                return Task.FromResult(result);
            }
        }
    }
}
