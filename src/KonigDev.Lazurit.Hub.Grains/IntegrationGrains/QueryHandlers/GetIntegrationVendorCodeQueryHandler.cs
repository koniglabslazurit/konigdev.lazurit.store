﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers
{
    public class GetIntegrationVendorCodeQueryHandler : Grain, IHubQueryHandler<GetIntegrationVendorCodeQuery, List<DtoIntegrationVendorCodeItem>>
    {
        private readonly IDBContextFactory _contextFactory;

        public GetIntegrationVendorCodeQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }
        public Task<List<DtoIntegrationVendorCodeItem>> Execute(GetIntegrationVendorCodeQuery query)
        {
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var result = context.Set<Article>()
                    .Select(p => new DtoIntegrationVendorCodeItem
                    {
                        Id = p.Id,
                        SyncCode1C = p.SyncCode1C,
                        Range = p.Range,
                        FurnitureTypeId = p.FurnitureTypeId
                    }).ToList();
                return Task.FromResult(result);
            }
        }
    }
}
