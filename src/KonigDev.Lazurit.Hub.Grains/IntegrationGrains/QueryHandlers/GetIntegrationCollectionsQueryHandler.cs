﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using Orleans;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers
{
    public class GetIntegrationCollectionsQueryHandler : Grain, IHubQueryHandler<GetIntegrationCollectionsQuery, List<DtoIntegrationCollectionItem>>
    {
        private readonly IDBContextFactory _contextFactory;

        public GetIntegrationCollectionsQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public Task<List<DtoIntegrationCollectionItem>> Execute(GetIntegrationCollectionsQuery query)
        {
            using (var context = _contextFactory.CreateLazuritIntegrationContext())
            {
                var result = context.Set<IntegrationCollection>()
                    .Select(p => new DtoIntegrationCollectionItem
                    {
                        Id = p.Id,
                        Room = p.IntegrationRoom.SyncCode1C,
                        Series = p.IntegrationSeries.SyncCode1C,
                        RoomId = p.IntegrationRoomId,
                        SeriesId = p.IntegrationSeriesId,
                        OriginalId = p.OriginalId
                    }).ToList();
                return Task.FromResult(result);
            }
        }
    }
}
