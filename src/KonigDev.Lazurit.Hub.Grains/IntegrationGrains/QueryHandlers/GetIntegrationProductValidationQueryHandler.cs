﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using Orleans;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto.Products;
using KonigDev.Lazurit.Model.Entities;
using KonigDev.Lazurit.Core.Enums;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers
{
    /// <summary>
    /// Валидация продукта для его возможности публикации в основные таблицы
    /// </summary>
    public class GetIntegrationProductValidationQueryHandler : Grain, IHubQueryHandler<GetIntegrationProductValidationQuery, DtoIntegrationProductValidation>
    {
        private readonly IDBContextFactory _dbContextFactory;

        /// <summary>
        /// Валидация продукта для его возможности публикации в основные таблицы
        /// </summary>
        public GetIntegrationProductValidationQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        /// <summary>
        /// Валидация продукта для его возможности публикации в основные таблицы
        /// </summary>
        public Task<DtoIntegrationProductValidation> Execute(GetIntegrationProductValidationQuery query)
        {
            var result = new DtoIntegrationProductValidation { IsValid = true };

            using (var context = _dbContextFactory.CreateLazuritIntegrationContext())
            {
                var product = context.Set<IntegrationProduct>().FirstOrDefault(p => p.SyncCode1C == query.SyncCode1C);
                if (product == null)
                {
                    result.IsValid = false;
                    result.Errors.Add("Не найден продукт.");
                    return Task.FromResult(result);
                }

                /* закоменчено: логика публикации продукта и варианта коллекции. Публикуется без варианта, проверка на изображения в варианте не нужно */
                //var variants = product.IntegrationCollectionsVariants.Select(p => p.Id);

                var pricesExist = context.Set<IntegrationPrice>().Any(p => p.ProductSyncCode1C == query.SyncCode1C);
                if (!pricesExist)
                {
                    result.IsValid = false;
                    result.Errors.Add("У продукта отсутсвуют цены.");
                }

                using (var baseContext = _dbContextFactory.CreateLazuritContext())
                {
                    var imagesCount = baseContext.Set<FileContent>()
                        .Where(p => p.ObjectId == product.ProductId)
                        .Where(p => p.Type.TechName == EnumFileContentType.WithoutInterior.ToString()
                        || p.Type.TechName == EnumFileContentType.InInterior.ToString()
                        || p.Type.TechName == EnumFileContentType.SliderMiddle.ToString()
                        || p.Type.TechName == EnumFileContentType.SliderOpen.ToString()).Count();

                    if (imagesCount < 4)
                    {
                        result.IsValid = false;
                        result.Errors.Add("У продукта не загружены изображения");
                    }

                    /* закоменчено: логика публикации продукта и варианта коллекции. Публикуется без варианта, проверка на изображения в варианте не нужно */
                    /* ------------------------------------------------------------- */
                    //var collectionImages = baseContext.Set<FileContent>()
                    //    .Where(p => variants.Contains(p.ObjectId))
                    //    .Where(p => p.Type.TechName == EnumFileContentType.InInterior.ToString()
                    //    || p.Type.TechName == EnumFileContentType.SliderMiddle.ToString()
                    //    || p.Type.TechName == EnumFileContentType.SliderMin.ToString())
                    //    .Count();

                    //if (collectionImages < 3)
                    //{
                    //    result.IsValid = false;
                    //    result.Errors.Add("У варианта коллекции не загружены картинки");
                    //}
                }
            }
            return Task.FromResult(result);
        }
    }
}
