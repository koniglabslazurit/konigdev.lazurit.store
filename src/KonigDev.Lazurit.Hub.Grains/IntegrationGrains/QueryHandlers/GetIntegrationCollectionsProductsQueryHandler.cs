﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using Orleans;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using KonigDev.Lazurit.Core.Enums.Enums;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto.Collections;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers
{
    public class GetIntegrationCollectionsProductsQueryHandler : Grain, IHubQueryHandler<GetIntegrationCollectionsProductsQuery, DtoIntegrationCollectionTableResponse>
    {
        private readonly IDBContextFactory _contextFactory;

        public GetIntegrationCollectionsProductsQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public Task<DtoIntegrationCollectionTableResponse> Execute(GetIntegrationCollectionsProductsQuery query)
        {
            using (var context = _contextFactory.CreateLazuritIntegrationContext())
            {

                var queryDb = context.Set<IntegrationCollection>()
                    .OrderBy(p => p.IntegrationSeries.SyncCode1C)
                    .AsQueryable();

                var totalCount = queryDb.Count();

                if (query.ItemsOnPage != 0)
                    queryDb = queryDb.Skip((query.Page - 1) * query.ItemsOnPage).Take(query.ItemsOnPage);               

                var collections = queryDb
                    .Select(p => new DtoIntegrationCollectionsProductsItem
                    {
                        Id = p.Id,
                        OriginalId = p.OriginalId,
                        Room = p.IntegrationRoom.SyncCode1C,
                        Series = p.IntegrationSeries.SyncCode1C,
                        PublishedProducts = p.IntegrationCollectionVariants.SelectMany(v => v.IntegrationProducts).Where(product => product.IntegrationStatus.TechName == EnumIntegrationStatus.Published.ToString()).Select(product => product.SyncCode1C).Distinct().Count(),
                        TemplateProducts = p.IntegrationCollectionVariants.SelectMany(v => v.IntegrationProducts).Where(product => product.IntegrationStatus.TechName == EnumIntegrationStatus.Template.ToString()).Select(product => product.SyncCode1C).Distinct().Count(),
                        ValidationProducts = p.IntegrationCollectionVariants.SelectMany(v => v.IntegrationProducts).Where(product => product.IntegrationStatus.TechName == EnumIntegrationStatus.Validation.ToString()).Select(product => product.SyncCode1C).Distinct().Count(),
                    }).ToList();

                return Task.FromResult(new DtoIntegrationCollectionTableResponse { Items = collections, TotalCount = totalCount});
            }
        }
    }
}
