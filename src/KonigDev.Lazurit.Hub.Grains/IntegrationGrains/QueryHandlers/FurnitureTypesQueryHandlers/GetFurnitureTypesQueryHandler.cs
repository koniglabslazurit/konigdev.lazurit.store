﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers
{
    public class GetFurnitureTypesQueryHandler : Grain, IHubQueryHandler<GetFurnitureTypesQuery, List<DtoFurnitureTypeItem>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetFurnitureTypesQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public Task<List<DtoFurnitureTypeItem>> Execute(GetFurnitureTypesQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritIntegrationContext())
            {
                var integrProducts = context.Set<IntegrationProduct>().AsQueryable();

                if (query.IntegrationCollectionId != Guid.Empty)
                    integrProducts = integrProducts.Where(p => p.IntegrationCollectionsVariants.Any(v => v.IntegrationCollectionId == query.IntegrationCollectionId));

                var resultQuery = integrProducts
                    .Select(p => p.FurnitureType)
                    .Distinct()
                    .Select(p => new DtoFurnitureTypeItem
                    {
                        Name = p,
                        SyncCode1C = p                        
                    }).ToList();

                return Task.FromResult(resultQuery);
            }
        }
    }
}
