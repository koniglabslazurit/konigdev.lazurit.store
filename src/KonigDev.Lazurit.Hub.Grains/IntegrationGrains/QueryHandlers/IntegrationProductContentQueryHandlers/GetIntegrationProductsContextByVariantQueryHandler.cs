﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using Orleans;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Integration.Context.Entities;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers.IntegrationProductContentQueryHandlers
{
    public class GetIntegrationProductsContextByVariantQueryHandler : Grain, IHubQueryHandler<GetIntegrationProductsContextByCollectionVariantQuery, List<DtoIntegrationProductContentResult>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetIntegrationProductsContextByVariantQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoIntegrationProductContentResult>> Execute(GetIntegrationProductsContextByCollectionVariantQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritIntegrationContext())
            {
                var products = context.Set<IntegrationProduct>()
                    .Where(p => p.IntegrationCollectionsVariants.Any(c => c.Id == query.IntegrationCollectionVariantId))
                    .Select(p => new DtoIntegrationProductContentResult
                    {
                        Id = p.ProductId,
                        Article = p.SyncCode1C
                    }).ToList();

                var productContents = context.Set<IntegrationCollectionVariantContentProductArea>()
                    .Where(x => x.IntegrationCollectionVariantId == query.IntegrationCollectionVariantId && x.IsEnabled)
                    .Select(x => new DtoIntegrationProductContentResult
                    {
                        Id = x.ProductId,
                        ObjectId = x.Id,
                        Height = x.Height,
                        Width = x.Width,
                        Left = x.Left,
                        Top = x.Top
                    }).ToList();

                foreach (var product in products)
                {
                    foreach (var productContent in productContents.Where(productContent => product.Id == productContent.Id))
                    {
                        product.ObjectId = productContent.ObjectId;
                        product.Height = productContent.Height;
                        product.Width = productContent.Width;
                        product.Top = productContent.Top;
                        product.Left = productContent.Left;
                    }
                }
                return Task.FromResult(products);
            }
        }
    }
}

