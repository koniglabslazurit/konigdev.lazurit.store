﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers
{
    public class GetSyncCode1CsQueryHandler : Grain, IHubQueryHandler<GetSyncCode1CsQuery, List<DtoSyncCode1CItem>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetSyncCode1CsQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public Task<List<DtoSyncCode1CItem>> Execute(GetSyncCode1CsQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritIntegrationContext())
            {
                var integrProducts = context.Set<IntegrationProduct>().AsQueryable();
                if (query.IntegrationCollectionId != Guid.Empty)
                    integrProducts = integrProducts.Where(p => p.IntegrationCollectionsVariants.Any(c => c.IntegrationCollectionId == query.IntegrationCollectionId));

                var resultQuery = integrProducts
                    .Select(p => p.SyncCode1C)
                    .Distinct()
                    .Select(p => new DtoSyncCode1CItem
                    {
                        Name = p
                    }).ToList();
                return Task.FromResult(resultQuery);
            }
        }
    }
}
