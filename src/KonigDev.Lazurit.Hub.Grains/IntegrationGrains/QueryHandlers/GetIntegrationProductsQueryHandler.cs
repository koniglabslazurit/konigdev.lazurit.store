﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using Orleans;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using KonigDev.Lazurit.Core.Enums.Enums;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers
{

    public class GetIntegrationProductsQueryHandler : Grain, IHubQueryHandler<GetIntegrationProductsQuery, List<DtoIntegrationProduct>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetIntegrationProductsQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public Task<List<DtoIntegrationProduct>> Execute(GetIntegrationProductsQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritIntegrationContext())
            {
                var result = context.Set<IntegrationProduct>()
                    .Select(p => new DtoIntegrationProduct
                    {
                        SyncCode1C = p.SyncCode1C,
                        Hash = p.Hash,
                        IntegrationStatus = p.IntegrationStatus.TechName
                    }).ToList();

                return Task.FromResult(result);
            }
        }
    }
}
