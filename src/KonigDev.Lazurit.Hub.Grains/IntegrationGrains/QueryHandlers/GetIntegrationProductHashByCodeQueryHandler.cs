﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using Orleans;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers
{
    public class GetIntegrationProductHashByCodeQueryHandler : Grain, IHubQueryHandler<GetIntegrationProductHashByCodeQuery, DtoIntegrationProduct>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetIntegrationProductHashByCodeQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<DtoIntegrationProduct> Execute(GetIntegrationProductHashByCodeQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritIntegrationContext())
            {
                var result = context.Set<IntegrationProduct>().Where(p => p.SyncCode1C == query.SyncCode1C)
                    .Select(p => new DtoIntegrationProduct
                    {
                        Hash = p.Hash
                    }).FirstOrDefault();

                return Task.FromResult(result);
            }
        }
    }
}
