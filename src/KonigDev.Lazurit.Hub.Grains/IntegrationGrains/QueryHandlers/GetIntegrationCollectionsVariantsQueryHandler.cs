﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using Orleans;
using System.Linq;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Integration.Context.Entities;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers
{
    public class GetIntegrationCollectionsVariantsQueryHandler : Grain, IHubQueryHandler<GetIntegrationCollectionsVariantsQuery, List<DtoIntegrationCollectionVariantItem>>
    {
        private readonly IDBContextFactory _contextFactory;

        public GetIntegrationCollectionsVariantsQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }
        public Task<List<DtoIntegrationCollectionVariantItem>> Execute(GetIntegrationCollectionsVariantsQuery query)
        {
            using (var context = _contextFactory.CreateLazuritIntegrationContext())
            {

                var queryDb = context.Set<IntegrationCollectionVariant>().AsQueryable();

                if (query.IsComplekt.HasValue)
                    queryDb = queryDb.Where(p => p.IsComplekt == query.IsComplekt.Value);

                var result = queryDb
                    .Select(p => new DtoIntegrationCollectionVariantItem
                    {
                        Id = p.Id,
                        CollectionId = p.IntegrationCollectionId,
                        Facing = p.Facing,
                        FacingColor = p.FacingColor,
                        FrameColor = p.FrameColor,
                        IsComplekt = p.IsComplekt,
                        Room = p.IntegrationCollection.IntegrationRoom.SyncCode1C,
                        Series = p.IntegrationCollection.IntegrationSeries.SyncCode1C,
                        SyncCode1C = p.SyncCode1C
                    }).ToList();
                return Task.FromResult(result);
            }
        }
    }
}
