﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers
{
    //todo вынести в базовый контекст и грейны
    public class GetIntegrationFurnitureTypeQueryHandler : Grain, IHubQueryHandler<GetIntegrationFurnitureTypeQuery, List<DtoIntegrationFurnitureTypeItem>>
    {
        private readonly IDBContextFactory _contextFactory;

        public GetIntegrationFurnitureTypeQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public Task<List<DtoIntegrationFurnitureTypeItem>> Execute(GetIntegrationFurnitureTypeQuery query)
        {
            //todo вынести в базовый контекст и грейны
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var result = context.Set<FurnitureType>()
                    .Select(p => new DtoIntegrationFurnitureTypeItem
                    {
                        Id = p.Id,
                        SyncCode1C = p.SyncCode1C
                    }).ToList();

                return Task.FromResult(result);
            }
        }
    }
}
