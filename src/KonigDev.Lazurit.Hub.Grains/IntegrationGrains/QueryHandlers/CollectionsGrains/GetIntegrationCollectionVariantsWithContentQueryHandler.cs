﻿using System.Data;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using Orleans;
using System.Linq;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using KonigDev.Lazurit.Core.Enums;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query.Collection;

namespace KonigDev.Lazurit.Hub.Grains.IntegrationGrains.QueryHandlers.CollectionsGrains
{
    /// <summary>
    /// Получение вариантов для коллекции, с продуктами и данными по контенту
    /// </summary>
    public class GetIntegrationVariantsWithContentQueryHandler : Grain, IHubQueryHandler<GetIntegrationVariantsWithContentQuery, DtoIntegrationCollectionWithContent>
    {
        private readonly IDBContextFactory _contextFactory;

        /// <summary>
        /// Получение вариантов для коллекции, с продуктами и данными по контенту
        /// </summary>
        public GetIntegrationVariantsWithContentQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        /// <summary>
        /// Получение вариантов для коллекции, с продуктами и данными по контенту
        /// </summary>
        public Task<DtoIntegrationCollectionWithContent> Execute(GetIntegrationVariantsWithContentQuery query)
        {
            using (var context = _contextFactory.CreateLazuritIntegrationContext())
            {
                var result = context.Set<IntegrationCollection>()
                    .Where(p => p.Id == query.CollectionId)
                    .Select(collection => new DtoIntegrationCollectionWithContent
                    {
                        Id = collection.Id,
                        RoomType = collection.IntegrationRoom.SyncCode1C,
                        SeriesName = collection.IntegrationSeries.SyncCode1C,
                        IntegrationVariants = collection.IntegrationCollectionVariants.Select(variant => new DtoIntegrationCollectionVariantContent
                        {
                            CollectionId = collection.Id,
                            Id = variant.Id,
                            OriginalId = variant.OriginalId,
                            CollectionTypeName = variant.IsComplekt == true
                            ? EnumCollectionTypes.Komplekt.ToString()
                            : EnumCollectionTypes.Collection.ToString(),
                            IsComplect = variant.IsComplekt,
                            IsDefault = variant.IsDefault,
                            IsPublished = variant.OriginalId.HasValue,
                            Facing = variant.Facing,
                            FacingColor = variant.FacingColor,
                            FrameColor = variant.FrameColor,
                            IntegrationProducts = variant.IntegrationProducts.Select(product => new DtoIntegrationProductContent
                            {
                                ProductId = product.ProductId,
                                SyncCode1C = product.SyncCode1C,
                                FurnitureType = product.FurnitureType,
                                Facing = product.Facing,
                                FrameColor = product.FrameColor,
                                FacingColor = product.FacingColor
                            }).ToList()
                        }).OrderBy(x => x.FrameColor).ThenBy(x => x.Facing).ThenBy(x => x.FacingColor).ToList()
                    }).FirstOrDefault();

                var variantsId = result.IntegrationVariants.Select(z => z.Id).ToList();
                var prodContents = context.Set<IntegrationCollectionVariantContentProductArea>()
                    .Where(x => variantsId.Contains(x.IntegrationCollectionVariantId))
                    .Select(x => new { x.Id, x.ProductId, x.Height, x.Width, x.Top, x.Left, x.IntegrationCollectionVariantId }).ToList();
                /* todo need optimization */
                if (result != null && prodContents.Count > 0)
                {
                    foreach (var variant in result.IntegrationVariants)
                    {
                        foreach (var product in variant.IntegrationProducts)
                        {
                            foreach (var content in prodContents)
                            {
                                if (product.ProductId == content.ProductId && variant.Id == content.IntegrationCollectionVariantId)
                                {
                                    product.ObjectId = content.Id;
                                    product.ContentHeight = content.Height;
                                    product.ContentWidth = content.Width;
                                    product.ContentTop = content.Top;
                                    product.ContentLeft = content.Left;
                                }
                            }
                        }
                    }
                }
                return Task.FromResult(result);
            }
        }
    }
}