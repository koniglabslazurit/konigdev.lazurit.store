﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants;
using KonigDev.Lazurit.Hub.Model.DTO.Layaways;
using KonigDev.Lazurit.Hub.Model.DTO.Layaways.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Products;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Layaways.QueryHandlers
{
    public class GetLayawaysByUserQueryHandler : Grain, IHubQueryHandler<GetLayawaysByUserQuery, List<DtoLayaway>>
    {
        private readonly IDBContextFactory _dbContextFactory;
        public GetLayawaysByUserQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public Task<List<DtoLayaway>> Execute(GetLayawaysByUserQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                 var layaways = context.Set<Layaway>().Where(x => x.UserId == query.UserId).Select(x => new DtoLayaway
                {
                    Id = x.Id,
                    CreationTime = x.CreationTime,
                    IsWholeCart = x.IsWholeCart,
                    UserId = x.UserId,
                    Collections = x.Collections.Select(c => new DtoLayawayItemCollection
                    {
                        Id = c.Id,
                        CollectionVarinatId = c.CollectionVarinatId,
                        CollectionName = c.CollectionVariant.Collection.Title,
                        RoomTypeName = c.CollectionVariant.Collection.RoomType.Name,
                        LayawayId = c.LayawayId,
                        Quantity = c.Quantity,
                        ProductsInCollection = c.ProductsInCollection.Select(p => new DtoLayawayItemProductInCollection
                        {
                            Id = p.Id,
                            IsAssemblyRequired = p.IsAssemblyRequired,
                            LayawayItemCollectionId = p.LayawayItemCollectionId,
                            ProductId = p.ProductId,
                            Quantity = p.Quantity,
                            Product = new DtoProductResult
                            {
                                Id = p.ProductId,
                                Article = p.Product.Article.SyncCode1C,
                                SeriesName = c.CollectionVariant.Collection.Series.Name,
                                CollectionAlias = c.CollectionVariant.Collection.Series.Alias,
                                CollectionId = c.CollectionVariant.Collection.Id,
                                FurnitureType = p.Product.Article.FurnitureType.Name,
                                Length = p.Product.Length,
                                Height = p.Product.Height,
                                Width = p.Product.Width,
                                FacingColorName = p.Product.FacingColor.Name,
                                FrameColorIdName = p.Product.FrameColor.Name,
                                FacingName = p.Product.Facing.Name
                            }
                        }).ToList()
                    }).ToList(),
                    Complects = x.Complects.Select(c => new DtoLayawayItemComplect
                    {
                        Id = c.Id,
                        CollectionVarinatId = c.CollectionVarinatId,
                        IsAssemblyRequired = c.IsAssemblyRequired,
                        LayawayId = c.LayawayId,
                        Quantity = c.Quantity,
                        CollectionName = c.CollectionVariant.Collection.Title,
                        RoomTypeName = c.CollectionVariant.Collection.RoomType.Name,
                        CollectionVariant = new DtoCollectionVariantResult
                        {
                            Products = c.CollectionVariant.Products.Select(p => new DtoProductResult
                            {
                                Id = p.Id,
                                Article = p.Article.SyncCode1C,
                                SeriesName = c.CollectionVariant.Collection.Series.Name,
                                CollectionAlias = c.CollectionVariant.Collection.Series.Alias,
                                CollectionId = c.CollectionVariant.Collection.Id,
                                FurnitureType = p.Article.FurnitureType.Name,
                                Length = p.Length,
                                Height = p.Height,
                                Width = p.Width,
                                FacingColorName = p.FacingColor.Name,
                                FrameColorIdName = p.FrameColor.Name,
                                FacingName = p.Facing.Name
                            })
                        }
                    }).ToList(),
                    Products = x.Products.Select(p => new DtoLayawayItemProduct
                    {
                        Id = p.Id,
                        IsAssemblyRequired = p.IsAssemblyRequired,
                        LayawayId = p.LayawayId,
                        ProductId = p.ProductId,
                        Quantity = p.Quantity,
                        Product = new DtoProductResult
                        {
                            Id = p.ProductId,
                            Article = p.Product.Article.SyncCode1C,
                            SeriesName = p.Product.Collection.Series.Name,
                            CollectionAlias = p.Product.Collection.Series.Alias,
                            CollectionId = p.Product.CollectionId,
                            FurnitureType = p.Product.Article.FurnitureType.Name,
                            Length = p.Product.Length,
                            Height = p.Product.Height,
                            Width = p.Product.Width,
                            FacingColorName = p.Product.FacingColor.Name,
                            FrameColorIdName = p.Product.FrameColor.Name,
                            FacingName = p.Product.Facing.Name
                        }
                    }).ToList()
                }).OrderBy(l => l.CreationTime).ToList();

                return Task.FromResult(layaways);
            }
        }
    }
}
