﻿using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Layaways.Command;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace KonigDev.Lazurit.Hub.Grains.Layaways.CommandHandlers
{
    public class RemoveLayawayItemCommandHandler : Grain, IHubCommandHandler<RemoveLayawayItemCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;
        private readonly ITransactionFactory _transactionFactory;

        public RemoveLayawayItemCommandHandler(IDBContextFactory dbContextFactory, ITransactionFactory transactionFactory)
        {
            _dbContextFactory = dbContextFactory;
            _transactionFactory = transactionFactory;
        }
        public async Task Execute(RemoveLayawayItemCommand command)
        {
            if (command == null)
                return;
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                Cart cart = context.Set<Cart>().FirstOrDefault(x => x.Id == command.CartId);
                Layaway layaway = context.Set<Layaway>().FirstOrDefault(x => x.Id == command.Id);
                if (cart == null || layaway == null)
                {
                    return;
                }
                foreach (var collection in command.Collections)
                {
                    var layawayItemCollection = layaway.Collections.FirstOrDefault(c => c.Id == collection.LayawayItemCollectionId);
                    if (layawayItemCollection == null)
                        return;
                    var cartItemCollectionId = Guid.NewGuid();
                    var cartItemProductsInCollection = new List<CartItemProductInCollection>();
                    if (cart.Collections.Any(c => c.CollectionVariantId == collection.CollectionVarinatId))
                    {
                        var cartCollection = cart.Collections.FirstOrDefault(c => c.CollectionVariantId == collection.CollectionVarinatId);
                        if (cartCollection != null)
                        {
                            cartCollection.Quantity += collection.Quantity;
                            cartItemCollectionId = cartCollection.Id;
                        }
                    }
                    else
                    {
                        cart.Collections.Add(new CartItemCollection
                        {
                            CollectionVariantId = collection.CollectionVarinatId,
                            CartId = cart.Id,
                            Quantity = collection.Quantity,
                            Id = cartItemCollectionId
                        });
                    }
                    foreach (var productInCollection in collection.ProductsInCollection)
                    {
                        if (context.Set<CartItemProductInCollection>().Any(p => p.CartItemCollection.CartId == cart.Id && p.ProductId == productInCollection.ProductId))
                        {
                            var cartProductInCollection = context.Set<CartItemProductInCollection>().FirstOrDefault(p => p.CartItemCollection.CartId == cart.Id && p.ProductId == productInCollection.ProductId);
                            if (cartProductInCollection != null)
                            {
                                cartProductInCollection.Quantity += productInCollection.Quantity;
                            }
                        }
                        else
                        {
                            cart.Collections.FirstOrDefault(c => c.CollectionVariantId == collection.CollectionVarinatId)
                                .Products.Add(new CartItemProductInCollection
                                {
                                    Id = Guid.NewGuid(),
                                    ProductId = productInCollection.ProductId,
                                    Quantity = productInCollection.Quantity,
                                    CartItemCollectionId = cartItemCollectionId,
                                    IsAssemblyRequired = true
                                });
                        }
                        var layawayItemProductInCollection = layawayItemCollection.ProductsInCollection
                            .FirstOrDefault(p => p.ProductId == productInCollection.ProductId);
                        if (layawayItemProductInCollection != null)
                        {
                            context.Set<LayawayItemProductInCollection>().Remove(layawayItemProductInCollection);
                        }
                    }
                    if (context.Set<LayawayItemProductInCollection>().Where(p => p.LayawayItemCollectionId == collection.LayawayItemCollectionId).Count() == collection.ProductsInCollection.Count)
                    {
                        context.Set<LayawayItemCollection>().Remove(layawayItemCollection);
                    }
                }

                foreach (var product in command.Products)
                {
                    if (cart.Products.Any(p => p.ProductId == product.ProductId))
                    {
                        var cartProduct = cart.Products.Where(p => p.ProductId == product.ProductId).FirstOrDefault();
                        if (cartProduct != null)
                        {
                            cartProduct.Quantity += product.Quantity;
                        }
                    }
                    else
                    {
                        cart.Products.Add(new CartItemProduct
                        {
                            Id = Guid.NewGuid(),
                            CartId = cart.Id,
                            ProductId = product.ProductId,
                            Quantity = product.Quantity,
                            IsAssemblyRequired = product.IsAssemblyRequired,
                        });
                    }
                    context.Set<LayawayItemProduct>().RemoveRange(layaway.Products.Where(x => x.ProductId == product.ProductId));
                }
                foreach (var complect in command.Complects)
                {
                    if (cart.Complects.Any(p => p.CollectionVariantId == complect.CollectionVariantId))
                    {
                        var cartComplect = cart.Complects.Where(p => p.CollectionVariantId == complect.CollectionVariantId).FirstOrDefault();
                        if (cartComplect != null)
                        {
                            cartComplect.Quantity += complect.Quantity;
                        }
                    }
                    else
                    {
                        cart.Complects.Add(new CartItemComplect
                        {
                            CollectionVariantId = complect.CollectionVariantId,
                            CartId = cart.Id,
                            Quantity = complect.Quantity,
                            Id = Guid.NewGuid(),
                            IsAssemblyRequired = complect.IsAssemblyRequired
                        });
                    }
                    //TODO NullReference
                    context.Set<LayawayItemComplect>().RemoveRange(layaway.Complects.Where(x => x.CollectionVarinatId == complect.CollectionVariantId));
                }
                //TODO NullReference
                if (layaway.Collections.Count == 0 && layaway.Complects.Count == 0 && layaway.Products.Count == 0)
                {
                    context.Set<Layaway>().Remove(layaway);
                }
                await context.SaveChangesAsync();
            }
        }
    }
}