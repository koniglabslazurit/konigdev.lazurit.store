﻿using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Layaways.Command;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace KonigDev.Lazurit.Hub.Grains.Layaways.CommandHandlers
{
    public class MergeLayawasCommandHandler : Grain, IHubCommandHandler<MergeLayawasCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;
        private readonly ITransactionFactory _transactionFactory;

        public MergeLayawasCommandHandler(IDBContextFactory dbContextFactory, ITransactionFactory transactionFactory)
        {
            _dbContextFactory = dbContextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(MergeLayawasCommand command)
        {
            if (command.UserId == null || command.UserId == Guid.Empty)
            {
                return;
            }
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var userLayaways = context.Set<Layaway>().Where(l => l.UserId == command.UserId).ToList();
                var anonimLayaways = context.Set<Layaway>().Where(l => command.LayawayIds.Contains(l.Id)).ToList();

                if (userLayaways == null && anonimLayaways != null)
                {
                    foreach (var anonimLayaway in anonimLayaways)
                    {
                        anonimLayaway.UserId = command.UserId;
                    }
                }
                else
                {
                    var anonimLayawaysWithWholeCart = anonimLayaways.Where(a => a.IsWholeCart);
                    foreach (var anonimLayaway in anonimLayawaysWithWholeCart)
                    {
                        anonimLayaway.UserId = command.UserId;
                    }

                    var anonimLayawaysWithoutWholeCart = anonimLayaways.Where(a => !a.IsWholeCart);
                    foreach (var anonimLayaway in anonimLayawaysWithoutWholeCart)
                    {
                        anonimLayaway.UserId = command.UserId;
                        foreach (var product in anonimLayaway.Products)
                        {
                            var existingProductInUserLayaways = context.Set<LayawayItemProduct>()
                                .FirstOrDefault(p => p.ProductId == product.ProductId && command.UserId == p.Layaway.UserId);
                            if (existingProductInUserLayaways != null)
                            {
                                existingProductInUserLayaways.Quantity += product.Quantity;
                            }
                        }
                        context.Set<LayawayItemProduct>().RemoveRange(anonimLayaway.Products
                            .Where(p => command.LayawayIds.Contains(p.Layaway.Id)
                            && userLayaways.Any(l => l.Products.Any(up => up.ProductId == p.ProductId))));

                        foreach (var collection in anonimLayaway.Collections)
                        {
                            var existingLayawayCollection = context.Set<LayawayItemCollection>().FirstOrDefault(p => p.CollectionVarinatId == collection.CollectionVarinatId && command.UserId == p.Layaway.UserId);
                            if (existingLayawayCollection != null)
                            {
                                foreach (var productInCollection in collection.ProductsInCollection)
                                {
                                    var existingLayawayProductInCollection = context.Set<LayawayItemProductInCollection>().FirstOrDefault(p => p.ProductId == productInCollection.ProductId && command.UserId == p.LayawayItemCollection.Layaway.UserId);
                                    if (existingLayawayProductInCollection != null)
                                    {
                                        existingLayawayProductInCollection.Quantity += productInCollection.Quantity;
                                    }
                                }
                                context.Set<LayawayItemProductInCollection>().RemoveRange(collection
                                    .ProductsInCollection
                                    .Where(p => command.LayawayIds
                                    .Contains(p.LayawayItemCollection.Layaway.Id) &&
                                    userLayaways.Any(l => l.Collections
                                        .Any(c => c.ProductsInCollection
                                        .Any(up => up.ProductId == p.ProductId)))));
                            }
                        }
                        if (anonimLayaway.Collections.Any(c => c.ProductsInCollection.Count == 0))
                            context.Set<LayawayItemCollection>().RemoveRange(context.Set<LayawayItemCollection>().Where(l => l.ProductsInCollection.Count == 0));

                        foreach (var complect in anonimLayaway.Complects)
                        {
                            var existingLayawayComplect = context.Set<LayawayItemComplect>().FirstOrDefault(p => p.CollectionVarinatId == complect.CollectionVarinatId && command.UserId == p.Layaway.UserId);
                            if (existingLayawayComplect != null)
                            {
                                existingLayawayComplect.Quantity += complect.Quantity;
                            }
                            context.Set<LayawayItemComplect>().RemoveRange(anonimLayaway.Complects
                           .Where(p => command.LayawayIds.Contains(p.Layaway.Id)
                           && userLayaways.Any(l => l.Complects.Any(up => up.CollectionVarinatId == p.CollectionVarinatId))));
                        }
                    }
                    context.Set<Layaway>().RemoveRange(
                        context.Set<Layaway>().Where(l =>
                        l.Products.Count == 0
                        && l.Collections.Count == 0
                        && l.Complects.Count == 0));
                }
                await context.SaveChangesAsync();
            }
        }
    }
}

