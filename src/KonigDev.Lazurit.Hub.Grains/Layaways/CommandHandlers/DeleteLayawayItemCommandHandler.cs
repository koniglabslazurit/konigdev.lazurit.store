﻿using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Layaways.Command;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace KonigDev.Lazurit.Hub.Grains.Layaways.CommandHandlers
{
    public class DeleteLayawayItemCommandHandler : Grain, IHubCommandHandler<DeleteLayawayItemCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;
        private readonly ITransactionFactory _transactionFactory;

        public DeleteLayawayItemCommandHandler(IDBContextFactory dbContextFactory, ITransactionFactory transactionFactory)
        {
            _dbContextFactory = dbContextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(DeleteLayawayItemCommand command)
        {
            if (command == null)
                return;
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                Layaway layaway = context.Set<Layaway>().FirstOrDefault(x => x.Id == command.LayawayId);
                if (command.LayawayId != Guid.Empty && command.CollectionItemLayawayId == Guid.Empty && command.ProductItemLayawayId == Guid.Empty && command.ComplectItemLayawayId == Guid.Empty)
                {
                    context.Set<Layaway>().Remove(layaway);
                }
                if (command.LayawayId != Guid.Empty && command.CollectionItemLayawayId != Guid.Empty && command.ProductInCollectionId == Guid.Empty && command.ProductItemLayawayId == Guid.Empty && command.ComplectItemLayawayId == Guid.Empty)
                {
                    //TODO NullReference
                    context.Set<LayawayItemCollection>().Remove(layaway.Collections.Where(c => c.Id == command.CollectionItemLayawayId).FirstOrDefault());
                }
                if (command.LayawayId != Guid.Empty && command.CollectionItemLayawayId != Guid.Empty && command.ProductInCollectionId != Guid.Empty && command.ProductItemLayawayId == Guid.Empty && command.ComplectItemLayawayId == Guid.Empty)
                {
                    //TODO NullReference
                    var products = layaway.Collections.Where(c => c.Id == command.CollectionItemLayawayId).FirstOrDefault().ProductsInCollection;
                    context.Set<LayawayItemProductInCollection>().Remove(products.FirstOrDefault(x => x.ProductId == command.ProductInCollectionId));
                }
                if (command.LayawayId != Guid.Empty && command.CollectionItemLayawayId == Guid.Empty && command.ProductItemLayawayId != Guid.Empty && command.ComplectItemLayawayId == Guid.Empty)
                {
                    //TODO NullReference
                    context.Set<LayawayItemProduct>().RemoveRange(layaway.Products.Where(x => x.ProductId == command.ProductItemLayawayId));
                }
                if (command.LayawayId != Guid.Empty && command.CollectionItemLayawayId == Guid.Empty && command.ProductItemLayawayId == Guid.Empty && command.ComplectItemLayawayId != Guid.Empty)
                {
                    //TODO NullReference
                    context.Set<LayawayItemComplect>().RemoveRange(layaway.Complects.Where(x => x.Id == command.ComplectItemLayawayId));
                }
                //TODO NullReference

                if (layaway.Collections.Any(c => c.ProductsInCollection.Count == 0))
                {
                    context.Set<LayawayItemCollection>().RemoveRange(layaway.Collections.Where(c => c.ProductsInCollection.Count == 0));
                }

                if (layaway.Collections.Count == 0 && layaway.Complects.Count == 0 && layaway.Products.Count == 0)
                {
                    context.Set<Layaway>().Remove(layaway);
                }
                await context.SaveChangesAsync();
            }
        }
    }
}
