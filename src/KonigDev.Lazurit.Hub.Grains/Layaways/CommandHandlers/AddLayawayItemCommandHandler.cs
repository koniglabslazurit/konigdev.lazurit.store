﻿using KonigDev.Lazurit.Core.Enums.Constants;
using KonigDev.Lazurit.Core.Enums.Exeptions.Cart;
using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Layaways.Command;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace KonigDev.Lazurit.Hub.Grains.Layaways.CommandHandlers
{
    public class AddLayawayItemCommandHandler : Grain, IHubCommandHandler<AddLayawayItemCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;
        private readonly ITransactionFactory _transactionFactory;

        public AddLayawayItemCommandHandler(IDBContextFactory dbContextFactory, ITransactionFactory transactionFactory)
        {
            _dbContextFactory = dbContextFactory;
            _transactionFactory = transactionFactory;
        }
        public async Task Execute(AddLayawayItemCommand command)
        {
            if (command == null)
                return;
            var layawayId = command.NewLayawayId;
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                Cart cart = context.Set<Cart>().SingleOrDefault(x => x.Id == command.CartId);
                if (cart != null)
                {
                    var layaway = new Layaway
                    {
                        Id = layawayId,
                        UserId = command.UserId,
                        IsWholeCart = command.IsWholeCart,
                        CreationTime = DateTime.Now
                    };
                    context.Set<Layaway>().Add(layaway);
                    foreach (var collection in command.Collections)
                    {
                        var cartCollection = cart.Collections.SingleOrDefault(x => x.Id == collection.CollectionCartItemId);
                        LayawayItemCollection collectionToLayaway;
                        if (cartCollection != null)
                        {
                            if (!command.IsWholeCart)
                            {
                                var existingLayawayCollection = context.Set<LayawayItemCollection>().FirstOrDefault(p => p.CollectionVarinatId == cartCollection.CollectionVariantId && command.LayawayIds.Contains(p.Layaway.Id));
                                if (existingLayawayCollection != null)
                                {
                                    collectionToLayaway = existingLayawayCollection;
                                }
                                else
                                {
                                    collectionToLayaway = new LayawayItemCollection
                                    {
                                        Id = Guid.NewGuid(),
                                        CollectionVarinatId = cartCollection.CollectionVariantId,
                                        LayawayId = layawayId,
                                        Quantity = cartCollection.Quantity
                                    };
                                    layaway.Collections.Add(collectionToLayaway);
                                }
                            }
                            else
                            {
                                collectionToLayaway = new LayawayItemCollection
                                {
                                    Id = Guid.NewGuid(),
                                    CollectionVarinatId = cartCollection.CollectionVariantId,
                                    LayawayId = layawayId,
                                    Quantity = cartCollection.Quantity
                                };
                                layaway.Collections.Add(collectionToLayaway);
                            }

                            foreach (var productInCollection in collection.ProductsInCollection)
                            {
                                var cartProductIncollection = cartCollection.Products.FirstOrDefault(p => p.ProductId == productInCollection.ProductId);
                                if (cartProductIncollection != null)
                                {
                                    context.Set<CartItemProductInCollection>().Remove(cartProductIncollection);
                                }
                                else
                                {
                                    throw new CartNotFound(Exceptions.Cart.CartItemNotFound);
                                }
                                if (!command.IsWholeCart)
                                {
                                    var existingLayawayProductInCollection = context.Set<LayawayItemProductInCollection>().FirstOrDefault(p => p.ProductId == productInCollection.ProductId && command.LayawayIds.Contains(p.LayawayItemCollection.Layaway.Id));
                                    if (existingLayawayProductInCollection != null)
                                    {
                                        existingLayawayProductInCollection.Quantity += productInCollection.Quantity;
                                    }
                                    else
                                    {
                                        var productInCollectionToLayaway = new LayawayItemProductInCollection
                                        {
                                            Id = Guid.NewGuid(),
                                            IsAssemblyRequired = productInCollection.IsAssemblyRequired,
                                            LayawayItemCollectionId = collectionToLayaway.Id,
                                            ProductId = productInCollection.ProductId,
                                            Quantity = productInCollection.Quantity
                                        };
                                        collectionToLayaway.ProductsInCollection.Add(productInCollectionToLayaway);
                                    }
                                }
                                else
                                {
                                    var productInCollectionToLayaway = new LayawayItemProductInCollection
                                    {
                                        Id = Guid.NewGuid(),
                                        IsAssemblyRequired = productInCollection.IsAssemblyRequired,
                                        LayawayItemCollectionId = collectionToLayaway.Id,
                                        ProductId = productInCollection.ProductId,
                                        Quantity = productInCollection.Quantity
                                    };
                                    collectionToLayaway.ProductsInCollection.Add(productInCollectionToLayaway);
                                }
                            }
                            if (cartCollection.Products.Count == 0)
                            {
                                context.Set<CartItemCollection>().Remove(cartCollection);
                            }
                        }
                        else
                        {
                            throw new CartNotFound(Exceptions.Cart.CartItemNotFound);
                        }
                        if (!collectionToLayaway.ProductsInCollection.Any())
                        {
                            context.Set<LayawayItemCollection>().Remove(collectionToLayaway);
                        }
                    }
                    foreach (var product in command.Products)
                    {
                        var cartProduct = cart.Products.FirstOrDefault(x => x.ProductId == product.ProductId);
                        if (cartProduct != null)
                        {
                            context.Set<CartItemProduct>().Remove(cartProduct);
                        }
                        else
                        {
                            throw new CartNotFound(Exceptions.Cart.CartItemNotFound);
                        }
                        if (!command.IsWholeCart)
                        {
                            var existingLayawayProduct = context.Set<LayawayItemProduct>().FirstOrDefault(p => p.ProductId == product.ProductId && command.LayawayIds.Contains(p.Layaway.Id));
                            if (existingLayawayProduct != null)
                            {
                                existingLayawayProduct.Quantity += product.Quantity;
                            }
                            else
                            {
                                var productToLayaway = new LayawayItemProduct
                                {
                                    Id = Guid.NewGuid(),
                                    IsAssemblyRequired = product.IsAssemblyRequired,
                                    ProductId = product.ProductId,
                                    LayawayId = layawayId,
                                    Quantity = product.Quantity
                                };
                                layaway.Products.Add(productToLayaway);
                            }
                        }
                        else
                        {
                            var productToLayaway = new LayawayItemProduct
                            {
                                Id = Guid.NewGuid(),
                                IsAssemblyRequired = product.IsAssemblyRequired,
                                ProductId = product.ProductId,
                                LayawayId = layawayId,
                                Quantity = product.Quantity
                            };
                            layaway.Products.Add(productToLayaway);
                        }
                    }
                    foreach (var complect in command.Complects)
                    {
                        var cartComplect = cart.Complects.FirstOrDefault(x => x.CollectionVariantId == complect.CollectionVariantId);
                        if (cartComplect != null)
                        {
                            context.Set<CartItemComplect>().Remove(cartComplect);
                        }
                        else
                        {
                            throw new CartNotFound(Exceptions.Cart.CartItemNotFound);
                        }
                        if (!command.IsWholeCart)
                        {
                            var existingLayawayComplect = context.Set<LayawayItemComplect>().FirstOrDefault(p => p.CollectionVarinatId == complect.CollectionVariantId && command.LayawayIds.Contains(p.Layaway.Id));
                            if (existingLayawayComplect != null)
                            {
                                existingLayawayComplect.Quantity += complect.Quantity;
                            }
                            else
                            {
                                var complectToLayaway = new LayawayItemComplect
                                {
                                    Id = Guid.NewGuid(),
                                    CollectionVarinatId = complect.ItemId,
                                    IsAssemblyRequired = complect.IsAssemblyRequired,
                                    LayawayId = layawayId,
                                    Quantity = complect.Quantity
                                };
                                layaway.Complects.Add(complectToLayaway);
                            }
                        }
                        else
                        {
                            var complectToLayaway = new LayawayItemComplect
                            {
                                Id = Guid.NewGuid(),
                                CollectionVarinatId = complect.ItemId,
                                IsAssemblyRequired = complect.IsAssemblyRequired,
                                LayawayId = layawayId,
                                Quantity = complect.Quantity
                            };
                            layaway.Complects.Add(complectToLayaway);
                        }
                    }
                    if (!layaway.Collections.Any() && !layaway.Complects.Any() && !layaway.Products.Any())
                    {
                        context.Set<Layaway>().Remove(layaway);
                    }
                    await context.SaveChangesAsync();
                }
                else
                {
                    throw new CartNotFound(Exceptions.Cart.CartNotFound);
                }
            }
        }
    }
}
