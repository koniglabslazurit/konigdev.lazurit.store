﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Seriess.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Seriess.Queries;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.SeriesGrains.CommandHandlers
{
    public class GetSeriesByRequestQueryHandler : Grain, IHubQueryHandler<GetSeriesByRequestQuery, List<DtoSeriesItem>>
    {
        private readonly IDBContextFactory _contextFactory;

        public GetSeriesByRequestQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }
     
        public Task<List<DtoSeriesItem>> Execute(GetSeriesByRequestQuery query)
        {
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var result = context.Set<Series>()
                    .Select(p => new DtoSeriesItem
                    {
                        Id = p.Id,
                        SyncCode1C = p.SyncCode1C,
                        Alias = p.Alias,
                        Name = p.Name
                    }).ToList();
                return Task.FromResult(result);
            }
        }
    }
}
