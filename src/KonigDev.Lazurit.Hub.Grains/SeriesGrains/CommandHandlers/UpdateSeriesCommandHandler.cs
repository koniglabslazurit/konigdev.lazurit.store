﻿using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using Orleans;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Transactions;
using KonigDev.Lazurit.Hub.Model.DTO.Seriess.Commands;

namespace KonigDev.Lazurit.Hub.Grains.SeriesGrains.CommandHandlers
{
    public class UpdateSeriesCommandHandler : Grain, IHubCommandHandler<UpdateSeriesCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;

        public UpdateSeriesCommandHandler(IDBContextFactory contextFactory, ITransactionFactory transactionFactory)
        {
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(UpdateSeriesCommand command)
        {
            var query = "";
            foreach (var item in command.Series)
            {
                query += $"UPDATE Series SET Range = '{item.RangeSeries}' WHERE Id = '{item.Id}';";
            }

            using (IDbConnection connection = _contextFactory.CreateDbConnection("LazuritBaseContext"))
            {
                connection.Open();

                TransactionOptions to = new TransactionOptions();
                to.IsolationLevel = System.Transactions.IsolationLevel.Snapshot;
                using (var transaction = _transactionFactory.CreateScope(TransactionScopeOption.Required, to))
                {
                    try
                    {
                        connection.Execute(query, transaction);
                        transaction.Complete();
                    }
                    catch (System.Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }
    }
}
