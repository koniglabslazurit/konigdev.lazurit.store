﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Grains.Users.Exceptions;
using KonigDev.Lazurit.Hub.Model.DTO.Users;
using KonigDev.Lazurit.Hub.Model.DTO.Users.Queries;
using KonigDev.Lazurit.Hub.Model.Mappings.User;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Users.QueryHandlers
{
public class GetUserProfileByLinkIdQueryHandler : Grain, IHubQueryHandler<GetUserProfileByLinkIdQuery, DtoUserProfile>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetUserProfileByLinkIdQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<DtoUserProfile> Execute(GetUserProfileByLinkIdQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var user = context.Set<UserProfile>().SingleOrDefault(x => x.LinkId == query.LinkId);
                if (user == null)
                    throw new UserNotFoundException($"Пользователь с LinkId={query.LinkId} не найден.");
                return Task.FromResult(user.UserMapToDTOProfile());
            }
        }
    }
}
