﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Grains.ShowRooms.Exceptions;
using KonigDev.Lazurit.Hub.Model.DTO.Users;
using KonigDev.Lazurit.Hub.Model.DTO.Users.Queries;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using KonigDev.Lazurit.Hub.Model.Mappings.User;

namespace KonigDev.Lazurit.Hub.Grains.Users.QueryHandlers
{
    public class GetUsersByShowRoomIdQueryHandler : Grain, IHubQueryHandler<GetUsersByShowRoomIdQuery, List<DtoUserProfile>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetUsersByShowRoomIdQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoUserProfile>> Execute(GetUsersByShowRoomIdQuery query)
        {
            using (var context= _dbContextFactory.CreateLazuritContext())
            {
                var usersInShowRoom= context.Set<Showroom>().Include(x=>x.Users).SingleOrDefault(x=>x.Id== query.ShowRoomId)?.Users.ToList();
                if (usersInShowRoom==null)
                    throw new ShowRoomNotFoundException($"Магазин с id={query.ShowRoomId} не найден.");
                return Task.FromResult(usersInShowRoom.Select(x => x.UserMapToDTOProfile()).ToList()) ;
            }
        }
    }
}
