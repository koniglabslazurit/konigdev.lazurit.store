﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Users;
using KonigDev.Lazurit.Hub.Model.DTO.Users.Queries;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.Mappings.User;
using KonigDev.Lazurit.Hub.Grains.Users.Exceptions;

namespace KonigDev.Lazurit.Hub.Grains.Users.QueryHandlers
{
    public class GetUserProfileByIdQueryHandler : Grain, IHubQueryHandler<GetUserProfileByIdQuery, DtoUserProfile>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetUserProfileByIdQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<DtoUserProfile> Execute(GetUserProfileByIdQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var user = context.Set<UserProfile>().SingleOrDefault(x => x.Id == query.UserId);
                if (user == null)
                    throw new UserNotFoundException($"Пользователь с id={query.UserId} не найден.");
                return Task.FromResult(user.UserMapToDTOProfile());
            }
        }
    }
}