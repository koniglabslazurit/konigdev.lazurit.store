﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Users;
using KonigDev.Lazurit.Hub.Model.DTO.Users.Region.Queries;
using KonigDev.Lazurit.Model.Entities;
using KonigDev.Lazurit.Hub.Model.Mappings.City;

namespace KonigDev.Lazurit.Hub.Grains.Users.Region.QueryHandlers
{
    public class GetCitiesByRegionIdQueryHandler : IHubQueryHandler<GetCitiesByRegionIdQuery, List<DTOCityResult>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetCitiesByRegionIdQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DTOCityResult>> Execute(GetCitiesByRegionIdQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var cities = context.Set<City>().Where(x => x.RegionId == query.RegionId).ToList();
                return Task.FromResult(cities.Select(x => x.MapToDTOCityResult()).ToList());
            }
        }
    }
}
