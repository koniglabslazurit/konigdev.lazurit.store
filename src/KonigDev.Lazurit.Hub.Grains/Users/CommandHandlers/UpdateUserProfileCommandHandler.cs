﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Users.Commands;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Users.CommandHandlers
{
    public class UpdateUserProfileCommandHandler : Grain, IHubCommandHandler<UpdateUserProfileCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public UpdateUserProfileCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task Execute(UpdateUserProfileCommand command)
        {
            if (command == null || command.Id == Guid.Empty)
                return;
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                UserProfile user = context.Set<UserProfile>().FirstOrDefault(x => x.Id == command.Id);
                user.FirstName = command.FirstName;
                user.MiddleName = command.MiddleName;
                user.LastName = command.LastName;
                user.SyncCode1C = command.SyncCode1C;
                user.Title = command.Title;
                //address info updating
                var deletingAdressess = user.UserAddresses.Where(a => command.UserAddresses.Any(c => c.Id != a.Id) || command.UserAddresses.Count == 0);
                context.Set<UserAddress>().RemoveRange(deletingAdressess);
                foreach (var address in command.UserAddresses)
                {
                    var userAddress = user.UserAddresses.FirstOrDefault(a => a.Id == address.Id);
                    if (userAddress == null)
                    {
                        userAddress = new UserAddress
                        {
                            Id = Guid.NewGuid(),
                            UserProfileId = user.Id,
                            Address = address.Address,
                            City = address.City,
                            Street = address.Street,
                            Block = address.Block,
                            Building = address.Building,
                            House = address.House,
                            EntranceHall = address.EntranceHall,
                            Floor = address.Floor,
                            Apartment = address.Apartment,
                            KladrCode = address.KladrCode,
                            IsDefault = !context.Set<UserAddress>().Where(x => x.UserProfileId == user.Id && x.IsDefault == true).Any()
                        };
                        context.Set<UserAddress>().Add(userAddress);
                    }
                    else
                    {
                        userAddress.Address = address.Address;
                        userAddress.City = address.City;
                        userAddress.Street = address.Street;
                        userAddress.Block = address.Block;
                        userAddress.Building = address.Building;
                        userAddress.House = address.House;
                        userAddress.EntranceHall = address.EntranceHall;
                        userAddress.Floor = address.Floor;
                        userAddress.Apartment = address.Apartment;
                        userAddress.KladrCode = address.KladrCode;
                        if (!userAddress.IsDefault)
                        {
                            userAddress.IsDefault = !context.Set<UserAddress>().Where(x => x.UserProfileId == user.Id).Any();
                        }
                    }
                }
                //phones info updating
                var deletingPhones = user.UserPhones.Where(a => command.UserPhones.Any(c => c.Id != a.Id) || command.UserPhones.Count == 0);
                context.Set<UserPhone>().RemoveRange(deletingPhones);
                foreach (var phone in command.UserPhones)
                {
                    var userPhone = user.UserPhones.FirstOrDefault(a => a.Id == phone.Id);
                    if (userPhone == null)
                    {
                        userPhone = new UserPhone
                        {
                            Id = Guid.NewGuid(),
                            UserProfileId = user.Id,
                            Phone = phone.Phone,
                            IsDefault = !context.Set<UserPhone>().Where(x => x.UserProfileId == user.Id).Any()
                        };
                        context.Set<UserPhone>().Add(userPhone);
                    }
                    else
                    {
                        userPhone.Phone = phone.Phone;
                        if (!userPhone.IsDefault)
                        {
                            userPhone.IsDefault = !context.Set<UserPhone>().Where(x => x.UserProfileId == user.Id && x.IsDefault == true).Any();
                        }
                    }
                }
                await context.SaveChangesAsync();
            }
        }
    }
}
