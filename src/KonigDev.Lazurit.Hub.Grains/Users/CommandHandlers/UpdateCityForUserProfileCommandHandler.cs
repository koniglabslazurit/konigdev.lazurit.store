﻿using KonigDev.Lazurit.Core.Enums.Exeptions.Common;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Users.Commands;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Users.CommandHandlers
{
    public class UpdateCityForUserProfileCommandHandler: Grain, IHubCommandHandler<UpdateCityUserProfile>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public UpdateCityForUserProfileCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task Execute(UpdateCityUserProfile command)
        {
            if (command == null)
                throw new NotValidCommandException(Core.Enums.Constants.Exceptions.Common.ModelEmpty);
            if (command.UserId==null || command.UserId == Guid.Empty)
                throw new NotValidCommandException(Core.Enums.Constants.Exceptions.User.UserIsEmpty);
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var user = context.Set<UserProfile>().SingleOrDefault(x => x.Id == command.UserId);
                user.CityId = command.CityId;
                await context.SaveChangesAsync();
            }
        }
    }
}
