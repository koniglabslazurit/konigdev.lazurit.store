﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Users.Commands;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Users.CommandHandlers
{
   public class AddUserProfileCommandHandler : Grain, IHubCommandHandler<AddUserCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public AddUserProfileCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task Execute(AddUserCommand command)
        {
            if (command == null)
                return;
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                UserProfile user = new UserProfile {Id = command.Id };
                user.FirstName = command.FirstName;
                user.MiddleName = command.MiddleName;
                user.LastName = command.LastName;
                user.SyncCode1C = command.SyncCode1C;
                user.Email = command.Email;
                user.Title = command.Title;
                foreach (var address in command.UserAddresses)
                {
                    UserAddress userAddress = new UserAddress { Id = Guid.NewGuid(), UserProfileId = user.Id };
                    userAddress.Address = address.Address;
                    userAddress.City = address.City;
                    userAddress.Street = address.Street;
                    userAddress.Block = address.Block;
                    userAddress.Building = address.Building;
                    userAddress.House = address.House;
                    userAddress.EntranceHall = address.EntranceHall;
                    userAddress.Floor = address.Floor;
                    userAddress.Apartment = address.Apartment;
                    userAddress.IsDefault = address.IsDefault;
                    userAddress.KladrCode = address.KladrCode;
                    context.Set<UserAddress>().Add(userAddress);
                }
                var deletingAdressess = user.UserAddresses.Where(a => command.UserAddresses.Any(c => c.Id != a.Id));
                context.Set<UserAddress>().RemoveRange(deletingAdressess);
                foreach (var phone in command.UserPhones)
                {
                    UserPhone userPhone = new UserPhone { Id = Guid.NewGuid(), UserProfileId = user.Id };                        
                    userPhone.Phone = phone.Phone;
                    userPhone.IsDefault = phone.IsDefault;
                    context.Set<UserPhone>().Add(userPhone);
                }
                var deletingPhones = user.UserPhones.Where(a => command.UserPhones.Any(c => c.Id != a.Id));
                context.Set<UserPhone>().RemoveRange(deletingPhones);
                context.Set<UserProfile>().Add(user);

                await context.SaveChangesAsync();
            }
        }
    }
}
