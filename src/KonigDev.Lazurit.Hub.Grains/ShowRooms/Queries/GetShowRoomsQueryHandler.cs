﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using KonigDev.Lazurit.Hub.Model.DTO.ShowRooms.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.ShowRooms.Query;

namespace KonigDev.Lazurit.Hub.Grains.ShowRooms.Queries
{
    /// <summary>
    /// Получение списка всех магазинов
    /// </summary>
    public class GetShowRoomsQueryHandler : Grain, IHubQueryHandler<GetShowRoomsQuery, List<DtoShowRoomItem>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetShowRoomsQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoShowRoomItem>> Execute(GetShowRoomsQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var result = context.Set<Showroom>()
                    .Select(p => new DtoShowRoomItem
                    {
                        Id = p.Id,
                        SyncCode1C = p.SyncCode1C
                    }).ToList();

                return Task.FromResult(result);
            }
        }
    }
}
