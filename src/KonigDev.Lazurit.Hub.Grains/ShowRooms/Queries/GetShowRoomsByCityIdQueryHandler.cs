﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.ShowRooms;
using KonigDev.Lazurit.Hub.Model.DTO.ShowRooms.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using KonigDev.Lazurit.Hub.Model.Mappings.ShowRooms;

namespace KonigDev.Lazurit.Hub.Grains.ShowRooms.Queries
{
    public class GetShowRoomsByCityIdQueryHandler : Grain, IHubQueryHandler<GetShowRoomsByCityIdQuery, List<DtoShowRoomResult>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetShowRoomsByCityIdQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoShowRoomResult>> Execute(GetShowRoomsByCityIdQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var showRooms = context.Set<Showroom>().Where(x => x.CityId == query.CityId).ToList();
                return Task.FromResult(showRooms.Select(x => x.MapToDto()).ToList());
            }
        }
    }
}
