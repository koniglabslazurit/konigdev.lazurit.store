﻿using System;

namespace KonigDev.Lazurit.Hub.Grains.ShowRooms.Exceptions
{
    public class ShowRoomNotFoundException:Exception
    {
        public ShowRoomNotFoundException(string message):base(message)
        {
        }
    }
}
