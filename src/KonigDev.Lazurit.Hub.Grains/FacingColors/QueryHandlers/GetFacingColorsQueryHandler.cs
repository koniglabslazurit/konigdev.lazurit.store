﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Model.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using Orleans;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Query;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Dto;

namespace KonigDev.Lazurit.Hub.Grains.FacingColors.QueryHandlers
{
    public class GetFacingColorsQueryHandler : Grain, IHubQueryHandler<GetFacingColorQuery, List<DtoFacingColorItem>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetFacingColorsQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoFacingColorItem>> Execute(GetFacingColorQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var result = context.Set<FacingColor>()
                    .Select(p => new DtoFacingColorItem
                    {
                        Id = p.Id,
                        Name = p.Name,
                        SyncCode1C = p.SyncCode1C
                    }).ToList();

                return Task.FromResult(result);
            }
        }
    }
}