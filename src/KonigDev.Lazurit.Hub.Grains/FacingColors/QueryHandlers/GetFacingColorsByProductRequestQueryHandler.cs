﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Filters.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.FacingColors.QueryHandlers
{
    public class GetFacingColorsByProductRequestQueryHandler : Grain, IHubQueryHandler<GetFiltersByProductRequestQuery, List<DtoFacingColorItem>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetFacingColorsByProductRequestQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoFacingColorItem>> Execute(GetFiltersByProductRequestQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var products = context.Set<Product>().AsQueryable();
                /* фильтр по варианту коллекции */
                if (query.CollectionVariantId.HasValue)
                    products = products.Where(p => p.CollectionsVariants.Any(c => c.Id == query.CollectionVariantId));
                /* фильтр по коллекции */
                if (query.CollectionId.HasValue)
                    products = products.Where(p => p.CollectionId == query.CollectionId); /* todo изменение выборки для коллекции напрямую, минуя вариант Было: products = products.Where(p => p.CollectionsVariants.Any(c => c.CollectionId == query.CollectionId));*/

                /* TODO отдавать еще продукты, у которых отсутствует один из цветов, но совпадают два других */
                /* фильтр по цвету отделки.  */
                if (query.FacingColorIds != null && query.FacingColorIds.Any())
                    products = products.Where(p => query.FacingColorIds.Any(f => f.Value == p.FacingColorId) || p.FacingColor.IsUniversal || p.FacingColorId == null);

                if (query.FacingIds != null && query.FacingIds.Any())
                    products = products.Where(p => query.FacingIds.Any(f => f.Value == p.FacingId));

                if (query.FrameColorIds != null && query.FrameColorIds.Any())
                    products = products.Where(p => query.FrameColorIds.Any(f => f.Value == p.FrameColorId));

                if (query.FurnitureTypeId.HasValue)
                    products = products.Where(p => p.Article.FurnitureTypeId == query.FurnitureTypeId);

                if (!string.IsNullOrWhiteSpace(query.FurnitureAlias))
                {
                    var furnitureType = context.Set<FurnitureType>().FirstOrDefault(f => f.Alias == query.FurnitureAlias);
                    if (furnitureType != null)
                    {
                        if (furnitureType.Id == furnitureType.ParentId)
                        {
                            products = products.Where(p => p.Article.FurnitureType.ParentId == furnitureType.ParentId);
                        }
                        else
                        {
                            products = products.Where(p => p.Article.FurnitureType.Id == furnitureType.Id);
                        }
                    }
                }

                var facingColors = products
                    .Where(p=>p.FacingColorId.HasValue)
                    .Select(p => new { p.FacingColor.Id, p.FacingColor.Name, p.FacingColor.SyncCode1C })
                    .Distinct()
                    .Select(p => new DtoFacingColorItem
                    {
                        Id = p.Id,
                        Name = p.Name,
                        SyncCode1C = p.SyncCode1C
                    }).ToList();

                return Task.FromResult(facingColors);
            }
        }
    }
}
