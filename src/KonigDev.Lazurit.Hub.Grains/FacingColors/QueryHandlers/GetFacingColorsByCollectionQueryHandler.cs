﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.FacingColors.QueryHandlers
{
    /// <summary>
    /// 
    /// </summary>
    public class GetFacingColorsByCollectionQueryHandler : Grain, IHubQueryHandler<GetFacingColorsByCollectionQuery, List<DtoFacingColorResult>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetFacingColorsByCollectionQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public Task<List<DtoFacingColorResult>> Execute(GetFacingColorsByCollectionQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var facingColorsList = new List<DtoFacingColorResult>();      
                if (query.CollectionId.HasValue)
                {
                    var facingColorsByCollectionId = context.Set<FacingColor>().Where(p => p.CollectionsVariants.Any(s => s.CollectionId == query.CollectionId))
                        .Select(p => new DtoFacingColorResult
                        {
                            Id = p.Id,
                            Name = p.Name
                        });
                    facingColorsList.AddRange(facingColorsByCollectionId);
                }
                return Task.FromResult(facingColorsList.Distinct().ToList());
            }
        }
    }
}
