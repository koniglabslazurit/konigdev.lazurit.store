﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.FacingColors.QueryHandlers
{
   public class GetFacingColorsByRoomTypeIdQueryHandler : Grain, IHubQueryHandler<GetFacingColorsByRoomTypeIdQuery, List<DtoFacingColorResult>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetFacingColorsByRoomTypeIdQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoFacingColorResult>> Execute(GetFacingColorsByRoomTypeIdQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var collectionIds = context.Set<Collection>().Where(p => p.RoomTypeId == query.RoomTypeId).Select(p => p.Id).ToArray();
                var facingColorsList = new List<DtoFacingColorResult>();
              
                foreach (var collectionId in collectionIds)
                {
                    var facingColors = context.Set<FacingColor>()
                        .Where(p => p.CollectionsVariants.Any(s => s.Id == collectionId))
                        .Select(p => new DtoFacingColorResult
                        {
                            Id = p.Id,
                            Name = p.Name
                        }).ToList();
                    facingColorsList.AddRange(facingColors);
                }
                return Task.FromResult(facingColorsList.Distinct().ToList());
            }
        }
    }
}
