﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Commands;
using Orleans;
using System.Threading.Tasks;
using KonigDev.Lazurit.Core.Transactions;
using System.Transactions;
using System.Data;
using Dapper;
using System;

namespace KonigDev.Lazurit.Hub.Grains.FacingColors.CommandHandlers
{
    public class CreateFacingColorsCommandHandler : Grain, IHubCommandHandler<CreateFacingColorsCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;
        public CreateFacingColorsCommandHandler(IDBContextFactory contextFactory, ITransactionFactory transactionFactory)
        {
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }
        public async Task Execute(CreateFacingColorsCommand command)
        {
            var query = "";
            foreach (var item in command.FacingColors)
            {
                query += $" INSERT INTO FacingsColors(Id,Name,SyncCode1C,IsUniversal) VALUES('{item.Id}',N'{item.Name}',N'{item.SyncCode1C}', {Convert.ToInt32(item.IsUniversal)} );";
            }

            using (IDbConnection connection = _contextFactory.CreateDbConnection("LazuritBaseContext"))
            {
                connection.Open();
                TransactionOptions to = new TransactionOptions();
                to.IsolationLevel = System.Transactions.IsolationLevel.Snapshot;
                using (var transaction = _transactionFactory.CreateScope(TransactionScopeOption.Required, to))
                {
                    try
                    {
                        connection.Execute(query, transaction);
                        transaction.Complete();
                    }
                    catch (System.Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }
    }
}
