﻿using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Commands;
using Orleans;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Transactions;

namespace KonigDev.Lazurit.Hub.Grains.RoomTypes.CommandHandlers
{
    public class UpdateRoomTypeCommandHandler : Grain, IHubCommandHandler<UpdateRoomTypesCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;

        public UpdateRoomTypeCommandHandler(IDBContextFactory contextFactory, ITransactionFactory transactionFactory)
        {
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(UpdateRoomTypesCommand command)
        {
            var query = "";
            foreach (var item in command.RoomTypes)
            {
                query += $"UPDATE RoomsTypes SET Range = '{item.RangeRoom}' WHERE Id = '{item.Id}';";
            }

            using (IDbConnection connection = _contextFactory.CreateDbConnection("LazuritBaseContext"))
            {
                connection.Open();

                TransactionOptions to = new TransactionOptions();
                to.IsolationLevel = System.Transactions.IsolationLevel.Snapshot;
                using (var transaction = _transactionFactory.CreateScope(TransactionScopeOption.Required, to))
                {
                    try
                    {
                        connection.Execute(query, transaction);
                        transaction.Complete();
                    }
                    catch (System.Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }
    }
}