﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Grains.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes;
using KonigDev.Lazurit.Model.Entities;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Collections;
using Orleans;
using KonigDev.Lazurit.Hub.Model.DTO.Styles.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Dto;

namespace KonigDev.Lazurit.Hub.Grains.RoomTypes.QueryHandlers
{
    public class GetRoomTypesQueryHandler : Grain, IHubQueryHandler<GetRoomTypesQuery, List<DtoRoomTypesResult>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetRoomTypesQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoRoomTypesResult>> Execute(GetRoomTypesQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var roomTypes = context.Set<RoomType>()
                    .Select(rt => new DtoRoomTypesResult
                    {
                        Id = rt.Id,
                        Name = rt.Name,
                        Alias = rt.Alias,
                        Range = rt.Range,
                        Collections = rt.Collections.Select(c => new DtoCollectionsResult
                        {
                            Id = c.Id,
                            Name = c.RoomType.Name + " " + c.Series.Name,
                            SeriesId = c.SeriesId,
                            RoomTypeId = c.RoomTypeId,
                            DefaultVariantId = c.CollectionVariants.Any(cv=>cv.IsDefault) 
                                ? c.CollectionVariants.FirstOrDefault(cv=>cv.IsDefault).Id
                                : c.CollectionVariants.Any() 
                                    ? c.CollectionVariants.Select(cv => cv.Id).FirstOrDefault()
                                    : Guid.Empty,
                            Alias = c.Series.Alias,
                            Range = c.Range,
                            Styles = c.CollectionVariants.SelectMany(cv => cv.Products).SelectMany(s => s.Styles)
                            .Select(s => new { s.Id, s.Name, s.SyncCode1C })
                            .Distinct()
                            .Select(a => new DtoStyleItem
                            {
                                Id = a.Id,
                                Name = a.Name,
                                SyncCode1C = a.SyncCode1C
                            }),
                            TargetAudiences = c.CollectionVariants.SelectMany(cv => cv.Products).SelectMany(ta => ta.TargetAudiences)
                            .Select(ta => new { ta.Id, ta.Name, ta.SyncCode1C })
                            .Distinct()
                            .Select(a => new DtoTargetAudienceItem
                            {
                                Id = a.Id,
                                Name = a.Name,
                                SyncCode1C = a.SyncCode1C
                            })
                        }).OrderByDescending(c => c.Range)
                    }).OrderByDescending(rt => rt.Range);

                return Task.FromResult(roomTypes.ToList());
            }
        }
    }
}