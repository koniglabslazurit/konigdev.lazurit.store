﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.RoomTypes.QueryHandlers
{
    public class GetRoomTypesByProductRequestQueryHandler : Grain, IHubQueryHandler<GetRoomTypesByProductRequestQuery, List<DtoRoomByProductItem>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetRoomTypesByProductRequestQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoRoomByProductItem>> Execute(GetRoomTypesByProductRequestQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var products = context.Set<Product>().AsQueryable();

                /* фильтр по варианту коллекции */
                if (query.CollectionVariantId.HasValue)
                    products = products.Where(p => p.CollectionsVariants.Any(c => c.Id == query.CollectionVariantId));
                /* фильтр по коллекции */
                if (query.CollectionId.HasValue)
                    products = products.Where(p => p.CollectionId == query.CollectionId);

                /* TODO отдавать еще продукты, у которых отсутствует один из цветов, но совпадают два других */
                /* фильтр по цвету отделки.  */
                if (query.FacingColorIds != null && query.FacingColorIds.Any())
                    products = products.Where(p => query.FacingColorIds.Any(f => f.Value == p.FacingColorId) || p.FacingColor.IsUniversal || p.FacingColorId == null);

                if (query.FacingIds != null && query.FacingIds.Any())
                    products = products.Where(p => query.FacingIds.Any(f => f.Value == p.FacingId));

                if (query.FrameColorIds != null && query.FrameColorIds.Any())
                    products = products.Where(p => query.FrameColorIds.Any(f => f.Value == p.FrameColorId));

                if (query.FurnitureTypeId.HasValue)
                    products = products.Where(p => p.Article.FurnitureTypeId == query.FurnitureTypeId);

                if (!string.IsNullOrWhiteSpace(query.FurnitureAlias))
                    products = products.Where(p => p.Article.FurnitureType.Alias == query.FurnitureAlias);

                var roomTypes = products.SelectMany(p => p.CollectionsVariants).Select(c => new DtoRoomByProductItem
                {
                    Alias = c.Collection.RoomType.Alias,
                    Id = c.Collection.RoomType.Id,
                    Name = c.Collection.RoomType.Name
                }).Distinct().ToList();

                return Task.FromResult(roomTypes);
            }
        }
    }
}
