﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.RoomTypes.QueryHandlers
{
    public class GetRoomsQueryHandler : Grain, IHubQueryHandler<GetRoomsQuery, List<DtoRoomItem>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetRoomsQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoRoomItem>> Execute(GetRoomsQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var result = context.Set<RoomType>()
                    .Select(p => new DtoRoomItem
                    {
                        Id = p.Id,
                        SyncCode1C = p.SyncCode1C
                    }).ToList();

                return Task.FromResult(result);
            }
        }
    }
}
