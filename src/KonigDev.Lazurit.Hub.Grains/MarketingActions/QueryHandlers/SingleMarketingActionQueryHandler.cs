﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.MarketingActions;
using KonigDev.Lazurit.Hub.Model.DTO.MarketingActions.Queries;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.MarketingActions.QueryHandlers
{
    public class SingleMarketingActionQueryHandler : Grain, IHubQueryHandler<SingleMarketingActionQuery, DtoMarketingAction>
    {
        private readonly IDBContextFactory _dbContextFactory;
        public SingleMarketingActionQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public Task<DtoMarketingAction> Execute(SingleMarketingActionQuery query)
        {
            return Task.Run(() =>
            {
                using (var context = _dbContextFactory.CreateLazuritContext())
                {
                    var result = context.Set<MarketingAction>()
                    .Where(x => x.SeoId == query.ActionSeoId)
                        .Select(x => new DtoMarketingAction
                        {
                            Id = x.Id,
                            SeoId = x.SeoId,
                            Discount = x.Discount,
                            Name = x.Name,
                            Code = x.Code,
                            Description = x.Description,
                            IsVisible = x.IsVisible,
                            FileId = x.FileId
                        });

                    return result.FirstOrDefault();
                }
            });
        }
    }
}
