﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.MarketingActions;
using KonigDev.Lazurit.Hub.Model.DTO.MarketingActions.Queries;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.MarketingActions.QueryHandlers
{
    public class AllMarketingActionsQueryHandler : Grain, IHubQueryHandler<AllMarketingActionsQuery, DtoMarketingActions>
    {
        private readonly IDBContextFactory _dbContextFactory;
        public AllMarketingActionsQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public Task<DtoMarketingActions> Execute(AllMarketingActionsQuery query)
        {
            return Task.Run(() =>
            {
                using (var context = _dbContextFactory.CreateLazuritContext())
                {
                    var skip = (query.PageNumber - 1) * query.Take;
                    var dbQuery = context.Set<MarketingAction>().AsQueryable();
                    dbQuery = FilterByVisibility(dbQuery, query);
                    dbQuery = dbQuery.OrderBy(x => x.CreatedOn);

                    var result = dbQuery
                        .Skip(skip)
                        .Take(query.Take)
                        .Select(x => new DtoMarketingAction
                        {
                            Id = x.Id,
                            SeoId = x.SeoId,
                            Discount = x.Discount,
                            Name = x.Name,
                            Code = x.Code,
                            Description = x.Description,
                            IsVisible = x.IsVisible,
                            FileId = x.FileId,
                            CreatedOn = x.CreatedOn
                        });
                        

                    return new DtoMarketingActions
                    {
                        Total = dbQuery.Count(),
                        Collection = result.ToList()
                    };
                }
            });
        }

        private IQueryable<MarketingAction> FilterByVisibility(IQueryable<MarketingAction> currentQuery, AllMarketingActionsQuery query)
        {
            if (!query.IsActive.HasValue)
            {
                return currentQuery;
            }

            return currentQuery.Where(x => x.IsVisible == query.IsActive);
        }
    }
}
