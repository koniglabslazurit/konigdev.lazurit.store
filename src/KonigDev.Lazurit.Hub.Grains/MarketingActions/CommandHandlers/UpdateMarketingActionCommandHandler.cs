﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.MarketingActions.Commands;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.MarketingActions.CommandHandlers
{
    public class UpdateMarketingActionCommandHandler : Grain, IHubCommandHandler<UpdateMarketingActionCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;
        public UpdateMarketingActionCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public async Task Execute(UpdateMarketingActionCommand command)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var current = context.Set<MarketingAction>().Find(command.Action.Id);
                if(current == null)
                {
                    throw new ArgumentException($"Unable to update MarketingAction with id: {command.Action.Id} - Entity doesn't exists.");
                }

                current.Discount = command.Action.Discount;
                current.Name = command.Action.Name;
                current.Code = command.Action.Code;
                current.Description = command.Action.Description;
                current.IsVisible = command.Action.IsVisible;
                current.FileId = command.Action.FileId;

                await context.SaveChangesAsync();
            }
        }
    }
}
