﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.MarketingActions.Commands;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.MarketingActions.CommandHandlers
{
    public class DeleteMarketingActionCommandHandler : Grain, IHubCommandHandler<DeleteMarketingActionCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;
        public DeleteMarketingActionCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public async Task Execute(DeleteMarketingActionCommand command)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var current = context.Set<MarketingAction>().Find(command.ActionId);
                if(current == null)
                {
                    return;
                }

                context.Set<MarketingAction>().Remove(current);
                await context.SaveChangesAsync();
            }
        }
    }
}
