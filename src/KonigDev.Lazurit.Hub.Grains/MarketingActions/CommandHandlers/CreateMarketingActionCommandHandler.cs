﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.MarketingActions.Commands;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.MarketingActions.CommandHandlers
{
    public class CreateMarketingActionCommandHandler : Grain, IHubCommandHandler<CreateMarketingActionCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;
        public CreateMarketingActionCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public async Task Execute(CreateMarketingActionCommand command)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var action = new MarketingAction
                {
                    Id = command.Action.Id,
                    Discount = command.Action.Discount,
                    Name = command.Action.Name,
                    Code = command.Action.Code,
                    Description = command.Action.Description,
                    IsVisible = command.Action.IsVisible,
                    FileId = command.Action.FileId,
                    CreatedOn = DateTime.Now
                };
                context.Set<MarketingAction>().Add(action);

                await context.SaveChangesAsync();
            }
        }
    }
}
