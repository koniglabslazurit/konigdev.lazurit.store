﻿using System.Configuration;
using System.Data;
using KonigDev.Lazurit.Model.Context.DataContext;
using System.Data.Entity;
using System.Data.SqlClient;
using System;
using KonigDev.Lazurit.Store.Integration.Context.DataContext;

namespace KonigDev.Lazurit.Hub.Grains.Factories
{
    public class DBContextFactory: IDBContextFactory
    {
        private readonly string _connectionStringName;

        public DBContextFactory(string connectionStringName)
        {
            _connectionStringName = connectionStringName;
        }

        public DbContext CreateLazuritContext()
        {
            return new LazuritBaseContext();
        }

        public IDbConnection CreateIdentityConnection()
        {
            return new SqlConnection(ConfigurationManager.ConnectionStrings[_connectionStringName].ConnectionString);
        }

        public DbContext CreateLazuritIntegrationContext()
        {
            return new LazuritIntegrationContext();
        }

        public IDbConnection CreateDbConnection(string connectionString)
        {
            return new SqlConnection(ConfigurationManager.ConnectionStrings[connectionString].ConnectionString);
        }
    }
}
