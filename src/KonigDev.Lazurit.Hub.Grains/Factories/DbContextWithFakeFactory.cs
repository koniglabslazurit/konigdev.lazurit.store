﻿using KonigDev.Lazurit.Model.Context.DataContext;
using KonigDev.Lazurit.Model.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace KonigDev.Lazurit.Hub.Grains.Factories
{
    /// <summary>
    /// Класс для того чтобы сделать фейк данных по маркетинговым акциям, дабы продолжить разработку без создания миграции
    /// </summary>
    public class DbContextWithFakeFactory : IDBContextFactory
    {
        private static readonly List<MarketingAction> _marketingActions = new List<MarketingAction>();

        #region Fakes
        /// <summary>
        /// Фэйковый DbSet, пишущий в локальную коллекцию, вместо базы
        /// </summary>
        private class MarketingActionsDbSetFake : DbSet<MarketingAction>, IDbSet<MarketingAction>
        {
            public override ObservableCollection<MarketingAction> Local
            {
                get
                {
                    return new ObservableCollection<MarketingAction>(_marketingActions);
                }
            }
            public override MarketingAction Add(MarketingAction entity)
            {
                _marketingActions.Add(entity);
                return entity;
            }
            public override MarketingAction Remove(MarketingAction entity)
            {
                _marketingActions.Remove(entity);
                return entity;
            }

            public override MarketingAction Find(params object[] keyValues)
            {
                var currentKey = (Guid)keyValues.First();

                return _marketingActions.FirstOrDefault(x => x.Id == currentKey);
            }

            public Type ElementType
            {
                get
                {
                    return _marketingActions.AsQueryable().ElementType;
                }
            }
            public Expression Expression
            {
                get
                {
                    return _marketingActions.AsQueryable().Expression;
                }
            }
            public IQueryProvider Provider
            {
                get
                {
                    return _marketingActions.AsQueryable().Provider;
                }
            }
            IEnumerator IEnumerable.GetEnumerator()
            {
                return _marketingActions.AsQueryable().GetEnumerator();
            }
            public IEnumerator<MarketingAction> GetEnumerator()
            {
                return _marketingActions.AsQueryable().GetEnumerator();
            }
        }

        private class LazuritContextWithFake : LazuritBaseContext
        {
            public override DbSet<TEntity> Set<TEntity>()
            {
                if (typeof(TEntity) == typeof(MarketingAction))
                {
                    return new MarketingActionsDbSetFake() as DbSet<TEntity>;
                }
                return base.Set<TEntity>();
            }
        }
        #endregion


        public DbContext CreateLazuritContext()
        {
            return new LazuritContextWithFake();
        }

        public IDbConnection CreateDbConnection(string connectionString)
        {
            throw new NotImplementedException();
        }

        public IDbConnection CreateIdentityConnection()
        {
            throw new NotImplementedException();
        }        

        public DbContext CreateLazuritIntegrationContext()
        {
            throw new NotImplementedException();
        }
    }
}
