﻿using System.Data;
using System.Data.Entity;

namespace KonigDev.Lazurit.Hub.Grains.Factories
{
    // TODO: Разделить интерфейс на 2-а интерфейса IDbContextFactory, IDbConnectionFactory
    public interface IDBContextFactory
    {
        DbContext CreateLazuritContext();
        DbContext CreateLazuritIntegrationContext();
        IDbConnection CreateIdentityConnection();
        IDbConnection CreateDbConnection(string connectionString);

    }
}
