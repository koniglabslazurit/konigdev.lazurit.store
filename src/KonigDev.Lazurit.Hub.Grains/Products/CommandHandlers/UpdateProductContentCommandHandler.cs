﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Command;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Products.CommandHandlers
{
    public class UpdateProductContentCommandHandler : Grain, IHubCommandHandler<UpdateProductContentCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public UpdateProductContentCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task Execute(UpdateProductContentCommand command)
        {
            if (command.Id == Guid.Empty)
                throw new NullReferenceException("Не указан ид объекта");
            
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var productContent = context.Set<CollectionVariantContentProductArea>().FirstOrDefault(p => p.Id == command.Id);
                if (productContent == null)                    
                    throw new NullReferenceException("Изображение товара на картинке варианта коллекции не найдено");

                productContent.Left = command.Left;
                productContent.Top = command.Top;
                productContent.Width = command.Width;
                productContent.Height = command.Height;

                await context.SaveChangesAsync();
            }
        }
    }
}
