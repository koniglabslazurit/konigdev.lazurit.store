﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using Orleans;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Core.Transactions;
using System.Transactions;
using System;
using System.Data.Entity;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Command;
using Dapper;
using System.Data;

namespace KonigDev.Lazurit.Hub.Grains.Products.CommandHandlers
{
    [Serializable]
    public class UpdateOrCreateProductsPackCommandHandler : Grain, IHubCommandHandler<UpdateOrCreateProductsPackCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;

        public UpdateOrCreateProductsPackCommandHandler(IDBContextFactory contextFactory, ITransactionFactory transactionFactory)
        {
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(UpdateOrCreateProductsPackCommand command)
        {
            var totalCount = command.ProductsPacks.Count();
            var iteration = 0;        

            for (int i = 0; i < command.ProductsPacks.Count; i = i + 30)
            {
                using (IDbConnection connection = _contextFactory.CreateDbConnection("LazuritBaseContext"))
                {
                    connection.Open();
                    TransactionOptions to = new TransactionOptions();
                    to.IsolationLevel = System.Transactions.IsolationLevel.Snapshot;
                    using (var transaction = _transactionFactory.CreateScope(TransactionScopeOption.Required, to))
                    {
                        try
                        {
                            var productPacks = command.ProductsPacks.OrderBy(p => p.ProductSyncCode1C).Skip(30 * iteration).Take(30);                            
                            connection.Execute(productPackQueryUpdateOrInsert, productPacks.Select(p => new
                            {
                                Id = Guid.NewGuid(), p.ProductSyncCode1C, p.SyncCode1C, p.Height, p.Width, p.Length, p.Weight, p.Amount
                            }));                            
                            transaction.Complete();
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }
                iteration++;
            }
        }

        private static string productPackQueryUpdateOrInsert
        {
            get
            {
                return @"UPDATE ProductsPacking SET Height = @Height,Width = @Width,Length = @Length,Weight = @Weight,Amount = @Amount WHERE ProductSyncCode1C = @ProductSyncCode1C and SyncCode1C = @SyncCode1C;
                        IF @@ROWCOUNT=0
                            INSERT INTO ProductsPacking(Id,ProductSyncCode1C,SyncCode1C,Height,Width,Length,Weight,Amount)
                             VALUES (@Id,@ProductSyncCode1C,@SyncCode1C,@Height,@Width,@Length,@Weight,@Amount);";
            }
        }
    }
}
