﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Command;
using Orleans;
using System;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Model.Entities;

namespace KonigDev.Lazurit.Hub.Grains.Products.CommandHandlers
{
    public class CreateProductContentCommandHandler : Grain, IHubCommandHandler<CreateProductContentCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public CreateProductContentCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task Execute(CreateProductContentCommand command)
        {
            if (command.Id == Guid.Empty)
                throw new NullReferenceException("Не указан ид объекта");
            if (command.ProductId == Guid.Empty)
                throw new NullReferenceException("Не указан ид продукта");
            if (command.CollectionVariantId == Guid.Empty)
                throw new NullReferenceException("Не указан ид варианта коллекции");
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var productContent = context.Set<CollectionVariantContentProductArea>().FirstOrDefault(p => p.ProductId== command.ProductId && p.CollectionVariantId==command.CollectionVariantId);
                if (productContent != null)
                    context.Set<CollectionVariantContentProductArea>().Remove(productContent);

                productContent = new CollectionVariantContentProductArea
                {
                    Id = command.Id,
                    ProductId= command.ProductId,
                    CollectionVariantId= command.CollectionVariantId,
                    IsEnabled = true
                };

                context.Set<CollectionVariantContentProductArea>().Add(productContent);
                await context.SaveChangesAsync();
            }
        }
    }
}
