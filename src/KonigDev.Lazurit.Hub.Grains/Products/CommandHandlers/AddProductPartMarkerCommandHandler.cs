﻿using KonigDev.Lazurit.Core.Enums.Exeptions.Product;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Command;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Products.CommandHandlers
{
    public class AddProductPartMarkerCommandHandler : Grain, IHubCommandHandler<AddProductPartMarkerCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public AddProductPartMarkerCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task Execute(AddProductPartMarkerCommand command)
        {
            if (command.ProductId == Guid.Empty)
                throw new NullReferenceException("Не указан ид продукта");
            if (command.ProductPartMarkerId == Guid.Empty)
                throw new NullReferenceException("Не указан ид признака продукта");
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
               var product = context.Set<Product>().FirstOrDefault(p => p.Id == command.ProductId);
                if (product == null)
                    throw new ProductNotFound(Core.Enums.Constants.Exceptions.Product.ProductNotFound);
                product.ProductPartMarkerId = command.ProductPartMarkerId;
                await context.SaveChangesAsync();
            }
        }
    }
}
