﻿using Dapper;
using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Command;
using Orleans;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace KonigDev.Lazurit.Hub.Grains.Products.CommandHandlers
{
    [Serializable]
    public class UpdateProductPricesCommandHandler : Grain, IHubCommandHandler<UpdateProductPricesCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;

        public UpdateProductPricesCommandHandler(IDBContextFactory contextFactory, ITransactionFactory transactionFactory)
        {
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(UpdateProductPricesCommand command)
        {
            var query = @"UPDATE IntegrationsPrices SET Id = @Id, PriceZoneId = @PriceZoneId, ProductSyncCode1C = @ProductSyncCode1C, Value = @Value
                        WHERE PriceZoneId = @PriceZoneId and ProductSyncCode1C = @ProductSyncCode1C;
                            IF @@ROWCOUNT = 0
                        INSERT INTO IntegrationsPrices(Id, PriceZoneId, ProductSyncCode1C, Value)
                            VALUES(@Id, @PriceZoneId, @ProductSyncCode1C, @Value)";

            var updatePrtoductQuery = @"UPDATE Prices 
                                        SET Prices.Value = @Value
                                        From Prices
                                        left join Products on Prices.ProductId = Products.Id
                                        WHERE Products.SyncCode1C = @ProductSyncCode1C and Prices.PriceZoneId = @PriceZoneId";

            var totalCount = command.ProductPrices.Count();
            var iteration = 0;

            for (int i = 0; i < command.ProductPrices.Count; i = i + 30)
            {
                var products = command.ProductPrices.OrderBy(p => p.ProductSyncCode).Skip(30 * iteration).Take(30);
                var productsList = new List<dynamic>();
                /* todo посмотреть как сделать читабелней и оптимальней */
                foreach (var item in products)
                {
                    foreach (var price in item.Prices)
                    {
                        productsList.Add(new { Id = Guid.NewGuid(), PriceZoneId = price.PriceZoneId, ProductSyncCode1C = item.ProductSyncCode, Value = price.Price });
                    }
                }

                using (IDbConnection connection = _contextFactory.CreateDbConnection("LazuritIntegrationContext"))
                {
                    connection.Open();
                    TransactionOptions to = new TransactionOptions();
                    to.IsolationLevel = System.Transactions.IsolationLevel.Snapshot;
                    using (var transaction = _transactionFactory.CreateScope(TransactionScopeOption.Required, to))
                    {
                        try
                        {
                            connection.Execute(query, productsList);                            
                            transaction.Complete();
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }

                using (IDbConnection baseConnection = _contextFactory.CreateDbConnection("LazuritBaseContext"))
                {
                    baseConnection.Open();
                    TransactionOptions to = new TransactionOptions();
                    to.IsolationLevel = System.Transactions.IsolationLevel.Snapshot;
                    using (var transaction = _transactionFactory.CreateScope(TransactionScopeOption.Required, to))
                    {
                        try
                        {                            
                            baseConnection.Execute(updatePrtoductQuery, productsList);
                            transaction.Complete();
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }

                iteration++;
            }
        }
    }
}
