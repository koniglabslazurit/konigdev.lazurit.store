﻿using KonigDev.Lazurit.Core.Enums.Enums;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Command;
using KonigDev.Lazurit.Model.Entities;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using Orleans;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Products.CommandHandlers
{    
    public class UnPublishProductCommandHandler : Grain, IHubCommandHandler<UnPublishProductCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public UnPublishProductCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task Execute(UnPublishProductCommand command)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var product = context.Set<Product>().FirstOrDefault(p => p.SyncCode1C == command.SyncCode1C);
                if(product!= null)
                {
                    context.Set<Product>().Remove(product);

                    var imageContent = context.Set<CollectionVariantContentProductArea>().Where(p => p.ProductId == product.Id).ToList();
                    if(imageContent.Count > 0)
                        context.Set<CollectionVariantContentProductArea>().RemoveRange(imageContent);
                    try
                    {
                        context.SaveChanges();
                    }
                    catch(Exception ex)
                    {

                    }
                }
            }

            using (var context = _dbContextFactory.CreateLazuritIntegrationContext())
            {
                var product = context.Set<IntegrationProduct>().FirstOrDefault(p => p.SyncCode1C == command.SyncCode1C);
                if(product != null)
                {
                    product.IntegrationStatusId = context.Set<IntegrationStatus>().Where(p => p.TechName == EnumIntegrationStatus.Template.ToString()).Select(p => p.Id).FirstOrDefault();
                    context.SaveChanges();
                }
            }
        }
    }
}
