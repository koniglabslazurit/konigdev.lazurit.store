﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Collections;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Query;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors;
using KonigDev.Lazurit.Hub.Model.DTO.Facings;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors;
using KonigDev.Lazurit.Hub.Model.DTO.Products;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;

namespace KonigDev.Lazurit.Hub.Grains.Products.QueryHandlers
{
    public class GetProductByUnicFieldsQueryHandler : Grain, IHubQueryHandler<GetProductByUniqueFieldsQuery, DtoProductByUniqueFieldsResult>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetProductByUnicFieldsQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<DtoProductByUniqueFieldsResult> Execute(GetProductByUniqueFieldsQuery query)
        {
            if (string.IsNullOrEmpty(query.SyncCode1C))
                throw new NotSupportedException();
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var resultProduct = context.Set<Product>()
                    .SingleOrDefault(a => a.SyncCode1C == query.SyncCode1C);
                if (resultProduct != null)
                {
                    return Task.FromResult(new DtoProductByUniqueFieldsResult
                    {
                        Id = resultProduct.Id,                       
                        ArticleId = resultProduct.ArticleId,
                        SyncCode1C = resultProduct.SyncCode1C,
                        Width = resultProduct.Width,
                        Height = resultProduct.Width,
                        Length = resultProduct.Length,
                        IsAssemblyRequired = resultProduct.IsAssemblyRequired,
                        Facing = new DtoFacingsResult
                        {
                            Id = resultProduct.Facing.Id,
                            Name = resultProduct.Facing.Name
                        },
                        FacingColor = new DtoFacingColorResult
                        {
                            Id = resultProduct.FacingColor.Id,
                            Name = resultProduct.FacingColor.Name
                        },
                        FrameColor = new DtoFrameColorResult
                        {
                            Id = resultProduct.FrameColor.Id,
                            Name = resultProduct.FrameColor.Name
                        }
                    });
                }
                return Task.FromResult<DtoProductByUniqueFieldsResult>(null);
            }
        }
    }
}
