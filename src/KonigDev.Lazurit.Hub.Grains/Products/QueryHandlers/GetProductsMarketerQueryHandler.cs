﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Products.QueryHandlers
{
    /// <summary>
    /// Получение маркетинговых артикулов с продуктами
    /// </summary>
    [Serializable]
    public class GetProductsMarketerQueryHandler : Grain, IHubQueryHandler<GetProductsMarketerQuery, DtoProductMarketerTableResponse>
    {
        private readonly IDBContextFactory _dbContextFactory;

        /// <summary>
        /// Получение маркетинговых артикулов с продуктами
        /// </summary>
        public GetProductsMarketerQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        /// <summary>
        /// Получение маркетинговых артикулов с продуктами
        /// </summary>
        public Task<DtoProductMarketerTableResponse> Execute(GetProductsMarketerQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                /* отбираем маркетинговые артикулы, где есть хоть один продукта, сортируем по весу по desc */
                var queryDb = context.Set<Article>()
                    .Where(article => article.Products.Any())
                    .OrderByDescending(p => p.Range).AsQueryable();

                #region
                /* фильтр по варианту коллекции */
                if (query.CollectionVariantId.HasValue)
                    queryDb = queryDb.Where(article => article.Products.SelectMany(product => product.CollectionsVariants).Any(cv => cv.Id == query.CollectionId.Value));

                /* фильтр по коллекции */
                if (query.CollectionId.HasValue)
                    queryDb = queryDb.Where(article => article.Products.Any(p => p.CollectionId == query.CollectionId.Value));

                /* фильтр по цвету отделки.  */
                if (query.FacingColorIds != null && query.FacingColorIds.Any())
                    queryDb = queryDb.Where(article => article.Products.Any(product => query.FacingColorIds.Any(facingColor => facingColor.Value == product.FacingColorId) || product.FacingColorId == null || product.FacingColor.IsUniversal));

                /* фильтр по отделке */
                if (query.FacingIds != null && query.FacingIds.Any())
                    queryDb = queryDb.Where(article => article.Products.Any(product => query.FacingIds.Any(facing => facing.Value == product.FacingId)));

                /* фильтр по цвету корпуса */
                if (query.FrameColorIds != null && query.FrameColorIds.Any())
                    queryDb = queryDb.Where(article => article.Products.Any(product => query.FrameColorIds.Any(frameColor => frameColor.Value == product.FrameColorId)));

                /* фильтр по типу мебели */
                if (query.FurnitureTypeId.HasValue)
                    queryDb = queryDb.Where(article => article.FurnitureTypeId == query.FurnitureTypeId);

                /* фильтр по алиасу типа мебели */
                /* Выдаем те продукты, которые входят так же в чилды типов мебели, если у типа есть чилды. Признак того что тип парент - у него ид парента равно своему ид
                 * необходимо для того чтобы при выборе парента, отображалист все входящие в чилды этого парента продукты */
                if (!string.IsNullOrWhiteSpace(query.FurnitureAlias))
                {
                    var ft = context.Set<FurnitureType>().FirstOrDefault(p => p.Alias == query.FurnitureAlias);
                    if (ft != null && ft.ParentId == ft.Id)
                        queryDb = queryDb.Where(article => article.FurnitureType.Alias == query.FurnitureAlias || article.FurnitureType.ParentId == ft.Id);
                    else queryDb = queryDb.Where(article => article.FurnitureType.Alias == query.FurnitureAlias);
                }
                #endregion

                /* общее количество */
                var result = new DtoProductMarketerTableResponse { TotalCount = queryDb.Count() };

                /* пагинация */
                if (query.ItemsOnPage != 0)
                    queryDb = queryDb.Skip((query.Page - 1) * query.ItemsOnPage).Take(query.ItemsOnPage);

                /* выборка маркетинговых артикулов и продуктов в них */
                result.Items = queryDb.Select(article => new DtoProductMarketer
                {
                    VendorCode = article.SyncCode1C,
                    Range = article.Range,
                    Products = article.Products
                    .OrderByDescending(product => product.Range)
                    .Select(product => new DtoProduct
                    {
                        Id = product.Id,
                        SyncCode1C = product.SyncCode1C,
                        ArticleName = article.Name,
                        ArticleSyncCode1c = article.SyncCode1C,
                        FurnitureTypeAlias = article.FurnitureType.Alias,
                        FurnitureTypeId = article.FurnitureTypeId,
                        FurnitureTypeName = article.FurnitureType.Name,
                        IsAssemblyRequired = product.IsAssemblyRequired,
                        MarketerRange = product.Range,
                        SeriesAlias = product.Collection.Series.Alias,
                        SeriesName = product.Collection.Series.Name,
                        Height = product.Height,
                        Length = product.Length,
                        Width = product.Width,
                        IsFavorite = product.FavoriteItemProducts.Any(favorite => favorite.FavoriteId == query.UserId)
                    }).ToList()
                }).ToList();
                return Task.FromResult(result);
            }
        }
    }
}
