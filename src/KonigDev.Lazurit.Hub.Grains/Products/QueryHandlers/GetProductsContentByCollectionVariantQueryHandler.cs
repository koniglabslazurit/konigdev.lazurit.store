﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Products;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Products.QueryHandlers
{
    public class GetProductsContentByCollectionVariantQueryHandler : Grain, IHubQueryHandler<GetProductsContextByCollectionVariantQuery, List<DtoProductContentResult>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetProductsContentByCollectionVariantQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoProductContentResult>> Execute(GetProductsContextByCollectionVariantQuery query)
        {

            //todo can query optimize?
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var products = context.Set<Product>()
                    .Where(p => p.CollectionsVariants.Any(c => c.Id == query.CollectionVariantId))
                    .Select(p => new DtoProductContentResult
                    {
                        Id = p.Id,
                        Article = p.Article.SyncCode1C,
                        IsFavorite= query.UserId.HasValue && p.FavoriteItemProducts.Any(x=>x.Favorite.UserProfile.Id==query.UserId)
                    }).ToList();

                var productContents = context.Set<CollectionVariantContentProductArea>()
                    .Where(x => x.CollectionVariantId == query.CollectionVariantId && x.IsEnabled)
                    .Select(x => new DtoProductContentResult
                    {
                        Id = x.ProductId,
                        ObjectId = x.Id,
                        Height = x.Height,
                        Width = x.Width,
                        Left = x.Left,
                        Top = x.Top
                    }).ToList();

                foreach (var product in products)
                {
                    foreach (var productContent in productContents.Where(productContent => product.Id == productContent.Id))
                    {
                        product.ObjectId = productContent.ObjectId;
                        product.Height = productContent.Height;
                        product.Width = productContent.Width;
                        product.Top = productContent.Top;
                        product.Left = productContent.Left;
                    }
                }

                return Task.FromResult(products);
            }

        }

    }
}
