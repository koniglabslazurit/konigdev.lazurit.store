﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Products;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Query;
using Orleans;
using KonigDev.Lazurit.Model.Entities;

namespace KonigDev.Lazurit.Hub.Grains.Products.QueryHandlers
{
    /// <summary>
    /// Получение цен, скидок, рассрочек по продуктам
    /// </summary>
    public class GetProductsPricesQueryHandler : Grain, IHubQueryHandler<GetProductsPricesQuery, List<DtoProductPricesResult>>
    {
        private readonly IDBContextFactory _contextFactory;

        /// <summary>
        /// Получение цен, скидок, рассрочек по продуктам
        /// </summary>
        public GetProductsPricesQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        /// <summary>
        /// Получение цен, скидок, рассрочек по продуктам
        /// </summary>
        public async Task<List<DtoProductPricesResult>> Execute(GetProductsPricesQuery query)
        {
            var date = DateTime.Now.Date.ToString("yyyyMMdd"); /* дата отбора рассрочек, скидок на которую ониустанволены. Берется актуальные данные на сегодня  */

            string sqlQuery = @"SELECT TOP 1 p.ProductId, p.Value, p.PriceForAssembly,
                                    i.Discount,
                                    i.Installment10Month,
                                    i.Installment12Month,
                                    i.Installment18Month,
                                    i.Installment24Month,
                                    i.Installment36Month,
                                    i.Installment48Month,
                                    i.Installment6Month
                                FROM Prices as p
                                LEFT JOIN InstallmentsItems as i on i.productId = p.Productid AND i.DateStart <= @date AND i.DateEnd >= @date
                                WHERE p.ProductId=@ProductId AND p.PriceZoneId=@PriceZoneId AND p.CurrencyCode=@CurrencyCode";

            ConcurrentBag<DtoProductPricesResult> productPricesBag = new ConcurrentBag<DtoProductPricesResult>();
            Parallel.ForEach(query.ProductIds, productId =>
            {
                /* TODO!!! строка подключения!! Решить как делать коннекшен!! */
                using (IDbConnection db = _contextFactory.CreateDbConnection("LazuritBaseContext"))
                {
                    dynamic queryEntity = db.Query<dynamic>(sqlQuery, new { ProductId = productId, PriceZoneId = query.PriceZoneId, CurrencyCode = query.CurrencyCode, Date = date }).SingleOrDefault();
                    var queryResult = new DtoProductPricesResult
                    {
                        AssemblyPrice = queryEntity.PriceForAssembly??0,
                        ProductId = (Guid)queryEntity.ProductId,
                        Discount = queryEntity.Discount,
                        Price = queryEntity.Value,
                        Installment10Month = queryEntity.Installment10Month ?? 0,
                        Installment12Month = queryEntity.Installment12Month ?? 0,
                        Installment18Month = queryEntity.Installment18Month ?? 0,
                        Installment24Month = queryEntity.Installment24Month ?? 0,
                        Installment36Month = queryEntity.Installment36Month ?? 0,
                        Installment48Month = queryEntity.Installment48Month ?? 0,
                        Installment6Month = queryEntity.Installment6Month ?? 0
                    };
                    productPricesBag.Add(queryResult);
                }
            });
            return await Task.FromResult(productPricesBag.AsList());
        }
    }
}
