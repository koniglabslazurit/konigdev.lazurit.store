﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Products.QueryHandlers
{
    public class GetProductsByRequestQueryHandler : Grain, IHubQueryHandler<GetProductsByRequestQuery, List<DtoProduct>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetProductsByRequestQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        /// <summary>
        /// Получение списка продуктов по запросу.
        /// </summary>
        /// <param name="query">Запрос</param>
        /// <returns></returns>
        public Task<List<DtoProduct>> Execute(GetProductsByRequestQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var products = context.Set<Product>().AsQueryable();

                /* фильтр по варианту коллекции */
                if (query.CollectionVariantId.HasValue)
                    products = products.Where(p => p.CollectionsVariants.Any(c => c.Id == query.CollectionVariantId));
                /* фильтр по коллекции */
                if (query.CollectionId.HasValue)
                    products = products.Where(p => p.CollectionId == query.CollectionId);

                /* TODO отдавать еще продукты, у которых отсутствует один из цветов, но совпадают два других */
                /* фильтр по цвету отделки.  */
                if (query.FacingColorIds != null && query.FacingColorIds.Any())
                    products = products.Where(p => query.FacingColorIds.Any(f => f.Value == p.FacingColorId) || p.FacingColor.IsUniversal || p.FacingColorId == null);

                if (query.FacingIds != null && query.FacingIds.Any())
                    products = products.Where(p => query.FacingIds.Any(f => f.Value == p.FacingId));

                if (query.FrameColorIds != null && query.FrameColorIds.Any())
                    products = products.Where(p => query.FrameColorIds.Any(f => f.Value == p.FrameColorId));

                if (query.FurnitureTypeId.HasValue)
                    products = products.Where(p => p.Article.FurnitureTypeId == query.FurnitureTypeId);

                if (!string.IsNullOrWhiteSpace(query.FurnitureAlias))
                {
                    var furnitureType = context.Set<FurnitureType>().FirstOrDefault(f => f.Alias == query.FurnitureAlias);
                    if (furnitureType != null)
                    {
                        if (furnitureType.Id == furnitureType.ParentId)
                        {
                            products = products.Where(p => p.Article.FurnitureType.ParentId == furnitureType.ParentId);
                        }
                        else
                        {
                            products = products.Where(p => p.Article.FurnitureType.Id == furnitureType.Id);
                        }
                    }
                }

                var productList = products.Select(p => new DtoProduct
                {
                    Id = p.Id,
                    SyncCode1C = p.SyncCode1C,
                    IsAssemblyRequired = p.IsAssemblyRequired,
                    Height = p.Height,
                    Length = p.Length,
                    Width = p.Width,
                    FurnitureTypeName = p.Article.FurnitureType.Name,
                    FurnitureTypeId = p.Article.FurnitureTypeId,
                    IsFavorite = p.FavoriteItemProducts.Any(x => x.FavoriteId == query.UserId),
                    ArticleName = p.Article.Name,
                    ArticleSyncCode1c = p.Article.SyncCode1C,
                    FurnitureTypeAlias = p.Article.FurnitureType.Alias,
                    SeriesAlias = p.Collection.Series.Alias,
                    SeriesName = p.Collection.Series.Name
                }).ToList();

                return Task.FromResult(productList);
            }
        }
    }
}
