﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace KonigDev.Lazurit.Hub.Grains.Products.QueryHandlers
{
    [Serializable]
    public class GetProductUnPublishValidationQueryHandler : Grain, IHubQueryHandler<GetProductUnPublishValidationQuery, DtoProductUnPublishValidation>
    {
        private readonly IDBContextFactory _contextFactory;

        public GetProductUnPublishValidationQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public async Task<DtoProductUnPublishValidation> Execute(GetProductUnPublishValidationQuery query)
        {
            var result = new DtoProductUnPublishValidation { IsValid = true };
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var existProduct = context.Set<Product>().FirstOrDefault(p => p.SyncCode1C == query.SyncCode1C);
                if (existProduct == null)
                {
                    result.IsValid = false;
                    result.Errors.Add("Не найден продукт в БД");
                    return result;
                }

                var orders = context.Set<OrderItemProduct>().Any(p => p.ProductId == existProduct.Id)
                    || context.Set<OrderItemProductInCollection>().Any(p => p.ProductId == existProduct.Id);
                if (orders == true)
                {
                    result.IsValid = false;
                    result.Errors.Add("Невозможно удалить продукт, так как он привязан к заказу");
                    return result;
                }
            }
            return result;
        }
    }
}
