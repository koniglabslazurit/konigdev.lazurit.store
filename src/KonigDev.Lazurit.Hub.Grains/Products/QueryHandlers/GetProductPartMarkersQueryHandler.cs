﻿using Dapper;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Products;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Products.QueryHandlers
{
    public class GetProductPartMarkersQueryHandler : Grain, IHubQueryHandler<GetProductPartMarkersQuery, List<DtoProductPartMarker>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetProductPartMarkersQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task<List<DtoProductPartMarker>> Execute(GetProductPartMarkersQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var productPartMarkers = context.Set<ProductPartMarker>().Select(p => new DtoProductPartMarker
                {
                    Id = p.Id,
                    Name = p.Name,
                    SortNumber = p.SortNumber,
                    TechName = p.TechName

                }).ToList();
                return await Task.FromResult(productPartMarkers);
            }
        }
    }
}

