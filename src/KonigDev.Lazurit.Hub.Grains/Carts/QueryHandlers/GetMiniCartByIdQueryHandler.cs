﻿using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.MiniCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Query;
using KonigDev.Lazurit.Model.Entities;

namespace KonigDev.Lazurit.Hub.Grains.Carts.QueryHandlers
{
    public class GetMiniCartByIdQueryHandler : GetMiniCartQueryHandler, IHubQueryHandler<GetMiniCartByIdQuery, DtoMiniCart>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetMiniCartByIdQueryHandler(IDBContextFactory dbContextFactory):base(dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public Task<DtoMiniCart> Execute(GetMiniCartByIdQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var cartQuery = context.Set<Cart>().Where(x => x.Id == query.CartId);
                return Execute(cartQuery);
            }
        }
    }
}
