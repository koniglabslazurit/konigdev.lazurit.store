﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cart;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Query;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Model.Entities;
using System.Data.Entity;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.FullCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants;
using KonigDev.Lazurit.Hub.Model.DTO.Products;

namespace KonigDev.Lazurit.Hub.Grains.Carts.QueryHandlers
{
    public class GetFullCartByIdQueryHandler : Grain, IHubQueryHandler<GetFullCartByIdQuery, DtoFullCart>
    {
        private readonly IDBContextFactory _dbContextFactory;
        public GetFullCartByIdQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public Task<DtoFullCart> Execute(GetFullCartByIdQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var cartQuery = context.Set<Cart>().Where(x => x.Id == query.CartId);
                var cartResult = cartQuery.Select(x => new DtoFullCart
                {
                    Id = x.Id,
                    IsDeliveryRequired = x.IsDeliveryRequired,
                    Complects = x.Complects.Select(c => new DtoCartItemComplect
                    {
                        CartId = c.CartId,
                        CollectionVariant = new DtoCollectionVariantResult
                        {
                            CollectionId = c.CollectionVariant.CollectionId,
                            CollectionAlias = c.CollectionVariant.Collection.Series.Alias,
                            Id = c.CollectionVariant.Id,
                            Products = c.CollectionVariant.Products.Select(p => new DtoProductResult
                            {
                                ArticleId = p.ArticleId,
                                FacingColorId = p.FacingColorId,
                                FacingId = p.FacingId,
                                FrameColorId = p.FrameColorId,
                                FurnitureType = p.Article.FurnitureType.Name,
                                Article = p.Article.Name,
                                Height = p.Height,
                                Id = p.Id,
                                IsAssemblyRequired = p.IsAssemblyRequired,
                                Length = p.Length,
                                SyncCode1C = p.SyncCode1C,
                                Width = p.Width,
                                SeriesName = c.CollectionVariant.Collection.Series.Name,
                                CollectionAlias = c.CollectionVariant.Collection.Series.Alias
                            }).ToList(),
                            SeriesName = c.CollectionVariant.Collection.Series.Name,
                            RoomTypeName = c.CollectionVariant.Collection.RoomType.Name,
                            RoomTypeAlias = c.CollectionVariant.Collection.RoomType.Alias
                        },
                        ItemId = c.CollectionVariant.Id,
                        Id = c.Id,
                        Quantity = c.Quantity,
                        IsAssemblyRequired = c.IsAssemblyRequired
                    }).ToList(),
                    Products = x.Products.Select(p => new DtoCartItemProduct
                    {
                        CartId = p.CartId,
                        Id = p.Id,
                        Product = new DtoProductResult
                        {
                            ArticleId = p.Product.ArticleId,
                            FacingColorId = p.Product.FacingColorId,
                            FacingId = p.Product.FacingId,
                            FrameColorId = p.Product.FrameColorId,
                            FurnitureType = p.Product.Article.FurnitureType.Name,
                            Article = p.Product.Article.Name,
                            Height = p.Product.Height,
                            Id = p.Product.Id,
                            IsAssemblyRequired = p.Product.IsAssemblyRequired,
                            Length = p.Product.Length,
                            SyncCode1C = p.Product.SyncCode1C,
                            Width = p.Product.Width,
                            SeriesName = p.Product.Collection.Series.Name,
                            CollectionAlias = p.Product.Collection.Series.Alias
                        },
                        ItemId = p.ProductId,
                        Quantity = p.Quantity,
                        IsAssemblyRequired = p.IsAssemblyRequired,
                    }).ToList(),
                    Collections = x.Collections.Select(c => new DtoCartItemCollection
                    {
                        CartId = c.CartId,
                        CollectionVariantId = c.CollectionVariantId,
                        Id = c.Id,
                        Quantity = c.Quantity,
                        Products = c.Products.Select(cp => new DtoCartItemProductInCollection
                        {
                            Id = cp.Id,
                            Product = new DtoProductResult
                            {
                                ArticleId = cp.Product.ArticleId,
                                FacingColorId = cp.Product.FacingColorId,
                                FacingId = cp.Product.FacingId,
                                FrameColorId = cp.Product.FrameColorId,
                                FurnitureType = cp.Product.Article.FurnitureType.Name,
                                Article = cp.Product.Article.Name,
                                Height = cp.Product.Height,
                                Id = cp.Product.Id,
                                IsAssemblyRequired = cp.Product.IsAssemblyRequired,
                                Length = cp.Product.Length,
                                SyncCode1C = cp.Product.SyncCode1C,
                                Width = cp.Product.Width,
                                SeriesName = c.CollectionVariant.Collection.Series.Name,
                                CollectionAlias = c.CollectionVariant.Collection.Series.Alias
                            },
                            ItemId = cp.ProductId,
                            Quantity = cp.Quantity,
                            IsAssemblyRequired = cp.IsAssemblyRequired,
                            CartItemCollectionId = cp.CartItemCollectionId
                        }).ToList(),
                        CollectionVariant = new DtoCartCollectionVariantResult
                        {
                            CollectionId = c.CollectionVariant.CollectionId,
                            CollectionAlias = c.CollectionVariant.Collection.Series.Alias,
                            Id = c.CollectionVariant.Id,
                            SeriesName = c.CollectionVariant.Collection.Series.Name,
                            RoomTypeName = c.CollectionVariant.Collection.RoomType.Name,
                            RoomTypeAlias = c.CollectionVariant.Collection.RoomType.Alias
                        }
                    }).ToList()
                }).SingleOrDefault();

                if (cartResult == null)
                    return Task.FromResult<DtoFullCart>(null);

                //TODO Удалить, когда разберёмся с SeriesAlias
                //достаём коллекции, в которых есть продукт из корзины одним запросом, что бы потом работать в памяти с ними
                //FillCollectionInfoForProductInCart(context, cartResult.Products.ToList());

                FillCollectionInfoForProductInCollections(cartResult.Collections.ToList());

                return Task.FromResult(cartResult);
            }
        }

        private static void FillCollectionInfoForProductInCollections(ICollection<DtoCartItemCollection> collections)
        {
            foreach (var collection in collections)
            {
                foreach (var product in collection.Products.Cast<DtoCartItemProductInCollection>())
                {
                    product.Product.SeriesName = collection.CollectionVariant.SeriesName;
                    product.Product.RoomTypeName = collection.CollectionVariant.RoomTypeName;
                    product.Product.CollectionAlias = collection.CollectionVariant.CollectionAlias;
                }
            }
        }

        //TODO Удалить, когда разберёмся с SeriesAlias
        //private static void FillCollectionInfoForProductInCart(DbContext context, ICollection<DtoCartItemProduct> products)
        //{
        //    var collectionsQuery = context.Set<Collection>();
        //    var distinctProductsInMemory =
        //        products.GroupBy(p => p.CollectionId).Select(g => g.First()).Select(x => x.CollectionId).ToList();
        //    var collectionInMemory =
        //        collectionsQuery.Where(x => distinctProductsInMemory.Contains(x.Id)).Select(x => new DtoCartCollectionName
        //        {
        //            CollectionId = x.Id,
        //            Name = x.Series.Name,
        //            RoomTypeName = x.RoomType.Name,
        //            CollectionAlias = x.Alias
        //        }).ToList();

        //    foreach (var product in products)
        //    {
        //        var collectionForProduct = collectionInMemory.Single(x => x.CollectionId == product.CollectionId);
        //        product.Product.SeriesName = collectionForProduct.Name;
        //        product.Product.RoomTypeName = collectionForProduct.RoomTypeName;
        //        product.Product.CollectionAlias = collectionForProduct.CollectionAlias;
        //    }
        //}
    }
}
