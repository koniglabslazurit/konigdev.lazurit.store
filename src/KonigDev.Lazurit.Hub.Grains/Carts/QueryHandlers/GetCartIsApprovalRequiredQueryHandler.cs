﻿using KonigDev.Lazurit.Core.Enums;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cart;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;

namespace KonigDev.Lazurit.Hub.Grains.Carts.QueryHandlers
{
    public class GetCartIsApprovalRequiredQueryHandler : Grain, IHubQueryHandler<GetCartIsApprovalRequiredQuery, DtoCartIsNeedApproval>
    {
        private readonly IDBContextFactory _dbContextFactory;
        public GetCartIsApprovalRequiredQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<DtoCartIsNeedApproval> Execute(GetCartIsApprovalRequiredQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var markers = context.Set<ProductPartMarker>()
                    .Where(p => p.TechName == EnumProductPartMarker.ExistedNonRequired.ToString()
                    || p.TechName == EnumProductPartMarker.ExistedRequired.ToString())
                    .Select(p => p.Id).ToList();

                var dbQuery = context.Set<Cart>().Where(x => x.Id == query.CartId);
                var result = dbQuery.Any(p => p.Products.Any(x => markers.Contains(x.Product.ProductPartMarkerId))
                || p.Complects.Any(c => c.CollectionVariant.Products.Any(x => markers.Contains(x.ProductPartMarkerId)))
                || p.Collections.Any(c => c.Products.Any(x => markers.Contains(x.Product.ProductPartMarkerId))));

                return Task.FromResult(new DtoCartIsNeedApproval { IsApprovalRequired = result });
            }
        }
    }
}