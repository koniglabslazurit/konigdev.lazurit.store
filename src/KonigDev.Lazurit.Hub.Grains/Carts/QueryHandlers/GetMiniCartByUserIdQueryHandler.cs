﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.MiniCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Query;
using KonigDev.Lazurit.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Carts.QueryHandlers
{
   public  class GetMiniCartByUserIdQueryHandler : GetMiniCartQueryHandler, IHubQueryHandler<GetMiniCartByUserIdQuery, DtoMiniCart>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetMiniCartByUserIdQueryHandler(IDBContextFactory dbContextFactory):base(dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public Task<DtoMiniCart> Execute(GetMiniCartByUserIdQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var cartQuery = context.Set<Cart>().Where(x => x.UserId == query.UserId);
                return Execute(cartQuery);
            }
        }
    }
}
