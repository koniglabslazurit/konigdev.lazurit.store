﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.MiniCartDto;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Carts.QueryHandlers
{
    public class GetMiniCartQueryHandler : Grain
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetMiniCartQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<DtoMiniCart> Execute(IQueryable<Cart> query)
        {
            DtoMiniCart cartResult = null;
            cartResult = query.Select(x => new DtoMiniCart
            {
                Id = x.Id,
                IsDeliveryRequired = x.IsDeliveryRequired,
                Complects = x.Complects.Select(c => new DtoCartMiniItemComplect
                {
                    Id = c.Id,
                    CartId = c.CartId,
                    ItemId = c.CollectionVariant.Id,
                    Quantity = c.Quantity,
                    CollectionId = c.CollectionVariant.CollectionId,
                    CollectionAlias = c.CollectionVariant.Collection.Series.Alias,
                    SeriesName = c.CollectionVariant.Collection.Series.Name,
                    RoomTypeName = c.CollectionVariant.Collection.RoomType.Name,
                    RoomTypeAlias = c.CollectionVariant.Collection.RoomType.Alias,
                    IsAssemblyRequired = c.IsAssemblyRequired
                }).ToList(),
                Products = x.Products.Select(p => new DtoCartMiniItemProduct
                {
                    Id = p.Id,
                    CartId = p.CartId,
                    ItemId = p.ProductId,
                    FurnitureType = p.Product.Article.FurnitureType.Name,
                    //добавил следующее поле для ссылок на товар из малой корзины
                    FurnitureTypeAlias = p.Product.Article.FurnitureType.AliasInPlural,
                    Article = p.Product.Article.Name,
                    //добавил следующее поле для ссылок на товар из малой корзины
                    ArticleSyncCode1C = p.Product.Article.SyncCode1C,
                    Quantity = p.Quantity,
                    SyncCode1C = p.Product.SyncCode1C,
                    IsAssemblyRequired = p.Product.IsAssemblyRequired,
                    ArticleId = p.Product.ArticleId,
                    SeriesName = p.Product.Collection.Series.Name,
                    //Изменить на SeriasAlias, это поле нужно как мингимум в малой корзине для ссылок на продукты
                    CollectionAlias = p.Product.Collection.Series.Alias
                }).ToList(),
                Collections = x.Collections.Select(c => new DtoCartMiniItemCollection
                {
                    Products = c.Products.Select(p => new DtoCartItem
                    {
                        IsAssemblyRequired = p.IsAssemblyRequired,
                        ItemId = p.ProductId,
                        Quantity = p.Quantity
                    }).ToList(),
                    CartId = c.CartId,
                    CollectionVariantId = c.CollectionVariantId,
                    Id = c.Id,
                    Quantity = c.Quantity,
                    CollectionId = c.CollectionVariant.CollectionId,
                    SeriesName = c.CollectionVariant.Collection.Series.Name,
                    RoomTypeName = c.CollectionVariant.Collection.RoomType.Name,
                    RoomTypeAlias = c.CollectionVariant.Collection.RoomType.Alias,
                    CollectionAlias = c.CollectionVariant.Collection.Series.Alias
                }).ToList()
            }).SingleOrDefault();

            if (cartResult == null)
                return Task.FromResult<DtoMiniCart>(null);

            return Task.FromResult(cartResult);
        }
    }
}
