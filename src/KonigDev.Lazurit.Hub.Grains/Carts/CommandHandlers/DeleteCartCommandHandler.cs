﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Command;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Carts.CommandHandlers
{
    public class DeleteCartCommandHandler : Grain, IHubCommandHandler<DeleteCartCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;
        public DeleteCartCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task Execute(DeleteCartCommand command)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var cart = context.Set<Cart>().FirstOrDefault(x => x.Id == command.CartId);
                if (cart == null) return;
                context.Set<Cart>().Remove(cart);
                await context.SaveChangesAsync();
            }
        }
    }
}
