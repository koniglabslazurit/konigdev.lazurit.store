﻿using KonigDev.Lazurit.Core.Enums;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Command;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;

namespace KonigDev.Lazurit.Hub.Grains.Carts.CommandHandlers
{
    public class RemoveCartItemCommandHandler : Grain, IHubCommandHandler<RemoveCartItemCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public RemoveCartItemCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task Execute(RemoveCartItemCommand command)
        {
            if (command == null)
                return;
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var cart = context.Set<Cart>().FirstOrDefault(x => x.Id == command.CartId);
                if (cart == null)
                    return;
                switch (command.CartItemType)
                {
                    case EnumCartItemType.ProductInCollection:
                        var products = context.Set<CartItemProductInCollection>().Where(x => x.CartItemCollectionId == command.RemoveCollectionId).ToList();
                        context.Set<CartItemProductInCollection>().Remove(products.FirstOrDefault(x => x.ProductId == command.RemoveProductId));
                        if (products.Count <= 1)
                        {
                            context.Set<CartItemCollection>().Remove(cart.Collections.FirstOrDefault(x => x.Id == command.RemoveCollectionId));
                        }
                        break;
                    case EnumCartItemType.Collection:
                        context.Set<CartItemCollection>().Remove(cart.Collections.FirstOrDefault(x => x.Id == command.RemoveCollectionId));
                        break;
                    case EnumCartItemType.Product:
                        context.Set<CartItemProduct>().Remove(cart.Products.FirstOrDefault(x => x.Id == command.RemoveProductId));
                        break;
                    case EnumCartItemType.Complect:
                        context.Set<CartItemComplect>().Remove(cart.Complects.FirstOrDefault(x => x.Id == command.RemoveCollectionId));
                        break;
                }
                await context.SaveChangesAsync();
            }
        }

    }

}

