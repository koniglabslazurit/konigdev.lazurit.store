﻿using KonigDev.Lazurit.Core.Enums;
using KonigDev.Lazurit.Core.Enums.Exeptions.Cart;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Command;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Carts.CommandHandlers
{
    public class AddCartItemCommandHandler : Grain, IHubCommandHandler<AddCartItemCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public AddCartItemCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public async Task Execute(AddCartItemCommand command)
        {
            if (command == null || command.CartId == Guid.Empty)
                return;
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                Cart cart = FindOrCreateCart(command.CartId, context);
                switch (command.CartItemType)
                {
                    case EnumCartItemType.Collection:

                        if (cart.Collections.Any(p => p.CollectionVariantId == command.AddElementId))
                            throw new NotValidExeption(Core.Enums.Constants.Exceptions.Cart.CartHasCollection);

                        var collection = context.Set<CollectionVariant>().Include("Products").FirstOrDefault(x => x.Id == command.AddElementId);
                        if (collection == null)
                            return;
                        var cartItemCollectionId = Guid.NewGuid();
                        var cartItemProductsInCollection = new List<CartItemProductInCollection>();
                        foreach (var product in collection.Products)
                        {
                            var lineItem = new CartItemProductInCollection
                            {
                                Id = Guid.NewGuid(),
                                ProductId = product.Id,
                                Quantity = 1,
                                CartItemCollectionId = cartItemCollectionId,
                                IsAssemblyRequired = true
                            };
                            cartItemProductsInCollection.Add(lineItem);
                        }
                        cart.Collections.Add(new CartItemCollection
                        {
                            CollectionVariantId = collection.Id,
                            CartId = cart.Id,
                            Quantity = 1,
                            Id = Guid.NewGuid(),
                            Products = cartItemProductsInCollection
                        });
                        break;
                    case EnumCartItemType.Complect:
                        if (cart.Complects.Any(p => p.CollectionVariantId == command.AddElementId))
                            throw new NotValidExeption(Core.Enums.Constants.Exceptions.Cart.CartHasComplekt);
                        var complect = context.Set<CollectionVariant>().FirstOrDefault(x => x.Id == command.AddElementId);
                        cart.Complects.Add(new CartItemComplect
                        {
                            CollectionVariantId = complect.Id,
                            CartId = cart.Id,
                            Quantity = 1,
                            Id = Guid.NewGuid(),
                            IsAssemblyRequired = true
                        });
                        break;
                    case EnumCartItemType.Product:
                        {
                            if (cart.Products.Any(p => p.ProductId == command.AddElementId))
                                throw new NotValidExeption(Core.Enums.Constants.Exceptions.Cart.CartHasProduct);
                            var product = context.Set<Product>().FirstOrDefault(x => x.Id == command.AddElementId);
                            if (product == null)
                                return;
                            var lineItem = new CartItemProduct
                            {
                                Id = Guid.NewGuid(),
                                CartId = cart.Id,
                                ProductId = product.Id,
                                Quantity = 1,
                                IsAssemblyRequired = true
                            };
                            cart.Products.Add(lineItem);
                        }
                        break;
                }
                await context.SaveChangesAsync();
            }
        }

        private Cart FindOrCreateCart(Guid cartId, DbContext context)
        {
            Cart cart;
            cart = context.Set<Cart>().FirstOrDefault(x => x.Id == cartId);
            if (cart == null)
            {
                cart = new Cart()
                {
                    Id = cartId,
                    IsDeliveryRequired = true
                };
                context.Set<Cart>().Add(cart);
            }
            return cart;
        }
    }
}
