﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Command;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Carts.CommandHandlers
{
    public class CreateOrUpdateCartCommandHandler : Grain, IHubCommandHandler<CreateOrUpdateCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public CreateOrUpdateCartCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task Execute(CreateOrUpdateCommand command)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var newCart = context.Set<Cart>().FirstOrDefault(x => x.Id == command.NewCart.Id);
                //На всякий случай подчищаем все корзины пользователя, кроме текущей,которую нужно обновить.(На данный момент структура бд позволяет создавать много корзин у одного пользователя, а по логики текущей у него может быть только одна корзина)
                var userCarts = context.Set<Cart>().Where(x => x.UserId == command.UserId);// && x.Id != command.NewCart.Id
                foreach (var userCart in userCarts)
                {
                    context.Set<Cart>().Remove(userCart);
                }
                if (newCart != null)
                    context.Set<Cart>().Remove(newCart);
                //context.SaveChanges();

                var cart = new Cart
                {
                    Id = command.NewCart.Id,
                    UserId = command.UserId,
                    IsDeliveryRequired = command.NewCart.IsDeliveryRequired
                };
                context.Set<Cart>().Add(cart);

                if (command.NewCart.Products != null)
                    foreach (var newProduct in command.NewCart.Products)
                    {
                        var product = context.Set<Product>().SingleOrDefault(x => x.Id == newProduct.ItemId);
                        if (product == null)
                            continue;
                        var lineItem = new CartItemProduct
                        {
                            Id = Guid.NewGuid(),
                            CartId = cart.Id,
                            ProductId = product.Id,
                            Quantity = 1,
                            IsAssemblyRequired = true
                        };
                        cart.Products.Add(lineItem);
                    }
                if (command.NewCart.Complects != null)
                    foreach (var newComplect in command.NewCart.Complects)
                    {
                        var complect = context.Set<CollectionVariant>().SingleOrDefault(x => x.Id == newComplect.ItemId);
                        if (complect == null)
                            continue;
                        cart.Complects.Add(new CartItemComplect
                        {
                            CollectionVariantId = complect.Id,
                            CartId = cart.Id,
                            Quantity = 1,
                            Id = Guid.NewGuid(),
                            IsAssemblyRequired = true
                        });
                    }
                if (command.NewCart.Collections != null)
                    foreach (var newCollection in command.NewCart.Collections)
                    {
                        var collection = context.Set<CollectionVariant>().Include("Products").SingleOrDefault(x => x.Id == newCollection.CollectionVariantId);
                        if (collection == null)
                            continue;
                        var cartItemCollectionId = Guid.NewGuid();
                        var cartItemProductsInCollection = new List<CartItemProductInCollection>();
                        foreach (var product in newCollection.Products)
                        {
                            var lineItem = new CartItemProductInCollection
                            {
                                Id = Guid.NewGuid(),
                                ProductId = product.ItemId,
                                Quantity = 1,
                                CartItemCollectionId = cartItemCollectionId,
                                IsAssemblyRequired = true
                            };
                            cartItemProductsInCollection.Add(lineItem);
                        }
                        cart.Collections.Add(new CartItemCollection
                        {
                            CollectionVariantId = collection.Id,
                            CartId = cart.Id,
                            Quantity = 1,
                            Id = Guid.NewGuid(),
                            Products = cartItemProductsInCollection
                        });
                    }

                await context.SaveChangesAsync();
            }
        }
    }
}
