﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Command;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Carts.CommandHandlers
{
    public class CreateCartCommandHandler : Grain, IHubCommandHandler<CreateCartCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public CreateCartCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task Execute(CreateCartCommand command)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var cart = context.Set<Cart>().SingleOrDefault(x => x.UserId == command.UserId);
                if (cart != null)
                    return;
                cart = new Cart
                {
                    Id = Guid.NewGuid(),
                    UserId = command.UserId
                };
                context.Set<Cart>().Add(cart);
                await context.SaveChangesAsync();
            }
        }
    }
}
