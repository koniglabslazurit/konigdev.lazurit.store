﻿using KonigDev.Lazurit.Core.Enums;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Command;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Carts.CommandHandlers
{
    public class UpdateCartItemCommandHandler : Grain, IHubCommandHandler<UpdateCartItemCommand>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public UpdateCartItemCommandHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public async Task Execute(UpdateCartItemCommand command)
        {
            if (command == null)
                return;
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var cart = context.Set<Cart>().FirstOrDefault(x => x.Id == command.CartId);
                if (cart == null)
                    return;
                switch (command.CartItemType)
                {
                    case EnumCartItemType.Collection:
                        {
                            var item = cart.Collections.FirstOrDefault(x => x.CollectionVariantId == command.UpdateCollectionId);
                            if (item != null)
                            {
                                item.Quantity = command.Quantity;
                            }
                        }
                        break;
                    case EnumCartItemType.ProductInCollection:
                        {
                            var item = cart.Collections.FirstOrDefault(x => x.CollectionVariantId == command.UpdateCollectionId).Products.FirstOrDefault(x=>x.ProductId==command.UpdateProductId);
                            if (item != null)
                            {
                                item.Quantity = command.Quantity;
                            }
                        }
                        break;
                    case EnumCartItemType.Product:
                        {
                            var item = cart.Products.FirstOrDefault(x => x.ProductId == command.UpdateProductId);
                            if (item != null)
                            {
                                item.Quantity = command.Quantity;
                            }
                        }
                        break;
                    case EnumCartItemType.Complect:
                        {
                            var item = cart.Complects.FirstOrDefault(x => x.CollectionVariantId == command.UpdateCollectionId);
                            if (item != null)
                            {
                                item.Quantity = command.Quantity;
                            }
                        }
                        break;
                }
                await context.SaveChangesAsync();
            }
        }
    }
}
