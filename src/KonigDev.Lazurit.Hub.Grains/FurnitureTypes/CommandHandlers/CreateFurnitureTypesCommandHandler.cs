﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Commands;
using Orleans;
using System.Threading.Tasks;
using KonigDev.Lazurit.Core.Transactions;
using System.Transactions;
using System;
using System.Data;
using Dapper;
using System.Linq;

namespace KonigDev.Lazurit.Hub.Grains.FurnitureTypes.CommandHandlers
{
    /// <summary>
    /// Создание типов мебели или обновление существующих
    /// </summary>
    public class CreateFurnitureTypesCommandHandler : Grain, IHubCommandHandler<CreateFurnitureTypesCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;

        /// <summary>
        /// Создание типов мебели или обновление существующих
        /// </summary>
        public CreateFurnitureTypesCommandHandler(IDBContextFactory contextFactory, ITransactionFactory transactionFactory)
        {
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }

        /// <summary>
        /// Создание типов мебели или обновление существующих
        /// </summary>
        public async Task Execute(CreateFurnitureTypesCommand command)
        {
           var query = @"UPDATE FurnituresTypes SET ParentId = @ParentId, IsFurniturePiece = @IsFurniturePiece WHERE Id = @Id;
                        IF @@ROWCOUNT = 0
                            INSERT INTO FurnituresTypes(Id,Name,SyncCode1C,Alias,ParentId,IsFurniturePiece)VALUES(@Id,@Name,@SyncCode1C,@Alias,@ParentId,@IsFurniturePiece);";

            using (IDbConnection connection = _contextFactory.CreateDbConnection("LazuritBaseContext"))
            {
                connection.Open();
                TransactionOptions to = new TransactionOptions();
                to.IsolationLevel = System.Transactions.IsolationLevel.Snapshot;
                using (var transaction = _transactionFactory.CreateScope(TransactionScopeOption.Required, to))
                {
                    try
                    {
                        connection.Execute(query, command.FurnitureTypes.Select(f => new { f.Id, f.Name, f.SyncCode1C, f.Alias, f.ParentId, IsFurniturePiece = f.ParentId.HasValue ? true : false }));
                        transaction.Complete();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }
    }
}
