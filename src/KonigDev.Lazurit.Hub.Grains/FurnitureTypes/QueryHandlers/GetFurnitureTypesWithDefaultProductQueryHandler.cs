﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.FurnitureTypes.QueryHandlers
{
    public class GetFurnitureTypesWithDefaultProductQueryHandler : Grain, IHubQueryHandler<GetFurnitureTypesWithDefaultProductQuery, List<DtoFurnitureTypeWithDefaultProductResult>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetFurnitureTypesWithDefaultProductQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoFurnitureTypeWithDefaultProductResult>> Execute(GetFurnitureTypesWithDefaultProductQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                /* выбираем типы мебели, которые являются парентами, а так же проверяем, что в паренте есть чилды, у которых есть продукты.
                 * Необходимо показывать, если был опубликован товар с типом мебели, не являющимся парентом, к примеру опубликовали стол письменный, нет твоара с типом стол, все равно показываем стол в предметах мебели */
                var furnitureTypesList = context.Set<FurnitureType>()
                    .Where(f => f.ParentId == f.Id && f.Articles.Any(a => a.Products.Count != 0)
                    || f.ParentId == f.Id && context.Set<FurnitureType>().Where(a => a.ParentId == f.Id && a.Articles.Any(pr => pr.Products.Count != 0)).Any())
                    .Select(f => new DtoFurnitureTypeWithDefaultProductResult
                    {
                        Id = f.Id,
                        Alias = f.Alias,
                        Name = f.Name,
                        DefaultProductId = context.Set<FurnitureType>()
                        .Where(ft => ft.ParentId == f.Id)
                        .Where(ft=>ft.Articles.Any(a=>a.Products.Count != 0))
                        .SelectMany(ft=>ft.Articles).SelectMany(p=>p.Products)
                        .OrderByDescending(p=>p.Range).Select(p=>p.Id).FirstOrDefault()
                        /* todo убрать после проверке */
                        //DefaultProductId = f.Articles
                        //    .SelectMany(a => a.Products)
                        //    .OrderByDescending(p => p.Range)
                        //    .Select(p => p.Id)
                        //    .FirstOrDefault()
                    }).ToList();

                return Task.FromResult(furnitureTypesList);
            }
        }
    }
}
