﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.FurnitureTypes.QueryHandlers
{
    /// <summary>
    /// получение типов мебели для набора продуктов по фильтрам
    /// </summary>
    public class GetFurnitureTypesByProductRequestQueryHandler : Grain, IHubQueryHandler<GetFurnitureTypesByProductRequestQuery, List<DtoFurnitureTypeItem>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetFurnitureTypesByProductRequestQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        /// <summary>
        /// получение типов мебели для набора продуктов по фильтрам
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public Task<List<DtoFurnitureTypeItem>> Execute(GetFurnitureTypesByProductRequestQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var products = context.Set<Product>().AsQueryable();

                /* фильтр по варианту коллекции */
                if (query.CollectionVariantId.HasValue)
                    products = products.Where(p => p.CollectionsVariants.Any(c => c.Id == query.CollectionVariantId));
                /* фильтр по коллекции */
                if (query.CollectionId.HasValue)
                    products = products.Where(p => p.CollectionId == query.CollectionId);

                /* TODO отдавать еще продукты, у которых отсутствует один из цветов, но совпадают два других */
                /* фильтр по цвету отделки.  */
                if (query.FacingColorIds != null && query.FacingColorIds.Any())
                    products = products.Where(p => query.FacingColorIds.Any(f => f.Value == p.FacingColorId) || p.FacingColor.IsUniversal || p.FacingColorId == null);

                if (query.FacingIds != null && query.FacingIds.Any())
                    products = products.Where(p => query.FacingIds.Any(f => f.Value == p.FacingId));

                if (query.FrameColorIds != null && query.FrameColorIds.Any())
                    products = products.Where(p => query.FrameColorIds.Any(f => f.Value == p.FrameColorId));

                if (query.FurnitureTypeId.HasValue)
                    products = products.Where(p => p.Article.FurnitureTypeId == query.FurnitureTypeId);

                if (!string.IsNullOrWhiteSpace(query.FurnitureAlias))
                {
                    var furnitureType = context.Set<FurnitureType>().Where(f => f.Alias == query.FurnitureAlias).FirstOrDefault();
                    products = products.Where(p => p.Article.FurnitureType.ParentId == furnitureType.ParentId);
                }

                var furnitureTypes = products
                    .Select(p => p.Article)
                    .Select(a => a.FurnitureType)
                    .Select(f => new DtoFurnitureTypeItem
                    {
                        Alias = f.Alias,
                        Id = f.Id,
                        Name = f.Name,
                        SyncCode1C = f.SyncCode1C
                    }).Distinct().ToList();

                return Task.FromResult(furnitureTypes);
            }
        }
    }
}
