﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes;
using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.FurnitureTypes.QueryHandlers
{
    public class GetFurnitureTypesByCollectionQueryHandler : Grain, IHubQueryHandler<GetFurnitureTypesByCollectionQuery, List<DtoFurnitureTypeResult>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetFurnitureTypesByCollectionQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoFurnitureTypeResult>> Execute(GetFurnitureTypesByCollectionQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var furnitureTypesList = new List<DtoFurnitureTypeResult>();
                var furnitureTypesSet = context.Set<FurnitureType>().AsQueryable();
                var productsSet = context.Set<Product>().AsQueryable();

                /* фильтр по цвету отделки.  */
                if (query.FacingColorIds != null && query.FacingColorIds.Any())
                    productsSet = productsSet.Where(p => query.FacingColorIds.Contains(p.FacingColor.Id) || p.FacingColorId == null || p.FacingColor.IsUniversal);
                /* фильтр по отделке */
                if (query.FacingIds != null && query.FacingIds.Any())
                    productsSet = productsSet.Where(p => query.FacingIds.Contains(p.Facing.Id));
                /* фильтр по цвету корпуса */
                if (query.FrameColorIds != null && query.FrameColorIds.Any())
                    productsSet = productsSet.Where(p => query.FrameColorIds.Contains(p.FrameColor.Id));


                if (query.CollectionId.HasValue)
                {
                    furnitureTypesSet = furnitureTypesSet.Where(f => f.Articles.Any(article => article.Products.Any(product => product.CollectionsVariants.Any(cv => cv.CollectionId == query.CollectionId.Value))));
                    furnitureTypesList = furnitureTypesSet.Select(f => new DtoFurnitureTypeResult
                    {
                        Id = f.Id,
                        Name = f.Name,
                        NameInPlural = f.NameInPlural,
                        Amount = productsSet
                        .Where(p => p.CollectionsVariants.Any(c => c.CollectionId == query.CollectionId))
                        .Where(p => p.Article.FurnitureTypeId == f.Id).Select(p => p.Article.Name)
                        .Distinct().Count()
                    }).Distinct().ToList();
                }
                else
                {
                    furnitureTypesSet = furnitureTypesSet.Where(f => f.Articles.Any(a => a.Products.Any(p => p.CollectionsVariants.Any(c => c.Collection.CollectionVariants.Any(cv => cv.Id == query.CollectionVariantId)))));
                    furnitureTypesList = furnitureTypesSet.Select(f => new DtoFurnitureTypeResult
                    {
                        Id = f.Id,
                        Name = f.Name,
                        NameInPlural = f.NameInPlural,
                        Amount = productsSet
                        .Where(p => p.CollectionsVariants.Any(c => c.Id == query.CollectionVariantId))
                        .Where(p => p.Article.FurnitureTypeId == f.Id).Select(p => p.Article.Name).Distinct().Count()
                    }).Distinct().ToList();
                }
                return Task.FromResult(furnitureTypesList);
            }
        }
    }
}
