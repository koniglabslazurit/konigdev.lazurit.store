﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Collections;
using KonigDev.Lazurit.Model.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants;
using Orleans;
using KonigDev.Lazurit.Hub.Model.DTO.Facings;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors;
using KonigDev.Lazurit.Hub.Model.DTO.Products;
using KonigDev.Lazurit.Hub.Model.DTO.Styles.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Facings.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Dto;

namespace KonigDev.Lazurit.Hub.Grains.Collections.QueryHandlers
{
    public class GetCollectionsByRequestQueryHandler : Grain, IHubQueryHandler<GetCollectionsQuery, List<DtoCollectionResultForCatalog>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetCollectionsByRequestQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoCollectionResultForCatalog>> Execute(GetCollectionsQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var collections = context.Set<Collection>()
                    .Where(x => x.RoomType.Alias == query.RoomAlias)
                    .Select(x => new DtoCollectionResultForCatalog
                    {
                        Id = x.Id,
                        Name = x.Series.Name,
                        Room = x.RoomType.Name,
                        Alias = x.Series.Alias,
                        Range = x.Range,
                        Variants = x.CollectionVariants
                        .Where(p => p.Products.Any())
                        .Select(v => new DtoCollectionVariantResultForCatalog
                        {
                            Id = v.Id,
                            CollectionId = v.CollectionId,

                            Facings = v.Products.Select(p => p.Facing)
                            .Select(s => new { s.Id, s.Name, s.SyncCode1C })
                            .Distinct()
                            .Select(a => new DtoFacingItem
                            {
                                Id = a.Id,
                                Name = a.Name,
                                SyncCode1C = a.SyncCode1C
                            }),

                            FacingColors = v.Products.Where(p => p.FacingColorId != null)
                            .Select(p => p.FacingColor)
                            .Select(s => new { s.Id, s.Name, s.SyncCode1C })
                            .Distinct()
                            .Select(a => new DtoFacingColorItem
                            {
                                Id = a.Id,
                                Name = a.Name,
                                SyncCode1C = a.SyncCode1C
                            }),

                            FrameColors = v.Products.Select(p => p.FrameColor)
                            .Select(s => new { s.Id, s.Name, s.SyncCode1C })
                            .Distinct()
                            .Select(a => new DtoFrameColorItem
                            {
                                Id = a.Id,
                                Name = a.Name,
                                SyncCode1C = a.SyncCode1C
                            }),

                            Styles = v.Products.SelectMany(p => p.Styles)
                            .Select(s => new { s.Id, s.Name, s.SyncCode1C })
                            .Distinct()
                            .Select(a => new DtoStyleItem
                            {
                                Id = a.Id,
                                Name = a.Name,
                                SyncCode1C = a.SyncCode1C
                            }).ToList(),

                            TargetAudiences = v.Products.SelectMany(p => p.TargetAudiences)
                            .Select(ta => new { ta.Id, ta.Name, ta.SyncCode1C })
                            .Distinct()
                            .Select(a => new DtoTargetAudienceItem
                            {
                                Id = a.Id,
                                Name = a.Name,
                                SyncCode1C = a.SyncCode1C
                            })
                        })
                    }).OrderByDescending(x => x.Range);

                return Task.FromResult(collections.ToList());
            }
        }
    }
}