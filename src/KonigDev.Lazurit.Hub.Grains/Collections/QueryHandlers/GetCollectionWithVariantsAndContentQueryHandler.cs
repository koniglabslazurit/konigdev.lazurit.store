﻿using KonigDev.Lazurit.Core.Enums;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Query;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors;
using KonigDev.Lazurit.Hub.Model.DTO.Facings;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Collections.QueryHandlers
{
    [Serializable]
    public class GetCollectionWithVariantsAndContentQueryHandler : Grain, IHubQueryHandler<GetCollectionWithVariantsAndContentQuery, DtoCollectionWithContent>
    {
        private readonly IDBContextFactory _contextFactory;

        public GetCollectionWithVariantsAndContentQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public async Task<DtoCollectionWithContent> Execute(GetCollectionWithVariantsAndContentQuery query)
        {
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var result = context.Set<Collection>()
                    .Where(collection => collection.Series.Alias == query.SeriesAlias)
                    .Where(collection => collection.RoomType.Alias == query.RoomTypeAlias)
                    .Select(collection => new DtoCollectionWithContent
                    {
                        Id = collection.Id,
                        RoomType = collection.RoomType.Name,
                        SeriesName = collection.Series.Name,
                        FacingColors = collection.CollectionVariants.SelectMany(cv => cv.Products)
                        //cuz FC can be null
                        .Where(p => p.FacingColor != null)
                        .Select(p => new DtoFacingColorResult
                        {
                            Id = p.FacingColor.Id,
                            Name = p.FacingColor.Name
                        }).Distinct(),
                        Facings = collection.CollectionVariants.SelectMany(cv => cv.Products)
                        .Select(p => new DtoFacingsResult
                        {
                            Id = p.Facing.Id,
                            Name = p.Facing.Name
                        }).Distinct(),
                        FrameColors = collection.CollectionVariants.SelectMany(cv => cv.Products)
                        .Select(p => new DtoFrameColorResult
                        {
                            Id = p.FrameColor.Id,
                            Name = p.FrameColor.Name
                        }).Distinct(),
                        Variants = collection.CollectionVariants.Select(variant => new DtoCollectionVariantContent
                        {
                            CollectionId = collection.Id,
                            Id = variant.Id,
                            CollectionTypeName = variant.IsComplect ? EnumCollectionTypes.Komplekt.ToString() : EnumCollectionTypes.Collection.ToString(),
                            FacingColors = variant.Products
                            //cuz FC can be null
                            .Where(p => p.FacingColor != null)
                            .Select(p => new DtoFacingColorResult
                            {
                                Id = p.FacingColor.Id,
                                Name = p.FacingColor.Name
                            }).Distinct(),
                            Facings = variant.Products
                            .Select(p => new DtoFacingsResult
                            {
                                Id = p.Facing.Id,
                                Name = p.Facing.Name
                            }).Distinct(),
                            FrameColors = variant.Products
                            .Select(p => new DtoFrameColorResult
                            {
                                Id = p.FrameColor.Id,
                                Name = p.FrameColor.Name
                            }).Distinct(),
                            IsComplect = variant.IsComplect,
                            IsFavorite = variant.FavoriteItemCollections.Any(f => f.CollectionVariantId == variant.Id && f.FavoriteId == query.UserId)
                            || variant.FavoriteItemComplects.Any(f => f.CollectionVariantId == variant.Id && f.FavoriteId == query.UserId),
                            //TODO: on collection page products is taken from another api. If there is no other pages wich use this grain and this field, then we should delete this field
                            Products = variant.CollectionVariantContentProductAreas.Select(content => new DtoProductContent
                            {
                                Id = content.Id,
                                SyncCode1C = content.Product.SyncCode1C,
                                ProductId = content.ProductId,
                                IsAssemblyRequired = content.Product.IsAssemblyRequired,
                                ArticleName = content.Product.Article.Name,
                                ArticleSyncCode1c = content.Product.Article.SyncCode1C,
                                ContentHeight = content.Height,
                                ContentLeft = content.Left,
                                ContentTop = content.Top,
                                ContentWidth = content.Width,
                                FurnitureTypeAlias = content.Product.Article.FurnitureType.Alias,
                                FurnitureTypeName = content.Product.Article.FurnitureType.Name,
                                IsEnabled = content.IsEnabled,
                                IsFavorite = content.Product.FavoriteItemProducts.Any(p => p.FavoriteId == query.UserId),
                                ObjectId = content.Id
                            }),
                        })
                    });

                return await Task.FromResult(result.FirstOrDefault());
            }
        }
    }
}
