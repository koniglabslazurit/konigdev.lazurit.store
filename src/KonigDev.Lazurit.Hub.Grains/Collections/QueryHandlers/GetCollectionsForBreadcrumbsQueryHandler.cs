﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Collections;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Collections.QueryHandlers
{
    [Serializable]
    public class GetCollectionsForBreadcrumbsQueryHandler : Grain, IHubQueryHandler<GetCollectionsForBreadcrumbsQuery, List<DtoCollectionItemBreadcrumbs>>
    {
        private readonly IDBContextFactory _contextFactory;

        public GetCollectionsForBreadcrumbsQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public Task<List<DtoCollectionItemBreadcrumbs>> Execute(GetCollectionsForBreadcrumbsQuery query)
        {
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var result = context.Set<Collection>()
                    .Where(p =>p.RoomType.Alias == query.RoomAlias)
                    .Select(p => new DtoCollectionItemBreadcrumbs
                    {
                        Id = p.Id,
                        Name = p.Series.Name,
                        Alias = p.Series.Alias
                    }).ToList();
                return Task.FromResult(result);
            }
        }
    }
}
