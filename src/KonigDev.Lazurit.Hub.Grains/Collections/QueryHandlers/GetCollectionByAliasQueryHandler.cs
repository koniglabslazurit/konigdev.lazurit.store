﻿using System;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Collections;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;

namespace KonigDev.Lazurit.Hub.Grains.Collections.QueryHandlers
{
    public class GetCollectionByAliasQueryHandler : Grain, IHubQueryHandler<GetCollectionByAliasQuery, DtoCollectionByAliasResult>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetCollectionByAliasQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<DtoCollectionByAliasResult> Execute(GetCollectionByAliasQuery query)
        {
            if (string.IsNullOrEmpty(query.Alias)) throw new NotSupportedException();
            using (var context = _dbContextFactory.CreateLazuritContext())
            {

                var articleInfo = context.Set<Collection>()
                    .SingleOrDefault(x => x.Series.Alias == query.Alias);
                if (articleInfo != null)
                    return Task.FromResult(new DtoCollectionByAliasResult
                    {
                        Id = articleInfo.Id,
                        Name = articleInfo.RoomType.Name + " " +articleInfo.Series.Name,
                        Alias = articleInfo.Series.Alias,
                    });                        
                return Task.FromResult<DtoCollectionByAliasResult>(null);
            }
        }
    }
}