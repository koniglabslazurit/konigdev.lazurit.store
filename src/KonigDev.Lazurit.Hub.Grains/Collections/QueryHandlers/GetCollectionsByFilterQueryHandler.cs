﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Collections;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Query;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors;
using KonigDev.Lazurit.Hub.Model.DTO.Facings;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants;

namespace KonigDev.Lazurit.Hub.Grains.Collections.QueryHandlers
{
    public class GetCollectionsByFilterQueryHandler : Grain, IHubQueryHandler<GetCollectionsByFilterQuery, List<DtoCollectionResult>>
    {
        private readonly IDBContextFactory _dbContextFactory;
        public GetCollectionsByFilterQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public Task<List<DtoCollectionResult>> Execute(GetCollectionsByFilterQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var collections = context.Set<Collection>()
                    .Where(x => x.RoomTypeId == query.RoomTypeId);

                if (query.FacingColorId.HasValue)
                    collections = collections.Where(x => x.CollectionVariants.Any(c => c.FacingColorId == query.FacingColorId));

                if (query.FacingId.HasValue)
                    collections = collections.Where(x => x.CollectionVariants.Any(c => c.FacingId == query.FacingId));

                if (query.FrameColorId.HasValue)
                    collections = collections.Where(x => x.CollectionVariants.Any(c => c.FrameColorId == query.FrameColorId));

                var collectionsList = collections.Select(x => new DtoCollectionResult
                {
                    Id = x.Id,
                    Name = x.Series.Name,
                    Room = x.RoomType.Name,
                    Variants = x.CollectionVariants.Select(v => new DtoCollectionVariantResult
                    {
                        Id = v.Id,
                        CollectionId = v.CollectionId,
                        Facing = new DtoFacingsResult
                        {
                            Id = v.Facing.Id,
                            Name = v.Facing.Name
                        },
                        FacingColor = new DtoFacingColorResult
                        {
                            Id = v.Facing.Id,
                            Name = v.Facing.Name
                        },
                        FrameColor = new DtoFrameColorResult
                        {
                            Id = v.Facing.Id,
                            Name = v.Facing.Name
                        }
                    })
                }).ToList();
                return Task.FromResult(collectionsList);
            }
        }
    }
}
