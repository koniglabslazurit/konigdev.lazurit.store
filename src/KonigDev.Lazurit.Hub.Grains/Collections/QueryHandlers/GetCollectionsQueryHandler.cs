﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Collections;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Collections.QueryHandlers
{
    public class GetCollectionsQueryHandler : Grain, IHubQueryHandler<GetCollections, List<DtoCollectionItem>>
    {
        private readonly IDBContextFactory _contextFactory;

        public GetCollectionsQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public Task<List<DtoCollectionItem>> Execute(GetCollections query)
        {
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var result = context.Set<Collection>()
                    .Select(p => new DtoCollectionItem
                    {
                        Id = p.Id,
                        Room = p.RoomType.SyncCode1C,
                        Series = p.Series.SyncCode1C
                    }).ToList();
                return Task.FromResult(result);
            }
        }
    }
}
