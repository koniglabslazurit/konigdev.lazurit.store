﻿using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using Orleans;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Transactions;
using KonigDev.Lazurit.Hub.Model.DTO.Collections.Commands;
using System;

namespace KonigDev.Lazurit.Hub.Grains.Collections.CommandHandlers
{
    public class UpdateCollectionCommandHandler : Grain, IHubCommandHandler<UpdateCollectionCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;

        public UpdateCollectionCommandHandler(IDBContextFactory contextFactory, ITransactionFactory transactionFactory)
        {
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(UpdateCollectionCommand command)
        {
            var query = "";
            foreach (var item in command.Collections)
            {
                query += $"UPDATE Collections SET Range = '{item.RangeCollection}' WHERE Id = '{item.Id}';";
            }

            using (IDbConnection connection = _contextFactory.CreateDbConnection("LazuritBaseContext"))
            {
                connection.Open();

                TransactionOptions to = new TransactionOptions();
                to.IsolationLevel = System.Transactions.IsolationLevel.Snapshot;
                using (var transaction = _transactionFactory.CreateScope(TransactionScopeOption.Required, to))
                {
                    try
                    {
                        connection.Execute(query, transaction);
                        transaction.Complete();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }
    }
}
