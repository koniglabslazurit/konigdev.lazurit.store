﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using System;
using KonigDev.Lazurit.Model.Entities;
using KonigDev.Lazurit.Hub.Model.DTO.Facings;
using KonigDev.Lazurit.Hub.Model.DTO.Facings.Query;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using Orleans;

namespace KonigDev.Lazurit.Hub.Grains.Facings.QueryHandlers
{
    public class GetFacingsByCollectionQueryHandler : Grain, IHubQueryHandler<GetFacingsByCollectionQuery, List<DtoFacingsResult>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetFacingsByCollectionQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoFacingsResult>> Execute(GetFacingsByCollectionQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var facingsList = new List<DtoFacingsResult>();
                if (query.CollectionId.HasValue)
                {
                    var facingsByCollectionId = context.Set<Facing>()
                        .Where(p => p.CollectionsVariants
                        .Any(s => s.CollectionId == query.CollectionId))
                        .Select(p => new DtoFacingsResult
                        {
                            Id = p.Id,
                            Name = p.Name
                        });
                    facingsList.AddRange(facingsByCollectionId);
                }
                return Task.FromResult(facingsList.Distinct().ToList());
            }
        }
    }
}
