﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Facings;
using KonigDev.Lazurit.Hub.Model.DTO.Facings.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Facings.QueryHandlers
{
   public class GetFacingsByRoomTypeIdQueryHandler : Grain, IHubQueryHandler<GetFacingsByRoomTypeIdQuery, List<DtoFacingsResult>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetFacingsByRoomTypeIdQueryHandler (IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoFacingsResult>> Execute(GetFacingsByRoomTypeIdQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var collectionIds = context.Set<Collection>().Where(x => x.RoomTypeId == query.RoomTypeId).Select(p => p.Id).ToArray();
                var facingsList = new List<DtoFacingsResult>();

                foreach (var collectionId in collectionIds)
                {
                    var facings = context.Set<Facing>()
                        .Where(p => p.CollectionsVariants.Any(s => s.Id == collectionId))
                        .Select(p => new DtoFacingsResult
                    {
                        Id = p.Id,
                        Name = p.Name
                    });
                    facingsList.AddRange(facings);
                }
                return Task.FromResult(facingsList.Distinct().ToList());
            }
        }
    }
}
