﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using System;
using KonigDev.Lazurit.Model.Entities;
using KonigDev.Lazurit.Hub.Model.DTO.Facings.Query;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using Orleans;
using KonigDev.Lazurit.Hub.Model.DTO.Facings.Dto;

namespace KonigDev.Lazurit.Hub.Grains.Facings.QueryHandlers
{
    [Serializable]
    public class GetFacingsQueryHandler : Grain, IHubQueryHandler<GetFacingsQuery, List<DtoFacingItem>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetFacingsQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoFacingItem>> Execute(GetFacingsQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var result = context.Set<Facing>()                                        
                    .Select(p => new DtoFacingItem
                    {
                        Id = p.Id,
                        Name = p.Name,
                        SyncCode1C = p.SyncCode1C
                    }).ToList();

                return Task.FromResult(result);
            }
        }
    }
}
