﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Facings.Exceptions
{
   public class FacingNotFoundException:Exception
    {
        public FacingNotFoundException(string message) : base(message)
        {
        }
    }
}
