﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.TargetAudiencesGrains.QueryHandlers
{

    public class GetTargetAudiencesQueryHandler : Grain, IHubQueryHandler<GetTargetAudiencesQuery, List<DtoTargetAudienceItem>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetTargetAudiencesQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public Task<List<DtoTargetAudienceItem>> Execute(GetTargetAudiencesQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var result = context.Set<TargetAudience>()
                    .Select(p => new DtoTargetAudienceItem
                    {
                        Id = p.Id,
                        Name = p.Name,
                        SyncCode1C = p.SyncCode1C
                    }).ToList();

                return Task.FromResult(result);
            }
        }
    }
}
