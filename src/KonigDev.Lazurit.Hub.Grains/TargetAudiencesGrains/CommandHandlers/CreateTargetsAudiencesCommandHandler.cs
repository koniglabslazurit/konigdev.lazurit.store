﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using Orleans;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Core.Transactions;
using System.Transactions;
using System;
using KonigDev.Lazurit.Model.Entities;
using KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Command;
using System.Data;
using Dapper;

namespace KonigDev.Lazurit.Hub.Grains.TargetAudiencesGrains.CommandHandlers
{
    public class CreateTargetsAudiencesCommandHandler : Grain, IHubCommandHandler<CreateTargetsAudiencesCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;

        public CreateTargetsAudiencesCommandHandler(IDBContextFactory contextFactory, ITransactionFactory transactionFactory)
        {
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(CreateTargetsAudiencesCommand command)
        {
            var query = "";
            foreach (var item in command.TargetAudiences)
            {
                query += $"INSERT INTO TargetAudiences(Id,SyncCode1C,Name) VALUES('{item.Id}',N'{item.SyncCode1C}',N'{item.Name}');";
            }

            using (IDbConnection connection = _contextFactory.CreateDbConnection("LazuritBaseContext"))
            {
                connection.Open();
                TransactionOptions to = new TransactionOptions();
                to.IsolationLevel = System.Transactions.IsolationLevel.Snapshot;
                using (var transaction = _transactionFactory.CreateScope(TransactionScopeOption.Required, to))
                {
                    try
                    {
                        connection.Execute(query, transaction);
                        transaction.Complete();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }            
        }
    }
}
