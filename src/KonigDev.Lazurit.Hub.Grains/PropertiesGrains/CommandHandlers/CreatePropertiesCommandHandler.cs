﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Core.Transactions;
using System.Transactions;
using System;
using KonigDev.Lazurit.Hub.Model.DTO.Property.Command;
using System.Data;
using Dapper;

namespace KonigDev.Lazurit.Hub.Grains.PropertiesGrains.CommandHandlers
{
    [Serializable]
    public class CreatePropertiesCommandHandler : Grain, IHubCommandHandler<CreatePropertiesCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;
        public CreatePropertiesCommandHandler(IDBContextFactory contextFactory, ITransactionFactory transactionFactory)
        {
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }
        public async Task Execute(CreatePropertiesCommand command)
        {
            var query = "";
            foreach (var item in command.Properties)
            {
                query += $"INSERT INTO Properties(Id,SyncCode1C,Title ,Value,IsMultiValue,GroupeId,IsRequired,SortNumber)VALUES('{item.Id}',N'{item.SyncCode1C}',N'{item.Title}',N'{item.Value}',{Convert.ToInt32(item.IsMultiValue)},{(item.GroupeId.HasValue ? "'" + item.GroupeId + "'" : "null")},0,0);";
            }

            using (IDbConnection connection = _contextFactory.CreateDbConnection("LazuritBaseContext"))
            {
                connection.Open();
                TransactionOptions to = new TransactionOptions();
                to.IsolationLevel = System.Transactions.IsolationLevel.Snapshot;
                using (var transaction = _transactionFactory.CreateScope(TransactionScopeOption.Required, to))
                {
                    try
                    {
                        connection.Execute(query, transaction);
                        transaction.Complete();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }
    }
}
