﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using System;
using KonigDev.Lazurit.Model.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using Orleans;
using KonigDev.Lazurit.Hub.Model.DTO.Property.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Property.Query;

namespace KonigDev.Lazurit.Hub.Grains.PropertiesGrains.QueryHandlers
{
    public class GetPropertiesQueryHandler : Grain, IHubQueryHandler<GetPropertiesQuery, List<DtoPropertyItem>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetPropertiesQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoPropertyItem>> Execute(GetPropertiesQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var resultQuery = context.Set<Property>().AsQueryable();
                if (!string.IsNullOrEmpty(query.PropertyType))
                    /* тип проперти берем по коду 1с, код 1с - наименование поля в 1С  */
                    resultQuery = resultQuery.Where(p => p.SyncCode1C == query.PropertyType);

                var result = resultQuery
                    .Select(p => new DtoPropertyItem
                    {
                        Id = p.Id,
                        SyncCode1C = p.SyncCode1C,
                        Value = p.Value,
                        Title = p.Title,
                        GroupeId = p.GroupeId,
                        IsMultiValue = p.IsMultiValue
                    }).ToList();

                return Task.FromResult(result);
            }
        }
    }
}
