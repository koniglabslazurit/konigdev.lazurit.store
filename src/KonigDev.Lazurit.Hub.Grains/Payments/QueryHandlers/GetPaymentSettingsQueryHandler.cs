﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Payment.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Payment.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Payments.QueryHandlers
{
    public class GetPaymentSettingsQueryHandler : Grain, IHubQueryHandler<GetPaymentSettingsQuery, DtoPaymentSettings>
    {
        private readonly IDBContextFactory _contextFactory;
        public GetPaymentSettingsQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public Task<DtoPaymentSettings> Execute(GetPaymentSettingsQuery query)
        {
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var allSettings = context.Set<Settings>()
                    .Select(x => new { x.Id, x.Name, x.Title, x.Value})
                    .ToList();
                var paymentSettings = new DtoPaymentSettings
                {
                    RobokassaUrl = allSettings.Where(x => x.Name == "RobokassaUrl").Select(x => x.Value).SingleOrDefault(),
                    MerchantLogin = allSettings.Where(x => x.Name == "RobokassaMerchantLogin").Select(x => x.Value).SingleOrDefault(),
                    Password1 = allSettings.Where(x => x.Name == "RobokassaPassword1").Select(x => x.Value).SingleOrDefault(),
                    Password2 = allSettings.Where(x => x.Name == "RobokassaPassword2").Select(x => x.Value).SingleOrDefault()
                };
                return Task.FromResult(paymentSettings);
            }
        }
    }
}
