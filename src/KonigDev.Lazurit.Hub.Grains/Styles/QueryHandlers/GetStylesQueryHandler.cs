﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Styles.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Styles.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.Styles.QueryHandlers
{
    public class GetStylesQueryHandler : Grain, IHubQueryHandler<GetStylesQuery, List<DtoStyleItem>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetStylesQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public Task<List<DtoStyleItem>> Execute(GetStylesQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var result = context.Set<Style>()
                    .Select(p => new DtoStyleItem
                    {
                        Id = p.Id,
                        Name = p.Name,
                        SyncCode1C = p.SyncCode1C
                    }).ToList();

                return Task.FromResult(result);
            }
        }
    }
}
