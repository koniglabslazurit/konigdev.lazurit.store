﻿using KonigDev.Lazurit.Core.Enums.Enums.CustomerRequests;
using KonigDev.Lazurit.Core.Extensions;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.CustomerRequests.Dtos;
using KonigDev.Lazurit.Hub.Model.DTO.CustomerRequests.Queries;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.CustomerRequests.QueryHandlers
{
    public class GetAllCustomerRequestsQueryHandler : Grain, IHubQueryHandler<GetAllCustomerRequestsQuery, DtoCustomerRequests>
    {
        private readonly IDBContextFactory _contextFactory;

        public GetAllCustomerRequestsQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public Task<DtoCustomerRequests> Execute(GetAllCustomerRequestsQuery query)
        {
            return Task.Run(() =>
            {
                using (var context = _contextFactory.CreateLazuritContext())
                {
                    var customerRequests = context
                        .Set<CustomerRequest>()
                        .OrderByDescending(p => p.CreationTime)
                        .Select(p => new DtoCustomerRequest
                        {
                            Id = p.Id,
                            CreationTime = p.CreationTime,
                            CustomerFullName = p.CustomerFullName,
                            Email = p.Email,
                            Phone = p.Phone,
                            TypeTechName = p.Type,
                            StatusTechName = p.Status
                        })
                        .ToList();

                    foreach (var customerRequest in customerRequests)
                    {
                        customerRequest.Type = customerRequest.TypeTechName
                            .GetEnumValueDescription<EnumCustomerRequestType>();
                    }

                    return new DtoCustomerRequests
                    {
                        CustomerRequests = customerRequests,
                        CustomerRequestStatuses = GetCustomerRequestStatuses()
                    };
                }
            });
        }

        private List<DtoCustomerRequestStatus> GetCustomerRequestStatuses()
        {
            return Enum
                .GetValues(typeof(EnumCustomerRequestStatus))
                .Cast<EnumCustomerRequestStatus>()
                .Select(p => new DtoCustomerRequestStatus
                {
                    TechName = p.ToString(),
                    Name = p.GetDescription()
                })
                .ToList();
        }
    }
}
