﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.CustomerRequests.Commands;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.CustomerRequests.CommandHandlers
{
    public class UpdateCustomerRequestStatusCommandHandler : Grain, IHubCommandHandler<UpdateCustomerRequestStatusCommand>
    {
        private readonly IDBContextFactory _contextFactory;

        public UpdateCustomerRequestStatusCommandHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public async Task Execute(UpdateCustomerRequestStatusCommand command)
        {
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var request = context
                    .Set<CustomerRequest>()
                    .FirstOrDefault(p => p.Id == command.Id);

                if (request != null)
                {
                    request.Status = command.Status;

                    await context.SaveChangesAsync();
                }
            }
        }
    }
}
