﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.CustomerRequests.Commands;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.CustomerRequests.CommandHandlers
{
    public class RemoveCustomerRequestCommandHandler : Grain, IHubCommandHandler<RemoveCustomerRequestCommand>
    {
        private readonly IDBContextFactory _contextFactory;

        public RemoveCustomerRequestCommandHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public async Task Execute(RemoveCustomerRequestCommand command)
        {
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var request = context
                    .Set<CustomerRequest>()
                    .FirstOrDefault(p => p.Id == command.Id);

                if (request != null)
                {
                    context
                        .Set<CustomerRequest>()
                        .Remove(request);

                    await context.SaveChangesAsync();
                }
            }
        }
    }
}
