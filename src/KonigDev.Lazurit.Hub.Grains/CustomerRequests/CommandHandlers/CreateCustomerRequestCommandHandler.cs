﻿using KonigDev.Lazurit.Core.Enums.Enums.CustomerRequests;
using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.CustomerRequests.Commands;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.CustomerRequests.CommandHandlers
{
    public class CreateCustomerRequestCommandHandler : Grain, IHubCommandHandler<CreateCustomerRequestCommand>
    {
        private readonly IDBContextFactory _contextFactory;

        public CreateCustomerRequestCommandHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public async Task Execute(CreateCustomerRequestCommand command)
        {
            using (var context = _contextFactory.CreateLazuritContext())
            {
                context
                    .Set<CustomerRequest>()
                    .Add(new CustomerRequest
                    {
                        Id = Guid.NewGuid(),
                        CreationTime = DateTime.Now,
                        CustomerFullName = command.CustomerFullName,
                        Email = command.Email,
                        Phone = command.Phone,
                        Type = command.Type,
                        Status = EnumCustomerRequestStatus.Queued.ToString()
                    });
                await context.SaveChangesAsync();
            }
        }
    }
}
