﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Commands;
using Orleans;
using System.Threading.Tasks;
using KonigDev.Lazurit.Core.Transactions;
using System.Transactions;
using System;
using System.Data;
using Dapper;

namespace KonigDev.Lazurit.Hub.Grains.FrameColors.CommandHandlers
{
    public class CreateFrameColorsCommandHandler : Grain, IHubCommandHandler<CreateFrameColorsCommand>
    {
        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;
        public CreateFrameColorsCommandHandler(IDBContextFactory contextFactory, ITransactionFactory transactionFactory)
        {
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(CreateFrameColorsCommand command)
        {
            var query = "";
            foreach (var item in command.FrameColors)
            {
                query += $" INSERT INTO FrameColors (Id,Name,SyncCode1C) VALUES('{item.Id}',N'{item.Name}',N'{item.SyncCode1C}');";
            }

            using (IDbConnection connection = _contextFactory.CreateDbConnection("LazuritBaseContext"))
            {
                connection.Open();
                TransactionOptions to = new TransactionOptions();
                to.IsolationLevel = System.Transactions.IsolationLevel.Snapshot;
                using (var transaction = _transactionFactory.CreateScope(TransactionScopeOption.Required, to))
                {
                    try
                    {
                        connection.Execute(query, transaction);
                        transaction.Complete();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }
    }
}
