﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Filters.Query;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Dto;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.FrameColors.QueryHandlers
{
    public class GetFrameColorsByProductRequestQueryHandler : Grain, IHubQueryHandler<GetFiltersByProductRequestQuery, List<DtoFrameColorItem>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetFrameColorsByProductRequestQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoFrameColorItem>> Execute(GetFiltersByProductRequestQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var products = context.Set<Product>().AsQueryable();

                /* фильтр по варианту коллекции */
                if (query.CollectionVariantId.HasValue)
                    products = products.Where(p => p.CollectionsVariants.Any(c => c.Id == query.CollectionVariantId));
                /* фильтр по коллекции */
                if (query.CollectionId.HasValue)
                    products = products.Where(p => p.CollectionId == query.CollectionId);

                /* TODO отдавать еще продукты, у которых отсутствует один из цветов, но совпадают два других */
                /* фильтр по цвету отделки.  */
                if (query.FacingColorIds != null && query.FacingColorIds.Any())
                    products = products.Where(p => query.FacingColorIds.Any(f => f.Value == p.FacingColorId) || p.FacingColor.IsUniversal || p.FacingColorId == null);

                if (query.FacingIds != null && query.FacingIds.Any())
                    products = products.Where(p => query.FacingIds.Any(f => f.Value == p.FacingId));

                if (query.FrameColorIds != null && query.FrameColorIds.Any())
                    products = products.Where(p => query.FrameColorIds.Any(f => f.Value == p.FrameColorId));

                if (query.FurnitureTypeId.HasValue)
                    products = products.Where(p => p.Article.FurnitureTypeId == query.FurnitureTypeId);

                if (!string.IsNullOrWhiteSpace(query.FurnitureAlias))
                {
                    var furnitureType = context.Set<FurnitureType>().FirstOrDefault(f => f.Alias == query.FurnitureAlias);
                    if (furnitureType != null)
                    {
                        if (furnitureType.Id == furnitureType.ParentId)
                        {
                            products = products.Where(p => p.Article.FurnitureType.ParentId == furnitureType.ParentId);
                        }
                        else
                        {
                            products = products.Where(p => p.Article.FurnitureType.Id == furnitureType.Id);
                        }
                    }
                }

                var frameColors = products
                    .Select(p => new { p.FrameColor.Id, p.FrameColor.Name, p.FrameColor.SyncCode1C })
                    .Distinct()
                    .Select(p => new DtoFrameColorItem
                    {
                        Id = p.Id,
                        Name = p.Name,
                        SyncCode1C = p.SyncCode1C
                    }).Distinct().ToList();

                return Task.FromResult(frameColors);
            }
        }
    }
}
