﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.FrameColors.QueryHandlers
{
   public class GetFrameColorsByRoomTypeIdQueryHandler : Grain, IHubQueryHandler<GetFrameColorsByRoomTypeIdQuery, List<DtoFrameColorResult>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetFrameColorsByRoomTypeIdQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoFrameColorResult>> Execute(GetFrameColorsByRoomTypeIdQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var collectionIds = context.Set<Collection>().Where(p => p.RoomTypeId == query.RoomTypeId).Select(p => p.Id).ToArray();
               
                var frameColorsList = new List<DtoFrameColorResult>();
                foreach (var collectionId in collectionIds)
                {
                    var frameColors = context.Set<FrameColor>()
                        .Where(p => p.CollectionsVariants.Any(s => s.Id == collectionId))
                        .Select(p => new DtoFrameColorResult
                    {
                        Id = p.Id,
                        Name = p.Name
                    }).ToList();
                    frameColorsList.AddRange(frameColors);
                }
                return Task.FromResult(frameColorsList.Distinct().ToList());
            }
        }
    }
}
