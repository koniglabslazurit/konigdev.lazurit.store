﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using System;
using KonigDev.Lazurit.Model.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Factories;
using Orleans;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Query;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Dto;

namespace KonigDev.Lazurit.Hub.Grains.FrameColors.QueryHandlers
{
    [Serializable]
    public class GetFrameColorsQueryHandler : Grain, IHubQueryHandler<GetFrameColorQuery, List<DtoFrameColorItem>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetFrameColorsQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoFrameColorItem>> Execute(GetFrameColorQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var result = context.Set<FrameColor>()
                    .Select(p => new DtoFrameColorItem
                    {
                        Id = p.Id,
                        Name = p.Name,
                        SyncCode1C = p.SyncCode1C
                    }).ToList();

                return Task.FromResult(result);
            }
        }
    }
}
