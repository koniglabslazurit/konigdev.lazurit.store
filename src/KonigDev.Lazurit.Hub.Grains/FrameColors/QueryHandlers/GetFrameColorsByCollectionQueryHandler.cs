﻿using KonigDev.Lazurit.Hub.Grains.Factories;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Query;
using KonigDev.Lazurit.Model.Entities;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Grains.FrameColors.QueryHandlers
{
   public class GetFrameColorsByCollectionQueryHandler : Grain, IHubQueryHandler<GetFrameColorsByCollectionQuery, List<DtoFrameColorResult>>
    {
        private readonly IDBContextFactory _dbContextFactory;

        public GetFrameColorsByCollectionQueryHandler(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Task<List<DtoFrameColorResult>> Execute(GetFrameColorsByCollectionQuery query)
        {
            using (var context = _dbContextFactory.CreateLazuritContext())
            {
                var frameColorsList = new List<DtoFrameColorResult>();              
                if (query.CollectionId.HasValue)
                {
                    var frameColorsByCollectionId = context.Set<FrameColor>().Where(p => p.CollectionsVariants.Any(s => s.CollectionId == query.CollectionId))
                    .Select(p => new DtoFrameColorResult
                    {
                        Id = p.Id,
                        Name = p.Name
                    });
                    frameColorsList.AddRange(frameColorsByCollectionId);
                }
                return Task.FromResult(frameColorsList.Distinct().ToList());
            }
        }
    }
}
