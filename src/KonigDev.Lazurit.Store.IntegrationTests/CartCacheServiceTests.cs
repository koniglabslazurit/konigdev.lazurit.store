﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;
using KonigDev.Lazurit.Store.Dto.Cart.CartMini;
using NUnit.Framework;
using KonigDev.Lazurit.Store.Services.Cart.Implementations;

namespace KonigDev.Lazurit.Store.IntegrationTests
{

    [TestFixture]
    public class CartCacheServiceTests
    {
        private CartElasticCacheService _service;
        private Guid _firstUserId;
        private Guid _firstUserCartId;
        private DtoCartMiniPriceResult _firstUserCart;
        private Guid _secondUserId;
        private Guid _secondUserCartId;
        private DtoCartMiniPriceResult _secondUserCart;
        private const string _cartTestIndex = "testminicartindex";

        [SetUp]
        public void SetUp()
        {
            _service = new CartElasticCacheService("http", "13.74.185.150:9200", _cartTestIndex);
            _firstUserId = Guid.NewGuid();
            _firstUserCartId = Guid.NewGuid();
            _firstUserCart = new DtoCartMiniPriceResult { Id = _firstUserCartId };
            _secondUserId = Guid.NewGuid();
            _secondUserCartId = Guid.NewGuid();
            _secondUserCart = new DtoCartMiniPriceResult { Id = _secondUserCartId };
        }

        [Test]
        public async Task SaveDifferentCache_ShouldSaveBothCart_WhenCacheNotContainsKeys()
        {
            //arrange
            //act
            await _service.SaveCart(_firstUserCart, _firstUserCartId);
            await _service.SaveCart(_secondUserCart, _secondUserCartId);

            //assert
            var res1 = await _service.GetCart(_firstUserCartId);
            Assert.IsTrue(res1.Id == _firstUserCartId);
            var res2 = await _service.GetCart(_secondUserCartId);
            Assert.IsTrue(res2.Id == _secondUserCartId);
        }

        [Test]
        public async Task SaveSameCache_ShouldRewriteData_WhenCacheContainsKeysAlready()
        {
            //arrange
            _firstUserCart.Products = new List<DtoCartMiniItemPriceProduct>() { new DtoCartMiniItemPriceProduct() { Article = "Тест" } };
            //act
            await _service.SaveCart(_firstUserCart, _firstUserCartId);
            _firstUserCart.Products.Add(new DtoCartMiniItemPriceProduct() { Article = "Тест2" });
            await _service.SaveCart(_firstUserCart, _firstUserCartId);

            //assert
            var res = await _service.GetCart(_firstUserCartId);
            Assert.IsTrue(res.Id == _firstUserCartId);
            Assert.IsTrue(res.Products.Count == 2);
        }

        /// <summary>
        /// Если у нас есть корзина с UserId и с CartId,то если обновление корзины должно не влиять то, по какому ключу мы её достаём.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task RelateCartWithUser_AnyCart_CanRelateWithUser()
        {
            //arrange
            _firstUserCart.Products = new List<DtoCartMiniItemPriceProduct>() { new DtoCartMiniItemPriceProduct() { Article = "Тест" } };
            //act
            await _service.SaveCart(_firstUserCart, _firstUserCartId);
            await _service.RelateCartWithUser(_secondUserId, _firstUserCartId);
            //assert
            var res2 = await _service.GetCart(_firstUserCartId);
            Assert.IsTrue(_firstUserCart.Id == res2.Id);
            Assert.IsTrue(_firstUserCart.Products.Count == res2.Products.Count);
        }

        [Test]
        public async Task RemoveCache_ShouldRemoveDataByUserId_WhenCacheContainsKey()
        {
            //arrange
            await _service.SaveCart(_firstUserCart, _firstUserCartId);
            await _service.SaveCart(_secondUserCart, _secondUserCartId);
            //act
            _service.RemoveCart(_firstUserCartId);
            var res1 = await _service.GetCart(_firstUserCartId);
            Assert.IsNull(res1);
            var res2 = await _service.GetCart(_secondUserCartId);
            Assert.IsTrue(res2.Id == _secondUserCartId);
        }

        [TearDown]
        public void CleanElasticSearch()
        {
            _service.DeleteAllCartCache();
        }
    }
}
