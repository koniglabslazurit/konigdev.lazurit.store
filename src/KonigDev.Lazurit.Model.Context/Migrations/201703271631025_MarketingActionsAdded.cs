namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MarketingActionsAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MarketingActions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SeoId = c.Int(nullable: false, identity: true),
                        Discount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsVisible = c.Boolean(nullable: false),
                        FileId = c.String(),
                        Name = c.String(),
                        Code = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MarketingActions");
        }
    }
}
