namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFieldsInAddressAndProfile : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UsersProfiles", "MiddleName", c => c.String());
            AddColumn("dbo.UsersAddresses", "City", c => c.String());
            AddColumn("dbo.UsersAddresses", "Street", c => c.String());
            AddColumn("dbo.UsersAddresses", "Block", c => c.String());
            AddColumn("dbo.UsersAddresses", "Building", c => c.String());
            AddColumn("dbo.UsersAddresses", "House", c => c.String());
            AddColumn("dbo.UsersAddresses", "EntranceHall", c => c.String());
            AddColumn("dbo.UsersAddresses", "Floor", c => c.String());
            AddColumn("dbo.UsersAddresses", "Apartment", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UsersAddresses", "Apartment");
            DropColumn("dbo.UsersAddresses", "Floor");
            DropColumn("dbo.UsersAddresses", "EntranceHall");
            DropColumn("dbo.UsersAddresses", "House");
            DropColumn("dbo.UsersAddresses", "Building");
            DropColumn("dbo.UsersAddresses", "Block");
            DropColumn("dbo.UsersAddresses", "Street");
            DropColumn("dbo.UsersAddresses", "City");
            DropColumn("dbo.UsersProfiles", "MiddleName");
        }
    }
}
