namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UsersProfiles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        LinkId = c.Guid(nullable: false),
                        Title = c.String(),
                        Email = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        SyncCode1C = c.String(),
                        CityId = c.Guid(),
                        Showroom_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.CityId)
                .ForeignKey("dbo.Showrooms", t => t.Showroom_Id)
                .Index(t => t.CityId)
                .Index(t => t.Showroom_Id);
            
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(),
                        SyncCode1C = c.String(),
                        CodeKladr = c.String(),
                        Longitude = c.Single(nullable: false),
                        Latitude = c.Single(nullable: false),
                        DeliveryPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RegionId = c.Guid(nullable: false),
                        PriceZoneId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PriceZones", t => t.PriceZoneId)
                .ForeignKey("dbo.Regions", t => t.RegionId)
                .Index(t => t.RegionId)
                .Index(t => t.PriceZoneId);
            
            CreateTable(
                "dbo.PriceZones",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        SyncCode1C = c.String(),
                        Number = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Prices",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PriceForAssembly = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Discount = c.Decimal(precision: 18, scale: 2),
                        PriceZoneId = c.Guid(nullable: false),
                        ProductId = c.Guid(),
                        CollectionVariantId = c.Guid(),
                        CurrencyCode = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PriceZones", t => t.PriceZoneId)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.PriceZoneId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        SyncCode1C = c.String(),
                        Height = c.Int(nullable: false),
                        Width = c.Int(nullable: false),
                        Length = c.Int(nullable: false),
                        IsAssemblyRequired = c.Boolean(nullable: false),
                        ArticleId = c.Guid(nullable: false),
                        FacingColorId = c.Guid(nullable: false),
                        FrameColorId = c.Guid(nullable: false),
                        FacingId = c.Guid(nullable: false),
                        ProductPartMarkerId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Articles", t => t.ArticleId)
                .ForeignKey("dbo.Facings", t => t.FacingId)
                .ForeignKey("dbo.FacingsColors", t => t.FacingColorId)
                .ForeignKey("dbo.FrameColors", t => t.FrameColorId)
                .ForeignKey("dbo.ProductPartMarkers", t => t.ProductPartMarkerId)
                .Index(t => t.ArticleId)
                .Index(t => t.FacingColorId)
                .Index(t => t.FrameColorId)
                .Index(t => t.FacingId)
                .Index(t => t.ProductPartMarkerId);
            
            CreateTable(
                "dbo.Articles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        SyncCode1C = c.String(),
                        Height = c.Int(nullable: false),
                        Width = c.Int(nullable: false),
                        Length = c.Int(nullable: false),
                        FurnitureTypeId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FurnituresTypes", t => t.FurnitureTypeId)
                .Index(t => t.FurnitureTypeId);
            
            CreateTable(
                "dbo.FurnituresTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        SyncCode1C = c.String(),
                        TechName = c.String(),
                        SortNumber = c.Short(nullable: false),
                        NameInPlural = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CollectionsVariants",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        SyncCode1C = c.String(),
                        CollectionId = c.Guid(nullable: false),
                        CollectionTypeId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Collections", t => t.CollectionId)
                .ForeignKey("dbo.CollectionsTypes", t => t.CollectionTypeId)
                .Index(t => t.CollectionId)
                .Index(t => t.CollectionTypeId);
            
            CreateTable(
                "dbo.Collections",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Alias = c.String(nullable: false, maxLength: 300),
                        SyncCode1C = c.String(),
                        SeriesId = c.Guid(nullable: false),
                        RoomTypeId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RoomsTypes", t => t.RoomTypeId)
                .ForeignKey("dbo.Series", t => t.SeriesId)
                .Index(t => t.Alias, unique: true)
                .Index(t => t.SeriesId)
                .Index(t => t.RoomTypeId);
            
            CreateTable(
                "dbo.RoomsTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Alias = c.String(nullable: false, maxLength: 300),
                        SyncCode1C = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Alias, unique: true);
            
            CreateTable(
                "dbo.Series",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        SyncCode1C = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CollectionsTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        SyncCode1C = c.String(),
                        TechName = c.String(),
                        SortNumber = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Facings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        SyncCode1C = c.String(),
                        TechName = c.String(),
                        SortNumber = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FacingsColors",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        SyncCode1C = c.String(),
                        TechName = c.String(),
                        SortNumber = c.Short(nullable: false),
                        IsUniversal = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FavoriteItemCollections",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Quantity = c.Int(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                        CollectionVariantId = c.Guid(nullable: false),
                        FavoriteId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CollectionsVariants", t => t.CollectionVariantId)
                .ForeignKey("dbo.Favorites", t => t.FavoriteId, cascadeDelete: true)
                .Index(t => t.CollectionVariantId)
                .Index(t => t.FavoriteId);
            
            CreateTable(
                "dbo.Favorites",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UsersProfiles", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.FavoriteItemComplects",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Quantity = c.Int(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                        CollectionVariantId = c.Guid(nullable: false),
                        FavoriteId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CollectionsVariants", t => t.CollectionVariantId)
                .ForeignKey("dbo.Favorites", t => t.FavoriteId, cascadeDelete: true)
                .Index(t => t.CollectionVariantId)
                .Index(t => t.FavoriteId);
            
            CreateTable(
                "dbo.FavoriteItemProducts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Quantity = c.Int(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                        ProductId = c.Guid(nullable: false),
                        FavoriteId = c.Guid(nullable: false),
                        CollectionVariantId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Favorites", t => t.FavoriteId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId)
                .Index(t => t.ProductId)
                .Index(t => t.FavoriteId);
            
            CreateTable(
                "dbo.FavoriteItemProductInCollections",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Quantity = c.Int(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                        ProductId = c.Guid(nullable: false),
                        FavoriteItemCollectionId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FavoriteItemCollections", t => t.FavoriteItemCollectionId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId)
                .Index(t => t.ProductId)
                .Index(t => t.FavoriteItemCollectionId);
            
            CreateTable(
                "dbo.FrameColors",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        SyncCode1C = c.String(),
                        TechName = c.String(),
                        SortNumber = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductPartMarkers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        TechName = c.String(nullable: false),
                        SortNumber = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Properties",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SyncCode1C = c.String(),
                        Title = c.String(),
                        TitleInPlural = c.String(),
                        Alias = c.String(),
                        AliasInPlural = c.String(),
                        Value = c.String(),
                        IsRequired = c.Boolean(nullable: false),
                        IsMultiValue = c.Boolean(nullable: false),
                        SortNumber = c.Int(nullable: false),
                        GroupeId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.IsMultiValue, name: "IX_Property_IsMultiValue")
                .Index(t => t.GroupeId, name: "IX_Property_GroupeId");
            
            CreateTable(
                "dbo.Regions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(),
                        LazuritPasswordHash = c.String(),
                        SyncCode1C = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UsersAddresses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserProfileId = c.Guid(nullable: false),
                        KladrCode = c.String(),
                        Address = c.String(),
                        IsDefault = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UsersProfiles", t => t.UserProfileId)
                .Index(t => t.UserProfileId);
            
            CreateTable(
                "dbo.UsersPhones",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserProfileId = c.Guid(nullable: false),
                        Phone = c.String(),
                        IsDefault = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UsersProfiles", t => t.UserProfileId)
                .Index(t => t.UserProfileId);
            
            CreateTable(
                "dbo.UsersToShowRoom",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        ShowRoomId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.ShowRoomId })
                .ForeignKey("dbo.Showrooms", t => t.ShowRoomId)
                .ForeignKey("dbo.UsersProfiles", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.ShowRoomId);
            
            CreateTable(
                "dbo.Showrooms",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(),
                        CodeAddress = c.String(),
                        CodeCity = c.String(),
                        SyncCode1C = c.String(),
                        RegionId = c.Guid(nullable: false),
                        CityId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.CityId)
                .ForeignKey("dbo.Regions", t => t.RegionId)
                .Index(t => t.RegionId)
                .Index(t => t.CityId);
            
            CreateTable(
                "dbo.Settings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Title = c.String(),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CollectionsVariantsContentProductsAreas",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IsEnabled = c.Boolean(nullable: false),
                        Top = c.Int(nullable: false),
                        Left = c.Int(nullable: false),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                        CollectionVariantId = c.Guid(nullable: false),
                        ProductId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CollectionsVariants", t => t.CollectionVariantId)
                .ForeignKey("dbo.Products", t => t.ProductId)
                .Index(t => t.CollectionVariantId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.FilesContent",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ObjectId = c.Guid(nullable: false),
                        FileExtension = c.String(),
                        FileName = c.String(),
                        DateCreating = c.DateTime(nullable: false),
                        Size = c.Long(nullable: false),
                        TypeId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FilesContentTypes", t => t.TypeId)
                .Index(t => t.ObjectId)
                .Index(t => t.TypeId);
            
            CreateTable(
                "dbo.FilesContentTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        TechName = c.String(),
                        SortNumber = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Carts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IsDeliveryRequired = c.Boolean(nullable: false),
                        UserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UsersProfiles", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.CartItemCollections",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Quantity = c.Int(nullable: false),
                        CollectionVariantId = c.Guid(nullable: false),
                        CartId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Carts", t => t.CartId, cascadeDelete: true)
                .ForeignKey("dbo.CollectionsVariants", t => t.CollectionVariantId, cascadeDelete: true)
                .Index(t => t.CollectionVariantId)
                .Index(t => t.CartId);
            
            CreateTable(
                "dbo.CartItemProductInCollections",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Quantity = c.Int(nullable: false),
                        IsAssemblyRequired = c.Boolean(nullable: false),
                        ProductId = c.Guid(nullable: false),
                        CartItemCollectionId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CartItemCollections", t => t.CartItemCollectionId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.CartItemCollectionId);
            
            CreateTable(
                "dbo.CartItemComplects",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Quantity = c.Int(nullable: false),
                        IsAssemblyRequired = c.Boolean(nullable: false),
                        CollectionVariantId = c.Guid(nullable: false),
                        CartId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Carts", t => t.CartId, cascadeDelete: true)
                .ForeignKey("dbo.CollectionsVariants", t => t.CollectionVariantId, cascadeDelete: true)
                .Index(t => t.CollectionVariantId)
                .Index(t => t.CartId);
            
            CreateTable(
                "dbo.CartItemProduct",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Quantity = c.Int(nullable: false),
                        IsAssemblyRequired = c.Boolean(nullable: false),
                        ProductId = c.Guid(nullable: false),
                        CartId = c.Guid(nullable: false),
                        CollectionId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Carts", t => t.CartId, cascadeDelete: true)
                .ForeignKey("dbo.Collections", t => t.CollectionId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.CartId)
                .Index(t => t.CollectionId);
            
            CreateTable(
                "dbo.Layaways",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        IsWholeCart = c.Boolean(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UsersProfiles", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.LayawayItemCollections",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CollectionVarinatId = c.Guid(nullable: false),
                        LayawayId = c.Guid(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CollectionsVariants", t => t.CollectionVarinatId, cascadeDelete: true)
                .ForeignKey("dbo.Layaways", t => t.LayawayId, cascadeDelete: true)
                .Index(t => t.CollectionVarinatId)
                .Index(t => t.LayawayId);
            
            CreateTable(
                "dbo.LayawayItemProductInCollections",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ProductId = c.Guid(nullable: false),
                        LayawayItemCollectionId = c.Guid(nullable: false),
                        IsAssemblyRequired = c.Boolean(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LayawayItemCollections", t => t.LayawayItemCollectionId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.LayawayItemCollectionId);
            
            CreateTable(
                "dbo.LayawayItemComplects",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CollectionVarinatId = c.Guid(nullable: false),
                        LayawayId = c.Guid(nullable: false),
                        IsAssemblyRequired = c.Boolean(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CollectionsVariants", t => t.CollectionVarinatId, cascadeDelete: true)
                .ForeignKey("dbo.Layaways", t => t.LayawayId, cascadeDelete: true)
                .Index(t => t.CollectionVarinatId)
                .Index(t => t.LayawayId);
            
            CreateTable(
                "dbo.LayawayItemProducts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ProductId = c.Guid(nullable: false),
                        LayawayId = c.Guid(nullable: false),
                        IsAssemblyRequired = c.Boolean(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Layaways", t => t.LayawayId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.LayawayId);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SyncCode1C = c.String(),
                        Number = c.Long(nullable: false, identity: true),
                        Sum = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreationTime = c.DateTime(nullable: false),
                        DeliveryAddress = c.String(),
                        DeliveryKladrCode = c.String(),
                        ApproveTime = c.DateTime(),
                        IsApprovedBySeller = c.Boolean(nullable: false),
                        IsPaid = c.Boolean(nullable: false),
                        IsDeliveryRequired = c.Boolean(nullable: false),
                        UserProfileId = c.Guid(nullable: false),
                        ApprovingUserId = c.Guid(),
                        PaymentMethodId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UsersProfiles", t => t.ApprovingUserId)
                .ForeignKey("dbo.PaymentMethods", t => t.PaymentMethodId, cascadeDelete: true)
                .ForeignKey("dbo.UsersProfiles", t => t.UserProfileId, cascadeDelete: true)
                .Index(t => t.UserProfileId)
                .Index(t => t.ApprovingUserId)
                .Index(t => t.PaymentMethodId);
            
            CreateTable(
                "dbo.OrderItemCollections",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Quantity = c.Int(nullable: false),
                        CollectionVariantId = c.Guid(nullable: false),
                        OrderId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CollectionsVariants", t => t.CollectionVariantId, cascadeDelete: true)
                .ForeignKey("dbo.Orders", t => t.OrderId, cascadeDelete: true)
                .Index(t => t.CollectionVariantId)
                .Index(t => t.OrderId);
            
            CreateTable(
                "dbo.OrderItemProductInCollections",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Discount = c.Byte(nullable: false),
                        Quantity = c.Int(nullable: false),
                        IsAssemblyRequired = c.Boolean(nullable: false),
                        ProductId = c.Guid(nullable: false),
                        OrderItemCollectionId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OrderItemCollections", t => t.OrderItemCollectionId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.OrderItemCollectionId);
            
            CreateTable(
                "dbo.OrderItemComplects",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Discount = c.Byte(nullable: false),
                        Quantity = c.Int(nullable: false),
                        IsAssemblyRequired = c.Boolean(nullable: false),
                        CollectionVariantId = c.Guid(nullable: false),
                        OrderId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CollectionsVariants", t => t.CollectionVariantId, cascadeDelete: true)
                .ForeignKey("dbo.Orders", t => t.OrderId, cascadeDelete: true)
                .Index(t => t.CollectionVariantId)
                .Index(t => t.OrderId);
            
            CreateTable(
                "dbo.PaymentMethods",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        TechName = c.String(nullable: false),
                        SortNumber = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrderItemProducts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Discount = c.Byte(nullable: false),
                        Quantity = c.Int(nullable: false),
                        IsAssemblyRequired = c.Boolean(nullable: false),
                        ProductId = c.Guid(nullable: false),
                        OrderId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orders", t => t.OrderId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.OrderId);
            
            CreateTable(
                "dbo.CollectionsVariantsToComplect",
                c => new
                    {
                        CollectionId = c.Guid(nullable: false),
                        CollectionComlectId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.CollectionId, t.CollectionComlectId })
                .ForeignKey("dbo.CollectionsVariants", t => t.CollectionId)
                .ForeignKey("dbo.CollectionsVariants", t => t.CollectionComlectId)
                .Index(t => t.CollectionId)
                .Index(t => t.CollectionComlectId);
            
            CreateTable(
                "dbo.FacingsToCollectionsVariants",
                c => new
                    {
                        CollectionVariantId = c.Guid(nullable: false),
                        FacingId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.CollectionVariantId, t.FacingId })
                .ForeignKey("dbo.CollectionsVariants", t => t.CollectionVariantId, cascadeDelete: true)
                .ForeignKey("dbo.Facings", t => t.FacingId, cascadeDelete: true)
                .Index(t => t.CollectionVariantId)
                .Index(t => t.FacingId);
            
            CreateTable(
                "dbo.FacingsColorsToCollectionsVariants",
                c => new
                    {
                        CollectionVariantId = c.Guid(nullable: false),
                        FacingColorId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.CollectionVariantId, t.FacingColorId })
                .ForeignKey("dbo.CollectionsVariants", t => t.CollectionVariantId, cascadeDelete: true)
                .ForeignKey("dbo.FacingsColors", t => t.FacingColorId, cascadeDelete: true)
                .Index(t => t.CollectionVariantId)
                .Index(t => t.FacingColorId);
            
            CreateTable(
                "dbo.FrameColorsToCollectionsVariants",
                c => new
                    {
                        CollectionVariantId = c.Guid(nullable: false),
                        FrameColorId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.CollectionVariantId, t.FrameColorId })
                .ForeignKey("dbo.CollectionsVariants", t => t.CollectionVariantId, cascadeDelete: true)
                .ForeignKey("dbo.FrameColors", t => t.FrameColorId, cascadeDelete: true)
                .Index(t => t.CollectionVariantId)
                .Index(t => t.FrameColorId);
            
            CreateTable(
                "dbo.ProductsToCollectionsVariants",
                c => new
                    {
                        ProductId = c.Guid(nullable: false),
                        CollectionVariantId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProductId, t.CollectionVariantId })
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.CollectionsVariants", t => t.CollectionVariantId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.CollectionVariantId);
            
            CreateTable(
                "dbo.PropertiesToProductss",
                c => new
                    {
                        PropertyId = c.Guid(nullable: false),
                        ProductId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.PropertyId, t.ProductId })
                .ForeignKey("dbo.Properties", t => t.PropertyId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.PropertyId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.UserProfileRegions",
                c => new
                    {
                        UserProfile_Id = c.Guid(nullable: false),
                        Region_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserProfile_Id, t.Region_Id })
                .ForeignKey("dbo.UsersProfiles", t => t.UserProfile_Id, cascadeDelete: true)
                .ForeignKey("dbo.Regions", t => t.Region_Id, cascadeDelete: true)
                .Index(t => t.UserProfile_Id)
                .Index(t => t.Region_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "UserProfileId", "dbo.UsersProfiles");
            DropForeignKey("dbo.OrderItemProducts", "ProductId", "dbo.Products");
            DropForeignKey("dbo.OrderItemProducts", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.Orders", "PaymentMethodId", "dbo.PaymentMethods");
            DropForeignKey("dbo.OrderItemComplects", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.OrderItemComplects", "CollectionVariantId", "dbo.CollectionsVariants");
            DropForeignKey("dbo.OrderItemProductInCollections", "ProductId", "dbo.Products");
            DropForeignKey("dbo.OrderItemProductInCollections", "OrderItemCollectionId", "dbo.OrderItemCollections");
            DropForeignKey("dbo.OrderItemCollections", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.OrderItemCollections", "CollectionVariantId", "dbo.CollectionsVariants");
            DropForeignKey("dbo.Orders", "ApprovingUserId", "dbo.UsersProfiles");
            DropForeignKey("dbo.Layaways", "UserId", "dbo.UsersProfiles");
            DropForeignKey("dbo.LayawayItemProducts", "ProductId", "dbo.Products");
            DropForeignKey("dbo.LayawayItemProducts", "LayawayId", "dbo.Layaways");
            DropForeignKey("dbo.LayawayItemComplects", "LayawayId", "dbo.Layaways");
            DropForeignKey("dbo.LayawayItemComplects", "CollectionVarinatId", "dbo.CollectionsVariants");
            DropForeignKey("dbo.LayawayItemProductInCollections", "ProductId", "dbo.Products");
            DropForeignKey("dbo.LayawayItemProductInCollections", "LayawayItemCollectionId", "dbo.LayawayItemCollections");
            DropForeignKey("dbo.LayawayItemCollections", "LayawayId", "dbo.Layaways");
            DropForeignKey("dbo.LayawayItemCollections", "CollectionVarinatId", "dbo.CollectionsVariants");
            DropForeignKey("dbo.Carts", "UserId", "dbo.UsersProfiles");
            DropForeignKey("dbo.CartItemProduct", "ProductId", "dbo.Products");
            DropForeignKey("dbo.CartItemProduct", "CollectionId", "dbo.Collections");
            DropForeignKey("dbo.CartItemProduct", "CartId", "dbo.Carts");
            DropForeignKey("dbo.CartItemComplects", "CollectionVariantId", "dbo.CollectionsVariants");
            DropForeignKey("dbo.CartItemComplects", "CartId", "dbo.Carts");
            DropForeignKey("dbo.CartItemProductInCollections", "ProductId", "dbo.Products");
            DropForeignKey("dbo.CartItemProductInCollections", "CartItemCollectionId", "dbo.CartItemCollections");
            DropForeignKey("dbo.CartItemCollections", "CollectionVariantId", "dbo.CollectionsVariants");
            DropForeignKey("dbo.CartItemCollections", "CartId", "dbo.Carts");
            DropForeignKey("dbo.FilesContent", "TypeId", "dbo.FilesContentTypes");
            DropForeignKey("dbo.CollectionsVariantsContentProductsAreas", "ProductId", "dbo.Products");
            DropForeignKey("dbo.CollectionsVariantsContentProductsAreas", "CollectionVariantId", "dbo.CollectionsVariants");
            DropForeignKey("dbo.UsersToShowRoom", "UserId", "dbo.UsersProfiles");
            DropForeignKey("dbo.UsersToShowRoom", "ShowRoomId", "dbo.Showrooms");
            DropForeignKey("dbo.UsersProfiles", "Showroom_Id", "dbo.Showrooms");
            DropForeignKey("dbo.Showrooms", "RegionId", "dbo.Regions");
            DropForeignKey("dbo.Showrooms", "CityId", "dbo.Cities");
            DropForeignKey("dbo.UsersPhones", "UserProfileId", "dbo.UsersProfiles");
            DropForeignKey("dbo.UsersAddresses", "UserProfileId", "dbo.UsersProfiles");
            DropForeignKey("dbo.UserProfileRegions", "Region_Id", "dbo.Regions");
            DropForeignKey("dbo.UserProfileRegions", "UserProfile_Id", "dbo.UsersProfiles");
            DropForeignKey("dbo.UsersProfiles", "CityId", "dbo.Cities");
            DropForeignKey("dbo.Cities", "RegionId", "dbo.Regions");
            DropForeignKey("dbo.Cities", "PriceZoneId", "dbo.PriceZones");
            DropForeignKey("dbo.PropertiesToProductss", "ProductId", "dbo.Products");
            DropForeignKey("dbo.PropertiesToProductss", "PropertyId", "dbo.Properties");
            DropForeignKey("dbo.Products", "ProductPartMarkerId", "dbo.ProductPartMarkers");
            DropForeignKey("dbo.Prices", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Products", "FrameColorId", "dbo.FrameColors");
            DropForeignKey("dbo.Products", "FacingColorId", "dbo.FacingsColors");
            DropForeignKey("dbo.Products", "FacingId", "dbo.Facings");
            DropForeignKey("dbo.ProductsToCollectionsVariants", "CollectionVariantId", "dbo.CollectionsVariants");
            DropForeignKey("dbo.ProductsToCollectionsVariants", "ProductId", "dbo.Products");
            DropForeignKey("dbo.FrameColorsToCollectionsVariants", "FrameColorId", "dbo.FrameColors");
            DropForeignKey("dbo.FrameColorsToCollectionsVariants", "CollectionVariantId", "dbo.CollectionsVariants");
            DropForeignKey("dbo.FavoriteItemProductInCollections", "ProductId", "dbo.Products");
            DropForeignKey("dbo.FavoriteItemProductInCollections", "FavoriteItemCollectionId", "dbo.FavoriteItemCollections");
            DropForeignKey("dbo.FavoriteItemCollections", "FavoriteId", "dbo.Favorites");
            DropForeignKey("dbo.Favorites", "Id", "dbo.UsersProfiles");
            DropForeignKey("dbo.FavoriteItemProducts", "ProductId", "dbo.Products");
            DropForeignKey("dbo.FavoriteItemProducts", "FavoriteId", "dbo.Favorites");
            DropForeignKey("dbo.FavoriteItemComplects", "FavoriteId", "dbo.Favorites");
            DropForeignKey("dbo.FavoriteItemComplects", "CollectionVariantId", "dbo.CollectionsVariants");
            DropForeignKey("dbo.FavoriteItemCollections", "CollectionVariantId", "dbo.CollectionsVariants");
            DropForeignKey("dbo.FacingsColorsToCollectionsVariants", "FacingColorId", "dbo.FacingsColors");
            DropForeignKey("dbo.FacingsColorsToCollectionsVariants", "CollectionVariantId", "dbo.CollectionsVariants");
            DropForeignKey("dbo.FacingsToCollectionsVariants", "FacingId", "dbo.Facings");
            DropForeignKey("dbo.FacingsToCollectionsVariants", "CollectionVariantId", "dbo.CollectionsVariants");
            DropForeignKey("dbo.CollectionsVariantsToComplect", "CollectionComlectId", "dbo.CollectionsVariants");
            DropForeignKey("dbo.CollectionsVariantsToComplect", "CollectionId", "dbo.CollectionsVariants");
            DropForeignKey("dbo.CollectionsVariants", "CollectionTypeId", "dbo.CollectionsTypes");
            DropForeignKey("dbo.CollectionsVariants", "CollectionId", "dbo.Collections");
            DropForeignKey("dbo.Collections", "SeriesId", "dbo.Series");
            DropForeignKey("dbo.Collections", "RoomTypeId", "dbo.RoomsTypes");
            DropForeignKey("dbo.Products", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.Articles", "FurnitureTypeId", "dbo.FurnituresTypes");
            DropForeignKey("dbo.Prices", "PriceZoneId", "dbo.PriceZones");
            DropIndex("dbo.UserProfileRegions", new[] { "Region_Id" });
            DropIndex("dbo.UserProfileRegions", new[] { "UserProfile_Id" });
            DropIndex("dbo.PropertiesToProductss", new[] { "ProductId" });
            DropIndex("dbo.PropertiesToProductss", new[] { "PropertyId" });
            DropIndex("dbo.ProductsToCollectionsVariants", new[] { "CollectionVariantId" });
            DropIndex("dbo.ProductsToCollectionsVariants", new[] { "ProductId" });
            DropIndex("dbo.FrameColorsToCollectionsVariants", new[] { "FrameColorId" });
            DropIndex("dbo.FrameColorsToCollectionsVariants", new[] { "CollectionVariantId" });
            DropIndex("dbo.FacingsColorsToCollectionsVariants", new[] { "FacingColorId" });
            DropIndex("dbo.FacingsColorsToCollectionsVariants", new[] { "CollectionVariantId" });
            DropIndex("dbo.FacingsToCollectionsVariants", new[] { "FacingId" });
            DropIndex("dbo.FacingsToCollectionsVariants", new[] { "CollectionVariantId" });
            DropIndex("dbo.CollectionsVariantsToComplect", new[] { "CollectionComlectId" });
            DropIndex("dbo.CollectionsVariantsToComplect", new[] { "CollectionId" });
            DropIndex("dbo.OrderItemProducts", new[] { "OrderId" });
            DropIndex("dbo.OrderItemProducts", new[] { "ProductId" });
            DropIndex("dbo.OrderItemComplects", new[] { "OrderId" });
            DropIndex("dbo.OrderItemComplects", new[] { "CollectionVariantId" });
            DropIndex("dbo.OrderItemProductInCollections", new[] { "OrderItemCollectionId" });
            DropIndex("dbo.OrderItemProductInCollections", new[] { "ProductId" });
            DropIndex("dbo.OrderItemCollections", new[] { "OrderId" });
            DropIndex("dbo.OrderItemCollections", new[] { "CollectionVariantId" });
            DropIndex("dbo.Orders", new[] { "PaymentMethodId" });
            DropIndex("dbo.Orders", new[] { "ApprovingUserId" });
            DropIndex("dbo.Orders", new[] { "UserProfileId" });
            DropIndex("dbo.LayawayItemProducts", new[] { "LayawayId" });
            DropIndex("dbo.LayawayItemProducts", new[] { "ProductId" });
            DropIndex("dbo.LayawayItemComplects", new[] { "LayawayId" });
            DropIndex("dbo.LayawayItemComplects", new[] { "CollectionVarinatId" });
            DropIndex("dbo.LayawayItemProductInCollections", new[] { "LayawayItemCollectionId" });
            DropIndex("dbo.LayawayItemProductInCollections", new[] { "ProductId" });
            DropIndex("dbo.LayawayItemCollections", new[] { "LayawayId" });
            DropIndex("dbo.LayawayItemCollections", new[] { "CollectionVarinatId" });
            DropIndex("dbo.Layaways", new[] { "UserId" });
            DropIndex("dbo.CartItemProduct", new[] { "CollectionId" });
            DropIndex("dbo.CartItemProduct", new[] { "CartId" });
            DropIndex("dbo.CartItemProduct", new[] { "ProductId" });
            DropIndex("dbo.CartItemComplects", new[] { "CartId" });
            DropIndex("dbo.CartItemComplects", new[] { "CollectionVariantId" });
            DropIndex("dbo.CartItemProductInCollections", new[] { "CartItemCollectionId" });
            DropIndex("dbo.CartItemProductInCollections", new[] { "ProductId" });
            DropIndex("dbo.CartItemCollections", new[] { "CartId" });
            DropIndex("dbo.CartItemCollections", new[] { "CollectionVariantId" });
            DropIndex("dbo.Carts", new[] { "UserId" });
            DropIndex("dbo.FilesContent", new[] { "TypeId" });
            DropIndex("dbo.FilesContent", new[] { "ObjectId" });
            DropIndex("dbo.CollectionsVariantsContentProductsAreas", new[] { "ProductId" });
            DropIndex("dbo.CollectionsVariantsContentProductsAreas", new[] { "CollectionVariantId" });
            DropIndex("dbo.Showrooms", new[] { "CityId" });
            DropIndex("dbo.Showrooms", new[] { "RegionId" });
            DropIndex("dbo.UsersToShowRoom", new[] { "ShowRoomId" });
            DropIndex("dbo.UsersToShowRoom", new[] { "UserId" });
            DropIndex("dbo.UsersPhones", new[] { "UserProfileId" });
            DropIndex("dbo.UsersAddresses", new[] { "UserProfileId" });
            DropIndex("dbo.Properties", "IX_Property_GroupeId");
            DropIndex("dbo.Properties", "IX_Property_IsMultiValue");
            DropIndex("dbo.FavoriteItemProductInCollections", new[] { "FavoriteItemCollectionId" });
            DropIndex("dbo.FavoriteItemProductInCollections", new[] { "ProductId" });
            DropIndex("dbo.FavoriteItemProducts", new[] { "FavoriteId" });
            DropIndex("dbo.FavoriteItemProducts", new[] { "ProductId" });
            DropIndex("dbo.FavoriteItemComplects", new[] { "FavoriteId" });
            DropIndex("dbo.FavoriteItemComplects", new[] { "CollectionVariantId" });
            DropIndex("dbo.Favorites", new[] { "Id" });
            DropIndex("dbo.FavoriteItemCollections", new[] { "FavoriteId" });
            DropIndex("dbo.FavoriteItemCollections", new[] { "CollectionVariantId" });
            DropIndex("dbo.RoomsTypes", new[] { "Alias" });
            DropIndex("dbo.Collections", new[] { "RoomTypeId" });
            DropIndex("dbo.Collections", new[] { "SeriesId" });
            DropIndex("dbo.Collections", new[] { "Alias" });
            DropIndex("dbo.CollectionsVariants", new[] { "CollectionTypeId" });
            DropIndex("dbo.CollectionsVariants", new[] { "CollectionId" });
            DropIndex("dbo.Articles", new[] { "FurnitureTypeId" });
            DropIndex("dbo.Products", new[] { "ProductPartMarkerId" });
            DropIndex("dbo.Products", new[] { "FacingId" });
            DropIndex("dbo.Products", new[] { "FrameColorId" });
            DropIndex("dbo.Products", new[] { "FacingColorId" });
            DropIndex("dbo.Products", new[] { "ArticleId" });
            DropIndex("dbo.Prices", new[] { "ProductId" });
            DropIndex("dbo.Prices", new[] { "PriceZoneId" });
            DropIndex("dbo.Cities", new[] { "PriceZoneId" });
            DropIndex("dbo.Cities", new[] { "RegionId" });
            DropIndex("dbo.UsersProfiles", new[] { "Showroom_Id" });
            DropIndex("dbo.UsersProfiles", new[] { "CityId" });
            DropTable("dbo.UserProfileRegions");
            DropTable("dbo.PropertiesToProductss");
            DropTable("dbo.ProductsToCollectionsVariants");
            DropTable("dbo.FrameColorsToCollectionsVariants");
            DropTable("dbo.FacingsColorsToCollectionsVariants");
            DropTable("dbo.FacingsToCollectionsVariants");
            DropTable("dbo.CollectionsVariantsToComplect");
            DropTable("dbo.OrderItemProducts");
            DropTable("dbo.PaymentMethods");
            DropTable("dbo.OrderItemComplects");
            DropTable("dbo.OrderItemProductInCollections");
            DropTable("dbo.OrderItemCollections");
            DropTable("dbo.Orders");
            DropTable("dbo.LayawayItemProducts");
            DropTable("dbo.LayawayItemComplects");
            DropTable("dbo.LayawayItemProductInCollections");
            DropTable("dbo.LayawayItemCollections");
            DropTable("dbo.Layaways");
            DropTable("dbo.CartItemProduct");
            DropTable("dbo.CartItemComplects");
            DropTable("dbo.CartItemProductInCollections");
            DropTable("dbo.CartItemCollections");
            DropTable("dbo.Carts");
            DropTable("dbo.FilesContentTypes");
            DropTable("dbo.FilesContent");
            DropTable("dbo.CollectionsVariantsContentProductsAreas");
            DropTable("dbo.Settings");
            DropTable("dbo.Showrooms");
            DropTable("dbo.UsersToShowRoom");
            DropTable("dbo.UsersPhones");
            DropTable("dbo.UsersAddresses");
            DropTable("dbo.Regions");
            DropTable("dbo.Properties");
            DropTable("dbo.ProductPartMarkers");
            DropTable("dbo.FrameColors");
            DropTable("dbo.FavoriteItemProductInCollections");
            DropTable("dbo.FavoriteItemProducts");
            DropTable("dbo.FavoriteItemComplects");
            DropTable("dbo.Favorites");
            DropTable("dbo.FavoriteItemCollections");
            DropTable("dbo.FacingsColors");
            DropTable("dbo.Facings");
            DropTable("dbo.CollectionsTypes");
            DropTable("dbo.Series");
            DropTable("dbo.RoomsTypes");
            DropTable("dbo.Collections");
            DropTable("dbo.CollectionsVariants");
            DropTable("dbo.FurnituresTypes");
            DropTable("dbo.Articles");
            DropTable("dbo.Products");
            DropTable("dbo.Prices");
            DropTable("dbo.PriceZones");
            DropTable("dbo.Cities");
            DropTable("dbo.UsersProfiles");
        }
    }
}
