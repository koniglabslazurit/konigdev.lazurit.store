namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatedOnForMarketingActionsAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MarketingActions", "CreatedOn", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MarketingActions", "CreatedOn");
        }
    }
}
