namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RelateProductWithCollection : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "CollectionId", c => c.Guid(nullable: false));
            AddColumn("dbo.CollectionsVariants", "SyncCode1C", c => c.String());
            CreateIndex("dbo.Products", "CollectionId");
            AddForeignKey("dbo.Products", "CollectionId", "dbo.Collections", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "CollectionId", "dbo.Collections");
            DropIndex("dbo.Products", new[] { "CollectionId" });
            DropColumn("dbo.CollectionsVariants", "SyncCode1C");
            DropColumn("dbo.Products", "CollectionId");
        }
    }
}
