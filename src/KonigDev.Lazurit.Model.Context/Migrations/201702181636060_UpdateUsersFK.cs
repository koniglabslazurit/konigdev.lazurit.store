namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateUsersFK : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UsersAddresses", "UserProfileId", "dbo.UsersProfiles");
            DropForeignKey("dbo.UsersPhones", "UserProfileId", "dbo.UsersProfiles");
            AddForeignKey("dbo.UsersAddresses", "UserProfileId", "dbo.UsersProfiles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.UsersPhones", "UserProfileId", "dbo.UsersProfiles", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UsersPhones", "UserProfileId", "dbo.UsersProfiles");
            DropForeignKey("dbo.UsersAddresses", "UserProfileId", "dbo.UsersProfiles");
            AddForeignKey("dbo.UsersPhones", "UserProfileId", "dbo.UsersProfiles", "Id");
            AddForeignKey("dbo.UsersAddresses", "UserProfileId", "dbo.UsersProfiles", "Id");
        }
    }
}
