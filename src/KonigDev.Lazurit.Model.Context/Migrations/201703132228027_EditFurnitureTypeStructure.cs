namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditFurnitureTypeStructure : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FurnituresTypes", "ParentId", c => c.Guid());
            AddColumn("dbo.FurnituresTypes", "IsFurniturePiece", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FurnituresTypes", "IsFurniturePiece");
            DropColumn("dbo.FurnituresTypes", "ParentId");
        }
    }
}
