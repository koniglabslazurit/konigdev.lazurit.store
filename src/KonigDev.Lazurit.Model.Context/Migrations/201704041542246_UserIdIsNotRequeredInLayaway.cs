namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserIdIsNotRequeredInLayaway : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Layaways", "UserId", "dbo.UsersProfiles");
            DropIndex("dbo.Layaways", new[] { "UserId" });
            AlterColumn("dbo.Layaways", "UserId", c => c.Guid());
            CreateIndex("dbo.Layaways", "UserId");
            AddForeignKey("dbo.Layaways", "UserId", "dbo.UsersProfiles", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Layaways", "UserId", "dbo.UsersProfiles");
            DropIndex("dbo.Layaways", new[] { "UserId" });
            AlterColumn("dbo.Layaways", "UserId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Layaways", "UserId");
            AddForeignKey("dbo.Layaways", "UserId", "dbo.UsersProfiles", "Id", cascadeDelete: true);
        }
    }
}
