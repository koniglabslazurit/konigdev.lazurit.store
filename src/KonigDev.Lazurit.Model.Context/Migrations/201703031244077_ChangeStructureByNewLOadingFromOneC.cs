namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeStructureByNewLOadingFromOneC : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CollectionsVariants", "CollectionTypeId", "dbo.CollectionsTypes");
            DropIndex("dbo.CollectionsVariants", new[] { "CollectionTypeId" });
            DropIndex("dbo.CollectionsTypes", new[] { "TechName" });
            CreateTable(
                "dbo.ProductsPacking",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ProductSyncCode1C = c.String(nullable: false, maxLength: 50),
                        SyncCode1C = c.String(),
                        Height = c.Int(nullable: false),
                        Width = c.Int(nullable: false),
                        Length = c.Int(nullable: false),
                        Weight = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Amount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.ProductSyncCode1C);
            
            AddColumn("dbo.Products", "IsComplect", c => c.Boolean(nullable: false));
            AddColumn("dbo.Products", "VendorCode", c => c.String());
            AddColumn("dbo.Products", "Packing", c => c.String());
            AddColumn("dbo.Products", "FurnitureForm", c => c.String());
            AddColumn("dbo.Products", "MarketerVendorCode", c => c.String());
            AddColumn("dbo.Articles", "Range", c => c.String());
            AddColumn("dbo.CollectionsVariants", "IsComplect", c => c.Boolean(nullable: false));
            AddColumn("dbo.Collections", "Range", c => c.String());
            AddColumn("dbo.RoomsTypes", "Range", c => c.String());
            AddColumn("dbo.Series", "Range", c => c.String());
            DropColumn("dbo.Products", "Cornice");
            DropColumn("dbo.Articles", "Height");
            DropColumn("dbo.Articles", "Width");
            DropColumn("dbo.Articles", "Length");
            DropColumn("dbo.CollectionsVariants", "CollectionTypeId");
            DropTable("dbo.CollectionsTypes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.CollectionsTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        TechName = c.String(nullable: false, maxLength: 25),
                        SortNumber = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.CollectionsVariants", "CollectionTypeId", c => c.Guid(nullable: false));
            AddColumn("dbo.Articles", "Length", c => c.Int(nullable: false));
            AddColumn("dbo.Articles", "Width", c => c.Int(nullable: false));
            AddColumn("dbo.Articles", "Height", c => c.Int(nullable: false));
            AddColumn("dbo.Products", "Cornice", c => c.String());
            DropIndex("dbo.ProductsPacking", new[] { "ProductSyncCode1C" });
            DropColumn("dbo.Series", "Range");
            DropColumn("dbo.RoomsTypes", "Range");
            DropColumn("dbo.Collections", "Range");
            DropColumn("dbo.CollectionsVariants", "IsComplect");
            DropColumn("dbo.Articles", "Range");
            DropColumn("dbo.Products", "MarketerVendorCode");
            DropColumn("dbo.Products", "FurnitureForm");
            DropColumn("dbo.Products", "Packing");
            DropColumn("dbo.Products", "VendorCode");
            DropColumn("dbo.Products", "IsComplect");
            DropTable("dbo.ProductsPacking");
            CreateIndex("dbo.CollectionsTypes", "TechName", unique: true);
            CreateIndex("dbo.CollectionsVariants", "CollectionTypeId");
            AddForeignKey("dbo.CollectionsVariants", "CollectionTypeId", "dbo.CollectionsTypes", "Id");
        }
    }
}
