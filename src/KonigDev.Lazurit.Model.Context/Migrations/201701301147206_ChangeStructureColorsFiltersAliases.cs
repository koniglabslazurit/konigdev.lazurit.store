namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeStructureColorsFiltersAliases : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.FacingsToCollectionsVariants", "CollectionVariantId", "dbo.CollectionsVariants");
            DropForeignKey("dbo.FacingsToCollectionsVariants", "FacingId", "dbo.Facings");
            DropForeignKey("dbo.FacingsColorsToCollectionsVariants", "CollectionVariantId", "dbo.CollectionsVariants");
            DropForeignKey("dbo.FacingsColorsToCollectionsVariants", "FacingColorId", "dbo.FacingsColors");
            DropForeignKey("dbo.FrameColorsToCollectionsVariants", "CollectionVariantId", "dbo.CollectionsVariants");
            DropForeignKey("dbo.FrameColorsToCollectionsVariants", "FrameColorId", "dbo.FrameColors");
            DropIndex("dbo.Products", new[] { "FacingColorId" });
            DropIndex("dbo.Articles", new[] { "FurnitureTypeId" });
            DropIndex("dbo.CollectionsVariants", new[] { "CollectionTypeId" });
            DropIndex("dbo.Collections", new[] { "Alias" });
            DropIndex("dbo.FacingsToCollectionsVariants", new[] { "CollectionVariantId" });
            DropIndex("dbo.FacingsToCollectionsVariants", new[] { "FacingId" });
            DropIndex("dbo.FacingsColorsToCollectionsVariants", new[] { "CollectionVariantId" });
            DropIndex("dbo.FacingsColorsToCollectionsVariants", new[] { "FacingColorId" });
            DropIndex("dbo.FrameColorsToCollectionsVariants", new[] { "CollectionVariantId" });
            DropIndex("dbo.FrameColorsToCollectionsVariants", new[] { "FrameColorId" });
            AddColumn("dbo.FurnituresTypes", "Alias", c => c.String(nullable: false, maxLength: 25));
            AddColumn("dbo.FurnituresTypes", "AliasInPlural", c => c.String());
            AddColumn("dbo.CollectionsVariants", "FacingId", c => c.Guid(nullable: false));
            AddColumn("dbo.CollectionsVariants", "FacingColorId", c => c.Guid(nullable: false));
            AddColumn("dbo.CollectionsVariants", "FrameColorId", c => c.Guid(nullable: false));
            AddColumn("dbo.Collections", "Title", c => c.String());
            AddColumn("dbo.Series", "Alias", c => c.String(nullable: false, maxLength: 25));
            AlterColumn("dbo.Products", "SyncCode1C", c => c.String(nullable: false, maxLength: 25));
            AlterColumn("dbo.Products", "FacingColorId", c => c.Guid());
            AlterColumn("dbo.Articles", "SyncCode1C", c => c.String(maxLength: 255));
            AlterColumn("dbo.Articles", "FurnitureTypeId", c => c.Guid(nullable: false));
            AlterColumn("dbo.FurnituresTypes", "SyncCode1C", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.CollectionsVariants", "CollectionTypeId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Products", "SyncCode1C", unique: true);
            CreateIndex("dbo.Products", "FacingColorId");
            CreateIndex("dbo.Articles", "SyncCode1C", unique: true);
            CreateIndex("dbo.Articles", "FurnitureTypeId");
            CreateIndex("dbo.FurnituresTypes", "Alias", unique: true);
            CreateIndex("dbo.FurnituresTypes", "SyncCode1C", unique: true);
            CreateIndex("dbo.CollectionsVariants", "CollectionTypeId");
            CreateIndex("dbo.CollectionsVariants", "FacingId");
            CreateIndex("dbo.CollectionsVariants", "FacingColorId");
            CreateIndex("dbo.CollectionsVariants", "FrameColorId");
            CreateIndex("dbo.Series", "Alias", unique: true);
            AddForeignKey("dbo.CollectionsVariants", "FacingId", "dbo.Facings", "Id", cascadeDelete: true);
            AddForeignKey("dbo.CollectionsVariants", "FacingColorId", "dbo.FacingsColors", "Id", cascadeDelete: true);
            AddForeignKey("dbo.CollectionsVariants", "FrameColorId", "dbo.FrameColors", "Id", cascadeDelete: true);
            DropColumn("dbo.Products", "Name");
            DropColumn("dbo.FurnituresTypes", "TechName");
            DropColumn("dbo.FurnituresTypes", "SortNumber");
            DropColumn("dbo.CollectionsVariants", "Name");
            DropColumn("dbo.CollectionsVariants", "SyncCode1C");
            DropColumn("dbo.Collections", "Name");
            DropColumn("dbo.Collections", "Alias");
            DropColumn("dbo.Collections", "SyncCode1C");
            DropColumn("dbo.CollectionsTypes", "SyncCode1C");
            DropColumn("dbo.Facings", "TechName");
            DropColumn("dbo.Facings", "SortNumber");
            DropColumn("dbo.FacingsColors", "TechName");
            DropColumn("dbo.FacingsColors", "SortNumber");
            DropColumn("dbo.FrameColors", "TechName");
            DropColumn("dbo.FrameColors", "SortNumber");
            DropTable("dbo.FacingsToCollectionsVariants");
            DropTable("dbo.FacingsColorsToCollectionsVariants");
            DropTable("dbo.FrameColorsToCollectionsVariants");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.FrameColorsToCollectionsVariants",
                c => new
                    {
                        CollectionVariantId = c.Guid(nullable: false),
                        FrameColorId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.CollectionVariantId, t.FrameColorId });
            
            CreateTable(
                "dbo.FacingsColorsToCollectionsVariants",
                c => new
                    {
                        CollectionVariantId = c.Guid(nullable: false),
                        FacingColorId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.CollectionVariantId, t.FacingColorId });
            
            CreateTable(
                "dbo.FacingsToCollectionsVariants",
                c => new
                    {
                        CollectionVariantId = c.Guid(nullable: false),
                        FacingId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.CollectionVariantId, t.FacingId });
            
            AddColumn("dbo.FrameColors", "SortNumber", c => c.Short(nullable: false));
            AddColumn("dbo.FrameColors", "TechName", c => c.String());
            AddColumn("dbo.FacingsColors", "SortNumber", c => c.Short(nullable: false));
            AddColumn("dbo.FacingsColors", "TechName", c => c.String());
            AddColumn("dbo.Facings", "SortNumber", c => c.Short(nullable: false));
            AddColumn("dbo.Facings", "TechName", c => c.String());
            AddColumn("dbo.CollectionsTypes", "SyncCode1C", c => c.String());
            AddColumn("dbo.Collections", "SyncCode1C", c => c.String());
            AddColumn("dbo.Collections", "Alias", c => c.String(nullable: false, maxLength: 300));
            AddColumn("dbo.Collections", "Name", c => c.String());
            AddColumn("dbo.CollectionsVariants", "SyncCode1C", c => c.String());
            AddColumn("dbo.CollectionsVariants", "Name", c => c.String());
            AddColumn("dbo.FurnituresTypes", "SortNumber", c => c.Short(nullable: false));
            AddColumn("dbo.FurnituresTypes", "TechName", c => c.String());
            AddColumn("dbo.Products", "Name", c => c.String());
            DropForeignKey("dbo.CollectionsVariants", "FrameColorId", "dbo.FrameColors");
            DropForeignKey("dbo.CollectionsVariants", "FacingColorId", "dbo.FacingsColors");
            DropForeignKey("dbo.CollectionsVariants", "FacingId", "dbo.Facings");
            DropIndex("dbo.Series", new[] { "Alias" });
            DropIndex("dbo.CollectionsVariants", new[] { "FrameColorId" });
            DropIndex("dbo.CollectionsVariants", new[] { "FacingColorId" });
            DropIndex("dbo.CollectionsVariants", new[] { "FacingId" });
            DropIndex("dbo.CollectionsVariants", new[] { "CollectionTypeId" });
            DropIndex("dbo.FurnituresTypes", new[] { "SyncCode1C" });
            DropIndex("dbo.FurnituresTypes", new[] { "Alias" });
            DropIndex("dbo.Articles", new[] { "FurnitureTypeId" });
            DropIndex("dbo.Articles", new[] { "SyncCode1C" });
            DropIndex("dbo.Products", new[] { "FacingColorId" });
            DropIndex("dbo.Products", new[] { "SyncCode1C" });
            AlterColumn("dbo.CollectionsVariants", "CollectionTypeId", c => c.Guid());
            AlterColumn("dbo.FurnituresTypes", "SyncCode1C", c => c.String());
            AlterColumn("dbo.Articles", "FurnitureTypeId", c => c.Guid());
            AlterColumn("dbo.Articles", "SyncCode1C", c => c.String());
            AlterColumn("dbo.Products", "FacingColorId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Products", "SyncCode1C", c => c.String());
            DropColumn("dbo.Series", "Alias");
            DropColumn("dbo.Collections", "Title");
            DropColumn("dbo.CollectionsVariants", "FrameColorId");
            DropColumn("dbo.CollectionsVariants", "FacingColorId");
            DropColumn("dbo.CollectionsVariants", "FacingId");
            DropColumn("dbo.FurnituresTypes", "AliasInPlural");
            DropColumn("dbo.FurnituresTypes", "Alias");
            CreateIndex("dbo.FrameColorsToCollectionsVariants", "FrameColorId");
            CreateIndex("dbo.FrameColorsToCollectionsVariants", "CollectionVariantId");
            CreateIndex("dbo.FacingsColorsToCollectionsVariants", "FacingColorId");
            CreateIndex("dbo.FacingsColorsToCollectionsVariants", "CollectionVariantId");
            CreateIndex("dbo.FacingsToCollectionsVariants", "FacingId");
            CreateIndex("dbo.FacingsToCollectionsVariants", "CollectionVariantId");
            CreateIndex("dbo.Collections", "Alias", unique: true);
            CreateIndex("dbo.CollectionsVariants", "CollectionTypeId");
            CreateIndex("dbo.Articles", "FurnitureTypeId");
            CreateIndex("dbo.Products", "FacingColorId");
            AddForeignKey("dbo.FrameColorsToCollectionsVariants", "FrameColorId", "dbo.FrameColors", "Id", cascadeDelete: true);
            AddForeignKey("dbo.FrameColorsToCollectionsVariants", "CollectionVariantId", "dbo.CollectionsVariants", "Id", cascadeDelete: true);
            AddForeignKey("dbo.FacingsColorsToCollectionsVariants", "FacingColorId", "dbo.FacingsColors", "Id", cascadeDelete: true);
            AddForeignKey("dbo.FacingsColorsToCollectionsVariants", "CollectionVariantId", "dbo.CollectionsVariants", "Id", cascadeDelete: true);
            AddForeignKey("dbo.FacingsToCollectionsVariants", "FacingId", "dbo.Facings", "Id", cascadeDelete: true);
            AddForeignKey("dbo.FacingsToCollectionsVariants", "CollectionVariantId", "dbo.CollectionsVariants", "Id", cascadeDelete: true);
        }
    }
}
