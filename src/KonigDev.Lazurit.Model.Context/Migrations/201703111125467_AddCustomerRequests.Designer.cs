// <auto-generated />
namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddCustomerRequests : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddCustomerRequests));
        
        string IMigrationMetadata.Id
        {
            get { return "201703111125467_AddCustomerRequests"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
