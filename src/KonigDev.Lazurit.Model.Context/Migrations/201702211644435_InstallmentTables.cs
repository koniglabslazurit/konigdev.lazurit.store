namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InstallmentTables : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Articles", new[] { "SyncCode1C" });
            CreateTable(
                "dbo.Installments",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Discount = c.Byte(nullable: false),
                        Installment6Month = c.Byte(nullable: false),
                        Installment10Month = c.Byte(nullable: false),
                        Installment12Month = c.Byte(nullable: false),
                        Installment18Month = c.Byte(nullable: false),
                        Installment24Month = c.Byte(nullable: false),
                        Installment36Month = c.Byte(nullable: false),
                        Installment48Month = c.Byte(nullable: false),
                        DateStart = c.DateTime(nullable: false),
                        DateEnd = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.InstallmentsItems",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Discount = c.Byte(nullable: false),
                        Installment6Month = c.Byte(nullable: false),
                        Installment10Month = c.Byte(nullable: false),
                        Installment12Month = c.Byte(nullable: false),
                        Installment18Month = c.Byte(nullable: false),
                        Installment24Month = c.Byte(nullable: false),
                        Installment36Month = c.Byte(nullable: false),
                        Installment48Month = c.Byte(nullable: false),
                        DateStart = c.DateTime(nullable: false),
                        DateEnd = c.DateTime(nullable: false),
                        ProductId = c.Guid(),
                        CollectionVariantId = c.Guid(),
                        InstallmentId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CollectionsVariants", t => t.CollectionVariantId)
                .ForeignKey("dbo.Installments", t => t.InstallmentId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId)
                .Index(t => new { t.ProductId, t.CollectionVariantId, t.DateStart, t.DateEnd }, name: "IX_DatesFksUnique")
                .Index(t => t.InstallmentId);
            
            AlterColumn("dbo.Articles", "SyncCode1C", c => c.String(nullable: false, maxLength: 50));
            CreateIndex("dbo.Articles", "SyncCode1C", unique: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InstallmentsItems", "ProductId", "dbo.Products");
            DropForeignKey("dbo.InstallmentsItems", "InstallmentId", "dbo.Installments");
            DropForeignKey("dbo.InstallmentsItems", "CollectionVariantId", "dbo.CollectionsVariants");
            DropIndex("dbo.InstallmentsItems", new[] { "InstallmentId" });
            DropIndex("dbo.InstallmentsItems", "IX_DatesFksUnique");
            DropIndex("dbo.Articles", new[] { "SyncCode1C" });
            AlterColumn("dbo.Articles", "SyncCode1C", c => c.String(maxLength: 50));
            DropTable("dbo.InstallmentsItems");
            DropTable("dbo.Installments");
            CreateIndex("dbo.Articles", "SyncCode1C", unique: true);
        }
    }
}
