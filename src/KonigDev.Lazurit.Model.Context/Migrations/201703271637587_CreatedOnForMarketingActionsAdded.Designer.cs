// <auto-generated />
namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class CreatedOnForMarketingActionsAdded : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CreatedOnForMarketingActionsAdded));
        
        string IMigrationMetadata.Id
        {
            get { return "201703271637587_CreatedOnForMarketingActionsAdded"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
