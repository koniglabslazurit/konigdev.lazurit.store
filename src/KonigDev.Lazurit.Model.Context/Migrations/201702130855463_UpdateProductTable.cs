namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateProductTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "SyncCode1CСonformity", c => c.String());
            AddColumn("dbo.Products", "IsDisassemblyState", c => c.Boolean(nullable: false));
            AddColumn("dbo.Products", "Descr", c => c.String());
            AddColumn("dbo.Products", "PermissibleLoad", c => c.String());
            AddColumn("dbo.Products", "SleepingAreaLength", c => c.String());
            AddColumn("dbo.Products", "SleepingAreaWidth", c => c.String());
            AddColumn("dbo.Products", "Warranty", c => c.String());
            AddColumn("dbo.Products", "RoomSize", c => c.String());
            AddColumn("dbo.Products", "SleepingAreaAmount", c => c.Short(nullable: false));
            AddColumn("dbo.Products", "Cornice", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "Cornice");
            DropColumn("dbo.Products", "SleepingAreaAmount");
            DropColumn("dbo.Products", "RoomSize");
            DropColumn("dbo.Products", "Warranty");
            DropColumn("dbo.Products", "SleepingAreaWidth");
            DropColumn("dbo.Products", "SleepingAreaLength");
            DropColumn("dbo.Products", "PermissibleLoad");
            DropColumn("dbo.Products", "Descr");
            DropColumn("dbo.Products", "IsDisassemblyState");
            DropColumn("dbo.Products", "SyncCode1CСonformity");
        }
    }
}
