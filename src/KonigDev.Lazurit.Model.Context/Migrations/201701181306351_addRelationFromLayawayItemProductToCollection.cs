namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addRelationFromLayawayItemProductToCollection : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LayawayItemProducts", "CollectionId", c => c.Guid(nullable: false));
            CreateIndex("dbo.LayawayItemProducts", "CollectionId");
            AddForeignKey("dbo.LayawayItemProducts", "CollectionId", "dbo.Collections", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LayawayItemProducts", "CollectionId", "dbo.Collections");
            DropIndex("dbo.LayawayItemProducts", new[] { "CollectionId" });
            DropColumn("dbo.LayawayItemProducts", "CollectionId");
        }
    }
}
