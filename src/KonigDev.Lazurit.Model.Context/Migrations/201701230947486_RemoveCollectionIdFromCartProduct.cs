namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveCollectionIdFromCartProduct : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CartItemProduct", "CollectionId", "dbo.Collections");
            DropIndex("dbo.CartItemProduct", new[] { "CollectionId" });
            DropColumn("dbo.CartItemProduct", "CollectionId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CartItemProduct", "CollectionId", c => c.Guid(nullable: false));
            CreateIndex("dbo.CartItemProduct", "CollectionId");
            AddForeignKey("dbo.CartItemProduct", "CollectionId", "dbo.Collections", "Id", cascadeDelete: true);
        }
    }
}
