namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditMaxValueProductSyncCode : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Products", new[] { "SyncCode1C" });
            AlterColumn("dbo.Products", "SyncCode1C", c => c.String(nullable: false, maxLength: 50));
            CreateIndex("dbo.Products", "SyncCode1C", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.Products", new[] { "SyncCode1C" });
            AlterColumn("dbo.Products", "SyncCode1C", c => c.String(nullable: false, maxLength: 25));
            CreateIndex("dbo.Products", "SyncCode1C", unique: true);
        }
    }
}
