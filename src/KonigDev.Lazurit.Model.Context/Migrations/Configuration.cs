﻿using KonigDev.Lazurit.Core.Enums;
using KonigDev.Lazurit.Model.Entities;

namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DataContext.LazuritBaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DataContext.LazuritBaseContext context)
        {
            var fromMailSetting = context.Set<Settings>().SingleOrDefault(x => x.Name == Constants.FromMailName);
            if (fromMailSetting == null)
                context.Set<Settings>().Add(new Settings { Id = Guid.NewGuid(), Name = Constants.FromMailName, Value = Constants.FromMailTestValue });
            var smtpPortSetting = context.Set<Settings>().SingleOrDefault(x => x.Name == Constants.SmtpPortName);
            if (smtpPortSetting == null)
                context.Set<Settings>().Add(new Settings { Id = Guid.NewGuid(), Name = Constants.SmtpPortName, Value = Constants.SmtpPortTestValue });
            var smtpHostSetting = context.Set<Settings>().SingleOrDefault(x => x.Name == Constants.SmtpHostName);
            if (smtpHostSetting == null)
                context.Set<Settings>().Add(new Settings { Id = Guid.NewGuid(), Name = Constants.SmtpHostName, Value = Constants.SmtpHostTestValue });
            var smtpUserNameSetting = context.Set<Settings>().SingleOrDefault(x => x.Name == Constants.SmtpUserCredentName);
            if (smtpUserNameSetting == null)
                context.Set<Settings>().Add(new Settings { Id = Guid.NewGuid(), Name = Constants.SmtpUserCredentName, Value = Constants.UserNameTestValue });
            var smtpPasswordSetting = context.Set<Settings>().SingleOrDefault(x => x.Name == Constants.SmtpPasswordCredentName);
            if (smtpPasswordSetting == null)
                context.Set<Settings>().Add(new Settings { Id = Guid.NewGuid(), Name = Constants.SmtpPasswordCredentName, Value = Constants.PasswordTestValue });


            #region Set Files Content Types
            if (!context.Set<FileContentType>().Any())
            {
                var fileContentTypes = new List<FileContentType>
                {
                    new FileContentType {Id = Guid.NewGuid(), Name = "В интерьере", TechName = EnumFileContentType.InInterior.ToString(), SortNumber = (short)EnumFileContentType.InInterior },
                    new FileContentType {Id = Guid.NewGuid(), Name = "Слайдер средний", TechName = EnumFileContentType.SliderMiddle.ToString(), SortNumber = (short)EnumFileContentType.SliderMiddle },
                    new FileContentType {Id = Guid.NewGuid(), Name = "Слайдер маленький", TechName = EnumFileContentType.SliderMin.ToString(), SortNumber = (short)EnumFileContentType.SliderMin },
                    new FileContentType {Id = Guid.NewGuid(), Name = "Тайл большой", TechName = EnumFileContentType.TileBig.ToString(), SortNumber = (short)EnumFileContentType.TileBig },
                    new FileContentType {Id = Guid.NewGuid(), Name = "Тайл маленький", TechName = EnumFileContentType.TileMin.ToString(), SortNumber = (short)EnumFileContentType.TileMin },
                };

                context.Set<FileContentType>().AddRange(fileContentTypes);
                context.SaveChanges();
            }

            if (!context.Set<FileContentType>().Any(p => p.TechName == EnumFileContentType.WithoutInterior.ToString()))
            {
                var fileContentType = new FileContentType { Id = Guid.NewGuid(), Name = "Изображение на белом фоне", TechName = EnumFileContentType.WithoutInterior.ToString(), SortNumber = (short)EnumFileContentType.WithoutInterior };
                context.Set<FileContentType>().Add(fileContentType);
                context.SaveChanges();
            }
            if (!context.Set<FileContentType>().Any(p => p.TechName == EnumFileContentType.SliderOpen.ToString()))
            {
                var fileContentType = new FileContentType { Id = Guid.NewGuid(), Name = "Слайдер открытый вид", TechName = EnumFileContentType.SliderOpen.ToString(), SortNumber = (short)EnumFileContentType.SliderOpen };
                context.Set<FileContentType>().Add(fileContentType);
                context.SaveChanges();
            }

            if (!context.Set<FileContentType>().Any(p => p.TechName == EnumFileContentType.SliderInInterior.ToString()))
            {
                var fileContentType = new FileContentType { Id = Guid.NewGuid(), Name = "Картинка для слайдера, в интерьере на странице продукта", TechName = EnumFileContentType.SliderInInterior.ToString(), SortNumber = (short)EnumFileContentType.SliderInInterior };
                context.Set<FileContentType>().Add(fileContentType);
                context.SaveChanges();
            }

            #endregion

            #region Set Product Markers
            if (!context.Set<ProductPartMarker>().Any())
            {
                var productPartMarkers = new List<ProductPartMarker>
                {
                    new ProductPartMarker {Id = Guid.NewGuid(), Name = "Existed And Required", TechName = EnumProductPartMarker.ExistedRequired.ToString(), SortNumber = (short)EnumProductPartMarker.ExistedRequired },
                    new ProductPartMarker {Id = Guid.NewGuid(), Name = "Existed And No Required", TechName = EnumProductPartMarker.ExistedNonRequired.ToString(), SortNumber = (short)EnumProductPartMarker.ExistedNonRequired },
                    new ProductPartMarker {Id = Guid.NewGuid(), Name = "Missing", TechName = EnumProductPartMarker.Missing.ToString(), SortNumber = (short)EnumProductPartMarker.Missing }
                };

                context.Set<ProductPartMarker>().AddRange(productPartMarkers);
                context.SaveChanges();
            }
            #endregion

            #region Set Payment Method
            if (!context.Set<PaymentMethod>().Any())
            {
                var paymentMethods = new List<PaymentMethod>
                {
                    new PaymentMethod {Id = Guid.NewGuid(), Name = "Банковские карты", TechName = EnumPaymentMethod.BankCard.ToString(), SortNumber = (short)EnumPaymentMethod.BankCard },
                    new PaymentMethod {Id = Guid.NewGuid(), Name = "Наличные", TechName = EnumPaymentMethod.Cash.ToString(), SortNumber = (short)EnumPaymentMethod.Cash },
                    new PaymentMethod {Id = Guid.NewGuid(), Name = "Рассрочка", TechName = EnumPaymentMethod.Instalment.ToString(), SortNumber = (short)EnumPaymentMethod.Instalment },
                    new PaymentMethod {Id = Guid.NewGuid(), Name = "Другой способ", TechName = EnumPaymentMethod.OtherMethod.ToString(), SortNumber = (short)EnumPaymentMethod.OtherMethod }

                };

                context.Set<PaymentMethod>().AddRange(paymentMethods);
                context.SaveChanges();
            }
            
            if (context.Set<PaymentMethod>().Any(x => x.TechName == EnumPaymentMethod.OtherMethod.ToString())) {
                var otherMethod = context.Set<PaymentMethod>().Single(x => x.TechName == EnumPaymentMethod.OtherMethod.ToString());
                context.Set<PaymentMethod>().Remove(otherMethod);
            }

            if (!context.Set<PaymentMethod>().Any(x=>x.TechName == EnumPaymentMethod.QiwiWallet.ToString())) {
                context.Set<PaymentMethod>().Add(
                    new PaymentMethod { Id = Guid.NewGuid(), Name="Qiwi кошелек", TechName = EnumPaymentMethod.QiwiWallet.ToString() , SortNumber = (short)EnumPaymentMethod.QiwiWallet }
                );
            }

            if (!context.Set<PaymentMethod>().Any(x => x.TechName == EnumPaymentMethod.YandexMoney.ToString()))
            {
                context.Set<PaymentMethod>().Add(
                    new PaymentMethod { Id = Guid.NewGuid(), Name = "Яндекс.Деньги", TechName = EnumPaymentMethod.YandexMoney.ToString(), SortNumber = (short)EnumPaymentMethod.YandexMoney}
                );
            }

            if (!context.Set<PaymentMethod>().Any(x => x.TechName == EnumPaymentMethod.PayPal.ToString()))
            {
                context.Set<PaymentMethod>().Add(
                    new PaymentMethod { Id = Guid.NewGuid(), Name = "PayPal", TechName = EnumPaymentMethod.PayPal.ToString(), SortNumber = (short)EnumPaymentMethod.PayPal}
                );
            }

            #endregion

            #region
            //if (!context.Set<CollectionType>().Any())
            //{
            //    var CollectionTypes = new List<CollectionType>
            //    {
            //        new CollectionType { Id = Guid.NewGuid(), Name = "Коллекция", TechName = EnumCollectionTypes.Collection.ToString(), SortNumber = (short)EnumCollectionTypes.Collection},
            //        new CollectionType { Id = Guid.NewGuid(), Name = "Коплект", TechName = EnumCollectionTypes.Komplekt.ToString(), SortNumber = (short)EnumCollectionTypes.Komplekt},
            //    };
            //    context.Set<CollectionType>().AddRange(CollectionTypes);
            //    context.SaveChanges();
            //}
            #endregion

        }
    }
}