namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFieldIsDefaultForProduct : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "IsDefault", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "IsDefault");
        }
    }
}
