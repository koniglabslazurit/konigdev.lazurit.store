namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class KkrFieldInOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "KkmOrderRecordUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "KkmOrderRecordUrl");
        }
    }
}
