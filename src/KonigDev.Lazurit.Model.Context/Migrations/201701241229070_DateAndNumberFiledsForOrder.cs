namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DateAndNumberFiledsForOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "CreationTime1C", c => c.DateTime());
            AddColumn("dbo.Orders", "OrderNumber1C", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "OrderNumber1C");
            DropColumn("dbo.Orders", "CreationTime1C");
        }
    }
}
