namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactorByIntegration : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Articles", new[] { "SyncCode1C" });
            DropIndex("dbo.FurnituresTypes", new[] { "Alias" });
            DropIndex("dbo.FurnituresTypes", new[] { "SyncCode1C" });
            DropIndex("dbo.RoomsTypes", new[] { "Alias" });
            DropIndex("dbo.Series", new[] { "Alias" });
            CreateTable(
                "dbo.Styles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SyncCode1C = c.String(nullable: false, maxLength: 100),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.SyncCode1C, unique: true);
            
            CreateTable(
                "dbo.TargetAudiences",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SyncCode1C = c.String(nullable: false, maxLength: 100),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.SyncCode1C, unique: true);
            
            CreateTable(
                "dbo.StylesToProducts",
                c => new
                    {
                        ProductId = c.Guid(nullable: false),
                        StyleId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProductId, t.StyleId })
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.Styles", t => t.StyleId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.StyleId);
            
            CreateTable(
                "dbo.TargetAudiencesToProducts",
                c => new
                    {
                        TargetAudienceId = c.Guid(nullable: false),
                        TargetAudience_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.TargetAudienceId, t.TargetAudience_Id })
                .ForeignKey("dbo.Products", t => t.TargetAudienceId, cascadeDelete: true)
                .ForeignKey("dbo.TargetAudiences", t => t.TargetAudience_Id, cascadeDelete: true)
                .Index(t => t.TargetAudienceId)
                .Index(t => t.TargetAudience_Id);
            
            AddColumn("dbo.CollectionsVariants", "IsDefault", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Articles", "SyncCode1C", c => c.String(maxLength: 50));
            AlterColumn("dbo.FurnituresTypes", "Alias", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.FurnituresTypes", "SyncCode1C", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.RoomsTypes", "Alias", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.RoomsTypes", "SyncCode1C", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Series", "Alias", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.CollectionsTypes", "TechName", c => c.String(nullable: false, maxLength: 25));
            AlterColumn("dbo.ProductPartMarkers", "TechName", c => c.String(nullable: false, maxLength: 25));
            AlterColumn("dbo.FilesContentTypes", "TechName", c => c.String(nullable: false, maxLength: 25));
            AlterColumn("dbo.PaymentMethods", "TechName", c => c.String(nullable: false, maxLength: 25));
            CreateIndex("dbo.Articles", "SyncCode1C", unique: true);
            CreateIndex("dbo.FurnituresTypes", "Alias", unique: true);
            CreateIndex("dbo.FurnituresTypes", "SyncCode1C", unique: true);
            CreateIndex("dbo.RoomsTypes", "Alias", unique: true);
            CreateIndex("dbo.RoomsTypes", "SyncCode1C", unique: true);
            CreateIndex("dbo.Series", "Alias", unique: true);
            CreateIndex("dbo.CollectionsTypes", "TechName", unique: true);
            CreateIndex("dbo.ProductPartMarkers", "TechName", unique: true);
            CreateIndex("dbo.FilesContentTypes", "TechName", unique: true);
            CreateIndex("dbo.PaymentMethods", "TechName", unique: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TargetAudiencesToProducts", "TargetAudience_Id", "dbo.TargetAudiences");
            DropForeignKey("dbo.TargetAudiencesToProducts", "TargetAudienceId", "dbo.Products");
            DropForeignKey("dbo.StylesToProducts", "StyleId", "dbo.Styles");
            DropForeignKey("dbo.StylesToProducts", "ProductId", "dbo.Products");
            DropIndex("dbo.TargetAudiencesToProducts", new[] { "TargetAudience_Id" });
            DropIndex("dbo.TargetAudiencesToProducts", new[] { "TargetAudienceId" });
            DropIndex("dbo.StylesToProducts", new[] { "StyleId" });
            DropIndex("dbo.StylesToProducts", new[] { "ProductId" });
            DropIndex("dbo.PaymentMethods", new[] { "TechName" });
            DropIndex("dbo.FilesContentTypes", new[] { "TechName" });
            DropIndex("dbo.TargetAudiences", new[] { "SyncCode1C" });
            DropIndex("dbo.Styles", new[] { "SyncCode1C" });
            DropIndex("dbo.ProductPartMarkers", new[] { "TechName" });
            DropIndex("dbo.CollectionsTypes", new[] { "TechName" });
            DropIndex("dbo.Series", new[] { "Alias" });
            DropIndex("dbo.RoomsTypes", new[] { "SyncCode1C" });
            DropIndex("dbo.RoomsTypes", new[] { "Alias" });
            DropIndex("dbo.FurnituresTypes", new[] { "SyncCode1C" });
            DropIndex("dbo.FurnituresTypes", new[] { "Alias" });
            DropIndex("dbo.Articles", new[] { "SyncCode1C" });
            AlterColumn("dbo.PaymentMethods", "TechName", c => c.String(nullable: false));
            AlterColumn("dbo.FilesContentTypes", "TechName", c => c.String());
            AlterColumn("dbo.ProductPartMarkers", "TechName", c => c.String(nullable: false));
            AlterColumn("dbo.CollectionsTypes", "TechName", c => c.String());
            AlterColumn("dbo.Series", "Alias", c => c.String(nullable: false, maxLength: 25));
            AlterColumn("dbo.RoomsTypes", "SyncCode1C", c => c.String());
            AlterColumn("dbo.RoomsTypes", "Alias", c => c.String(nullable: false, maxLength: 300));
            AlterColumn("dbo.FurnituresTypes", "SyncCode1C", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.FurnituresTypes", "Alias", c => c.String(nullable: false, maxLength: 25));
            AlterColumn("dbo.Articles", "SyncCode1C", c => c.String(maxLength: 255));
            DropColumn("dbo.CollectionsVariants", "IsDefault");
            DropTable("dbo.TargetAudiencesToProducts");
            DropTable("dbo.StylesToProducts");
            DropTable("dbo.TargetAudiences");
            DropTable("dbo.Styles");
            CreateIndex("dbo.Series", "Alias", unique: true);
            CreateIndex("dbo.RoomsTypes", "Alias", unique: true);
            CreateIndex("dbo.FurnituresTypes", "SyncCode1C", unique: true);
            CreateIndex("dbo.FurnituresTypes", "Alias", unique: true);
            CreateIndex("dbo.Articles", "SyncCode1C", unique: true);
        }
    }
}
