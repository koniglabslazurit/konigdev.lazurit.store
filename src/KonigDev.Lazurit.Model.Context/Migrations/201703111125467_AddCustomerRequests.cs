namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCustomerRequests : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CustomerRequests",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CustomerFullName = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Phone = c.String(nullable: false),
                        Type = c.String(nullable: false),
                        Status = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CustomerRequests");
        }
    }
}
