namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRangeProduct : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "Range", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "Range");
        }
    }
}
