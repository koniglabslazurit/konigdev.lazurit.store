﻿using KonigDev.Lazurit.Model.Entities;
using System.Data.Entity;

namespace KonigDev.Lazurit.Model.Context.DataContext
{
    public partial class LazuritBaseContext
    {
        private void FavoriteBuilder(DbModelBuilder modelBuilder)
        {
            //tables
            modelBuilder.Entity<Favorite>()
                .ToTable("Favorites")
                .HasKey(x => x.Id);

            modelBuilder.Entity<FavoriteItemCollection>()
                .ToTable("FavoriteItemCollections")
                .HasKey(x => x.Id);

            modelBuilder.Entity<FavoriteItemComplect>()
                .ToTable("FavoriteItemComplects")
                .HasKey(x => x.Id);

            modelBuilder.Entity<FavoriteItemProduct>()
                .ToTable("FavoriteItemProducts")
                .HasKey(x => x.Id);

            modelBuilder.Entity<FavoriteItemProductInCollection>()
                .ToTable("FavoriteItemProductInCollections")
                .HasKey(x => x.Id);

            //relations
            #region Favorite
            modelBuilder.Entity<Favorite>()
                .HasRequired(x => x.UserProfile).WithOptional();
            #endregion

            #region Favorite complect
            modelBuilder.Entity<FavoriteItemComplect>().HasRequired(x => x.Favorite).WithMany(x => x.Complects).HasForeignKey(x => x.FavoriteId);
            modelBuilder.Entity<FavoriteItemComplect>().HasRequired(x => x.CollectionVariant).WithMany(x=>x.FavoriteItemComplects).HasForeignKey(p => p.CollectionVariantId).WillCascadeOnDelete(false);
            #endregion

            #region Favorite collection
            modelBuilder.Entity<FavoriteItemCollection>().HasRequired(x => x.Favorite).WithMany(x => x.Collections).HasForeignKey(x => x.FavoriteId);
            modelBuilder.Entity<FavoriteItemCollection>().HasRequired(x => x.CollectionVariant).WithMany(x=>x.FavoriteItemCollections).HasForeignKey(p => p.CollectionVariantId).WillCascadeOnDelete(false);
            #endregion

            #region Favorite product
            modelBuilder.Entity<FavoriteItemProduct>()
                .HasRequired(x => x.Favorite)
                .WithMany(x => x.Products)
                .HasForeignKey(x => x.FavoriteId);
            modelBuilder.Entity<FavoriteItemProduct>()
                .HasRequired(x => x.Product)
                .WithMany(x=>x.FavoriteItemProducts)
                .HasForeignKey(p => p.ProductId)
                .WillCascadeOnDelete(false);
            #endregion

            #region Favorite product-in-collection
            modelBuilder.Entity<FavoriteItemProductInCollection>().HasRequired(x => x.FavoriteItemCollection).WithMany(x => x.Products).HasForeignKey(x => x.FavoriteItemCollectionId);
            modelBuilder.Entity<FavoriteItemProductInCollection>().HasRequired(x => x.Product).WithMany().HasForeignKey(p => p.ProductId).WillCascadeOnDelete(false);
            #endregion
        }
    }
}
