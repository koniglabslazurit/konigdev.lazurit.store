﻿using KonigDev.Lazurit.Model.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;

namespace KonigDev.Lazurit.Model.Context.DataContext
{
    public partial class LazuritBaseContext
    {
        private void FileContentBuilder(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FileContent>()
                .ToTable("FilesContent")
                .HasKey(p => p.Id);

            modelBuilder.Entity<FileContent>()
                .Property(p => p.ObjectId)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute()));

            modelBuilder.Entity<FileContent>()
                .Property(p => p.DateCreating)
                .IsRequired();

            modelBuilder.Entity<FileContent>()
                .HasRequired(p => p.Type)
                .WithMany()
                .HasForeignKey(p => p.TypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FileContentType>()
                .ToTable("FilesContentTypes")
                .HasKey(p => p.Id);

            modelBuilder.Entity<FileContentType>()
                .Property(p => p.TechName)
                .IsRequired()
                .HasMaxLength(25)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = true }));
        }
    }
}