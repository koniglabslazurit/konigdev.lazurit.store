﻿using KonigDev.Lazurit.Model.Context.DataContext.Configurations;
using KonigDev.Lazurit.Model.Context.Migrations;
using KonigDev.Lazurit.Model.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;

namespace KonigDev.Lazurit.Model.Context.DataContext
{
    public partial class LazuritBaseContext : DbContext
    {
        public LazuritBaseContext() : base("name=LazuritBaseContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<LazuritBaseContext, Configuration>("LazuritBaseContext"));
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            #region User
            modelBuilder.Entity<UserProfile>()
                .ToTable("UsersProfiles")
                .HasKey(p => p.Id);
            modelBuilder.Entity<UserProfile>()
                .HasOptional(p => p.City)
                .WithMany()
                .HasForeignKey(p => p.CityId)
                .WillCascadeOnDelete(false);
            #endregion

            #region UserPhone
            modelBuilder.Entity<UserPhone>()
                .ToTable("UsersPhones")
                .HasKey(p => p.Id);

            modelBuilder.Entity<UserPhone>()
               .HasRequired(p => p.UserProfile)
               .WithMany(p => p.UserPhones)
               .HasForeignKey(p => p.UserProfileId);
            #endregion

            #region UserAddress
            modelBuilder.Entity<UserAddress>()
                .ToTable("UsersAddresses")
                .HasKey(p => p.Id);

            modelBuilder.Entity<UserAddress>()
              .HasRequired(p => p.UserProfile)
              .WithMany(p => p.UserAddresses)
              .HasForeignKey(p => p.UserProfileId);
            #endregion

            #region UserToShowRoom
            modelBuilder.Entity<UserToShowRoom>()
                .ToTable("UsersToShowRoom")
                .HasKey(p => new { p.UserId, p.ShowRoomId });

            modelBuilder.Entity<UserToShowRoom>()
                .HasRequired(p => p.ShowRoom)
                .WithMany()
                .HasForeignKey(p => p.ShowRoomId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserToShowRoom>()
                .HasRequired(p => p.User)
                .WithMany()
                .HasForeignKey(p => p.UserId)
                .WillCascadeOnDelete(false);
            #endregion           

            #region City
            modelBuilder.Entity<City>()
                .ToTable("Cities")
                .HasKey(p => p.Id);

            modelBuilder.Entity<City>()
                .HasRequired(p => p.Region)
                .WithMany()
                .HasForeignKey(p => p.RegionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<City>()
                .HasRequired(p => p.PriceZone)
                .WithMany()
                .HasForeignKey(p => p.PriceZoneId)
                .WillCascadeOnDelete(false);
            #endregion

            #region Region
            modelBuilder.Entity<Region>()
                .ToTable("Regions")
                .HasKey(p => p.Id);
            #endregion

            #region ShowRoom
            modelBuilder.Entity<Showroom>()
                .ToTable("Showrooms")
                .HasKey(p => p.Id);

            modelBuilder.Entity<Showroom>()
                .HasRequired(p => p.Region)
                .WithMany()
                .HasForeignKey(p => p.RegionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Showroom>()
                .HasOptional(p => p.City)
                .WithMany(p=>p.Showrooms)
                .HasForeignKey(p => p.CityId)
                .WillCascadeOnDelete(false);

            #endregion

            #region RegionsToUsers

            modelBuilder.Entity<UserProfile>()
                .HasMany<Region>(r => r.Regions)
                .WithMany(c => c.UserProfiles);
            #endregion

            #region Setting

            modelBuilder.Entity<Settings>()
                .ToTable("Settings")
                .HasKey(p => p.Id);

            #endregion

            #region Facing
            modelBuilder.Entity<Facing>()
                .ToTable("Facings")
                .HasKey(p => p.Id);
            #endregion

            #region FacingColor
            modelBuilder.Entity<FacingColor>()
                .ToTable("FacingsColors")
                .HasKey(p => p.Id);
            #endregion

            #region FrameColor
            modelBuilder.Entity<FrameColor>()
                .ToTable("FrameColors")
                .HasKey(p => p.Id);
            #endregion

            #region FurnitureType
            modelBuilder.Entity<FurnitureType>()
                .ToTable("FurnituresTypes")
                .HasKey(p => p.Id);

            modelBuilder.Entity<FurnitureType>()
                .Property(p => p.SyncCode1C)
                .IsRequired()
                .HasMaxLength(50)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = true }));

            modelBuilder.Entity<FurnitureType>()
                .Property(p => p.Alias)
                .IsRequired()
                .HasMaxLength(50)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = true }));
            #endregion

            #region Region
            modelBuilder.Entity<Region>()
                .ToTable("Regions")
                .HasKey(p => p.Id);
            #endregion

            #region RoomType
            modelBuilder.Entity<RoomType>()
                .ToTable("RoomsTypes")
                .HasKey(p => p.Id);

            modelBuilder.Entity<RoomType>()
              .Property(x => x.Alias)
              .HasMaxLength(50)
              .IsRequired()
              .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                  new IndexAnnotation(new IndexAttribute("IX_Alias") { IsUnique = true }));

            modelBuilder.Entity<RoomType>()
                .Property(p => p.SyncCode1C)
                .IsRequired()
                .HasMaxLength(50)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = true }));

            #endregion

            #region Series
            modelBuilder.Entity<Series>()
                .ToTable("Series")
                .HasKey(p => p.Id);

            modelBuilder.Entity<Series>()
                .Property(p => p.Alias)
                .IsRequired()
                .HasMaxLength(50)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = true }));

            #endregion


            #region TargetAudience
            modelBuilder.Entity<TargetAudience>()
                .ToTable("TargetAudiences")
                .HasKey(p => p.Id);

            modelBuilder.Entity<TargetAudience>()
                .Property(p => p.SyncCode1C)
                .IsRequired()
                .HasMaxLength(100)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = true }));

            #endregion

            #region Style
            modelBuilder.Entity<Style>()
                .ToTable("Styles")
                .HasKey(p => p.Id);

            modelBuilder.Entity<Style>()
                .Property(p => p.SyncCode1C)
                .IsRequired()
                .HasMaxLength(100)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = true }));

            #endregion

            CollectionBuilder(modelBuilder);
            ArticleBuilder(modelBuilder);
            FileContentBuilder(modelBuilder);
            ProductContextBuilder(modelBuilder);
            CartBuilder(modelBuilder);
            LayawayBuilder(modelBuilder);
            FavoriteBuilder(modelBuilder);
            PropertyBuilder(modelBuilder);
            OrderBuilder(modelBuilder);
            PaymentMethodBuilder(modelBuilder);
            InstallmentBuilder(modelBuilder);
            MarketingActionBuilder(modelBuilder);

            modelBuilder.Configurations.Add(new CustomerRequestEntityConfiguration());

            base.OnModelCreating(modelBuilder);
        }


    }
}
