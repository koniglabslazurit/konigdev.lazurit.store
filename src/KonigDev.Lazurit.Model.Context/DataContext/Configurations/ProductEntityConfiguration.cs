﻿using System.Data.Entity.ModelConfiguration;
using KonigDev.Lazurit.Model.Entities;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KonigDev.Lazurit.Model.Context.DataContext.Configurations
{
    public class ProductEntityConfiguration : EntityTypeConfiguration<Product>
    {
        public ProductEntityConfiguration()
        {
            ToTable("Products")
              .HasKey(p => p.Id);

            HasRequired(p => p.ProductPartMarker)
           .WithMany()
           .HasForeignKey(p => p.ProductPartMarkerId)
           .WillCascadeOnDelete(false);

            HasRequired(p => p.Facing)
            .WithMany(p => p.Products)
            .HasForeignKey(p => p.FacingId)
            .WillCascadeOnDelete(false);

            HasOptional(p => p.FacingColor)
            .WithMany(p => p.Products)
            .HasForeignKey(p => p.FacingColorId)
            .WillCascadeOnDelete(false);

            HasRequired(p => p.FrameColor)
            .WithMany(p => p.Products)
            .HasForeignKey(p => p.FrameColorId)
            .WillCascadeOnDelete(false);

            HasRequired(p => p.Article)
            .WithMany(p => p.Products)
            .HasForeignKey(p => p.ArticleId)
            .WillCascadeOnDelete(false);

            HasMany(x => x.Prices)
                .WithOptional(x => x.Product)
                .HasForeignKey(x => x.ProductId)
                .WillCascadeOnDelete(true);

            HasRequired(p => p.Collection)
                .WithMany(p => p.Products)
                .HasForeignKey(p => p.CollectionId);

            Property(p => p.SyncCode1C)
                .IsRequired()
                .HasMaxLength(50)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = true }));

            HasMany(p => p.CollectionsVariants)
            .WithMany(p => p.Products)
            .Map(p =>
            {
                p.ToTable("ProductsToCollectionsVariants");
                p.MapLeftKey("ProductId");
                p.MapRightKey("CollectionVariantId");
            });

            HasMany(p => p.TargetAudiences)
                .WithMany(p => p.Products)
                .Map(p =>
                {
                    p.ToTable("TargetAudiencesToProducts");
                    p.MapLeftKey("ProductId");
                    p.MapLeftKey("TargetAudienceId");
                });

            HasMany(p => p.Styles)
                .WithMany(p => p.Products)
                .Map(p =>
                {
                    p.ToTable("StylesToProducts");
                    p.MapLeftKey("ProductId");
                    p.MapRightKey("StyleId");
                });
        }
    }
}
