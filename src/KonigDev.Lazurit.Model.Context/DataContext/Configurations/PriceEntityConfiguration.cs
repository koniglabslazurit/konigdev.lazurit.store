﻿using KonigDev.Lazurit.Model.Entities;
using System.Data.Entity.ModelConfiguration;

namespace KonigDev.Lazurit.Model.Context.DataContext.Configurations
{
    public class PriceEntityConfiguration : EntityTypeConfiguration<Price>
    {
        public PriceEntityConfiguration()
        {
            ToTable("Prices")
            .HasKey(p => p.Id);

            Property(x => x.Value)
            .IsRequired();

            HasRequired(x => x.PriceZone)
            .WithMany(x => x.Prices)
            .HasForeignKey(x => x.PriceZoneId)
            .WillCascadeOnDelete(false);

        }
    }
}
