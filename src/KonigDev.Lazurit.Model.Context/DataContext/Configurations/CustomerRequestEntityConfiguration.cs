﻿using KonigDev.Lazurit.Model.Entities;
using System.Data.Entity.ModelConfiguration;

namespace KonigDev.Lazurit.Model.Context.DataContext.Configurations
{
    public class CustomerRequestEntityConfiguration : EntityTypeConfiguration<CustomerRequest>
    {
        public CustomerRequestEntityConfiguration()
        {
            ToTable("CustomerRequests")
              .HasKey(p => p.Id);

            Property(p => p.CreationTime)
                .IsRequired();

            Property(p => p.CustomerFullName)
                .IsRequired();

            Property(p => p.Email)
                .IsRequired();

            Property(p => p.Phone)
                .IsRequired();

            Property(p => p.Type)
                .IsRequired();

            Property(p => p.Status)
                .IsRequired();
        }
    }
}
