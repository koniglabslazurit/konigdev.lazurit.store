﻿using System.Data.Entity;
using KonigDev.Lazurit.Model.Context.DataContext.Configurations;
using KonigDev.Lazurit.Model.Entities;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KonigDev.Lazurit.Model.Context.DataContext
{
    public partial class LazuritBaseContext
    {
        private void ProductContextBuilder(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ProductEntityConfiguration());

            #region PriceZone
            modelBuilder.Entity<PriceZone>()
                .ToTable("PriceZones")
                .HasKey(p => p.Id);

            #endregion

            #region Product Part Marker
            modelBuilder.Entity<ProductPartMarker>()
                .ToTable("ProductPartMarkers")
                .HasKey(p => p.Id);

            modelBuilder.Entity<ProductPartMarker>()
                .Property(p => p.Name)
                .IsRequired();

            modelBuilder.Entity<ProductPartMarker>()
               .Property(p => p.TechName)
               .IsRequired()
               .HasMaxLength(25)
               .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = true }));


            #endregion

            #region ProductPack
            modelBuilder.Entity<ProductPack>()
                .ToTable("ProductsPacking")
                .HasKey(p => p.Id);

            modelBuilder.Entity<ProductPack>()
                .Property(p => p.ProductSyncCode1C)
                .IsRequired()
                .HasMaxLength(50)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = false }));

            modelBuilder.Entity<ProductPack>()
                .Property(p => p.Amount)
                .IsRequired();

            #endregion

            modelBuilder.Configurations.Add(new PriceEntityConfiguration());

        }
    }
}
