﻿using KonigDev.Lazurit.Model.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;

namespace KonigDev.Lazurit.Model.Context.DataContext
{
    partial class LazuritBaseContext
    {
        private void PaymentMethodBuilder(DbModelBuilder modelBuilder)
        {
            #region Product Part Marker

            modelBuilder.Entity<PaymentMethod>()
                .ToTable("PaymentMethods")
                .HasKey(p => p.Id);

            modelBuilder.Entity<PaymentMethod>()
                .Property(p => p.Name)
                .IsRequired();

            modelBuilder.Entity<PaymentMethod>()
               .Property(p => p.TechName)
               .IsRequired()
               .HasMaxLength(25)
               .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = true })); ;

            #endregion
        }
    }
}
