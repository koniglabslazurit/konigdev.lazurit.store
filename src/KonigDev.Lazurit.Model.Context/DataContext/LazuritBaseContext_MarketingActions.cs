﻿using KonigDev.Lazurit.Model.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Model.Context.DataContext
{
    public partial class LazuritBaseContext
    {
        private void MarketingActionBuilder(DbModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<MarketingAction>()
                .ToTable("MarketingActions")
                .HasKey(x => x.Id);

            modelBuilder
                .Entity<MarketingAction>()
                .Property(x => x.SeoId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}
