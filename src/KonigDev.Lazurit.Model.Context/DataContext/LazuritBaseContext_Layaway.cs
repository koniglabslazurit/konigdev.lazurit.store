﻿using KonigDev.Lazurit.Model.Entities;
using System.Data.Entity;

namespace KonigDev.Lazurit.Model.Context.DataContext
{
    public partial class LazuritBaseContext
    {
        private void LayawayBuilder(DbModelBuilder modelBuilder)
        {
            //tables
            modelBuilder.Entity<Layaway>()
                .ToTable("Layaways")
                .HasKey(x => x.Id);

            modelBuilder.Entity<LayawayItemCollection>()
                .ToTable("LayawayItemCollections")
                .HasKey(x => x.Id);

            modelBuilder.Entity<LayawayItemComplect>()
                .ToTable("LayawayItemComplects")
                .HasKey(x => x.Id);

            modelBuilder.Entity<LayawayItemProduct>()
                .ToTable("LayawayItemProducts")
                .HasKey(x => x.Id);

            modelBuilder.Entity<LayawayItemProductInCollection>()
                .ToTable("LayawayItemProductInCollections")
                .HasKey(x => x.Id);

            //relations
            #region Layaway complect
            modelBuilder.Entity<Layaway>()
                .HasOptional(x => x.User)
                .WithMany()
                .HasForeignKey(p => p.UserId);

            modelBuilder.Entity<LayawayItemComplect>()
                .HasRequired(x => x.Layaway)
                .WithMany(x => x.Complects)
                .HasForeignKey(x => x.LayawayId);

            modelBuilder.Entity<LayawayItemComplect>()
                .HasRequired(x => x.CollectionVariant)
                .WithMany()
                .HasForeignKey(p => p.CollectionVarinatId);
            #endregion

            #region Layaway collection
            modelBuilder.Entity<LayawayItemCollection>()
                .HasRequired(x => x.Layaway)
                .WithMany(x => x.Collections)
                .HasForeignKey(x => x.LayawayId);

            modelBuilder.Entity<LayawayItemCollection>()
                .HasRequired(x => x.CollectionVariant)
                .WithMany()
                .HasForeignKey(p => p.CollectionVarinatId);
            #endregion

            #region Layaway product
            modelBuilder.Entity<LayawayItemProduct>()
                .HasRequired(x => x.Layaway)
                .WithMany(x => x.Products)
                .HasForeignKey(x => x.LayawayId);

            modelBuilder.Entity<LayawayItemProduct>()
                .HasRequired(x => x.Product)
                .WithMany()
                .HasForeignKey(p => p.ProductId);
            #endregion

            #region Layaway product-in-collection
            modelBuilder.Entity<LayawayItemProductInCollection>()
                .HasRequired(x => x.Product)
                .WithMany()
                .HasForeignKey(p => p.ProductId);

            modelBuilder.Entity<LayawayItemProductInCollection>()
                .HasRequired(x => x.LayawayItemCollection)
                .WithMany(x => x.ProductsInCollection)
                .HasForeignKey(x => x.LayawayItemCollectionId);
            #endregion
            //
        }
    }
}
