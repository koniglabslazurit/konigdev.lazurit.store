﻿using System.Data.Entity;
using KonigDev.Lazurit.Model.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace KonigDev.Lazurit.Model.Context.DataContext
{
    public partial class LazuritBaseContext
    {
        private void OrderBuilder(DbModelBuilder modelBuilder)
        {
            //tables
            modelBuilder.Entity<Order>()
                .ToTable("Orders")
                .HasKey(x => x.Id);

            modelBuilder.Entity<OrderItemComplect>()
                .ToTable("OrderItemComplects")
                .HasKey(x => x.Id);
            modelBuilder.Entity<OrderItemProduct>()
                .ToTable("OrderItemProducts")
                .HasKey(x => x.Id);

            modelBuilder.Entity<OrderItemCollection>()
                .ToTable("OrderItemCollections")
                .HasKey(x => x.Id);

            modelBuilder.Entity<OrderItemProductInCollection>()
                .ToTable("OrderItemProductInCollections")
                .HasKey(x => x.Id);
            
            //relations
            modelBuilder.Entity<Order>()
                .HasRequired(p => p.User)
                .WithMany()
                .HasForeignKey(p => p.UserProfileId);

            modelBuilder.Entity<Order>()
               .HasOptional(p => p.ApprovingUser)
               .WithMany()
               .HasForeignKey(p => p.ApprovingUserId);

            modelBuilder.Entity<Order>()
                .HasRequired(p => p.PaymentMethod)
                .WithMany()
                .HasForeignKey(p => p.PaymentMethodId);

            modelBuilder.Entity<Order>()
               .Property(p => p.Number)
               .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            #region Order complect

            modelBuilder.Entity<OrderItemComplect>()
                .HasRequired(x => x.Order)
                .WithMany(x => x.Complects)
                .HasForeignKey(x => x.OrderId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<OrderItemComplect>()
                .HasRequired(x => x.CollectionVariant)
                .WithMany()
                .HasForeignKey(p => p.CollectionVariantId)
                .WillCascadeOnDelete(true);

            #endregion

            #region Order product

            modelBuilder.Entity<OrderItemProduct>()
                .HasRequired(x => x.Order)
                .WithMany(x => x.Products)
                .HasForeignKey(x => x.OrderId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<OrderItemProduct>()
                .HasRequired(x => x.Product)
                .WithMany()
                .HasForeignKey(p => p.ProductId)
                .WillCascadeOnDelete(true);

            #endregion

            #region Order collection

            modelBuilder.Entity<OrderItemCollection>()
                .HasRequired(x => x.Order)
                .WithMany(x => x.Collections)
                .HasForeignKey(x => x.OrderId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<OrderItemCollection>()
                .HasRequired(x => x.CollectionVariant)
                .WithMany()
                .HasForeignKey(x => x.CollectionVariantId)
                .WillCascadeOnDelete(true);

            #endregion

            #region Order product in collection

            modelBuilder.Entity<OrderItemProductInCollection>()
                .HasRequired(x => x.OrderItemCollection)
                .WithMany(x => x.Products)
                .HasForeignKey(x => x.OrderItemCollectionId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<OrderItemProductInCollection>()
                .HasRequired(x => x.Product)
                .WithMany()
                .HasForeignKey(p => p.ProductId)
                .WillCascadeOnDelete(true);

            #endregion
        }
    }
}
