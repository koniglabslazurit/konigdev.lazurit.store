﻿using KonigDev.Lazurit.Model.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;

namespace KonigDev.Lazurit.Model.Context.DataContext
{
    public partial class LazuritBaseContext
    {
        private void PropertyBuilder(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Property>()
                .ToTable("Properties")
                .HasKey(p => p.Id);

            modelBuilder.Entity<Property>()
                .Property(p => p.GroupeId)
              .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("IX_Property_GroupeId")));

            modelBuilder.Entity<Property>()
              .Property(p => p.IsMultiValue)
              .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("IX_Property_IsMultiValue")));

            modelBuilder.Entity<Property>()
            .HasMany(p => p.Products)
            .WithMany(p => p.Properties)
            .Map(p =>
            {
                p.ToTable("PropertiesToProductss");
                p.MapLeftKey("PropertyId");
                p.MapRightKey("ProductId");
            });
        }
    }
}