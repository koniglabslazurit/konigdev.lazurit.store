﻿using System.Data.Entity;
using KonigDev.Lazurit.Model.Entities;


namespace KonigDev.Lazurit.Model.Context.DataContext
{
    public partial class LazuritBaseContext
    {
        private void CartBuilder(DbModelBuilder modelBuilder)
        {
            //tables
            modelBuilder.Entity<Cart>().ToTable("Carts").HasKey(x => x.Id);
            modelBuilder.Entity<CartItemComplect>().ToTable("CartItemComplects").HasKey(x => x.Id);
            modelBuilder.Entity<CartItemProduct>().ToTable("CartItemProduct").HasKey(x => x.Id);
            modelBuilder.Entity<CartItemCollection>().ToTable("CartItemCollections").HasKey(x => x.Id);
            modelBuilder.Entity<CartItemProductInCollection>().ToTable("CartItemProductInCollections").HasKey(x => x.Id);
            //relations

            modelBuilder.Entity<Cart>()
                .HasOptional(p => p.User)
                .WithMany()
                .HasForeignKey(p => p.UserId);
            
            #region Cart complect

            modelBuilder.Entity<CartItemComplect>()
                .HasRequired(x => x.Cart)
                .WithMany(x => x.Complects)
                .HasForeignKey(x => x.CartId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<CartItemComplect>()
                .HasRequired(x => x.CollectionVariant)
                .WithMany()
                .HasForeignKey(p => p.CollectionVariantId)
                .WillCascadeOnDelete(true);

            
            #endregion

            #region cart product
            modelBuilder.Entity<CartItemProduct>().HasRequired(x => x.Cart).WithMany(x => x.Products).HasForeignKey(x => x.CartId).WillCascadeOnDelete(true);
            modelBuilder.Entity<CartItemProduct>().HasRequired(x => x.Product).WithMany().HasForeignKey(p => p.ProductId).WillCascadeOnDelete(true);
            #endregion
            
            #region cart collection
            modelBuilder.Entity<CartItemCollection>().HasRequired(x => x.Cart).WithMany(x => x.Collections).HasForeignKey(x => x.CartId).WillCascadeOnDelete(true);
            modelBuilder.Entity<CartItemCollection>().HasRequired(x => x.CollectionVariant).WithMany().HasForeignKey(x => x.CollectionVariantId).WillCascadeOnDelete(true);            
            #endregion

            #region cart product in collection
            modelBuilder.Entity<CartItemProductInCollection>().HasRequired(x => x.CartItemCollection).WithMany(x => x.Products).HasForeignKey(x => x.CartItemCollectionId).WillCascadeOnDelete(true);
            modelBuilder.Entity<CartItemProductInCollection>().HasRequired(x => x.Product).WithMany().HasForeignKey(p => p.ProductId).WillCascadeOnDelete(true);            
            #endregion
            //
        }
    }
}
