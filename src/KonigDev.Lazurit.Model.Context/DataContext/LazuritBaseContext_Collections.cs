﻿using System.ComponentModel.DataAnnotations.Schema;
using KonigDev.Lazurit.Model.Entities;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;

namespace KonigDev.Lazurit.Model.Context.DataContext
{
    public partial class LazuritBaseContext
    {
        private void CollectionBuilder(DbModelBuilder modelBuilder)
        {
            #region Collection
            modelBuilder.Entity<Collection>()
                .ToTable("Collections")
                .HasKey(p => p.Id);

            modelBuilder.Entity<Collection>()
               .HasRequired(p => p.Series)
               .WithMany(p => p.Collections)
               .HasForeignKey(p => p.SeriesId)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<Collection>()
               .HasRequired(p => p.RoomType)
               .WithMany(p => p.Collections)
               .HasForeignKey(p => p.RoomTypeId)
               .WillCascadeOnDelete(false);            
            #endregion

            #region CollectionVariants
            modelBuilder.Entity<CollectionVariant>()
                .ToTable("CollectionsVariants")
                .HasKey(p => p.Id);

            modelBuilder.Entity<CollectionVariant>()
                .HasRequired(p => p.Collection)
                .WithMany(p => p.CollectionVariants)
                .HasForeignKey(p => p.CollectionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CollectionVariant>()
                .HasRequired(p => p.FacingColor)
                .WithMany(p => p.CollectionsVariants)
                .HasForeignKey(p => p.FacingColorId);

            modelBuilder.Entity<CollectionVariant>()
                .HasRequired(p => p.FrameColor)
                .WithMany(p => p.CollectionsVariants)
                .HasForeignKey(p => p.FrameColorId);

            modelBuilder.Entity<CollectionVariant>()
                .HasRequired(p => p.Facing)
                .WithMany(p => p.CollectionsVariants)
                .HasForeignKey(p => p.FacingId);

            modelBuilder.Entity<CollectionVariant>()
                .HasMany(p => p.CollectionVariantComplects)
                .WithMany()
                .Map(p =>
                {
                    p.ToTable("CollectionsVariantsToComplect");
                    p.MapLeftKey("CollectionId");
                    p.MapRightKey("CollectionComlectId");
                });

            #endregion


            #region CollectionVariantContextProductArea

            modelBuilder.Entity<CollectionVariantContentProductArea>()
             .ToTable("CollectionsVariantsContentProductsAreas")
             .HasKey(p => p.Id);

            modelBuilder.Entity<CollectionVariantContentProductArea>()
                .HasRequired(p => p.CollectionVariant)
                .WithMany(p => p.CollectionVariantContentProductAreas)
                .HasForeignKey(p => p.CollectionVariantId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CollectionVariantContentProductArea>()
                .HasRequired(p => p.Product)
                .WithMany()
                .HasForeignKey(p => p.ProductId)
                .WillCascadeOnDelete(false);

            #endregion
        }
    }
}
