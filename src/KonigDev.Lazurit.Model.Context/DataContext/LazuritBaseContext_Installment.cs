﻿using KonigDev.Lazurit.Model.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;

namespace KonigDev.Lazurit.Model.Context.DataContext
{
    public partial class LazuritBaseContext
    {
        private void InstallmentBuilder(DbModelBuilder modelBuilder)
        {
            #region Installment
            modelBuilder.Entity<Installment>()
               .ToTable("Installments")
               .HasKey(p => p.Id);

            modelBuilder.Entity<Installment>()
              .Property(p => p.Discount)
              .IsRequired();

            modelBuilder.Entity<Installment>()
               .Property(p => p.Installment6Month)
               .IsRequired();

            modelBuilder.Entity<Installment>()
                .Property(p => p.Installment10Month)
                .IsRequired();

            modelBuilder.Entity<Installment>()
                .Property(p => p.Installment12Month)
                .IsRequired();

            modelBuilder.Entity<Installment>()
                .Property(p => p.Installment18Month)
                .IsRequired();

            modelBuilder.Entity<Installment>()
                .Property(p => p.Installment24Month)
                .IsRequired();

            modelBuilder.Entity<Installment>()
                .Property(p => p.Installment36Month)
                .IsRequired();

            modelBuilder.Entity<Installment>()
                .Property(p => p.Installment48Month)
                .IsRequired();

            #endregion

            #region InstallmentItem
            modelBuilder.Entity<InstallmentItem>()
               .ToTable("InstallmentsItems")
               .HasKey(p => p.Id);

            modelBuilder.Entity<InstallmentItem>()
                .HasRequired(p => p.Installment)
                .WithMany(p => p.InstallmentItems)
                .HasForeignKey(p => p.InstallmentId);

            modelBuilder.Entity<InstallmentItem>()
                .HasOptional(p => p.Product)
                .WithMany()
                .HasForeignKey(p => p.ProductId);

            modelBuilder.Entity<InstallmentItem>()
                .HasOptional(p => p.CollectionVariant)
                .WithMany()
                .HasForeignKey(p => p.CollectionVariantId);

            modelBuilder.Entity<InstallmentItem>()
                .Property(p => p.ProductId)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new[] { new IndexAttribute("IX_DatesFksUnique") { Order = 1 } }));

            modelBuilder.Entity<InstallmentItem>()
                .Property(p => p.CollectionVariantId)                
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new[] { new IndexAttribute("IX_DatesFksUnique") { Order = 2 } }));

            modelBuilder.Entity<InstallmentItem>()
                .Property(p => p.DateStart)
                .IsRequired()
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new[] { new IndexAttribute("IX_DatesFksUnique") { Order = 3 } }));

            modelBuilder.Entity<InstallmentItem>()
                .Property(p => p.DateEnd)
                .IsRequired()
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new[] { new IndexAttribute("IX_DatesFksUnique") { Order = 4 } }));

            modelBuilder.Entity<InstallmentItem>()
               .Property(p => p.Discount)
               .IsRequired();

            modelBuilder.Entity<InstallmentItem>()
               .Property(p => p.Installment6Month)
               .IsRequired();

            modelBuilder.Entity<InstallmentItem>()
                .Property(p => p.Installment10Month)
                .IsRequired();

            modelBuilder.Entity<InstallmentItem>()
                .Property(p => p.Installment12Month)
                .IsRequired();

            modelBuilder.Entity<InstallmentItem>()
                .Property(p => p.Installment18Month)
                .IsRequired();

            modelBuilder.Entity<InstallmentItem>()
                .Property(p => p.Installment24Month)
                .IsRequired();

            modelBuilder.Entity<InstallmentItem>()
                .Property(p => p.Installment36Month)
                .IsRequired();

            modelBuilder.Entity<InstallmentItem>()
                .Property(p => p.Installment48Month)
                .IsRequired();
            #endregion
        }
    }
}