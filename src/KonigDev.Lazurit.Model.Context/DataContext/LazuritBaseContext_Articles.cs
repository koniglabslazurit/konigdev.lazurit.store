﻿using KonigDev.Lazurit.Model.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;

namespace KonigDev.Lazurit.Model.Context.DataContext
{
    public partial class LazuritBaseContext
    {
        private void ArticleBuilder(DbModelBuilder modelBuilder)
        {
            
            modelBuilder.Entity<Article>()
                .ToTable("Articles")
                .HasKey(p => p.Id);

            modelBuilder.Entity<Article>()
                .Property(p=>p.SyncCode1C)
                .HasMaxLength(50)
                .IsRequired()
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = true }));

            modelBuilder.Entity<Article>()
               .HasRequired(p => p.FurnitureType)
               .WithMany(p=>p.Articles)
               .HasForeignKey(p => p.FurnitureTypeId)
               .WillCascadeOnDelete(false);                        
        }
    }
}
