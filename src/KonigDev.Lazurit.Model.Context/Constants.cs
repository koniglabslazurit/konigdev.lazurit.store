﻿namespace KonigDev.Lazurit.Model.Context
{
    public static class Constants
    {
        #region emailSettigs
        public const string FromMailName = "FromMail";
        public const string SmtpPortName = "SmtpPort";
        public const string SmtpHostName = "SmtpHost";
        public const string SmtpUserCredentName = "UserNameCredential";
        public const string SmtpPasswordCredentName = "PasswordCredential";

        public const string FromMailTestValue = "LazuritTest@yandex.ru";
        public const string SmtpPortTestValue = "25";
        public const string SmtpHostTestValue = "smtp.yandex.ru";
        public const string UserNameTestValue = "LazuritTest";
        public const string PasswordTestValue = "Lazurit";

        #endregion
    }
}
