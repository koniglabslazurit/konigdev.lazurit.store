INSERT INTO [Series]
VALUES 
('0b911448-88c2-4e0f-9041-59ffcc4f32b0',N'МАГНА','0001'),
('e11e7b21-b6d7-45c9-bd68-615c239a4e32',N'ВИОЛА','0002'),
('6d8d5c48-9bff-43d2-a06c-0c26c5981621',N'ЭЛЕОНОРА','0003')

INSERT INTO [CollectionsTypes]
VALUES 
('e03b61dd-8d7b-47e6-b8e0-9d09540fa0b8',N'Type 1','0001',N'ТехИмя1','1'),
('1b6844f3-e0b0-4c75-b3cf-0ec8725f743a',N'Type 2','0002',N'ТехИмя2','2'),
('8a9365fd-5f4d-4a86-8035-fb8a8be9c2b5',N'Type 3','0003',N'ТехИмя3','3')

INSERT INTO [FurnituresTypes]
VALUES 
('ec2cbbc0-6b94-4d24-8a8e-1f2d8996167d',N'Кровать','0001',N'ТехИмя1','1'),
('80af7bd7-1d6e-4abf-9632-a1cdb17b229d',N'Шкаф','0002',N'ТехИмя2','2'),
('8410c516-3706-4161-8868-e5d4968d9dd5',N'Стол','0003',N'ТехИмя3','3')

INSERT INTO [RoomsTypes]
VALUES 
('cc5830ba-d6f4-4e2c-affc-bed0c2b279d3',N'Спальня','0001'),
('4810e081-111b-4daa-bf29-efde22c2f546',N'Гостиная','0002'),
('413cf2a9-5f5f-4450-8ffb-8a4cbd635b50',N'Прихожая','0003')

INSERT INTO [FrameColors]
VALUES 
('f157d159-61c1-482d-9b40-648875f278a6',N'Кедр шоколад','0001',N'ТехИмя1','1'),
('68dd4ecd-5e92-41a7-bb64-b204b4f4eeaf',N'Орех светлый','0002',N'ТехИмя1','2'),
('be89fe8e-cbe7-44a2-a8c2-c0f00193501a',N'Дуб беленый','0003',N'ТехИмя1','3')

INSERT INTO [FacingsColors]
VALUES 
('2969b41d-418c-45f1-b3c5-ad11b742cb18',N'Лава','0001',N'ТехИмя1','1'),
('f38330c9-0e1c-4dc0-bb45-f692f8e68fb0',N'Белый','0002',N'ТехИмя1','2'),
('554ba5d4-f931-4e0e-8fa7-e5223875df86',N'Карбон','0003',N'ТехИмя1','3')

INSERT INTO [Facings]
VALUES 
('4b450ac9-cdd2-47be-8d52-e808919b29f0',N'Бамбук','0001',N'ТехИмя1','1'),
('288b1222-d73d-4207-913b-1b9b269d1f78',N'Глянец','0002',N'ТехИмя1','2'),
('612a42f9-2eae-4401-8631-c15a1235a4c8',N'Без отделки','0003',N'ТехИмя1','3')

INSERT INTO [Articles]
VALUES 
('32f3e0a3-7217-4292-ae9b-fdcfa5f532f1',N'Спальня','0001' ,'50','60','120','ec2cbbc0-6b94-4d24-8a8e-1f2d8996167d'),
('55005458-248c-4f5f-a593-7ea460ab1f1d',N'Гостиная','0002','50','60','120','80af7bd7-1d6e-4abf-9632-a1cdb17b229d'),
('626ac761-e9a5-4f8c-85de-e1b3d86cdba4',N'Прихожая','0003','50','60','120','8410c516-3706-4161-8868-e5d4968d9dd5')

INSERT INTO [Collections]
VALUES 
('c4c1bd4d-bf1d-44d9-8a14-a72d36097137',N'Коллекция 1','0001','0b911448-88c2-4e0f-9041-59ffcc4f32b0','e03b61dd-8d7b-47e6-b8e0-9d09540fa0b8','cc5830ba-d6f4-4e2c-affc-bed0c2b279d3'),
('f411e527-3e17-445c-b968-b4ce0391002e',N'Коллекция 2','0002','e11e7b21-b6d7-45c9-bd68-615c239a4e32','1b6844f3-e0b0-4c75-b3cf-0ec8725f743a','4810e081-111b-4daa-bf29-efde22c2f546'),
('783a16e1-5734-4246-8237-f8c0648c81d1',N'Коллекция 3','0003','6d8d5c48-9bff-43d2-a06c-0c26c5981621','8a9365fd-5f4d-4a86-8035-fb8a8be9c2b5','413cf2a9-5f5f-4450-8ffb-8a4cbd635b50')


INSERT INTO [CollectionsVariants]
VALUES 
('939f0925-fbd0-49ad-9317-dc6e53719939',N'ВариантКоллекции1','0001','c4c1bd4d-bf1d-44d9-8a14-a72d36097137','4b450ac9-cdd2-47be-8d52-e808919b29f0','2969b41d-418c-45f1-b3c5-ad11b742cb18','f157d159-61c1-482d-9b40-648875f278a6'),
('8c2752b7-51c7-4ffd-a41c-6c05d44a0adb',N'ВариантКоллекции2','0002','f411e527-3e17-445c-b968-b4ce0391002e','288b1222-d73d-4207-913b-1b9b269d1f78','f38330c9-0e1c-4dc0-bb45-f692f8e68fb0','68dd4ecd-5e92-41a7-bb64-b204b4f4eeaf'),
('c682cdf8-b4aa-44ff-9820-57dc0ccca5eb',N'ВариантКоллекции3','0003','783a16e1-5734-4246-8237-f8c0648c81d1','612a42f9-2eae-4401-8631-c15a1235a4c8','554ba5d4-f931-4e0e-8fa7-e5223875df86','be89fe8e-cbe7-44a2-a8c2-c0f00193501a')

INSERT INTO [Regions]
VALUES 
('7a4e7369-368e-4251-8125-5112f4b47ee8',N'Центральный','0001','asdasdasd'),
('5faac3f7-187a-4609-83c5-e67668731eb2',N'Северный','0002','asdasdasd'),
('0c1d20f0-c3d5-45ec-92fc-457f22b00a4d',N'Южный','0003','asdasdasd')

INSERT INTO [Cities](Id,Title,RegionId)
VALUES 
('9fd3953b-d0bd-4eeb-ae20-219ebeb164dc',N'Москва'     ,'7a4e7369-368e-4251-8125-5112f4b47ee8'),
('795934ae-e863-40b0-9939-e62f81505def',N'Калининград','5faac3f7-187a-4609-83c5-e67668731eb2'),
('215a609f-0064-4dae-a85c-585b27252f69',N'Владивосток','0c1d20f0-c3d5-45ec-92fc-457f22b00a4d')

INSERT INTO [Showrooms]
VALUES 
('a03392e1-f78d-419f-b0fa-39438b0aa755',N'Магазин 1','0001',N'Кладр1',N'КладрA','7a4e7369-368e-4251-8125-5112f4b47ee8','9fd3953b-d0bd-4eeb-ae20-219ebeb164dc'),
('b9e57818-210d-4932-80f3-0766a322bdbb',N'Магазин 2','0002',N'Кладр2',N'КладрB','5faac3f7-187a-4609-83c5-e67668731eb2','795934ae-e863-40b0-9939-e62f81505def'),
('c8126fc3-29d8-4fe6-8ed1-4703d9265629',N'Магазин 3','0003',N'Кладр3',N'КладрC','0c1d20f0-c3d5-45ec-92fc-457f22b00a4d','215a609f-0064-4dae-a85c-585b27252f69')

INSERT INTO [FacingColorsToArticles]
VALUES 
('32f3e0a3-7217-4292-ae9b-fdcfa5f532f1','2969b41d-418c-45f1-b3c5-ad11b742cb18'),
('55005458-248c-4f5f-a593-7ea460ab1f1d','f38330c9-0e1c-4dc0-bb45-f692f8e68fb0'),
('626ac761-e9a5-4f8c-85de-e1b3d86cdba4','554ba5d4-f931-4e0e-8fa7-e5223875df86')

INSERT INTO [FacingColorsToCollections]
VALUES 
('c4c1bd4d-bf1d-44d9-8a14-a72d36097137','2969b41d-418c-45f1-b3c5-ad11b742cb18'),
('f411e527-3e17-445c-b968-b4ce0391002e','f38330c9-0e1c-4dc0-bb45-f692f8e68fb0'),
('783a16e1-5734-4246-8237-f8c0648c81d1','554ba5d4-f931-4e0e-8fa7-e5223875df86')

INSERT INTO [FacingsToArticles]
VALUES 
('32f3e0a3-7217-4292-ae9b-fdcfa5f532f1','4b450ac9-cdd2-47be-8d52-e808919b29f0'),
('55005458-248c-4f5f-a593-7ea460ab1f1d','288b1222-d73d-4207-913b-1b9b269d1f78'),
('626ac761-e9a5-4f8c-85de-e1b3d86cdba4','612a42f9-2eae-4401-8631-c15a1235a4c8')

INSERT INTO [FacingsToCollections]
VALUES 
('c4c1bd4d-bf1d-44d9-8a14-a72d36097137','4b450ac9-cdd2-47be-8d52-e808919b29f0'),
('f411e527-3e17-445c-b968-b4ce0391002e','288b1222-d73d-4207-913b-1b9b269d1f78'),
('783a16e1-5734-4246-8237-f8c0648c81d1','612a42f9-2eae-4401-8631-c15a1235a4c8')

INSERT INTO [FrameColorsToArticles]
VALUES 
('32f3e0a3-7217-4292-ae9b-fdcfa5f532f1','f157d159-61c1-482d-9b40-648875f278a6'),
('55005458-248c-4f5f-a593-7ea460ab1f1d','68dd4ecd-5e92-41a7-bb64-b204b4f4eeaf'),
('626ac761-e9a5-4f8c-85de-e1b3d86cdba4','be89fe8e-cbe7-44a2-a8c2-c0f00193501a')

INSERT INTO [FrameColorsToCollections]
VALUES 
('c4c1bd4d-bf1d-44d9-8a14-a72d36097137','f157d159-61c1-482d-9b40-648875f278a6'),
('f411e527-3e17-445c-b968-b4ce0391002e','68dd4ecd-5e92-41a7-bb64-b204b4f4eeaf'),
('783a16e1-5734-4246-8237-f8c0648c81d1','be89fe8e-cbe7-44a2-a8c2-c0f00193501a')

