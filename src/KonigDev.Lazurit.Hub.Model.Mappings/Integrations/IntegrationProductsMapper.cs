﻿using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Model.Entities;
using KonigDev.Lazurit.Store.Integration.Context.Entities;
using System;

namespace KonigDev.Lazurit.Hub.Model.Mappings.Integrations
{
    [Serializable]
    public static class IntegrationProductsMapper
    {
        public static IntegrationProduct MapToEntity(this DtoIntegrationProductItem item)
        {
            return new IntegrationProduct
            {
                SyncCode1C = item.SyncCode1C, /* id */
                Backlight = item.Backlight,
                Collection = item.Collection,
                Descr = item.Descr,
                Facing = item.Facing,
                FacingColor = item.FacingColor,
                FrameColor = item.FrameColor,
                FurnitureForm = item.FurnitureForm,
                FurnitureType = item.FurnitureType,
                Hash = item.Hash,
                Height = item.Height,
                LeftRight = item.LeftRight,
                Length = item.Length,
                Material = item.Material,
                Mechanism = item.Mechanism,
                MarketerRange = item.MarketerRange,
                MarketerVendorCode = item.MarketerVendorCode,
                Packing = item.Packing,
                PermissibleLoad = item.PermissibleLoad,
                ProductPartMarker = item.ProductPartMarker,
                RoomType = item.RoomType,
                Series = item.Series,
                SleepingAreaLength = item.SleepingAreaLength,
                SleepingAreaWidth = item.SleepingAreaWidth,
                Style = item.Style,
                Type = item.TargetAudience,
                VendorCode = item.VendorCode,
                VendorCodeList = item.VendorCodeList,
                Warranty = item.Warranty,
                Width = item.Width
            };
        }

        public static IntegrationProduct MapCommandToEntity(this UpdateIntegrationProductsCommand item, IntegrationProduct product)
        {

            product.SyncCode1C = item.SyncCode1C; /* id */
            product.SyncCode1CСonformity = item.SyncCode1CConformity;
            product.IsDefault = item.IsDefault;
            product.IsDisassemblyState = item.IsDisassemblyState;
            product.Backlight = item.Backlight;
            product.Descr = item.Descr;
            product.FurnitureForm = item.FurnitureForm;
            product.Height = item.Height;
            product.LeftRight = item.LeftRight;
            product.Length = item.Length;
            product.Material = item.Material;
            product.Mechanism = item.Mechanism;
            product.PermissibleLoad = item.PermissibleLoad;
            product.SleepingAreaLength = item.SleepingAreaLength;
            product.SleepingAreaWidth = item.SleepingAreaWidth;
            product.Warranty = item.Warranty;
            product.Width = item.Width;
            product.RoomSize = item.RoomSize;
            product.SleepingAreaAmount = item.SleepingAreaAmount;
            product.ProductPartMarker = item.ProductPartMarker;
            return product;
        }

        public static Product MapToProductEntity(this IntegrationProduct item, Product product)
        {
            product.SyncCode1C = item.SyncCode1C; /* id */
            product.SyncCode1CСonformity = item.SyncCode1CСonformity;
            product.IsDefault = item.IsDefault;
            product.IsDisassemblyState = item.IsDisassemblyState;
            product.Descr = item.Descr;
            product.PermissibleLoad = item.PermissibleLoad;
            product.SleepingAreaLength = item.SleepingAreaLength;
            product.SleepingAreaWidth = item.SleepingAreaWidth;
            product.Warranty = item.Warranty;
            product.RoomSize = item.RoomSize;
            product.SleepingAreaAmount = item.SleepingAreaAmount;
            product.FurnitureForm = item.FurnitureForm;
            product.MarketerVendorCode = item.MarketerVendorCode;
            product.Packing = item.Packing;
            product.MarketerVendorCode = item.MarketerVendorCode;
            product.VendorCode = item.VendorCode;
            product.Range = item.MarketerRange;
            product.IsComplect = item.IsComplect;
            /* TODO */
            product.Height = Convert.ToInt16(item.Height);
            product.Width = Convert.ToInt16(item.Width);
            product.Length = Convert.ToInt16(item.Length);

            return product;
        }
    }
}
