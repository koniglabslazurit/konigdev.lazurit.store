﻿namespace KonigDev.Lazurit.Hub.Model.Mappings.Integrations
{
    public class IntegrationMarketingRange
    {

        /// <summary>
        /// Вес артикула
        /// </summary>
        public string RangeVendorCode { get; private set; }

        /// <summary>
        /// Вес коллекции (тип команты плюс серия)
        /// </summary>
        public string RangeCollection { get; private set; }

        /// <summary>
        /// Вес серии
        /// </summary>
        public string RangeSeries { get; private set; }

        /// <summary>
        /// Вес комнаты
        /// </summary>
        public string RangeRoom { get; private set; }

        /// <summary>
        /// Общий вес
        /// </summary>
        public string RangeTotal { get; private set; }

        private void SetDefault()
        {
            RangeTotal = "0";
            RangeCollection = "0";
            RangeRoom = "0";
            RangeSeries = "0";
            RangeVendorCode = "0";
        }

        public IntegrationMarketingRange(string range)
        {
            if (string.IsNullOrWhiteSpace(range))
            {
                SetDefault();
                return;
            }

            var splitted = range.Split(' ');
            if (splitted.Length != 4)
            {
                SetDefault();
                return;
            }

            this.RangeVendorCode = splitted[3];
            this.RangeCollection = splitted[2];
            this.RangeSeries = splitted[1];
            this.RangeRoom = splitted[0];
            this.RangeTotal = range;
        }
    }
}
