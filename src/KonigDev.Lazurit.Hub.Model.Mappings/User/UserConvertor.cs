﻿using KonigDev.Lazurit.Hub.Model.DTO.Users;
using KonigDev.Lazurit.Model.Entities;
using System.Linq;

namespace KonigDev.Lazurit.Hub.Model.Mappings.User
{
    public static class UserConvertor
    {
        public static DtoUserProfile UserMapToDTOProfile(this UserProfile userProfile)
        {
            return new DtoUserProfile
            {
                Title = userProfile.Title,
                Id = userProfile.Id,
                LinkId = userProfile.LinkId,
                UserAddresses = userProfile.UserAddresses.Select(UserAddressMapToDTOUserAddress).ToList(),
                Email = userProfile.Email,
                FirstName = userProfile.FirstName,
                MiddleName = userProfile.MiddleName,
                LastName = userProfile.LastName,

                UserPhones = userProfile.UserPhones.Select(UserPhoneMapToDTOUserPhone).ToList()
            };
        }
        public static DtoUserPhone UserPhoneMapToDTOUserPhone (this UserPhone userPhone)
        {
            return new DtoUserPhone
            {
                Id = userPhone.Id,
                IsDefault = userPhone.IsDefault,
                Phone = userPhone.Phone
            };
        }
        public static DtoUserAddress UserAddressMapToDTOUserAddress (this UserAddress userAddress)
        {
            return new DtoUserAddress
            {
              Id = userAddress.Id,
              Address = userAddress.Address,
              IsDefault = userAddress.IsDefault,
              KladrCode = userAddress.KladrCode,
              City = userAddress.City,
              Street = userAddress.Street,
              Block = userAddress.Block,
              Building = userAddress.Building,
              House = userAddress.House,
              EntranceHall = userAddress.EntranceHall,
              Floor = userAddress.Floor,
              Apartment = userAddress.Apartment
            };
        }
    }
}
