﻿using KonigDev.Lazurit.Hub.Model.DTO.ShowRooms;
using KonigDev.Lazurit.Model.Entities;

namespace KonigDev.Lazurit.Hub.Model.Mappings.ShowRooms
{
    public static class ShowRoomConvertor
    {
        public static DtoShowRoomResult MapToDto(this Showroom showroom)
        {
            return new DtoShowRoomResult
            {
                Id = showroom.Id,
                Title = showroom.Title
            };
        }
    }
}
