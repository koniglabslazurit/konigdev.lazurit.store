﻿using KonigDev.Lazurit.Hub.Model.DTO.Users;

namespace KonigDev.Lazurit.Hub.Model.Mappings.City
{
    public static class CityConvertor
    {
        public static DTOCityResult MapToDTOCityResult(this Lazurit.Model.Entities.City city)
        {
            return new DTOCityResult
            {
                RegionId = city.RegionId,
                Id = city.Id,
                Title = city.Title
            };
        }
    }
}
