select p.SyncCode1C, p.Series as Product_Series, p.RoomType as Product_Room,
p.FrameColor as 'Цвет корпуса продукта', cv.FrameColor as 'Цвет корпуса варианта', 
p.Facing as 'Отделка продукта', cv.Facing as 'Отделка варианта',
p.FacingColor as 'Цвет отделки продукта', cv.FacingColor as 'Цвет отделки варианта',
s.SyncCode1C as Series, r.SyncCode1C as Room
from IntegrationsProducts as p
left join IntegrationsProductToIntegrationCollectionsVariants as variantsproducts on variantsproducts.IntegrationProductId = p.SyncCode1C
left join IntegrationsCollectionsVariants as cv on cv.Id = variantsproducts.IntegrationCollectionVariantId
left join IntegrationsCollections as c on c.Id = cv.IntegrationCollectionId
left join IntegrationsSeries as s on s.Id = c.IntegrationSeriesId
left join IntegrationsRooms as r on r.Id = c.IntegrationRoomId
where s.SyncCode1C = N'Аталия' or p.Series = N'Аталия'
order by p.Series, p.RoomType

