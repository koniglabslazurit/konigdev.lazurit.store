﻿using KonigDev.Lazurit.Core.Services;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Integration.Worker.Logging
{
    public class LoggingService : ILoggingService
    {
        private readonly Logger _logger;

        public LoggingService()
        {
            _logger = LogManager.GetCurrentClassLogger();
        }

        public void Error(Exception exception)
        {
            _logger.Error(exception);
        }

        public void Error(string message)
        {
            _logger.Error(message);
        }

        public void Error(string message, object parameters)
        {
            _logger.Trace(parameters);
            _logger.Error(message);
        }

        public void ErrorWithIncomeParameter(Exception exception, object parameters)
        {
            _logger.Trace(parameters);
            _logger.Error(exception);
        }

        public void Fatal(Exception exception)
        {
            _logger.Fatal(exception);
        }

        public void Info(Exception exception)
        {
            _logger.Info(exception);
        }

        public void InfoMessage(string message)
        {
            _logger.Info(message);
        }

        public void Trace(Exception exception)
        {
            _logger.Trace(exception);
        }

        public void Warn(Exception exception)
        {
            _logger.Warn(exception);
        }
    }
}
