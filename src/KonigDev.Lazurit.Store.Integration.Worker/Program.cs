﻿using KonigDev.Lazurit.Core.Services;
using KonigDev.Lazurit.Hub.GrainResolver.NinjectModules;
using KonigDev.Lazurit.Store.Integration.Interfaces;
using KonigDev.Lazurit.Store.Integration.NinjectModules;
using KonigDev.Lazurit.Store.Integration.Worker.Logging;
using Ninject;
using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace KonigDev.Lazurit.Store.Integration.Worker
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

                IKernel kernel = NinjectKernelResolver.CreateKernel();

                kernel.Bind<ILoggingService>().To<LoggingService>();

                kernel.Load<HubBaseModule>();
                kernel.Load<HubUsersHandlersModule>();
                kernel.Load<HubCollectionsHandlersModule>();
                kernel.Load<HubFiltersHandlerModule>();
                kernel.Load<HubProductsHandlerModule>();
                kernel.Load<HubArticlesHandlerModule>();
                kernel.Load<HubFurnitureTypesHandlerModule>();
                kernel.Load<HubIntegrationHandlerModule>();
                kernel.Load<HubPropertiesHandlersModule>();
                kernel.Load<HubShowRoomHandlersModule>();
                kernel.Load<HubCityHandlerModule>();
                kernel.Load<HubPriceZoneHandlerModule>();
                kernel.Load<HubRegionHandlerModule>();
                kernel.Load<HubRoomHandlerModule>();
                kernel.Load<HubSeriesHandlerModule>();

                var logger = kernel.Get<ILoggingService>();
                var path = ConfigurationManager.AppSettings["importpath"];

                if (string.IsNullOrEmpty(path))
                {
                    logger.Error("IMPORT_PRODUCTS: Missing settings base path to files with import data");
                    return;
                }

                ImportProducts(path);
                ImportZones(path);

                AppDomain.CurrentDomain.UnhandledException -= CurrentDomain_UnhandledException;
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {           
            IKernel kernel = NinjectKernelResolver.CreateKernel();
            var logger = kernel.Get<ILoggingService>();
            try
            {
                logger.Error((Exception)e.ExceptionObject);
            }
            catch (Exception ex)
            {
                logger.Error(e.ExceptionObject.ToString());
            }
            Environment.Exit(0);
        }

        private static void ImportProducts(string path)
        {
            IKernel kernel = NinjectKernelResolver.CreateKernel();
            var logger = kernel.Get<ILoggingService>();

            var productFile = "";

            var productsFiles = Directory.GetFiles(path, "*.*", SearchOption.TopDirectoryOnly)
                .Where(s => Path.GetFileName(s).EndsWith(".xml"))
                .Where(s => Path.GetFileName(s).StartsWith("product"));

            if (productsFiles.Count() == 0)
                logger.Error("IMPORT_PRODUCTS: Missing files with import data for products");


            if (productsFiles.Count() > 1)
            {
                logger.Error("IMPORT_PRODUCTS: Found more files with import data for products");
                logger.Error($"IMPORT_PRODUCTS: Select first file to import {productFile}");
            }
            productFile = productsFiles.FirstOrDefault();

            if (string.IsNullOrEmpty(productFile))
                logger.Error("IMPORT_PRODUCTS: Product file path is empty");

            var importService = kernel.Get<IImportService>();

            if (!File.Exists(productFile))
                logger.Error($"IMPORT_PRODUCTS: Product file path is missing {productFile}");
            else
                importService.ImportProducts(productFile);

            logger.InfoMessage($"IMPORT_PRODUCTS: delete products files");
            foreach (var item in productsFiles)
            {
                if (File.Exists(item))
                    File.Delete(item);
            }
        }

        private static void ImportZones(string path)
        {
            IKernel kernel = NinjectKernelResolver.CreateKernel();
            var logger = kernel.Get<ILoggingService>();

            var zoneFile = "";
            var zoneFiles = Directory.GetFiles(path, "*.*", SearchOption.TopDirectoryOnly)
                .Where(s => Path.GetFileName(s).EndsWith(".xml"))
                .Where(s => Path.GetFileName(s).StartsWith("zone"));

            if (zoneFiles.Count() == 0)
                logger.Error("IMPORT_ZONES: Missing files with import data for zones");

            if (zoneFiles.Count() > 1)
            {
                logger.Error("IMPORT_ZONES: Found more files with import data for zones");
                logger.Error($"IMPORT_ZONES: Select first file to import zones {zoneFile}");
            }

            zoneFile = zoneFiles.FirstOrDefault();

            if (string.IsNullOrEmpty(zoneFile))
                logger.Error("IMPORT_ZONES: Zones file path is empty");

            var importService = kernel.Get<IImportService>();

            if (!File.Exists(zoneFile))
                logger.Error($"IMPORT_ZONES: Zones file path is missing {zoneFile}");
            else
                importService.ImportPricesZones(zoneFile);

            logger.InfoMessage($"IMPORT_ZONES: delete products files");
            foreach (var item in zoneFiles)
            {
                if (File.Exists(item))
                    File.Delete(item);
            }
        }
    }

    public static class NinjectKernelResolver
    {
        private static IKernel _kernel;

        public static IKernel CreateKernel()
        {
            if (_kernel == null)
                _kernel = new StandardKernel(new IntegrationModule());
            return _kernel;
        }

    }
}