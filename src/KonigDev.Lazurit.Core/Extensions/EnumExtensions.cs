﻿using System;
using System.ComponentModel;

namespace KonigDev.Lazurit.Core.Extensions
{
    public static class EnumExtensions
    {
        /// <summary>
        /// Get description of enum value
        /// </summary>
        /// <param name="value">Enum value</param>
        /// <returns>Description attribute value or string view of initial value if no description provided</returns>
        public static string GetDescription(this Enum value)
        {
            var field = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])field
                .GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            else
            {
                return value.ToString();
            }
        }
    }
}
