﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace KonigDev.Lazurit.Core.Extensions
{
    public static class StringExtensions
    {
        private static readonly Dictionary<char, string> Letters = new Dictionary<char, string>
        {
            {'а', "a"},  {'б', "b"},  {'в', "v"},   {'г', "g"}, {'д', "d"}, {'е', "e"}, {'ё', "yo"}, {'ж', "zh"},
            {'з', "z"},  {'и', "i"},  {'й', "j"},   {'к', "k"}, {'л', "l"}, {'м', "m"}, {'н', "n"},  {'о', "o"},
            {'п', "p"},  {'р', "r"},  {'с', "s"},   {'т', "t"}, {'у', "u"}, {'ф', "f"}, {'х', "h"},  {'ц', "c"},
            {'ч', "ch"}, {'ш', "sh"}, {'щ', "sch"}, {'ъ', ""},  {'ы', "y"}, {'ь', ""},  {'э', "e"},  {'ю', "yu"},
            {'я', "ya"}, {' ', "-"},  {'-', "-"},   {'+', "-"}, {'=', "-"}, {'_', "_"}, {'0', "0"},  {'1', "1"},
            {'2', "2"},  {'3', "3"},  {'4', "4"},   {'5', "5"}, {'6', "6"}, {'7', "7"}, {'8', "8"},  {'9', "9"},
            {'a', "a"},  {'b', "b"},  {'c', "c"},   {'d', "d"}, {'e', "e"}, {'f', "f"}, {'g', "g"},  {'h', "h"},
            {'i', "i"},  {'j', "j"},  {'k', "k"},   {'l', "l"}, {'m', "m"}, {'n', "n"}, {'o', "o"},  {'p', "p"},
            {'q', "q"},  {'r', "r"},  {'s', "s"},   {'t', "t"}, {'u', "u"}, {'v', "v"}, {'w', "w"},  {'x', "x"},
            {'y', "y"},  {'z', "z"}
        };

        public static string Transliterate(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return string.Empty;
            var result = new StringBuilder();
            value.ToLower().ToCharArray().Where(item => Letters.ContainsKey(item)).ToList().ForEach(item => result.Append(Letters[item]));
            if (result.Length == 0)
            {
                result.Append(Guid.NewGuid());
            }
            return result.ToString();
        }

        /// <summary>
        /// Get description of enum value
        /// </summary>
        /// <typeparam name="TEnum">Enumerated type that has this enum value</typeparam>
        /// <param name="value">String view of enum value</param>
        /// <returns>Description attribute value or initial value if no description provided</returns>
        public static string GetEnumValueDescription<TEnum>(this string value) where TEnum : struct, IConvertible
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("TEnum must be an enumerated type");
            }

            var field = typeof(TEnum).GetField(value);
            var attributes = (DescriptionAttribute[])field
                .GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            else
            {
                return value;
            }
        }
    }
}
