﻿using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using KonigDev.Lazurit.Core.Services.ImagesServices;
using NLog;

namespace KonigDev.Lazurit.Core.Services.StorageServices
{
    public class AzureStorageService : IStorageService
    {
        private readonly string _fileStoragePath;
        private readonly IAzureService _azureService;
        private readonly Logger _logger;

        public AzureStorageService(string fileStoragePath, IAzureService azureService)
        {
            _fileStoragePath = fileStoragePath;
            _azureService = azureService;
            _logger = LogManager.GetCurrentClassLogger();
        }

        public void CreateStorageStructure()
        {
            _logger.Info("Start create if not exists azure storage structure");
            foreach (var item in GetSequence())
            {
                var res = _azureService.GetContainer(string.Concat(_fileStoragePath, "-", item)).CreateIfNotExists();
                _logger.Info($"Crete container {string.Concat(_fileStoragePath, $"-", item)} result - {res}");
            }
        }

        public bool ExistsFile(string fileName)
        {
            fileName = fileName.ToLower();
            return _azureService.GetCloudBlob(GetContainerName(fileName), fileName).Exists();
        }

        public void SaveFile(string fileName, byte[] fileBytes)
        {
            fileName = fileName.ToLower();
            using (var memoStream = new MemoryStream(fileBytes))
            {
                var image = Image.FromStream(memoStream);
                image = ImageService.ResizingImage((Bitmap)image, image.Width, image.Height);
                var bytes = ImageService.ImageToStream(image).ToArray();
                var blob = _azureService.GetCloudBlob(GetContainerName(fileName), fileName);
                blob.UploadFromByteArray(bytes, 0, bytes.Length);
                _logger.Info($"Save file: {fileName} (AzureStorage)");
            }
        }

        public byte[] GetFile(string fileName)
        {
            fileName = fileName.ToLower();
            _logger.Info("Get file from blobstorage: " + fileName);
            var blob = _azureService.GetCloudBlob(GetContainerName(fileName), fileName);
            if (!blob.Exists())
            {
                _logger.Info($"Get file from blobstorage: {fileName}, file missed");
                return null;
            }
            using (var ms = new MemoryStream())
            {
                blob.DownloadToStream(ms);
                return ms.ToArray();
            }
        }

        public void DeleteFile(string fileName)
        {
            fileName = fileName.ToLower();
            _azureService.GetCloudBlob(GetContainerName(fileName), fileName).DeleteIfExists();
            _logger.Info($"Delete file: {fileName} (AzureStorage)");
        }

        /// <summary>
        /// Получение имени конейнера
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private string GetContainerName(string fileName)
        {
            return string.Concat(_fileStoragePath.ToLower(), "-", fileName.ToLower()[0]);
        }

        private static IEnumerable<string> GetSequence()
        {
            for (var c = 'a'; c <= 'z'; c++)
                yield return c.ToString(CultureInfo.InvariantCulture);

            for (var i = 0; i < 10; i++)
                yield return i.ToString(CultureInfo.InvariantCulture);
        }
    }
}