﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace KonigDev.Lazurit.Core.Services.StorageServices
{
    public interface IAzureService
    {
        CloudStorageAccount GetAccount();

        CloudBlobContainer GetContainer(string name);

        CloudBlockBlob GetCloudBlob(string container, string name);
    }
}
