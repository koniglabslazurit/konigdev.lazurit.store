using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace KonigDev.Lazurit.Core.Services.StorageServices
{
    public class AzureService : IAzureService
    {
        private readonly string _storageConnectionString;

        public AzureService(string storageConnectionString)
        {
            _storageConnectionString = storageConnectionString;
        }

        public CloudStorageAccount GetAccount()
        {
            return CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting(_storageConnectionString));
        }

        public CloudBlobContainer GetContainer(string name)
        {
            return GetAccount().CreateCloudBlobClient().GetContainerReference(name);
        }

        public CloudBlockBlob GetCloudBlob(string container, string name)
        {
            return GetContainer(container).GetBlockBlobReference(name.ToUpper());
        }
    }
}