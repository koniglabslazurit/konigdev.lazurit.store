﻿namespace KonigDev.Lazurit.Core.Services.StorageServices
{
    public interface IStorageService
    {
        /// <summary>
        /// Создание структуры файлового хранилища
        /// </summary>
        void CreateStorageStructure();

        /// <summary>
        /// Проверка на наличие файла
        /// </summary>
        /// <param name="fileName">Имя файла</param>
        /// <returns></returns>
        bool ExistsFile(string fileName);

        /// <summary>
        /// Сохранение файла
        /// </summary>
        /// <param name="fileName">Имя файла</param>
        /// <param name="fileBytes">Массив байтов</param>
        void SaveFile(string fileName, byte[] fileBytes);

        /// <summary>
        /// Получение файла по имени
        /// </summary>
        /// <param name="fileName">Имя файла</param>
        byte[] GetFile(string fileName);

        /// <summary>
        /// Удаление файла
        /// </summary>
        /// <param name="fileName">Имя файла</param>
        void DeleteFile(string fileName);
    }
}
