﻿using System;

namespace KonigDev.Lazurit.Core.Services
{
    public interface ILoggingService
    {
        void ErrorWithIncomeParameter(Exception exception, object parameters);
        void Error(string message);
        void Error(string message,object parameters);
        void Error(Exception exception);
        void Fatal(Exception exception);
        void Info(Exception exception);
        void InfoMessage(string message);
        void Trace(Exception exception);
        void Warn(Exception exception);
    }
}
