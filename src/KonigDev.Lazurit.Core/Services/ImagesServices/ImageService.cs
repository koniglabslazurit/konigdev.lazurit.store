﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace KonigDev.Lazurit.Core.Services.ImagesServices
{
    internal static class ImageService
    {
        /// <summary>
        /// Получение потока из изображения
        /// </summary>
        /// <param name="image">Изображение</param>
        /// <returns></returns>
        public static MemoryStream ImageToStream(Image image)
        {
            var stream = new MemoryStream();
            image.Save(stream, ImageFormat.Jpeg);
            stream.Position = 0;
            return stream;
        }

        /// <summary>
        /// Ресайзинг изображения по стороне
        /// </summary>
        /// <param name="image">Bitmap изображение.</param>
        /// <param name="width">Ширина</param>
        /// <param name="height">Высота.</param>
        /// <param name="quality">Значение параметра качества.</param>
        public static Image ResizingImage(Bitmap image, int width, int height, int quality = 70)
        {
            var originalWidth = image.Width;
            var originalHeight = image.Height;

            var ratioX = width / (float)originalWidth;
            var ratioY = height / (float)originalHeight;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(originalWidth * ratio);
            var newHeight = (int)(originalHeight * ratio);

            var newImage = new Bitmap(newWidth, newHeight, PixelFormat.Format24bppRgb);

            using (var graphics = Graphics.FromImage(newImage))
            {
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);
            }

            newImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            /* Сохранение с параметрами кодека и качеством */
            var imageCodecInfo = GetEncoder(ImageFormat.Jpeg);
            var encoder = Encoder.Quality;
            var encoderParameters = new EncoderParameters(1);
            var encoderParameter = new EncoderParameter(encoder, quality);
            encoderParameters.Param[0] = encoderParameter;

            /* Сохраняем полученное изображение */
            var ms = new MemoryStream();
            newImage.Save(ms, imageCodecInfo, encoderParameters);

            image.Dispose();

            return Image.FromStream(ms);
        }
        
        // Получение кодека изображения
        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            return ImageCodecInfo.GetImageDecoders().SingleOrDefault(c => c.FormatID == format.Guid);
        }
    }
}