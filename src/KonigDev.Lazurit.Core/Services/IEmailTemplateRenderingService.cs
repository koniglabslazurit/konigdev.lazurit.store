﻿using System.Collections.Generic;

namespace KonigDev.Lazurit.Core.Services
{
    public interface IEmailTemplateRenderingService
    {
        /// <summary>
        /// Получает тело сообщения для любого вида продовца.
        /// </summary>
        /// <param name="userName">логин продовца</param>
        /// <param name="regionPasswords">словарь, состоящий из РО и пароля от РО</param>
        /// <returns>тело сообщения для Email письма</returns>
        string GetBodyForSallers(string userName, Dictionary<string, string> regionPasswords);
        /// <summary>
        /// Получает телов сообщения для пользователя
        /// </summary>
        /// <param name="userName">логин</param>
        /// <param name="password">пароль</param>
        /// <returns></returns>
        string GetBodyClients(string userName, string password);
    }
}
