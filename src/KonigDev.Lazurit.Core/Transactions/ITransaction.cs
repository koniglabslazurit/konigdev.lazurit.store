﻿using System;
using System.Transactions;
using IsolationLevel = System.Data.IsolationLevel;

namespace KonigDev.Lazurit.Core.Transactions
{
    public interface ITransaction : IDisposable
    {
        IsolationLevel IsolationLevel { get; }

        TransactionInformation TransactionInformation { get; }
        /// <summary>
        /// Возвращает копию транзакции
        /// </summary>
        /// <returns></returns>
        ITransaction Clone();

        void Rollback();
        /// <summary>
        /// Откат транзакции
        /// </summary>
        /// <param name="e">Ошибка, содержащая причину отката</param>
        void Rollback(Exception e);
    }
}
