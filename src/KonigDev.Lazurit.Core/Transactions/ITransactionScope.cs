﻿using System;

namespace KonigDev.Lazurit.Core.Transactions
{
    public interface ITransactionScope
         : IDisposable
    {
        void Complete();
    }
}
