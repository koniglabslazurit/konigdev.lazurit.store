﻿using System;
using System.Transactions;

namespace KonigDev.Lazurit.Core.Transactions
{
    public interface ITransactionFactory
    {
        /// <summary>
        /// Создаёт новый экземпляр класса <see cref="ITransactionScope"/>
        /// </summary>
        /// <returns></returns>
        ITransactionScope CreateScope();

        /// <summary>
        /// Создаёт новый экземпляр класса <see cref="ITransactionScope"/>
        /// </summary>
        /// <param name="scopeOption">параметры для работы с async await</param>
        /// <returns></returns>
        ITransactionScope CreateScope(TransactionScopeAsyncFlowOption scopeOption);

        /// <summary>
        /// Создаёт новый экземпляр класса <see cref="ITransactionScope"/>
        /// </summary>
        /// <param name="scopeOption">Экземпляр <see cref="TransactionScopeOption"/> перечисления, необходимый для описания настроек транзакции</param>
        /// <returns></returns>
        ITransactionScope CreateScope(TransactionScopeOption scopeOption);


        /// <summary>
        /// Создаёт новый экземпляр класса <see cref="ITransactionScope"/> с определённым таймаутом и параметрами
        /// </summary>
        /// <param name="scopeOption">Экземпляр <see cref="TransactionScopeOption"/> перечисления, необходимый для описания настроек транзакции</param>
        /// <param name="scopeTimeout">таймаут, после которого транзакция заканчивается (abort)</param>
        /// <returns></returns>
        ITransactionScope CreateScope(TransactionScopeOption scopeOption, TimeSpan scopeTimeout);

        /// <summary>
        /// Создаёт новый экземпляр класса <see cref="ITransactionScope"/> с определёнными параметрами
        /// </summary>
        /// <param name="scopeOption">Экземпляр <see cref="TransactionScopeOption"/> перечисления, необходимый для описания настроек транзакции</param>
        /// <param name="transactionOptions">Структура <see cref="TransactionOptions"/>, описывающая опции транзакции, используемые если новая транзакция была создана.
        /// Если транзакция уже используется,то значение таймаута в этом параметре применяется к transaction scope. Если это время таймаута истекает перед тем как scope 
        /// очищен (disposed), то транзакция обрывается (aborted)</param>
        /// <returns></returns>
        ITransactionScope CreateScope(TransactionScopeOption scopeOption, TransactionOptions transactionOptions);

        /// <summary>
        /// Создаёт новый экземпляр класса <see cref="ITransactionScope"/> с определённым scope, COM+ параметрами и опциями транзакции
        /// </summary>
        /// <param name="scopeOption">Экземпляр <see cref="TransactionScopeOption"/> перечисления, необходимый для описания настроек транзакции</param>
        /// <param name="transactionOptions">Структура <see cref="TransactionOptions"/>, описывающая опции транзакции, используемые если новая транзакция была создана.
        /// Если транзакция уже используется,то значение таймаута в этом параметре применяется к transaction scope. Если это время таймаута истекает перед тем как scope 
        /// очищен (disposed), то транзакция обрывается (aborted)</param>
        /// <param name="interopOption">Экземпляр перечисления <see cref="EnterpriseServicesInteropOption"/>,описывающий как текущая транзакция будет взаимодействовать
        /// с COM+ транзакциями.</param>
        /// <returns></returns>
        ITransactionScope CreateScope(TransactionScopeOption scopeOption, TransactionOptions transactionOptions, EnterpriseServicesInteropOption interopOption);
    }
}
