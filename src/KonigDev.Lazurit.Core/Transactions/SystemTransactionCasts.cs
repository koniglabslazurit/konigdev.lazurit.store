﻿using System.Diagnostics;
using System.Transactions;

namespace KonigDev.Lazurit.Core.Transactions
{
    /// <summary>
    /// Класс, кастующий системную транзакцию к нашему интерфейсу <see cref="ITransactionScope"/>
    /// </summary>
    internal static class SystemTransactionCasts
    {
        [DebuggerStepThrough]
        public static ITransactionScope Cast(this TransactionScope scope)
        {
            return new TransactionScopeAdapter(scope);
        }

        private class TransactionScopeAdapter :Adapter<TransactionScope>, ITransactionScope
        {
            public TransactionScopeAdapter(TransactionScope scope)
                : base(scope)
            {}

            public void Complete()
            {
                WrappedObject.Complete();
            }

            public void Dispose()
            {
                WrappedObject.Dispose();
            }
        }

        private class Adapter<T>
        {
            protected readonly T WrappedObject;

            [DebuggerStepThrough]
            protected Adapter(T wrappedObject)
            {
                WrappedObject = wrappedObject;
            }

            [DebuggerStepThrough]
            public override bool Equals(object obj)
            {
                return WrappedObject.Equals(obj);
            }

            [DebuggerStepThrough]
            public override int GetHashCode()
            {
                return WrappedObject.GetHashCode();
            }

            [DebuggerStepThrough]
            public override string ToString()
            {
                return string.Concat("Обёртка для: ", WrappedObject.ToString());
            }
        }
    }
}
