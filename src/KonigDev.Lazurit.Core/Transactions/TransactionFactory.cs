﻿using System;
using System.Transactions;

namespace KonigDev.Lazurit.Core.Transactions
{
    public class TransactionFactory : ITransactionFactory
    {
        public ITransactionScope CreateScope()
        {
            return new TransactionScope().Cast();
        }

        public ITransactionScope CreateScope(TransactionScopeAsyncFlowOption scopeOption)
        {
            return new TransactionScope(scopeOption).Cast();
        }

        public ITransactionScope CreateScope(TransactionScopeOption scopeOption, TransactionOptions transactionOptions)
        {
            return new TransactionScope(scopeOption, transactionOptions).Cast();
        }

        public ITransactionScope CreateScope(TransactionScopeOption scopeOption, TransactionOptions transactionOptions, EnterpriseServicesInteropOption interopOption)
        {
            return new TransactionScope(scopeOption, transactionOptions, interopOption).Cast();
        }

        ITransactionScope ITransactionFactory.CreateScope(TransactionScopeOption scopeOption, TimeSpan scopeTimeout)
        {
            return new TransactionScope(scopeOption, scopeTimeout).Cast();
        }

        ITransactionScope ITransactionFactory.CreateScope(TransactionScopeOption scopeOption)
        {
            return new TransactionScope(scopeOption).Cast();
        }
    }
}
