﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Model.Entities
{
   public class LayawayItemProductInCollection : IBaseEntity
    {
        public Guid Id { set; get; }

        public Guid ProductId { set; get; }
        public Guid LayawayItemCollectionId { set; get; }

        public bool IsAssemblyRequired { get; set; }
        public int Quantity { set; get; }

        public Product Product { set; get; }
        public LayawayItemCollection LayawayItemCollection { set; get; }
    }
}
