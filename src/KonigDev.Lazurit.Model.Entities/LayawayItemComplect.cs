﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;

namespace KonigDev.Lazurit.Model.Entities
{
   public class LayawayItemComplect : IBaseEntity
    {
        public Guid Id { set; get; }

        public Guid CollectionVarinatId { set; get; }
        public Guid LayawayId { set; get; }

        public bool IsAssemblyRequired { get; set; }
        public int Quantity { set; get; }

        public Layaway Layaway { set; get; }
        public CollectionVariant CollectionVariant { set; get; }
    }
}
