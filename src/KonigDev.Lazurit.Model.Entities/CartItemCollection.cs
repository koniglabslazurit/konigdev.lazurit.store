﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Model.Entities
{
    public class CartItemCollection : IBaseEntity
    {
        public CartItemCollection()
        {
            Products = new HashSet<CartItemProductInCollection>();
        }
        public Guid Id { get; set; }
        public int Quantity { set; get; }

        public Guid CollectionVariantId { set; get; }
        public Guid CartId { set; get; }

        public virtual CollectionVariant CollectionVariant { set; get; }
        public virtual Cart Cart { set; get; }
        public virtual ICollection<CartItemProductInCollection> Products { set; get; }
    }
}
