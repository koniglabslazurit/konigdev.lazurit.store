﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Model.Entities
{
    public class UserProfile : IBaseEntity
    {
        public UserProfile()
        {
            Regions = new HashSet<Region>();
        }
        public Guid Id { get; set; }
        public Guid LinkId { get; set; }
        public string Title { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        /// <summary>
        /// Код синхронизации с 1С
        /// </summary>                
        public string SyncCode1C { get; set; }

        public Guid? CityId { get; set; }

        public virtual City City { get; set; }
        /// <summary>
        /// Регионы
        /// </summary>
        public virtual ICollection<Region> Regions { get; set; }
        public virtual ICollection<UserPhone> UserPhones { get; set; }
        public virtual ICollection<UserAddress> UserAddresses { get; set; }
    }
}