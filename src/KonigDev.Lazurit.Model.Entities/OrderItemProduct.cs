﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Model.Entities
{
   public class OrderItemProduct : IBaseEntity
    {
        public Guid Id { set; get; }

        public decimal Price { get; set; }
        public byte Discount { get; set; }

        public int Quantity { set; get; }
        public bool IsAssemblyRequired { get; set; }

        public Guid ProductId { set; get; }
        public Guid OrderId { set; get; }

        public virtual Product Product { set; get; }
        public virtual Order Order { set; get; }
    }
}
