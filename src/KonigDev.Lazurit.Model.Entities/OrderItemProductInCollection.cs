﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Model.Entities
{
   public class OrderItemProductInCollection : IBaseEntity
    {
        public Guid Id { get; set; }

        public decimal Price { get; set; }
        public byte Discount { get; set; }
        
        public int Quantity { set; get; }
        public bool IsAssemblyRequired { get; set; }

        public Guid ProductId { set; get; }
        public Guid OrderItemCollectionId { get; set; }

        public virtual Product Product { set; get; }
        public virtual OrderItemCollection OrderItemCollection { get; set; }
    }
}
