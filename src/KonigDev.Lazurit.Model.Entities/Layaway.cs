﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Model.Entities
{
   public class Layaway : IBaseEntity
    {
        public Layaway()
        {
            Products = new HashSet<LayawayItemProduct>();
            Complects = new HashSet<LayawayItemComplect>();
            Collections = new HashSet<LayawayItemCollection>();
        }
        public Guid Id { get; set; }
       
        public Guid? UserId { set; get; }
        /// <summary>
        /// признак того, отложена ли целая корзина
        /// </summary>
        public bool IsWholeCart { set; get; }

        public DateTime CreationTime { get; set; }

        public UserProfile User { set; get; }

        public virtual ICollection<LayawayItemProduct> Products { set; get; }
        public virtual ICollection<LayawayItemComplect> Complects { set; get; }
        public virtual ICollection<LayawayItemCollection> Collections { set; get; }
      
    }
}
