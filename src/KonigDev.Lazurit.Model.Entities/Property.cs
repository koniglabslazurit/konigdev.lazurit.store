﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Model.Entities
{
    /// <summary>
    /// Свойства для продуктов
    /// </summary>
    public class Property : IBaseEntity, IOneCEntity
    {
        public Property()
        {
            Products = new HashSet<Product>();
        }

        public Guid Id { get; set; }
        public string SyncCode1C { get; set; }
        public string Title { get; set; }
        public string TitleInPlural { get; set; }
        public string Alias { get; set; }
        public string AliasInPlural { get; set; }
        public string Value { get; set; }
        public bool IsRequired { get; set; }
        public bool IsMultiValue { get; set; }
        public int SortNumber { get; set; }

        /// <summary>
        /// Группировка для свойств, которые могут являться списочными.
        /// </summary>
        public Guid? GroupeId { get; set; }

        public virtual ICollection<Product> Products { get; set; }        
    }
}