﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;

namespace KonigDev.Lazurit.Model.Entities
{
    public class FileContent : IBaseEntity
    {
        public Guid Id { get; set; }
        public Guid ObjectId { get; set; }
        public string FileExtension { get; set; }
        public string FileName { get; set; }
        public DateTime DateCreating { get; set; }
        public long Size { get; set; }

        public Guid TypeId { get; set; }
        public virtual FileContentType Type { get; set; }

    }
}
