﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Model.Entities
{
    public class Cart : IBaseEntity
    {
        public Cart()
        {
            Products = new HashSet<CartItemProduct>();
            Complects = new HashSet<CartItemComplect>();
            Collections = new HashSet<CartItemCollection>();
        }
        public Guid Id { get; set; }
        public bool IsDeliveryRequired { get; set; }

        public Guid? UserId { set; get; }

        public virtual UserProfile User { set; get; }

        public virtual ICollection<CartItemProduct> Products { set; get; }
        public virtual ICollection<CartItemComplect> Complects { set; get; }
        public virtual ICollection<CartItemCollection> Collections { get; set; }        
    }
}
