﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Model.Entities
{
    public class UserAddress : IBaseEntity
    {
        public Guid Id { get; set; }
        public Guid UserProfileId { get; set; }
        public string KladrCode { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string Address { get; set; }
        /// <summary>
        /// корпус
        /// </summary>
        public string Block { get; set; }
        /// <summary>
        /// строение
        /// </summary>
        public string Building { get; set; }
        /// <summary>
        /// дом
        /// </summary>
        public string House { get; set; }
        /// <summary>
        /// подъезд
        /// </summary>
        public string EntranceHall { get; set; }
        /// <summary>
        /// этаж
        /// </summary>
        public string Floor { get; set; }
        /// <summary>
        /// квартира
        /// </summary>
        public string Apartment { get; set; }

        public bool IsDefault { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}