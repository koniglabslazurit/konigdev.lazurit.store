﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Model.Entities
{
    public class Product : IBaseEntity, IOneCEntity
    {
        public Product()
        {
            CollectionsVariants = new HashSet<CollectionVariant>();
            Properties = new HashSet<Property>();
            TargetAudiences = new HashSet<TargetAudience>();
            Styles = new HashSet<Style>();
        }

        public Guid Id { get; set; }
        public Guid CollectionId { get; set; }
        public string SyncCode1C { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public bool IsAssemblyRequired { get; set; }
        public string SyncCode1CСonformity { get; set; }

        /// <summary>
        /// Признак разобранного вида
        /// </summary>
        public bool IsDisassemblyState { get; set; }

        /// <summary>
        /// Дефлотный товар в рамках маркетингового артикула
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// Признак единичного товара как комплект
        /// </summary>
        public bool IsComplect { get; set; }

        /// <summary>
        /// Маркетинговое описание
        /// </summary>
        public string Descr { get; set; }

        /// <summary>
        /// Допустимая Нагрузка На Кровать
        /// </summary>
        public string PermissibleLoad { get; set; }

        /// <summary>
        /// РазмерСпальногоМестаДлина
        /// </summary>
        public string SleepingAreaLength { get; set; }

        /// <summary>
        /// РазмерСпальногоМестаШирина
        /// </summary>
        public string SleepingAreaWidth { get; set; }

        /// <summary>
        /// Артикул, базовый НЕ-маркетинговый
        /// </summary>
        public string VendorCode { get; set; }

        /// <summary>
        /// КолУпаковок
        /// </summary>
        public string Packing { get; set; }

        /// <summary>
        /// Форма артикула
        /// </summary>
        public string FurnitureForm { get; set; }
        /// <summary>
        /// Гарантия
        /// </summary>
        public string Warranty { get; set; }

        /// <summary>
        /// Размер комнаты
        /// </summary>
        public string RoomSize { get; set; }

        /// <summary>
        /// Маркетинговый Вес
        /// </summary>
        public string MarketerVendorCode { get; set; }

        /// <summary>
        /// Размер спального места
        /// </summary>
        public short SleepingAreaAmount { get; set; }

        /// <summary>
        /// Маркетинговый вес продукта
        /// </summary>
        public string Range { get; set; }

        public Guid ArticleId { get; set; }

        public Guid? FacingColorId { get; set; }

        public Guid FrameColorId { get; set; }

        public Guid FacingId { get; set; }

        public Guid ProductPartMarkerId { get; set; }

        /// <summary>
        /// Артикул
        /// </summary>
        public virtual Article Article { get; set; }

        /// <summary>
        /// Цвет отделки, ЦветФасадаИзделия
        /// </summary>
        public virtual FacingColor FacingColor { get; set; }

        /// <summary>
        /// Цвет корпуса, ЦветКорпуса
        /// </summary>
        public virtual FrameColor FrameColor { get; set; }

        /// <summary>
        /// Отделка, ВариантОтделкиИзделия
        /// </summary>
        public virtual Facing Facing { get; set; }
        public virtual ProductPartMarker ProductPartMarker { get; set; }
        public virtual Collection Collection { get; set; }

        public virtual ICollection<CollectionVariant> CollectionsVariants { get; set; }
        public virtual ICollection<Property> Properties { get; set; }
        public virtual ICollection<Price> Prices { get; set; }
        public virtual ICollection<FavoriteItemProduct> FavoriteItemProducts { get; set; }
        public virtual ICollection<TargetAudience> TargetAudiences { get; set; }
        public virtual ICollection<Style> Styles { get; set; }        
    }
}
