﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Model.Entities
{
    public class Showroom : IBaseEntity
    {
         
        public Guid Id { get; set; }
        public string Title { get; set; }
        /// <summary>
        /// Код по кладру
        /// </summary>
        public string CodeAddress { get; set; }
        /// <summary>
        /// Код города по кладру
        /// </summary>
        public string CodeCity { get; set; }
        /// <summary>
        /// Код синхронизации с 1С
        /// </summary>                
        public string SyncCode1C { get; set; }

        public Guid RegionId { get; set; }
        public Guid? CityId { get; set; }

        public virtual Region Region { get; set; }
        public virtual City City { get; set; }

        public virtual ICollection<UserProfile> Users { get; set; }      
    }
}
