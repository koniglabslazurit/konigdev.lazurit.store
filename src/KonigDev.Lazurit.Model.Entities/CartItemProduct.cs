﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;

namespace KonigDev.Lazurit.Model.Entities
{
    public class CartItemProduct : IBaseEntity
    {
        public Guid Id { set; get; }
        public int Quantity { set; get; }
        public bool IsAssemblyRequired { get; set; }

        public Guid ProductId { set; get; }
        public Guid CartId { set; get; }

        public virtual Product Product { set; get; }
        public virtual Cart Cart { set; get; }
    }
}
