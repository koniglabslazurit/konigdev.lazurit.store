﻿using System;
using KonigDev.Lazurit.Model.Entities.Interfaces;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Model.Entities
{
    public class Settings : IBaseEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Value { get; set; }
    }
}
