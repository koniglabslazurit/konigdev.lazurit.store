﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;

namespace KonigDev.Lazurit.Model.Entities
{
    public class CartItemProductInCollection : IBaseEntity
    {
        public Guid Id { get; set; }
        public int Quantity { set; get; }
        public bool IsAssemblyRequired { get; set; }

        public Guid ProductId { set; get; }
        public Guid CartItemCollectionId { get; set; }

        public virtual Product Product { set; get; }
        public virtual CartItemCollection CartItemCollection { get; set; }
    }
}
