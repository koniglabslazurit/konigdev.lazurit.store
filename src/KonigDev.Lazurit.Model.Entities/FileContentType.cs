﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;

namespace KonigDev.Lazurit.Model.Entities
{
    public class FileContentType:IBaseEntity, IEnumEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string TechName { get; set; }
        public short SortNumber { get; set; }
    }
}