﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Model.Entities
{
    public class FacingColor : IBaseEntity, IOneCEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string SyncCode1C { get; set; }
        public bool IsUniversal { get; set; }

        public virtual ICollection<CollectionVariant> CollectionsVariants { get; set; }
        public virtual ICollection<Product> Products { get; set; }

    }
}
