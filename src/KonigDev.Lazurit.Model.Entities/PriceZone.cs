﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Model.Entities
{
    public class PriceZone : IBaseEntity
    {
        public PriceZone()
        {
            Prices = new HashSet<Price>();
        }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string SyncCode1C { get; set; }
        public byte Number { get; set; }

       public virtual ICollection<Price> Prices { get; set; }
    }
}
