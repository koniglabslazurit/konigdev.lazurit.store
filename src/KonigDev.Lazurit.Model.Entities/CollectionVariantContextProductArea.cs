﻿using System;

namespace KonigDev.Lazurit.Model.Entities
{
    public class CollectionVariantContentProductArea
    {
        public Guid Id { get; set; }
        public bool IsEnabled { get; set; }
        public int Top { get; set; }
        public int Left { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public Guid CollectionVariantId { get; set; }
        public Guid ProductId { get; set; }

        public virtual CollectionVariant CollectionVariant { get; set; }
        public virtual Product Product { get; set; }
    }
}
