﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Model.Entities
{
    public class Collection : IBaseEntity
    {
        public Collection()
        {
            CollectionVariants = new HashSet<CollectionVariant>();
            Products = new HashSet<Product>();
        }

        public Guid Id { get; set; }
        public string Title { get; set; }
        public Guid SeriesId { get; set; }
        public Guid RoomTypeId { get; set; }

        /// <summary>
        /// Маркетинговый вес
        /// </summary>
        public string Range { get; set; }

        public virtual Series Series { get; set; }
        public virtual RoomType RoomType { get; set; }
        public virtual ICollection<CollectionVariant> CollectionVariants { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}