﻿namespace KonigDev.Lazurit.Model.Entities.Interfaces
{
    public interface IOneCEntity
    {
        /// <summary>
        /// Уникальный код из БД 1С.
        /// </summary>
        string SyncCode1C { get; set; }
    }
}
