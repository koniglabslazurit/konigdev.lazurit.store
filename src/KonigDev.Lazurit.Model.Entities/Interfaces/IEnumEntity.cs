﻿namespace KonigDev.Lazurit.Model.Entities.Interfaces
{
    public interface IEnumEntity
    {        
        string Name { get; set; }
        string TechName { get; set; }
        short SortNumber { get; set; }
    }
}
