﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Model.Entities
{
   public class LayawayItemCollection : IBaseEntity
    {
        public LayawayItemCollection()
        {
            ProductsInCollection = new HashSet<LayawayItemProductInCollection>();
        }
        public Guid Id { set; get; }

        public Guid CollectionVarinatId { set; get; }
        public Guid LayawayId { set; get; }

        public int Quantity { set; get; }

        public virtual ICollection<LayawayItemProductInCollection> ProductsInCollection { set; get; }
        public Layaway Layaway { set; get; }
        public CollectionVariant CollectionVariant { set; get; }
    }
}
