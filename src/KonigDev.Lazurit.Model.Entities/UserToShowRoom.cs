﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;

namespace KonigDev.Lazurit.Model.Entities
{
    public class UserToShowRoom
    {
        public Guid UserId { get; set; }
        public Guid ShowRoomId { get; set; }

        public virtual Showroom ShowRoom { get; set; }
        public virtual UserProfile User { get; set; }
    }
}
