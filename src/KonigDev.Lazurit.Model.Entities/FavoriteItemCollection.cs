﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Model.Entities
{
   public class FavoriteItemCollection : IBaseEntity
    {
        public FavoriteItemCollection()
        {
            Products = new HashSet<FavoriteItemProductInCollection>();
        }
        public Guid Id { set; get; }
        public int Quantity { set; get; }
        public DateTime CreationDate { set; get; }

        public Guid CollectionVariantId { set; get; }
        public Guid FavoriteId { set; get; }

        public virtual CollectionVariant CollectionVariant { set; get; }
        public virtual ICollection<FavoriteItemProductInCollection> Products { set; get; }
        public virtual Favorite Favorite { set; get; }
    }
}
