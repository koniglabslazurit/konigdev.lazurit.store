﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Model.Entities
{
   public class PaymentMethod : IBaseEntity, IEnumEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string TechName { get; set; }
        public short SortNumber { get; set; }
    }
}
