﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Model.Entities
{
    public class MarketingAction : IBaseEntity
    {
        public Guid Id { get; set; }
        public int SeoId { get; set; }

        public decimal Discount { get; set; }
        public bool IsVisible { get; set; }
        public string FileId { get; set; }

        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
