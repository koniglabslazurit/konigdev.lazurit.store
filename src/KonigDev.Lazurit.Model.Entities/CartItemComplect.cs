﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Model.Entities
{
    public class CartItemComplect : IBaseEntity
    {
        public Guid Id { set; get; }
        public int Quantity { set; get; }
        public bool IsAssemblyRequired { get; set; }
     
        public Guid CollectionVariantId { set; get; }
        public Guid CartId { set; get; }
                
        public virtual CollectionVariant CollectionVariant { set; get; }
        public virtual Cart Cart { set; get; }
    }
}
