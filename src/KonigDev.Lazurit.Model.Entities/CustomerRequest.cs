﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;

namespace KonigDev.Lazurit.Model.Entities
{
    /// <summary>
    /// Customer request
    /// </summary>
    /// <example>
    /// Call request
    /// </example>
    public class CustomerRequest : IBaseEntity
    {
        /// <summary>
        /// Request Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Request creation time
        /// </summary>
        public DateTime CreationTime { get; set; }

        /// <summary>
        /// Customer full name
        /// </summary>
        /// <remarks>
        /// ФИО
        /// </remarks>
        public string CustomerFullName { get; set; }

        /// <summary>
        /// Customer email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Customer phone number
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Request type
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Request status
        /// </summary>
        public string Status { get; set; }
    }
}
