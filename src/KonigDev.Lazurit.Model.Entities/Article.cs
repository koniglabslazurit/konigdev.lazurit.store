﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Model.Entities
{
    //todo need rename this entity to VendorCode
    /// <summary>
    /// Маркетинговый артикул для товара
    /// </summary>
    public class Article : IBaseEntity, IOneCEntity
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string SyncCode1C { get; set; }
                
        /// <summary>
        /// Маркетинговый вес
        /// </summary>
        public string Range { get; set; }

        public Guid FurnitureTypeId { get; set; }

        public virtual FurnitureType FurnitureType { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}