﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Model.Entities
{
    /// <summary>
    /// Стили
    /// </summary>
    public class Style: IBaseEntity, IOneCEntity
    {
        public Guid Id { get; set; }

        public string SyncCode1C { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
