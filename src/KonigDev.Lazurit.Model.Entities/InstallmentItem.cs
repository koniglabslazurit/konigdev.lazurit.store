﻿using System;

namespace KonigDev.Lazurit.Model.Entities
{
    /// <summary>
    /// Хранение рассрочек по товарам, и комплектам.
    /// </summary>    
    public class InstallmentItem
    {
        public Guid Id { get; set; }

        public byte Discount { get; set; }
        public byte Installment6Month { get; set; }
        public byte Installment10Month { get; set; }
        public byte Installment12Month { get; set; }
        public byte Installment18Month { get; set; }
        public byte Installment24Month { get; set; }
        public byte Installment36Month { get; set; }
        public byte Installment48Month { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }

        public Guid? ProductId { get; set; }
        public Guid? CollectionVariantId { get; set; }
        public Guid InstallmentId { get; set; }

        public virtual Product Product { get; set; }
        public virtual CollectionVariant CollectionVariant { get; set; }
        public virtual Installment Installment { get; set; }
    }
}