﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Model.Entities
{
    public class City : IBaseEntity
    {
        public City()
        {
            Showrooms = new HashSet<Showroom>();
        }
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string SyncCode1C { get; set; }
        public string CodeKladr { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }
        public decimal DeliveryPrice { get; set; }

        public Guid RegionId { get; set; }
        public Guid PriceZoneId { get; set; }        

        public virtual Region Region { get; set; }
        public virtual PriceZone PriceZone { get; set; }

        public virtual ICollection<Showroom> Showrooms { get; set; }
    }
}