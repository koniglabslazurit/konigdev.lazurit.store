﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Model.Entities
{

    public class Order : IBaseEntity, IOneCEntity
    {
        public Order()
        {
            Products = new HashSet<OrderItemProduct>();
            Complects = new HashSet<OrderItemComplect>();
            Collections = new HashSet<OrderItemCollection>();
        }
        public Guid Id { get; set; }
        public string SyncCode1C { get; set; }
        public long Number { get; set; }
        public decimal Sum { get; set; }
        public DateTime CreationTime { get; set; }
        public string DeliveryAddress { get; set; }
        public string DeliveryKladrCode { get; set; }
        public DateTime? ApproveTime { get; set; }
        public DateTime? CreationTime1C { get; set; }
        public string OrderNumber1C { get; set; }
        public string KkmOrderRecordUrl { get; set; }



        /* todo нужно ли дбавить поле что требуется апрув и оставить что заапрувлен продавцом? */
        /// <summary>
        /// Признак апрува продавцом заказа
        /// </summary>
        public bool IsApprovedBySeller { get; set; }
        public bool IsPaid { get; set; }
        public bool IsDeliveryRequired { get; set; }

       
        public Guid UserProfileId { set; get; }
        public Guid? ApprovingUserId { get; set; }
        public Guid PaymentMethodId { get; set;}

        public virtual UserProfile User { set; get; }
        public virtual UserProfile ApprovingUser { set; get; }
        public virtual PaymentMethod PaymentMethod { get; set; }

        public virtual ICollection<OrderItemProduct> Products { set; get; }
        public virtual ICollection<OrderItemComplect> Complects { set; get; }
        public virtual ICollection<OrderItemCollection> Collections { get; set; }
    }
}
