﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Model.Entities
{
    public class FavoriteItemProduct : IBaseEntity
    {
        public Guid Id { set; get; }
        public int Quantity { set; get; }
        public DateTime CreationDate { set; get; }

        public Guid ProductId { set; get; }
        public Guid FavoriteId { set; get; }
        public Guid CollectionVariantId { set; get; }
        
        public virtual Product Product { set; get; }
        public virtual Favorite Favorite { set; get; }
    }
}
