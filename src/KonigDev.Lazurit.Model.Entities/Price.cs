﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Model.Entities.Interfaces;

namespace KonigDev.Lazurit.Model.Entities
{
    public class Price : IBaseEntity
    {
        public Guid Id { get; set; }
        /// <summary>
        /// Значение цены
        /// </summary>
        public decimal Value { get; set; }

        public decimal PriceForAssembly { get; set; }
        /// <summary>
        /// Скидка
        /// </summary>
        public decimal? Discount { get; set; }

        public Guid PriceZoneId { get; set; }
        public virtual PriceZone PriceZone { get; set; }
        public Guid? ProductId { get; set; }
        public virtual Product Product { get; set; }

        public Guid? CollectionVariantId { get; set; }
        /// <summary>
        /// Код валюты
        /// </summary>
        public string CurrencyCode { get; set; }
    }
}
