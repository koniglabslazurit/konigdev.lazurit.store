﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Model.Entities
{
    public class Installment
    {
        public Installment()
        {
            InstallmentItems = new HashSet<InstallmentItem>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public byte Discount { get; set; }
        public byte Installment6Month { get; set; }
        public byte Installment10Month { get; set; }
        public byte Installment12Month { get; set; }
        public byte Installment18Month { get; set; }
        public byte Installment24Month { get; set; }
        public byte Installment36Month { get; set; }
        public byte Installment48Month { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }

        public virtual ICollection<InstallmentItem> InstallmentItems { get; set; }
    }
}
