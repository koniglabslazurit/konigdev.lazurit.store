﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KonigDev.Lazurit.Model.Entities
{
   public class OrderItemCollection : IBaseEntity
    {
        public OrderItemCollection()
        {
            Products = new HashSet<OrderItemProductInCollection>();
        }
        public Guid Id { get; set; }
       
        public int Quantity { set; get; }

        public Guid CollectionVariantId { set; get; }
        public Guid OrderId { set; get; }

        public virtual CollectionVariant CollectionVariant { set; get; }
        public virtual Order Order { set; get; }
        public virtual ICollection<OrderItemProductInCollection> Products { set; get; }
    }
}
