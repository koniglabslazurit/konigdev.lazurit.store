﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;

namespace KonigDev.Lazurit.Model.Entities
{
    public class ProductPack : IBaseEntity, IOneCEntity
    {
        public Guid Id { get; set; }

        public string ProductSyncCode1C { get; set; }
        public string SyncCode1C { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public decimal Weight { get; set; }
        public int Amount { get; set; }
    }
}
