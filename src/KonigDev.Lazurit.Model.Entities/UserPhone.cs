﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Model.Entities
{
   public class UserPhone : IBaseEntity
    {
        public Guid Id { get; set; }
        public Guid UserProfileId { get; set; }
        public string Phone { get; set; }
        public bool IsDefault { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
