﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Model.Entities
{
    public class RoomType : IBaseEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        /// <summary>
        /// Алиас-это имя, которое является уникальным для данного элемента и может вставляться в УРЛ адресс. Оно нужно для СЕО.
        /// </summary>
        public string Alias { get; set; }

        /// <summary>
        /// Уникальный код из БД 1С.
        /// </summary>
        public string SyncCode1C { get; set; }

        /// <summary>
        /// Маркетинговый вес
        /// </summary>
        public string Range { get; set; }

        public virtual ICollection<Collection> Collections { get; set; }
    }
}
