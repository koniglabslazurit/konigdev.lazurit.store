﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Model.Entities
{
    public class Favorite : IBaseEntity
    {
        public Favorite()
        {
            Products = new HashSet<FavoriteItemProduct>();
            Complects = new HashSet<FavoriteItemComplect>();
            Collections = new HashSet<FavoriteItemCollection>();
        }

        public Guid Id { get; set; }

        public virtual ICollection<FavoriteItemProduct> Products { set; get; }
        public virtual ICollection<FavoriteItemComplect> Complects { set; get; }
        public virtual ICollection<FavoriteItemCollection> Collections { set; get; }
        public virtual UserProfile UserProfile { set; get; }
    }


}
