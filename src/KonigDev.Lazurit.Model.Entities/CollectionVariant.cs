﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Model.Entities
{
    //TODO не забывать удалять цены
    public class CollectionVariant : IBaseEntity
    {

        public CollectionVariant()
        {
            Products = new HashSet<Product>();
        }
        public Guid Id { get; set; }

        public Guid CollectionId { get; set; }   
        //TODO: we should delete these id's cuz there is m to m connection from CV to Facings, FC, FrmC
        [Obsolete]     
        public Guid FacingId { get; set; }
        [Obsolete]
        public Guid FacingColorId { get; set; }
        [Obsolete]
        public Guid FrameColorId { get; set; }

        /// <summary>
        /// Признак дефлотного варианта коллекции
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// Признак комплекта
        /// </summary>
        public bool IsComplect { get; set; }

        /// <summary>
        /// Код 1с для комплектов
        /// </summary>
        public string SyncCode1C { get; set; }

        public virtual Collection Collection { get; set; }
        public virtual Facing Facing { get; set; }
        public virtual FacingColor FacingColor { get; set; }
        public virtual FrameColor FrameColor { get; set; }

        public virtual ICollection<CollectionVariant> CollectionVariantComplects { get; set; }
        public virtual ICollection<CollectionVariantContentProductArea> CollectionVariantContentProductAreas { get; set; }
        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<FavoriteItemComplect> FavoriteItemComplects { get; set; }
        public virtual ICollection<FavoriteItemCollection> FavoriteItemCollections { get; set; }
    }
}
