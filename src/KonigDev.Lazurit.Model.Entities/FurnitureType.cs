﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Model.Entities
{
    public class FurnitureType : IBaseEntity, IOneCEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }        
        public string NameInPlural { get; set; }

        /// <summary>
        /// Алиас-это имя, которое является уникальным для данного элемента и может вставляться в УРЛ адресс. Оно нужно для СЕО. ОБЯЗАТЕЛЬНО сохранять алиас при импорте с 1С
        /// </summary>
        public string Alias { get; set; }

        /// <summary>
        /// Алиас во множественном числе - это имя, которое является уникальным для данного элемента и может вставляться в УРЛ адресс. Оно нужно для СЕО. ОБЯЗАТЕЛЬНО сохранять алиас при импорте с 1С
        /// </summary>
        public string AliasInPlural { get; set; }
        public string SyncCode1C { get; set; }

        /// <summary>
        /// Признак и ссылка на родителя, группы типов мебели (=тип артикула, =форма артикула)
        /// </summary>
        public Guid? ParentId { get; set; }
        public bool IsFurniturePiece { get; set; }        

        public virtual ICollection<Article> Articles { get; set; }
    }
}
