﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;

namespace KonigDev.Lazurit.Model.Entities
{
   public class LayawayItemProduct : IBaseEntity
    {
        public Guid Id { set; get; }

        public Guid ProductId { set; get; }
        public Guid LayawayId { set; get; }

        public bool IsAssemblyRequired { get; set; }
        public int Quantity { set; get; }

        public Product Product { set; get; }
        public Layaway Layaway { set; get; }
    }
}
