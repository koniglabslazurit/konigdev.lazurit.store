﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.MarketingActions.Queries
{
    [Serializable]
    public class SingleMarketingActionQuery
    {
        public int ActionSeoId { get; set; }
    }
}
