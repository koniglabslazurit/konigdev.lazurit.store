﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.MarketingActions
{
    [Serializable]
    public class DtoMarketingActions
    {
        public ICollection<DtoMarketingAction> Collection { get; set; }
        public int Total { get; set; }
    }
}
