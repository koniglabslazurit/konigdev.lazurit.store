﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.MarketingActions.Commands
{
    [Serializable]
    public class UpdateMarketingActionCommand
    {
        public DtoMarketingAction Action { get; set; }
    }
}
