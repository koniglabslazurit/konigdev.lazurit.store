﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Payment.Dto
{
    public class DtoPaymentSettings
    {
        public string RobokassaUrl { get; set; }
        public string MerchantLogin { get; set; }
        public string Password1 { get; set; }
        public string Password2 { get; set; }
    }
}
