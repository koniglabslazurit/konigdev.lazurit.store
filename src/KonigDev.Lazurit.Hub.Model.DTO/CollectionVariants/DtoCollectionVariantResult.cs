﻿using System;
using System.Collections.Generic;
using KonigDev.Lazurit.Hub.Model.DTO.Products;
using KonigDev.Lazurit.Hub.Model.DTO.Facings;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors;
using KonigDev.Lazurit.Hub.Model.DTO.Styles.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Dto;

namespace KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants
{
    [Serializable]
    public class DtoCollectionVariantResult
    {
        public DtoCollectionVariantResult()
        {
            Styles = new HashSet<DtoStyleItem>();
            TargetAudiences = new HashSet<DtoTargetAudienceItem>();
        }
        public Guid Id { get; set; }
        public Guid CollectionId { get; set; }       
        public bool IsAssemblyRequired { get; set; }
        public DtoFacingsResult Facing { get; set; }
        public DtoFacingColorResult FacingColor { get; set; }
        public DtoFrameColorResult FrameColor { get; set; }
        public IEnumerable<DtoProductResult> Products { get; set; }
        public string SeriesName { get; set; }
        public string RoomTypeName { get; set; }
        public string CollectionTypeName { get; set; }   
        [Obsolete("Must be rename to series alias")]    
        public string CollectionAlias { get; set; }
        public string RoomTypeAlias { get; set; }
        public bool IsComplect { get; set; }
        public bool IsFavorite { get; set; }
        public Guid? FavoriteItemCollectionId { get; set; }
        public IEnumerable<DtoStyleItem> Styles { get; set; }
        public IEnumerable<DtoTargetAudienceItem> TargetAudiences { get; set; }
    }
}
