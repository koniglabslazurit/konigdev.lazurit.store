﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Query
{
    [DataContract]
    [Serializable]
    public class GetComplectsPricesQuery
    {
        public List<Guid> ComplectIds { get; set; }

        public Guid PriceZoneId { get; set; }

        public string CurrencyCode { get; set; }
    }
}
