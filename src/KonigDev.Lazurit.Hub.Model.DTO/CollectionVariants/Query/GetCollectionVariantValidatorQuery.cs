﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Query
{
    [Serializable]
    public class GetCollectionVariantValidatorQuery
    {
        public Guid Id { get; set; }
    }
}
