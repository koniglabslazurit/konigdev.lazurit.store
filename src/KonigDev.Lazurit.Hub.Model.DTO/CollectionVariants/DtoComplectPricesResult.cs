﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants
{
    [Serializable]
    public class DtoComplectPricesResult
    {
        public Guid ComplectId { get; set; }

        public decimal Price { get; set; }

        public decimal? Discount { get; set; }

        public decimal AssemblyPrice { get; set; }
    }
}
