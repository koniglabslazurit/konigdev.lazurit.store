﻿using System;
using System.Collections.Generic;
using KonigDev.Lazurit.Hub.Model.DTO.Products;
using KonigDev.Lazurit.Hub.Model.DTO.Styles.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Facings.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Dto;

namespace KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants
{
    [Serializable]
    public class DtoCollectionVariantResultForCatalog
    {
        public DtoCollectionVariantResultForCatalog()
        {
            Facings = new List<DtoFacingItem>();
            FacingColors = new List<DtoFacingColorItem>();
            FrameColors = new List<DtoFrameColorItem>();
            Styles = new List<DtoStyleItem>();
            TargetAudiences = new List<DtoTargetAudienceItem>();
        }
        public Guid Id { get; set; }
        public Guid CollectionId { get; set; }
        public IEnumerable<DtoFacingItem> Facings { get; set; }
        public IEnumerable<DtoFacingColorItem> FacingColors { get; set; }
        public IEnumerable<DtoFrameColorItem> FrameColors { get; set; }
        public IEnumerable<DtoStyleItem> Styles { get; set; }
        public IEnumerable<DtoTargetAudienceItem> TargetAudiences { get; set; }
    }
}
