﻿using KonigDev.Lazurit.Hub.Model.DTO.FacingColors;
using KonigDev.Lazurit.Hub.Model.DTO.Facings;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Dto
{
    [Serializable]
    public class DtoCollectionVariantContent
    {
        public Guid Id { get; set; }
        public Guid CollectionId { get; set; }
        public IEnumerable<DtoFacingsResult> Facings { get; set; }
        public IEnumerable<DtoFacingColorResult> FacingColors { get; set; }
        public IEnumerable<DtoFrameColorResult> FrameColors { get; set; }
        public string CollectionTypeName { get; set; }
        public bool IsComplect { get; set; }
        public bool IsFavorite { get; set; }
        public IEnumerable<DtoProductContent> Products { get; set; }
    }
}
