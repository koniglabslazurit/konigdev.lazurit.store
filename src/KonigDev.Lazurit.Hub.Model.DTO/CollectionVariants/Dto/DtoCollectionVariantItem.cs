﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Dto
{
    [Serializable]
    public class DtoCollectionVariantItem
    {
        public Guid Id { get; set; }
        public Guid CollectionId { get; set; }
        public string Facing { get; set; }
        public string FacingColor { get; set; }
        public string FrameColor { get; set; }
    }
}
