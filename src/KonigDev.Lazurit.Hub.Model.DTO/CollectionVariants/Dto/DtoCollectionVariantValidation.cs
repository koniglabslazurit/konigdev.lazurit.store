﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Dto
{
    [Serializable]
    public class DtoCollectionVariantValidation
    {
        public DtoCollectionVariantValidation()
        {
            Errors = new List<string>();
        }

        public bool IsValid { get; set; }
        public List<string> Errors { get; set; }
    }
}
