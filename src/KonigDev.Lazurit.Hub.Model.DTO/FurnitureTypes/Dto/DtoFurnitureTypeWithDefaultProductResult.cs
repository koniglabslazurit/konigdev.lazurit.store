﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Dto
{
    [Serializable]
    public class DtoFurnitureTypeWithDefaultProductResult
    {
        public Guid Id { get; set; }
        public string Alias { get; set; }
        public string Name { get; set; }
        public Guid? DefaultProductId { get; set; }
    }
}
