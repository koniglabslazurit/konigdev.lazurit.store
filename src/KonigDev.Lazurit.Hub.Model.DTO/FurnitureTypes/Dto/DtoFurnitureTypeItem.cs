﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Dto
{
    [Serializable]
    public class DtoFurnitureTypeItem
    {
        public Guid Id { get; set; }
        public string SyncCode1C { get; set; }
        public string Alias { get; set; }
        public string Name { get; set; }
        public Guid? ParentId { get; set; }
    }
}
