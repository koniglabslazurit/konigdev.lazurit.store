﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Query
{
   public class GetFurnitureTypesByCollectionQuery
    {
        public Guid? CollectionId { get; set; }
        public Guid CollectionVariantId { get; set; }
        public IEnumerable<Guid?> FacingIds { get; set; }
        public IEnumerable<Guid?> FacingColorIds { get; set; }
        public IEnumerable<Guid?> FrameColorIds { get; set; }
    }
}
