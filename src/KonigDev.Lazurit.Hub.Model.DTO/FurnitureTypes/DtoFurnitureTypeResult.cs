﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes
{
    [Serializable]
    public class DtoFurnitureTypeResult
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string NameInPlural{ get; set; }
        public int Amount { get; set; }
    }
}