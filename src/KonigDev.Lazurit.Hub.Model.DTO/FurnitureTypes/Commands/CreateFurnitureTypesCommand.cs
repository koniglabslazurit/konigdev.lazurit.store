﻿using KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.FurnitureTypes.Commands
{
    [Serializable]
    public class CreateFurnitureTypesCommand
    {
        public List<DtoFurnitureTypeItem> FurnitureTypes { get; set; }
    }
}
