﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cities.Queries
{
    [Serializable]
    public class DtoCityCoordinates
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }
        public double Distance { get; set; }
        public decimal DeliveryPrice { get; set; }
        public Guid RegionId { get; set; }
        public Guid PriceZoneId { get; set; }
    }
}
