﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cities.Queries
{
    [Serializable]
    public class GetCityQuery
    {
        public Guid Id { get; set; }
    }
}
