﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cities.Queries
{
    public class GetCityByCoordinates
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
