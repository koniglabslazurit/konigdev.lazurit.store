﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cities.Dto
{
    [Serializable]
    public class DtoCityItem
    {
        public Guid Id { get; set; }
        public string SyncCode1C { get; set; }
    }
}
