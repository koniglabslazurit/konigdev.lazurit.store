﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cities.Commands
{
    [Serializable]
    public class UpdateCityCommand
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string SyncCode1C { get; set; }
        public string CodeKladr { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }
        public decimal DeliveryPrice { get; set; }

        public Guid RegionId { get; set; }
        public Guid PriceZoneId { get; set; }
    }
}
