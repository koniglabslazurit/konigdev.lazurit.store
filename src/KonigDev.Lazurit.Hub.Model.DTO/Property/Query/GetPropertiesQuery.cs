﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Property.Query
{
    [Serializable]
    public class GetPropertiesQuery
    {
        public string PropertyType { get; set; }
    }
}