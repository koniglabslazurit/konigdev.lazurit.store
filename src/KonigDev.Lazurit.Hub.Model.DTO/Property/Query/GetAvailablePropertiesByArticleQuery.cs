﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Property.Query
{
    [Serializable]
    public class GetAvailablePropertiesByArticleQuery
    {
        public Guid ArticleId { get; set; }
    }
}
