﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Property.Dto
{
    [Serializable]
    public class DtoPropertyCreating
    {        
        public Guid? GroupeId { get; set; }
        public Guid Id { get; set; }
        public bool IsMultiValue { get; set; }
        public string SyncCode1C { get; set; }
        public string Title { get; set; }
        public string Value { get; set; }
    }
}
