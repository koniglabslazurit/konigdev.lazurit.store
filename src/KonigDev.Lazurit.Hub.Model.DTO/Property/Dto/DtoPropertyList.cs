﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Property.Dto
{

    [Serializable]
    public class DtoPropertyList
    {
        public DtoPropertyList()
        {
            ListPropirties = new List<DtoPropertyItem>();
        }
        public List<DtoPropertyItem> ListPropirties { get; set; }        
    }
}
