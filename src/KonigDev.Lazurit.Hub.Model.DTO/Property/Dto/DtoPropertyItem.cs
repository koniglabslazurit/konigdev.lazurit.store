﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Property.Dto
{
    [Serializable]
    public class DtoPropertyItem
    {
        public Guid Id { get; set; }
        public string SyncCode1C { get; set; }
        public string Title { get; set; }
        public string TitleInPlural { get; set; }
        public string Alias { get; set; }
        public string AliasInPlural { get; set; }
        public string Value { get; set; }
        public bool IsRequired { get; set; }
        public bool IsMultiValue { get; set; }
        public int SortNumber { get; set; }
        public Guid? GroupeId { get; set; }
    }
}
