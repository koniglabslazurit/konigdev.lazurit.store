﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Property.Dto
{
    [Serializable]
    public class DtoProperties
    {
        public DtoProperties()
        {
            Properties = new List<DtoPropertyList>();
            PropertiesSingle = new List<DtoPropertyItem>();
        }

        public List<DtoPropertyList> Properties { get; set; }
        public List<DtoPropertyItem> PropertiesSingle { get; set; }
    }
}
