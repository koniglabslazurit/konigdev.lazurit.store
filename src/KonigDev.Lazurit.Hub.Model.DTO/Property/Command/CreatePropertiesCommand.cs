﻿using KonigDev.Lazurit.Hub.Model.DTO.Property.Dto;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Property.Command
{
    [Serializable]
    public class CreatePropertiesCommand
    {
        public List<DtoPropertyCreating> Properties { get; set; }
    }
}
