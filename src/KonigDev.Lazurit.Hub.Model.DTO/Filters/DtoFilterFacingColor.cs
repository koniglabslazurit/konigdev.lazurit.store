﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Filters
{

    public class DtoFilterFacingColor
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
