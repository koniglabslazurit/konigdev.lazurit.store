﻿using KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Facings.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Dto;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Filters
{
    public class DtoFilterItem
    {
        public List<DtoFacingColorItem> FacingColors { get; set; }
        public List<DtoFacingItem> Facings { get; set; }
        public List<DtoFrameColorItem> FrameColors { get; set; }
    }
}
