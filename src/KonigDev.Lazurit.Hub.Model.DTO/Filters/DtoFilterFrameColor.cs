﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Filters
{
    public class DtoFilterFrameColor
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        //public string TechName { get; set; }
        public IEnumerable<DtoFilterFacing> AliavableFacings { get; set; }
    }
}