﻿using KonigDev.Lazurit.Hub.Model.DTO.Products.Query;
using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Filters.Query
{
    [Serializable]
    public class GetFiltersByProductRequestQuery : GetProductsByRequestQuery
    {

    }
}
