﻿using KonigDev.Lazurit.Core.Enums;
using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Dto.Command
{
    [Serializable]
    public class CreateFileContentCommand
    {
        public Guid ObjectId { get; set; }
        public string FileExtension { get; set; }
        public string FileName { get; set; }
        public long Size { get; set; }
        public Guid Id { get; set; }
        public EnumFileContentType Type { get; set; }
    }
}