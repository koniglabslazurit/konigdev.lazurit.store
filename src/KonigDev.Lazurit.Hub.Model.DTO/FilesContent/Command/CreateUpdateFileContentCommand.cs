﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Core.Enums;

namespace KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Command
{
    [Serializable]
    public class CreateUpdateFileContentCommand
    {
            public Guid ObjectId { get; set; }
            public string FileExtension { get; set; }
            public string FileName { get; set; }
            public long Size { get; set; }
            public Guid Id { get; set; }
            public EnumFileContentType Type { get; set; }
    }
}
