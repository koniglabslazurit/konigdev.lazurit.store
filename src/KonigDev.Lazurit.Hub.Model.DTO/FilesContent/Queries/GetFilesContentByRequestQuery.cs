﻿using KonigDev.Lazurit.Core.Enums;
using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Queries
{
    public class GetFilesContentByRequestQuery
    {
        public Guid ObjectId { get; set; }
        public EnumFileContentType Type { get; set; }
    }
}
