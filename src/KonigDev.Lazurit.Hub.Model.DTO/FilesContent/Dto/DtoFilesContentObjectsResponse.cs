﻿using KonigDev.Lazurit.Hub.Model.DTO.Collections;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors;
using KonigDev.Lazurit.Hub.Model.DTO.Facings;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors;
using System.Collections.Generic;
using KonigDev.Lazurit.Hub.Model.DTO.Products;

namespace KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Dto
{
    public class DtoFilesContentObjectsResponse
    {
        public List<DtoCollectionResult> Collections { get; set; }
        public List<DtoCollectionVariant> CollectionVariants { get; set; }
        public List<DtoFacingColorResult> FacingColors { get; set; }
        public List<DtoFacingsResult> Facings { get; set; }
        public List<DtoFrameColorResult> FrameColors { get; set; }
        public List<DtoProductResult> Products { get; set; }
    }
}
