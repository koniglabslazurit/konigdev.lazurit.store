﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.FilesContent.Dto
{
    [Serializable]
    public class DtoFilesContent
    {
        public Guid Id { get; set; }
        public string Extensions { get; set; }
        public string Type { get; set; }
        public string FileName { get; set; }
    }
}