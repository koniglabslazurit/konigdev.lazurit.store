﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.ProductPackages
{
    [Serializable]
    public class DtoProductPackageResult
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public int Amount { get; set; }
        public decimal Weight { get; set; }

    }
}
