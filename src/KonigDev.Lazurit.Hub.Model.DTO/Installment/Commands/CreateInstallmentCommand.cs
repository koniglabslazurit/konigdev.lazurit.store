﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Installment.Commands
{
    [Serializable]
    public class CreateInstallmentCommand
    {
        public CreateInstallmentCommand()
        {
            VendorCodes = new List<Guid>();
            Series = new List<Guid>();
            FurnitureTypes = new List<Guid>();
            Rooms = new List<Guid>();
        }

        //public Guid Id { get; set; }
        public string Name { get; set; }
        public byte Discount { get; set; }
        public byte Installment6Month { get; set; }
        public byte Installment10Month { get; set; }
        public byte Installment12Month { get; set; }
        public byte Installment18Month { get; set; }
        public byte Installment24Month { get; set; }
        public byte Installment36Month { get; set; }
        public byte Installment48Month { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }

        public List<Guid> Series { get; set; }
        public List<Guid> Rooms { get; set; }
        public List<Guid> FurnitureTypes { get; set; }
        public List<Guid> VendorCodes { get; set; }
    }
}
