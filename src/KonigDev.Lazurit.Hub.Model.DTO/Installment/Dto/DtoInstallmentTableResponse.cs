﻿using KonigDev.Lazurit.Hub.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Installment.Dto
{
    [Serializable]
    public class DtoInstallmentTableResponse: DtoCommonTableResponse<DtoInstallmentItem>
    {
    }
}
