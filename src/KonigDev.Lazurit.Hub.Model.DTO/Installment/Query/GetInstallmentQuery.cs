﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Installment.Query
{
    [Serializable]
    public class GetInstallmentQuery : DtoCommonTableRequest
    {
        public bool IsFuture { get; set; }
    }
}
