﻿using KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Dto;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Commands
{
    [Serializable]
    public class UpdateRoomTypesCommand
    {
        public List<DtoUpdatingRoomType> RoomTypes { get; set; }
    }
}
