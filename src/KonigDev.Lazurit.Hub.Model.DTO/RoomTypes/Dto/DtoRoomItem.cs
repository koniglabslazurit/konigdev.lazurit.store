﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Dto
{
    [Serializable]
    public class DtoRoomItem
    {
        public Guid Id { get; set; }
        public string SyncCode1C { get; set; }

    }
}
