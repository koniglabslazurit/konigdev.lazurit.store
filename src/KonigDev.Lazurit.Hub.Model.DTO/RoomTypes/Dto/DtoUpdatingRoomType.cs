﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.RoomTypes.Dto
{
    [Serializable]
    public class DtoUpdatingRoomType
    {
        public Guid Id { get; set; }
        public string RangeRoom { get; set; }
        public string SyncCode1C { get; set; }
    }
}
