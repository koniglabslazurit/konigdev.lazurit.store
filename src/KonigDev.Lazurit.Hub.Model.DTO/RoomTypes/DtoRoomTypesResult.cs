﻿using KonigDev.Lazurit.Hub.Model.DTO.Collections;
using KonigDev.Lazurit.Hub.Model.DTO.Styles.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Dto;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.RoomTypes
{
    public class DtoRoomTypesResult
    {
        public DtoRoomTypesResult()
        {
            Collections = new HashSet<DtoCollectionsResult>();
            Styles = new HashSet<DtoStyleItem>();
            TargetAudiences = new HashSet<DtoTargetAudienceItem>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Range { get; set; }
        public IEnumerable<DtoCollectionsResult> Collections { get; set; }
        public IEnumerable<DtoStyleItem> Styles { get; set; }
        public IEnumerable<DtoTargetAudienceItem> TargetAudiences { get; set; }
    }
}
