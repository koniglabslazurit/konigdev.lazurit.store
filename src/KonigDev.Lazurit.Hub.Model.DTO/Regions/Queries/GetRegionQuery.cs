﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Regions.Queries
{
    [Serializable]
    public class GetRegionQuery
    {
        public Guid Id { get; set; }
    }
}
