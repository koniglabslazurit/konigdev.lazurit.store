﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Regions.Queries
{
    [Serializable]
    public class DtoRegion
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string LazuritPasswordHash { get; set; }
        public string SyncCode1C { get; set; }
    }
}
