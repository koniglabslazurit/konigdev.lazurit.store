﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Regions.Commands
{
    [Serializable]
    public class RemoveRegionCommand
    {
        public Guid Id { get; set; }
    }
}
