﻿using KonigDev.Lazurit.Hub.Model.DTO.Regions.Dto;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Regions.Commands
{
    [Serializable]
    public class UpdateRegionsCommand
    {
        public List<DtoRegionUpdating> Regions { get; set; }
    }
}
