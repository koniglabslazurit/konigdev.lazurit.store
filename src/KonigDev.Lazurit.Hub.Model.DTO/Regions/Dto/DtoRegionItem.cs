﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Regions.Dto
{
    [Serializable]
    public class DtoRegionItem
    {
        public Guid Id { get; set; }
        public string SyncCode1C { get; set; }
    }
}
