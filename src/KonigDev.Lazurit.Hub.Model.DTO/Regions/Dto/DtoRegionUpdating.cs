﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace KonigDev.Lazurit.Hub.Model.DTO.Regions.Dto
{
    [Serializable]
    public class DtoRegionUpdating
    {
        public DtoRegionUpdating()
        {
            Cities = new List<DtoCityUpdating>();
        }

        public string SyncCode1C { get; set; }
        public string Title { get; set; }
        public List<DtoCityUpdating> Cities { get; set; }
        public string WebAddress { get; set; }
        public Guid Id { get; set; }
    }

    [Serializable]
    public class PriceZoneUpdating
    {
        public Guid Id { get; set; }
        public string SyncCode1C { get; set; }
    }

    [Serializable]
    public class DtoCityUpdating
    {
        public DtoCityUpdating()
        {
            Showrooms = new List<DtoShowroomUpdating>();
        }

        public Guid PriceZoneId { get; set; }
        public string SyncCode1C { get; set; }
        public string Title { get; set; }

        public List<DtoShowroomUpdating> Showrooms { get; set; }
        public Guid Id { get; set; }

        /// <summary>
        /// Долгота города, берем по магазинам если есть
        /// </summary>
        public float Longitude
        {
            get
            {
                if (this.Showrooms.Count == 0)
                    return 0;
                return this.Showrooms.First().Longitude;
            }
        }

        /// <summary>
        /// Широта города - берем по магазинам, если есть
        /// </summary>
        public float Latitude
        {
            get
            {
                if (this.Showrooms.Count == 0)
                    return 0;
                return this.Showrooms.First().Latitude;
            }
        }
    }

    [Serializable]
    public class DtoShowroomUpdating
    {
        private const char GeoSeparator = ',';

        public DtoShowroomUpdating()
        {
            Emails = new List<string>();
            Phones = new List<string>();
        }

        public string Address { get; set; }
        public List<string> Emails { get; set; }
        public List<string> Phones { get; set; }
        public string SyncCode1C { get; set; }
        public string Title { get; set; }

        public string GeoCoordinats { get; set; }

        /// <summary>
        /// Долгота
        /// </summary>
        public float Longitude
        {
            get
            {
                if (string.IsNullOrWhiteSpace(GeoCoordinats))
                    return 0;
                var splittedGeo = GeoCoordinats.Split(GeoSeparator);
                if (splittedGeo.Length != 2)
                    return 0;

                float res = 0;
                float.TryParse(splittedGeo[1], out res);
                return res;
            }
        }

        /// <summary>
        /// Широта
        /// </summary>
        public float Latitude
        {
            get
            {
                if (string.IsNullOrWhiteSpace(GeoCoordinats))
                    return 0;
                var splittedGeo = GeoCoordinats.Split(GeoSeparator);
                if (splittedGeo.Length != 2)
                    return 0;

                float res = 0;
                float.TryParse(splittedGeo[0], out res);
                return res;
            }
        }      
    }
}
