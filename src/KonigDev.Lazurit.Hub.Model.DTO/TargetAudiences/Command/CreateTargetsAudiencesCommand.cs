﻿using KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Dto;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Command
{
    [Serializable]
    public class CreateTargetsAudiencesCommand
    {
        public List<DtoTargetAudienceItem> TargetAudiences { get; set; }
    }
}
