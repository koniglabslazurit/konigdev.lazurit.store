﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Dto
{
    [Serializable]
    public class DtoTargetAudienceItem
    {
        public Guid Id { get; set; }
        public string SyncCode1C { get; set; }
        public string Name { get; set; }
    }
}
