﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Users
{
    [Serializable]
   public class DtoUserPhone
    {
        public Guid Id { get; set; }
        public string Phone { get; set; }
        public bool IsDefault { get; set; }
    }
}
