﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Users.Commands
{
    [Serializable]
    public class UpdateUserProfileCommand
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        //email изменяется через Auth
        //public string Email { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string SyncCode1C { get; set; }
        public virtual ICollection<DtoUserPhone> UserPhones { get; set; }
        public virtual ICollection<DtoUserAddress> UserAddresses { get; set; }
    }
}
