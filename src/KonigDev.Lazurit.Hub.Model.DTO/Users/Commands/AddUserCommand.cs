﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Users.Commands
{
   public class AddUserCommand
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string SyncCode1C { get; set; }
        public virtual ICollection<DtoUserPhone> UserPhones { get; set; }
        public virtual ICollection<DtoUserAddress> UserAddresses { get; set; }
    }
}
