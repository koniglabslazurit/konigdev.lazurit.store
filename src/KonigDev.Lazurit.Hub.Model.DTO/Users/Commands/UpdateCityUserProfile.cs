﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Users.Commands
{
    public class UpdateCityUserProfile
    {
        public Guid UserId { get; set; }
        public Guid CityId { get; set; }
    }
}
