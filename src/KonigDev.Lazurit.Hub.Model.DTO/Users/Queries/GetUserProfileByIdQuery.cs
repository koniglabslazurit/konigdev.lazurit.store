﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Users.Queries
{
    [Serializable]
    [DataContract]
    public class GetUserProfileByIdQuery
    {
        public Guid? UserId { get; set; }
    }
}
