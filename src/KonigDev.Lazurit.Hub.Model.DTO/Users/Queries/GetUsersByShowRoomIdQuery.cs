﻿using System;
using System.Runtime.Serialization;

namespace KonigDev.Lazurit.Hub.Model.DTO.Users.Queries
{
    [Serializable]
    [DataContract]
    public class GetUsersByShowRoomIdQuery
    {
        public Guid ShowRoomId { get; set; }
    }
}
