﻿using System;
using System.Runtime.Serialization;

namespace KonigDev.Lazurit.Hub.Model.DTO.Users.Region.Queries
{
    [Serializable]
    [DataContract]
    public class GetCitiesByRegionIdQuery
    {
        public Guid RegionId { get; set; }
    }
}
