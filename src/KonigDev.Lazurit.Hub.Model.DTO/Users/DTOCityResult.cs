﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Users
{
    [Serializable]
    public class DTOCityResult
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public Guid RegionId { get; set; }
    }
}
