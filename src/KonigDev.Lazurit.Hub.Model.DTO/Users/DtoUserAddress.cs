﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Users
{
    [Serializable]
   public class DtoUserAddress
    {
        public Guid Id { get; set; }
        public string KladrCode { get; set; }
        public string Address { get; set; }
        public bool IsDefault { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string Block { get; set; }
        public string Building { get; set; }
        public string House { get; set; }
        public string EntranceHall { get; set; }
        public string Floor { get; set; }
        public string Apartment { get; set; }
    }
}
