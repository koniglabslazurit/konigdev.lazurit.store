﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cart.MiniCartDto
{
    [Serializable]
    public class DtoCartMiniItemProduct: DtoCartItem
    {
        public Guid Id { get; set; }
        public Guid CartId { set; get; }
        public string SyncCode1C { get; set; }
        public Guid ArticleId { get; set; }
        public string Article { get; set; }
        public string FurnitureTypeAlias { get; set; }
        public string ArticleSyncCode1C { get; set; }

        public string FurnitureType { get; set; }
        public string SeriesName { get; set; }
        public string RoomTypeName { get; set; }
        [Obsolete("Must be rename to series alias")]
        public string CollectionAlias { get; set; }
    }
}
