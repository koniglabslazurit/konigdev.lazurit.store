﻿using System;
using System.Collections.Generic;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cart.MiniCartDto
{
    [Serializable]
    public class DtoCartMiniItemCollection
    {
        public Guid Id { set; get; }
        public Guid CartId { set; get; }
        public Guid CollectionVariantId { set; get; }
        public int Quantity { set; get; }
        public string SeriesName { get; set; }
        public string RoomTypeName { get; set; }
        [Obsolete("Must be rename to series alias")]
        public string CollectionAlias { get; set; }
        public string RoomTypeAlias { get; set; }
        public Guid CollectionId { get; set; }
        public List<DtoCartItem> Products { get; set; }
    }
}
