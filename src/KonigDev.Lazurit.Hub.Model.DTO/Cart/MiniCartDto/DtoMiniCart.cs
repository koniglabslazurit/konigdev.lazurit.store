﻿using System;
using System.Collections.Generic;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.FullCartDto;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cart.MiniCartDto
{
    [Serializable]
    public class DtoMiniCart
    {
        public Guid Id { set; get; }
        public bool IsDeliveryRequired { get; set; }

        public List<DtoCartMiniItemComplect> Complects { set; get; }
        public List<DtoCartMiniItemProduct> Products { set; get; }
        public List<DtoCartMiniItemCollection> Collections { get; set; }
    }
}
