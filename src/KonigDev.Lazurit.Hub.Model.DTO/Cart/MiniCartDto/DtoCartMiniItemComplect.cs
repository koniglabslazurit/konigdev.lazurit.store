﻿using System;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cart.MiniCartDto
{
    [Serializable]
    public class DtoCartMiniItemComplect : DtoCartItem
    {
        public Guid Id { get; set; }
        public Guid CartId { set; get; }
        public string SeriesName { get; set; }
        public string RoomTypeName { get; set; }
        public Guid CollectionId { get; set; }
        [Obsolete("Must be rename to series alias")]
        public string CollectionAlias { get; set; }
        public string RoomTypeAlias { get; set; }

    }
}
