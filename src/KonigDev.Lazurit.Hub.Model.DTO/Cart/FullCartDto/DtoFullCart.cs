using System;
using System.Collections.Generic;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cart.FullCartDto
{
    [Serializable]
    public class DtoFullCart
    {
        public Guid Id { set; get; }
        public bool IsDeliveryRequired { get; set; }
        public List<DtoCartItemComplect> Complects { set; get; }
        public List<DtoCartItemProduct> Products { set; get; }
        public ICollection<DtoCartItemCollection> Collections { get; set; }
    }
}
