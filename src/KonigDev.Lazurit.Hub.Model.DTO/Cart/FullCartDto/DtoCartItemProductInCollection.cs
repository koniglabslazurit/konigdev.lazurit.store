﻿using System;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.Products;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cart.FullCartDto
{
    [Serializable]
    public class DtoCartItemProductInCollection:DtoCartItem
    {
        public Guid Id { set; get; }

        public Guid CartId { set; get; }

        public Guid CartItemCollectionId { get; set; }
        public DtoProductResult Product { set; get; }
    }
}
