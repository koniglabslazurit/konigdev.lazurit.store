﻿using System;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cart.FullCartDto
{
    [Serializable]
    public class DtoCartItemComplect : DtoCartItem
    {
        public Guid Id { set; get; }

        public Guid CartId { set; get; }

        public DtoCollectionVariantResult CollectionVariant { set; get; }
    }
}
