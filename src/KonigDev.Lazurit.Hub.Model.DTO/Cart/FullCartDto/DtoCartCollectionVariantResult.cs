﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cart.FullCartDto
{
    public class DtoCartCollectionVariantResult
    {
        public Guid Id { get; set; }
        public Guid CollectionId { get; set; }
        public bool IsAssemblyRequired { get; set; }
        public string SeriesName { get; set; }
        public string RoomTypeName { get; set; }
        public string CollectionTypeName { get; set; }
        [Obsolete("Must be rename to series alias")]
        public string CollectionAlias { get; set; }
        public string RoomTypeAlias { get; set; }
        public bool IsComplect { get; set; }
        public bool IsFavorite { get; set; }
        public Guid? FavoriteItemCollectionId { get; set; }
    }
}
