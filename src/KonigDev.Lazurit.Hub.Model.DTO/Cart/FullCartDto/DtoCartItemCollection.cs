﻿using System;
using System.Collections.Generic;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cart.FullCartDto
{
    public class DtoCartItemCollection
    {
        public Guid Id { set; get; }

        public Guid CartId { set; get; }

        public int Quantity { set; get; }

        public Guid CollectionVariantId { set; get; }
        public DtoCartCollectionVariantResult CollectionVariant { set; get; }

        public List<DtoCartItemProductInCollection> Products { get; set; }

    }
}
