﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cart.Query
{
    [Serializable]
   public class GetCartIsApprovalRequiredQuery
    {
        public Guid CartId { get; set; }
    }
}
