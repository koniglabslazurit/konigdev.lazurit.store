﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cart.Query
{
    [Serializable]
    public class GetMiniCartByUserIdQuery
    {
        public Guid UserId { set; get; }
    }
}
