﻿using System;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cart.Query
{
    public class GetFullCartByIdQuery
    {
        public Guid CartId { get; set; }
    }
}
