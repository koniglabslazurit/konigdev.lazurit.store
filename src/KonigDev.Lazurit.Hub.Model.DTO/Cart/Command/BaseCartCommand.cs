﻿using KonigDev.Lazurit.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cart.Command
{
    public abstract class BaseCartCommand
    {
        public Guid CartId { set; get; }
        public EnumCartItemType CartItemType { set; get; }
    }
}
