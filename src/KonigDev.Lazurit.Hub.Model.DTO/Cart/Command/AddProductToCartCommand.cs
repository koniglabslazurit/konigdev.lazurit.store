﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cart.Command
{
    public class AddProductToCartCommand
    {
        public Guid ProductId { get; set; }
        public Guid CartId { get; set; }
    }
}
