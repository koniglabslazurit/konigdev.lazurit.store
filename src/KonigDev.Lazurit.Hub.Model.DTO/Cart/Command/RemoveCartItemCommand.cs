﻿using KonigDev.Lazurit.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cart.Command
{
    public class RemoveCartItemCommand : BaseCartCommand
    {
        public Guid RemoveProductId { set; get; }
        public Guid RemoveCollectionId { set; get; }
    }
}
