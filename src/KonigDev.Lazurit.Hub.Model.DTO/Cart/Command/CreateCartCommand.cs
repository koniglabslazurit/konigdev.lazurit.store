﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cart.Command
{
    public class CreateCartCommand
    {
        public Guid UserId { set; get; }
    }
}
