﻿using KonigDev.Lazurit.Hub.Model.DTO.Cart.MiniCartDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cart.Command
{
    [Serializable]
    public class CreateOrUpdateCommand
    {
        public DtoMiniCart NewCart { get; set; }
        public Guid UserId { get; set; }
    }
}
