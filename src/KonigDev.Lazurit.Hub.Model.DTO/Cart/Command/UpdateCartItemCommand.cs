﻿using KonigDev.Lazurit.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cart.Command
{
    public class UpdateCartItemCommand : BaseCartCommand
    {
        public Guid UpdateProductId { set; get; }
        public Guid UpdateCollectionId { set; get; }
        public int Quantity { set; get; }
    }
}
