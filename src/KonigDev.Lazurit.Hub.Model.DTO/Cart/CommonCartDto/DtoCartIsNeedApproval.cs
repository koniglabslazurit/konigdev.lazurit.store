﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto
{
    [Serializable]
    public class DtoCartIsNeedApproval
    {
        public bool IsApprovalRequired { get; set; }
    }
}
