﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto
{
    [Serializable]
    public class DtoCartItem
    {
       public Guid ItemId { set; get; }
        public int Quantity { set; get; }

        public bool IsAssemblyRequired { get; set; }
    }
}
