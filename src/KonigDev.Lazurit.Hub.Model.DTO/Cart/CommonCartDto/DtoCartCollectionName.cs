﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto
{
    public class DtoCartCollectionName
    {
        [Obsolete("Must be rename to series alias")]
        public string CollectionAlias { get; set; }
        public Guid CollectionId { get; set; }
        public string Name { get; set; }
        public string RoomTypeName { get; set; }
    }
}
