﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Favorite.Command
{
    [Serializable]
    public class RemoveFavoriteItemCommand : BaseFavoriteCommand
    {
        public Guid RemoveProductId { set; get; }
        public Guid RemoveCollectionId { set; get; }
    }
}
