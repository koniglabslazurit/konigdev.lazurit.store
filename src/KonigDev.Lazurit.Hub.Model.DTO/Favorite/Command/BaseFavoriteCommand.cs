﻿using KonigDev.Lazurit.Core.Enums;
using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Favorite.Command
{
    [Serializable]
    public abstract class BaseFavoriteCommand
    {
        public Guid UserId { set; get; }
        public EnumFavoriteItemType FavoriteItemType { set; get; }
    }
}
