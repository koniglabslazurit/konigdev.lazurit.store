﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Favorite.Command
{
    [Serializable]
    public class CreateFavoriteCommand
    {
        public Guid UserId { set; get; }
    }
}
