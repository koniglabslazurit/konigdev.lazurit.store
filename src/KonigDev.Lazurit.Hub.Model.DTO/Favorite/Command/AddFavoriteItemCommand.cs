﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Favorite.Command
{
    [Serializable]
    public class AddFavoriteItemCommand : BaseFavoriteCommand
    {
        public Guid AddElementId { set; get; }
    }
}
