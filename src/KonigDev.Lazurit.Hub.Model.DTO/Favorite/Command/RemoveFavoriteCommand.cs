﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Favorite.Command
{
    [Serializable]
    public class RemoveFavoriteCommand
    {
        public Guid FavoriteId { set; get; }
    }
}
