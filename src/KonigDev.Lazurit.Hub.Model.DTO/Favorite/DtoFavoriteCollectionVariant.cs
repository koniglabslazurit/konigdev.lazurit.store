﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Favorite
{
    public class DtoFavoriteCollectionVariant
    {
        [Obsolete("Must be rename to series alias")]
        public string CollectionAlias { get; set; }
        public string CollectionName { get; set; }
        public Guid Id { get; set; }
        public string RoomTypeAlias { get; set; }
        public string RoomTypeName { get; set; }
    }
}
