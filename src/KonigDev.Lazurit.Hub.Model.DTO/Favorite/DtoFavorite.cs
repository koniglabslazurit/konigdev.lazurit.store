﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Favorite
{
    [Serializable]
    public class DtoFavorite
    {
        public DtoFavorite()
        {
            Complects = new List<DtoFavoriteItemComplect>();
            Products = new List<DtoFavoriteItemProduct>();
            Collections = new List<DtoFavoriteItemCollection>();
        }

        public Guid Id { set; get; }

        public IEnumerable<DtoFavoriteItemComplect> Complects { set; get; }
        public IEnumerable<DtoFavoriteItemProduct> Products { set; get; }
        public IEnumerable<DtoFavoriteItemCollection> Collections { get; set; }
    }
}
