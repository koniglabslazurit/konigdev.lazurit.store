﻿using KonigDev.Lazurit.Hub.Model.DTO.Products;
using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Favorite
{
    [Serializable]
    public class DtoFavoriteItemProduct
    {
        public Guid Id { set; get;}
        public DtoFavoriteProduct Product { set; get; }
    }
}
