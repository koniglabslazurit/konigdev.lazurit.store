﻿using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Favorite
{
    [Serializable]
    public class DtoFavoriteItemCollection
    {
        public DtoFavoriteItemCollection()
        {
            Products = new List<DtoFavoriteItemProductInCollection>();
        }

        public Guid Id { set; get; }
        public DtoFavoriteCollectionVariant CollectionVariant { set; get; }
        public IEnumerable<DtoFavoriteItemProductInCollection> Products { get; set; }
    }
}
