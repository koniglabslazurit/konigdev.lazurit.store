﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Favorite
{
    [Serializable]
    public class DtoFavoriteComplect
    {
        public Guid Id { get; set; }
        public string CollectionName { get; set; }
        public string RoomTypeName { get; set; }
        public string CollectionAlias { get; set; }
        public string RoomTypeAlias { get; set; }

        public IEnumerable<DtoFavoriteComplectProductItem> Products { get; set; }
    }
}
