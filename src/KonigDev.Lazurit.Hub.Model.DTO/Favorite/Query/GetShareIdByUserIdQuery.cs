﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Favorite.Query
{
    public class GetShareIdByUserIdQuery
    {
        public Guid UserId { set; get; }
    }
}
