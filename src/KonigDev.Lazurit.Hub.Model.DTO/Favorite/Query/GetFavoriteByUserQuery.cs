﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Favorite.Query
{
    public class GetFavoriteByUserQuery
    {
        public Guid UserId { set; get; }
    }
}
