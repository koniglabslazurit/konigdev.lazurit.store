﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Favorite
{
    [Serializable]
    public class DtoFavoriteItemComplect
    {
        public Guid Id { set; get; }
        public DtoFavoriteComplect CollectionVariant { set; get; }
    }
}
