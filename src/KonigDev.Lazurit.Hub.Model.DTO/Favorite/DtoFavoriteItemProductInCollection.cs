﻿using KonigDev.Lazurit.Hub.Model.DTO.Products;
using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Favorite
{
    [Serializable]
    public class DtoFavoriteItemProductInCollection
    {
        public Guid Id { set; get; }
        public DtoFavoriteProductInCollection Product { set; get; }
    }
}
