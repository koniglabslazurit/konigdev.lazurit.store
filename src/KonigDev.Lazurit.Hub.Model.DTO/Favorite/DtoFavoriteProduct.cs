﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Favorite
{
    [Serializable]
    public class DtoFavoriteProduct
    {
        public string Article { get; set; }
        [Obsolete("Must be rename to series alias")]
        public string CollectionAlias { get; set; }
        public string CollectionName { get; set; }
        public string FacingColorName { get; set; }
        public string FacingName { get; set; }
        public string FrameColorIdName { get; set; }
        public string FurnitureType { get; set; }
        public int Height { get; set; }
        public Guid Id { get; set; }
        public int Length { get; set; }
        public string SyncCode1C { get; set; }
        public int Width { get; set; }
        public string FurnitureTypeAlias { get; set; }
    }
}
