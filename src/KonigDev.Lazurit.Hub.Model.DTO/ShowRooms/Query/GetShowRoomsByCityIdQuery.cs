﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.ShowRooms.Query
{
    [Serializable]
    [DataContract]
    public class GetShowRoomsByCityIdQuery
    {
        public Guid CityId { get; set; }
    }
}
