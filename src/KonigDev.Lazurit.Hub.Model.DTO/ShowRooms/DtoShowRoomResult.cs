﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.ShowRooms
{
    [Serializable]
    public class DtoShowRoomResult
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
    }
}
