﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.ShowRooms.Dto
{
    [Serializable]
    public class DtoShowRoomItem
    {
        public Guid Id { get; set; }
        public string SyncCode1C { get; set; }
    }
}
