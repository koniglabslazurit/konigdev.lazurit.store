﻿using KonigDev.Lazurit.Hub.Model.DTO.Products;
using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Layaways
{
    public class DtoLayawayItemProduct
    {
        public Guid Id { set; get; }

        public DtoProductResult Product { get; set; }

        public Guid CollectionId { get; set; }

        public Guid ProductId { set; get; }
        public Guid LayawayId { set; get; }

        public bool IsAssemblyRequired { get; set; }
        public int Quantity { set; get; }
    }
}
