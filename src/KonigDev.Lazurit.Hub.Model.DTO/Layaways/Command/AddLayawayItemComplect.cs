﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Layaways.Command
{
   public class AddLayawayItemComplect
    {
        public Guid CollectionVariantId { get; set; }
        public Guid ItemId { get; set; }
        public bool IsAssemblyRequired { get; set; }
        public int Quantity { get; set; }
    }
}
