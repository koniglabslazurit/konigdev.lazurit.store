﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Layaways.Command
{
    public class AddLayawayItemCollection
    {
        public AddLayawayItemCollection()
        {
            ProductsInCollection = new HashSet<AddLayawayItemProductInCollection>();
        }
        public Guid CollectionCartItemId { get; set; }
        public ICollection<AddLayawayItemProductInCollection> ProductsInCollection { get; set; }
        public int Quantity { get; set; }
    }
}