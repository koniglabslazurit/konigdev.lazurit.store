﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Layaways.Command
{
    [Serializable]
    public class AddLayawayItemCommand
    {
        public AddLayawayItemCommand()
        {
            Products = new HashSet<AddLayawayItemProduct>();
            Complects = new HashSet<AddLayawayItemComplect>();
            Collections = new HashSet<AddLayawayItemCollection>();
        }
        public Guid? UserId { set; get; }

        public Guid CartId;
        public Guid NewLayawayId;
        public List<Guid> LayawayIds;
        /// <summary>
        /// признак того, отложена ли целая корзина
        /// </summary>
        public bool IsWholeCart { set; get; }

        public virtual ICollection<AddLayawayItemProduct> Products { set; get; }
        public virtual ICollection<AddLayawayItemComplect> Complects { set; get; }
        public virtual ICollection<AddLayawayItemCollection> Collections { set; get; }
    }
}
