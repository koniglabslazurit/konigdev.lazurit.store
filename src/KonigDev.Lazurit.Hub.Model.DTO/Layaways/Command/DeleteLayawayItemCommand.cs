﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Layaways.Command
{
   public class DeleteLayawayItemCommand
    {
        public Guid LayawayId { get; set; }
        public Guid CollectionItemLayawayId { get; set; }
        public Guid ComplectItemLayawayId { get; set; }
        public Guid ProductItemLayawayId { get; set; }
        public Guid ProductInCollectionId { get; set; }
    }
}
