﻿using KonigDev.Lazurit.Hub.Model.DTO.Cart.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Layaways.Command
{
    [Serializable]
    public class RemoveLayawayItemCommand
    {
        public RemoveLayawayItemCommand()
        {
            Products = new HashSet<RemoveLayawayItemProduct>();
            Complects = new HashSet<RemoveLayawayItemComplect>();
            Collections = new HashSet<RemoveLayawayItemCollection>();
        }
        /// <summary>
        /// id отложенного
        /// </summary>
        public Guid Id { set; get; }
        public Guid CartId;
        public DateTime CreationTime { get; set; }

        public virtual ICollection<RemoveLayawayItemProduct> Products { set; get; }
        public virtual ICollection<RemoveLayawayItemComplect> Complects { set; get; }
        public virtual ICollection<RemoveLayawayItemCollection> Collections { set; get; }
    }
}
