﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Layaways.Command
{
   public class RemoveLayawayItemCollection
    {
        public RemoveLayawayItemCollection()
        {
            ProductsInCollection = new HashSet<RemoveLayawayItemProductInCollection>();
        }
        public Guid LayawayItemCollectionId { get; set; }
        public Guid CollectionVarinatId { get; set; }
        public ICollection<RemoveLayawayItemProductInCollection> ProductsInCollection { get; set; }
        public int Quantity { get; set; }
    }
}
