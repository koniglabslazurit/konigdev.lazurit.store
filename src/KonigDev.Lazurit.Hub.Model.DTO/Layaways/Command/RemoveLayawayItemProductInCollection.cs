﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Layaways.Command
{
    public class RemoveLayawayItemProductInCollection
    {
        public Guid Id { set; get; }
        public Guid ProductId { set; get; }
        public bool IsAssemblyRequired { get; set; }
        public int Quantity { get; set; }
    }
}
