﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Layaways
{
   public class DtoLayawayItemCollection
    {
        public DtoLayawayItemCollection()
        {
            ProductsInCollection = new HashSet<DtoLayawayItemProductInCollection>();
        }
        public Guid Id { set; get; }

        public Guid CollectionVarinatId { set; get; }
        public Guid LayawayId { set; get; }
        public string CollectionName { get; set; }
        public string RoomTypeName { get; set; }

        public int Quantity { set; get; }

        public ICollection<DtoLayawayItemProductInCollection> ProductsInCollection { set; get; }
    }
}
