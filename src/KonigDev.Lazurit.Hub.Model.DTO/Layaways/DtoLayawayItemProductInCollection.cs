﻿using KonigDev.Lazurit.Hub.Model.DTO.Products;
using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Layaways
{
   public class DtoLayawayItemProductInCollection
    {
        public Guid Id { set; get; }
        public DtoProductResult Product { get; set; }
        public Guid ProductId { set; get; }
        public Guid LayawayItemCollectionId { set; get; }
        public bool IsAssemblyRequired { get; set; }
        public int Quantity { set; get; }
    }
}
