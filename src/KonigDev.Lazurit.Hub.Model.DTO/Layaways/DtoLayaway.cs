﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Layaways
{
   public class DtoLayaway
    {
        public Guid Id { get; set; }

        public Guid? UserId { set; get; } 
        public bool IsWholeCart { set; get; }

        public DateTime CreationTime { get; set; }

        public virtual ICollection<DtoLayawayItemProduct> Products { set; get; }
        public virtual ICollection<DtoLayawayItemComplect> Complects { set; get; }
        public virtual ICollection<DtoLayawayItemCollection> Collections { set; get; }
    }
}
