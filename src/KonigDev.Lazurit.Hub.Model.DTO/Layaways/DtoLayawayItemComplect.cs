﻿using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Layaways
{
   public class DtoLayawayItemComplect
    {
        public Guid Id { set; get; }

        public Guid CollectionVarinatId { set; get; }
        public Guid LayawayId { set; get; }

        public string CollectionName { get; set; }
        public string RoomTypeName { get; set; }

        public bool IsAssemblyRequired { get; set; }
        public int Quantity { set; get; }

        public DtoCollectionVariantResult CollectionVariant { set; get; }
    }
}
