﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Layaways.Query
{
    public class GetLayawaysByLayawaysIdsQuery
    {
        public List<Guid> LayawayIds;
    }
}
