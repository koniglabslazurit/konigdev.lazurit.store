﻿using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto.DtoIntegrationLoadings;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands
{
    [Serializable]
    public class CreateIntegrationLoadingCommand
    {
        public CreateIntegrationLoadingCommand()
        {
            Items = new List<DtoIntegrationLoadingCreateItem>();
        }
        public Guid Id { get; set; }

        public DateTime DateLoad { get; set; }
        public string FileName { get; set; }
        public string Type { get; set; }
        public string Descr { get; set; }

        public List<DtoIntegrationLoadingCreateItem> Items { get; set; }
    }
}
