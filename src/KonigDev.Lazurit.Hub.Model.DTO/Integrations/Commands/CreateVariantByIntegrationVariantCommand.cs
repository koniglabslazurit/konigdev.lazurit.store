﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands
{
    [Serializable]
    public class CreateVariantByIntegrationVariantCommand
    {
        public Guid Id { get; set; }
    }
}
