﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands
{
    [Serializable]
    public class CreateIntegrationProductContentCommand
    {
        public Guid Id { get; set; }
        public Guid IntegrationCollectionVariantId { get; set; }
        public Guid ProductId { get; set; }
    }
}
