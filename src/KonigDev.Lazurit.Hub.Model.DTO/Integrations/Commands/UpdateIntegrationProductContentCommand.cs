﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands
{
    [Serializable]
    public class UpdateIntegrationProductContentCommand
    {
        public Guid Id { get; set; }
        public int Left { get; set; }
        public int Top { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
}
