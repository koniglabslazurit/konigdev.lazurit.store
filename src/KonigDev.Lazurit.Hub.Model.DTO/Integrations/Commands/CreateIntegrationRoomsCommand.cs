﻿using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands
{
    [Serializable]
    public class CreateIntegrationRoomsCommand
    {
        public List<DtoIntegrationRoomItem> Rooms { get; set; }
    }
}
