﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands
{
    [Serializable]
    public class UpdateIntegrationVariantCommand
    {
        public Guid CollectionVariantId { get; set; }
    }
}
