﻿using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto.Creating;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands
{
    [Serializable]
    public class CreateIntegrationCollectionVariantsCommand
    {
        public List<DtoIntegrationCollectionVariantCreating> CollectionsVariants { get; set; }
    }
}
