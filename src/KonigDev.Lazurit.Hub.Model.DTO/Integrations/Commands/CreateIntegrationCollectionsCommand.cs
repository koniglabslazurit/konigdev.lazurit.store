﻿using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto.Creating;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands
{
    [Serializable]
    public class CreateIntegrationCollectionsCommand
    {
        public List<DtoIntegrationCollectionCreating> Collections { get; set; }
    }
}
