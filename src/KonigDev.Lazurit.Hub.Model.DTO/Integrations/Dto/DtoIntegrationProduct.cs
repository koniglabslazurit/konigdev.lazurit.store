﻿using System;
using KonigDev.Lazurit.Core.Enums.Enums;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto
{
    [Serializable]
    public class DtoIntegrationProduct
    {
        public string SyncCode1C { get; set; }
        public string Hash { get; set; }

        /// <summary>
        /// Статус продукта, показывающий его необходимость валидации,публикации и т.д.
        /// </summary>
        public string IntegrationStatus { get; set; }
    }
}
