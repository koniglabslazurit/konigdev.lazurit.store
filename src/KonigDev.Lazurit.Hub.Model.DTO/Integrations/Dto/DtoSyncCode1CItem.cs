﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto
{
   public class DtoSyncCode1CItem
    {
        public string Name { get; set; }
    }
}
