﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto
{
    [Serializable]
    public class DtoIntegrationCollectionWithContent
    {
        public Guid Id { get; set; }
        public string RoomType { get; set; }
        public string SeriesName { get; set; }
        public List<DtoIntegrationCollectionVariantContent> IntegrationVariants { get; set; }
    }
}
