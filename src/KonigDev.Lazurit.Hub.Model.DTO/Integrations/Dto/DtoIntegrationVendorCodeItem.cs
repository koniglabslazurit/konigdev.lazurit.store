﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto
{
    [Serializable]
    public class DtoIntegrationVendorCodeItem
    {
        public Guid FurnitureTypeId { get; set; }
        public Guid Id { get; set; }
        public string Range { get; set; }
        public string SyncCode1C { get; set; }
    }
}
