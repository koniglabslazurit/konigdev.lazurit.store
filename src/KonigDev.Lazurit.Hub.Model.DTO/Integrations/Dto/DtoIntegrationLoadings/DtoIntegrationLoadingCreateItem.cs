﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto.DtoIntegrationLoadings
{
    [Serializable]
    public class DtoIntegrationLoadingCreateItem
    {
        public Guid Id { get; set; }
        public Guid IntegrationLoadingId { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
    }
}
