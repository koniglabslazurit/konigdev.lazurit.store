﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto
{
    [Serializable]
    public class DtoIntegrationProductContent
    {
        public Guid ProductId { get; set; }
        public string SyncCode1C { get; set; }
        public Guid? ObjectId { get; set; }
        public bool IsEnabled { get; set; }
        public int ContentLeft { get; set; }
        public int ContentTop { get; set; }
        public int ContentWidth { get; set; }
        public int ContentHeight { get; set; }
        public string FurnitureType { get; set; }
        public string FurnitureForm { get; set; }
        public string FrameColor { get; set; }
        public string Facing { get; set; }
        public string FacingColor { get; set; }
    }
}
