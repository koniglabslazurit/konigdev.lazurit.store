﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto.Products
{
    [Serializable]
    public class DtoIntegrationProductResponse : DtoCommonTableResponse<DtoIntegrationProductItemResponse>
    {

    }
}
