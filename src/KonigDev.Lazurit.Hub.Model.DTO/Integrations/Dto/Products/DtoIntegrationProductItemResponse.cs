﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto.Products
{
    [Serializable]
    public class DtoIntegrationProductItemResponse
    {
        /// <summary>
        /// Ид продукта
        /// </summary>
        public Guid ProductId { get; set; }

        /// <summary>
        /// Статус продукта - (опубликован, черновик, валидация)
        /// </summary>
        public string IntegrationStatus { get; set; }

        /// <summary>
        /// ТипАртикула
        /// </summary>
        public string FurnitureType { get; set; }

        /// <summary>
        /// Артикул
        /// </summary>
        public string VendorCode { get; set; }

        /// <summary>
        /// Цвет корпуса, ЦветКорпуса
        /// </summary>
        public string FrameColor { get; set; }

        /// <summary>
        /// Отделка, ВариантОтделкиИзделия
        /// </summary>
        public string Facing { get; set; }

        /// <summary>
        /// Цвет отделки, ЦветФасадаИзделия
        /// </summary>
        public string FacingColor { get; set; }

        /// <summary>
        /// Код1с
        /// </summary>
        public string SyncCode1C { get; set; }

        /// <summary>
        /// Подсветка
        /// </summary>
        public string Backlight { get; set; }

        /// <summary>
        /// МатериалОбивки
        /// </summary>
        public string Material { get; set; }

        /// <summary>
        /// ЛевыйПравый
        /// </summary>
        public string LeftRight { get; set; }

        /// <summary>
        /// ФормаАртикула
        /// </summary>
        public string FurnitureForm { get; set; }       

        /// <summary>
        /// Высота
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// Ширина
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Длина
        /// </summary>
        public int Length { get; set; }

        /// <summary>
        /// Механизм
        /// </summary>
        public string Mechanism { get; set; }

        /// <summary>
        /// ДопустимаяНагрузкаНаКровать
        /// </summary>
        public string PermissibleLoad { get; set; }

        /// <summary>
        /// РазмерСпальногоМестаШирина
        /// </summary>
        public string SleepingAreaWidth { get; set; }

        /// <summary>
        /// РазмерСпальногоМестаДлина
        /// </summary>
        public string SleepingAreaLength { get; set; }

        /// <summary>
        /// МаркетинговоеОписание
        /// </summary>
        public string Descr { get; set; }

        /// <summary>
        /// ТипЦА
        /// </summary>
        public List<string> TargetTypes { get; set; }

        /// <summary>
        /// Стиль
        /// </summary>
        public List<string> Styles { get; set; }        

        /// <summary>
        /// Гарантия
        /// </summary>
        public string Warranty { get; set; }       

        /// <summary>
        /// Карнизы
        /// </summary>
        public string ProductPartMarker { get; set; }

        /// <summary>
        /// Размер комнаты
        /// </summary>
        public string RoomSize { get; set; }

        /// <summary>
        /// Количество спальных мест
        /// </summary>
        public short SleepingAreaAmount { get; set; }

        /// <summary>
        /// Код ссответствия
        /// </summary>
        public string SyncCode1CConformity { get; set; }

        /// <summary>
        /// Признак разобранного вида
        /// </summary>
        public bool IsDisassemblyState { get; set; }

        /// <summary>
        /// Является ли товар дефолтным в артикуле
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// Маркетинговый артикул
        /// </summary>
        public string MarketerVendorCode { get; set; }

        /// <summary>
        /// Маркетинговый вес
        /// </summary>
        public string MarketerRange { get; set; }
    }
}
