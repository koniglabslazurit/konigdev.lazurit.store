﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto.Products
{
    [Serializable]
    public class DtoPublishedProductResponse: DtoCommonTableResponse<DtoPublishedProductItem>
    {
       
    }

    [Serializable]
    public class DtoPublishedProductItem
    {
        public string VendorCode { get; set; }
        public string FurnitureType { get; set; }

        public IEnumerable<DtoIntegrationProductItemResponse> Products { get; set; }
    }
}
