﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto.Products
{
    /// <summary>
    /// Модель с результатом провекри валидации возможности публикации продукта
    /// </summary>
    [Serializable]    
    public class DtoIntegrationProductValidation
    {
        public DtoIntegrationProductValidation()
        {
            Errors = new List<string>();
        }

        public bool IsValid { get; set; }
        public List<string> Errors { get; set; }
    }
}

