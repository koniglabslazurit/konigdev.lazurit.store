﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto
{
    [Serializable]
    public class DtoIntegrationCollectionItem
    {
        public Guid Id { get; set; }
        public string Series { get; set; }
        public string Room { get; set; }
        public Guid RoomId { get; set; }
        public Guid SeriesId { get; set; }
        public Guid? OriginalId { get; set; }
    }
}
