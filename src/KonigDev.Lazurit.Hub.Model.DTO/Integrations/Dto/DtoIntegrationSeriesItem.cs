﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto
{
    [Serializable]
    public class DtoIntegrationSeriesItem
    {
        public Guid Id { get; set; }
        public string SyncCode1C { get; set; }
        public Guid? OriginalId { get; set; }
    }
}
