﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto.Creating
{
    [Serializable]
    public class DtoIntegrationCollectionCreating
    {
        public Guid Id { get; set; }
        public Guid RoomId { get; set; }
        public Guid SeriesId { get; set; }
    }
}
