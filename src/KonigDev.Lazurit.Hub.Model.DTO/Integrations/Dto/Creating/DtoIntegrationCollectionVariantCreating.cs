﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto.Creating
{
    [Serializable]
    public class DtoIntegrationCollectionVariantCreating
    {
        public Guid Id { get; set; }
        public Guid CollectionId { get; set; }
        public string Facing { get; set; }
        public string FacingColor { get; set; }
        public string FrameColor { get; set; }
        public bool IsComplekt { get; set; }
        public string SyncCode1C { get; set; }
    }
}
