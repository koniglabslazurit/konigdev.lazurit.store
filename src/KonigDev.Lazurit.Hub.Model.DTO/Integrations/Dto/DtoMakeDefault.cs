﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto
{
    public class DtoMakeDefault
    {
        public Guid VariantId { get; set; }
    }
}
