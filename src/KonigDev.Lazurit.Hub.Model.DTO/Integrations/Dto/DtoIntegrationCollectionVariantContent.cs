﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto
{
    [Serializable]
    public class DtoIntegrationCollectionVariantContent
    {
        public Guid Id { get; set; }
        public Guid CollectionId { get; set; }
        public Guid? OriginalId { get; set; }
        public Guid FacingId { get; set; }
        public string Facing { get; set; }
        public Guid FacingColorId { get; set; }
        public string FacingColor { get; set; }
        public Guid FrameColorId { get; set; }
        public string FrameColor { get; set; }
        public string CollectionTypeName { get; set; }
        public bool IsComplect { get; set; }
        public bool IsDefault { get; set; }
        public bool IsPublished { get; set; }
        public List<DtoIntegrationProductContent> IntegrationProducts { get; set; }
    }
}
