﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto
{
    [Serializable]
    public class DtoIntegrationProductsCount
    {
        public string SyncCode1C { get; set; }
        public string Status { get; set; }
        public string StatusName { get; set; }
    }
}
