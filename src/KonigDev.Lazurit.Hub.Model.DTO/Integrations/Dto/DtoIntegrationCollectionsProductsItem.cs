﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto
{
    [Serializable]
    public class DtoIntegrationCollectionsProductsItem
    {
        public Guid Id { get; set; }
        public Guid? OriginalId { get; set; }
        public string Series { get; set; }
        public string Room { get; set; }
        public int ValidationProducts { get; set; }
        public int TemplateProducts { get; set; }
        public int PublishedProducts { get; set; }
    }
}
