﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto
{
    [Serializable]
    public class DtoIntegrationCollectionVariantItem
    {
        public Guid Id { get; set; }
        public Guid CollectionId { get; set; }
        public string Room { get; set; }
        public string Series { get; set; }
        public string Facing { get; set; }
        public string FacingColor { get; set; }
        public string FrameColor { get; set; }
        public bool IsComplekt { get; set; }
        public string SyncCode1C { get; set; }
        public bool IsNew { get; set; }
    }
}
