﻿using System;
using KonigDev.Lazurit.Core.Enums.Enums;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto
{
    [Serializable]
    public class DtoIntegrationProductCreating : DtoIntegrationProductItem
    {
        public Guid CollectionId { get; set; }
        public List<Guid> CollectionVariants { get; set; }
        public Guid ProductId { get; set; }
        public List<string> Styles { get; set; }
        public List<string> TargetAudiences { get; set; }
    }

    [Serializable]
    public class DtoIntegrationProductItem
    {
        /// <summary>
        /// 
        /// </summary>
        public string Hash { get; set; }

        /// <summary>
        /// Статус продукта - новый, обновлен, не требует обновления
        /// </summary>
        public EnumIntegrationProductStatus IntegrationStatus { get; set; }
        
        /// <summary>
        /// ТипАртикула
        /// </summary>
        public string FurnitureType { get; set; }

        /// <summary>
        /// Артикул
        /// </summary>
        public string VendorCode { get; set; }

        /// <summary>
        /// МаркетинговыйВес
        /// </summary>        
        public string MarketerRange { get; set; }

        /// <summary>
        /// МаркетинговыйВес
        /// </summary>
        public string MarketerVendorCode { get; set; }

        /// <summary>
        /// ТипКомнаты
        /// </summary>
        public string RoomType { get; set; }

        /// <summary>
        /// Серия
        /// </summary>
        public string Series { get; set; }

        /// <summary>
        /// Коллекция
        /// </summary>
        public string Collection { get; set; }

        /// <summary>
        /// СписокАртикуловКомплекта
        /// </summary>
        public string VendorCodeList { get; set; }

        /// <summary>
        /// Цвет корпуса, ЦветКорпуса
        /// </summary>
        public string FrameColor { get; set; }

        /// <summary>
        /// Отделка, ВариантОтделкиИзделия
        /// </summary>
        public string Facing { get; set; }

        /// <summary>
        /// Цвет отделки, ЦветФасадаИзделия
        /// </summary>
        public string FacingColor { get; set; }

        /// <summary>
        /// Код1с
        /// </summary>
        public string SyncCode1C { get; set; }

        /// <summary>
        /// Подсветка
        /// </summary>
        public string Backlight { get; set; }

        /// <summary>
        /// МатериалОбивки
        /// </summary>
        public string Material { get; set; }

        /// <summary>
        /// ЛевыйПравый
        /// </summary>
        public string LeftRight { get; set; }

        /// <summary>
        /// ФормаАртикула
        /// </summary>
        public string FurnitureForm { get; set; }

        /// <summary>
        /// НаличиеОбязательностьКарнизов
        /// </summary>
        public string ProductPartMarker { get; set; }

        /// <summary>
        /// Высота
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// Ширина
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Длина
        /// </summary>
        public int Length { get; set; }

        /// <summary>
        /// Механизм
        /// </summary>
        public string Mechanism { get; set; }

        /// <summary>
        /// ДопустимаяНагрузкаНаКровать
        /// </summary>
        public string PermissibleLoad { get; set; }

        /// <summary>
        /// РазмерСпальногоМестаШирина
        /// </summary>
        public string SleepingAreaWidth { get; set; }

        /// <summary>
        /// РазмерСпальногоМестаДлина
        /// </summary>
        public string SleepingAreaLength { get; set; }

        /// <summary>
        /// МаркетинговоеОписание
        /// </summary>
        public string Descr { get; set; }

        /// <summary>
        /// ТипЦА
        /// </summary>
        public string TargetAudience { get; set; }

        /// <summary>
        /// Стиль
        /// </summary>
        public string Style { get; set; }

        /// <summary>
        /// КолУпаковок
        /// </summary>
        public string Packing { get; set; }

        /// <summary>
        /// Гарантия
        /// </summary>
        public string Warranty { get; set; }
    }
}
