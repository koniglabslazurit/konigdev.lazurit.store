﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto.Collections
{
    [Serializable]
    public class DtoIntegrationCollectionTableResponse : DtoCommonTableResponse<DtoIntegrationCollectionsProductsItem>
    {

    }
}
