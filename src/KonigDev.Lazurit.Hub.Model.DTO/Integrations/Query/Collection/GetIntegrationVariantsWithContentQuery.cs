﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query.Collection
{
    [Serializable]
    public class GetIntegrationVariantsWithContentQuery
    {
        public Guid CollectionId { get; set; }
    }
}
