﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query
{
   public class GetSyncCode1CsQuery
    {
        public Guid IntegrationCollectionId { get; set; }
    }
}
