﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query
{
    [Serializable]
    public class GetPublishedIntegrationProductsByVendorCodeQuery : DtoCommonTableRequest
    {
        public string VendorCode { get; set; }
    }
}
