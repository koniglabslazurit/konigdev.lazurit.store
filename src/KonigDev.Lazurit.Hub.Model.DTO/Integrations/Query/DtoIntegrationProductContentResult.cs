﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query
{
    [Serializable]
    public class DtoIntegrationProductContentResult
    {
        public Guid Id { get; set; }
        public string Article { get; set; }
        public Guid? ObjectId { get; set; }
        public bool IsEnabled { get; set; }
        public int Left { get; set; }
        public int Top { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
}
