﻿using System;
using System.Runtime.Serialization;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query
{
    [Serializable]
    [DataContract]
    public class GetIntegrationProductsContextByCollectionVariantQuery
    {
        public Guid IntegrationCollectionVariantId { get; set; }
        public Guid? UserId { get; set; }
    }
}
