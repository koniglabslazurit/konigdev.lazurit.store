﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query
{
    /// <summary>
    /// Запрос на получение товаров из интеграционных таблиц
    /// </summary>
    [Serializable]
    public class GetIntegrationProductByRequestQuery : DtoCommonTableRequest
    {
        /// <summary>
        /// Статус товара для отбора.
        /// <see cref="KonigDev.Lazurit.Core.Enums.Enums.EnumIntegrationStatus"/>
        /// </summary>
        public string ProductStatus { get; set; }

        /// <summary>
        /// Вендор кода товар (артикул) для отбора
        /// </summary>
        public string VendorCode { get; set; }

        /// <summary>
        /// Коллекция, в которую входит товар
        /// </summary>
        public Guid IntegrationCollectionId { get; set; }

        public string FurnitureType { get; set; }

        public string Facing { get; set; }
        public string FacingColor { get; set; }
        public string FrameColor { get; set; }

        public string SyncCode1C { get; set; }
    }
}
