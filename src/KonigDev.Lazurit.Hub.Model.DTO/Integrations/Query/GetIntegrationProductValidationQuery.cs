﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query
{
    [Serializable]
    public class GetIntegrationProductValidationQuery
    {
        public string SyncCode1C { get; set; }
    }
}
