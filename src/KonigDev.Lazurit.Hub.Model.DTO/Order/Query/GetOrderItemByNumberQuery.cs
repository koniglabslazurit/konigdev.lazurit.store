﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Order.Query
{
    public class GetOrderItemByNumberQuery
    {
        public long Number { get; set; }
    }
}
