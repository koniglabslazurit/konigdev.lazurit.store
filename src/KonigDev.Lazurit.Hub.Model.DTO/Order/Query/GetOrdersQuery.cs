﻿using KonigDev.Lazurit.Hub.Model.DTO.Order.Dto;
using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Order.Query
{
    [Serializable]
    public class GetOrdersQuery : DtoCommonTableRequest
    {
        public Guid UserProfileId { get; set; }
        public bool? IsPaid { get; set; }
        public bool? IsApprovedBySeller { get; set; }
    }
}
