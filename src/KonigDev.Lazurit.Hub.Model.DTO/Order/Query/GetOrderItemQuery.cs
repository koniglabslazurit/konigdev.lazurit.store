﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Order.Query
{
    public class GetOrderItemQuery
    {
        public Guid Id { get; set; }
    }
}
