﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Order.Query
{
    public class GetOrderQuery
    {
        public Guid Id { get; set; }
    }
}
