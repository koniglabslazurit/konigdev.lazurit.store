﻿using KonigDev.Lazurit.Hub.Model.DTO.Order.Dto;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Order.Command
{
    [Serializable]
    public class CreateOrderCommand
    {
        public CreateOrderCommand()
        {
            Products = new List<DtoOrderProductCreating>();
            Complects = new List<DtoOrderComplectCreating>();
            Collections = new List<DtoOrderCollectionCreating>();
        }

        public Guid UserProfileId { get; set; }
        public Guid Id { get; set; }
        public decimal Sum { get; set; }
        public DateTime CreationTime { get; set; }
        public bool IsApprovedBySeller { get; set; }
        public bool IsDeliveryRequired { get; set; }

        public List<DtoOrderProductCreating> Products { get; set; }
        public List<DtoOrderComplectCreating> Complects { get; set; }
        public List<DtoOrderCollectionCreating> Collections { get; set; }
        public Guid PaymentMethodId { get; set; }
        public string DeliveryKladrCode { get; set; }
        public string DeliveryAddress { get; set; }
    }
}
