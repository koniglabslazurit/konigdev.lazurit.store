﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Order.Command
{
    [Serializable]
    public class UpdateOrder1CFieldsCommand
    {
        public Guid Id { get; set; }
        public DateTime? CreationTime1C { get; set; }
        public string OrderNumber1C { get; set; }
    }
}

