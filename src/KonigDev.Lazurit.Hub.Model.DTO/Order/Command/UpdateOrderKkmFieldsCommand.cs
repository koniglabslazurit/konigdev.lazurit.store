﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Order.Command
{
    [Serializable]
    public class UpdateOrderKkmFieldsCommand
    {
        public string InvId { get; set; }
        public string KkmOrderRecordUrl { get; set; }
    }
}