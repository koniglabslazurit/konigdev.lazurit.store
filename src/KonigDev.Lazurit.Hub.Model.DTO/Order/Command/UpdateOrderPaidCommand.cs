﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Order.Command
{
    [Serializable]
    public class UpdateOrderPaidCommand
    {
        public Guid Id { get; set; }
        public bool IsPaid { get; set; }
    }
}
