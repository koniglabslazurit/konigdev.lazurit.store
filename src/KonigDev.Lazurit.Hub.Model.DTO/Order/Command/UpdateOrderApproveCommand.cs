﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Order.Command
{
    [Serializable]
    public class UpdateOrderApproveCommand
    {
        public Guid Id { get; set; }
        public bool IsApproved { get; set; }
    }
}
