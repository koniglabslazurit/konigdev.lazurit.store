﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Order.Dto
{
    public class DtoOrderItem
    {
        public Guid Id { get; set; }
        public decimal Sum { get; set; }
        public long Number { get; set; }
        public bool IsPaid { get; set; }
        public string PaymentMethod { get; set; }
    }
}
