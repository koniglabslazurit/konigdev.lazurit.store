﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Order.Dto
{
    [Serializable]
    public class DtoOrderCollection
    {
        public int Quantity { set; get; }
        public string SeriaName { get; set; }
        //TODO у серии отсутствует алиас - добавить после обнолвения бд для серии!!
        public string SeriaAlias { get; set; }
        public string RoomName { get; set; }
        public List<DtoOrderProductInCollection> Products { get; set; }
    }
}
