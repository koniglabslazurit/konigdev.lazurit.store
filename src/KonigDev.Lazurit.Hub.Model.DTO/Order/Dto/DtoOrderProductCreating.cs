﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Order.Dto
{
    [Serializable]
    public class DtoOrderProductCreating
    {
        public Guid Id { set; get; }
        public decimal Price { get; set; }
        public byte Discount { get; set; }
        public int Quantity { set; get; }
        public bool IsAssemblyRequired { get; set; }
        public Guid ProductId { set; get; }
    }
}
