﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Order.Dto
{
    [Serializable]
    public class DtoOrderProductInCollection
    {
        public Guid Id { get; set; }
        public string SyncCode1C { get; set; }
        public bool IsAssemblyRequired { get; set; }
        public string FurnitureType { get; set; }       
        public string Article { get; set; }
        
        public decimal Price { get; set; }
        public byte Discount { get; set; }
        public int Quantity { set; get; }
    }
}
