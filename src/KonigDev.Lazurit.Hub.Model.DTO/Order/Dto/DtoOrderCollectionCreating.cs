﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Order.Dto
{
    [Serializable]
    public class DtoOrderCollectionCreating
    {
        public Guid Id { get; set; }
        public int Quantity { set; get; }
        public Guid CollectionVariantId { set; get; }
        public List<DtoOrderProductCreating> Products { set; get; }
    }
}
