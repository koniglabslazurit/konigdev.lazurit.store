﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Order.Dto
{
    [Serializable]
    public class DtoOrder
    {
        public Guid UserProfileId { get; set; }
        public Guid Id { get; set; }
        public decimal Sum { get; set; }
        public DateTime CreationTime { get; set; }
        public bool IsApprovedBySeller { get; set; }
        public bool IsDeliveryRequired { get; set; }
        public DateTime? CreationTime1C { get; set; }
        public string OrderNumber1C { get; set; }
        
        public List<DtoOrderProduct> Products { get; set; }
        public List<DtoOrderComplect> Complects { get; set; }
        public List<DtoOrderCollection> Collections { get; set; }
    }
}
