﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Order.Dto
{
    [Serializable]
    public class DtoOrderComplect
    {
        public Guid Id { get; set; }
        public bool IsAssemblyRequired { get; set; }
        public string SeriaName { get; set; }
        public string RoomName { get; set; }
        public decimal Price { get; set; }
        public byte Discount { get; set; }
        public int Quantity { set; get; }
    }
}
