﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.FacingColors
{
    [Serializable]
    public class DtoFacingColorResult
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
