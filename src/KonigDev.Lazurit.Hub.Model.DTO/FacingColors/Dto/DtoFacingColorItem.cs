﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Dto
{
    [Serializable]
    public class DtoFacingColorItem
    {
        public Guid Id { get; set; }
        public bool IsUniversal { get; set; }
        public string Name { get; set; }
        public string SyncCode1C { get; set; }
    }
}
