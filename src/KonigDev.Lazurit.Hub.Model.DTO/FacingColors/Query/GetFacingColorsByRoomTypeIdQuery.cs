﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Query
{
   public class GetFacingColorsByRoomTypeIdQuery
    {
        public Guid RoomTypeId { get; set; }
    }
}
