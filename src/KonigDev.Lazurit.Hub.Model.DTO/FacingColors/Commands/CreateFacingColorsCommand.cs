﻿using KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Dto;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.FacingColors.Commands
{
    [Serializable]
    public class CreateFacingColorsCommand
    {
        public List<DtoFacingColorItem> FacingColors { get; set; }
    }
}
