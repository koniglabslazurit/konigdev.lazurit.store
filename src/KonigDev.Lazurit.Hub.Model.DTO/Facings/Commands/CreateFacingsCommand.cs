﻿using KonigDev.Lazurit.Hub.Model.DTO.Facings.Dto;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Facings.Commands
{
    [Serializable]
    public class CreateFacingsCommand
    {
        public List<DtoFacingItem> Facings { get; set; }
    }
}
