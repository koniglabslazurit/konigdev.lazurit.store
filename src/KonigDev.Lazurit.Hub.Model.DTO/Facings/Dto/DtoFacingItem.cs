﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Facings.Dto
{
    [Serializable]
    public class DtoFacingItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string SyncCode1C { get; set; }
    }
}
