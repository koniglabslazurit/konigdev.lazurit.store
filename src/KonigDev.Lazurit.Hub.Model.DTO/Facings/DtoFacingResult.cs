﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Facings
{
    [Serializable]
    public class DtoFacingsResult
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
