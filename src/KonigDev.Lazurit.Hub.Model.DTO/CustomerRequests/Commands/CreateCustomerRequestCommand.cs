﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.CustomerRequests.Commands
{
    [Serializable]
    public class CreateCustomerRequestCommand
    {
        /// <summary>
        /// Customer full name
        /// </summary>
        /// <remarks>
        /// ФИО
        /// </remarks>
        public string CustomerFullName { get; set; }

        /// <summary>
        /// Customer email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Customer phone number
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Request type
        /// </summary>
        public string Type { get; set; }
    }
}
