﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.CustomerRequests.Commands
{
    [Serializable]
    public class RemoveCustomerRequestCommand
    {
        /// <summary>
        /// Removing request Id
        /// </summary>
        public Guid Id { get; set; }
    }
}
