﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.CustomerRequests.Commands
{
    [Serializable]
    public class UpdateCustomerRequestStatusCommand
    {
        /// <summary>
        /// Request Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// New request status
        /// </summary>
        public string Status { get; set; }
    }
}
