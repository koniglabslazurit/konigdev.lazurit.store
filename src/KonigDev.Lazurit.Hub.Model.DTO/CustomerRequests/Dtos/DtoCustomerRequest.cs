﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.CustomerRequests.Dtos
{
    [Serializable]
    public class DtoCustomerRequest
    {
        /// <summary>
        /// Request Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Request creation time
        /// </summary>
        public DateTime CreationTime { get; set; }

        /// <summary>
        /// Customer full name
        /// </summary>
        /// <remarks>
        /// ФИО
        /// </remarks>
        public string CustomerFullName { get; set; }

        /// <summary>
        /// Customer email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Customer phone number
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Request type tech name
        /// </summary>
        public string TypeTechName { get; set; }

        /// <summary>
        /// Request type
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Request status tech name
        /// </summary>
        public string StatusTechName { get; set; }
    }
}
