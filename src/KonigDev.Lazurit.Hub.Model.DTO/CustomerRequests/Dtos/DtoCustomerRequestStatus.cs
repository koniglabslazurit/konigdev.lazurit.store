﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.CustomerRequests.Dtos
{
    [Serializable]
    public class DtoCustomerRequestStatus
    {
        /// <summary>
        /// Status tech name
        /// </summary>
        public string TechName { get; set; }

        /// <summary>
        /// Status name
        /// </summary>
        public string Name { get; set; }
    }
}
