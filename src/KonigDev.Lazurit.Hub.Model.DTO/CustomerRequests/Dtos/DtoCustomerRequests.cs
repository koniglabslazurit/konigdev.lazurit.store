﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.CustomerRequests.Dtos
{
    [Serializable]
    public class DtoCustomerRequests
    {
        /// <summary>
        /// List of customer requests
        /// </summary>
        public List<DtoCustomerRequest> CustomerRequests { get; set; }

        /// <summary>
        /// List of all customer request statuses
        /// </summary>
        public List<DtoCustomerRequestStatus> CustomerRequestStatuses { get; set; }
    }
}
