﻿using KonigDev.Lazurit.Hub.Model.DTO.Article.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Article.Commands
{
    [Serializable]
    public class UpdateVendorCodeCommand
    {
        public IEnumerable<DtoUpdatingVendorCode> VendorCodes { get; set; }
    }
}
