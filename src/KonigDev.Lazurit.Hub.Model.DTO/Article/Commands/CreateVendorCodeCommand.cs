﻿using KonigDev.Lazurit.Hub.Model.DTO.Article.Dto;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Article.Commands
{
    [Serializable]
    public class CreateVendorCodeCommand
    {
        public List<DtoVendorCodeItem> VendorCodes { get; set; }
    }
}
