﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Article.Query
{
    public class GetArticleByUniqueFieldsQuery
    {
        public Guid? ArticleId { get; set; }
        public string ArticleSyncCode1C { get; set; }
        public Guid? UserId { get; set; }
    }
}
