﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Article.Query
{
    [Serializable]
    public class GetVendorCodesForInstallmentQuery
    {
        public GetVendorCodesForInstallmentQuery()
        {
            FurnitureTypesIds = new List<Guid>();
            SeriesIds = new List<Guid>();
            Rooms = new List<Guid>();
        }

        public List<Guid> FurnitureTypesIds { get; set; }
        public List<Guid> SeriesIds { get; set; }
        public List<Guid> Rooms { get; set; }
    }
}
