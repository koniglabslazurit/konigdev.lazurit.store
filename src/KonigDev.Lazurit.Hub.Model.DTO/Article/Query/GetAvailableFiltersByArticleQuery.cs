﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Article.Query
{
    public class GetAvailableFiltersByArticleQuery
    {
        public Guid ArticleId { get; set; }
    }
}
