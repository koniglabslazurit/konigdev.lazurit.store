﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Article.Dto
{
    [Serializable]
    public class DtoVendorCodeItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string SyncCode1C { get; set; }
        public Guid FurnitureTypeId { get; set; }
        public string Range { get; set; }
    }
}
