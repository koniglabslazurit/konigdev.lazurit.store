﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Article.Dto
{
    [Serializable]
    public class DtoUpdatingVendorCode
    {
        public Guid FurnitureTypeId { get; set; }
        public Guid Id { get; set; }
        public string RangeVendorCode { get; set; }


    }
}
