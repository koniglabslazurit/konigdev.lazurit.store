﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors;
using KonigDev.Lazurit.Hub.Model.DTO.Facings;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors;
using KonigDev.Lazurit.Hub.Model.DTO.Products;

namespace KonigDev.Lazurit.Hub.Model.DTO.Article
{
    [Serializable]
    public class DtoArticleByUniqueFieldsQueryResult
    {
        public string FurnitureTypeName { get; set; }
        public Guid ArticleId { get; set; }
        public string ArticleSyncCode1C { get; set; }
        public List<DtoProductResult> Products { get; set; }
        public List<DtoFacingsResult> Facings { get; set; }
        public List<DtoFacingColorResult> FacingsColors { get; set; }
        public List<DtoFrameColorResult> FramesColors { get; set; }
        public List<int> Heights { get; set; }
        public List<int> Widths { get; set; }
        public List<int> Lengths { get; set; }
    }
}
