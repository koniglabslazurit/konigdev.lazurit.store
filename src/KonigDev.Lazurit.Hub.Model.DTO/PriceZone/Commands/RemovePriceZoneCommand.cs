﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Commands
{
    [Serializable]
    public class RemovePriceZoneCommand
    {
        public Guid Id { get; set; }
    }
}
