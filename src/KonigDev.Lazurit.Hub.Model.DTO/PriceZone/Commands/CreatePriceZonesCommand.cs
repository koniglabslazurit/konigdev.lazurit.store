﻿using KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Dto;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Commands
{
    [Serializable]
    public class CreatePriceZonesCommand
    {
        public List<DtoPriceZoneItem> PrizeSones { get; set; }
    }
}
