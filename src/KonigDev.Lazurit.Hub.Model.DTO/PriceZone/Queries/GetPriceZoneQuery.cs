﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Queries
{
    [Serializable]
    public class GetPriceZoneQuery
    {
        public Guid Id { get; set; }
    }
}
