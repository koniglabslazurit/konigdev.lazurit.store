﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Queries
{
    [Serializable]
    public class DtoPriceZone
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string SyncCode1C { get; set; }
        public byte Number { get; set; }
    }
}
