﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.PriceZone.Dto
{
    [Serializable]
    public class DtoPriceZoneItem
    {
        public Guid Id { get; set; }
        public string SyncCode1C { get; set; }
    }
}
