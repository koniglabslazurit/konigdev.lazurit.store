﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Styles.Dto
{
    [Serializable]
    public class DtoStyleItem
    {
        public Guid Id { get; set; }
        public string SyncCode1C { get; set; }
        public string Name { get; set; }
    }
}
