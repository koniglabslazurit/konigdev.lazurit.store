﻿using KonigDev.Lazurit.Hub.Model.DTO.Styles.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Styles.Command
{
    [Serializable]
    public class CreateStylesCommand
    {
        public List<DtoStyleItem> Styles { get; set; }
    }
}
