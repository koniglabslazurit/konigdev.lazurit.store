﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Products.Command
{
    [Serializable]
    public class UpdateProductContentCommand
    {
        public Guid Id { get; set; }
        public int Left { get; set; }
        public int Top { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
}
