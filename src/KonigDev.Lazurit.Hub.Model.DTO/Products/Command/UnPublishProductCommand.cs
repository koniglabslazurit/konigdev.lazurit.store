﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Products.Command
{
    [Serializable]
    public class UnPublishProductCommand
    {
        public string SyncCode1C { get; set; }
    }
}
