﻿using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Products.Command
{
    [Serializable]
    public class UpdateProductPricesCommand
    {
        public List<DtoProductPricesUpdating> ProductPrices { get; set; }
    }
}
