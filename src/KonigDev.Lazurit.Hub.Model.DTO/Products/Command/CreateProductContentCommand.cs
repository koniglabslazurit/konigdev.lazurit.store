﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Products.Command
{
    [Serializable]
    public class CreateProductContentCommand
    {
        public Guid Id { get; set; }
        public Guid CollectionVariantId { get; set; }
        public Guid ProductId { get; set; }
    }
}
