﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Products.Command
{
    [Serializable]
    public class AddProductPartMarkerCommand
    {
        public Guid ProductId { get; set; }
        public Guid ProductPartMarkerId { get; set; }
    }
}
