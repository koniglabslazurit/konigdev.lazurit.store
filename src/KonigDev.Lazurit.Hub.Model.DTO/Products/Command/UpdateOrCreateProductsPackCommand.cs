﻿using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Products.Command
{
    [Serializable]
    public class UpdateOrCreateProductsPackCommand
    {
        public List<DtoProductPack> ProductsPacks { get; set; }
    }
}
