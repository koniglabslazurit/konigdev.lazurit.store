﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Products.Query
{
    [Serializable]
    [DataContract]
    public class GetProductsByRequestQuery
    {
        public IEnumerable<Guid?> FacingIds { get; set; }
        public IEnumerable<Guid?> FacingColorIds { get; set; }
        public IEnumerable<Guid?> FrameColorIds { get; set; }
        public Guid? FurnitureTypeId { get; set; }
        public string FurnitureAlias { get; set; }
        public Guid? CollectionId { get; set; }
        public Guid? CollectionVariantId { get; set; }
        public Guid? UserId { get; set; }
    }
}
