﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Products.Query
{
    [Serializable]
    public class GetProductUnPublishValidationQuery
    {
        public string SyncCode1C { get; set; }
    }
}
