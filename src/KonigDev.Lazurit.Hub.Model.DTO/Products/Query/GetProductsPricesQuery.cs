﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace KonigDev.Lazurit.Hub.Model.DTO.Products.Query
{
    /// <summary>
    /// Запрос на получение цен для продуктов по коду валюты и ценовой зоне
    /// </summary>
    [Serializable]
    [DataContract]
    public class GetProductsPricesQuery
    {
        public List<Guid> ProductIds { get; set; }
        public Guid PriceZoneId { get; set; }
        public string CurrencyCode { get; set; }
    }
}
