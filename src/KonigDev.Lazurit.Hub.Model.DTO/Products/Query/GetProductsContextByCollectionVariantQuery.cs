﻿using System;
using System.Runtime.Serialization;

namespace KonigDev.Lazurit.Hub.Model.DTO.Products.Query
{
    [Serializable]
    [DataContract]
    public class GetProductsContextByCollectionVariantQuery
    {
        public Guid CollectionVariantId { get; set; }
        public Guid? UserId { get; set; }
    }
}
