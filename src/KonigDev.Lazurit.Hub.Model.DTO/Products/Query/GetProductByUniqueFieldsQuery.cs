﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Products.Query
{
    [Serializable]
    [DataContract]
    public class GetProductByUniqueFieldsQuery
    {
        public string SyncCode1C { get; set; }
    }
}
