﻿using KonigDev.Lazurit.Hub.Model.DTO.ProductPackages;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Products
{
    [Serializable]
    public class DtoProductResult
    {
        public Guid Id { get; set; }
        public string SyncCode1C { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public bool IsAssemblyRequired { get; set; }
        public Guid ArticleId { get; set; }
        public Guid? FacingId { get; set; }
        public Guid? FacingColorId { get; set; }
        public Guid? FrameColorId { get; set; }
        public string FacingName { get; set; }
        public string FacingColorName { get; set; }
        //todo name is wrong
        public string FrameColorIdName { get; set; }        
        public string FurnitureType { get; set; }
        public string SeriesName { get; set; }
        public string RoomTypeName { get; set; }
        public string Article { get; set; }
        [Obsolete("Must be rename to series alias")]
        public string CollectionAlias { get; set; }
        public List<Guid> Propirties { get; set; }
        public bool IsFavorite { get; set; }
        public string Warranty { get; set; }
        public DtoProductPackageResult ProductPackage { get; set; }
        public string PackagesAmount { get; set; }
        public decimal PackageWeight { get; set; }
        //Существует только у продуктов, которые достаются вместе с вариантами коллекций.
        public Guid CollectionId { get; set; }
        public bool IsDisassemblyState { get; set; }
        public string Descr { get; set; }
    }
}
