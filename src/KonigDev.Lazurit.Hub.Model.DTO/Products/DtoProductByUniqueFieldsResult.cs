﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors;
using KonigDev.Lazurit.Hub.Model.DTO.Facings;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors;

namespace KonigDev.Lazurit.Hub.Model.DTO.Products
{
    [Serializable]
    public class DtoProductByUniqueFieldsResult
    {
        public Guid Id { get; set; }
        public Guid ArticleId { get; set; }
        //public string Name { get; set; }
        public string SyncCode1C { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public bool IsAssemblyRequired { get; set; }
        public DtoFacingsResult Facing { get; set; }
        public DtoFacingColorResult FacingColor { get; set; }
        public DtoFrameColorResult FrameColor { get; set; }
    }
}
