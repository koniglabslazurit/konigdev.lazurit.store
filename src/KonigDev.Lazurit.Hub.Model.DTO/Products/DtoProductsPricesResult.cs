﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Products
{
    [Serializable]
    public class DtoProductPricesResult
    {
        public Guid ProductId { get; set; }

        /// <summary>
        /// Цена подукта
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Скидка на продукт
        /// </summary>
        public decimal? Discount { get; set; }

        /// <summary>
        /// Стоимость сборки
        /// </summary>
        public decimal AssemblyPrice { get; set; }

        /// <summary>
        /// Значение рассрочки на 6 мес
        /// </summary>
        public int Installment6Month { get; set; }

        /// <summary>
        /// Значение рассрочки на 10 мес
        /// </summary>
        public int Installment10Month { get; set; }

        /// <summary>
        /// Значение рассрочки на 12 мес
        /// </summary>
        public int Installment12Month { get; set; }

        /// <summary>
        /// Значение рассрочки на 18 мес
        /// </summary>
        public int Installment18Month { get; set; }

        /// <summary>
        /// Значение рассрочки на 24 мес
        /// </summary>
        public int Installment24Month { get; set; }

        /// <summary>
        /// Значение рассрочки на 36 мес
        /// </summary>
        public int Installment36Month { get; set; }

        /// <summary>
        /// Значение рассрочки на 48 мес
        /// </summary>
        public int Installment48Month { get; set; }
    }
}
