﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Products
{
    [Serializable]
    public class DtoProductPartMarker
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string TechName { get; set; }
        public short SortNumber { get; set; }
    }
}
