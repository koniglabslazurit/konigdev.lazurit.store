﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Products.Dto
{
    [Serializable]
    public class DtoProductUnPublishValidation
    {
        public DtoProductUnPublishValidation()
        {
            Errors = new List<string>();
        }

        public bool IsValid { get; set; }
        public List<string> Errors { get; set; }
    }
}
