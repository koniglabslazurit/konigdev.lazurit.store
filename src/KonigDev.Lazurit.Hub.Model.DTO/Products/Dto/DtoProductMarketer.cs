﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Products.Dto
{
    [Serializable]
    public class DtoProductMarketer
    {
        public DtoProductMarketer()
        {
            Products = new List<DtoProduct>();
        }

        public string VendorCode { get; set; }
        public string Range { get; set; }
        public IEnumerable<DtoProduct> Products { get; set; }
    }
}
