﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Products.Dto
{
    [Serializable]
    public class DtoProductPricesUpdating
    {
        public DtoProductPricesUpdating()
        {
            Prices = new List<DtoProductPricesItemUpdating>();
        }
        public string ProductSyncCode { get; set; }

        public List<DtoProductPricesItemUpdating> Prices { get; set; }
    }

    [Serializable]
    public class DtoProductPricesItemUpdating
    {
        public Guid PriceZoneId { get; set; }
        public decimal Price { get; set; }
    }
}
