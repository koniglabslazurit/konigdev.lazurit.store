﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Products.Dto
{
    /// <summary>
    /// Модель данных для продукта для отображения ее в тайле. Имеет минимальный набаор данных для работы спродуктом.
    /// </summary>
    [Serializable]
    public class DtoProduct
    {
        /* при расширении модели дто, подумай, а нужно ли */
        public Guid Id { get; set; }
        public string SyncCode1C { get; set; }
        public string SeriesName { get; set; }
        public string SeriesAlias { get; set; }
        public string ArticleName { get; set; }
        public string ArticleSyncCode1c { get; set; }
        public string FurnitureTypeAlias { get; set; }
        public string FurnitureTypeName { get; set; }
        public Guid FurnitureTypeId { get; set; }
        public bool IsFavorite { get; set; }
        public bool IsAssemblyRequired { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public string MarketerRange {get;set;}
    }
}
