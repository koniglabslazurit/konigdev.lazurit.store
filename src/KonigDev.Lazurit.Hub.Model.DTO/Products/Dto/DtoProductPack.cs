﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Products.Dto
{
    [Serializable]
    public class DtoProductPack
    {
        public Guid Id { get; set; }

        public string ProductSyncCode1C { get; set; }
        public string SyncCode1C { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public decimal Weight { get; set; }
        public int Amount { get; set; }
    }
}
