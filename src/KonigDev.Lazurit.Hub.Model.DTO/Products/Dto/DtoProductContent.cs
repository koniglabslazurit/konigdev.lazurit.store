﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Products.Dto
{
    [Serializable]
    public class DtoProductContent
    {
        public Guid Id { get; set; }
        public string SyncCode1C { get; set; }
        public string ArticleName { get; set; }
        public string ArticleSyncCode1c { get; set; }
        public string FurnitureTypeAlias { get; set; }
        public string FurnitureTypeName { get; set; }       
        public Guid? ObjectId { get; set; }
        public bool IsEnabled { get; set; }
        public int ContentLeft { get; set; }
        public int ContentTop { get; set; }
        public int ContentWidth { get; set; }
        public int ContentHeight { get; set; }
        public bool IsFavorite { get; set; }
        public Guid ProductId { get; set; }
        public bool IsAssemblyRequired { get; set; }
    }
}
