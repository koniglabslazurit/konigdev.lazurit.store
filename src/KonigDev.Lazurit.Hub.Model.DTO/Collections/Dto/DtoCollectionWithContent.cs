﻿using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.FacingColors;
using KonigDev.Lazurit.Hub.Model.DTO.Facings;
using KonigDev.Lazurit.Hub.Model.DTO.FrameColors;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Collections.Dto
{
    [Serializable]
    public class DtoCollectionWithContent
    {
        public Guid Id { get; set; }
        public string RoomType { get; set; }
        public string SeriesName { get; set; }
        public IEnumerable<DtoFacingsResult> Facings { get; set; }
        public IEnumerable<DtoFacingColorResult> FacingColors { get; set; }
        public IEnumerable<DtoFrameColorResult> FrameColors { get; set; }
        public IEnumerable<DtoCollectionVariantContent> Variants { get; set; }
    }
}
