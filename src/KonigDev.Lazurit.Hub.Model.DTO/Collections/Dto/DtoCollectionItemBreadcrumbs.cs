﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Collections.Dto
{
    [Serializable]
    public class DtoCollectionItemBreadcrumbs
    {

        /// <summary>
        /// Алиас серии
        /// </summary>
        public string Alias { get; set; }

        /// <summary>
        /// Ид коллекции
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Наименвоание серии
        /// </summary>
        public string Name { get; set; }
    }
}
