﻿using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Collections.Dto
{
    [Serializable]
    public class DtoCollectionResultForCatalog
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Room { get; set; }
        public string Alias { get; set; }
        public string Range { get; set; }

        public IEnumerable<DtoCollectionVariantResultForCatalog> Variants { get; set; }
    }
}
