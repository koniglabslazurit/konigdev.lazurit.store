﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Collections
{
    [Serializable]
    public class DtoCollectionVariant
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid FacingId { get; set; }
        public Guid FaceColorId { get; set; }
        public Guid FrameColorId { get; set; }
    }
}
