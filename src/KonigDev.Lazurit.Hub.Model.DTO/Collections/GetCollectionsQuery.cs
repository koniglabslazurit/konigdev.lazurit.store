﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Collections
{
    [Serializable]
    public class GetCollectionsQuery
    {
        public string RoomAlias { get; set; }
    }

    [Serializable]
    public class GetCollections
    {
        public string Room { get; set; }
    }
}
