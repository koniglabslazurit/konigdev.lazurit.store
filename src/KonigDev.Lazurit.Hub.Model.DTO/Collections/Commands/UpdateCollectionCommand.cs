﻿using KonigDev.Lazurit.Hub.Model.DTO.Collections.Dto;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Collections.Commands
{
    [Serializable]
    public class UpdateCollectionCommand
    {
        public IEnumerable<DtoUpdatingCollection> Collections { get; set; }
    }
}
