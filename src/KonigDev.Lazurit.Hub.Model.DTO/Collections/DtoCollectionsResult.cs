﻿using KonigDev.Lazurit.Hub.Model.DTO.Styles.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.TargetAudiences.Dto;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Collections
{
    [Serializable]
    public class DtoCollectionsResult
    {
        public DtoCollectionsResult()
        {
            Styles = new HashSet<DtoStyleItem>();
            TargetAudiences = new HashSet<DtoTargetAudienceItem>();
        }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid SeriesId { get; set; }
        public Guid CollectionTypeId { get; set; }
        public Guid RoomTypeId { get; set; }
        public Guid DefaultVariantId { get; set; }
        public string Alias { get; set; }
        public string Range { get; set; }
        public IEnumerable<DtoStyleItem> Styles { get; set; }
        public IEnumerable<DtoTargetAudienceItem> TargetAudiences { get; set; }
    }
}
