﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Collections.Query
{
    [Serializable]
    public class GetCollectionsForBreadcrumbsQuery
    {
        public string RoomAlias { get; set; }
    }
}
