﻿namespace KonigDev.Lazurit.Hub.Model.DTO.Collections.Query
{
    public class GetCollectionByAliasQuery
    {
        public string Alias { get; set; }
    }
}
