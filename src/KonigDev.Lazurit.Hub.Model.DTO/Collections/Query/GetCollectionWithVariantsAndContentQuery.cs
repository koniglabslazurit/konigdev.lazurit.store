﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Collections.Query
{
    [Serializable]
    public class GetCollectionWithVariantsAndContentQuery
    {
        public string RoomTypeAlias { get; set; }
        public string SeriesAlias { get; set; }
        public Guid? UserId { get; set; }
    }
}
