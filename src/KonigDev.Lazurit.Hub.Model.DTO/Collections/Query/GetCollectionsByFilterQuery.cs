﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Collections.Query
{
   public class GetCollectionsByFilterQuery
    {
        public Guid RoomTypeId { get; set; }
        public Guid? FacingId { get; set; }
        public Guid? FacingColorId { get; set; }
        public Guid? FrameColorId { get; set; }
    }
}
