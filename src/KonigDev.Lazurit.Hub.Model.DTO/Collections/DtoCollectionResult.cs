﻿using System;
using System.Collections.Generic;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants;

namespace KonigDev.Lazurit.Hub.Model.DTO.Collections
{
    [Serializable]
    public class DtoCollectionResult
    {        
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Room { get; set; }
        public string Alias { get; set; }

        public IEnumerable<DtoCollectionVariantResult> Variants { get; set; }
    }

    [Serializable]
    public class DtoCollectionItem
    {
        public Guid Id { get; set; }
        public string Series { get; set; }
        public string Room { get; set; }
    }
}
