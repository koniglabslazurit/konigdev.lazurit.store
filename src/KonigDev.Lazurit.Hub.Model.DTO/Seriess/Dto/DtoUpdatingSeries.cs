﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.Seriess.Dto
{
    [Serializable]
    public class DtoUpdatingSeries
    {
        public Guid Id { get; set; }
        public string RangeSeries { get; set; }
        public string SyncCode1C { get; set; }
    }
}
