﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.Seriess.Dto
{
    [Serializable]
    public class DtoSeriesItem
    {
        public Guid Id { get; set; }
        public string SyncCode1C { get; set; }
        public string Alias { get; set; }
        public string Name { get; set; }
    }
}