﻿using KonigDev.Lazurit.Hub.Model.DTO.Seriess.Dto;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.Seriess.Commands
{
    [Serializable]
    public class UpdateSeriesCommand
    {
        public IEnumerable<DtoUpdatingSeries> Series { get; set; }
    }
}
