﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO
{
    /// <summary>
    /// Базовый класс для запросов получения списков
    /// </summary>
    [Serializable]   
    public class DtoCommonTableRequest
    {
        public int Page { get; set; }
        public int ItemsOnPage { get; set; }
        public string Sort { get; set; }        
        public string SortColumn { get; set; }
    }
}
