﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Query
{
   public class GetFrameColorsByCollectionQuery
    {
        public Guid? CollectionId { get; set; }
        public Guid? ArticleId { get; set; }
    }
}
