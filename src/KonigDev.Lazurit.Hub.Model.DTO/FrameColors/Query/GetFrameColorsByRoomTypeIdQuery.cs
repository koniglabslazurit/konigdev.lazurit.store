﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Query
{
   public class GetFrameColorsByRoomTypeIdQuery
    {
        public Guid RoomTypeId { get; set; }
    }
}
