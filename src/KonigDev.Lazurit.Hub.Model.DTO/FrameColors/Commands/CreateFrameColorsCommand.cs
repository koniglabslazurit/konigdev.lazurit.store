﻿using KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Dto;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Commands
{
    [Serializable]
    public class CreateFrameColorsCommand
    {
        public List<DtoFrameColorItem> FrameColors { get; set; }
    }
}
