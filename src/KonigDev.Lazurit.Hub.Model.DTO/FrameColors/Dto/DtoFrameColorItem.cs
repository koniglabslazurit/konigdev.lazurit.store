﻿using System;

namespace KonigDev.Lazurit.Hub.Model.DTO.FrameColors.Dto
{
    [Serializable]
    public class DtoFrameColorItem
    {
        public Guid Id { get; set; }
        public bool IsUniversal { get; set; }
        public string Name { get; set; }
        public string SyncCode1C { get; set; }
    }
}
