﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Store.Integration.Contracts;
using KonigDev.Lazurit.Store.Integration.CreatingServices;
using KonigDev.Lazurit.Store.Integration.Implemetations;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Integration.UnitTests.UpdatingIntegrationTablesServiceTests
{
    [TestFixture]
    public class UpdateSeriesTests
    {
        private CreatingIntegrationDataService _classForTest;
        private Mock<IHubHandlersFactory> _hubHandlersFactoryMock;

        [SetUp]
        public void SetUp()
        {
            _hubHandlersFactoryMock = new Mock<IHubHandlersFactory>();
            _hubHandlersFactoryMock.Setup(p => p.CreateCommandHandler<CreateIntegrationSeriesCommand>())
                .Returns(new HandlerMock());
            _classForTest = new CreatingIntegrationDataService(_hubHandlersFactoryMock.Object);
        }

        [Test]
        public async Task UpdateSeriesTests_ListIsEmpty_NotCallGrain()
        {
            //arrange
            var list = new List<IntegrationSeriesContract>();
            //act
            await _classForTest.CreatingSeries(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationSeriesCommand>(), Times.Never);
        }

        [Test]
        public async Task UpdateSeriesTests_ListNotHasNew_NotCallGrain()
        {
            //arrange
            var list = new List<IntegrationSeriesContract> {
                new IntegrationSeriesContract {IsNew = false }
            };
            //act
            await _classForTest.CreatingSeries(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationSeriesCommand>(), Times.Never);
        }

        [Test]
        public async Task UpdateSeriesTests_ListHasNew_CallGrain()
        {
            //arrange
            var list = new List<IntegrationSeriesContract> {
                new IntegrationSeriesContract {IsNew = true }
            };
            //act
            await _classForTest.CreatingSeries(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationSeriesCommand>(), Times.Once);
        }

        internal class HandlerMock : IHubCommandHandler<CreateIntegrationSeriesCommand>
        {
            public Task Execute(CreateIntegrationSeriesCommand command)
            {
                return Task.FromResult("");
            }
        }
    }
}
