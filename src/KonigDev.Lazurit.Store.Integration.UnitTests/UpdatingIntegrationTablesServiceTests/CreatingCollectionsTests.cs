﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Integration.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using KonigDev.Lazurit.Store.Integration.Implemetations;
using Moq;
using NUnit.Framework;
using KonigDev.Lazurit.Store.Integration.CreatingServices;

namespace KonigDev.Lazurit.Store.Integration.UnitTests.UpdatingIntegrationTablesServiceTests
{
    [TestFixture]
    public class CreatingCollectionsTests
    {
        private CreatingIntegrationDataService _classForTest;
        private Mock<IHubHandlersFactory> _hubHandlersFactoryMock;
        private Mock<IHubQueryHandler<GetIntegrationSeriesQuery, List<DtoIntegrationSeriesItem>>> _seriesHandlerMock;
        private Mock<IHubQueryHandler<GetIntegrationRoomsQuery, List<DtoIntegrationRoomItem>>> _roomsHandlerMock;

        [SetUp]
        public void SetUp()
        {
            _hubHandlersFactoryMock = new Mock<IHubHandlersFactory>();
            _seriesHandlerMock = new Mock<IHubQueryHandler<GetIntegrationSeriesQuery, List<DtoIntegrationSeriesItem>>>();
            _roomsHandlerMock = new Mock<IHubQueryHandler<GetIntegrationRoomsQuery, List<DtoIntegrationRoomItem>>>();

            _hubHandlersFactoryMock.Setup(p => p.CreateCommandHandler<CreateIntegrationCollectionsCommand>())
                .Returns(new HandlerMock());

            _hubHandlersFactoryMock.Setup(p => p.CreateQueryHandler<GetIntegrationSeriesQuery, List<DtoIntegrationSeriesItem>>())
                .Returns(_seriesHandlerMock.Object);
            _hubHandlersFactoryMock.Setup(p => p.CreateQueryHandler<GetIntegrationRoomsQuery, List<DtoIntegrationRoomItem>>())
                .Returns(_roomsHandlerMock.Object);

            _roomsHandlerMock.Setup(p => p.Execute(It.IsAny<GetIntegrationRoomsQuery>()))
                .Returns(Task.FromResult(new List<DtoIntegrationRoomItem>()));
            _seriesHandlerMock.Setup(p => p.Execute(It.IsAny<GetIntegrationSeriesQuery>()))
                .Returns(Task.FromResult(new List<DtoIntegrationSeriesItem>()));

            _classForTest = new CreatingIntegrationDataService(_hubHandlersFactoryMock.Object);
        }

        [Test]
        public async Task UpdateCollections_ListIsEmpty_NotCallGrain()
        {
            //arrange
            var list = new List<IntegrationCollectionContract>();
            //act
            await _classForTest.CreatingCollections(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationCollectionsCommand>(), Times.Never);
        }

        [Test]
        public async Task UpdateCollections_ListNotEmptyNotHasRoomsAndSeries_NotCallGrain()
        {
            //arrange
            var list = new List<IntegrationCollectionContract> { new IntegrationCollectionContract { IsNew = true } };
            //act
            await _classForTest.CreatingCollections(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationCollectionsCommand>(), Times.Never);
        }


        [Test]
        public async Task UpdateCollections_ListNotEmptyNotHasRoomsAndHasSeries_NotCallGrain()
        {
            //arrange
            var list = new List<IntegrationCollectionContract> { new IntegrationCollectionContract { IsNew = true } };
            _seriesHandlerMock.Setup(p => p.Execute(It.IsAny<GetIntegrationSeriesQuery>()))
              .Returns(Task.FromResult(new List<DtoIntegrationSeriesItem> { new DtoIntegrationSeriesItem { SyncCode1C = "123" } }));

            //act
            await _classForTest.CreatingCollections(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationCollectionsCommand>(), Times.Never);
        }

        [Test]
        public async Task UpdateCollections_ListNotEmptyHasRoomsAndNotHasSeries_NotCallGrain()
        {
            //arrange
            var list = new List<IntegrationCollectionContract> { new IntegrationCollectionContract { IsNew = true } };
            _roomsHandlerMock.Setup(p => p.Execute(It.IsAny<GetIntegrationRoomsQuery>()))
                .Returns(Task.FromResult(new List<DtoIntegrationRoomItem> { new DtoIntegrationRoomItem { SyncCode1C = "123" } }));

            //act
            await _classForTest.CreatingCollections(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationCollectionsCommand>(), Times.Never);
        }

        [Test]
        public async Task UpdateCollections_ListNotEmptyNotFoundRoomAndSeries_NotCallGrain()
        {
            //arrange
            var list = new List<IntegrationCollectionContract> { new IntegrationCollectionContract { IsNew = true, Room = "room", Series = "series" } };
            _roomsHandlerMock.Setup(p => p.Execute(It.IsAny<GetIntegrationRoomsQuery>()))
                .Returns(Task.FromResult(new List<DtoIntegrationRoomItem> { new DtoIntegrationRoomItem { SyncCode1C = "123" } }));
            _seriesHandlerMock.Setup(p => p.Execute(It.IsAny<GetIntegrationSeriesQuery>()))
              .Returns(Task.FromResult(new List<DtoIntegrationSeriesItem> { new DtoIntegrationSeriesItem { SyncCode1C = "123" } }));

            //act
            await _classForTest.CreatingCollections(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationCollectionsCommand>(), Times.Never);
        }

        [Test]
        public async Task UpdateCollections_ListNotEmptyFoundRoomAndSeries_CallGrain()
        {
            //arrange
            var list = new List<IntegrationCollectionContract> { new IntegrationCollectionContract { IsNew = true, Room = "room", Series = "series" } };
            _roomsHandlerMock.Setup(p => p.Execute(It.IsAny<GetIntegrationRoomsQuery>()))
                .Returns(Task.FromResult(new List<DtoIntegrationRoomItem> { new DtoIntegrationRoomItem { SyncCode1C = "room", Id = Guid.NewGuid() } }));
            _seriesHandlerMock.Setup(p => p.Execute(It.IsAny<GetIntegrationSeriesQuery>()))
                 .Returns(Task.FromResult(new List<DtoIntegrationSeriesItem> { new DtoIntegrationSeriesItem { SyncCode1C = "series", Id = Guid.NewGuid() } }));

            //act
            await _classForTest.CreatingCollections(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationCollectionsCommand>(), Times.Once);
        }

        [Test]
        public async Task UpdateCollections_ListNotEmptyItemsIsNotNewFoundRoomAndSeries_CallGrain()
        {
            //arrange
            var list = new List<IntegrationCollectionContract> { new IntegrationCollectionContract { IsNew = false, Room = "room", Series = "series" } };
            _roomsHandlerMock.Setup(p => p.Execute(It.IsAny<GetIntegrationRoomsQuery>()))
                .Returns(Task.FromResult(new List<DtoIntegrationRoomItem> { new DtoIntegrationRoomItem { SyncCode1C = "room", Id = Guid.NewGuid() } }));
            _seriesHandlerMock.Setup(p => p.Execute(It.IsAny<GetIntegrationSeriesQuery>()))
                 .Returns(Task.FromResult(new List<DtoIntegrationSeriesItem> { new DtoIntegrationSeriesItem { SyncCode1C = "series", Id = Guid.NewGuid() } }));

            //act
            await _classForTest.CreatingCollections(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationCollectionsCommand>(), Times.Never);
        }

        [Test]
        public async Task UpdateCollections_ListNotEmptyFoundRoomAndSeriesGuidIsEmpty_CallGrain()
        {
            //arrange
            var list = new List<IntegrationCollectionContract> { new IntegrationCollectionContract { IsNew = true, Room = "room", Series = "series" } };
            _roomsHandlerMock.Setup(p => p.Execute(It.IsAny<GetIntegrationRoomsQuery>()))
                .Returns(Task.FromResult(new List<DtoIntegrationRoomItem> { new DtoIntegrationRoomItem { SyncCode1C = "room", Id = Guid.Empty } }));
            _seriesHandlerMock.Setup(p => p.Execute(It.IsAny<GetIntegrationSeriesQuery>()))
                 .Returns(Task.FromResult(new List<DtoIntegrationSeriesItem> { new DtoIntegrationSeriesItem { SyncCode1C = "series", Id = Guid.Empty } }));

            //act
            await _classForTest.CreatingCollections(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationCollectionsCommand>(), Times.Never);
        }

        internal class HandlerMock : IHubCommandHandler<CreateIntegrationCollectionsCommand>
        {
            public Task Execute(CreateIntegrationCollectionsCommand command)
            {
                return Task.FromResult("");
            }
        }
    }
}
