﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Integration.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;
using KonigDev.Lazurit.Store.Integration.Implemetations;
using Moq;
using NUnit.Framework;
using System;
using KonigDev.Lazurit.Store.Integration.CreatingServices;

namespace KonigDev.Lazurit.Store.Integration.UnitTests.UpdatingIntegrationTablesServiceTests
{
    public class UpdateCollectionVariantsTests
    {
        private CreatingIntegrationDataService _classForTest;
        private Mock<IHubHandlersFactory> _hubHandlersFactoryMock;
        private Mock<IHubQueryHandler<GetIntegrationCollectionsQuery, List<DtoIntegrationCollectionItem>>> _collectionsHandlerMock;

        [SetUp]
        public void SetUp()
        {
            _hubHandlersFactoryMock = new Mock<IHubHandlersFactory>();
            _collectionsHandlerMock = new Mock<IHubQueryHandler<GetIntegrationCollectionsQuery, List<DtoIntegrationCollectionItem>>>();

            _hubHandlersFactoryMock.Setup(p => p.CreateCommandHandler<CreateIntegrationCollectionVariantsCommand>())
                .Returns(new HandlerMock());
            _hubHandlersFactoryMock.Setup(p => p.CreateQueryHandler<GetIntegrationCollectionsQuery, List<DtoIntegrationCollectionItem>>())
                .Returns(_collectionsHandlerMock.Object);

            _collectionsHandlerMock.Setup(p => p.Execute(It.IsAny<GetIntegrationCollectionsQuery>()))
                .Returns(Task.FromResult(new List<DtoIntegrationCollectionItem>()));

            _classForTest = new CreatingIntegrationDataService(_hubHandlersFactoryMock.Object);
        }

        [Test]
        public async Task UpdateCollectionVariants_ListIsEmpty_NotCallGrain()
        {
            //arrange
            var list = new List<IntegrationCollectionVariantContract>();
            //act
            await _classForTest.CreatingCollectionVariants(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationCollectionsCommand>(), Times.Never);
        }

        [Test]
        public async Task UpdateCollectionVariants_ListIsNotEmptyCollectionsIsEmpty_NotCallGrain()
        {
            //arrange
            var list = new List<IntegrationCollectionVariantContract> { new IntegrationCollectionVariantContract { IsNew = true } };
            //act
            await _classForTest.CreatingCollectionVariants(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationCollectionVariantsCommand>(), Times.Never);
        }

        [Test]
        public async Task UpdateCollectionVariants_ListIsNotEmptyItemsIsNotNewCollectionsIsEmpty_NotCallGrain()
        {
            //arrange
            var list = new List<IntegrationCollectionVariantContract> { new IntegrationCollectionVariantContract { IsNew = false } };
            //act
            await _classForTest.CreatingCollectionVariants(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationCollectionVariantsCommand>(), Times.Never);
        }

        [Test]
        public async Task UpdateCollectionVariants_ListIsNotEmptyItemsIsNotNewCollectionsExist_NotCallGrain()
        {
            //arrange
            var list = new List<IntegrationCollectionVariantContract> { new IntegrationCollectionVariantContract { IsNew = false, Room = "room", Series = "series" } };
            _collectionsHandlerMock.Setup(p => p.Execute(It.IsAny<GetIntegrationCollectionsQuery>()))
                .Returns(Task.FromResult(new List<DtoIntegrationCollectionItem> { new DtoIntegrationCollectionItem { Room = "room", Series = "series" } }));
            //act
            await _classForTest.CreatingCollectionVariants(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationCollectionVariantsCommand>(), Times.Never);
        }

        [Test]
        public async Task UpdateCollectionVariants_ListIsNotEmptyCollectionsExistIdIsEmpty_NotCallGrain()
        {
            //arrange
            var list = new List<IntegrationCollectionVariantContract> { new IntegrationCollectionVariantContract { IsNew = false, Room = "room", Series = "series" } };
            _collectionsHandlerMock.Setup(p => p.Execute(It.IsAny<GetIntegrationCollectionsQuery>()))
                .Returns(Task.FromResult(new List<DtoIntegrationCollectionItem> { new DtoIntegrationCollectionItem { Room = "room", Series = "series" } }));
            //act
            await _classForTest.CreatingCollectionVariants(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationCollectionVariantsCommand>(), Times.Never);
        }

        [Test]
        public async Task UpdateCollectionVariants_ListIsNotEmptyCollectionsNotFoundBySeries_NotCallGrain()
        {
            //arrange
            var list = new List<IntegrationCollectionVariantContract> { new IntegrationCollectionVariantContract { IsNew = false, Room = "room", Series = "series" } };
            _collectionsHandlerMock.Setup(p => p.Execute(It.IsAny<GetIntegrationCollectionsQuery>()))
                .Returns(Task.FromResult(new List<DtoIntegrationCollectionItem> { new DtoIntegrationCollectionItem { Room = "room", Series = "123" } }));
            //act
            await _classForTest.CreatingCollectionVariants(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationCollectionVariantsCommand>(), Times.Never);
        }

        [Test]
        public async Task UpdateCollectionVariants_ListIsNotEmptyCollectionsNotFoundByRoom_NotCallGrain()
        {
            //arrange
            var list = new List<IntegrationCollectionVariantContract> { new IntegrationCollectionVariantContract { IsNew = false, Room = "room", Series = "series" } };
            _collectionsHandlerMock.Setup(p => p.Execute(It.IsAny<GetIntegrationCollectionsQuery>()))
                .Returns(Task.FromResult(new List<DtoIntegrationCollectionItem> { new DtoIntegrationCollectionItem { Room = "123", Series = "series" } }));
            //act
            await _classForTest.CreatingCollectionVariants(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationCollectionVariantsCommand>(), Times.Never);
        }


        [Test]
        public async Task UpdateCollectionVariants_ListIsNotEmptyCollectionFoundAndGuidIsEmpty_NotCallGrain()
        {
            //arrange
            var list = new List<IntegrationCollectionVariantContract> { new IntegrationCollectionVariantContract { IsNew = false, Room = "room", Series = "series" } };
            _collectionsHandlerMock.Setup(p => p.Execute(It.IsAny<GetIntegrationCollectionsQuery>()))
                .Returns(Task.FromResult(new List<DtoIntegrationCollectionItem> { new DtoIntegrationCollectionItem { Room = "room", Series = "series" } }));
            //act
            await _classForTest.CreatingCollectionVariants(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationCollectionVariantsCommand>(), Times.Never);
        }

        [Test]
        public async Task UpdateCollectionVariants_ListIsNotEmptyCollectionFound_CallGrain()
        {
            //arrange
            var list = new List<IntegrationCollectionVariantContract> { new IntegrationCollectionVariantContract { IsNew = true, Room = "room", Series = "series" } };
            _collectionsHandlerMock.Setup(p => p.Execute(It.IsAny<GetIntegrationCollectionsQuery>()))
                .Returns(Task.FromResult(new List<DtoIntegrationCollectionItem> { new DtoIntegrationCollectionItem { Room = "room", Series = "series", Id = Guid.NewGuid() } }));
            //act
            await _classForTest.CreatingCollectionVariants(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationCollectionVariantsCommand>(), Times.Once);
        }

        internal class HandlerMock : IHubCommandHandler<CreateIntegrationCollectionVariantsCommand>
        {
            public Task Execute(CreateIntegrationCollectionVariantsCommand command)
            {
                return Task.FromResult("");
            }
        }
    }

}
