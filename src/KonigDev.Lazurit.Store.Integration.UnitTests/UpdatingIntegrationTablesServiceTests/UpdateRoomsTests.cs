﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Store.Integration.Contracts;
using KonigDev.Lazurit.Store.Integration.CreatingServices;
using KonigDev.Lazurit.Store.Integration.Implemetations;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.Integration.UnitTests.UpdatingIntegrationTablesServiceTests
{
    public class UpdateRoomsTests
    {
        private CreatingIntegrationDataService _classForTest;
        private Mock<IHubHandlersFactory> _hubHandlersFactoryMock;

        [SetUp]
        public void SetUp()
        {
            _hubHandlersFactoryMock = new Mock<IHubHandlersFactory>();
            _hubHandlersFactoryMock.Setup(p => p.CreateCommandHandler<CreateIntegrationRoomsCommand>())
                .Returns(new HandlerMock());
            _classForTest = new CreatingIntegrationDataService(_hubHandlersFactoryMock.Object);
        }

        [Test]
        public async Task UpdateRoomsTests_ListIsEmpty_NotCallGrain()
        {
            //arrange
            var list = new List<IntegrationRoomContract>();
            //act
            await _classForTest.CreatingRooms(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationRoomsCommand>(), Times.Never);
        }

        [Test]
        public async Task UpdateRoomsTests_ListNotHasNew_NotCallGrain()
        {
            //arrange
            var list = new List<IntegrationRoomContract> {
                new  IntegrationRoomContract{IsNew = false }
            };
            //act
            await _classForTest.CreatingRooms(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationRoomsCommand>(), Times.Never);
        }

        [Test]
        public async Task UpdateRoomsTests_ListHasNew_CallGrain()
        {
            //arrange
            var list = new List<IntegrationRoomContract> {
                new IntegrationRoomContract {IsNew = true }
            };
            //act
            await _classForTest.CreatingRooms(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationRoomsCommand>(), Times.Once);
        }
        internal class HandlerMock : IHubCommandHandler<CreateIntegrationRoomsCommand>
        {
            public Task Execute(CreateIntegrationRoomsCommand command)
            {
                return Task.FromResult("");
            }
        }
    }
}
