﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Core.Enums.Enums;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Integration.Contracts;
using KonigDev.Lazurit.Store.Integration.Implemetations;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.NUnit3;
using KonigDev.Lazurit.Store.Integration.AnalysisServices;

namespace KonigDev.Lazurit.Store.Integration.UnitTests.AnalysisProductsServiceTests
{
    [TestFixture]
    public class AnalysisProductsServiceTests
    {
        [SetUp]
        public void SetUp()
        {
        }

        [Test]
        public async Task AnalysisProducts_DifferentProducts_FilterByValidationIntegrationStatus()
        {
            //arr
            var fixture = new Fixture();
            List<IntegrationProductContract> parsingProducts = fixture.Create<List<IntegrationProductContract>>();
            List<DtoIntegrationProduct> differentProducts = fixture.Create<List<DtoIntegrationProduct>>();
            parsingProducts.ForEach(x => x.IntegrationStatus = EnumIntegrationProductStatus.NotChanged);
            differentProducts.Last().IntegrationStatus = EnumIntegrationStatus.Validation.ToString();
            for (int i = 0; i < differentProducts.Count; i++)
            {
                parsingProducts[i].SyncCode1C = differentProducts[i].SyncCode1C;
            }

            var hubMock = new Mock<IHubHandlersFactory>();
            var productHandlerMock = new Mock<IHubQueryHandler<GetIntegrationProductsQuery, List<DtoIntegrationProduct>>>();
            hubMock.Setup(x => x.CreateQueryHandler<GetIntegrationProductsQuery, List<DtoIntegrationProduct>>())
                .Returns(productHandlerMock.Object);

            productHandlerMock.Setup(x => x.Execute(It.IsAny<GetIntegrationProductsQuery>()))
                .Returns(Task.FromResult(differentProducts));
            var service = new AnalysisProductsService(hubMock.Object);


            //act
            List<IntegrationProductContract> filteredProductsFromAnaliz = await service.AnalysisProducts(parsingProducts, new List<IntegrationCollectionContract>());

            //assert
            Assert.IsTrue(filteredProductsFromAnaliz.Count == differentProducts.Count);
            Assert.IsTrue(filteredProductsFromAnaliz.Count(x => x.IntegrationStatus == EnumIntegrationProductStatus.Changed) == 1);
            Assert.IsTrue(filteredProductsFromAnaliz.Count(x => x.IntegrationStatus == EnumIntegrationProductStatus.Skip) == 2);
        }
    }
}
