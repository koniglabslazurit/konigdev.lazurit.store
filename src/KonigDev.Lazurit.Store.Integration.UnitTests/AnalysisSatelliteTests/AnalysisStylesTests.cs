﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using KonigDev.Lazurit.Store.Integration.Contracts;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Styles.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Styles.Dto;
using KonigDev.Lazurit.Store.Integration.Implemetations;
using Moq;
using NUnit.Framework;
using KonigDev.Lazurit.Store.Integration.AnalysisServices;

namespace KonigDev.Lazurit.Store.Integration.UnitTests.AnalysisSatelliteTests
{
    [TestFixture]
    public class AnalysisStylesTests
    {
        private AnalysisSatellite _classForTest;
        private Mock<IHubHandlersFactory> _hubHandlersFactoryMock;
        private Guid styleId = Guid.NewGuid();
        private Mock<IHubQueryHandler<GetStylesQuery, List<DtoStyleItem>>> _styleHandlerMock;

        [SetUp]
        public void SetUp()
        {
            _hubHandlersFactoryMock = new Mock<IHubHandlersFactory>();
            _styleHandlerMock = new Mock<IHubQueryHandler<GetStylesQuery, List<DtoStyleItem>>>();
            _hubHandlersFactoryMock.Setup(p => p.CreateQueryHandler<GetStylesQuery, List<DtoStyleItem>>())
                .Returns(_styleHandlerMock.Object);

            _styleHandlerMock.Setup(p => p.Execute(It.IsAny<GetStylesQuery>()))
                .Returns(Task.FromResult(new List<DtoStyleItem> { new DtoStyleItem { Id = styleId, Name = "style", SyncCode1C = "style" } }));

            _classForTest = new AnalysisSatellite(_hubHandlersFactoryMock.Object);
        }

        [Test]
        public async Task AnalysisStyles_EmptyList_ReturnEmptyList()
        {
            //arrange
            var list = new List<IntegrationStyleContract>();
            //act
            var result = await _classForTest.AnalysisStyles(list);
            //assert
            Assert.IsEmpty(result);
        }     

        [Test]
        public async Task AnalysisStyles_OnceItemWithErrorInList_ReturnEmptyList()
        {
            //arrange
            var list = new List<IntegrationStyleContract>
            {
                new IntegrationStyleContract { SyncCode1C = "" },
                new IntegrationStyleContract { SyncCode1C = "" }
            };
            //act
            var result = await _classForTest.AnalysisStyles(list);
            //assert
            Assert.IsNotEmpty(list);
        }
    }
}
