﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Commands;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Integrations.Query;
using KonigDev.Lazurit.Store.Integration.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using KonigDev.Lazurit.Store.Integration.CreatingServices;
using Ploeh.AutoFixture;
using System.Linq;

namespace KonigDev.Lazurit.Store.Integration.UnitTests.CreatingIntegrationDataServiceTests
{
    [TestFixture]
    public class CreatingProductsTests
    {
        private CreatingIntegrationDataService _classForTest;
        private Mock<IHubHandlersFactory> _hubHandlersFactoryMock;
        private Mock<IHubQueryHandler<GetIntegrationCollectionsVariantsQuery, List<DtoIntegrationCollectionVariantItem>>> _variantsHandlerMock;

        [SetUp]
        public void SetUp()
        {
            _hubHandlersFactoryMock = new Mock<IHubHandlersFactory>();
            _variantsHandlerMock = new Mock<IHubQueryHandler<GetIntegrationCollectionsVariantsQuery, List<DtoIntegrationCollectionVariantItem>>>();

            _hubHandlersFactoryMock.Setup(p => p.CreateCommandHandler<CreateIntegrationProductsCommand>())
                .Returns(new HandlerMock());

            _hubHandlersFactoryMock.Setup(p => p.CreateQueryHandler<GetIntegrationCollectionsVariantsQuery, List<DtoIntegrationCollectionVariantItem>>())
                .Returns(_variantsHandlerMock.Object);

            _variantsHandlerMock.Setup(p => p.Execute(It.IsAny<GetIntegrationCollectionsVariantsQuery>()))
                .Returns(Task.FromResult(new List<DtoIntegrationCollectionVariantItem>()));

            _classForTest = new CreatingIntegrationDataService(_hubHandlersFactoryMock.Object);
        }

        [Test]
        public async Task CreatingProducts_ListIsEmpty_NotCallGrain()
        {
            //arrange
            var list = new List<IntegrationProductContract>();
            //act
            await _classForTest.CreatingProducts(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationCollectionsCommand>(), Times.Never);
        }

        [Test, Ignore("")]
        public async Task CreatingProducts_ListNotEmpty_CallGrain()
        {
            //arrange
            var fixture = new Fixture();
            var variants = fixture.CreateMany<DtoIntegrationCollectionVariantItem>(2).ToList();
            variants.First().Facing = "зеркало,шпон,бамбук";
            variants.First().FacingColor = "зеркало,лава,дуб беленый белый";
            variants.First().FrameColor = "дуб беленый";

            _variantsHandlerMock.Setup(p => p.Execute(It.IsAny<GetIntegrationCollectionsVariantsQuery>()))
                .Returns(Task.FromResult(variants));

            var list = fixture.Build<IntegrationProductContract>()
                .With(p => p.IntegrationStatus, Core.Enums.Enums.EnumIntegrationProductStatus.Missing)
                .CreateMany(3).ToList();

            list.First().Facing = "зеркало";
            list.First().FacingColor = "зеркало";
            list.First().FrameColor = "дуб беленый";

            list[1].Facing = "дуб";
            list[1].FacingColor = "зеркало";
            list[1].FrameColor = "дуб беленый";

            list.Last().Facing = "шпон";
            list.Last().FacingColor = "дуб беленый белый";
            list.Last().FrameColor = "дуб беленый";
            //act
            await _classForTest.CreatingProducts(list);
            //assert
            _hubHandlersFactoryMock.Verify(p => p.CreateCommandHandler<CreateIntegrationCollectionsCommand>(), Times.Once);
        }

        internal class HandlerMock : IHubCommandHandler<CreateIntegrationProductsCommand>
        {
            public Task Execute(CreateIntegrationProductsCommand command)
            {
                return Task.FromResult("");
            }
        }
    }
}
