﻿using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;
using KonigDev.Lazurit.Store.Hubs;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Security.Principal;

namespace KonigDev.Lazurit.Store.UnitTests.ServicesTests
{
    [TestFixture]
    public class SignalrServicesTests
    {

        [SetUp]
        public void SetUp()
        {

        }
        [Test]
        public void UpdateCart_ShouldCallHubWhenValidId()
        {
            //arrange
            var connectionId = "1234";
            Guid identityId = Guid.NewGuid();
            var mockRequest = new Mock<IRequest>();
            mockRequest.Setup(x => x.Cookies).Returns(new Dictionary<string, Cookie> { { "cartId", new Cookie("cartId", identityId.ToString()) } });

            var mockContext = new HubCallerContext(mockRequest.Object, connectionId);

            dynamic all = new ExpandoObject();
            var mockClients = new Mock<IHubCallerConnectionContext<dynamic>>();
            mockClients.Setup(x => x.Client(connectionId)).Returns((ExpandoObject)all);
            mockClients.Setup(x => x.Caller).Returns((ExpandoObject)all);

            bool isRefresh = false;
            all.refreshCart = new Action<string>((id) =>
            {
                isRefresh = true;
            });
            bool isConnect = false;
            all.onConnected = new Action<string, string>((id, usId) =>
            {
                isConnect = true;
            });

            var commonHub = new CommonHub()
            {
                Context = mockContext,
                Clients = mockClients.Object,
            };
            commonHub.Connect();

            var _service = new SignalrService.SignalrService(commonHub);
            //act
            _service.UpdateCart(identityId);
            //assert
            Assert.False(isRefresh);
        }

    }
}
