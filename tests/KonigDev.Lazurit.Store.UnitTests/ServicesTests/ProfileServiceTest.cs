﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Grains.Users.QueryHandlers;
using KonigDev.Lazurit.Hub.Model.DTO.Users;
using KonigDev.Lazurit.Hub.Model.DTO.Users.Queries;
using KonigDev.Lazurit.Store.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using KonigDev.Lazurit.Store.Services.Profile;

namespace KonigDev.Lazurit.Store.UnitTests.ServicesTests
{

    [TestFixture]
    public class ProfileServiceTest
    {
        private ProfileService _classForTest;
        private Mock<IHubHandlersFactory> _hubFactoryMock;
        private Mock<IHubQueryHandler<GetUserProfileByIdQuery, DtoUserProfile>> _getUserProfileHandlerMock;

        [SetUp]
        public void SetUp()
        {
            _hubFactoryMock = new Mock<IHubHandlersFactory>();
            _getUserProfileHandlerMock = new Mock<IHubQueryHandler<GetUserProfileByIdQuery, DtoUserProfile>>();
            _hubFactoryMock.Setup(x => x.CreateQueryHandler<GetUserProfileByIdQuery, DtoUserProfile>()).Returns(_getUserProfileHandlerMock.Object);
            _classForTest = new ProfileService(_hubFactoryMock.Object);
        }

        [Test]
        public async Task GetUserProfile_UserHasAnonymInfo_BothFieldAreNull()
        {
            //arrange
            _getUserProfileHandlerMock.Setup(x => x.Execute(It.IsAny<GetUserProfileByIdQuery>())).ReturnsAsync(new DtoUserProfile()
            {
                Email = "Anonym123",
                FirstName = "Anonym123"
            });
            //act
            var res = await _classForTest.GetUserProfile(new GetUserProfileByIdQuery() { UserId = Guid.NewGuid() });
            //assert    
            Assert.IsNull(res.Email);
            Assert.IsNull(res.FirstName);
        }

        [Test]
        public async Task GetUserProfile_UserHasCorrectInfo_BothFieldAreUnchanged()
        {
            //arrange
            _getUserProfileHandlerMock.Setup(x => x.Execute(It.IsAny<GetUserProfileByIdQuery>())).ReturnsAsync(new DtoUserProfile()
            {
                Email = "email",
                FirstName = "name"
            });
            //act
            var res = await _classForTest.GetUserProfile(new GetUserProfileByIdQuery() { UserId = Guid.NewGuid() });
            //assert    
            Assert.IsTrue(res.Email == "email");
            Assert.IsTrue(res.FirstName == "name");
        }
    }
}
