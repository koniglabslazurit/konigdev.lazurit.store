﻿using System;
using System.Collections.Generic;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.FullCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants;

namespace KonigDev.Lazurit.Store.UnitTests.ServicesTests.PriceServiceTests
{
    class PriceServiceCartFullBuilder
    {
        private readonly DtoFullCart _cartFullResult = new DtoFullCart();
        public PriceServiceCartFullBuilder WithProducts(Guid firstProductId, Guid lastProductId)
        {
            _cartFullResult.Products = new List<DtoCartItemProduct>
            {
                new DtoCartItemProduct
                {
                    ItemId = firstProductId,
                    IsAssemblyRequired = false,
                    Quantity = 2
                },
                new DtoCartItemProduct
                {
                    ItemId = lastProductId,
                    IsAssemblyRequired = false,
                    Quantity = 3
                }
            };
            return this;
        }

        public PriceServiceCartFullBuilder WithCollections(Guid firstProductId, Guid lastProductId, Guid firstCollectionId, Guid lastCollectionId)
        {
            _cartFullResult.Collections = new List<DtoCartItemCollection>()
            {
                new DtoCartItemCollection
                {
                    Id=firstCollectionId,
                    CollectionVariantId = firstCollectionId,
                    Quantity = 1,
                    Products = new List<DtoCartItemProductInCollection>()
                    {
                        new DtoCartItemProductInCollection
                        {
                            Id=firstProductId,
                            ItemId = firstProductId,
                            Quantity = 2,
                            IsAssemblyRequired = true,
                            CartItemCollectionId=firstCollectionId
                        },
                        new DtoCartItemProductInCollection
                        {
                            Id=lastProductId,
                            ItemId = lastProductId,
                            Quantity = 3,
                            IsAssemblyRequired = true,
                            CartItemCollectionId=firstCollectionId
                        }
                    }
                },
                new DtoCartItemCollection
                {
                    Id=lastCollectionId,
                    CollectionVariantId = lastCollectionId,
                    Quantity = 2,
                    Products = new List<DtoCartItemProductInCollection>()
                    {
                        new DtoCartItemProductInCollection
                        {
                            ItemId = firstProductId,
                            Quantity = 2,
                            IsAssemblyRequired = false,
                            CartItemCollectionId=lastCollectionId
                        },
                        new DtoCartItemProductInCollection
                        {
                            ItemId = lastProductId,
                            Quantity = 3,
                            IsAssemblyRequired = false,
                            CartItemCollectionId=lastCollectionId
                        }
                    }
                }
            };
            return this;
        }

        public PriceServiceCartFullBuilder WithComplects(Guid firstProductId, Guid lastProductId)
        {
            _cartFullResult.Complects = new List<DtoCartItemComplect>()
            {
                new DtoCartItemComplect
                {
                    Id=firstProductId,
                    Quantity=1,
                    IsAssemblyRequired=true,
                    CollectionVariant= new DtoCollectionVariantResult() {Id= firstProductId}

                },
                new DtoCartItemComplect
                {
                    Id=lastProductId,
                    Quantity=2,
                    CollectionVariant= new DtoCollectionVariantResult() {Id= lastProductId}
                }
            };
            return this;
        }

        public DtoFullCart Build()
        {
            return _cartFullResult;
        }
    }
}

