﻿using System;
using System.Collections.Generic;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.MiniCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;

namespace KonigDev.Lazurit.Store.UnitTests.ServicesTests.PriceServiceTests
{
    class PriceServiceCartMiniBuilder
    {
        private readonly DtoMiniCart _cartMiniResult = new DtoMiniCart();
        public PriceServiceCartMiniBuilder WithProducts(Guid firstProductId, Guid lastProductId)
        {
            _cartMiniResult.Products = new List<DtoCartMiniItemProduct>
            {
                new DtoCartMiniItemProduct
                {
                    ItemId = firstProductId,
                    IsAssemblyRequired = false,
                    Quantity = 2
                },
                new DtoCartMiniItemProduct
                {
                    ItemId = lastProductId,
                    IsAssemblyRequired = false,
                    Quantity = 3
                }
            };
            return this;
        }

        public PriceServiceCartMiniBuilder WithCollections(Guid firstProductId, Guid lastProductId, Guid firstCollectionId, Guid lastCollectionId)
        {
            _cartMiniResult.Collections = new List<DtoCartMiniItemCollection>()
            {
                new DtoCartMiniItemCollection
                {
                    Id=firstCollectionId,
                    CollectionVariantId = firstCollectionId,
                    Quantity = 1,
                    Products = new List<DtoCartItem>()
                    {
                        new DtoCartItem
                        {
                            ItemId = firstProductId,
                            Quantity = 2,
                            IsAssemblyRequired = true
                            //CartItemCollectionId=firstCollectionId
                        },
                        new DtoCartItem
                        {
                            ItemId = lastProductId,
                            Quantity = 3,
                            IsAssemblyRequired = true
                            //CartItemCollectionId=firstCollectionId
                        }
                    }
                },
                new DtoCartMiniItemCollection
                {
                    Id=lastCollectionId,
                    CollectionVariantId = lastCollectionId,
                    Quantity = 2,
                    Products = new List<DtoCartItem>()
                    {
                        new DtoCartItem
                        {
                            ItemId = firstProductId,
                            Quantity = 2,
                            IsAssemblyRequired = false
                            //CartItemCollectionId=lastCollectionId
                        },
                        new DtoCartItem
                        {
                            ItemId = lastProductId,
                            Quantity = 3,
                            IsAssemblyRequired = false
                            //CartItemCollectionId=lastCollectionId
                        }
                    }
                }
            };
            return this;
        }

        public PriceServiceCartMiniBuilder WithComplects(Guid firstProductId, Guid lastProductId)
        {
            _cartMiniResult.Complects = new List<DtoCartMiniItemComplect>()
            {
                new DtoCartMiniItemComplect
                {
                    Id=firstProductId,
                    ItemId=firstProductId,
                    Quantity=1,
                    IsAssemblyRequired=true
                    //CollectionVariant= new DtoCollectionVariantResult() {Id= firstProductId}

                },
                new DtoCartMiniItemComplect
                {
                    Id=lastProductId,
                    ItemId=lastProductId,
                    Quantity=2
                    //CollectionVariant= new DtoCollectionVariantResult() {Id= lastProductId}
                }
            };
            return this;
        }

        public DtoMiniCart Build()
        {
            return _cartMiniResult;
        }
    }
}

