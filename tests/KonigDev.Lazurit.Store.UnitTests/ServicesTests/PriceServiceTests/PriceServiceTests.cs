﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cart;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Products;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Query;
using KonigDev.Lazurit.Store.Models.Localization;
using KonigDev.Lazurit.Store.Services;
using KonigDev.Lazurit.Store.Services.CookieService;
using Moq;
using NUnit.Framework;
using KonigDev.Lazurit.Store.Models.Prices;
using KonigDev.Lazurit.Store.Services.PriceSrvice;

namespace KonigDev.Lazurit.Store.UnitTests.ServicesTests.PriceServiceTests
{
    [TestFixture]
    public class PriceServiceTests
    {
        [Test]
        public void CalculateCartFullPrice_ForIncomingProducts_CalculateTruePrice()
        {
            //arr
            var firstProductId = Guid.NewGuid();
            var lastProductId = Guid.NewGuid();
            var cartFullBuilder = new PriceServiceCartFullBuilder();
            var dtoCartFullMock = cartFullBuilder
                .WithProducts(firstProductId, lastProductId)
                .Build();
            var pricesMock = new List<DtoProductPricesResult>()
            {
                new DtoProductPricesResult
                {
                    AssemblyPrice=10,
                    Discount=50,
                    Price=1000,
                    ProductId=firstProductId
                },
                new DtoProductPricesResult
                {
                    AssemblyPrice=1,
                    Discount=50,
                    Price=100,
                    ProductId=lastProductId
                }
            };

            var queryHandlerMock = new Mock<IHubQueryHandler<GetProductsPricesQuery, List<DtoProductPricesResult>>>();
            queryHandlerMock.Setup(x => x.Execute(It.IsAny<GetProductsPricesQuery>())).Returns(Task.FromResult(pricesMock));
            var hubFactoryMock = new Mock<IHubHandlersFactory>();
            hubFactoryMock.Setup(x => x.CreateQueryHandler<GetProductsPricesQuery, List<DtoProductPricesResult>>())
                .Returns(queryHandlerMock.Object);

            var cookieMock = new Mock<ICookieService>();
            cookieMock.Setup(x => x.GetCookie("zoneId")).Returns(Guid.NewGuid().ToString());
            cookieMock.Setup(x => x.GetCookie("delivery")).Returns("100");

            var service = new PriceService(hubFactoryMock.Object);

            //act
            var res = service.CalculateCartFullPrice(Guid.NewGuid(), 100, new Currency(new Language(LocalizeConstant.DefaultCulture), LocalizeConstant.DefaultCurrencyCode), dtoCartFullMock);

            //assert
            var firstProduct = res.Products.Single(x => x.ProductId == firstProductId);
            var lastProduct = res.Products.Last(x => x.ProductId == lastProductId);
            Assert.That(firstProduct.Price.Amount == 1000);
            Assert.That(firstProduct.Discount == 50);
            Assert.That(firstProduct.PriceForAssembly.Amount == 10);
            Assert.That(firstProduct.SalePrice.Amount == 500); //Цена со скидкой в процентах, с учётом сборки,если она нужна

            Assert.That(lastProduct.Price.Amount == 100);
            Assert.That(lastProduct.Discount == 50);
            Assert.That(lastProduct.PriceForAssembly.Amount == 1);
            Assert.That(lastProduct.SalePrice.Amount == 50);//Цена со скидкой в процентах, с учётом сборки,если она нужна

            //цена корзины, учитывающая продажные цены товаров,колличество и доставку
            if (res.IsDeliveryRequired)
                Assert.That(res.TotalPrice == firstProduct.SalePrice * 2 + lastProduct.SalePrice * 3 + res.PriceForDelivery.Amount);
            else
                Assert.That(res.TotalPrice == firstProduct.SalePrice * 2 + lastProduct.SalePrice * 3);
        }

        [Test]
        public void CalculateCartFullPrice_ForIncomingCollections_CalculateTruePrice()
        {
            //arr
            var firstProductId = Guid.NewGuid();
            var lastProductId = Guid.NewGuid();
            var firstCollectionId = Guid.NewGuid();
            var lastCollectionId = Guid.NewGuid();
            var cartFullBuilder = new PriceServiceCartFullBuilder();
            var dtoCartFullMock = cartFullBuilder
                .WithCollections(firstProductId, lastProductId, firstCollectionId, lastCollectionId)
                .Build();

            var pricesMock = new List<DtoProductPricesResult>()
            {
                new DtoProductPricesResult
                {
                    AssemblyPrice=10,
                    Discount=50,
                    Price=1000,
                    ProductId=firstProductId
                },
                new DtoProductPricesResult
                {
                    AssemblyPrice=1,
                    Discount=50,
                    Price=100,
                    ProductId=lastProductId
                }
            };

            var queryHandlerMock = new Mock<IHubQueryHandler<GetProductsPricesQuery, List<DtoProductPricesResult>>>();
            queryHandlerMock.Setup(x => x.Execute(It.IsAny<GetProductsPricesQuery>())).Returns(Task.FromResult(pricesMock));
            var hubFactoryMock = new Mock<IHubHandlersFactory>();
            hubFactoryMock.Setup(x => x.CreateQueryHandler<GetProductsPricesQuery, List<DtoProductPricesResult>>())
                .Returns(queryHandlerMock.Object);

            var cookieMock = new Mock<ICookieService>();
            cookieMock.Setup(x => x.GetCookie("zoneId")).Returns(Guid.NewGuid().ToString());
            cookieMock.Setup(x => x.GetCookie("delivery")).Returns("100");

            var service = new PriceService(hubFactoryMock.Object);

            //act
            var res = service.CalculateCartFullPrice(Guid.NewGuid(), 100, new Currency(new Language(LocalizeConstant.DefaultCulture), LocalizeConstant.DefaultCurrencyCode), dtoCartFullMock);

            //assert
            var firstProductInFirstCollection = res.Collections.Single(x => x.CollectionVariantId == firstCollectionId).Products.Single(x => x.ItemId == firstProductId);
            var firstProductInLastCollection = res.Collections.Single(x => x.CollectionVariantId == lastCollectionId).Products.Single(x => x.ItemId == firstProductId);
            var lastProductInFirstCollection = res.Collections.Single(x => x.CollectionVariantId == firstCollectionId).Products.Last(x => x.ItemId == lastProductId);
            var lastProductInLastCollection = res.Collections.Single(x => x.CollectionVariantId == lastCollectionId).Products.Last(x => x.ItemId == lastProductId);

            Assert.That(firstProductInFirstCollection.Price.Amount == 1000);
            Assert.That(firstProductInFirstCollection.Discount == 50);
            Assert.That(firstProductInFirstCollection.PriceForAssembly.Amount == 10);
            Assert.That(firstProductInFirstCollection.SalePrice.Amount == 510); //Цена со скидкой в процентах, с учётом сборки,если она нужна

            Assert.That(firstProductInLastCollection.Price.Amount == 1000);
            Assert.That(firstProductInLastCollection.Discount == 50);
            Assert.That(firstProductInLastCollection.PriceForAssembly.Amount == 10);
            Assert.That(firstProductInLastCollection.SalePrice.Amount == 500);//Цена со скидкой в процентах, с учётом сборки,если она нужна

            Assert.That(lastProductInFirstCollection.Price.Amount == 100);
            Assert.That(lastProductInFirstCollection.Discount == 50);
            Assert.That(lastProductInFirstCollection.PriceForAssembly.Amount == 1);
            Assert.That(lastProductInFirstCollection.SalePrice.Amount == 51); //Цена со скидкой в процентах, с учётом сборки,если она нужна

            Assert.That(lastProductInLastCollection.Price.Amount == 100);
            Assert.That(lastProductInLastCollection.Discount == 50);
            Assert.That(lastProductInLastCollection.PriceForAssembly.Amount == 1);
            Assert.That(lastProductInLastCollection.SalePrice.Amount == 50);//Цена со скидкой в процентах, с учётом сборки,если она нужна

            //цена корзины, учитывающая продажные цены товаров,колличество и доставку
            if (res.IsDeliveryRequired)
                Assert.That(res.TotalPrice == firstProductInFirstCollection.SalePrice * 2 + firstProductInLastCollection.SalePrice * 2 +
                    lastProductInFirstCollection.SalePrice * 3 + lastProductInLastCollection.SalePrice * 3 + res.PriceForDelivery.Amount);
            else
                Assert.That(res.TotalPrice == firstProductInFirstCollection.SalePrice * 2 + firstProductInLastCollection.SalePrice * 2 +
                   lastProductInFirstCollection.SalePrice * 3 + lastProductInLastCollection.SalePrice * 3);
        }


        [Test]
        public void CalculateCartFullPrice_ForIncomingComplects_CalculateTruePrice()
        {
            //arr
            var firstProductId = Guid.NewGuid();
            var lastProductId = Guid.NewGuid();
            var cartFullBuilder = new PriceServiceCartFullBuilder();
            var dtoCartFullMock = cartFullBuilder
                .WithComplects(firstProductId, lastProductId)
                .Build();

            var pricesMock = new List<DtoComplectPricesResult>()
            {
                new DtoComplectPricesResult
                {
                    AssemblyPrice=10,
                    Discount=50,
                    Price=1000,
                    ComplectId=firstProductId
                },
                new DtoComplectPricesResult
                {
                    AssemblyPrice=1,
                    Discount=50,
                    Price=100,
                    ComplectId=lastProductId
                }
            };

            var queryHandlerMock = new Mock<IHubQueryHandler<GetComplectsPricesQuery, List<DtoComplectPricesResult>>>();
            queryHandlerMock.Setup(x => x.Execute(It.IsAny<GetComplectsPricesQuery>())).Returns(Task.FromResult(pricesMock));
            var hubFactoryMock = new Mock<IHubHandlersFactory>();
            hubFactoryMock.Setup(x => x.CreateQueryHandler<GetComplectsPricesQuery, List<DtoComplectPricesResult>>())
                .Returns(queryHandlerMock.Object);

            var cookieMock = new Mock<ICookieService>();
            cookieMock.Setup(x => x.GetCookie("zoneId")).Returns(Guid.NewGuid().ToString());
            cookieMock.Setup(x => x.GetCookie("delivery")).Returns("100");

            var service = new PriceService(hubFactoryMock.Object);

            //act
            var res = service.CalculateCartFullPrice(Guid.NewGuid(), 100, new Currency(new Language(LocalizeConstant.DefaultCulture), LocalizeConstant.DefaultCurrencyCode), dtoCartFullMock);

            //assert
            var firstProduct = res.Complects.Single(x => x.CollectionVariant.Id == firstProductId);
            var lastProduct = res.Complects.Last(x => x.CollectionVariant.Id == lastProductId);
            Assert.That(firstProduct.Price.Amount == 1000);
            Assert.That(firstProduct.Discount == 50);
            Assert.That(firstProduct.PriceForAssembly.Amount == 10);
            Assert.That(firstProduct.SalePrice.Amount == 510); //Цена со скидкой в процентах, с учётом сборки,если она нужна

            Assert.That(lastProduct.Price.Amount == 100);
            Assert.That(lastProduct.Discount == 50);
            Assert.That(lastProduct.PriceForAssembly.Amount == 1);
            Assert.That(lastProduct.SalePrice.Amount == 50);//Цена со скидкой в процентах, с учётом сборки,если она нужна

            //цена корзины, учитывающая продажные цены товаров,колличество и доставку
            if (res.IsDeliveryRequired)
                Assert.That(res.TotalPrice == firstProduct.SalePrice * 1 + lastProduct.SalePrice * 2 + res.PriceForDelivery.Amount);
            else
                Assert.That(res.TotalPrice == firstProduct.SalePrice * 1 + lastProduct.SalePrice * 2);
        }
        [Test]
        public void CalculateCartMiniPrice_ForIncomingProducts_CalculateTruePrice()
        {
            //arr
            var firstProductId = Guid.NewGuid();
            var lastProductId = Guid.NewGuid();
            var cartMiniBuilder = new PriceServiceCartMiniBuilder();
            var dtoCartMiniMock = cartMiniBuilder
                .WithProducts(firstProductId, lastProductId)
                .Build();

            var pricesMock = new List<DtoProductPricesResult>()
            {
                new DtoProductPricesResult
                {
                    AssemblyPrice=10,
                    Discount=50,
                    Price=1000,
                    ProductId=firstProductId
                },
                new DtoProductPricesResult
                {
                    AssemblyPrice=1,
                    Discount=50,
                    Price=100,
                    ProductId=lastProductId
                }
            };

            var queryHandlerMock = new Mock<IHubQueryHandler<GetProductsPricesQuery, List<DtoProductPricesResult>>>();
            queryHandlerMock.Setup(x => x.Execute(It.IsAny<GetProductsPricesQuery>())).Returns(Task.FromResult(pricesMock));
            var hubFactoryMock = new Mock<IHubHandlersFactory>();
            hubFactoryMock.Setup(x => x.CreateQueryHandler<GetProductsPricesQuery, List<DtoProductPricesResult>>())
                .Returns(queryHandlerMock.Object);

            var cookieMock = new Mock<ICookieService>();
            cookieMock.Setup(x => x.GetCookie("zoneId")).Returns(Guid.NewGuid().ToString());
            cookieMock.Setup(x => x.GetCookie("delivery")).Returns("100");

            var service = new PriceService(hubFactoryMock.Object);

            //act
            var res = service.CalculateCartMiniPrice(Guid.NewGuid(), 100, new Currency(new Language(LocalizeConstant.DefaultCulture), LocalizeConstant.DefaultCurrencyCode), dtoCartMiniMock);

            //assert
            var firstProduct = res.Products.Single(x => x.ItemId == firstProductId);
            var lastProduct = res.Products.Last(x => x.ItemId == lastProductId);
            Assert.That(firstProduct.Price.Amount == 1000);
            Assert.That(firstProduct.Discount == 50);
            Assert.That(firstProduct.PriceForAssembly.Amount == 10);
            Assert.That(firstProduct.SalePrice.Amount == 500); //Цена со скидкой в процентах, с учётом сборки,если она нужна

            Assert.That(lastProduct.Price.Amount == 100);
            Assert.That(lastProduct.Discount == 50);
            Assert.That(lastProduct.PriceForAssembly.Amount == 1);
            Assert.That(lastProduct.SalePrice.Amount == 50);//Цена со скидкой в процентах, с учётом сборки,если она нужна

            //цена корзины, учитывающая продажные цены товаров,колличество и доставку
            if (res.IsDeliveryRequired)
                Assert.That(res.TotalPrice == firstProduct.SalePrice * 2 + lastProduct.SalePrice * 3 + res.PriceForDelivery.Amount);
            else
                Assert.That(res.TotalPrice == firstProduct.SalePrice * 2 + lastProduct.SalePrice * 3);
        }

        [Test]
        public void CalculateCartMiniPrice_ForIncomingCollections_CalculateTruePrice()
        {
            //arr
            var firstProductId = Guid.NewGuid();
            var lastProductId = Guid.NewGuid();
            var firstCollectionId = Guid.NewGuid();
            var lastCollectionId = Guid.NewGuid();
            var cartMiniBuilder = new PriceServiceCartMiniBuilder();
            var dtoCartMiniMock = cartMiniBuilder
                .WithCollections(firstProductId, lastProductId, firstCollectionId, lastCollectionId)
                .Build();

            var pricesMock = new List<DtoProductPricesResult>()
            {
                new DtoProductPricesResult
                {
                    AssemblyPrice=10,
                    Discount=50,
                    Price=1000,
                    ProductId=firstProductId
                },
                new DtoProductPricesResult
                {
                    AssemblyPrice=1,
                    Discount=50,
                    Price=100,
                    ProductId=lastProductId
                }
            };

            var queryHandlerMock = new Mock<IHubQueryHandler<GetProductsPricesQuery, List<DtoProductPricesResult>>>();
            queryHandlerMock.Setup(x => x.Execute(It.IsAny<GetProductsPricesQuery>())).Returns(Task.FromResult(pricesMock));
            var hubFactoryMock = new Mock<IHubHandlersFactory>();
            hubFactoryMock.Setup(x => x.CreateQueryHandler<GetProductsPricesQuery, List<DtoProductPricesResult>>())
                .Returns(queryHandlerMock.Object);

            var cookieMock = new Mock<ICookieService>();
            cookieMock.Setup(x => x.GetCookie("zoneId")).Returns(Guid.NewGuid().ToString());
            cookieMock.Setup(x => x.GetCookie("delivery")).Returns("100");

            var service = new PriceService(hubFactoryMock.Object);

            //act
            var res = service.CalculateCartMiniPrice(Guid.NewGuid(), 100, new Currency(new Language(LocalizeConstant.DefaultCulture), LocalizeConstant.DefaultCurrencyCode), dtoCartMiniMock);

            //assert
            var firstProductInFirstCollection = res.Collections.Single(x => x.CollectionVariantId == firstCollectionId).Products.Single(x => x.ItemId == firstProductId);
            var firstProductInLastCollection = res.Collections.Single(x => x.CollectionVariantId == lastCollectionId).Products.Single(x => x.ItemId == firstProductId);
            var lastProductInFirstCollection = res.Collections.Single(x => x.CollectionVariantId == firstCollectionId).Products.Last(x => x.ItemId == lastProductId);
            var lastProductInLastCollection = res.Collections.Single(x => x.CollectionVariantId == lastCollectionId).Products.Last(x => x.ItemId == lastProductId);

            Assert.That(firstProductInFirstCollection.Price.Amount == 1000);
            Assert.That(firstProductInFirstCollection.Discount == 50);
            Assert.That(firstProductInFirstCollection.PriceForAssembly.Amount == 10);
            Assert.That(firstProductInFirstCollection.SalePrice.Amount == 510); //Цена со скидкой в процентах, с учётом сборки,если она нужна

            Assert.That(firstProductInLastCollection.Price.Amount == 1000);
            Assert.That(firstProductInLastCollection.Discount == 50);
            Assert.That(firstProductInLastCollection.PriceForAssembly.Amount == 10);
            Assert.That(firstProductInLastCollection.SalePrice.Amount == 500);//Цена со скидкой в процентах, с учётом сборки,если она нужна

            Assert.That(lastProductInFirstCollection.Price.Amount == 100);
            Assert.That(lastProductInFirstCollection.Discount == 50);
            Assert.That(lastProductInFirstCollection.PriceForAssembly.Amount == 1);
            Assert.That(lastProductInFirstCollection.SalePrice.Amount == 51); //Цена со скидкой в процентах, с учётом сборки,если она нужна

            Assert.That(lastProductInLastCollection.Price.Amount == 100);
            Assert.That(lastProductInLastCollection.Discount == 50);
            Assert.That(lastProductInLastCollection.PriceForAssembly.Amount == 1);
            Assert.That(lastProductInLastCollection.SalePrice.Amount == 50);//Цена со скидкой в процентах, с учётом сборки,если она нужна

            //цена корзины, учитывающая продажные цены товаров,колличество и доставку
            if (res.IsDeliveryRequired)
                Assert.That(res.TotalPrice == firstProductInFirstCollection.SalePrice * 2 + firstProductInLastCollection.SalePrice * 2 +
                    lastProductInFirstCollection.SalePrice * 3 + lastProductInLastCollection.SalePrice * 3 + res.PriceForDelivery.Amount);
            else
                Assert.That(res.TotalPrice == firstProductInFirstCollection.SalePrice * 2 + firstProductInLastCollection.SalePrice * 2 +
                   lastProductInFirstCollection.SalePrice * 3 + lastProductInLastCollection.SalePrice * 3);
        }

        [Test]
        public void CalculateCartMiniPrice_ForIncomingComplects_CalculateTruePrice()
        {
            //arr
            var firstProductId = Guid.NewGuid();
            var lastProductId = Guid.NewGuid();
            var cartMiniBuilder = new PriceServiceCartMiniBuilder();
            var dtoCartMiniMock = cartMiniBuilder
                .WithComplects(firstProductId, lastProductId)
                .Build();

            var pricesMock = new List<DtoComplectPricesResult>()
            {
                new DtoComplectPricesResult
                {
                    AssemblyPrice=10,
                    Discount=50,
                    Price=1000,
                    ComplectId=firstProductId
                },
                new DtoComplectPricesResult
                {
                    AssemblyPrice=1,
                    Discount=50,
                    Price=100,
                    ComplectId=lastProductId
                }
            };

            var queryHandlerMock = new Mock<IHubQueryHandler<GetComplectsPricesQuery, List<DtoComplectPricesResult>>>();
            queryHandlerMock.Setup(x => x.Execute(It.IsAny<GetComplectsPricesQuery>())).Returns(Task.FromResult(pricesMock));
            var hubFactoryMock = new Mock<IHubHandlersFactory>();
            hubFactoryMock.Setup(x => x.CreateQueryHandler<GetComplectsPricesQuery, List<DtoComplectPricesResult>>())
                .Returns(queryHandlerMock.Object);

            var cookieMock = new Mock<ICookieService>();
            cookieMock.Setup(x => x.GetCookie("zoneId")).Returns(Guid.NewGuid().ToString());
            cookieMock.Setup(x => x.GetCookie("delivery")).Returns("100");

            var service = new PriceService(hubFactoryMock.Object);

            //act
            var res = service.CalculateCartMiniPrice(Guid.NewGuid(), 100, new Currency(new Language(LocalizeConstant.DefaultCulture), LocalizeConstant.DefaultCurrencyCode), dtoCartMiniMock);

            //assert
            Assert.That(res.Complects.Count == 2);
            Assert.That(res.TotalPrice == 610);

            //цена корзины, учитывающая продажные цены товаров,колличество и доставку
            if (res.IsDeliveryRequired)
                Assert.That(res.TotalPrice == res.Complects.Sum(item => item.SalePrice.Amount * item.Quantity) +  res.PriceForDelivery.Amount);
            else
                Assert.That(res.TotalPrice == res.Complects.Sum(item => item.SalePrice.Amount * item.Quantity));
        }
    }
}
