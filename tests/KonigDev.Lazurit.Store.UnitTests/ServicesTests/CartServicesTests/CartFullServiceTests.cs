﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.FullCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Query;
using KonigDev.Lazurit.Store.Dto.Cart.CartFull;
using KonigDev.Lazurit.Store.Models.Localization;
using KonigDev.Lazurit.Store.Models.Prices;
using KonigDev.Lazurit.Store.Services.Cart.Implementations;
using KonigDev.Lazurit.Store.UnitTests.ServicesTests.PriceServiceTests;
using Moq;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;
using KonigDev.Lazurit.Store.Services.PriceSrvice;

namespace KonigDev.Lazurit.Store.UnitTests.ServicesTests.CartServicesTests
{

    [TestFixture]
    public class CartFullServiceTests
    {
        private CartFullService _service;
        private Mock<IHubHandlersFactory> _hubFactoryMock;
        private Mock<IPriceService> _priceServiceMock;
        private PriceServiceCartFullBuilder _cartFullBuilder;
        private DtoFullCart _dtoCartFullMock;

        [SetUp]
        public void SetUp()
        {

            _hubFactoryMock = new Mock<IHubHandlersFactory>();
            _priceServiceMock = new Mock<IPriceService>();
            _cartFullBuilder = new PriceServiceCartFullBuilder();
            _dtoCartFullMock = _cartFullBuilder
                .WithProducts(Guid.NewGuid(), Guid.NewGuid())
                .Build();
            _service = new CartFullService(_hubFactoryMock.Object, _priceServiceMock.Object);
        }

        [Test]
        public async Task GetCartFullWithPrices_ShouldWorkFine_WhenCacheNotContainsCart()
        {
            //arrange
            var queryHandlerMock = new Mock<IHubQueryHandler<GetFullCartByIdQuery, DtoFullCart>>();
            queryHandlerMock.Setup(x => x.Execute(It.IsAny<GetFullCartByIdQuery>())).Returns(Task.FromResult(_dtoCartFullMock));
            _hubFactoryMock.Setup(x => x.CreateQueryHandler<GetFullCartByIdQuery, DtoFullCart>()).Returns(queryHandlerMock.Object);
            var priceResultId = Guid.NewGuid();
            _priceServiceMock.Setup(x => x.CalculateCartFullPrice(It.IsAny<Guid>(), It.IsAny<decimal>(), It.IsAny<Currency>(), It.IsAny<DtoFullCart>())).Returns(new DtoCartFullPriceResult { Id = priceResultId });
            //act
            var res = await _service.GetCart( Guid.NewGuid(), new Currency(new Language(LocalizeConstant.DefaultCulture), LocalizeConstant.DefaultCurrencyCode), Guid.NewGuid(), 500);
            //assert
            Assert.IsTrue(res.Id == priceResultId);
            _hubFactoryMock.Verify(t => t.CreateQueryHandler<GetFullCartByIdQuery, DtoFullCart>());
            _priceServiceMock.Verify(t => t.CalculateCartFullPrice(It.IsAny<Guid>(), It.IsAny<decimal>(), It.IsAny<Currency>(), It.IsAny<DtoFullCart>()));
        }

        [Test]
        public async Task GetCartFullWithPrices_ShouldWorkFine_ButReturnNull()
        {
            //arrange
            var queryHandlerMock = new Mock<IHubQueryHandler<GetFullCartByIdQuery, DtoFullCart>>();
            queryHandlerMock.Setup(x => x.Execute(It.IsAny<GetFullCartByIdQuery>())).Returns(Task.FromResult((DtoFullCart)null));
            _hubFactoryMock.Setup(x => x.CreateQueryHandler<GetFullCartByIdQuery, DtoFullCart>()).Returns(queryHandlerMock.Object);
            //act
            var res = await _service.GetCart( Guid.NewGuid(), new Currency(new Language(LocalizeConstant.DefaultCulture), LocalizeConstant.DefaultCurrencyCode), Guid.NewGuid(), 500);
            //assert
            Assert.IsNull(res);
            _hubFactoryMock.Verify(t => t.CreateQueryHandler<GetFullCartByIdQuery, DtoFullCart>());
            _priceServiceMock.Verify(t => t.CalculateCartFullPrice(It.IsAny<Guid>(), It.IsAny<decimal>(), It.IsAny<Currency>(), It.IsAny<DtoFullCart>()), Times.Never());
        }
    }
}