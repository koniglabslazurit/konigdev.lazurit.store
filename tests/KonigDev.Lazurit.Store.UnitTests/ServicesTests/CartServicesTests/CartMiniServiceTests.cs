﻿using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.MiniCartDto;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.Query;
using KonigDev.Lazurit.Store.Dto.Cart.CartMini;
using KonigDev.Lazurit.Store.Models.Localization;
using KonigDev.Lazurit.Store.Models.Prices;
using KonigDev.Lazurit.Store.Services;
using KonigDev.Lazurit.Store.Services.Cart.Implementations;
using KonigDev.Lazurit.Store.Services.Cart.Interfaces;
using KonigDev.Lazurit.Store.SignalrService;
using KonigDev.Lazurit.Store.UnitTests.ServicesTests.PriceServiceTests;
using Moq;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.Cart.CommonCartDto;
using KonigDev.Lazurit.Store.Services.PriceSrvice;

namespace KonigDev.Lazurit.Store.UnitTests.ServicesTests.CartServicesTests
{
    [TestFixture]
    public class CartMiniServiceTests
    {
        private CartMiniService _service;
        private Mock<IHubHandlersFactory> _hubFactoryMock;
        private Mock<ISignalrService> _signalrServiceMock;
        private Mock<ICartCacheService> _cartCacheMock;
        private Mock<IPriceService> _priceServiceMock;
        private PriceServiceCartMiniBuilder _cartMiniBuilder;
        private DtoMiniCart _dtoCartMiniMock;

        [SetUp]
        public void SetUp()
        {

            _hubFactoryMock = new Mock<IHubHandlersFactory>();
            _signalrServiceMock = new Mock<ISignalrService>();
            _cartCacheMock = new Mock<ICartCacheService>();
            _priceServiceMock = new Mock<IPriceService>();
            _cartMiniBuilder = new PriceServiceCartMiniBuilder();
            _dtoCartMiniMock = _cartMiniBuilder
                .WithProducts(Guid.NewGuid(), Guid.NewGuid())
                .Build();
            _service = new CartMiniService(_hubFactoryMock.Object, _signalrServiceMock.Object, _cartCacheMock.Object, _priceServiceMock.Object);
        }

        [Test]
        public async Task GetCartMiniWithPrices_ShouldWorkFine_WhenCacheNotContainsCart()
        {
            //arrange
            var queryHandlerMock = new Mock<IHubQueryHandler<GetMiniCartByIdQuery, DtoMiniCart>>();
            queryHandlerMock.Setup(x => x.Execute(It.IsAny<GetMiniCartByIdQuery>())).Returns(Task.FromResult(_dtoCartMiniMock));
            _hubFactoryMock.Setup(x => x.CreateQueryHandler<GetMiniCartByIdQuery, DtoMiniCart>()).Returns(queryHandlerMock.Object);
            var PriceResultID = Guid.NewGuid();
            _cartCacheMock.Setup(x => x.GetCart(It.IsAny<Guid>())).Returns(Task.FromResult((DtoCartMiniPriceResult)null));
            _priceServiceMock.Setup(x => x.CalculateCartMiniPrice(It.IsAny<Guid>(), It.IsAny<decimal>(), It.IsAny<Currency>(), It.IsAny<DtoMiniCart>())).Returns(new DtoCartMiniPriceResult { Id = PriceResultID });
            //act
            var res = await _service.GetCart(Guid.NewGuid(), new Currency(new Language(LocalizeConstant.DefaultCulture), LocalizeConstant.DefaultCurrencyCode), Guid.NewGuid(), 500);
            //assert
            Assert.IsTrue(res.Id == PriceResultID);
            _hubFactoryMock.Verify(t => t.CreateQueryHandler<GetMiniCartByIdQuery, DtoMiniCart>());
            _cartCacheMock.Verify(t => t.GetCart(It.IsAny<Guid>()));
            _priceServiceMock.Verify(t => t.CalculateCartMiniPrice(It.IsAny<Guid>(), It.IsAny<decimal>(), It.IsAny<Currency>(), It.IsAny<DtoMiniCart>()));
        }

        [Test]
        public async Task GetCartMiniWithPrices_ShouldWorkFine_WhenCacheContainsCartAlready()
        {
            //arrange
            var queryHandlerMock = new Mock<IHubQueryHandler<GetMiniCartByIdQuery, DtoMiniCart>>();
            queryHandlerMock.Setup(x => x.Execute(It.IsAny<GetMiniCartByIdQuery>())).Returns(Task.FromResult(_dtoCartMiniMock));
            _hubFactoryMock.Setup(x => x.CreateQueryHandler<GetMiniCartByIdQuery, DtoMiniCart>()).Returns(queryHandlerMock.Object);
            var PriceResultID = Guid.NewGuid();
            _cartCacheMock.Setup(x => x.GetCart(It.IsAny<Guid>())).Returns(Task.FromResult(new DtoCartMiniPriceResult { Id = PriceResultID }));
            _priceServiceMock.Setup(x => x.CalculateCartMiniPrice(It.IsAny<Guid>(), It.IsAny<decimal>(), It.IsAny<Currency>(), It.IsAny<DtoMiniCart>())).Returns((DtoCartMiniPriceResult)null);
            //act
            var res = await _service.GetCart(Guid.NewGuid(), new Currency(new Language(LocalizeConstant.DefaultCulture), LocalizeConstant.DefaultCurrencyCode), Guid.NewGuid(), 500);
            //assert
            Assert.IsTrue(res.Id == PriceResultID);
            _hubFactoryMock.Verify(t => t.CreateQueryHandler<GetMiniCartByIdQuery, DtoMiniCart>(), Times.Never());
            _cartCacheMock.Verify(t => t.GetCart(It.IsAny<Guid>()));
            _priceServiceMock.Verify(t => t.CalculateCartMiniPrice(It.IsAny<Guid>(), It.IsAny<decimal>(), It.IsAny<Currency>(), It.IsAny<DtoMiniCart>()), Times.Never());
        }
    }
}
