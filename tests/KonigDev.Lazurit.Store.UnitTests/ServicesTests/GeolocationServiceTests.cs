﻿using KonigDev.Lazurit.Core.Services;
using KonigDev.Lazurit.Store.Services.CustomRequest;
using KonigDev.Lazurit.Store.Services.Geolocation;
using Moq;
using NUnit.Framework;

namespace KonigDev.Lazurit.Store.UnitTests.ServicesTests
{
    [TestFixture]

    public class GeolocationServiceTests
    {
        private Mock<ICustomRequestService> _mockCustomRequest;
        private Mock<ILoggingService> _mockLogging;
        private GeolocationService _service;
        [SetUp]
        public void SetUp()
        {
            _mockCustomRequest = new Mock<ICustomRequestService>();
            _mockLogging= new Mock<ILoggingService>();
            _service = new GeolocationService(_mockCustomRequest.Object, _mockLogging.Object);
        }
        //[Test]
        //public void GetIpAdress_ShouldReturnIpAdress_IfCanDeterminateIt()
        //{
        //    //arrange
        //    var ip = "123.123.123.123";
        //    _mockCustomRequest.Setup(x => x.CreateRequest(It.IsAny<string>())).Returns(ip);
        //    //act
        //    var actual = _service.GetIpAdress();
        //    //assert
        //    Assert.IsInstanceOf<string>(actual);
        //    Assert.NotNull(actual);
        //    Assert.AreEqual("123.123.123.123", actual);
        //}
        //[Test]
        //public void GetIpAdress_ShouldReturnEmptyString_IfCanNotDeterminateIt()
        //{
        //    //arrange            
        //    _mockCustomRequest.Setup(x => x.CreateRequest(It.IsAny<string>())).Returns(string.Empty);
        //    //act
        //    var actual = _service.GetIpAdress();
        //    //assert
        //    Assert.IsInstanceOf<string>(actual);
        //    Assert.AreEqual(string.Empty, actual);
        //}
    }
}
