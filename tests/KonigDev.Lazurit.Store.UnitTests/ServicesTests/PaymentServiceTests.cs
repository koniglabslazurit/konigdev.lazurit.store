﻿using KonigDev.Lazurit.Core.Services;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Order.Query;
using KonigDev.Lazurit.Hub.Model.DTO.Payment.Dto;
using KonigDev.Lazurit.Hub.Model.DTO.Payment.Query;
using KonigDev.Lazurit.Store.Dto.Payment;
using KonigDev.Lazurit.Store.Services.PaymentService;
using Moq;
using NUnit.Framework;
using System;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace KonigDev.Lazurit.Store.UnitTests.ServicesTests
{
    [TestFixture]
    public class PaymentServiceTests
    {
        private Mock<IHubHandlersFactory> _hubFactoryMock;
        private PaymentService _service;
        private Mock<IHubQueryHandler<GetPaymentSettingsQuery, DtoPaymentSettings>> _getSettingsMock;
        private Mock<IHubQueryHandler<GetOrderItemByNumberQuery, DtoOrderItem>> _getOrderByNumberMock;
        private Mock<IHubQueryHandler<GetOrderItemQuery, DtoOrderItem>> _getOrderMock;
        private Mock<ILoggingService> _loggerMock;
        [SetUp]
        public void Setup()
        {
            _hubFactoryMock = new Mock<IHubHandlersFactory>();
            _loggerMock = new Mock<ILoggingService>();
            _getSettingsMock = new Mock<IHubQueryHandler<GetPaymentSettingsQuery, DtoPaymentSettings>>();
            _getOrderByNumberMock = new Mock<IHubQueryHandler<GetOrderItemByNumberQuery, DtoOrderItem>>();
            _getOrderMock = new Mock<IHubQueryHandler<GetOrderItemQuery, DtoOrderItem>>();
            _hubFactoryMock.Setup(x => x.CreateQueryHandler<GetPaymentSettingsQuery, DtoPaymentSettings>()).Returns(_getSettingsMock.Object);
            _hubFactoryMock.Setup(x => x.CreateQueryHandler<GetOrderItemQuery, DtoOrderItem>()).Returns(_getOrderMock.Object);
            _hubFactoryMock.Setup(x => x.CreateQueryHandler<GetOrderItemByNumberQuery, DtoOrderItem>()).Returns(_getOrderByNumberMock.Object);
            _service = new PaymentService(ConfigurationManager.AppSettings["kkrDomainUrl"], _hubFactoryMock.Object, _loggerMock.Object);
        }

        [Test]
        public void GetPaymentSettings_ShoultReturnSettings_IfGrainReturnSettings()
        {
            //arrange
            _getSettingsMock.Setup(x => x.Execute(It.IsAny<GetPaymentSettingsQuery>())).ReturnsAsync(                
                    new DtoPaymentSettings
                    {
                        MerchantLogin = "login",
                        RobokassaUrl = "url",
                        Password1= "password",
                        Password2 = "password2"
                    });
            //act
            var actual = _service.GetPaymentSettings();
            //assert
            Assert.IsInstanceOf<DtoPaymentSettings>(actual);
            Assert.AreEqual("login", actual.MerchantLogin);
            Assert.AreEqual("url", actual.RobokassaUrl);
            Assert.AreEqual("password", actual.Password1);
        }

        [Test]
        public void GetPaymentSettings_ShoultReturnNullSettings_IfGrainReturnNullSettings()
        {
            //arrange
            _getSettingsMock.Setup(x => x.Execute(It.IsAny<GetPaymentSettingsQuery>())).ReturnsAsync(
                    new DtoPaymentSettings
                    {
                        MerchantLogin = null,
                        RobokassaUrl = null,
                        Password1 = null,
                        Password2 = null
                    });
            //act
            var actual = _service.GetPaymentSettings();
            //assert
            Assert.IsInstanceOf<DtoPaymentSettings>(actual);
            Assert.AreEqual(null, actual.MerchantLogin);
            Assert.AreEqual(null, actual.RobokassaUrl);
            Assert.AreEqual(null, actual.Password1);
        }
        
        [Test]
        public void GetPaymentSignature_ShouldReturnSignature_WhenNotNullParams()
        {
            //arrange
            var settings = new DtoPaymentSettings
            {
                MerchantLogin = "login",
                RobokassaUrl = "url",
                Password1 = "password",
                Password2 = "password2"
            };
            long orderNumber = 123;
            string sum= "33.44";
            var signature = string.Format("{0}:{1}:{2}:{3}",
                settings.MerchantLogin, sum,
                orderNumber, settings.Password1);
            var md5 = new MD5CryptoServiceProvider();
            var signatureValue = BitConverter.ToString(md5.ComputeHash(Encoding.UTF8.GetBytes(signature))).Replace("-", "").ToLower();

            //act
            var actual = _service.GetPaymentSignature(settings, orderNumber, sum);
            //assert
            Assert.IsInstanceOf<DtoPaymentSignatureValue>(actual);
            Assert.AreEqual(signatureValue, actual.SignatureValue);
        }

        [Test]
        public void GetPaymentSignature_ShouldReturnNull_WhenAnySettingsIsNull()
        {
            //arrange
            var settings = new DtoPaymentSettings
            {
                MerchantLogin = null,
                RobokassaUrl = "url",
                Password1 = "password",
                Password2 = "password2"
            };
            long orderNumber = 123;
            string sum = "33.44";
            
            //act
            var actual = _service.GetPaymentSignature(settings, orderNumber, sum);
            //assert
            Assert.IsInstanceOf<DtoPaymentSignatureValue>(actual);
            Assert.AreEqual(null, actual.SignatureValue);
        }

        [Test]
        public void GetPaymentSignature_ShouldReturnNull_WhenOrderNumberLessOrEqualNull()
        {
            //arrange
            var settings = new DtoPaymentSettings
            {
                MerchantLogin = "login",
                RobokassaUrl = "url",
                Password1 = "password",
                Password2 = "password2"
            };
            long orderNumber = 0;
            string sum = "33.44";

            //act
            var actual = _service.GetPaymentSignature(settings, orderNumber, sum);
            //assert
            Assert.IsInstanceOf<DtoPaymentSignatureValue>(actual);
            Assert.AreEqual(null, actual.SignatureValue);
        }

        [Test]
        public void IsValidResultResponse_ShouldReturnTrue_WhenCorrectRkResult()
        {
            //arrange
            _getSettingsMock.Setup(x => x.Execute(It.IsAny<GetPaymentSettingsQuery>())).ReturnsAsync(
                    new DtoPaymentSettings
                    {
                        MerchantLogin = "login",
                        RobokassaUrl = "url",
                        Password1 = "password",
                        Password2 = "password2"
                    });            
            var signature = string.Format("{0}:{1}:{2}", "100", 1, "password");
            var md5 = new MD5CryptoServiceProvider();
            var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(signature));
            var signatureValue = BitConverter.ToString(hash).Replace("-", "").ToLower();
            DtoRobokassaResult result = new DtoRobokassaResult
            {
                InvId = "1",
                OutSum = "100",
                SignatureValue = signatureValue
            };            
            _getOrderByNumberMock.Setup(x => x.Execute(It.IsAny<GetOrderItemByNumberQuery>())).ReturnsAsync(
                new DtoOrderItem{
                    Id = Guid.NewGuid(),
                    IsPaid = false,
                    Number = 1,
                    Sum =100
            });
            //act
            var actual = _service.IsValidResultResponse(result);
            //assert
            Assert.IsInstanceOf<bool>(actual);
            Assert.IsTrue(actual);
        }

        [Test]
        public void IsValidResultResponse_ShouldReturnFalse_WhenPassword2IsNull()
        {
            //arrange
            _getSettingsMock.Setup(x => x.Execute(It.IsAny<GetPaymentSettingsQuery>())).ReturnsAsync(
                    new DtoPaymentSettings
                    {
                        MerchantLogin = "login",
                        RobokassaUrl = "url",
                        Password1 = null,
                        Password2 = null
                    });
            DtoRobokassaResult result = new DtoRobokassaResult
            {
                InvId = "1",
                OutSum = "100",
                SignatureValue = "111"
            };
            //act
            var actual = _service.IsValidResultResponse(result);
            //assert
            Assert.IsInstanceOf<bool>(actual);
            Assert.IsFalse(actual);
        }

        [Test]
        public void IsValidResultResponse_ShouldReturnFalse_WhenSignatureNotEquals()
        {
            //arrange
            _getSettingsMock.Setup(x => x.Execute(It.IsAny<GetPaymentSettingsQuery>())).ReturnsAsync(
                    new DtoPaymentSettings
                    {
                        MerchantLogin = "login",
                        RobokassaUrl = "url",
                        Password1 = "password",
                        Password2 = "password2"
                    });
            var signature = string.Format("{0}:{1}:{2}", "100", 1, "password");
            var md5 = new MD5CryptoServiceProvider();
            var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(signature));
            var signatureValue = BitConverter.ToString(hash).Replace("-", "").ToLower();
            DtoRobokassaResult result = new DtoRobokassaResult
            {
                InvId = "1",
                OutSum = "100",
                SignatureValue = "11123123123123"
            };
            //act
            var actual = _service.IsValidResultResponse(result);
            //assert
            Assert.IsInstanceOf<bool>(actual);
            Assert.IsFalse(actual);
        }

        [Test]
        public void IsValidResultResponse_ShouldReturnFalse_WhenSumNotEquals()
        {
            //arrange
            _getSettingsMock.Setup(x => x.Execute(It.IsAny<GetPaymentSettingsQuery>())).ReturnsAsync(
                    new DtoPaymentSettings
                    {
                        MerchantLogin = "login",
                        RobokassaUrl = "url",
                        Password1 = "password",
                        Password2 = "password2"
                    });
            var signature = string.Format("{0}:{1}:{2}", "100", 1, "password");
            var md5 = new MD5CryptoServiceProvider();
            var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(signature));
            var signatureValue = BitConverter.ToString(hash).Replace("-", "").ToLower();
            DtoRobokassaResult result = new DtoRobokassaResult
            {
                InvId = "1",
                OutSum = "200",
                SignatureValue = signatureValue
            };
            _getOrderMock.Setup(x => x.Execute(It.IsAny<GetOrderItemQuery>())).ReturnsAsync(
                new DtoOrderItem
                {
                    Id = Guid.NewGuid(),
                    IsPaid = false,
                    Number = 1,
                    Sum = 100
                });
            //act
            var actual = _service.IsValidResultResponse(result);
            //assert
            Assert.IsInstanceOf<bool>(actual);
            Assert.IsFalse(actual);
        }

        [Test]
        public void IsValidResultResponse_ShouldReturnFalse_WhenOrderIsPaidYet()
        {
            //arrange
            _getSettingsMock.Setup(x => x.Execute(It.IsAny<GetPaymentSettingsQuery>())).ReturnsAsync(
                    new DtoPaymentSettings
                    {
                        MerchantLogin = "login",
                        RobokassaUrl = "url",
                        Password1 = "password",
                        Password2 = "password2"
                    });
            var signature = string.Format("{0}:{1}:{2}", "100", 1, "password");
            var md5 = new MD5CryptoServiceProvider();
            var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(signature));
            var signatureValue = BitConverter.ToString(hash).Replace("-", "").ToLower();
            DtoRobokassaResult result = new DtoRobokassaResult
            {
                InvId = "1",
                OutSum = "100",
                SignatureValue = signatureValue
            };
            _getOrderByNumberMock.Setup(x => x.Execute(It.IsAny<GetOrderItemByNumberQuery>())).ReturnsAsync(
                new DtoOrderItem
                {
                    Id = Guid.NewGuid(),
                    IsPaid = true,
                    Number = 1,
                    Sum = 100
                });
            //act
            var actual = _service.IsValidResultResponse(result);
            //assert
            Assert.IsInstanceOf<bool>(actual);
            Assert.IsFalse(actual);
        }
    }
}
