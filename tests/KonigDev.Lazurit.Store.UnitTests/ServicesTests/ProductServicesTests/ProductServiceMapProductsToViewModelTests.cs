﻿using AutoMapper;
using KonigDev.Lazurit.Auth.Model.DTO.Common;
using KonigDev.Lazurit.Hub.Model.DTO.Products.Dto;
using KonigDev.Lazurit.Store.App_Start.MappingProfiles;
using KonigDev.Lazurit.Store.Services.ProductService;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Ploeh.AutoFixture;

namespace KonigDev.Lazurit.Store.UnitTests.ServicesTests.ProductServicesTests
{
    [TestFixture]
    public class ProductServiceMapProductsToViewModelTests
    {

        private ProductService _classForTest;
        private DtoProductMarketerTableResponse _actual;
        private DtoProduct _product1;
        private DtoProduct _product2;
        private DtoProduct _product3;
        private Guid _maxRangeProductId = Guid.NewGuid();
        private string _vendorCode = "1369";

        private Fixture _fixture;

        [SetUp]
        public void SetUp()
        {
            _fixture = new Fixture();
            Mapper.Initialize(m => m.AddProfile<ProductProfile>());
            _classForTest = new ProductService();

            _product1 = _fixture.Build<DtoProduct>()
                .With(p => p.Length, 100)
                .With(p => p.Width, 100)
                .With(p => p.Height, 100)
                .With(p => p.Id, _maxRangeProductId)
                .With(p => p.MarketerRange, "616 150 018 001")
                .With(p => p.ArticleSyncCode1c, _vendorCode)
                .Create();

            _product2 = _fixture.Build<DtoProduct>()
                .With(p => p.Length, 100)
                .With(p => p.Width, 100)
                .With(p => p.Height, 100)
                .With(p => p.MarketerRange, "516 150 018 001")
                .With(p => p.ArticleSyncCode1c, _vendorCode)
                .Create();

            _product3 = _fixture.Build<DtoProduct>().Create();

            var products = new List<DtoProduct> { _product1, _product2 };
            var productMarketers = new List<DtoProductMarketer> {
                new DtoProductMarketer { VendorCode = _vendorCode, Products = products },
                _fixture.Build<DtoProductMarketer>().With(p => p.Products, new List<DtoProduct> { _product3 }).Create()};

            _actual = _fixture.Build<DtoProductMarketerTableResponse>()
                .With(p => p.Items, productMarketers).Create();
        }

        [Test]
        public void ProductService_MapProductsToViewModelTests_WhenModelIsEmpty_ShouldBeEmptyResult()
        {
            //arrange
            var productMarketer = new DtoProductMarketerTableResponse { Items = new List<DtoProductMarketer>() };

            //act
            var result = _classForTest.MapProductsToViewModel(productMarketer);

            //assert
            result.Items.Should().BeEmpty();
        }

        [Test]
        public void ProductService_MapProductsToViewModelTests_ModelNotEmpty_ShouldBeNotEmpty()
        {
            //arrange
            var productMarketer = _fixture.Build< DtoProductMarketerTableResponse> ()
                .With(p => p.TotalCount, 2)
                .With(p => p.Items, _fixture.Build<DtoProductMarketer>()
                .With(p => p.Products, _fixture.CreateMany<DtoProduct>(2)).CreateMany(2))
                .Create();

            //act
            var result = _classForTest.MapProductsToViewModel(productMarketer);
            var firstproductMarketer = result.Items.FirstOrDefault();

            var maxproductFirst = firstproductMarketer.Products.FirstOrDefault(p => p.MarketerRange == firstproductMarketer.Products.Max(pr => pr.MarketerRange));
            var maxproductLast = result.Items.Last().Products.OrderByDescending(p => p.MarketerRange).First();

            //assert
            result.Items.Should().NotBeEmpty();
            result.Items.Count().Should().Be(2);
            result.Items.Last().MaxRangeProductId.Should().Be(maxproductLast.Id);

            firstproductMarketer.Products.Count().Should().Be(2);
            firstproductMarketer.Sizes.Count().Should().Be(2);
            firstproductMarketer.Sizes.FirstOrDefault(p => p.ProductId == maxproductFirst.Id).Length.Should().Be(maxproductFirst.Length);
            firstproductMarketer.Sizes.FirstOrDefault(p => p.ProductId == maxproductFirst.Id).Width.Should().Be(maxproductFirst.Width);
            firstproductMarketer.Sizes.FirstOrDefault(p => p.ProductId == maxproductFirst.Id).Height.Should().Be(maxproductFirst.Height);
            firstproductMarketer.MaxRangeProductId.Should().Be(maxproductFirst.Id);
        }

        [Test]
        public void ProductService_MapProductsToViewModelTests_ModelNotEmpty_ShouldBeCorrectDefaultProduct()
        {
            //arrange
            //act
            var result = _classForTest.MapProductsToViewModel(_actual);
            var marketerProduct = result.Items.FirstOrDefault(p => p.VendorCode == _vendorCode);
            var sizes = marketerProduct.Sizes;

            //assert
            result.Items.Should().NotBeEmpty();
            result.Items.Count().Should().Be(2);
            result.Items.SelectMany(p => p.Sizes).Count().Should().Be(2);

            sizes.Should().NotBeEmpty();
            sizes.Count().Should().Be(1);
            sizes.FirstOrDefault().Length.Should().Be(100);
            sizes.FirstOrDefault().Width.Should().Be(100);
            sizes.FirstOrDefault().Height.Should().Be(100);
            sizes.FirstOrDefault().ProductId.Should().Be(_maxRangeProductId);

            marketerProduct.Should().NotBeNull();
            marketerProduct.Products.Count.Should().Be(1);
            marketerProduct.MaxRangeProductId.Should().Be(_maxRangeProductId);
        }
    }
}
