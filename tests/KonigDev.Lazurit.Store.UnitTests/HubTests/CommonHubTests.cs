﻿using KonigDev.Lazurit.Store.Hubs;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet;
using System.Dynamic;
using System;
using System.Security.Principal;
using Microsoft.AspNet.Identity;

namespace KonigDev.Lazurit.Store.UnitTests.HubTests
{
    [TestFixture]
    public class CommonHubTests
    {
        [SetUp]
        public void SetUp()
        { }

        [Test]
        public void Connect_ShouldAddHubIDentityAndCallOnConnected()
        {
            //arrange            

            var connectionId = "1239";
            var mockRequest = new Mock<IRequest>();
            mockRequest.Setup(x => x.User).Returns(new Mock<IPrincipal>().Object);
            mockRequest.Setup(x => x.Cookies).Returns(new Dictionary<string, Cookie> { { "cartId", new Cookie("cartId", Guid.NewGuid().ToString()) } });

            var mockContext = new HubCallerContext(mockRequest.Object, connectionId);

            dynamic all = new ExpandoObject();
            var mockClients = new Mock<IHubCallerConnectionContext<dynamic>>();
            mockClients.Setup(x => x.Caller).Returns((ExpandoObject)all);

            var commonHub = new CommonHub()
            {
                Context = mockContext,
                Clients = mockClients.Object,
            };

            bool isConnect = false;
            all.onConnected = new Action<string, string>((id, usId) =>
            {
                isConnect = true;
            });

            var usersCountBefore = commonHub.GetHubIdentities().Count();
            //act
            commonHub.Connect();

            //assert
            Assert.True(isConnect);
            Assert.AreEqual(connectionId, commonHub.GetCurrentConnectionId());
            Assert.AreEqual((usersCountBefore + 1), commonHub.GetHubIdentities().Count());
        }

        [Test]
        public void Connect_ShouldRememberConnectionId_IfUserIsNotAuthentificated()
        {
            //arrange            
            var connectionId = "987654321";
            var mockRequest = new Mock<IRequest>();
            mockRequest.Setup(x => x.User.Identity.IsAuthenticated).Returns(false);
            mockRequest.Setup(x => x.Cookies).Returns(new Dictionary<string, Cookie>());

            var mockContext = new HubCallerContext(mockRequest.Object, connectionId);

            var commonHub = new CommonHub()
            {
                Context = mockContext
            };
            //act
            commonHub.Connect();

            //assert
            Assert.AreEqual(connectionId, commonHub.GetCurrentConnectionId());
        }

        [Test]
        public void OnDisconnected_ShouldRemoveHubUserAndCallOnUserDisconnected_IfConnectionIsExists()
        {
            //arrange            

            var connectionId = "1238";
            var mockRequest = new Mock<IRequest>();
            mockRequest.Setup(x => x.User).Returns(new Mock<IPrincipal>().Object);
            mockRequest.Setup(x => x.Cookies).Returns(new Dictionary<string, Cookie> { { "cartId", new Cookie("cartId", Guid.NewGuid().ToString()) } });

            var mockContext = new HubCallerContext(mockRequest.Object, connectionId);

            dynamic all = new ExpandoObject();
            var mockClients = new Mock<IHubCallerConnectionContext<dynamic>>();
            mockClients.Setup(x => x.Caller).Returns((ExpandoObject)all);
            mockClients.Setup(x => x.All).Returns((ExpandoObject)all);

            var commonHub = new CommonHub()
            {
                Context = mockContext,
                Clients = mockClients.Object,
            };

            bool isConnect = false;
            all.onConnected = new Action<string, string>((id, usId) =>
            {
                isConnect = true;
            });
            bool isDisconnect = false;
            all.onUserDisconnected = new Action<string, string>((id, usId) =>
            {
                isDisconnect = true;
            });
          

            commonHub.Connect();
            var usersCountBefore = commonHub.GetHubIdentities().Count();
            //act
            commonHub.OnDisconnected(true);

            //assert
            Assert.True(isDisconnect);
            Assert.AreEqual((usersCountBefore - 1), commonHub.GetHubIdentities().Count());
        }

        [Test]
        public void GetCurrentConnectionId_ShouldReturnLastConnectionId_IfConnectionWas()
        {
            //arrange            

            var connectionId = "1234";
            var mockRequest = new Mock<IRequest>();
            mockRequest.Setup(x => x.User).Returns(new Mock<IPrincipal>().Object);
            mockRequest.Setup(x => x.Cookies).Returns(new Dictionary<string, Cookie> { { "cartId", new Cookie("cartId", Guid.NewGuid().ToString()) } });

            var mockContext = new HubCallerContext(mockRequest.Object, connectionId);

            dynamic all = new ExpandoObject();
            var mockClients = new Mock<IHubCallerConnectionContext<dynamic>>();
            mockClients.Setup(x => x.Caller).Returns((ExpandoObject)all);

            var commonHub = new CommonHub()
            {
                Context = mockContext,
                Clients = mockClients.Object
            };
            bool isConnect = false;
            all.onConnected = new Action<string, string>((id, usId) =>
            {
                isConnect = true;
            });

            commonHub.Connect();

            //act
            var actual = commonHub.GetCurrentConnectionId();

            //assert            
            Assert.AreEqual(connectionId, actual);
        }
    }
}
