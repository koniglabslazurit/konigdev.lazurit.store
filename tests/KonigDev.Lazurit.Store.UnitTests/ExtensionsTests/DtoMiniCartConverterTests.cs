﻿using KonigDev.Lazurit.Hub.Model.DTO.Cart.MiniCartDto;
using KonigDev.Lazurit.Store.Extensions.Converters.Cart.CartMini;
using KonigDev.Lazurit.Store.UnitTests.ServicesTests.PriceServiceTests;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Store.UnitTests.ExtensionsTests
{
    [TestFixture]
    class DtoMiniCartConverterTests
    {
        private PriceServiceCartMiniBuilder _cartMiniBuilder;
        private DtoMiniCart _dtoCartMiniMock;

        [SetUp]
        public void SetUp()
        {
            _cartMiniBuilder = new PriceServiceCartMiniBuilder();
            _dtoCartMiniMock = _cartMiniBuilder
                .WithProducts(Guid.NewGuid(), Guid.NewGuid())
                .WithCollections(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid())
                .WithComplects(Guid.NewGuid(), Guid.NewGuid())
                .Build();
        }

        [Test]
        public void MapCart_CartIsEmpty_ReturnCorrectDto()
        {
            //arrange
            var cartId = Guid.NewGuid();
            //act
            var cartPriceResult = DtoMiniCartConverter.ToPriceResult(new DtoMiniCart { Id = cartId, IsDeliveryRequired = false });
            //assert
            Assert.IsTrue(cartPriceResult.Id == cartId);
            Assert.IsTrue(cartPriceResult.Products.Count == 0 && cartPriceResult.Collections.Count == 0 && cartPriceResult.Complects.Count == 0);
        }

        [Test]
        public void MapCart_CartIsNotEmpty_ReturnCorrectDto()
        {
            //arrange
            var cartId = Guid.NewGuid();
            //act
            var cartPriceResult = DtoMiniCartConverter.ToPriceResult(_dtoCartMiniMock);
            //assert
            Assert.IsTrue(cartPriceResult.Products.Count == 2 && cartPriceResult.Collections.Count == 2 && cartPriceResult.Complects.Count == 2);
        }
    }
}
