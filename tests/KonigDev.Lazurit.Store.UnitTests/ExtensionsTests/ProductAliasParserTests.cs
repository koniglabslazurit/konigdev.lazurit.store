﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Hub.Model.DTO.CollectionVariants.Query;
using KonigDev.Lazurit.Model.Entities;
using KonigDev.Lazurit.Store.Extensions;
using Moq;
using NUnit.Framework;

namespace KonigDev.Lazurit.Store.UnitTests.ExtensionsTests
{
    [TestFixture]
    class ProductAliasParserTests
    {
        [SetUp]
        public void SetUp()
        {
            //use it if you'll need in future
        }

        [Test]
        public void ParseAlias_StringIsValid_GetCurrectResponse()
        {
            //arrange
            string alias = "collection-magna-5678df";
            //act
            var res = ProductAliasParser.ParseAlias(alias);

            //assert
            Assert.True(res.ArticleCodeFull == "5678df");
            Assert.True(res.ArticleCodeOnlyDigits == "5678");
            Assert.True(res.CollectionAlias == "collection-magna");
            Assert.True(!res.HasErrors);
        }

        [Test]
        public void ParseAlias_StringIsAlmostValid_GetCurrectResponse()
        {
            //arrange
            string alias = "collection-magna-5678df-";
            //act
            var res = ProductAliasParser.ParseAlias(alias);

            //assert
            Assert.True(res.ArticleCodeFull == "5678df");
            Assert.True(res.ArticleCodeOnlyDigits == "5678");
            Assert.True(res.CollectionAlias == "collection-magna");
            Assert.True(!res.HasErrors);
        }

        [Test]
        public void ParseAlias_StringIsInvalid_GetResponseWithErrorMark()
        {
            //arrange
            string alias = "collection-magna/5678df-";
            //act
            var res = ProductAliasParser.ParseAlias(alias);

            //assert
            Assert.True(res.HasErrors);
        }
    }
}
