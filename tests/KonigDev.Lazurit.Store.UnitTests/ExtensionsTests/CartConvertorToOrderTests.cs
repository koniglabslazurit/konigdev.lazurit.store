﻿using NUnit.Framework;
using KonigDev.Lazurit.Store.Dto.Cart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Store.Dto.Cart.CartFull;
using KonigDev.Lazurit.Store.Extensions.Converters.Cart.CartFull;
using KonigDev.Lazurit.Store.Models.Prices.KonigDev.Lazurit.Store.Models.Prices;
using KonigDev.Lazurit.Store.Models.Prices;
using KonigDev.Lazurit.Store.Models.Localization;

namespace KonigDev.Lazurit.Store.UnitTests.ExtensionsTests
{
    [TestFixture]
    public class CartConvertorToOrderTests
    {
        private DtoCartFullPriceResult cart;
        private Currency _currency;
        private Money _money;
        private decimal _amount = 1.2M;

        [SetUp]
        public void SetUp()
        {
            _currency = new Currency(new Language("ru-RU"), "RUB");
            _money = new Money(_amount, _currency);
        }

        //TODO Не понятно зачем это функциональность, так как метод расширения и он не сможет вызваться как расширение базового объекта, который равен null
        [Test]
        public void CartToOrder_CartIsNull_ReturnNull()
        {
            //arrange
            DtoCartFullPriceResult cartNull = null;
            //act
            var res = DtoCartConverter.ToOrder(cartNull);
            //assert
            Assert.IsNull(res);
        }


        [Test]
        public void CartToOrder_CartIsNotNull_CorrectMapProducts()
        {
            //arrange
            var productId = Guid.NewGuid();
            cart = new DtoCartFullPriceResult
            {
                TotalPrice = _money,
                Collections = new List<DtoCartFullItemPriceCollection>(),
                Products = new List<DtoCartFullItemPriceProduct>
                {
                    new DtoCartFullItemPriceProduct { Discount = 10, IsAssemblyRequired = true, Price = _money, SalePrice = _money, ProductId = productId, Quantity = 1 } },
                Complects = new List<DtoCartFullItemPriceComplect>()
            };
            //act
            var res = DtoCartConverter.ToOrder(cart);
            var product = res.Products.FirstOrDefault(p => p.ProductId == productId);
            //assert
            Assert.AreEqual(product.Discount, 10);
            Assert.IsTrue(product.IsAssemblyRequired);
            Assert.AreEqual(product.Quantity, 1);
        }
    }
}
