﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace KonigDev.Lazurit.Store.UnitTests
{
    [TestFixture]
    public class SampleTests
    {
        [Test]
        public void TestFirst()
        {
            //arrange
            //act
            //assert
            Assert.True(true);
        }
        [Test]
        public void TestSecond()
        {
            //arrange
            //act
            //assert
            Assert.True(true);
        }

        [Test]
        public void TestThrid()
        {
            //arrange
            var token =
                "<W>Ty+bN";
            //act
            var tokenHtml = HttpUtility.HtmlEncode(token);
            //assert
            Assert.IsFalse(tokenHtml == token);
        }
    }
}
