﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Hub.Grains.Interfaces;
using KonigDev.Lazurit.Store.Controllers.ApiControllers;
using KonigDev.Lazurit.Store.Services.CookieService;
using NUnit.Framework;
using Moq;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Roles;
using KonigDev.Lazurit.Store.Dto.UserRole;

namespace KonigDev.Lazurit.Store.UnitTests.ControllersTests
{
    [TestFixture]
    public class UserControllerTests
    {
        private Mock<IHubHandlersFactory> _mockhubFactory;
        private Mock<IAuthHandlersFactory> _mockauthFactory;
        private UserController _classForTest;
        [SetUp]
        public void SetUp()
        {
            _mockhubFactory = new Mock<IHubHandlersFactory>();
            _mockauthFactory = new Mock<IAuthHandlersFactory>();
            _classForTest = new UserController(_mockhubFactory.Object, _mockauthFactory.Object);
        }

        //[Test]
        //public void ChangeRole_ShouldReturnExpection_IfOneOfQueryInvalid()
        //{
        //    //arrange
        //    var command1 = new UpdateUserRoleCmd
        //    {
        //        Role = "",
        //        UserId=Guid.NewGuid()
        //    };
        //    var command2 = new UpdateUserRoleCmd
        //    {
        //        Role = "buyer",
        //        UserId = Guid.NewGuid()
        //    };

        //    var mockCommand = new Mock<IHubCommandHandler<UpdateUserRoleCmd>>();
        //    var grain = _mockhubFactory.Setup(x => x.CreateCommandHandler<UpdateUserRoleCmd>()).Returns(mockCommand.Object);
        //    mockCommand.Setup(x => x.Execute(command1)).Returns(new Task(null));
        //    mockCommand.Setup(x => x.Execute(command2)).Returns(new Task(null));
        //    var query = new List<UpdateUserRoleQuery>
        //    {
        //        new UpdateUserRoleQuery
        //        {
        //            Role = command1.Role,
        //            UserId = command1.UserId
        //        },
        //        new UpdateUserRoleQuery
        //        {
        //            Role=command2.Role,
        //            UserId = command2.UserId
        //        }
        //    };
        //    //act
        //    //var actual = _classForTest.ChangeRole(query);
        //    //assert
        //    Throws<AggregateException>(() => { _classForTest.ChangeRole(query); });
        //}
    }
}
