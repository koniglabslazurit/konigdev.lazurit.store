﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Store.Models.Prices.KonigDev.Lazurit.Store.Models.Prices;
using Newtonsoft.Json;
using NUnit.Framework;
using KonigDev.Lazurit.Store.Models.Constants;

namespace KonigDev.Lazurit.Store.UnitTests.InfrastructureTests
{
    [TestFixture]
    public class MoneySerializerTests
    {
        [Test]
        public void MoneySerializer_NotEmaptyMoney_SerializedMoneyObject()
        {
            //arr
            var jsonMoney =
                "{\"InternalAmount\":50.0,\"Amount\":50.0,\"TruncatedAmount\":50.0,\"FormattedAmount\":\"50,00 руб\",\"FormattedAmountWithoutPoint\":\"50 руб\",\"FormattedAmountWithoutPointAndCurrency\":\"50 \",\"Currency\":{\"Code\":\"RUB\",\"CultureName\":\"ru-RU\",\"Symbol\":\"руб\",\"EnglishName\":\"Russian Ruble\",\"ExchangeRate\":1.0,\"CustomFormatting\":null},\"DecimalDigits\":2}";
            var money = new Money(50d, new Models.Prices.Currency(new Models.Localization.Language("ru-RU"), "RUB", String.Empty, Common.RubSymbol, 1));

            //act
            var result = JsonConvert.SerializeObject(money);

            //ass
            Assert.IsFalse(string.IsNullOrWhiteSpace(result));
            Assert.IsTrue(result == jsonMoney);
        }

        [Test]
        public void MoneySerializer_NotEmaptyMoney_Desirialize()
        {
            //arr
            var money = new Money(50d, new Models.Prices.Currency(new Models.Localization.Language("ru-RU"), "RUB", String.Empty, Common.RubSymbol, 1));

            //act
            var json = JsonConvert.SerializeObject(money);
            Money result = JsonConvert.DeserializeObject<Money>(json);

            //ass
            Assert.IsTrue(result.Amount == 50);
            Assert.IsTrue(result.Currency.Code == "RUB");
            Assert.IsTrue(result.Currency.CultureName == "ru-RU");
            Assert.IsTrue(result.Currency.Symbol == Common.RubSymbol);
        }

        [Test]
        public void MoneySerializer_EmptyMoney_ThrowJsonSerializationException()
        {
            //arr
            var money = new Money();

            //act
            //ass
            Assert.Throws<JsonSerializationException>(()=> JsonConvert.SerializeObject(money));
        }
    }
}
